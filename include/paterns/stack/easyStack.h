#ifndef EASY_STACK_H
#define EASY_STACK_H

#include <cstring>
#include <algorithm>
#include "alignmentMemory.h"

template <typename T>
class EasyStack
{
private:
    T *memory;
    sizeM pos;
    sizeM capacity;
    bool autoUpdate;

    T defaultValue;
    bool useDefault;

private:
    bool checkIsBegin() const
    {
        return pos == 0;
    }

    bool checkIsEnd() const
    {
        return pos == capacity;
    }

public:
    T *assignmentMemory()
    {
        if (pos + 1 > capacity)
        {
            if (autoUpdate)
            {
                sizeM c = 1;
                setCapacity(std::max(c, capacity * 2));
                return assignmentMemory();
            }
            {
                return NULL;
            }
        }
        else
        {
            T *mem = memory + pos;
            pos++;
            return mem;
        }
    }

    T *assignmentMemoryAndCopy(const T *_src)
    {
        if (pos + 1 > capacity)
        {
            if (autoUpdate)
            {
                sizeM c = 1;
                setCapacity(std::max(c, capacity * 2));
                return assignmentMemoryAndCopy(_src);
            }
            {
                return NULL;
            }
        }
        else
        {
            T *mem = memory + pos;
            *mem = *_src;
            pos++;
            return mem;
        }
    }

    void popMemory()
    {
        if (pos != 0)
        {
            pos--;
        }
    }

    T *getMemoryAndPop()
    {
        if (pos != 0)
        {
            T *m = getMemory(pos - 1);
            pos--;

            return m;
        }
        else
        {
            return NULL;
        }
    }

    T *getMemory(sizeM index) const
    {
        return memory + index;
    }

    void clearMemory()
    {
        pos = 0;
    }

    void setCapacity(sizeM _capacity)
    {
        T *new_memory = new T[_capacity];
        if (useDefault)
        {
            for (int i = 0; i < (int)_capacity; i++)
            {
                new_memory[i] = defaultValue;
            }
        }
        sizeM __capacity = _capacity;

        if (memory == NULL)
        {
            memory = new_memory;
            capacity = __capacity;
            return;
        }

        if (capacity < _capacity)
        {
            _capacity = capacity;
        }

        std::memcpy((void *)new_memory, (void *)memory, _capacity * sizeof(T));
        delete[] memory;

        memory = new_memory;
        capacity = __capacity;

        if (pos > _capacity)
        {
            pos = _capacity;
        }
    }

public:
    EasyStack()
    {
        memory = NULL;
        pos = 0;
        capacity = 0;
        autoUpdate = true;
        useDefault = false;
        setCapacity(1);
    }

    EasyStack(sizeM _capacity)
    {
        memory = NULL;
        pos = 0;
        capacity = 0;
        autoUpdate = true;
        useDefault = false;
        setCapacity(_capacity);
    }

    EasyStack(const EasyStack &_easy_stack)
    {
        useDefault = false;
        capacity = _easy_stack.capacity;
        pos = _easy_stack.pos;
        memory = new T[capacity];
        std::memcpy(memory, _easy_stack.memory, capacity);
    }

    ~EasyStack()
    {
        delete[] memory;
    }

    sizeM getPos() const { return pos; }

    sizeM getCapacity() const { return capacity; }

    bool getAutoUpdate() const { return autoUpdate; }

    void setAutoUpdate(bool state) { autoUpdate = state; }

    void setDefault(const T *def) { defaultValue = *def; }

    const T *getDefault() const { return &defaultValue; }

    void setUseDefault(bool use) { useDefault = use; }

    bool getUseDefault() const { return useDefault; }
};

#endif