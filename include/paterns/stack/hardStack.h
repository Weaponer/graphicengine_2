#ifndef HARD_STACK_H
#define HARD_STACK_H

#include <cstring>
#include <algorithm>
#include "alignmentMemory.h"

class HardStack
{
private:
    char *memory;
    sizeM pos;
    sizeM capacity;
    bool autoUpdate;

private:
    bool checkIsBegin()
    {
        return pos == 0;
    }

    bool checkIsEnd()
    {
        return pos == capacity;
    }

    sizeM correctCountMemory(sizeM _count)
    {
        sizeM e = _count % ALIGNMENT_STEP;
        if (_count % ALIGNMENT_STEP == 0)
        {
            return _count;
        }
        else
        {
            return _count + (4 - e);
        }
    }

public:
    template <typename T>
    T *assignmentMemory()
    {
        sizeM countM = correctCountMemory(sizeof(T));
        if (pos + countM + sizeof(sizeM) > capacity)
        {
            if (autoUpdate)
            {
                sizeM c = countM + sizeof(sizeM);
                setCapacity(std::max(c, capacity * 2));
                return assignmentMemory<T>();
            }
            {
                return NULL;
            }
        }
        else
        {
            T *mem = (T *)(memory + pos);
            pos += countM;
            std::memcpy(memory + pos, &countM, sizeof(sizeM));
            pos += sizeof(sizeM);
            return mem;
        }
    }

    template <typename T>
    T *assignmentMemoryAndCopy(const T *_src)
    {
        sizeM countM = correctCountMemory(sizeof(T));
        if (pos + countM + sizeof(sizeM) > capacity)
        {
            if (autoUpdate)
            {
                sizeM c = countM + sizeof(sizeM);
                setCapacity(std::max(c, capacity * 2));
                return assignmentMemoryAndCopy<T>(_src);
            }
            {
                return NULL;
            }
        }
        else
        {
            T *mem = (T *)(memory + pos);

            *(T *)(memory + pos) = *_src;
            pos += countM;

            std::memcpy(memory + pos, &countM, sizeof(sizeM));
            pos += sizeof(sizeM);
            return mem;
        }
    }

    void popMemory()
    {
        if (checkIsBegin())
            return;
        sizeM countMemoryPos = pos - sizeof(sizeM);
        sizeM *countMemory = (sizeM *)(memory + countMemoryPos);
        pos -= sizeof(sizeM) + (*countMemory);
    }

    void clearMemory()
    {
        pos = 0;
    }

    void setCapacity(sizeM _capacity)
    {
        char *new_memory = new char[_capacity];
        sizeM __capacity = _capacity;

        if (memory == NULL)
        {
            memory = new_memory;
            capacity = __capacity;
            return;
        }

        if (capacity < _capacity)
        {
            _capacity = capacity;
        }

        std::memcpy(new_memory, memory, _capacity);
        delete[] memory;

        memory = new_memory;
        capacity = __capacity;

        if (pos > _capacity)
        {
            pos = _capacity;
        }
    }

public:
    HardStack()
    {
        memory = NULL;
        pos = 0;
        capacity = 0;
        autoUpdate = true;
        setCapacity(sizeof(long));
    }

    HardStack(sizeM _capacity)
    {
        memory = NULL;
        pos = 0;
        capacity = 0;
        autoUpdate = true;
        setCapacity(_capacity);
    }

    HardStack(const HardStack &_hard_stack)
    {
        capacity = _hard_stack.capacity;
        pos = _hard_stack.pos;
        memory = new char[capacity];
        std::memcpy(memory, _hard_stack.memory, capacity);
    }

    ~HardStack()
    {
        delete[] memory;
    }

    sizeM getPos() const { return pos; }

    sizeM getCapacity() const { return capacity; }

    bool getAutoUpdate() const { return autoUpdate; }

    void setAutoUpdate(bool state) { autoUpdate = state; }
};

#endif