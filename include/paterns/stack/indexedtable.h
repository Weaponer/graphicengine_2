#ifndef INDEXEDTABLE_H
#define INDEXEDTABLE_H

#include "alignmentMemory.h"
#include "easyStack.h"

template <typename T>
class Indexedtable
{
private:
    EasyStack<T> stack;
    EasyStack<sizeM> freeElements;

public:
    sizeM getCapacity() const
    {
        return stack.getCapacity();
    }

    sizeM getCountUse() const
    {
        return stack.getPos() - freeElements.getPos();
    }

    sizeM getPosBuffer() const
    {
        return stack.getPos();
    }

    T *getMemory(sizeM index)
    {
        return stack.getMemory(index);
    }

    T *assignmentMemory(sizeM &index)
    {
        if (freeElements.getPos() != 0)
        {
            index = *freeElements.getMemoryAndPop();
            return stack.getMemory(index);
        }

        T *el = stack.assignmentMemory();
        index = stack.getPos() - 1;
        return el;
    }

    T *assignmentMemoryAndCopy(const T *_src, sizeM &index)
    {
        if (freeElements.getPos() != 0)
        {
            index = *freeElements.getMemoryAndPop();

            T *dest = stack.getMemory(index);
            *dest = *_src;
            return stack.getMemory(index);
        }

        T *el = stack.assignmentMemoryAndCopy(_src);
        index = stack.getPos() - 1;
        return el;
    }

    void setDefault(const T *def) { stack.setDefault(def); }

    const T *getDefault() const { return stack.getDefault(); }

    void setUseDefault(bool use) { stack.setUseDefault(use); }

    bool getUseDefault() const { return stack.getUseDefault(); }

    void popMemory(sizeM index)
    {
        if (index == stack.getPos() - 1)
        {
            stack.popMemory();
        }
        else
        {
            freeElements.assignmentMemoryAndCopy(&index);
        }
        if (getUseDefault())
        {
            T *element = getMemory(index);
            *element = *getDefault();
        }
    }

    void clearMemory()
    {
        stack.clearMemory();
        freeElements.clearMemory();
    }

public:
    Indexedtable() : stack(), freeElements()
    {
    }

    Indexedtable(sizeM _capacity) : stack(_capacity), freeElements(_capacity)
    {
    }

    Indexedtable(const Indexedtable &_indexedtable) : stack(_indexedtable.stack), freeElements(_indexedtable.freeElements)
    {
    }

    ~Indexedtable()
    {
    }
};

#endif