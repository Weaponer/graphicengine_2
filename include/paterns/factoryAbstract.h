#ifndef FACTORY_ABSTRACT_H
#define FACTORY_ABSTRACT_H

#include<list>
#include<algorithm>

template<class ObjectT, class DefineT> class AbstactFactory;

template<class ObjectT, class DefineT> class GeneratorObject
{
protected:
    AbstactFactory<ObjectT, DefineT>* factory;

public:
    GeneratorObject() {}

    virtual bool checkIs(DefineT define) = 0;

    virtual ObjectT generate(DefineT define) = 0;

    virtual ~GeneratorObject() {}

    friend class AbstactFactory<ObjectT, DefineT>;
};

template<class ObjectT, class DefineT> class AbstactFactory
{
protected:
    std::list<GeneratorObject<ObjectT, DefineT>*> registerTypes;

    GeneratorObject<ObjectT, DefineT>* previousGenerator;
public:
    AbstactFactory() : previousGenerator(NULL) {}

    template<class T> void addRegisterType()
    {
        T* reg = new T();
        reg->factory = this;
        registerTypes.push_back(reg);
    }

    void addRegisterType(GeneratorObject<ObjectT, DefineT>* generator)
    {
        generator->factory = this;
        registerTypes.push_back(generator);
    }

    virtual ObjectT getDefault() = 0;

    ObjectT loadGenerator(DefineT define)
    {        
        if(previousGenerator != NULL)
        {
            if(previousGenerator->checkIs(define))
            {
                return previousGenerator->generate(define);
            }
        }
        for(auto i = registerTypes.begin(); i != registerTypes.end(); i++)
        {
            if((*i)->checkIs(define))
            {
                previousGenerator = (*i);
                return (*i)->generate(define);
            }
        }
        return getDefault();
    }

    virtual ~AbstactFactory() 
    {
        std::for_each(registerTypes.begin(), registerTypes.end(), [](GeneratorObject<ObjectT, DefineT>* generator) { delete generator; });
    }
};


#endif