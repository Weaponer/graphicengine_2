#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include <string>
#include <list>
#include <algorithm>

#include "layerManager/layer.h"
#include "objectRegister/object.h"
#include "gameObject/transform.h"
#include "gameObject/component/component.h"

class Scene;

class GameObject : public Object
{
private:
    std::string name;
    Layer layer;
    Scene *scene;

    Transform transform;
    std::list<Component *> components;

public:
    GameObject();

    virtual ~GameObject();

    Transform *getTransform() { return &transform; }

    const char *getName()
    {
        return name.c_str();
    }

    void setName(const char *str)
    {
        name = str;
    }

    void addComponent(Component *component);

    void removeComponent(Component *component);

    template <class T>
    T *getComponent()
    {
        auto end = components.end();
        auto find = std::find_if(components.begin(), components.end(), [](Component *component)
                                 { return typeid(T) == typeid(*component); });
        if (find == end)
        {
            return NULL;
        }
        else
        {
            return static_cast<T *>(*find);
        }
    }

    template <class T>
    int getComponents(Component **ar, int lenght)
    {
        int pos = 0;
        auto end = components.end();
        for (auto i = components.begin(); i != end; ++i)
        {
            if (typeid(**i) == typeid(T))
            {
                ar[pos] = static_cast<T *>(*i);
                pos++;
            }

            if (pos >= lenght - 1)
            {
                break;
            }
        }
        return pos;
    }

    std::list<Component *> *getComponents() { return &components; }

    void setScene(Scene *_scene) { scene = _scene; }

    Scene *getScene() const { return scene; }

    Layer getLayer() const { return layer; }

    void setLayer(Layer _layer);

    void setActive(bool _active);

    bool getActive();

    bool getLocalActive();

    void updateStatusActive();
};

#endif
