#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <MTH/vectors.h>
#include <MTH/quaternion.h>
#include <list>

#include "paterns/stack/indexedtable.h"

class GameObject;

class Transform
{
private:
    GameObject *gameObject;

    bool localActive;
    bool globalActive;

    Transform *parent;
    std::list<Transform *> children;
    int branch;

    mth::mat4 localMatrix;
    mth::mat4 globalMatrix;

    mth::vec3 localPosition;
    mth::quat localRotate;

    mth::vec3 position;
    mth::quat rotate;

    mth::vec3 scale;
    mth::vec3 localScale;

    char maskChangeTransform;

public:
    Transform(GameObject *_gameObject);

    ~Transform() {}

    GameObject *getGameObject() const;

    void setActive(bool _active);

    bool getActive();

    bool getLocalActive();

    void setScale(mth::vec3 _scale);

    mth::vec3 getScale() const;

    mth::vec3 getLocalScale() const;


    void setLocalPosition(mth::vec3 _pos);

    mth::vec3 getLocalPosition() const;

    void setPosition(mth::vec3 _pos);    

    mth::vec3 getPosition() const;


    void setLocalRotationEuler(mth::vec3 _rot);

    void setLocalQuaternion(mth::quat _quaternion);

    void setRotationEuler(mth::vec3 _rot);

    void setQuaternion(mth::quat _quaternion);

    mth::quat getQuaternion() const;

    mth::quat getLocalQuaternion() const;
    
    void setBranch(int _number);

    void setParent(Transform *_transform);
    
    int getBranch() const;

    Transform * getParent() const;

    int getCountChildren() const;

    const std::list<Transform *> *getListChildren() const;

    Transform *getChildren(int _index) const;

    mth::mat4 getLocalMatrix() const;

    mth::mat4 getGlobalMatrix() const;

    void updateDatas();

    bool isChangeMove() const;

    bool isChangeRotation() const;

    bool isChangeScale() const;

    char getMaskChange() const { return maskChangeTransform; }

    void resetMaskChange();

};

#endif