#ifndef SPHERE_COLLIDER_COMPONENT_H
#define SPHERE_COLLIDER_COMPONENT_H

#include "gameObject/component/component.h"
#include "physics/physicsEngine.h"
#include "debugOutput/threadDebugOutput.h"
#include "sphereCollider.h"
#include "colliderComponent.h"

class SphereColliderComponent : public ColliderComponent
{
private:
    ThreadDebugOutput *mainDebugOutput;

    float radius;

public:
    SphereColliderComponent(ThreadDebugOutput *mainDebugOutput, PhysicsEngine *phsyics);

    virtual ~SphereColliderComponent();

    float getRadius() const { return radius; }

    void setRadius(float radius);

    void setPosition(const mth::vec3 &pos) override;

    void update() override;

protected:
    void onInit() override;

    void onDestroy() override;
};

#endif