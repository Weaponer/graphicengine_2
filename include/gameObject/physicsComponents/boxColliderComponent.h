#ifndef BOX_COLLIDER_COMPONENT_H
#define BOX_COLLIDER_COMPONENT_H

#include "gameObject/component/component.h"
#include "physics/physicsEngine.h"
#include "debugOutput/threadDebugOutput.h"
#include "boxCollider.h"
#include "colliderComponent.h"

class BoxColliderComponent : public ColliderComponent
{
private:
    ThreadDebugOutput *mainDebugOutput;

    mth::vec3 size;

public:
    BoxColliderComponent(ThreadDebugOutput *mainDebugOutput, PhysicsEngine *phsyics);

    virtual ~BoxColliderComponent();

    mth::vec3 getSize() const { return size; }

    void setSize(const mth::vec3 &size);

    void setPosition(const mth::vec3 &pos) override;

    void update() override;

protected:
    void onInit() override;

    void onDestroy() override;
};

#endif