#ifndef RIGIDBODY_COMPONENT_H
#define RIGIDBODY_COMPONENT_H

#include "gameObject/component/component.h"

#include "rigidbodyActor.h"

#include "physics/physicsEngine.h"
#include "gameObject/physicsComponents/colliderComponent.h"

class Rigidbody : public Component
{
private:
    PhysicsEngine *engine;
    RigidbodyActor *actor;

public:
    Rigidbody(PhysicsEngine *engine);

    virtual ~Rigidbody();

    void update() override;

    void connectCollider(ColliderComponent *collider);

    void disconnectCollider(ColliderComponent *collider);

    void setMass(float mass) { actor->setMass(mass); }

    float getMass() const { return actor->mass; }

    void setKinematic(bool _b) { engine->setKinematic(actor, _b); }

    bool getKinematic() const { return actor->kinematic; }

    void addForce(const mth::vec3 &force) { actor->addForce(force); }

    void addForceAtPoint(const mth::vec3 &force, const mth::vec3 &point) { actor->addForceAtPoint(force, point); }

    void addTorque(const mth::vec3 &torque) { actor->addTorque(torque); }

    mth::vec3 getVelocity() const { return actor->velocity; }

    mth::vec3 getAngularVelocity() const { return actor->angularVelocity; }

    void setVelocity(mth::vec3& vel) { actor->velocity = vel; }

    void setAngularVelocity(mth::vec3& vel) { actor->angularVelocity = vel; }

    void updateOnNewLayer(Layer layer) override;

protected:
    void onInit() override;

    void onDestroy() override;

    void onEnable() override;

    void onDisable() override;
};

#endif