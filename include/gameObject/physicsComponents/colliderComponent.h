#ifndef COLLIDER_COMPONENT_H
#define COLLIDER_COMPONENT_H

#include "gameObject/component/component.h"
#include "physics/physicsEngine.h"
#include "collider.h"

class ColliderComponent : public Component
{
protected:
    PhysicsEngine *phsyics;
    Collider *collider;

    mth::vec3 origin;

public:
    ColliderComponent(PhysicsEngine *phsyics) : phsyics(phsyics), collider(NULL), origin(0, 0, 0)
    {
    }

    virtual ~ColliderComponent()
    {
    }

    Collider *getColliderData() { return collider; }

    virtual mth::vec3 getPosition() const { return origin; }

    virtual void setPosition(const mth::vec3 &pos) = 0;

    void onEnable() override
    {
        collider->enable = true;
    }

    void onDisable() override
    {
        collider->enable = false;
    }
};

#endif