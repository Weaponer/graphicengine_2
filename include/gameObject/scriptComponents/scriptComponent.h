#ifndef SCRIPT_COMPONENT_H
#define SCRIPT_COMPONENT_H

#include "gameObject/component/component.h"

#include "scriptEngine/base/objectScriptData.h"
#include "scriptEngine/base/scriptEngine.h"

class ScriptComponent : public Component
{
private:
    ScriptEngine *scriptEngine;
    ObjectScriptData *osd;

public:
    ScriptComponent(ScriptEngine *_scriptEngine);

    virtual ~ScriptComponent();

    void setType(const char *type);

    ObjectScriptData *getObjectScriptData() { return osd; }

protected:
    void onInit() override;

    void onDestroy() override;

    void onEnable() override;

    void onDisable() override;
};

#endif