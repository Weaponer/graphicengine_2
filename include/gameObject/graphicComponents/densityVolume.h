#ifndef DENSITY_VOLUME_H
#define DENSITY_VOLUME_H

#include <MTH/color.h>

#include "gameObject/component/component.h"
#include "renderEngine/graphicEngine.h"

#include "renderEngine/mainSystems/sceneStructure/entities/densityVolumeGE.h"

class DensityVolume : public Component
{
public:
    enum DENSITY_VOLUME_TYPE : int
    {
        DENSITY_VOLUME_TYPE_CUBE = 0,
        DENSITY_VOLUME_TYPE_SPHERE = 1,
        DENSITY_VOLUME_TYPE_CYLINDER = 2,
    };

    enum DENSITY_VOLUME_PLACEMENT_PLANE : int
    {
        DENSITY_VOLUME_PLACEMENT_PLANE_XY = 0,
        DENSITY_VOLUME_PLACEMENT_PLANE_XZ = 1,
        DENSITY_VOLUME_PLACEMENT_PLANE_YZ = 2,
    };

private:
    GraphicEngine *gEngine;
    SceneStructure *sceneStructure;

    DensityVolumeGE densityVolume;

public:
    DensityVolume(GraphicEngine *gEngine);

    virtual ~DensityVolume();

protected:
    void onInit() override;

    void onDestroy() override;

    void onEnable() override;

    void update() override;

    void onDisable() override;

public:
    void setDensityVolumeType(DENSITY_VOLUME_TYPE _type);

    DENSITY_VOLUME_TYPE getDensityVolumeType() const;

    void setDensityVolumePlacementPlane(DENSITY_VOLUME_PLACEMENT_PLANE _placement);

    DENSITY_VOLUME_PLACEMENT_PLANE getDensityVolumePlacementPlane() const;

    void setDensity(float _density);

    float getDensity() const;

    void setColorVolume(mth::col _color);

    mth::col getColorVolume() const;

    void setPowGradient(float _powGradient);

    float getPowGradient() const;

    void setPlacementPlaneGradient(DENSITY_VOLUME_PLACEMENT_PLANE _placement);

    DENSITY_VOLUME_PLACEMENT_PLANE getPlacementPlaneGradient() const;

    void setStartDecay(float _startDecay);

    float getStartDecay() const;

    void setEndDecay(float _endDecay);

    float getEndDecay() const;

    void setEdgeFade(float _edgeFade);

    float getEdgeFade() const;
};

#endif