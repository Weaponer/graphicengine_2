#ifndef LIGHT_POINT_COMPONENT_H
#define LIGHT_POINT_COMPONENT_H

#include <MTH/vectors.h>

#include "gameObject/component/component.h"
#include "renderEngine/graphicEngine.h"
#include "renderEngine/mainSystems/sceneStructure/entities/pointLightGE.h"
#include "renderEngine/mainSystems/sceneStructure/entities/debugRenderGE.h"

class LightPoint : public Component
{
private:
    GraphicEngine *gEngine;
    SceneStructure *sceneStructure;
    PointLightGE pointLightGE;
    DebugRendererGE debugRendererGE;
    bool isChangeRadius;

public:
    LightPoint(GraphicEngine *gEngine);

    virtual ~LightPoint();

    float getRange() const { return pointLightGE.range; }

    void setRange(float range) { pointLightGE.range = range; isChangeRadius = true; }

    float getIntensivity() const { return pointLightGE.intensivity; }

    void setIntensivity(float intensivity) { pointLightGE.intensivity = intensivity; }

    bool getIsCreateShadows() const { return pointLightGE.createShadows; }

    void setIsCreateShadows(bool isCreateShadows) { pointLightGE.createShadows = isCreateShadows; }

    mth::vec3 getColor() const { return pointLightGE.color; }

    void setColor(mth::vec3 color) { pointLightGE.color = color; }

protected:
    void onInit() override;

    void onDestroy() override;

    void onEnable() override;

    void update() override;

    void onDisable() override;
};

#endif