#ifndef MESH_RENDERER_COMPONENT_H
#define MESH_RENDERER_COMPONENT_H

#include "gameObject/component/component.h"
#include "renderEngine/graphicEngine.h"
#include "renderEngine/resources/mesh.h"
#include "renderEngine/resources/material/material.h"

#include "renderEngine/mainSystems/sceneStructure/entities/meshRenderGE.h"

class MeshRenderer : public Component
{
private:
    GraphicEngine *gEngine;
    SceneStructure *sceneStructure;
    MeshRenderGE meshRenderGE;

    bool changeBound;

public:
    MeshRenderer(GraphicEngine *gEngine);

    virtual ~MeshRenderer();

    Material *getMaterial(int _lod) const;

    Mesh *getMesh(int _lod) const;

    float getCoefHeightLod(int _lod) const;

    void setMaterial(Material *m, int _lod);

    void setMesh(Mesh *m, int _lod);

    void setHeightCoefLod(int _lod, float _coefHeight);

    bool getGenerateShadows() const;

    void setGenerateShadows(bool _generate);

    int coundLods() const;

    void addLod();

    void removeLod(int _lod);

protected:
    void onInit() override;

    void onDestroy() override;

    void onEnable() override;

    void update() override;

    void onDisable() override;
};

#endif