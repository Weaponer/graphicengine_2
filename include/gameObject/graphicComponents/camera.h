#ifndef CAMERA_COMPONENT_H
#define CAMERA_COMPONENT_H

#include <MTH/vectors.h>
#include <MTH/glGen_matrix.h>
#include <MTH/matrix.h>
#include <MTH/plane.h>
#include <array>

#include "gameObject/component/component.h"
#include "renderEngine/graphicEngine.h"
#include "renderEngine/mainSystems/sceneStructure/entities/cameraGE.h"

class Camera : public Component
{
private:
    GraphicEngine *gEngine;
    CameraGE cameraGE;

public:
    Camera(GraphicEngine *gEngine);

    virtual ~Camera();

    void setOrtographicProjection(float wight, float height, float near, float front);

    void setPerspectiveProjection(float angle, float sizeScreenX, float sizeScreenY, float near, float front);

    void setProjectionMatrix(mth::mat4& matrix);

    mth::mat4 getProjectionMatrix();

    const std::array<mth::plane, 6> *getClippingPlanes();

    const std::array<mth::vec3, 8> *getPointsPiramidClipping();

protected:
    void onInit() override;

    void onDestroy() override;

    void onEnable() override;

    void update() override;

    void onDisable() override;
};

#endif