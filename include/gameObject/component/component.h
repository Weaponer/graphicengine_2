#ifndef COMPONENT_H
#define COMPONENT_H

#include "objectRegister/object.h"
#include "gameObject/transform.h"
#include "layerManager/layer.h"

class GameObject;

class Component : public Object
{
protected:
    GameObject *gameObject;
    Transform *transform;
    bool localActive;
    bool oldState;

public:
    Component();

    virtual ~Component();

    void init(GameObject *_gameObject);

    void destroy();

    virtual void update();

    Transform *getTransform();

    GameObject *getGameObject();

    virtual void updateOnNewLayer(Layer layer) {}

    virtual void onInit();

    virtual void onDestroy();

    void setActive(bool _active);

    bool getActive();

    bool getLocalActive();

    void upadteOnActive();

protected:
    virtual void onEnable() {}

    virtual void onDisable() {}
};

#endif