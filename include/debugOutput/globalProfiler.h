#ifndef GLOBAL_PROFILER_H
#define GLOBAL_PROFILER_H

#include <time.h>
#include <map>
#include <string>
#include <chrono>

class GlobalProfiler
{
private:
    struct dataSamples
    {
        std::string name;
        int countSamples;
        float allTime;
        std::chrono::_V2::system_clock::time_point startTime;
        bool isStart;
    };

    static std::map<std::string, dataSamples *> data; 

public:
    static void startSample(const char *_name);

    static float stopSample(const char *_name);

    static void writeResultInFile(const char *_path);

};

#endif