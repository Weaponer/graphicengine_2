#ifndef DEBUG_MANAGER_H
#define DEBUG_MANAGER_H

#include "debugOutput/dataDraw.h"
#include "paterns/stack/easyStack.h"
#include "paterns/stack/indexedtable.h"
#include "debugOutput/threadDebugOutput.h"
#include "renderEngine/graphicEngine.h"
#include "renderEngine/mainSystems/sceneStructure/entities/debugRenderGE.h"

class DebugOutput
{
private:
    GraphicEngine *graphicEngine;
    std::list<DebugRendererGE *> generatedDebugRenderers;

    Indexedtable<ThreadDebugOutput *> debugOutputs;
    Indexedtable<dataDraw> datas;

    EasyStack<DebugRendererGE *> graphicServices;
    int countUseServices;

    ThreadDebugOutput *mainDebugOutput;

public:
    DebugOutput();

    ~DebugOutput();

    void connectGraphicEngine(GraphicEngine *graphicEngine);

    void update(float timeDelta);

    ThreadDebugOutput *createSubDebugOutput();

    void removeSubDebugOutput(ThreadDebugOutput *subDebugOutput);

    ThreadDebugOutput *getMainDebugOutput() { return mainDebugOutput; };
    
private:
    void clearAllDataInServices();

    DebugRendererGE *pushNewGraphicServices();

    bool pushDataInService(dataDraw *data, DebugRendererGE *service);
};

#endif