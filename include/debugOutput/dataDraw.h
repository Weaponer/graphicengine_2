#ifndef DATA_DRAW_H
#define DATA_DRAW_H

#include <MTH/matrix.h>
#include <MTH/vectors.h>

struct dataDraw
{
    enum TypeDraw : char
    {
        NONE,
        LINE,
        CUBE,
        RECT
    };

    mth::mat4 mat;
    mth::vec3 firstV;
    mth::vec3 secondV;
    mth::vec3 color;
    float time;
    TypeDraw type;
};

#endif