#ifndef THREAD_DEBUG_OUTPUT_H
#define THREAD_DEBUG_OUTPUT_H

#include <thread>
#include <mutex>
#include <MTH/matrix.h>
#include <MTH/vectors.h>

#include "debugOutput/dataDraw.h"
#include "paterns/stack/easyStack.h"

class DebugOutput;

class ThreadDebugOutput
{
public:
    static ThreadDebugOutput *mainThreadDebugOutput;

private:
    DebugOutput *debugOutput;
    std::mutex mutex;
    EasyStack<dataDraw> *currentBuffer;
    EasyStack<dataDraw> *nextBuffer;

public:
    ThreadDebugOutput(DebugOutput *debugOutput);

    ~ThreadDebugOutput();

    void debugLog(const char *c_str, ...);

    void debugWarning(const char *c_str, ...);

    void debugError(const char *c_str, ...);

    void drawLine(mth::vec3 start, mth::vec3 end, mth::vec3 color = mth::vec3(1, 1, 1), float time = 0);

    void drawLine(mth::vec3 start, mth::vec3 end, mth::mat4 mat, mth::vec3 color = mth::vec3(1, 1, 1), float time = 0);

    void drawCube(mth::vec3 pos, mth::vec3 size, mth::vec3 color = mth::vec3(1, 1, 1), float time = 0);

    void drawCube(mth::vec3 pos, mth::vec3 size, mth::mat4 mat, mth::vec3 color = mth::vec3(1, 1, 1), float time = 0);

    void drawRect(mth::vec3 pos, mth::vec3 size, mth::vec3 color = mth::vec3(1, 1, 1), float time = 0);

    void drawRect(mth::vec3 pos, mth::vec3 size, mth::mat4 mat, mth::vec3 color = mth::vec3(1, 1, 1), float time = 0);

    void swapBuffer();

    const EasyStack<dataDraw> *getBufferWithData();

    void discard();

private:
    void pushData(mth::vec3 &first, mth::vec3 &second, mth::mat4 &mat, mth::vec3 &color, dataDraw::TypeDraw type, float &time);
};

#endif