#ifndef LAYER_DATA_LOAD_H
#define LAYER_DATA_LOAD_H

#include <SDL2/SDL.h>
#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "layerManager/layerData.h"
#include "scriptEngine/base/script.h"

#include "resourceLoader/asyncLoadersFactory.h"
#include "resourceLoader/syncLoadersFactory.h"


#define LAYERS_V "Layers"
#define PHYSICS_CONTACTS_V "PhysicsContacts"


class GeneratorSyncLoadLayerData : public GeneratorLoadResource<TypeResource::LAYER_DATA>
{
public:
    ResultLoad generate(const PathToResource *define) override;
};


class GeneratorAsyncLoadLayerData : public GeneratorAsyncLoadResource<TypeResource::LAYER_DATA>
{
public:
    AsyncLoader *generate(const PathToResource *define) override;
};

class LayerDataAsyncLoader : public AsyncLoader
{    
private:
    lua_State *L;
    LayerData *data;

public:
    LayerDataAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer);

    virtual~LayerDataAsyncLoader();

    virtual StatusState updateAsyncProcess() override;

    virtual StatusState updateSyncProcess() override;

};

#endif