#ifndef MODEL_OBJ_LOAD_H
#define MODEL_OBJ_LOAD_H

#include <SDL2/SDL.h>

#include "renderEngine/resources/mesh.h"
#include "renderEngine/resources/model.h"

#include "resourceLoader/asyncLoadersFactory.h"
#include "resourceLoader/syncLoadersFactory.h"

class GeneratorSyncLoadModelObj : public GeneratorLoadResource<TypeResource::MODEL_OBJ>
{
public:
    ResultLoad generate(const PathToResource *define) override;
};

class GeneratorAsyncLoadModelObj : public GeneratorAsyncLoadResource<TypeResource::MODEL_OBJ>
{
public:
    AsyncLoader *generate(const PathToResource *define) override;
};

class ModelObjAsyncLoader : public AsyncLoader
{
private:
    std::vector<std::string> names;
    std::vector<std::vector<GLushort>> indexs;
    std::vector<std::vector<mth::vec3>> positions;
    std::vector<std::vector<mth::vec2>> uvs;
    std::vector<std::vector<mth::vec3>> tangent;
    std::vector< std::vector<mth::vec3>> normals;

public:
    ModelObjAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer);

    virtual ~ModelObjAsyncLoader();

    virtual StatusState updateAsyncProcess() override;

    virtual StatusState updateSyncProcess() override;
};

#endif