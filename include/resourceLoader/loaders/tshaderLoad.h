#ifndef TSHADERS_LOAD_H
#define TSHADERS_LOAD_H
#define GL_GLEXT_PROTOTYPES

#include <GL/glu.h>

#include <SDL2/SDL.h>

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include <vector>

#define VERTEX_P "Vertex"
#define FRAGMENT_P "Fragment"
#define COMPUT_P "Comput"

#include "resourceLoader/asyncLoadersFactory.h"
#include "resourceLoader/syncLoadersFactory.h"

#include "engineBase/specialSystems.h"

class GeneratorSyncLoadPacketShader : public GeneratorLoadResource<TypeResource::PACKED_SHADER>
{
private:
    SpecialSystems *specialSystems;

public:
    GeneratorSyncLoadPacketShader(SpecialSystems *_specialSystems) : specialSystems(_specialSystems) {}

    ResultLoad generate(const PathToResource *define) override;
};

class GeneratorAsyncLoadPacketShader : public GeneratorAsyncLoadResource<TypeResource::PACKED_SHADER>
{
private:
    SpecialSystems *specialSystems;

public:
    GeneratorAsyncLoadPacketShader(SpecialSystems *_specialSystems) : specialSystems(_specialSystems) {}

    AsyncLoader *generate(const PathToResource *define) override;
};

class PacketShaderAsyncLoader : public AsyncLoader
{
private:
    SpecialSystems *specialSystems;

    std::string name;
    GLenum type;
    std::vector<std::string> libs;
    std::string program;
    std::string finalProgram;

public:
    PacketShaderAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer, SpecialSystems *_specialSystems);

    virtual ~PacketShaderAsyncLoader();

    virtual StatusState updateAsyncProcess() override;

    virtual StatusState updateSyncProcess() override;
};
#endif