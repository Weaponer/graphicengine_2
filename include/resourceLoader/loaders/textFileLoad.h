#ifndef TEXT_FILE_LOAD_H
#define TEXT_FILE_LOAD_H
#define GL_GLEXT_PROTOTYPES

#include <SDL2/SDL.h>

#include <string>

#include "resourceLoader/asyncLoadersFactory.h"
#include "resourceLoader/syncLoadersFactory.h"

#include "resourceLoader/resources/textFile.h"

class GeneratorSyncLoadTextFile : public GeneratorLoadResource<TypeResource::TEXT_FILE>
{
public:
    ResultLoad generate(const PathToResource *define) override;
};

class GeneratorAsyncLoadTextFile : public GeneratorAsyncLoadResource<TypeResource::TEXT_FILE>
{
public:
    AsyncLoader *generate(const PathToResource *define) override;
};

class TextFileAsyncLoader : public AsyncLoader
{
private:
    TextFile *textFile;

public:
    TextFileAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer);

    virtual ~TextFileAsyncLoader();

    virtual StatusState updateAsyncProcess() override;

    virtual StatusState updateSyncProcess() override;
};

#endif