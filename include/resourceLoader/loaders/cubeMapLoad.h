#ifndef CUBE_MAP_LOAD_H
#define CUBE_MAP_LOAD_H
#define GL_GLEXT_PROTOTYPES

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>
#include <array>

#include "resourceLoader/asyncLoadersFactory.h"
#include "resourceLoader/syncLoadersFactory.h"

class GeneratorSyncLoadCubeMap : public GeneratorLoadResource<TypeResource::CUBE_MAP>
{
public:
    ResultLoad generate(const PathToResource *define) override;
};

class GeneratorAsyncLoadCubeMap : public GeneratorAsyncLoadResource<TypeResource::CUBE_MAP>
{
public:
    AsyncLoader *generate(const PathToResource *define) override;
};

class CubeMapAsyncLoader : public AsyncLoader
{
private:
    lua_State *L;
    std::array<SDL_Surface *, 6> surfaces;

public:
    CubeMapAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer);

    virtual ~CubeMapAsyncLoader();

    virtual StatusState updateAsyncProcess() override;

    virtual StatusState updateSyncProcess() override;
};

#endif