#ifndef TEXTURE_LOAD_H
#define TEXTURE_LOAD_H
#define GL_GLEXT_PROTOTYPES

#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>

#include "resourceLoader/asyncLoadersFactory.h"
#include "resourceLoader/syncLoadersFactory.h"


class GeneratorSyncLoadTexture : public GeneratorLoadResource<TypeResource::TEXTURE>
{
public:
    ResultLoad generate(const PathToResource *define) override;
};


class GeneratorAsyncLoadTexture : public GeneratorAsyncLoadResource<TypeResource::TEXTURE>
{
public:
    AsyncLoader *generate(const PathToResource *define) override;
};

class TextureAsyncLoader : public AsyncLoader
{
private:
    SDL_Surface *surf;

public:
    TextureAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer);

    virtual~TextureAsyncLoader();

    virtual StatusState updateAsyncProcess() override;

    virtual StatusState updateSyncProcess() override;

};

#endif