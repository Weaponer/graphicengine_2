#ifndef SHADER_AB_LOADER_H
#define SHADER_AB_LOADER_H

#include <SDL2/SDL.h>
#include <MTH/vectors.h>
#include <MTH/color.h>
#include <string>

#include "renderEngine/resources/shader/enums.h"

#include "resourceLoader/asyncLoadersFactory.h"
#include "resourceLoader/syncLoadersFactory.h"

#include "renderEngine/resources/shader/shader.h"
#include "renderEngine/resources/texture.h"
#include "renderEngine/resources/cubeMap.h"
#include "engineBase/specialSystems.h"

#include "resourceLoader/loaders/textureLoad.h"

struct PropertyTemp
{
    std::string name;
    TypeProperty type;
};


struct IntPropertyT : public PropertyTemp
{
    int value;
};

struct FloatPropertyT : public PropertyTemp
{
    float value;
};

struct TogglePropertyT : public PropertyTemp
{
    bool value;
    int mask;
};

struct ColorPropertyT : public PropertyTemp
{
    mth::col value;
};

struct VectorPropertyT : public PropertyTemp
{
    mth::vec4 value;
};

struct TexturePropertyT : public PropertyTemp
{
    std::string defaultPath;
    bool isHDRTexture;
    Texture *texture;
};


struct CubeMapPropertyT : public PropertyTemp
{
    std::string defaultPath;
    CubeMap *cubeMap;
};

struct InternalPropertyT : public PropertyTemp
{
};

struct ProgramTemp
{
    int mask;
    std::string program;
    std::vector<std::pair<std::string, TypeProperty>> properties;
    std::vector<std::string> uniformBlocksUse;
};

struct PassTemp
{
    std::string name;
    TypePass pass;

    bool zTest;
    bool zWrite;
    QueueRender queue;
    RenderType renderType;
    CullType cullType;
    bool generateInstancing;

    bool useDefaultPrograms;
    bool useNormalMap;
    bool useClippingMap;

    int sizeAdditionalData;
    std::vector<char> additionalData;

    int maskUsingDefinesVertex;
    int maskUsingDefinesFragment;

    std::vector<ProgramTemp> vertexPrograms;
    std::vector<ProgramTemp> fragmentPrograms;
};

class GeneratorSyncLoadShader : public GeneratorLoadResource<TypeResource::SHADER>
{
private:
    SpecialSystems *specialSystems;

public:
    GeneratorSyncLoadShader(SpecialSystems *_specialSystems) : specialSystems(_specialSystems) {}

    ResultLoad generate(const PathToResource *define) override;
};

class GeneratorAsyncLoadShader : public GeneratorAsyncLoadResource<TypeResource::SHADER>
{
private:
    SpecialSystems *specialSystems;

public:
    GeneratorAsyncLoadShader(SpecialSystems *_specialSystems) : specialSystems(_specialSystems) {}

    AsyncLoader *generate(const PathToResource *define) override;
};

class ShaderAsyncLoader : public AsyncLoader
{
private:
    SpecialSystems *specialSystems;

    std::string name;
    std::vector<PropertyTemp *> tempProperties;
    ShaderSettings settings;
    std::vector<PassTemp> tempPasses;
    std::vector<std::pair<AsyncLoader *, PropertyTemp *>> textures;

public:
    ShaderAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer, SpecialSystems *_specialSystems);

    virtual ~ShaderAsyncLoader();

    virtual StatusState updateAsyncProcess() override;

    virtual StatusState updateSyncProcess() override;
};

#endif