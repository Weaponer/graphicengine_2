#ifndef HDR_TEXTURE_LOAD_H
#define HDR_TEXTURE_LOAD_H

#include <SDL2/SDL.h>

#include "resourceLoader/asyncLoadersFactory.h"
#include "resourceLoader/syncLoadersFactory.h"

struct HDRTextureLoadData
{
    int width;
    int height;
    int sizeData;
    float *data;
};

class GeneratorSyncLoadHDRTexture : public GeneratorLoadResource<TypeResource::HDR_TEXTURE>
{
public:
    ResultLoad generate(const PathToResource *define) override;
};

class GeneratorAsyncLoadHDRTexture : public GeneratorAsyncLoadResource<TypeResource::HDR_TEXTURE>
{
public:
    AsyncLoader *generate(const PathToResource *define) override;
};

class HDRTextureAsyncLoader : public AsyncLoader
{
private:
    HDRTextureLoadData hdrTextureData;

public:
    HDRTextureAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer);

    virtual ~HDRTextureAsyncLoader();

    virtual StatusState updateAsyncProcess() override;

    virtual StatusState updateSyncProcess() override;
};

#endif