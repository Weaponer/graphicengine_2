#ifndef MODEL_LIGHT_SHADER_LOAD_H
#define MODEL_LIGHT_SHADER_LOAD_H
#define GL_GLEXT_PROTOTYPES

#include <GL/glu.h>

#include <SDL2/SDL.h>

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include <vector>

#include "resourceLoader/asyncLoadersFactory.h"
#include "resourceLoader/syncLoadersFactory.h"

#include "engineBase/specialSystems.h"

class GeneratorSyncLoadModelLightShader : public GeneratorLoadResource<TypeResource::MODEL_LIGHT_SHADER>
{
private:
    SpecialSystems *specialSystems;

public:
    GeneratorSyncLoadModelLightShader(SpecialSystems *_specialSystems) : specialSystems(_specialSystems) {}

    ResultLoad generate(const PathToResource *define) override;
};

class GeneratorAsyncLoadModelLightShader : public GeneratorAsyncLoadResource<TypeResource::MODEL_LIGHT_SHADER>
{
private:
    SpecialSystems *specialSystems;

public:
    GeneratorAsyncLoadModelLightShader(SpecialSystems *_specialSystems) : specialSystems(_specialSystems) {}

    AsyncLoader *generate(const PathToResource *define) override;
};

class ModelLightShaderAsyncLoader : public AsyncLoader
{
private:
    SpecialSystems *specialSystems;

    std::string name;
    int useAdditionalLayers;
    std::vector<std::string> libs;
    std::string vertexProgram;
    std::string program;
    std::string finalVertexProgram;
    std::string finalProgram;

public:
    ModelLightShaderAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer, SpecialSystems *_specialSystems);

    virtual ~ModelLightShaderAsyncLoader();

    virtual StatusState updateAsyncProcess() override;

    virtual StatusState updateSyncProcess() override;
};
#endif