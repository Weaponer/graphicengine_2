#ifndef MATERIAL_LOAD_H
#define MATERIAL_LOAD_H

#include <SDL2/SDL.h>

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "resourceLoader/asyncLoadersFactory.h"
#include "resourceLoader/syncLoadersFactory.h"

#include "engineBase/specialSystems.h"

#define FIELD_PATH_FOR_STANDARD_TEXTURE "path"
#define FIELD_PATH_FOR_HDR_TEXTURE "pathHDRTexture"
#define FIELD_PATH_FOR_CUBE_MAP "pathCubeMap"

class GeneratorSyncLoadMaterial : public GeneratorLoadResource<TypeResource::MATERIAL>
{
private:
    SpecialSystems *specialSystems;

public:
    GeneratorSyncLoadMaterial(SpecialSystems *_specialSystems) : specialSystems(_specialSystems) {}

    ResultLoad generate(const PathToResource *define) override;
};

class GeneratorAsyncLoadMaterial : public GeneratorAsyncLoadResource<TypeResource::MATERIAL>
{
private:
    SpecialSystems *specialSystems;

public:
    GeneratorAsyncLoadMaterial(SpecialSystems *_specialSystems) : specialSystems(_specialSystems) {}

    AsyncLoader *generate(const PathToResource *define) override;
};

class MaterialAsyncLoader : public AsyncLoader
{
private:
    SpecialSystems *specialSystems;

    lua_State *L;
    std::string name;
    std::string shaderName;
    bool generateInstancing;
    std::vector<std::pair<std::string, AsyncLoader *>> loadersForParameter;

public:
    MaterialAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer, SpecialSystems *_specialSystems);

    virtual ~MaterialAsyncLoader();

    virtual StatusState updateAsyncProcess() override;

    virtual StatusState updateSyncProcess() override;
};

#endif