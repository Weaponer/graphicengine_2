#ifndef SCRIPT_LOAD_H
#define SCRIPT_LOAD_H

#include <SDL2/SDL.h>
#include "scriptEngine/base/script.h"

#include "resourceLoader/asyncLoadersFactory.h"
#include "resourceLoader/syncLoadersFactory.h"

class GeneratorSyncLoadScript : public GeneratorLoadResource<TypeResource::SCRIPT>
{
public:
    ResultLoad generate(const PathToResource *define) override;
};


class GeneratorAsyncLoadScript : public GeneratorAsyncLoadResource<TypeResource::SCRIPT>
{
public:
    AsyncLoader *generate(const PathToResource *define) override;
};

class ScriptAsyncLoader : public AsyncLoader
{
private:
    char *data;
    
public:
    ScriptAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer);

    virtual~ScriptAsyncLoader();

    virtual StatusState updateAsyncProcess() override;

    virtual StatusState updateSyncProcess() override;

};

#endif