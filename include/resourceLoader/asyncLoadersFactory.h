#ifndef ASYNC_LOADERS_FACTORY_H
#define ASYNC_LOADERS_FACTORY_H

#include "paterns/stack/indexedtable.h"
#include "objectRegister/object.h"
#include "paterns/factoryAbstract.h"
#include "pathToResource.h"

class ResourceLoader;

enum StatusState
{
    SS_NotStart,
    SS_Failed,
    SS_Succeed,
    SS_NotFinished
};

class AsyncLoader
{
protected:
    PathToResourceD path;

protected:
    Indexedtable<AsyncLoader *> *asyncLoadersBuffer;
    uint indexInBuffer;

    AsyncLoader **loaders;
    uint coundLoaders;
    Object *resource;
    bool useLoadedResource;
    StatusState statusLastOperation;
    bool isEndAsyncProcess;
    bool isEndSyncProcess;
    bool isFailed;

public:
    AsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer);

    virtual ~AsyncLoader();

    virtual StatusState updateAsyncProcess() = 0;

    virtual StatusState updateSyncProcess() = 0;

    Object *getResource();

    bool checkLoadedAdditionalResources();

    void clearAllSubloaders();

    bool checkIsFailed() { return isFailed; }

    friend ResourceLoader;

protected:
    void initSubAsyncLoader(AsyncLoader *asyncLoader);

};

template <uint T>
class GeneratorAsyncLoadResource : public GeneratorObject<AsyncLoader *, const PathToResource *>
{
public:
    GeneratorAsyncLoadResource() {}

    bool checkIs(const PathToResource *define) override
    {
        return T == define->type;
    }

    virtual ~GeneratorAsyncLoadResource() {}
};

class AsyncLoadersFactory : public AbstactFactory<AsyncLoader *, const PathToResource *>
{
private:
    ResourceLoader *resourceLoader;
    Indexedtable<AsyncLoader *> *asyncLoadersBuffer;

    AsyncLoader *getDefault() override
    {
        return NULL;
    }

public:
    AsyncLoadersFactory(ResourceLoader *resourceLoader, Indexedtable<AsyncLoader *> *asyncLoadersBuffer)
        : resourceLoader(resourceLoader), asyncLoadersBuffer(asyncLoadersBuffer) {}

    virtual ~AsyncLoadersFactory() {}

    ResourceLoader *getResourceLoader() { return resourceLoader; }

    Indexedtable<AsyncLoader *> *getAsyncLoaderBuffer() { return asyncLoadersBuffer; }
};

#endif