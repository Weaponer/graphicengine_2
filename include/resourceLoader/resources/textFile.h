#ifndef TEXT_FILE_H
#define TEXT_FILE_H

#include <string>

#include "object.h"

class TextFile : public Object
{
private:
    std::string nameFile;
    std::string contentFile;

public:
    TextFile(const char *_nameFile, const char *_contentFile);

    virtual ~TextFile();

    const char *getNameFile() const;

    const char *getContentFile() const;
};

#endif