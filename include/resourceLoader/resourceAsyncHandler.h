#ifndef RESOURCE_ASYNC_HANDLER_H
#define RESOURCE_ASYNC_HANDLER_H

#include <string>

#include "objectRegister/object.h"
#include "resourceLoader/typeResources.h"
#include "resourceLoader/asyncLoadersFactory.h"

enum StatusLoad
{
    SL_None,
    SL_Failed,
    SL_Succeed
};

class ResourceLoader;

class ResourceAsyncHandler : public Object
{
private:
    std::string path;
    TypeResource type;
    StatusLoad status;
    Object *object;

    AsyncLoader *loader;

    bool isDiscard;

public:
    ResourceAsyncHandler(AsyncLoader *loader, const char *path, TypeResource type);

    virtual ~ResourceAsyncHandler();

    const char *getPath() const
    {
        return path.c_str();
    }

    TypeResource getType() const
    {
        return type;
    }

    StatusLoad getStatus() const
    {
        return status;
    }

    Object *getResource() const
    {
        return object;
    }

    bool getDiscardState() const
    {
        return isDiscard;
    }

    void discard()
    {
        isDiscard = true;
    }

    friend ResourceLoader;
};

#endif