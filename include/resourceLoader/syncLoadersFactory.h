#ifndef SYNC_LOADERS_FACTORY_H
#define SYNC_LOADERS_FACTORY_H

#include "objectRegister/object.h"
#include "paterns/factoryAbstract.h"
#include "pathToResource.h"


class ResourceLoader;

struct ResultLoad
{
    bool isAllNormal;
    Object *resource;
};

template <uint T>
class GeneratorLoadResource : public GeneratorObject<ResultLoad, const PathToResource *>
{
public:
    GeneratorLoadResource() {}

    bool checkIs(const PathToResource *define) override
    {
        return T == define->type;
    }

    virtual ~GeneratorLoadResource() {}
};

class SyncLoadersFactory : public AbstactFactory<ResultLoad, const PathToResource *>
{
private:
    ResourceLoader *resourceLoader;

    ResultLoad getDefault() override
    {
        ResultLoad l;
        l.isAllNormal = false;
        l.resource = NULL;
        return l;
    }

public:
    SyncLoadersFactory(ResourceLoader *resourceLoader) : resourceLoader(resourceLoader) {}

    ResourceLoader *getResourceLoader() { return resourceLoader; }

    virtual ~SyncLoadersFactory() {}
};

#endif