#ifndef PATH_TO_RESOURCE_H
#define PATH_TO_RESOURCE_H

#include <string>

#include "resourceLoader/typeResources.h"

struct PathToResource
{
    TypeResource type;
    const char *path;
};

struct PathToResourceD
{
    TypeResource type;
    std::string path;
};

#endif