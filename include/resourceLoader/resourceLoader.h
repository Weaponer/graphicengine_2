#ifndef RESOURCE_LOADER_H
#define RESOURCE_LOADER_H

#include <mutex>
#include <thread>
#include <map>

#include "paterns/stack/indexedtable.h"
#include "paterns/stack/easyStack.h"
#include "objectRegister/object.h"

#include "objectRegister/objectRegister.h"

#include "resourceLoader/typeResources.h"
#include "resourceLoader/resourceAsyncHandler.h"

#include "resourceLoader/syncLoadersFactory.h"
#include "resourceLoader/asyncLoadersFactory.h"

#include "engineBase/specialSystems.h"

#define NUMBER_REQURESTS_UPDATED_SYNC_TICK 10
#define NUMBER_REQURESTS_UPDATED_ASYNC_TICK 10

class ResourceLoader
{
private:
    ObjectRegister *objectRegister;

    SyncLoadersFactory syncLoadersFactory;
    AsyncLoadersFactory asyncLoadersFactory;

    EasyStack<AsyncLoader *> requrestsAsyncLoaders;
    Indexedtable<ResourceAsyncHandler *> handlersInQueue;

    std::list<std::pair<std::string, uint>> loadedPaths;

private:    
    std::thread threadAsyncLoader;
    std::mutex mutexAsyncLoader;

    Indexedtable<AsyncLoader *> asyncLoaders;

    bool discardThread;

public:
    ResourceLoader(ObjectRegister *objectRegister, SpecialSystems *_specialSystems);

    ~ResourceLoader();

private:
    void disableThread();

    void enableThread();

public:
    Object *loadResource(const char *path, TypeResource type);

    void unloadResource(Object *object);

public:
    ResourceAsyncHandler *loadAsyncResource(const char *path, TypeResource type);

    void unloadAsyncResource(ResourceAsyncHandler *handler);

    void update();

private:
    void updateAsyncThread();

    void moveRequrests();

    void unloadNoNormalResources(AsyncLoader *asyncLoader);

    int getLoadedResourceWithPath(const char *path);

    void setLoadedResourceInAsyncLoader(AsyncLoader *asyncLoader, Object *resource);

    void registerNewResource(Object *resource, const char *path);

    void removeResource(Object *resource);
};

#endif