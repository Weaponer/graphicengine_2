#ifndef TYPE_RESOURCES_H
#define TYPE_RESOURCES_H

#include <stdlib.h>

enum TypeResource : uint
{
    PACKET = 0,
    SCENE = 1,
    TEST_RESOURCE = 2,
    MODEL_OBJ = 5,
    TEXTURE = 7,
    HDR_TEXTURE = 8,
    PACKED_SHADER = 9,
    SCRIPT = 10,
    LAYER_DATA = 11,
    SHADER = 12,
    MATERIAL = 13,
    CUBE_MAP = 14,
    MODEL_LIGHT_SHADER = 15,
    TEXT_FILE = 16
};

#endif