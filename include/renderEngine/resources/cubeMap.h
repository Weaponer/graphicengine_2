#ifndef CUBE_MAP_H
#define CUBE_MAP_H
#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL2/SDL.h>
#include <array>

#include "objectRegister/object.h"

class CubeMap : public Object
{
private:
    GLuint tex;

public:
    CubeMap(std::array<SDL_Surface *, 6> &_surfaces);

    GLuint getIndexCubeMap() { return tex; }

    virtual ~CubeMap();

};

#endif