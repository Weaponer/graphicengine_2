#ifndef MODEL_H
#define MODEL_H

#include <MTH/vectors.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <vector>
#include <MTH/boundAABB.h>

#include "object.h"
#include "mesh.h"

class Model : public Object
{
private:
    std::vector<Mesh *> meshs;

public:
    Model(std::vector<Mesh *> &meshs);

    virtual ~Model();

    uint countMeshs() { return meshs.size(); }

    Mesh *getMeshAtIndex(uint index);

    Mesh *getMeshAtName(const std::string *name);
};
#endif