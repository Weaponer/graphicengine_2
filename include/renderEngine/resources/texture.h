#ifndef TEXTURE_H
#define TEXTURE_H
#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL2/SDL.h>

#include "objectRegister/object.h"

class Texture : public Object
{
private:
    GLuint tex;

public:
    Texture(int _width, int _height, GLenum _internalFormat, GLenum _format, GLenum _type, void *_data, bool _generateMipMaps);

    GLuint getIndexTexture() { return tex; }

    virtual ~Texture();
};

#endif