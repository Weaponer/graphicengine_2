#ifndef TEMPLATE_TAG_H
#define TEMPLATE_TAG_H

class TemplateTag
{
protected:
    void *refOnTag;

public:
    TemplateTag();

    ~TemplateTag();

    void *getRefOnTag() const;

    void setRefOnTag(void *_refOnTag);

    void clearRefOnTag();
};

#endif