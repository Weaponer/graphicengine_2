#ifndef MESH_H
#define MESH_H

#include <MTH/vectors.h>
#include <MTH/boundAABB.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <vector>

#include "renderEngine/resources/templateTag.h"

struct Vertex
{
    mth::vec3 pos;
    mth::vec2 uv;
    mth::vec3 normal;
    mth::vec3 tangent;
};

class Mesh : public TemplateTag
{
private:
    std::string nameMesh;

    GLuint bufferV;
    GLuint arrayV;
    GLuint arrayE;
    uint countIndexes;

    mth::boundAABB boundAABB;

public:
    Mesh(std::string name, std::vector<GLushort> *indexes, std::vector<mth::vec3> *positions, std::vector<mth::vec2> *uvs,
         std::vector<mth::vec3> *normals, std::vector<mth::vec3> *tangents);

    ~Mesh();

    mth::boundAABB getBoundAABB() { return boundAABB; }

    GLuint getCountIndexes() { return countIndexes; }

    const std::string* getName() { return &nameMesh; }

    void enableBuffers();

    void disableBuffers();
};

#endif