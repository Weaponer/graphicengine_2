#ifndef TECHNICAL_COMPUTE_PIPELINE_H
#define TECHNICAL_COMPUTE_PIPELINE_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include "tshader.h"

class TechnicalComputePipeline
{
private:
    TShader *computeProgram;
    GLuint countXGroups;
    GLuint countYGroups;
    GLuint countZGroups;

    GLuint programPipeline;
    GLuint indexComputeProgram;

public:
    TechnicalComputePipeline();

    ~TechnicalComputePipeline();

    void setPipeline(TShader *_computeProgram, GLuint _countXGroups = 1, GLuint _countYGroups = 1, GLuint _countZGroups = 1);

    TShader *getComputeTShader() const;

    GLuint getComputeProgram() const;

    GLint getLocUniformCompute(const char *_name) const;

    GLint getLocUniformBlockCompute(const char *_name) const;

    void enablePipeline() const;

    void dispatch();

    void dispatch(GLuint _countXGroups, GLuint _countYGroups, GLuint _countZGroups);
};

#endif