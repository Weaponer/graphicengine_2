#ifndef TECHNICAL_PROGRAM_PIPELINE_H
#define TECHNICAL_PROGRAM_PIPELINE_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include "tshader.h"

class TechnicalProgramPipeline
{
private:
    TShader *vertexProgram;
    TShader *fragmentProgram;

    GLuint programPipeline;
    GLuint indexVertexProgram;
    GLuint indexFragmentProgram;

public:
    TechnicalProgramPipeline();

    ~TechnicalProgramPipeline();

    void setPipeline(TShader *_vertexProgram, TShader *_fragmentProgram);

    TShader *getVertexTShader() const;

    TShader *getFragmentTShader() const;

    GLuint getVertexProgram() const;

    GLuint getFragmentProgram() const;

    GLint getLocUniformVertex(const char *_name) const;

    GLint getLocUniformFragment(const char *_name) const;

    GLint getLocUniformBlockVertex(const char *_name) const;

    GLint getLocUniformBlockFragment(const char *_name) const;

    void enablePipeline() const;
};

#endif