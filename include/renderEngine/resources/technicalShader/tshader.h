#ifndef TSHADER_H
#define TSHADER_H
#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <string>

#include "object.h"

class TShader : public Object
{
private:
    std::string name;
    GLenum type;
    GLuint program;

public:
    TShader(const char *_name, GLenum _type, const GLchar *_source);

    virtual ~TShader();

    const char *getName() const { return name.c_str(); }

    GLenum getType() const { return type; }

    GLuint getProgram() const { return program; }
};
#endif