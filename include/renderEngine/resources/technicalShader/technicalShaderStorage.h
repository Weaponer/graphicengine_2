#ifndef TECHNICAL_SHADER_STORAGE_H
#define TECHNICAL_SHADER_STORAGE_H

#include <map>
#include <string>
#include "tshader.h"

class TechnicalShaderStorage
{
private:
    std::map<std::string, TShader *> shaders;

public:
    TechnicalShaderStorage();

    ~TechnicalShaderStorage();

    bool addTechnicalShader(const char *_name, TShader *_shader);

    bool removeTechnicalShader(const char *_name);

    TShader *getTShader(const char *_name) const;
};

#endif