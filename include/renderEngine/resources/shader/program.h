#ifndef PROGRAM_H
#define PROGRAM_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <string>
#include <map>
#include <vector>

#include "properties/property.h"
#include "setInitializedTextures.h"

class Program
{
private:
    struct DefaultGLint
    {
        GLint glint = -1;
    };

    struct DefaultGLenum
    {
        GLenum glenum = GL_NONE; 
    };

private:
    int mask;

    GLuint program;
    bool noneAutoRemove;
    GLenum typeShader;

    std::map<int, DefaultGLint> propertiesInProgram;
    std::map<int, DefaultGLenum> sizeVectorInProgram;
    std::map<int, DefaultGLint> numTextureInProgram;
    std::vector<int> internalProperties;
    std::vector<int> useUniformBlocks;

public:
    Program();

    ~Program();

    void setProgram(const char *_codeShaedr, GLenum _type);

    void setCompiledProgram(GLuint _program, GLenum _type);

    void setProperties(int _count, const Property **_properties, SetInitializedTextures *_setInitializedTextures);

    void setActiveUniformBlock(const char *_nameUniformBlock, GLuint _binding);

    int getCountUseUniformBlocks();

    const int *getUseUniformBlocks();

    void setMask(int _mask);

    int getMask() const;

    GLuint getProgram() const;

    GLint getPropertyInProgram(int _indexProperty);

    GLint getNumberSamplerTexture(int _indexProperty);

    GLenum getTypeVector(int _indexProperty);

    int getCountInternalProperties();

    int getIndexInternalProperty(int _num);
};

#endif