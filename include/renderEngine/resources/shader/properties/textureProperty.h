#ifndef TEXTURE_PROPERY_H
#define TEXTURE_PROPERY_H

#include "renderEngine/resources/texture.h"
#include "property.h"

class TextureProperty : public Property
{
private:
    Texture *value;

public:
    TextureProperty(int _codeData, PropertiesRegistry *_registry);

    virtual ~TextureProperty();

    Texture *getValue() const;

    void setValue(Texture *_value);

    virtual void pastValue(Property *_property) const override;

    virtual Property *copy() const override;
};

#endif