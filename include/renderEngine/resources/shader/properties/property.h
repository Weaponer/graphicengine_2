#ifndef PROPERTY_H
#define PROPERTY_H

#include "shader/enums.h"

class PropertiesRegistry;


class Property
{
protected:
    int codeData;
    PropertiesRegistry *registry;
    TypeProperty type;

public:
    Property(int _codeData, PropertiesRegistry *_registry, TypeProperty _type);

    virtual ~Property();

    int getCodeData() const;

    TypeProperty getType() const;

    const char *getName() const;

    const Property *getGlobalSetting() const;

    bool getUseGlobalSetting() const;

    virtual void pastValue(Property *_property) const = 0;

    virtual Property *copy() const = 0;
};

#endif