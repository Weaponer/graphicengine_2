#ifndef TOGGLE_PROPERY_H
#define TOGGLE_PROPERY_H

#include <string>

#include "property.h"

class ToggleProperty : public Property
{
private:
    bool value;

public:
    ToggleProperty(int _codeData, PropertiesRegistry *_registry);

    virtual ~ToggleProperty();

    bool getValue() const;

    void setValue(bool _value);

    virtual void pastValue(Property *_property) const override;

    virtual Property *copy() const override;
};

#endif