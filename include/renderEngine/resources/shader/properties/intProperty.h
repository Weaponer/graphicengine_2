#ifndef INT_PROPERY_H
#define INT_PROPERY_H

#include "property.h"

class IntProperty : public Property
{
private:
    int value;

public:
    IntProperty(int _codeData, PropertiesRegistry *_registry);

    virtual ~IntProperty();

    int getValue() const;

    void setValue(int _value);

    virtual void pastValue(Property *_property) const override;

    virtual Property *copy() const override;
};

#endif