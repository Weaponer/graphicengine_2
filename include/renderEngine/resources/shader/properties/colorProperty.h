#ifndef COLOR_PROPERY_H
#define COLOR_PROPERY_H

#include <MTH/color.h>

#include "property.h"

class ColorProperty : public Property
{
private:
    mth::col value;

public:
    ColorProperty(int _codeData, PropertiesRegistry *_registry);

    virtual ~ColorProperty();

    mth::col getValue() const;

    void setValue(const mth::col *_value);

    virtual void pastValue(Property *_property) const override;

    virtual Property *copy() const override;
};

#endif