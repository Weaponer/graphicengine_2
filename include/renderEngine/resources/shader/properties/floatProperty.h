#ifndef FLOAT_PROPERY_H
#define FLOAT_PROPERY_H

#include "property.h"

class FloatProperty : public Property
{
private:
    float value;

public:
    FloatProperty(int _codeData, PropertiesRegistry *_registry);

    virtual ~FloatProperty();

    float getValue() const;

    void setValue(float _value);

    virtual void pastValue(Property *_property) const override;

    virtual Property *copy() const override;
};

#endif