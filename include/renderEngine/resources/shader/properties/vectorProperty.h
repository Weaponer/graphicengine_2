#ifndef VECTOR_PROPERY_H
#define VECTOR_PROPERY_H

#include <MTH/vectors.h>

#include "property.h"

class VectorProperty : public Property
{
private:
    mth::vec4 value;

public:
    VectorProperty(int _codeData, PropertiesRegistry *_registry);

    virtual ~VectorProperty();

    mth::vec4 getValue() const;

    void setValue(const mth::vec4 *_value);

    virtual void pastValue(Property *_property) const override;

    virtual Property *copy() const override;
};

#endif