#ifndef CUBE_MAP_PROPERY_H
#define CUBE_MAP_PROPERY_H

#include "renderEngine/resources/cubeMap.h"
#include "property.h"

class CubeMapProperty : public Property
{
private:
    CubeMap *value;

public:
    CubeMapProperty(int _codeData, PropertiesRegistry *_registry);

    virtual ~CubeMapProperty();

    CubeMap *getValue() const;

    void setValue(CubeMap *_value);
    
    virtual void pastValue(Property *_property) const override;

    virtual Property *copy() const override;
};

#endif