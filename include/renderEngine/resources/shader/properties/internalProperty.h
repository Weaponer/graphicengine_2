#ifndef INTERNAL_PROPERY_H
#define INTERNAL_PROPERY_H

#include "property.h"

class InternalProperty : public Property
{
public:
    InternalProperty(int _codeData, PropertiesRegistry *_registry);

    virtual ~InternalProperty();

    virtual void pastValue(Property *_property) const override;

    virtual Property *copy() const override;
};

#endif