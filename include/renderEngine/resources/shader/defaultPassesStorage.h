#ifndef DEFAULT_PASSES_STORAGE_H
#define DEFAULT_PASSES_STORAGE_H

#include "pass.h"
#include "enums.h"

#include "resources/technicalShader/technicalShaderStorage.h"
#include "resources/technicalShader/tshader.h"

class DefaultPassesStorage
{
private:
    std::vector<std::pair<Pass *, char>> defaultPasses;

public:
    DefaultPassesStorage(TechnicalShaderStorage *_technicalShaderStorage);

    ~DefaultPassesStorage();

    Pass *getDefaultPass(TypePass _type, bool _useClipping, bool _useNormalMap);

private:
    char getPassData(bool _useClipping, bool _useNormalMap, bool _noneHaveNormalMap);

    bool checkCorrectData(bool _useClipping, bool _useNormalMap, char _dataForCheck);

    void pushVertexProgramToPass(TechnicalShaderStorage *_technicalShaderStorage, Pass *_pass);

    void pushFragmentProgramToPass(TechnicalShaderStorage *_technicalShaderStorage, const char *_nameFragmentProgram, Pass *_pass);

    void pushPassesWithoutNormals(TechnicalShaderStorage *_technicalShaderStorage, TypePass _typePass, CullType _cullType, const char *_name);

    void pushPassesWithoutNormalMaps(TechnicalShaderStorage *_technicalShaderStorage, TypePass _typePass, CullType _cullType, const char *_name);

    void pushPassesWithNormalMaps(TechnicalShaderStorage *_technicalShaderStorage, TypePass _typePass, CullType _cullType, const char *_name);

    void addDefaultPassShadow(TechnicalShaderStorage *_technicalShaderStorage);

    void addDefaultPassDepth(TechnicalShaderStorage *_technicalShaderStorage);

    void addDefaultPassNormal(TechnicalShaderStorage *_technicalShaderStorage);

    void addDefaultPassDepthNormal(TechnicalShaderStorage *_technicalShaderStorage);
};

#endif