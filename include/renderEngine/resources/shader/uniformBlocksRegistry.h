#ifndef UNIFORM_BLOCKS_REGISTRY_H
#define UNIFORM_BLOCKS_REGISTRY_H

#include <map>
#include <string>

class UniformBlocksRegistry
{
private:
    std::map<std::string, int> indexesByName;

public:
    UniformBlocksRegistry();

    ~UniformBlocksRegistry();

    void addNewReserveUniformBLock(const char *_name);

    int getIndexReserveUniformBlock(const char *_name) const;
};

#endif