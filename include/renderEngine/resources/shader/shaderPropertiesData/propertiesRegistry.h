#ifndef PROPERTIES_REGISTRY_H
#define PROPERTIES_REGISTRY_H

#include <map>
#include <string>
#include <MTH/vectors.h>

#include "paterns/stack/indexedtable.h"

#include "propertyData.h"
#include "shader/enums.h"

#include "renderEngine/resources/texture.h"

#include "properties/internalProperty.h"
#include "properties/intProperty.h"
#include "properties/floatProperty.h"
#include "properties/toggleProperty.h"
#include "properties/vectorProperty.h"
#include "properties/colorProperty.h"
#include "properties/textureProperty.h"
#include "properties/cubeMapProperty.h"


typedef std::pair<std::string, TypeProperty> DefineProperty;

class PropertiesRegistry
{
private:
    Indexedtable<PropertyData *> defineProperties;

    std::map<DefineProperty, int> indexPropertiesByNames;
    
public:
    PropertiesRegistry();

    ~PropertiesRegistry();

    void clearProperties();

    bool checkPropertyExist(const char *_name, TypeProperty _type);

    void addNewProperties(const char *_name, TypeProperty _type);

    void setUseGlobalProperty(int _codeProperty, bool _use);

    bool getUseGlobalProperty(int _codeProperty);

    void setGlobalPropertyValue(int _codeProperty, const Property *_property);

    void setGlobalPropertyValue(const char *_name, TypeProperty _type, const Property *_property);

    int getPropertyCodeRegistry(const char *_name, TypeProperty _type);

    const char *getNameProperty(int _codeProperty);

    const Property *getGlobalSettingProperty(int _codeProperty);

    const Property *getGlobalSettingProperty(const char *_name, TypeProperty _type);

public:
    Property *createDefaultProperty(const char *_name, TypeProperty _type);

    InternalProperty *createInternalProperty(const char *_name);

    IntProperty *createIntProperty(const char *_name, int _val = 0);

    FloatProperty *createFloatProperty(const char *_name, float _val = 0);

    ToggleProperty *createToggleProperty(const char *_name, bool _val = false);

    VectorProperty *createVectorProperty(const char *_name, mth::vec4 _val = mth::vec4(0, 0, 0, 0));

    ColorProperty *createColorProperty(const char *_name, mth::col _val = mth::col(0, 0, 0, 1));

    TextureProperty *createTextureProperty(const char *_name, Texture *_val = NULL);

    CubeMapProperty *createCubeMapProperty(const char *_name, CubeMap *_val = NULL);
};

#endif