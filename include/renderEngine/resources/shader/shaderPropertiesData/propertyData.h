#ifndef PROPERTY_DATA_H
#define PROPERTY_DATA_H

#include <string>

#include "shader/enums.h"
#include "shader/properties/property.h"

class PropertyData
{
private:
    std::string name;
    bool useGlobalData;
    Property *exampleProperty;

public:
    PropertyData();

    ~PropertyData();

    void setName(const char *_name);

    const char *getName() const;

    void setExampleProperty(Property *_exampleProperty);

    void setUseGlobalData(bool _enable);

    bool getUseGlobalData() const;

    Property *getPropertyForExample() const;

};

#endif