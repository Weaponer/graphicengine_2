#ifndef SHADERS_STORAGE_H
#define SHADERS_STORAGE_H

#include <map>
#include <string>

#include "renderEngine/resources/technicalShader/technicalShaderStorage.h"

#include "paterns/stack/indexedtable.h"
#include "renderEngine/resources/shader/shader.h"
#include "shader/shaderPropertiesData/propertiesRegistry.h"
#include "defaultPassesStorage.h"
#include "uniformBlocksRegistry.h"

class ShadersStorage
{
private:
    std::map<std::string, Shader *> shadersByName;

    PropertiesRegistry propertiesRegistry;
    DefaultPassesStorage defaultPassesStorage;
    UniformBlocksRegistry uniformBlocksRegistry;

public:
    ShadersStorage(TechnicalShaderStorage *_technicalShaderStorage);

    ~ShadersStorage();

    void addNewShader(Shader *_shader, int _countProperties, const std::vector<std::string *> *_propertiesNames, const std::vector<TypeProperty> *_propertiesTypes);

    void removeShader(Shader *_shader);

    Shader *findShader(const char *_name);

    PropertiesRegistry *getPropertiesRegistry();

    DefaultPassesStorage *getDefaultPassesStorage();

    UniformBlocksRegistry *getUniformBlocksRegistry();

private:
    void correctNameShader(Shader *_shader);
};

#endif