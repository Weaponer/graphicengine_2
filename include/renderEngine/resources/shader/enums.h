#ifndef SHADER_AB_ENUMS_H
#define SHADER_AB_ENUMS_H

#define COUNT_TYPE_PASSES 6
#define DEPTH_PASS_INDEX 0
#define NORMAL_PASS_INDEX 1
#define DEPTH_NORMAL_PASS_INDEX 2
#define BASE_COLOR_PASS_INDEX 3
#define SHADOW_PASS_INDEX 4
#define SURFACE_PASS_INDEX 5

enum TypeClippingChanel
{
    Type_Clipping_Chanel_R = 0,
    Type_Clipping_Chanel_G = 1,
    Type_Clipping_Chanel_B = 2,
    Type_Clipping_Chanel_A = 3,
};

enum TypePass : int
{
    Depth = 1,
    Normal = 2,
    DepthNormal = 4,
    BaseColor = 8,
    Shadow = 16,
    Surface = 32,
};

enum QueueRender : int
{
    QueueRender_Geometry = 1,
    QueueRender_AlphaTest = 2,
    QueueRender_Transparent = 4,
    QueueRender_Overlay = 8,
};

enum RenderType : int
{
    RenderType_Opaque = 1,
    RenderType_TransparentCutout = 2,
    RenderType_Transparent = 4,
    RenderType_Terrain = 8,
    RenderType_TerrainTree = 16,
    RenderType_Water = 32,
};

enum CullType : int
{
    CullType_Back = 0,
    CullType_Front = 1,
    CullType_Off = 2,
};

#define COUNT_PROPERTIES 8
#define OFFSET_NUMBER_PROPERTIES ((-1))

enum TypeProperty : int
{
    Internal_P = -1,
    Texture_P = 0,
    Int_P = 1,
    Float_P = 2,
    Toggle_P = 3,
    CubeMap_P = 4,
    Vector_P = 5,
    Color_P = 6,
};

#endif