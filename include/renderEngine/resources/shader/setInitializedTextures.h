#ifndef SET_INITIALIZED_TEXTURES_H
#define SET_INITIALIZED_TEXTURES_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <map>

class SetInitializedTextures
{
private:
    std::map<int, int> initializedTextures;

    bool errorTry;

public:
    SetInitializedTextures();
    
    ~SetInitializedTextures();

    bool wasErrorTry() const;

    int tryTakeTexturePosition(int _indexProperty);
};

#endif