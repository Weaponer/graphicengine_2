#ifndef PASS_H
#define PASS_H

#include <string>
#include <vector>

#include "enums.h"
#include "program.h"
#include "renderEngine/resources/templateTag.h"

#define MAX_COUNT_PROGRAMS 256

#define MODEL_LIGHT_PASS(P) (P->getAdditionalData()[1])
#define USE_ADDITIONAL_LAYERS_PASS(P) (P->getAdditionalData()[0])
#define USE_FIRST_ADD_LAYER_G_BUFFER(P) (((P->getAdditionalData()[0] >> 0) & 1) != 0)
#define USE_SECOND_ADD_LAYER_G_BUFFER(P) (((P->getAdditionalData()[0] >> 1) & 1) != 0)

class Pass : public TemplateTag
{
private:
    std::string name;

    TypePass typePass;
    QueueRender queue;
    RenderType renderType;
    CullType cullType;
    bool zTest;
    bool zWrite;
    bool generateInstancing;

    bool isDefaultPass;
    char settingsDefaultPass;

    int sizeAdditionalData;
    char *additionalData;

    std::vector<Program *> vertexPrograms;
    std::vector<Program *> fragmentPrograms;

    int maskUsingDefinesVertex;
    int maskUsingDefinesFragment;

    std::map<short, GLuint> renderPipelines;
    short oldCodeForPipeline;
    GLuint oldRenderPipeline;

public:
    Pass(const char *_name, TypePass _typePass, QueueRender _queue, RenderType _renderType, CullType _cullType,
         bool _zTest, bool _zWrite, bool _generateInstancing);

    ~Pass();

    const char *getName() const;

    TypePass getTypePass() const;

    QueueRender getQueueRender() const;

    RenderType getRenderType() const;

    CullType getCullType() const;

    bool zTestOn() const;

    bool zWriteOn() const;

    bool generateInstancingOn() const;

    void setMasks(int _maskUsingDefinesVertex, int _maskUsingDefinesFragment);

    void clearPrograms();

    void connectProgram(Program *_program, GLenum _type);

    void getIndexesPrograms(int _mask, int *_indexVertexProgram, int *_indexFragmentProgram);

    GLuint getProgramPipeline(int _indexVertexProgram, int _indexFragmentProgram);

    Program *getVertexProgram(int _indexVertexProgram);

    Program *getFragmentProgram(int _indexFragmentProgram);

    void setPassIsDefault(char _data);

    bool checkIsDefaultPass();

    char getDataDefaultPassSettings();

    void initAdditionalData(int _size, char *_data);

    const char *getAdditionalData() const;
};

#endif