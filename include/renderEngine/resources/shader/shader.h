#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <vector>

#include "enums.h"
#include "properties/property.h"
#include "properties/toggleProperty.h"
#include "pass.h"

#include "renderEngine/resources/templateTag.h"
#include "object.h"

#define MAX_COUNT_PASSES 64

class ShadersStorage;

struct ShaderSettings
{
    int normalMapIndex;
    int clippingTextureIndex;
    TypeClippingChanel typeClippingChanel;
    float clippingValue;
    bool inverseClipping;
};

class Shader : public TemplateTag, public Object
{
private:
    std::string name;
    ShadersStorage *shadersStorage;
    QueueRender maxQueue;
    ShaderSettings settings;
    GLuint uboSettings;

    std::vector<Property *> defaultProperties;
    std::map<int, int> masksToggleProperties;

    std::vector<Pass *> baseColorPasses;
    std::vector<Pass *> depthPasses;
    std::vector<Pass *> depthNormalPasses;
    std::vector<Pass *> normalPasses;
    std::vector<Pass *> shadowPasses;
    std::vector<Pass *> surfacePasses;

public:
    Shader(const char *_name);

    ~Shader();

    void setSettings(const ShaderSettings &_settings);

    GLuint getUBOClippingSettings();

    ShaderSettings getSettings() const;

    void setShadersStorage(ShadersStorage *_shadersStorage);

    ShadersStorage *getShadersStorage() const;

    const char *getName() const;

    void setName(const char *_correctName);

    QueueRender getQueue() const;

    void setDefaultProperties(int _count, Property **_properties);

    int getCountProperties() const;

    Property *getProperty(int _num) const;

    bool checkHaveProperty(int _code) const;

    int getMaskToggle(int _indexToggle) const;

    void setMaskToggle(int _indexToggle, int _mask);

    void addPass(Pass *_pass);

    int getCountPasses(TypePass _typePass) const;

    Pass *getPass(TypePass _typePass, int _num) const;

private:
    void clearDefaultProperties();

    void clearPasses();

    void clearSpecTypePasses(std::vector<Pass *> *_passes);
};

#endif