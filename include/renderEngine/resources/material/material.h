#ifndef MATEIRAL_H
#define MATEIRAL_H

#include <string>
#include <vector>
#include <MTH/vectors.h>
#include <MTH/color.h>

#include "object.h"

#include "renderEngine/resources/shader/shader.h"
#include "renderEngine/resources/cubeMap.h"
#include "renderEngine/resources/texture.h"
#include "renderEngine/resources/shader/properties/property.h"
#include "renderEngine/resources/shader/properties/textureProperty.h"

#include "renderEngine/resources/templateTag.h"

class Material : public TemplateTag, public Object
{
private:
    std::string name;
    Shader *shader;
    bool generateInstancing;

    std::vector<Property *> properties;
    int maskDefineProperties;

    TextureProperty *normalMapProperty;
    TextureProperty *clippingTextureProperty;

public:
    Material(Shader *_shader);

    ~Material();

    void setName(const char *_name);

    const char *getName() const;

    Shader *getShader() const;

    int getCount() const;

    Property *getProperty(int _index) const;

    void setInt(const char *_name, int value);

    void setFloat(const char *_name, float value);

    void setToggle(const char *_name, bool value);

    void setVector(const char *_name, const mth::vec4 *value);

    void setColor(const char *_name, const mth::col *value);

    void setTexture(const char *_name, Texture *value);

    void setCubeMap(const char *_name, CubeMap *value);

    int getInt(const char *_name);

    float getFloat(const char *_name);

    bool getToggle(const char *_name);

    mth::vec4 getVector(const char *_name);

    mth::col getColor(const char *_name);

    Texture *getTexture(const char *_name);

    CubeMap *getCubeMap(const char *_name);

    TypeProperty getTypeProperty(const char *_name);

    int getMaskDefineProperties() const;

    Texture *getSpecialNormalMap();

    Texture *getSpecialClippingTexture();

    const Property *getCorrectProperty(const Property *_localProperty);

    bool getGenerateInstancing() const;

    void setGenerateInstancing(bool _enable);

private:
    Property *getPropertyByName(const char *_name, TypeProperty _typeProperty);

    void updateMaskDefineProperties();
};

#endif