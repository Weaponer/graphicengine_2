#ifndef MODEL_LIGHT_SHADER_STORAGE_H
#define MODEL_LIGHT_SHADER_STORAGE_H

#include <vector>
#include "modelLightShader.h"

#define MAX_COUNT_MODEL_LIGHT 16
#define OFFSET_INDEX_MODEL_LIGHT 1

class ModelLightShaderStorage
{
private:
    std::vector<ModelLightShader *> modelLightsShaders;

public:
    ModelLightShaderStorage();

    ~ModelLightShaderStorage();

    void addModelLight(ModelLightShader *_modelLightShader);

    int getIndexModelLight(const char *_name) const;

    ModelLightShader *getModelLightShader(int _index);
};

#endif