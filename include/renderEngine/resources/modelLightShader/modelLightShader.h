#ifndef MODEL_LIGHT_SHADER_H
#define MODEL_LIGHT_SHADER_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <string>

#include "object.h"

#define ML_POINT_AND_LIGHT_DIRECTION_FORCE_LOC 0
#define ML_LIGHT_POINT_DIRECTION_LOC 1
#define ML_DEPTH_BUFFER_LOC 2
//normal buffer
#define ML_LAYER_1_LOC 3
//color, material index
#define ML_LAYER_2_LOC 4 
//metallic, roughness
#define ML_LAYER_3_LOC 5
#define ML_ADDITIONAL_LAYER_1_LOC 6
#define ML_ADDITIONAL_LAYER_2_LOC 7
#define ML_MODEL_LIGHT_LOC 8

class ModelLightShader : public Object
{
private:
    std::string name;

    GLuint pipeline;
    GLuint program;
    GLuint vertexProgram;

    int countUseLayers;
    int indexModelLight;

public:
    ModelLightShader(const char *_name, const char *_program, const char *_vertexProgram, int _countUseLayers);

    virtual ~ModelLightShader();

    void setIndexModelLight(int _index);

    int getIndexModelLight() const;

    const char *getName() const;

    int getCountUseLayers() const;

    GLuint getProgram();

    GLuint getPipeline();

};

#endif