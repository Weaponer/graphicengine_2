#ifndef RENDER_LIGHT_G_BUFFER_H
#define RENDER_LIGHT_G_BUFFER_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include "renderEngine/mainFrameBuffers/GBuffer.h"
#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"

#include "renderEngine/resources/modelLightShader/modelLightShader.h"
#include "renderEngine/resources/modelLightShader/modelLightShaderStorage.h"

#include "renderEngine/graphicsPipeline/GBufferRender.h"
#include "renderEngine/graphicsPipeline/datas/lightRenderData.h"

#include "renderEngine/settings/resolutionSettings.h"
#include "renderEngine/mainSystems/mainFunctions/cameraMatricesData.h"

class RenderLightGBuffer
{
private:
    ResolutionSettings resolution;

    GBuffer *gBuffer;
    GBufferRender *gBufferRender;
    ModelLightShaderStorage *modelLightShaderStorage;
    CameraVertexesData *cameraVertexesData;
    const LightRenderData *lightRenderData;

    ColorFrameBuffer *resultHDRBuffer;

public:
    RenderLightGBuffer(GBuffer *_gBuffer, GBufferRender *_gBufferRender, ModelLightShaderStorage *_modelLightShaderStorage,
                       CameraVertexesData *_cameraVertexesData, const LightRenderData *_lightRenderData, ColorFrameBuffer *_resultHDRBuffer);

    ~RenderLightGBuffer();

    void update();

private:
    void enableDataForPrograms(int _useAdditionalLayers);

    void disableDataForPrograms(int _useAdditionalLayers);
};

#endif