#ifndef COMBINER_RESULT_LIGHT_BUFFER_H
#define COMBINER_RESULT_LIGHT_BUFFER_H

#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"

#include "renderEngine/resources/technicalShader/tshader.h"
#include "renderEngine/resources/technicalShader/technicalShaderStorage.h"
#include "renderEngine/resources/technicalShader/technicalProgramPipeline.h"
#include "renderEngine/mainSystems/mainFunctions/cameraVertexesData.h"

#include "renderEngine/settings/settingsGraphicPipeline.h"

#include "renderEngine/graphicsPipeline/postEffects/postEffects.h"
#include "renderEngine/graphicsPipeline/postEffects/ssaoBufferRender.h"
#include "renderEngine/graphicsPipeline/lightRender/volumetricFogRender.h"

#define MAX_SAMPLES_BLOOM_EFFECT 12

class CombinerResultLightBuffer
{
private:
    ResolutionSettings defaultResolution;
    SettingsGraphicPipeline *settingsGraphicPipeline;
    CameraVertexesData *CVD;
    ColorFrameBuffer *resultFrameBuffer;

    VolumetricFogRender *volumetricFogRender;
    PostEffects *postEffects;

private:
    TechnicalProgramPipeline combiner;

    ColorFrameBuffer *skyBuffer;
    ColorFrameBuffer *basicColorBuffer;
    SSAOBufferRender *ssaoBuffer;
    ColorFrameBuffer *volumetricFogBuffer;

    struct CombinerData
    {
        bool enableSSAO;
        bool _m1[3];
        bool enableFog;
        bool _m2[3];
        bool enableBloom;
        bool _m3[3];
    } combinerData;

    GLuint combinerDataUBO;

private:
    TechnicalProgramPipeline bloomGenerator;

    GLuint framebuffersBloom[MAX_SAMPLES_BLOOM_EFFECT];
    GLuint texturesBloom[MAX_SAMPLES_BLOOM_EFFECT];
    mth::Vector2Int sizes[MAX_SAMPLES_BLOOM_EFFECT];

    GLuint framebufferFull;
    GLuint textureFull;
    mth::Vector2Int oldSize;

private:
    TechnicalProgramPipeline bloomCombiner;

    struct BloomCombinerData
    {
        int countSubMaps;
        float forceBloom;
    } bloomCombinerData;
    GLuint bloomCombinerDataUBO;

public:
    CombinerResultLightBuffer(ResolutionSettings _resolution, SettingsGraphicPipeline *_settingsGraphicPipeline, CameraVertexesData *_CVD,
                              ColorFrameBuffer *_resultLightBuffer, TechnicalShaderStorage *_technicalShaderStorage);

    ~CombinerResultLightBuffer();

    void setOtherFrameBuffers(ColorFrameBuffer *_skyBuffer,
                              ColorFrameBuffer *_basicColorBuffer,
                              SSAOBufferRender *_ssaoBuffer,
                              ColorFrameBuffer *_volumetricFogBuffer);

    void update();

private:
    void updateUniformBuffer();

private:
    void generateTextures();
};

#endif