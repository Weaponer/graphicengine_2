#ifndef LIGHT_FIRST_STAGE_BUFFER_RENDER_H
#define LIGHT_FIRST_STAGE_BUFFER_RENDER_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include "renderEngine/resources/technicalShader/technicalProgramPipeline.h"
#include "renderEngine/resources/technicalShader/technicalShaderStorage.h"
#include "renderEngine/resources/technicalShader/tshader.h"

#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"
#include "renderEngine/mainFrameBuffers/lightBasicFrameBuffer.h"
#include "renderEngine/graphicsPipeline/lightRender/renderShadowDirecitonLight.h"

#include "renderEngine/settings/specialDefines.h"

struct PointLightData
{
    mth::vec3 color;
    float intensivity;
    mth::vec3 position;
    float range;
};

struct PointLightShadow
{
    mth::mat4 matrix_V[6];
    mth::mat4 matrixInverse_V[6];
    mth::mat4 matrix_P;
    mth::mat4 matrixInverse_P;
    mth::vec3 color;
    float intensivity;
    mth::vec3 position;
    float range;
    float near;
    float far;
};

class LightFirstStageBufferRender
{
private:
    MainFunctions *mainFunctions;
    RenderShadowDirectionLight *renderShadowDirectionLight;
    LightBasicFrameBuffer *lightBasicFrameBuffer;

    ColorFrameBuffer *normalBuffer;
    MainDepthBuffer *depthBuffer;

private:
    TechnicalProgramPipeline pointLightPassProgram;
    TechnicalProgramPipeline pointLightShadowPassProgram;
    TechnicalProgramPipeline mainLightCombinerProgram;

    struct PointsLightsData
    {
        PointLightData pointsLights[MAX_COUNT_POINT_LIGHTS];
        int countPointLights;
    } allLightsPointsData;
    GLuint allLightsPointsDataUBO;

    PointsLightsData lightsPointsWithoutShadowData;
    GLuint lightsPointsWithoutShadowDataUBO;

    struct PointsLightsShadowsData
    {
        PointLightShadow pointsLightsShadows[MAX_COUNT_POINT_LIGHTS];
    } pointsLightsShadowsData;
    int countPointsLightsWithShadow;
    GLuint shadowMaps[MAX_COUNT_POINT_LIGHTS];

    GLuint pointsLightsShadowsDatasUBO[MAX_COUNT_POINT_LIGHTS];

public:
    LightFirstStageBufferRender(MainFunctions *_mainFunctions, TechnicalShaderStorage *_TSS, RenderShadowDirectionLight *_renderShadowDirectionLight,
                                LightBasicFrameBuffer *_lightBasicFrameBuffer, ColorFrameBuffer *_normalBuffer, MainDepthBuffer *_depthBuffer);

    ~LightFirstStageBufferRender();

    void update();

    GLuint getUBOAllLightsPoints();

private:
    void fillDatas();
};

#endif