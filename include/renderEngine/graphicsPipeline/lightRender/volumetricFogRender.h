#ifndef VOLUMETRIC_FOG_RNDER_H
#define VOLUMETRIC_FOG_RNDER_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <array>
#include <MTH/vectors.h>

#include "renderEngine/resources/technicalShader/tshader.h"
#include "renderEngine/resources/technicalShader/technicalShaderStorage.h"
#include "renderEngine/resources/technicalShader/technicalProgramPipeline.h"
#include "renderEngine/resources/technicalShader/technicalComputePipeline.h"

#include "renderEngine/mainFrameBuffers/mainDepthBuffer.h"
#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"

#include "renderEngine/graphicsPipeline/lightRender/lightFirstStageBufferRender.h"

#include "renderEngine/graphicsPipeline/datas/volumetricFogData.h"
#include "renderEngine/mainSystems/mainFunctions/cameraVertexesData.h"
#include "renderEngine/settings/settingsGraphicPipeline.h"

#include "renderEngine/graphicsPipeline/transparentBufferRender.h"

#define SIZE_LOCAL_GROUP_DENSITY_VOLUME 4

#define INDEX_DENSITY_VOLUME_UBO 1
#define INDEX_COMPUTE_SHADER_DATA_UBO 2

class VolumetricFogRender
{
private:
    MainSystems *mainSystems;
    SettingsGraphicPipeline *settingsGraphicPipeline;
    LightFirstStageBufferRender *lightFirstStageRender;
    RenderShadowDirectionLight *renderShadowDirectionLight;

    MainDepthBuffer *depthBuffer;
    ColorFrameBuffer *resultBuffer;

    GLuint framebufferSupporting;
    GLuint textureSupporting;

    GLuint noiseTexture;
    bool needGenerateNoise;

    GLuint textureFogDensityGrid;
    GLuint textureFogLightGrid;

    std::array<int, 3> currentSizeGrid;

    struct FogData
    {
        mth::vec3 colorFog;
        float nearPlaneFog;
        mth::vec3 directionSun;
        float farPlaneFog;
        mth::vec4 colorSun;
        mth::vec4 environmentLight;

        float farPlaneRays;

        float anisotropic;
        float density;
        float scattering;
        float absorption;
    } fogData;

    GLuint fogDataUBO;

    struct ComputShaderData
    {
        int startID_DensityVolume[3];
    } computShaderData;

    GLuint computShaderDataUBO;

private:
    TechnicalComputePipeline noiseGenerator;
    GLuint locationRandomSeed;

    TechnicalComputePipeline fogFillerDensityProgram;
    TechnicalComputePipeline fogDensityVolumeFillerProgram;
    TechnicalComputePipeline fogBasicLightProgram;
    TechnicalComputePipeline fogShadowDirectionLight;
    GLuint locationIndexShadow;

    TechnicalComputePipeline fogPointLightsProgram;

    TechnicalComputePipeline fogRayMarchSumatorProgram;

    TechnicalProgramPipeline fogCombinerProgram;

public:
    VolumetricFogRender(MainSystems *_mainSystems, SettingsGraphicPipeline *_settingsGraphicPipeline, LightFirstStageBufferRender *_lightFirstStageRender,
                        RenderShadowDirectionLight *_renderShadowDirectionLight, MainDepthBuffer *_depthBuffer, ColorFrameBuffer *_resultBuffer);

    ~VolumetricFogRender();

    void update();

    void combineWithTransparent(TransparentBufferRender *_transparentBufferRender);

private:
    std::array<mth::vec3, 2> boundAABBToBoundScreen(mth::boundAABB _aabb, CameraVertexesData *_CVD) const;

    float getGridZFromDepth(float _depthProject, CameraVertexesData *_CVD) const;

    void updateGridSizeForProgram(GLuint _program, int _x, int _y, int _z);

    void updateGridSizeForAllPrograms();

    void checkAndUpdateFogData();

    void checkAndUpdateSizeGrid();

    void clearTextures();

    void createTextures();
};

#endif