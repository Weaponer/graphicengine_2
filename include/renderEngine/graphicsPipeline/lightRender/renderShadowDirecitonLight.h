#ifndef RENDER_SHADOW_DIRECTION_LIGHT_H
#define RENDER_SHADOW_DIRECTION_LIGHT_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <MTH/matrix.h>
#include <MTH/vectors.h>

#include "renderEngine/resources/technicalShader/tshader.h"

#include "renderEngine/mainFrameBuffers/effectBuffers.h"
#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"
#include "renderEngine/mainFrameBuffers/lightBasicFrameBuffer.h"
#include "shadow/shadowDirectionLight.h"

class RenderShadowDirectionLight
{
private:
    ResolutionSettings defaultResolution;
    MainFunctions *mainFunctions;
    SettingsGraphicPipeline *settingsGraphicPipeline;

    MainDepthBuffer *depthBuffer;
    LightBasicFrameBuffer *lightBasicFrameBuffer;
    ShadowDirectionLight *shadowDirectionLight;

private:
    TechnicalProgramPipeline directionLightShadowProgram;

    struct DirectionLightShadowData
    {
        mth::mat4 _shadowMatrix_VP[MAX_SHADOWS_ON_DIRECTION_LIGHTS];
        mth::vec4 _shadowClipDistancesBias[MAX_SHADOWS_ON_DIRECTION_LIGHTS];
        mth::vec3 directionBoundAlongCamera;
        int countPartition;
        float startFadeDistance;
        float endFadeDistance;
    } directionLightShadowData;

    GLuint directionLightShadowDataUBO;

    GLint locationFragIndexPartion;

private:
    GLuint frameBuffer;
    GLuint textureBuffer;

public:
    RenderShadowDirectionLight(MainFunctions *_mainFunctions, TechnicalShaderStorage *_TSS, SettingsGraphicPipeline *_settingsGraphicPipeline,
                               ShadowDirectionLight *_shadowDirectionLight, LightBasicFrameBuffer *_lightBasicFrameBuffer, MainDepthBuffer *_depthBuffer);

    ~RenderShadowDirectionLight();

    GLuint getTextureBuffer();

    void update();

    GLuint getUBODirectionLightShadows();

    int countDirectionLightShadows();

    std::array<GLuint, MAX_SHADOWS_ON_DIRECTION_LIGHTS> getTexturesDirectionLightShadows();

private:
    void fillDatas();
};

#endif