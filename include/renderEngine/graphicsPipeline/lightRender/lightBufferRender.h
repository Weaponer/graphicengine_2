#ifndef LIGHT_BUFFER_RENDER_H
#define LIGHT_BUFFER_RENDER_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include "renderEngine/resources/technicalShader/tshader.h"
#include "renderEngine/resources/technicalShader/technicalProgramPipeline.h"

#include "renderEngine/mainSystems/mainSystems.h"
#include "renderEngine/mainSystems/mainFunctions/mainFunctions.h"
#include "renderEngine/settings/settingsGraphicPipeline.h"

#include "renderEngine/mainFrameBuffers/effectBuffers.h"
#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"
#include "renderEngine/mainFrameBuffers/lightBasicFrameBuffer.h"

#include "shadow/shadowGenerator.h"
#include "shadow/shadowDirectionLight.h"
#include "shadow/shadowsPointLights.h"
#include "lightFirstStageBufferRender.h"
#include "renderShadowDirecitonLight.h"
#include "volumetricFogRender.h"
#include "combinerResultLightBuffer.h"
#include "renderEngine/graphicsPipeline/transparentBufferRender.h"

class LightBufferRender
{
private:
    ShadowGenerator shadowGenerator;
    ShadowDirectionLight shadowDirectionLight;
    ShadowsPointLights shadowsPointLights;

    RenderShadowDirectionLight renderShadowDirectionLight;
    LightFirstStageBufferRender lightFirstStageBufferRender;
    VolumetricFogRender volumetricForRender;
    CombinerResultLightBuffer combinerResultLightBuffer;

    ColorFrameBuffer supportBuffer;
    
    TechnicalProgramPipeline combinerWithTransparentProgram;

    MainSystems *mainSystems;
    MainFunctions *mainFunctions;
    SettingsGraphicPipeline *settingsGraphicPipeline;

    TransparentBufferRender *transparentBufferRender;

    ColorFrameBuffer *normalBuffer;
    MainDepthBuffer *depthBuffer;
    LightBasicFrameBuffer *lightBasicFrameBuffer;
    ColorFrameBuffer *resultLightBuffer;

public:
    LightBufferRender(MainSystems *_mainSystems, SettingsGraphicPipeline *_settingsGraphicPipeline, TransparentBufferRender *_transparentBufferRender,
                      ColorFrameBuffer *_resultLightBuffer, LightBasicFrameBuffer *_lightBasicFrameBuffer,
                      MainDepthBuffer *_depthBuffer, ColorFrameBuffer *_normalBuffer);

    ~LightBufferRender();

    void update();

    void updateFog();

    void combineWithTransparentAndFog();

    ShadowGenerator *getShadowGenerator();

    ShadowDirectionLight *getShadowDirectionLight();

    ShadowsPointLights *getShadowsPointLights();

    LightFirstStageBufferRender *getLightFirstStageBufferRender();

    RenderShadowDirectionLight *getRenderShadowDirectionLight();

    VolumetricFogRender *getVolumetricFogRender();

    CombinerResultLightBuffer *getCombinerResultLightBuffer();
};

#endif