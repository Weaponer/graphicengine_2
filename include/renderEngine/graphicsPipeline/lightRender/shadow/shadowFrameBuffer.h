#ifndef SHADOW_FRAME_BUFFER_H
#define SHADOW_FRAME_BUFFER_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <array>

#include <MTH/matrix.h>
#include <MTH/vectors.h>
#include <MTH/boundOBB.h>

#include "renderEngine/mainSystems/mainFunctions/cameraVertexesData.h"
#include "renderEngine/mainSystems/mainFunctions/cameraMatricesData.h"

#define MAX_COUNT_VARIANTS_ASPECTS_SHADOW 4


class ShadowFrameBuffer
{
private:
    std::array<GLuint, MAX_COUNT_VARIANTS_ASPECTS_SHADOW> framebuffers;
    std::array<GLuint, MAX_COUNT_VARIANTS_ASPECTS_SHADOW> textures;
    std::array<mth::Vector2Int, MAX_COUNT_VARIANTS_ASPECTS_SHADOW> texturesSizes;
    std::array<float, MAX_COUNT_VARIANTS_ASPECTS_SHADOW> aspectTextures;

    mth::boundOBB boundOBB;
    CameraMatricesDataUBO matricesDataUBO;

    int currentUseIndex;

public:
    ShadowFrameBuffer(int _widht, int _height);

    ~ShadowFrameBuffer();

    void setBoundOBB(const mth::boundOBB &_boundOBB);

    mth::boundOBB getBoundOBB() const;

    CameraMatricesDataUBO *getCameraMatricesData();

    void updateDataCameraMatrices();

    mth::Vector2Int getSize() const;

    mth::Vector2Int getStandartSize() const;

    GLuint getFrameBuffer() const;

    GLuint getShadowTexture() const;

    void setUseAspect(float _aspect);

    void clear();
};

#endif