#ifndef SHADOW_CUBE_FRAME_BUFFER_H
#define SHADOW_CUBE_FRAME_BUFFER_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <array>
#include <MTH/matrix.h>
#include <MTH/vectors.h>
#include <MTH/plane.h>
#include <MTH/boundOBB.h>

#include "renderEngine/mainSystems/mainFunctions/cameraMatricesData.h"

class ShadowCubeFrameBuffer
{
private:
    float sizeSide;

    GLuint cubeFrameBuffers[6];
    GLuint cubeTexture;

    std::array<CameraMatricesDataUBO, 6> matricesSidesUBO;
    std::array<std::array<mth::plane, 4>, 6> planes;
    std::array<mth::boundOBB, 6> bounds;


    mth::vec3 positionCenter;

    float farPlane;
    float nearPlane;

public:
    ShadowCubeFrameBuffer(float _sizeSide);

    ~ShadowCubeFrameBuffer();

    void setPosition(const mth::vec3 *_position, float _distance);

    const mth::vec3 *getPosition() const;

    float getSizeSide() const;

    GLuint getCubeFrameBuffer(int _index);

    GLuint getCubeTexture();

    const std::array<mth::plane, 4> *getClipingPlanes(int _index) const;

    mth::boundOBB getBoundOBB(int _index) const;

    CameraMatricesDataUBO *getMatricesUBOData(int _index);

    float getFarPlane() const;

    float getNearPlane() const;

    void clear();

private:
    void calculate();
};

#endif