#ifndef SHADOW_DIRECTION_LIGHT_H
#define SHADOW_DIRECTION_LIGHT_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <vector>
#include <MTH/vectors.h>
#include <MTH/matrix.h>
#include <MTH/plane.h>
#include <MTH/glGen_matrix.h>

#include "renderEngine/settings/settingsGraphicPipeline.h"
#include "renderEngine/graphicsPipeline/datas/shadowDirectionLightData.h"
#include "renderEngine/graphicsPipeline/lightRender/shadow/shadowGenerator.h"
#include "renderEngine/mainSystems/mainSystems.h"
#include "shadowFrameBuffer.h"

class ShadowDirectionLight
{
private:
    SettingsGraphicPipeline *settingsGraphicPipeline;
    SettingsGraphicPipeline::ShadowDirectionLightSettings data;
    ShadowGenerator *shadowGenerator;
    MainSystems *mainSystems;

    bool dataUpdate;

    int countUseShadowBuffers;
    float valueCrossShadowBlocks;
    std::array<float, MAX_SHADOWS_ON_DIRECTION_LIGHTS + 1> distancesParts;
    std::array<float, MAX_SHADOWS_ON_DIRECTION_LIGHTS> maxDistancesParts;
    mth::vec3 directionBoundAlongCamera;


    std::vector<ShadowFrameBuffer *> shadowBuffers;

public:
    ShadowDirectionLight(SettingsGraphicPipeline *_settingsGraphicPipeline, ShadowGenerator *_shadowGenerator, MainSystems *_mainSystems);

    ~ShadowDirectionLight();

    const SettingsGraphicPipeline::ShadowDirectionLightSettings *getShadowDirectionLightData() const;

    int getCountShadowBuffers() const;

    ShadowFrameBuffer *getShadowBuffer(int _index);

    float getDistanceShadowCascade(int _index);

    mth::vec3 getDirectionBoundAlongCamera();

    void update();

private:
    void clear();

    void updateShadowBuffersFromData();

    void fillShadowBuffers();

    mth::mat4 generateBasisDirectionLight(const mth::vec3 &normal);

    float getYOnLines(float _x, int _countLines, const std::array<mth::vec3, 3> *_lines);
    
    int testNewGenerationBoundsDirectionLight(mth::boundOBB *_boundsShadow, mth::mat4 *_shadow_V, mth::mat4 *_shadow_P,
                                              mth::mat4 *_shadowInverse_V, mth::mat4 *_shadowInverse_P, float *_shadowFarPlane, float *_shadowNearPlane, float *_shadowAspects);
};

#endif