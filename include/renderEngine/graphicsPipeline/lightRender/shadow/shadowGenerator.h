#ifndef SHADOW_GENERATOR_H
#define SHADOW_GENERATOR_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <MTH/vectors.h>
#include <MTH/plane.h>

#include "renderEngine/resources/technicalShader/tshader.h"
#include "renderEngine/resources/technicalShader/technicalProgramPipeline.h"
#include "renderEngine/resources/technicalShader/technicalShaderStorage.h"

#include "renderEngine/mainFrameBuffers/effectBuffers.h"
#include "renderEngine/mainSystems/programActivator/programActivator.h"

#include "shadowFrameBuffer.h"
#include "shadowCubeFrameBuffer.h"
#include "renderEngine/mainSystems/mainFunctions/mainFunctions.h"
#include "renderEngine/mainSystems/mainFunctions/cameraVertexesData.h"
#include "renderEngine/mainSystems/mainFunctions/cameraMatricesData.h"

class ShadowGenerator
{
private:
    MainFunctions *mainFunctions;
    ProgramActivator *programActivator;

    ShadowCubeFrameBuffer *testCubeFrameBuffer = NULL;

public:
    ShadowGenerator(MainFunctions *_mainFunctions, ProgramActivator *_programActivator, TechnicalShaderStorage *_technicalShaderStorage);

    ~ShadowGenerator();

    void generateShadowMap(ShadowFrameBuffer *_shadowFrameBuffer);

    void generateShadowCubeMap(ShadowCubeFrameBuffer *_shadowCubeFrameBuffer);

    ShadowCubeFrameBuffer *getTestCubeFrameBuffer() { return testCubeFrameBuffer; }

};

#endif