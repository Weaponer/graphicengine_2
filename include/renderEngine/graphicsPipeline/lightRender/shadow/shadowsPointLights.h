#ifndef SHADOWS_POINT_LIGHTS_H
#define SHADOWS_POINT_LIGHTS_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <vector>
#include <MTH/vectors.h>
#include <MTH/matrix.h>
#include <MTH/plane.h>
#include <MTH/glGen_matrix.h>

#include "renderEngine/mainSystems/sceneStructure/entities/pointLightGE.h"
#include "renderEngine/graphicsPipeline/lightRender/shadow/shadowGenerator.h"
#include "shadowFrameBuffer.h"
#include "renderEngine/mainSystems/mainFunctions/mainFunctions.h"

class ShadowsPointLights
{
private:
    MainFunctions *mainFunctions;
    ShadowGenerator *shadowGenerator;

public:
    ShadowsPointLights(MainFunctions *_mainFunctions, ShadowGenerator *_shadowGenerator);

    ~ShadowsPointLights();

    void update();

    /*private:
        void updateShadowBuffersFromData();

        void fillShadowBuffers();

        mth::mat4 generateBasisDirectionLight(const mth::vec3 &normal);

        void generateBoundDirectionLight(float _farPlane, float _nearPlane, mth::mat4 *_shadow_V, mth::mat4 *_shadow_P,
            mth::mat4 *_shadowInverse_V, mth::mat4 *_shadowInverse_P, float *_shadowFarPlane, float *_shadowNearPlane, std::array<mth::plane, 6> *_shadowPlanes);
    */
};

#endif