#ifndef TRANSPARENT_BUFFER_RENDER_H
#define TRANSPARENT_BUFFER_RENDER_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include "renderEngine/mainSystems/mainSystems.h"
#include "renderEngine/mainSystems/mainFunctions/mainFunctions.h"

#include "renderEngine/mainFrameBuffers/mainDepthBuffer.h"
#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"
#include "renderEngine/mainFrameBuffers/mainDepthBuffer.h"

#include "renderEngine/mainSystems/mainFunctions/renderQueue.h"
#include "renderEngine/mainSystems/programActivator/programActivator.h"

class TransparentBufferRender
{
private:
    MainSystems *mainSystems;
    MainFunctions *mainFunctions;

    RenderQueue *renderQueue;
    ProgramActivator *programActivator;
    MainDepthBuffer *mainDepthBuffer;
    ColorFrameBuffer testTexture;

    GLuint framebufferTransparent;
    GLuint mainColorTexture;
    GLuint alphaMulTexture;
    GLuint sumZPosAndCountTexture;


public:
    TransparentBufferRender(MainSystems *_mainSystems, ProgramActivator *_programActivator, MainDepthBuffer *_mainDepthBuffer);

    ~TransparentBufferRender();

    void update();

    GLuint getMainColorTexture();
    
    GLuint getAlphaMulTexture();

    GLuint getSumZposAndCountTexture();

    void enableTransparentTextures();

};


#endif