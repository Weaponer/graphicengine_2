#ifndef SKY_BUFFER_RENDER_H
#define SKY_BUFFER_RENDER_H

#define GL_GLEXT_PROTOTYPES

#include <MTH/matrix.h>

#include <GL/gl.h>
#include <GL/glu.h>

#include "renderEngine/mainSystems/mainSystems.h"
#include "renderEngine/resources/technicalShader/tshader.h"
#include "renderEngine/resources/technicalShader/technicalShaderStorage.h"
#include "renderEngine/resources/technicalShader/technicalProgramPipeline.h"
#include "renderEngine/mainSystems/mainFunctions/cameraVertexesData.h"
#include "renderEngine/mainSystems/programActivator/programActivator.h"

#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"

#include "datas/skyRenderData.h"
#include "renderEngine/settings/settingsGraphicPipeline.h"

struct SkyDataBuffer
{
    mth::vec3 directionSun;
    float intensity;
    mth::vec4 colorSun;
    mth::vec3 ambientLightColor;
    float intensityAmbientLight;
    int environmentBufferMaxLevel;
    bool environmentBufferAsAmbientLight;
};

class SkyBufferRender
{
private:
    MainSystems *mainSystems;
    SettingsGraphicPipeline *settingsGraphicPipeline;
    TechnicalShaderStorage *technicalShaderStorage;
    CameraVertexesData *mainCameraVertexesData;
    SkyRenderData *skyRenderData;
    ProgramActivator *programActivator;

    TechnicalProgramPipeline renderSkyBoxInBufferProgram;

private:
    bool onUpdate;

    GLuint uboSkyDataBuffer;
    SkyDataBuffer oldSkyData;

private:
    std::vector<std::pair<GLuint, int>> environmentFrameBuffers;
    GLuint environmentMapBuffer;

    GLuint bufferData;
    GLuint vertexArray;

private:
    TechnicalProgramPipeline generatorSubMapProgram;

public:
    SkyBufferRender(MainSystems *_mainSystem, SettingsGraphicPipeline *_settingsGraphicPipeline);

    ~SkyBufferRender();

    void renderSkyBoxInBuffer(ColorFrameBuffer *_dest);

    void update();

    void callUpdate();

    GLuint getEnvironmentCubeMap() { return environmentMapBuffer; };

    GLuint getUBOSkyData() { return uboSkyDataBuffer; }

private:
    void createVertexBuffer();

    void destroyVertexBuffer();

    void enableVertexBuffer();
    
    void disableVertexBuffer();

    void callDrawVertexBuffer();

    void createEnvironmentMap();

    void clearEnvironmentMap();

};

#endif