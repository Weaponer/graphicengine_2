#ifndef HDR_CORRECTION_H
#define HDR_CORRECTION_H

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glu.h>

#include <MTH/vectors.h>

#include "renderEngine/settings/resolutionSettings.h"
#include "renderEngine/mainSystems/mainFunctions/cameraVertexesData.h"
#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"

#include "renderEngine/mainSystems/mainFunctions/mainFunctions.h"
#include "renderEngine/mainSystems/mainFunctions/graphicOperations.h"

#include "renderEngine/resources/technicalShader/tshader.h"
#include "renderEngine/resources/technicalShader/technicalProgramPipeline.h"
#include "renderEngine/resources/technicalShader/technicalShaderStorage.h"
#include "renderEngine/resources/technicalShader/technicalComputePipeline.h"

#include "renderEngine/settings/settingsGraphicPipeline.h"

#define SIZE_GROUP 256
#define SIZE_SQRT_GROUP 16

class HDRCorrection
{
private:
    CameraVertexesData *CVD;
    TechnicalShaderStorage *technicalShaderStorage;
    MainFunctions *mainFunctions;
    SettingsGraphicPipeline *settingsGprahicPipeline;
    ColorFrameBuffer *resultBuffer;

    GLuint tempFrameBuffer;
    GLuint tempTexture;

    TechnicalProgramPipeline hdrCorrectionPipeline;

    GLuint bufferHistogram;
    GLuint imageResultValue;

    bool useStaticValue;
    float staticHDRValue;
    mth::vec4 params;
    GLuint locationParamsHisogramGenerator;
    GLuint locationParamsAverageCalculate;
    TechnicalComputePipeline histogramGeneratorPipeline;
    TechnicalComputePipeline averageCalculatePipeline;

    mth::Vector2Int countWorkGroup;

public:
    HDRCorrection(MainFunctions *_mainFunctions, SettingsGraphicPipeline *_settingsGprahicPipeline, ColorFrameBuffer *_resultBuffer);

    ~HDRCorrection();

    void update();

private:
    void generateBuffers();

    void deleteBuffers();

    void clearHistogramBuffer();

    void checkAndUpdateBuffer();
};

#endif