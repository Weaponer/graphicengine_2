#ifndef VIGNETTE_EFFECT_H
#define VIGNETTE_EFFECT_H

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glu.h>

#include <MTH/vectors.h>

#include "renderEngine/settings/resolutionSettings.h"
#include "renderEngine/mainSystems/mainFunctions/cameraVertexesData.h"
#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"

#include "renderEngine/mainSystems/mainFunctions/mainFunctions.h"
#include "renderEngine/mainSystems/mainFunctions/graphicOperations.h"

#include "renderEngine/resources/technicalShader/tshader.h"
#include "renderEngine/resources/technicalShader/technicalProgramPipeline.h"
#include "renderEngine/resources/technicalShader/technicalShaderStorage.h"

#include "renderEngine/settings/settingsGraphicPipeline.h"

class VignetteEffect
{
private:
    CameraVertexesData *CVD;
    TechnicalShaderStorage *technicalShaderStorage;
    SettingsGraphicPipeline *settingsGraphicPipeline;
    ColorFrameBuffer *resultBuffer;

    TechnicalProgramPipeline vignetteEffectProgram;
    struct SettingsVignetteData
    {
        mth::col color;
        float radius;
        float pow;
    } settingsVignetteData;
    GLuint settingsVignetteDataBuffer;

public:
    VignetteEffect(CameraVertexesData *_CVD, TechnicalShaderStorage *_technicalShaderStorage,
                   SettingsGraphicPipeline *_settingsGraphicPipeline, ColorFrameBuffer *_resultBuffer);

    ~VignetteEffect();

    void update();

private:
    void generateBuffer();

    void deleteBuffer();

    void updateSettingsBuffer();
};

#endif