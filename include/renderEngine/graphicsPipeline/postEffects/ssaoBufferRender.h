#ifndef SSAO_BUFFER_RENDER_H
#define SSAO_BUFFER_RENDER_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <vector>
#include <MTH/vectors.h>

#include "renderEngine/mainSystems/mainSystems.h"
#include "renderEngine/settings/resolutionSettings.h"
#include "renderEngine/mainSystems/mainFunctions/cameraVertexesData.h"
#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"

#include "renderEngine/resources/technicalShader/tshader.h"
#include "renderEngine/resources/technicalShader/technicalProgramPipeline.h"
#include "renderEngine/resources/technicalShader/technicalShaderStorage.h"


#define MAX_SSAO_ITERATIONS 16

class SSAOBufferRender
{
private:
    MainSystems *mainSystems;
    ResolutionSettings resolution;
    ColorFrameBuffer *normalDepthBuffer;
    GLuint depthBuffer;
    GLuint normalBuffer;
    CameraVertexesData *CVD;

    ColorFrameBuffer *resultBuffer;

    GLuint ssaoFrameBuffers;
    GLuint ssaoTextureBuffers;

    mth::Vector2Int sizeFramebuffer;

private:
    TechnicalProgramPipeline programGenerateOcclusion;

    struct IterationData
    {
        mth::mat2 rotate;
        mth::vec4 _m;
        int countSubIterations;
        float scale;
        mth::vec2 _m2;
    };

    struct SSAOGeneratorData
    {
        IterationData iterations[MAX_SSAO_ITERATIONS];
        mth::Vector2Int size;
        uint countIterations;
        float radius;
    } generateOcclusionData;
    GLuint generateOcclusionDataUBO;

    TechnicalProgramPipeline programCombineResult;

public:
    SSAOBufferRender(MainSystems *_mainSystems, ColorFrameBuffer *_normalDepthBuffer, ColorFrameBuffer *_resultBuffer);

    ~SSAOBufferRender();

    void update();

    GLuint getTextureSSAOBuffer();

private:
    void generateData();
};

#endif