#ifndef POST_EFFECTS_H
#define POST_EFFECTS_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include "ssaoBufferRender.h"
#include "bloomRender.h"
#include "hdrCorrection.h"
#include "vignetteEffect.h"
#include "renderEngine/mainSystems/mainSystems.h"
#include "renderEngine/mainSystems/mainFunctions/mainFunctions.h"
#include "renderEngine/settings/settingsGraphicPipeline.h"
#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"

class PostEffects
{
private:
    SSAOBufferRender ssaoBufferRender;
    BloomRender bloomRender;
    HDRCorrection hdrCorrection;
    VignetteEffect vignetteEffect;
    SettingsGraphicPipeline *settingsGraphicPipeline;
    ColorFrameBuffer *resultBuffer;

public:
    PostEffects(MainSystems *_mainSystems, ColorFrameBuffer *_normalDepthBuffer,
                SettingsGraphicPipeline *_settingsGraphicPipeline, ColorFrameBuffer *_resultBuffer);

    ~PostEffects();

    SSAOBufferRender *getSSAOBufferRender();

    void update();

    void updateBloom();

    void updateHDR();

    void updateOtherEffects();
};

#endif