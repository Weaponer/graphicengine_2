#ifndef BLOOM_RENDER_H
#define BLOOM_RENDER_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <vector>
#include <MTH/vectors.h>

#include "renderEngine/settings/resolutionSettings.h"
#include "renderEngine/mainSystems/mainFunctions/cameraVertexesData.h"
#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"

#include "renderEngine/resources/technicalShader/tshader.h"
#include "renderEngine/resources/technicalShader/technicalProgramPipeline.h"
#include "renderEngine/resources/technicalShader/technicalShaderStorage.h"

#include "renderEngine/settings/settingsGraphicPipeline.h"

#define MAX_SAMPLES_BLOOM_EFFECT 12

class BloomRender
{
private:
    CameraVertexesData *CVD;
    TechnicalShaderStorage *technicalShaderStorage;
    SettingsGraphicPipeline *settingsGraphicPipeline;
    ColorFrameBuffer *resultBuffer;


private:
    TechnicalProgramPipeline bloomPreparation;
    TechnicalProgramPipeline bloomGenerator;

    GLuint framebuffersBloom[MAX_SAMPLES_BLOOM_EFFECT];
    GLuint texturesBloom[MAX_SAMPLES_BLOOM_EFFECT];
    mth::Vector2Int sizes[MAX_SAMPLES_BLOOM_EFFECT];

private:
    TechnicalProgramPipeline bloomCombiner;

    struct BloomCombinerData
    {
        int countSubMaps;
        float forceBloom;
    } bloomCombinerData;
    GLuint bloomCombinerDataUBO;

public:
    BloomRender(CameraVertexesData *_CVD, TechnicalShaderStorage *_technicalShaderStorage,
                SettingsGraphicPipeline *_settingsGraphicPipeline, ColorFrameBuffer *_resultBuffer);

    ~BloomRender();

    void update();

private:
    void generateTextures();

    void updateUniformBuffer();

};

#endif