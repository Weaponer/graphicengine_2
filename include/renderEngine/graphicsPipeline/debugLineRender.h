#ifndef DEBUG_LINE_RENDER_H
#define DEBUG_LINE_RENDER_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include "renderEngine/mainFrameBuffers/mainDepthBuffer.h"
#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"

#include "renderEngine/resources/technicalShader/technicalShaderStorage.h"
#include "renderEngine/resources/technicalShader/technicalProgramPipeline.h"
#include "renderEngine/resources/technicalShader/tshader.h"

#include "renderEngine/mainSystems/mainFunctions/cameraVertexesData.h"
#include "renderEngine/mainSystems/sceneStructure/entityRegister.h"

class DebugLineRender
{
private:
    ColorFrameBuffer *colorBuffer;
    TechnicalProgramPipeline debugLinesProgram;
    CameraVertexesData *mainCameraVertexesData;
    EntityRegister *entityRegister;

    GLuint bufferArray;
    GLuint vertexArrays;

private:
    bool isEnable;

public:
    DebugLineRender(ColorFrameBuffer *_colorBuffer, TechnicalShaderStorage *_techincalShaderStorage,
                    CameraVertexesData *_mainCameraVertexesData, EntityRegister *_entityRegister);

    ~DebugLineRender();

    void enableDebug();

    void disableDebug();

    void update();
};

#endif