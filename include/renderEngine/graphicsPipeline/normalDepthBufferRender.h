#ifndef NORMAL_DEPTH_BUFFER_RENDER_H
#define NORMAL_DEPTH_BUFFER_RENDER_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include "renderEngine/mainSystems/mainFunctions/mainFunctions.h"
#include "renderEngine/mainFrameBuffers/mainDepthBuffer.h"

#include "renderEngine/mainSystems/mainFunctions/renderQueue.h"
#include "renderEngine/mainSystems/programActivator/programActivator.h"

class NormalDepthBufferRender
{
private:
    ColorFrameBuffer *normalDepthBuffer;

    MainFunctions *mainFunctions;
    ProgramActivator *programActivator;

public:
    NormalDepthBufferRender(ColorFrameBuffer *_normalDepthBuffer, MainFunctions *_mainFunctions, ProgramActivator *_programActivator);

    ~NormalDepthBufferRender();

    void update();
};

#endif