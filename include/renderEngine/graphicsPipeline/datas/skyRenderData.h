#ifndef SKY_RENDER_DATA_H
#define SKY_RENDER_DATA_H

#include <MTH/vectors.h>
#include <MTH/color.h>
#include "renderEngine/resources/material/material.h"

enum TypeUpdateSky
{
    EveryFrame = 0,
    Call = 1,
};

struct SkyRenderData
{
    TypeUpdateSky typeUpdateSky;
    Material *skyMaterial;
    
    mth::vec3 directionSun;
    mth::col colorSun;
    float intensity;

    mth::col ambientLightColor;
    float intensityAmbientLight;
    bool environmentBufferAsAmbientLight;

};

#endif