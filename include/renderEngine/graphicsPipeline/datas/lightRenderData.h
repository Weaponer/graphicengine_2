#ifndef LIGHT_RENDER_DATA_H
#define LIGHT_RENDER_DATA_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

struct LightRenderData
{
    GLuint environmentBuffer;
    GLuint pointLightsAndDirectionLightForceBuffer;
    GLuint pointLightsNormalsBuffer;
    GLuint skyDataUBO;
    GLuint allPointLightsUBO;
};

#endif