#ifndef VOLUMETRIC_FOG_DATA_H
#define VOLUMETRIC_FOG_DATA_H

#include <MTH/vectors.h>
#include <MTH/color.h>

struct  VolumetricFogData
{
    bool generateFog;

    float intensivityFog;
    float intensivityLightInFog;

    mth::col colorFog;
    float startDist;
    float endFogDist;

    bool usePointsLightForce;
    float pointLightAttenuationPower;

    bool generateRaysDirectionLight;
    float maxDistRaysPerPixel;
    int countSamplesPerPixel;
};


#endif