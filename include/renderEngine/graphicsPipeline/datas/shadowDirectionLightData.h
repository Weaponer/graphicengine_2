#ifndef SHADOW_DIRECTION_LIGHT_DATA_H
#define SHADOW_DIRECTION_LIGHT_DATA_H

enum SDLD_ReductionType
{
    SDLD_REDUCTION_TYPE_LINEAR = 0,
    SDLD_REDUCTION_TYPE_SQUARE = 1
};

struct ShadowDirectionLightData
{
    float shadowDistance;
    float maxUpShadowDistance;
    int degreePartition;
    int countSubMaps;
    SDLD_ReductionType reductionTypePartitionVolume;

    int sizeSide;
    SDLD_ReductionType reductionTypeSizeSubMaps;
    SDLD_ReductionType reductionTypeSizePartition;

    float decayDistance;
    float maxDecay;
};

#endif