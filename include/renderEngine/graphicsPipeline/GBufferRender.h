#ifndef G_BUFFER_RENDER_H
#define G_BUFFER_RENDER_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include "renderEngine/resources/modelLightShader/modelLightShaderStorage.h"

#include "renderEngine/mainSystems/mainFunctions/mainFunctions.h"
#include "renderEngine/mainFrameBuffers/GBuffer.h"

#include "renderEngine/mainSystems/mainFunctions/renderQueue.h"
#include "renderEngine/mainSystems/programActivator/programActivator.h"

class GBufferRender
{
private:
    GBuffer *gBuffer;

    MainFunctions *mainFunctions;
    ProgramActivator *programActivator;

    bool renderedSurfacePasses;
    int usedAdditionalLayers;
    bool usedModelLight[MAX_COUNT_MODEL_LIGHT];

public:
    GBufferRender(GBuffer *_gBuffer, MainFunctions *_mainFunctions, ProgramActivator *_programActivator);

    ~GBufferRender();

    bool wasRenderSurfacePasses() const;

    int getUsedAdditionalLayers() const;

    const bool *getUsedModelLight() const;

    void update();
};

#endif