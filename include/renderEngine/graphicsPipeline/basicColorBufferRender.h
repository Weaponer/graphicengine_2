#ifndef BASIC_COLOR_BUFFER_RENDER_H
#define BASIC_COLOR_BUFFER_RENDER_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include "renderEngine/mainSystems/mainFunctions/mainFunctions.h"

#include "renderEngine/mainFrameBuffers/mainDepthBuffer.h"
#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"

#include "renderEngine/mainSystems/mainFunctions/renderQueue.h"
#include "renderEngine/mainSystems/programActivator/programActivator.h"

class BasicColorBufferRender
{
private:
    MainFunctions *mainFunctions;
    ColorFrameBuffer *colorBuffer;

    RenderQueue *renderQueue;
    ProgramActivator *programActivator;

public:
    BasicColorBufferRender(ColorFrameBuffer *_colorBuffer, MainFunctions *_mainFunctions, ProgramActivator *_programActivator);

    ~BasicColorBufferRender();

    void update();
};

#endif