#ifndef GRAPHIC_ENGINE_H
#define GRAPHIC_ENGINE_H
#define GL_GLEXT_PROTOTYPES

#include <MTH/vectors.h>

#include "window.h"
#include "settings/resolutionSettings.h"
#include "settings/settingsGraphicPipeline.h"
#include "settings/settingsMainSystems.h"

#include "mainSystems/mainSystems.h"
#include "graphicPipeline.h"
#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"

class GraphicEngine
{
private:
    Window *win;

private:
    SettingsMainSystems *settingsMainSystems;
    SettingsGraphicPipeline settingsGraphicPipeline;
    MainSystems mainSystems;
    GraphicPipeline graphicPipeline;

public:
    GraphicEngine(SettingsMainSystems *_settingsMainSystems);

    ~GraphicEngine();

    void connectSDLWindow(Window *window);

    void render(float _deltaTime);

    void endRender();

    void swapWindow();

public:
    MainFunctions *getMainFunctions() { return mainSystems.getMainFunctions(); }

    SceneStructure *getSceneStructure() { return mainSystems.getSceneStructure(); }

    MainSystems *getMainSystems() { return &mainSystems; }

    SettingsGraphicPipeline *getSettingsGraphicPipeline() { return &settingsGraphicPipeline; }

    const SettingsMainSystems *getSettingsMainSystems() { return settingsMainSystems; }

    GraphicPipeline *getGraphicPipeline() { return &graphicPipeline; };

    mth::vec2 getSizeWindow();

private:
    void drawDepthBuffer();
};

#endif