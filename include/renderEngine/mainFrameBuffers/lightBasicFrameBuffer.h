#ifndef LIGHT_BASIC_FRAME_BUFFER_H
#define LIGHT_BASIC_FRAME_BUFFER_H

#define GL_GLEXT_PROTOTYPES

#include<GL/gl.h>
#include<GL/glu.h>

#include "settings/resolutionSettings.h"

class LightBasicFrameBuffer
{
private:
    int width;
    int height;

    GLuint frameBuffer;
    GLuint pointsAndDirectionalLightBuffer;
    GLuint pointLightsDirectionBuffer;

public:
    LightBasicFrameBuffer(ResolutionSettings _resolution);

    ~LightBasicFrameBuffer();

    int getWidth() const;

    int getHeight() const;

    GLuint getFrameBuffer();

    GLuint getPointsAndDirectionalLightTexture();

    GLuint getPointLightsDirectionTexture();

    void clear();

    void resize(const ResolutionSettings *_resolution);

    void enable();

    void disable();

};

#endif