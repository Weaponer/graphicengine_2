#ifndef EFFECT_BUFFERS_H
#define EFFECT_BUFFERS_H

#define GL_GLEXT_PROTOTYPES

#include<GL/gl.h>
#include<GL/glu.h>

class EffectBuffers
{
private:
    GLuint vertexBuffer;
    GLuint bufferVectors;

public:
    EffectBuffers();

    ~EffectBuffers();

    void enable();

    void callDraw();

    void disable();
};

#endif