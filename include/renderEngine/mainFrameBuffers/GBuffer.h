#ifndef G_BUFFER_H
#define G_BUFFER_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include "settings/resolutionSettings.h"
#include "mainDepthBuffer.h"
#include "colorFrameBuffer.h"

#define G_BUFFER_COUNT_ADDITIONAL_BUFFERS 2
#define G_BUFFER_COUNT_BUFFERS 4
#define G_BUFFER_OFFSET_ADDITIONAL_BUFFERS 2

#define G_BUFFER_LAYER_1 (1)
#define G_BUFFER_LAYER_2 (1 << 1)
#define G_BUFFER_LAYER_3 (1 << 2)
#define G_BUFFER_LAYER_4 (1 << 3)

class GBuffer
{
private:
    GLuint frameBuffer;

    ColorFrameBuffer *normalsBuffer;
    MainDepthBuffer *depthBuffer;
    GLuint layers[G_BUFFER_COUNT_BUFFERS];
    // rgb - color; a - model light ID;
    // r - metallic, g - roughness, ba - free;
    // additional layer;
    // additional layer;

    int enabledLayers;

public:
    GBuffer(ResolutionSettings resolution, MainDepthBuffer *_depthBuffer, ColorFrameBuffer *_normalsBuffer);

    ~GBuffer();

    void clearLayers();

    void enableFramebuffer(int _layers);

    void disableFramebuffer();

    ColorFrameBuffer *getNormalsBuffer();

    MainDepthBuffer *getDepthBuffer();

    GLuint getLayer(int _index);

    GLuint getLayer1();

    GLuint getLayer2();

    GLuint getLayer3();

    GLuint getLayer4();
};

#endif