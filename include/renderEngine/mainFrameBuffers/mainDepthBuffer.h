#ifndef MAIN_DEPTH_BUFFER_H
#define MAIN_DEPTH_BUFFER_H

#define GL_GLEXT_PROTOTYPES

#include<GL/gl.h>
#include<GL/glu.h>

#include "settings/resolutionSettings.h"

class MainDepthBuffer
{
private:
    int width;
    int height;

    GLuint frameBuffer;
    GLuint depthTexture;

public:
    MainDepthBuffer(ResolutionSettings _resolution);

    ~MainDepthBuffer();

    int getWidth() const;

    int getHeight() const;

    GLuint getFrameBuffer();

    GLuint getDepthtexture();

    void clear();

    void resize(const ResolutionSettings *_resolution);

    void enable();

    void disable();
};

#endif