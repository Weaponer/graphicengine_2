#ifndef TRANSPARENT_FRAME_BUFFER_H
#define TRANSPARENT_FRAME_BUFFER_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>
#include <MTH/vectors.h>

#include "settings/resolutionSettings.h"
#include "mainDepthBuffer.h"

#define MAX_COUNT_FRAGMENTS_PER_PIXEL 9

class TransparantFrameBuffer
{
private:
    struct FragNode
    {
        mth::vec4 normalDepth;
        uint next;
        float _m[3];
    };

    struct FragLightNode
    {
        mth::vec4 pointAndDirectionLightsForce;
        mth::vec4 lightPointsDirection;
    };

    struct FragResultNode
    {
        mth::vec4 color;
    };

private:
    int width;
    int height;

    GLuint frameBuffer;

    GLuint bufferNodes;
    GLuint bufferLightNodes;
    GLuint bufferResultNodes;
    uint countNodes;

    GLuint uimageBuffer;
    GLuint imageCountBuffer;
    GLuint atomicCounterNodes;

    MainDepthBuffer *mainDepthBuffer;

public:
    TransparantFrameBuffer(const ResolutionSettings *_resolution, MainDepthBuffer *_mainDetphBuffer);

    ~TransparantFrameBuffer();

    int getWidth() const;

    int getHeight() const;

    GLuint getFrameBuffer();

    GLuint getUImageBuffer();

    GLuint getUImageCountBuffer();

    GLuint getBufferNodes();

    GLuint getAtomicCounterNodes();

    GLuint getCountNodes();

    void clear();

    void resize(const ResolutionSettings *_resolution);

    void enable();

    void disable();
};

#endif