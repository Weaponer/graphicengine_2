#ifndef COLOR_FRAME_BUFFER_H
#define COLOR_FRAME_BUFFER_H

#define GL_GLEXT_PROTOTYPES

#include<GL/gl.h>
#include<GL/glu.h>

#include <MTH/vectors.h>

#include "mainDepthBuffer.h"

class ColorFrameBuffer
{
private:
    int width;
    int height;
    GLint format;
    mth::vec4 clearColor;

    GLuint frameBuffer;
    GLuint textureBuffer;
    MainDepthBuffer *mainDepthBuffer;

public:
    ColorFrameBuffer(GLint _format, mth::vec4 _clearColor, ResolutionSettings _resolution, MainDepthBuffer *_mainDetphBuffer);

    ~ColorFrameBuffer();

    int getWidth() const;

    int getHeight() const;

    GLint getFormat() const;

    GLuint getFrameBuffer();

    MainDepthBuffer *getMainDepthBuffer();

    GLuint getColorTexture();

    void clear();

    mth::vec4 getClearColor() const;

    void setClearColor(const mth::vec4 *_vec);

    void resize(const ResolutionSettings *_resolution);

    void enable();

    void disable();
};

#endif