#ifndef GRAPHIC_PIPELINE_H
#define GRAPHIC_PIPELINE_H

#include "settings/settingsGraphicPipeline.h"
#include "renderEngine/mainSystems/mainSystems.h"
#include "renderEngine/mainSystems/mainFunctions/mainFunctions.h"
#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"
#include "renderEngine/mainFrameBuffers/mainDepthBuffer.h"
#include "renderEngine/mainFrameBuffers/lightBasicFrameBuffer.h"
#include "renderEngine/mainFrameBuffers/GBuffer.h"

#include "renderEngine/graphicsPipeline/normalDepthBufferRender.h"
#include "renderEngine/graphicsPipeline/GBufferRender.h"
#include "renderEngine/graphicsPipeline/skyBufferRender.h"
#include "renderEngine/graphicsPipeline/lightRender/lightBufferRender.h"
#include "renderEngine/graphicsPipeline/renderLightGBuffer.h"
#include "renderEngine/graphicsPipeline/basicColorBufferRender.h"
#include "renderEngine/graphicsPipeline/transparentBufferRender.h"
#include "renderEngine/graphicsPipeline/postEffects/postEffects.h"
#include "renderEngine/graphicsPipeline/debugLineRender.h"

#include "renderEngine/graphicsPipeline/datas/lightRenderData.h"

class GraphicPipeline
{
private:
    SettingsGraphicPipeline *settingsGraphicPipeline;
    MainSystems *mainSystems;
    MainFunctions *mainFunctions;

    MainDepthBuffer mainDepthBuffer;
    ColorFrameBuffer normalBuffer;
    LightBasicFrameBuffer lightBasicFrameBuffer;
    GBuffer gBuffer;
    ColorFrameBuffer resultColorBuffer;

private:
    NormalDepthBufferRender normalDepthBufferRender;
    GBufferRender gBufferRender;
    SkyBufferRender skyBufferRender;
    LightBufferRender lightBufferRender;
    LightRenderData lightRenderData;
    RenderLightGBuffer renderLightGBuffer;
    BasicColorBufferRender basicColorBufferRender;
    TransparentBufferRender transparentBufferRender; 
    PostEffects postEffectsRender;
    DebugLineRender debugLineRender;

public:
    GraphicPipeline(SettingsGraphicPipeline *_settingsGraphicPipeline, MainSystems *_mainSystems);

    ~GraphicPipeline();

    void update();

    GLuint getResultColorBuffer();

    GLuint getDepthBuffer();

    GLuint getNormalBuffer();

    const LightRenderData *getLightRenderData() const;

    SkyBufferRender *getSkyBufferRender();

private:
    void initOptionsOpenGL();
};

#endif