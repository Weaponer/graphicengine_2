#ifndef GRAPHIC_CONFIG_H
#define GRAPHIC_CONFIG_H

#include "engineBase/configManager/subConfig.h"
#include "engineBase/configManager/configTable.h"

#define GRAPHIC_CONFIG_MAINT_TABLE "Graphic"
#define GRAPHIC_CONFIG_INTERNAL_TABLE "Internal"
#define GRAPHIC_CONFIG_COMPILER_TABLE "Compiler"

class GraphicConfig : public SubConfig
{
public:
    GraphicConfig();

    virtual ~GraphicConfig();

    ConfigTable getGraphicTable();
    
    ConfigTable getGraphicInternalTable();

    ConfigTable getGraphicCompilerTable();

protected:
    virtual void initCategories(lua_State *L) override;

};

#endif