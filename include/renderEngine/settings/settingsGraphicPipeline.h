#ifndef SETTINGS_GRAPHIC_PIPELINE_H
#define SETTINGS_GRAPHIC_PIPELINE_H

#include <algorithm>
#include <array>

#include "renderEngine/graphicsPipeline/datas/skyRenderData.h"
#include "renderEngine/graphicsPipeline/datas/shadowDirectionLightData.h"

#include "specialDefines.h"

struct SettingsGraphicPipeline
{
    struct
    {
        struct SSAOSettings
        {
            bool enable;

        } SSAO;

        struct BloomSettings
        {
            bool enable;

        private:
            int countSamples;
            float force;

        public:
            int getCountSamples() const { return countSamples; }
            void setCountSamples(int _countSamples) { countSamples = std::max(1, std::min(_countSamples, 12)); }

            float getForce() const { return force; }
            void setForce(float _force) { force = std::max(0.0f, _force); }
        } bloom;
        struct HDRCorrection
        {
            bool useStaticValue;

        private:
            float staticValue;
            float minimumLogLuminance;
            float inverseLogLuminanceRange;

        public:
            float getStaticValue() const { return staticValue; }
            void setStaticValue(float _staticValue) { staticValue = std::max(0.0f, _staticValue); }

            float getMinimumLogLuminance() const { return minimumLogLuminance; }
            void setMinimumLogLuminance(float _minimumLogLuminance) { minimumLogLuminance = std::max(0.0f, _minimumLogLuminance); }

            float getInverseLogLuminanceRange() const { return inverseLogLuminanceRange; }
            void setInverseLogLuminanceRange(float _inverseLogLuminanceRange) { inverseLogLuminanceRange = std::max(0.0f, _inverseLogLuminanceRange); }
        } hdrCorrection;
        struct VignetteEffect
        {
            bool useVignetteEffect;
            mth::col color;

        private:
            float radius;
            float pow;

        public:
            float getRadius() const { return radius; }
            void setRadius(float _radius) { radius = std::max(0.0f, _radius); }

            float getPow() const { return pow; }
            void setPow(float _pow) { pow = std::max(0.0f, _pow); }

        } vignetteEffect;
    } postEffects;

    SkyRenderData sky;

    struct ShadowDirectionLightSettings
    {
        SDLD_ReductionType reductionTypePartitionVolume;

        SDLD_ReductionType reductionTypeSizePartition;

    private:
        std::array<float, MAX_SHADOWS_ON_DIRECTION_LIGHTS> shadowBias;
        float shadowDistance;
        float maxUpShadowDistance;
        int degreePartition;

        int sizeSide;

        float startFadeDistance;

    public:
        float getShadowBias(int _index) const { return shadowBias[std::min(std::max(_index, 0), MAX_SHADOWS_ON_DIRECTION_LIGHTS - 1)]; };
        void setShadowBias(int _index, float _bias) { shadowBias[std::min(std::max(_index, 0), MAX_SHADOWS_ON_DIRECTION_LIGHTS - 1)] = std::max(_bias, 0.0f); };

        float getShadowDistance() const { return shadowDistance; }
        void setShadowDistance(float _shadowDistance) { shadowDistance = std::max(0.01f, _shadowDistance); }

        float getMaxUpShadowDistance() const { return maxUpShadowDistance; }
        void setMaxUpShadowDistance(float _maxUpShadowDistance) { maxUpShadowDistance = std::max(0.01f, _maxUpShadowDistance); }

        int getDegreePartition() const { return degreePartition; }
        void setDegreePartition(int _degreePartition) { degreePartition = std::min(MAX_SHADOWS_ON_DIRECTION_LIGHTS, _degreePartition); }

        int getSizeSide() const { return sizeSide; }
        void setSizeSide(int _sizeSide) { sizeSide = std::max(128, std::min(_sizeSide, 4096)); }

        float getStartFadeDistance() const { return startFadeDistance; }
        void setStartFadeDistance(float _startFadeDistance) { startFadeDistance = std::min(shadowDistance, std::max(0.0f, _startFadeDistance)); }
    } shadowDirectionLight;

    struct VolumetricFogData
    {
        bool generateFog;
        bool usePointsLightForce;
        bool generateRaysDirectionLight;

        mth::col colorFog;

    private:
        std::array<int, 3> sizeFogGrid = {16, 16, 16};

        float intensivityFog;
        float anisotropic;
        float scattering;
        float absorption;

        float startDist = 0.3f;
        float endFogDist = 200.0f;

        float maxDistRaysPerPixel = 200.0f;

    public:
        std::array<int, 3> getSizeFogGrid() const { return sizeFogGrid; }
        void setSizeFogGrid(std::array<int, 3> _sizeFogGrid) { sizeFogGrid = {std::max(16, _sizeFogGrid[0]), std::max(16, _sizeFogGrid[1]), std::max(16, _sizeFogGrid[2])}; }

        float getIntensivityFog() const { return intensivityFog; }
        void setIntensivityFog(float _intensivityFog) { intensivityFog = std::max(0.0f, _intensivityFog); }

        float getAnisotropic() const { return anisotropic; }
        void setAnisotropic(float _anisotropic) { anisotropic = std::min(1.0f, std::max(0.0f, _anisotropic)); }

        float getScattering() const { return scattering; }
        void setScattering(float _scattering) { scattering = std::min(1.0f, std::max(0.0f, _scattering)); }

        float getAbsorption() const { return absorption; }
        void setAbsorption(float _absorption) { absorption = std::min(1.0f, std::max(0.0f, _absorption)); }

        float getStartDist() const { return startDist; }
        void setStartDist(float _startDist) { startDist = std::max(std::min(endFogDist, _startDist), 0.0f); }

        float getEndFogDist() const { return endFogDist; }
        void setEndFogDist(float _endFogDist) { endFogDist = std::max(std::max(startDist, _endFogDist), 0.0f); }

        float getMaxDistRaysPerPixel() const { return maxDistRaysPerPixel; }
        void setMaxDistRaysPerPixel(float _maxDistRaysPerPixel) { maxDistRaysPerPixel = std::max(0.0f, _maxDistRaysPerPixel); }

    } volumetricFog;

    struct LODSettings
    {
        bool useLods;
        bool useLodsForShadow;

    private:
        float lodBias = 2.0f;
        float lodShadowBias = 2.0f;

    public:
        float getLodBias() const { return lodBias; }
        void setLodBias(float _lodBias) { lodBias = std::max(0.01f, _lodBias); }

        float getLodShadowBias() const { return lodShadowBias; }
        void setLodShadowBias(float _lodShadowBias) { lodShadowBias = std::max(0.01f, _lodShadowBias); }
    } lodSettings;
};

#endif