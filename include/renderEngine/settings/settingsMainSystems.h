#ifndef SETTINGS_MAIN_SYSTEMS_H
#define SETTINGS_MAIN_SYSTEMS_H

#include "resolutionSettings.h"
#include "renderEngine/resources/technicalShader/technicalShaderStorage.h"
#include "renderEngine/resources/modelLightShader/modelLightShaderStorage.h"
#include "graphicConfig.h"

struct SettingsMainSystems
{
    ResolutionSettings resolutionSettings;
    GraphicConfig graphicConfig;
    TechnicalShaderStorage techincalShaderStorage;
    ModelLightShaderStorage modelLightShaderStorage;
};

#endif