#ifndef RESOLUTION_SETTINGS_H
#define RESOLUTION_SETTINGS_H

struct ResolutionSettings
{
    int width;
    int height;
};

#endif