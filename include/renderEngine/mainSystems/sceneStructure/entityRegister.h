#ifndef ENTITY_REGISTER_H
#define ENTITY_REGISTER_H

#include "entities/entity.h"
#include "entities/cameraGE.h"
#include "entities/debugRenderGE.h"
#include "entities/meshRenderGE.h"
#include "entities/pointLightGE.h"
#include "entities/densityVolumeGE.h"

class EntityRegister
{
private:
    std::list<CameraGE *> cameras;
    std::list<DebugRendererGE *> debugRenders;
    std::list<MeshRenderGE *> meshRenders;
    std::list<PointLightGE *> pointLights;
    std::list<DensityVolumeGE *> densityVolumes;

public:
    EntityRegister();

    ~EntityRegister();

    void _addEntityGE(TypeGraphicEntity _type, Entity *_entity);

    void _removeEntityGE(Entity *_entity);

    CameraGE *getActiveCamera() const;

    const std::list<DebugRendererGE *> *getListDebugRenderers() const;

    const std::list<MeshRenderGE *> *getListMeshRenders() const;

    const std::list<PointLightGE *> *getListPointLights() const;

    const std::list<DensityVolumeGE *> *getListDensityVolumes() const;
};

#endif