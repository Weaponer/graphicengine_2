#ifndef SCENE_STRUCTURE_H
#define SCENE_STRUCTURE_H

#include "entityRegister.h"
#include "sceneGrid.h"

#include "renderEngine/settings/graphicConfig.h"

class SceneStructure
{
private:
    EntityRegister entityRegister;
    SceneGrid sceneGrid;

public:
    SceneStructure(GraphicConfig *_graphicConfig);

    ~SceneStructure();

    void addEntityGE(TypeGraphicEntity _type, Entity *_entity);

    void removeEntityGE(Entity *_entity);

    void changeEntityOrientation(Entity *_entity);

    EntityRegister *getEntityRegister();

    SceneGrid *getSceneGrid();
};

#endif