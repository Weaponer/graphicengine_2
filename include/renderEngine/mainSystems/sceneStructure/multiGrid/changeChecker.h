#ifndef CHANGE_CHECKER_H
#define CHANGE_CHECKER_H

#include "datas/changeCheckerData.h"

class ChangeChecker
{
private:
    unsigned long long currectState;

public:
    ChangeChecker();

    ~ChangeChecker();
    
    void nextStep();

    void synchronize(ChangeCheckerData *_changeChekerData) const;

    bool wasCheck(ChangeCheckerData *_changeChekerData);
};

#endif