#ifndef GRID_CONTROLLER_H
#define GRID_CONTROLLER_H

#include "datas/gridData.h"
#include "datas/cellData.h"
#include "datas/changeCheckerData.h"
#include "datas/iterator.h"
#include "datas/objectData.h"
#include "datas/configGrid.h"
#include "datas/searchAreaData.h"

#include "memoryManager.h"
#include "changeChecker.h"

#include "debugOutput/threadDebugOutput.h"

class GridController
{
private:
    struct FindCellData
    {
        HashPosition hash;
        CellData *result;
        CellData *endCell;
        GridData *endGrid;
        int endLayer;
    };

private:
    MemoryManager memoryManager;
    ChangeChecker changeChecker;
    ConfigGrid configGrid;

    std::vector<int> sizesCells;

    GridData *mainGrid;

    ThreadDebugOutput *debugOutput;

public:
    GridController(ThreadDebugOutput *_debugOutput);

    ~GridController();

    void setConfig(ConfigGrid _configGrid);

    void clear();

    void addObjectSphereBound(ObjectData *_object, mth::vec3 _newPos, float _radius);

    void addObjectAABBBound(ObjectData *_object, mth::vec3 _newPos, mth::vec3 _size);

    bool removeObject(ObjectData *_object);

    void moveResizeObject(ObjectData *_object, mth::vec3 _newPos, BoundObjectType _boundType, mth::vec3 _boundData);

private:
    inline unsigned int getNeedLayer(float _size);

    inline int getPos(float _v, int size);

    inline HashPosition getHash(int _layer, mth::vec3 _position);

    FindCellData findCell(int _layer, mth::vec3 _position);

    CellData *checkCell(int _layer, mth::vec3 _position);

    CellData *getOrCreateCell(int _layer, mth::vec3 _position);

    inline bool checkCellIsFree(CellData *_cell);

    void optimizeCell(CellData *_cell);

    void addObject(ObjectData *_object, int _layer, mth::vec3 _posCell);

    bool removeObject(ObjectData *_object, int _layer, mth::vec3 _posCell);

    void addObjectToCell(CellData *_cell, ObjectData *_object);

    bool removeObjectFromCell(CellData *_cell, ObjectData *_object);

    void cellNowActive(CellData *_cell);

    void cellNowNoneActive(CellData *_cell);

public:
    unsigned int getObjectsInArea(SearchAreaData _searchAreaData, unsigned int _maxCount, ObjectData **_objects, bool _includeNoneActive = false);

private:
    bool getObjectsInCells(SearchAreaData &_searchAreaData, unsigned int _maxCount, ObjectData **_objects, unsigned int &_currentCount,
                           CellData *_cell, unsigned int _layer, bool _allInside, bool _includeNoneActive = false);

    inline int aabbInsideSearchArea(SearchAreaData &_searchAreaData, mth::boundAABB &_aabb);

    inline bool checkCellAndPush(SearchAreaData &_searchAreaData, unsigned int _maxCount, ObjectData **_objects, unsigned int &_currentCount,
                                 CellData *_cell, bool _allInside, bool _includeNoneActive = false);
};

#endif