#ifndef ITERATOR_H
#define ITERATOR_H

struct ObjectData;

struct Iterator
{
    Iterator *next;
    ObjectData *data;
};

#endif