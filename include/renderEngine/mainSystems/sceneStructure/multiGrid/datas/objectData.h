#ifndef OBJECT_DATA_H
#define OBJECT_DATA_H

#include <MTH/vectors.h>
#include <MTH/matrix.h>

#include "enumsMultiGrid.h"
#include "changeCheckerData.h"
#include "cellData.h"

struct ObjectData
{
    ChangeCheckerData changeCheckerData;
    
    mth::vec3 position;
    mth::vec3 boundData;
    BoundObjectType boundType;

    HashPosition minBound;
    HashPosition maxBound;
    unsigned int layerGrid;
    bool allInside;
    
    bool active;

    void *mainObject;
    int maskObjectType;
};

#endif