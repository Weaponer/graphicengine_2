#ifndef CONFIG_GRID_H
#define CONFIG_GRID_H

#define MAX_COUNT_LAYERS_GRID 10

struct ConfigGrid
{
    int countLayers;
    float maxSizeCell;
    int stepDived;
    int scaleFactor;
};

#endif