#ifndef CELL_DATA_H
#define CELL_DATA_H

#include <MTH/vectors.h>

#include "iterator.h"

struct GridData;

struct HashPosition
{
    unsigned long long hash;
    int x;
    int y;
    int z;

    bool operator==(const HashPosition &_val)
    {
        return hash == _val.hash && x == _val.x && y == _val.y && z == _val.z; 
    }

    bool operator!=(const HashPosition &_val)
    {
        return !(hash == _val.hash && x == _val.x && y == _val.y && z == _val.z); 
    }
};

struct CellData
{
    HashPosition hash;

    GridData *subGrid;
    GridData *mainGrid;

    CellData *nextCell;
    CellData *mainCell;

    bool activeSubGrid;

    Iterator *beginFreeIterators;
    Iterator *begin;
};

#endif