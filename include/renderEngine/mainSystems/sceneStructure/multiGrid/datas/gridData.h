#ifndef GRID_DATA_H
#define GRID_DATA_H

#include <MTH/vectors.h>
#include "cellData.h"

struct GridData
{
    CellData *mainCell;

    int countCells;
    CellData **hashCells;
    CellData **additionalCells;

    int countActiveCells;
};

#endif