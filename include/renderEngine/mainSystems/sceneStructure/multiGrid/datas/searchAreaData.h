#ifndef SEARCH_AREA_DATA_H
#define SEARCH_AREA_DATA_H

#include <MTH/bounds.h>
#include <MTH/boundAABB.h>
#include <MTH/boundOBB.h>
#include <MTH/boundSphere.h>
#include <MTH/plane.h>

#include "enumsMultiGrid.h"

struct SearchAreaData
{
    SearchAreaType searchAreaType;
    struct
    {
        mth::boundAABB aabb;
        mth::boundOBB obb;
        mth::boundSphere sphere;
    } bound;

    int maskTypeObjects;

    int countClippingPlanes;
    mth::plane *clippingPlanes;
};

#endif