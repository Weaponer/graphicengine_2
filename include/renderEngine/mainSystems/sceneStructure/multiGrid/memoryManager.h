#ifndef MEMORY_MANAGER_H
#define MEMORY_MANAGER_H

#include <stack>
#include <queue>

#include <MTH/vectors.h>

#include "datas/cellData.h"
#include "datas/gridData.h"
#include "datas/iterator.h"

class MemoryManager
{
private:
    std::stack<GridData *> _freeGrids;
    Iterator *_freeIterators;
    CellData *freeCells;
    std::priority_queue<std::pair<unsigned int, std::pair<CellData **, CellData **>>> _freeCellArrays;

public:
    MemoryManager();

    ~MemoryManager();
    
    GridData *getGrid();

    void freeGrid(GridData *_grid);

    void resizeGrid(GridData *_grid, unsigned int _minimumNewSize);

    void addFreeIterationsCell(CellData *_cell, int _countIterations);

private:
    CellData *getFreeCell();

    void returnCell(CellData *_cell);

    void fillDefaultCell(CellData *_cell);

    void clearCell(CellData *_cell);

    void returnIterator(Iterator *_iterator);

    Iterator *getFreeIterator();
};

#endif