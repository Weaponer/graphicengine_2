#ifndef ENUMS_MULTI_GRID_H
#define ENUMS_MULTI_GRID_H

enum BoundObjectType
{
    AABB_BOUND_OBJECT = 0,
    SPHEARE_BOUND_OBJECT = 1,
};

enum SearchAreaType
{
    AABB_SEARCH_BOUND = 0,
    OBB_SEARCH_BOUND = 1,
    SPHERE_SEARCH_BOUND = 2,
};

#endif