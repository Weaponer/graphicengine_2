#ifndef POINT_LIGHT_GE_H
#define POINT_LIGHT_GE_H

#include <MTH/vectors.h>
#include <MTH/quaternion.h>
#include <MTH/matrix.h>

#include "entity.h"
#include "multiGrid/datas/objectData.h"
#include "renderEngine/graphicsPipeline/lightRender/shadow/shadowCubeFrameBuffer.h"

struct PointLightGE : Entity
{
    ObjectData objectGridData;

    mth::vec3 position;

    mth::vec3 color;

    float range;
    float intensivity;

    bool createShadows;
    ShadowCubeFrameBuffer *shadowCubeFrameBuffer;
};

#endif