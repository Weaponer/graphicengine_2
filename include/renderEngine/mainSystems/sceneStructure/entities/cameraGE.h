#ifndef CAMERA_GE_H //Graphic entity
#define CAMERA_GE_H

#include <MTH/matrix.h>
#include <MTH/vectors.h>
#include <MTH/quaternion.h>
#include <MTH/plane.h>

#include <array>

#include "entity.h"

struct CameraGE : Entity
{
    std::array<mth::vec3, 8> pointsPiramidClipping;
    std::array<mth::plane, 6> clippingPlanes;

    mth::mat4 matrixPojection;
    mth::mat4 inverseMatrixPojection;

    mth::quat rotate;
    mth::vec3 position;

    float nearPlane;
    float farPlane;
    
    float halfHeight;
    float halfWidth;

    bool enable;
};

#endif