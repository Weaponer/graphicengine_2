#ifndef DENSITY_VOLUME_GE_H
#define DENSITY_VOLUME_GE_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <MTH/vectors.h>
#include <MTH/matrix.h>
#include <MTH/quaternion.h>
#include <MTH/color.h>
#include <MTH/glGen_matrix.h>
#include <MTH/boundAABB.h>

#include "entity.h"
#include "multiGrid/datas/objectData.h"

struct DensityVolumeGE : Entity
{
    enum DENSITY_VOLUME_GE_TYPE
    {
        DENSITY_VOLUME_GE_TYPE_CUBE = 0,
        DENSITY_VOLUME_GE_TYPE_SPHERE = 1,
        DENSITY_VOLUME_GE_TYPE_CYLINDER = 2,
    };

    enum DENSITY_VOLUME_GE_PLACEMENT_PLANE
    {
        DENSITY_VOLUME_GE_PLACEMENT_PLANE_XY = 0,
        DENSITY_VOLUME_GE_PLACEMENT_PLANE_XZ = 1,
        DENSITY_VOLUME_GE_PLACEMENT_PLANE_YZ = 2,
    };

    struct DensityVolumeData
    {
        mth::mat4 toLocalSpace;
        mth::vec4 density_pow_startD_endD;
        mth::col color;
        int typeVolume_PPV_PPG[3];
        float edgeFade;
    };

    GLuint uboDensityVolumeData;

    ObjectData objectGridData;

    mth::boundAABB bound;

    mth::quat rotate;
    mth::vec3 position;
    mth::vec3 scale;

    DENSITY_VOLUME_GE_TYPE typeVolume;
    DENSITY_VOLUME_GE_PLACEMENT_PLANE placementPlaneVolume;
    float density;

    mth::col colorVolume;

    float powGradient;
    DENSITY_VOLUME_GE_PLACEMENT_PLANE placementPlaneGradient;

    float startDecay;
    float endDecay;

    float edgeFade;

    bool wasUpdate;

    DensityVolumeGE();

    ~DensityVolumeGE();

    void updateUBO();

    void setWasUpdate();

    bool isWasUpdate() const;
};

#endif