#ifndef DEBUG_RENDERER_GE_H
#define DEBUG_RENDERER_GE_H

#include <MTH/vectors.h>
#include <MTH/matrix.h>
#include <transform.h>
#include <array>

#include "entity.h"

#define CODE_DEBUG_RENDERER_COLOR 0
#define CODE_DEBUG_RENDERER_START_POS 1
#define CODE_DEBUG_RENDERER_END_POS 2

#define DEBUG_RENDERER_STACK_SIZE 1024

struct DebugRendererGE : Entity
{
public:
    struct _vector_data
    {
        mth::vec3 vec;
        char code;
    };

private:
    mth::mat4 mxTransforms;

    std::array<_vector_data, DEBUG_RENDERER_STACK_SIZE> dates;

    size_t currentPos;

public:
    DebugRendererGE();

    ~DebugRendererGE();

    void setGlobalMatrix();

    void setLocalMatrix(const mth::mat4 &matrix);

    void setMatrixFromTransform(const Transform &trasform);

    const mth::mat4 &getMatrix() { return mxTransforms; }

    size_t getCountDates() { return currentPos + 1; }

    const _vector_data &getVectorData(int index);

    void clear();

private:
    bool isThereEnoughSpace(size_t count);

public:
    bool setColor(mth::vec3 color);

    bool drawCube(mth::vec3 pos, mth::vec3 size);

    bool drawLine(mth::vec3 start, mth::vec3 end, bool _useMatetric = true);

    bool drawRect(mth::vec3 pos, mth::vec2 size);
};

#endif