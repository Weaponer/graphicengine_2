#ifndef MESH_RENDER_GE_H
#define MESH_RENDER_GE_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <MTH/vectors.h>
#include <MTH/matrix.h>
#include <MTH/quaternion.h>

#include "entity.h"
#include "multiGrid/datas/objectData.h"

#include "renderEngine/resources/mesh.h"
#include "renderEngine/resources/material/material.h"

struct MeshRenderGE : Entity
{
    struct MatricesData
    {
        mth::mat4 globalMat;
        mth::mat4 invGlobalMat;
    };

    struct LOD
    {
        Material *materials;
        Mesh *mesh;
        float minimalHeightCoef;
    };

    ObjectData objectGridData;

    MatricesData matrices;

    mth::quat rotate;
    mth::vec3 position;
    mth::vec3 scale;

    mth::boundAABB bound;

    Mesh *mesh;
    Material *material;

    std::vector<LOD> lods;

    bool staticRender;
    
    bool generateShadows;

    GLuint bufferMatricesData;

    MeshRenderGE();

    ~MeshRenderGE();

    void matricesUpdated();

    void changedTypeRender();

    int getLodPerCoef(float _heightCoef) const;

    std::pair<Material *, Mesh *> getLod(int _lod) const;

    float getHeightCoefLod(int _lod) const;

    int countLods() const;

    void removeLod(int _index);

    void addLod();

    void setMaterialInLod(int _lod, Material *_material);

    void setMeshInLod(int _lod, Mesh *_mesh);

    void setHeightCoefInLod(int _lod, float _heightCoef);
};

#endif