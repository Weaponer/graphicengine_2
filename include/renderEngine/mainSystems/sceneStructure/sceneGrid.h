#ifndef SCENE_GRID_H
#define SCENE_GRID_H

#include "entities/entity.h"
#include "entities/typeGE.h"

#include "entities/meshRenderGE.h"
#include "entities/pointLightGE.h"
#include "entities/densityVolumeGE.h"

#include "multiGrid/gridController.h"
#include "multiGrid/enumsMultiGrid.h"

#include "renderEngine/settings/graphicConfig.h"

class SceneGrid
{
private:
    struct OrientationData
    {
        mth::vec3 position;
        mth::vec3 boundData;
        BoundObjectType boundType;
    };

private:
    GridController gridController;

public:
    SceneGrid(GraphicConfig *_graphicConfig);

    ~SceneGrid();

    bool checkTypeEntityHaveGridLocation(TypeGraphicEntity _type) const;

    void addEntity(Entity *_entity);

    void removeEntity(Entity *_entity);

    void changeOrientation(Entity *_entity);

    unsigned int getObjectsInSearchArea(SearchAreaData _searchAreaData, unsigned int _maxCount, ObjectData **_objects);

private:
    OrientationData getOrientationData(Entity *_entity, ObjectData **_objectDataRef);
};

#endif