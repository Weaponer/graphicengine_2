#ifndef MAIN_SYSTEMS_H
#define MAIN_SYSTEMS_H

#include "renderEngine/settings/settingsMainSystems.h"
#include "renderEngine/settings/settingsGraphicPipeline.h"

#include "mainFunctions/mainFunctions.h"
#include "renderEngine/resources/shader/shadersStorage.h"
#include "programActivator/programActivator.h"
#include "sceneStructure/sceneStructure.h"

class MainSystems
{
private:
    SettingsMainSystems *settings;
    SettingsGraphicPipeline *settingsGraphicPipeline;
    MainFunctions mainFunctions;
    ShadersStorage shaderStorage;
    ProgramActivator programActivator;
    SceneStructure sceneStructure;

public:
    MainSystems(SettingsMainSystems *_settings, SettingsGraphicPipeline *_settingsGraphicPipeline);

    ~MainSystems();

    void update(float _deltaTime);

    MainFunctions *getMainFunctions();

    ShadersStorage *getShadersStorage();

    ProgramActivator *getProgramActivator();

    SceneStructure *getSceneStructure();

    TechnicalShaderStorage *getTechnicalShaderStorage() const;
    
    ModelLightShaderStorage *getModelLightStorage() const;

    ResolutionSettings getResolutionSettings() const;

    SettingsMainSystems *getSettingsMainSystems();

    SettingsGraphicPipeline *getSettingsGraphicPipeline();
};

#endif