#ifndef MATRICES_DATA_H
#define MATRICES_DATA_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <MTH/matrix.h>

struct CameraMatricesData
{
    mth::mat4 matrix_V;
    mth::mat4 matrix_P;
    mth::mat4 matrix_VP;
    mth::mat4 matrixInverse_P;
    mth::mat4 matrixInverse_V;

    mth::vec3 position;
    float near_plane;
    mth::vec3 direction;
    float far_plane;
};

struct CameraMatricesDataUBO
{
    CameraMatricesData data;
    GLuint indexUBOBuffer;

    CameraMatricesDataUBO();

    CameraMatricesDataUBO(GLenum _typeMemory);

    ~CameraMatricesDataUBO();

    void update();
};

#endif