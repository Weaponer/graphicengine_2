#ifndef GRAPHIC_OPERATIONS_H
#define GRAPHIC_OPERATIONS_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include "renderEngine/resources/technicalShader/technicalShaderStorage.h"
#include "renderEngine/resources/technicalShader/tshader.h"
#include "renderEngine/resources/technicalShader/technicalProgramPipeline.h"
#include "cameraVertexesData.h"

#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"
#include "renderEngine/mainFrameBuffers/lightBasicFrameBuffer.h"
#include "renderEngine/mainFrameBuffers/transparentFrameBuffer.h"
#include "renderEngine/graphicsPipeline/lightRender/shadow/shadowFrameBuffer.h"
#include "renderEngine/graphicsPipeline/lightRender/shadow/shadowCubeFrameBuffer.h"

class GraphicOperations
{
private:
    TechnicalShaderStorage *techincalShaderStorage;
    CameraVertexesData *mainCameraVertexesData;

private:
    TechnicalProgramPipeline blitPipeline;
    TechnicalProgramPipeline blitStencilPipeline;
    TechnicalProgramPipeline blitDepthPipeline;
    TechnicalProgramPipeline blitLightPipeline;
    TechnicalProgramPipeline blitShadowPipeline;
    TechnicalProgramPipeline blitCubeShadowPipeline;
    TechnicalProgramPipeline blitTransparentPipeline;

public:
    GraphicOperations(TechnicalShaderStorage *_techincalShaderStorage, CameraVertexesData *_mainCameraVertexesData);

    ~GraphicOperations();

    void blitToContext(GLuint _sourceTexture);

    void blitToTexture(ResolutionSettings _sourceSize, GLuint _sourceTexture, ColorFrameBuffer *_colorFrameBuffer);

    void blitToTexture(ResolutionSettings _sourceSize, ResolutionSettings _destSize, GLuint _sourceTexture, GLuint framebuffer);
    
    void blitStencil(ResolutionSettings _sourceSize, GLuint _sourceTexture, ColorFrameBuffer *_colorFrameBuffer);

    void blitDepthToTexture(ResolutionSettings _sourceSize, MainDepthBuffer *_mainDepthBuffer, ColorFrameBuffer *_colorFrameBuffer);

    void blitLitght(ResolutionSettings _sourceSize, LightBasicFrameBuffer *_lightFrameBuffer, ColorFrameBuffer *_colorFrameBuffer);

    void blitShadow(ResolutionSettings _sourceSize, ShadowFrameBuffer *_shadowFrameBuffer, ColorFrameBuffer *_colorFrameBuffer);

    void blitTransparent(ResolutionSettings _sourceSize, TransparantFrameBuffer *_source, ColorFrameBuffer *_colorFrameBuffer);

    void blitCubeShadow(ResolutionSettings _sourceSize, ShadowCubeFrameBuffer *_shadowCubeFrameBuffer, ColorFrameBuffer *_colorFrameBuffer);
};

#endif