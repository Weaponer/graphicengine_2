#ifndef SETTINGS_RENDER_QUEUE_H
#define SETTINGS_RENDER_QUEUE_H

#include <MTH/boundOBB.h>

#include "renderEngine/resources/shader/enums.h"
#include "renderEngine/mainSystems/sceneStructure/entities/cameraGE.h"

struct SettingsRenderQueue
{
    bool checkCamera;
    CameraGE *camera;
    struct SettingsArea
    {
        mth::boundOBB bound;
        int countClippingPlanes;
        mth::plane *clippingPlanes;
    } searchArea;

    CameraGE *cameraForCheckLod;
    bool useLodGroups;
    float lodBias;

    bool getPointLights;
    bool getDensityVolumes;
    bool sort;
    bool generateInstancing;
    int maskPasses;
    int maskQueuesRender;
    int maskRenderTypes;
};

#endif