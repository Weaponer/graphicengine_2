#ifndef RENDER_QUEUE_H
#define RENDER_QUEUE_H

#include "settingsRenderQueue.h"
#include "renderEngine/resources/shader/enums.h"
#include "renderEngine/resources/shader/pass.h"
#include "renderEngine/resources/shader/program.h"
#include "renderEngine/resources/mesh.h"
#include "renderEngine/resources/material/material.h"

#include "renderEngine/mainSystems/sceneStructure/entities/pointLightGE.h"
#include "renderEngine/mainSystems/sceneStructure/entities/densityVolumeGE.h"
#include "renderEngine/settings/specialDefines.h"

struct DrawCall
{
    Shader *shader;
    RenderType renderType;
    QueueRender typeQueueRender;
    Pass *pass;
    int numVertexShader;
    int numFragmentShader;
    GLuint pipeline;

    Material *material;

    Mesh *mesh;
    GLuint uboMatricesModel;

    uint countInstances;
    DrawCall *nextDrawCall;
};

struct RenderQueue
{
    SettingsRenderQueue settings;
    std::array<DrawCall *, COUNT_TYPE_PASSES> beginDrawCall;
    std::array<DrawCall *, COUNT_TYPE_PASSES> endDrawCall;

    struct PointLights
    {
        int countPointLights;
        std::array<PointLightGE *, MAX_COUNT_POINT_LIGHTS> pointLights;
    };

    PointLights *pointLightsData;

    struct DensityVolumes
    {
        int countDensityVolumes;
        std::array<DensityVolumeGE *, MAX_COUNT_DENSITY_VOLUMES> densityVolumes;
    };

    DensityVolumes *densityVolumesData;
};

#endif