#ifndef RENDER_QUEUE_GENERATOR_H
#define RENDER_QUEUE_GENERATOR_H

#include <array>

#include "paterns/stack/easyStack.h"

#include "settingsRenderQueue.h"
#include "renderQueue.h"

#include "renderEngine/mainSystems/sceneStructure/sceneStructure.h"
#include "renderEngine/mainSystems/sceneStructure/entityRegister.h"

#include "renderEngine/mainSystems/sceneStructure/entities/meshRenderGE.h"

#include "renderEngine/mainSystems/sceneStructure/multiGrid/datas/objectData.h"
#include "renderEngine/settings/specialDefines.h"

class RenderQueueGenerator
{
private:
    struct PreDrawCall
    {
        Shader *shader;
        Pass *pass;
        int numPass;
        int numVertProgram;
        int numFragProgram;

        Material *material;
        Mesh *mesh;
        GLuint uboMatrices;
        MeshRenderGE::MatricesData *matricesData;

        float distance;
    };

private:
    int countObjects;
    std::array<ObjectData *, MAX_COUNT_OBJECTS_PER_QUEUE> bufferObjectDatas;

    int countMeshRenderers;
    std::array<MeshRenderGE *, MAX_COUNT_OBJECTS_PER_QUEUE> bufferMeshRenderes;
    std::array<std::pair<Material *, Mesh *>, MAX_COUNT_DRAW_CALLS_PER_TYPE_PASS> bufferUseMeshRenderesData;

    int countPointLights;
    std::array<PointLightGE *, MAX_COUNT_POINT_LIGHTS> bufferPointLights;

    int countDensityVolumes;
    std::array<DensityVolumeGE *, MAX_COUNT_DENSITY_VOLUMES> bufferDensityVolumes;

    int countUseShaders;
    std::array<Shader *, MAX_COUNT_SHADERS_PER_QUEUE> bufferShaders;
    int countUsePasses;
    int countUseMaterials;
    int countUseMeshs;
    std::array<int, MAX_COUNT_SHADER_PASSES_PER_QUEUE> bufferIndexes;

    int countUsePreDrawCalls;
    std::array<PreDrawCall, MAX_COUNT_DRAW_CALLS_PER_TYPE_PASS> bufferPreDrawCalls;
    std::array<std::pair<unsigned long, unsigned long>, MAX_COUNT_DRAW_CALLS_PER_TYPE_PASS> bufferValueDrawCalls;

    std::array<MeshRenderGE::MatricesData *, MAX_COUNT_INSTANCE_OBJECTS> bufferReferencesOnMatrices;
    std::array<MeshRenderGE::MatricesData, MAX_COUNT_INSTANCE_OBJECTS> bufferMatricesForInstancing;

    EasyStack<GLuint> freeBuffersInstancings;

    struct AdditionalBuffersObjectsIterator
    {
        int typeObject;
        void *buffer;
        AdditionalBuffersObjectsIterator *next;
    };

    AdditionalBuffersObjectsIterator *freeABOIterators;
    AdditionalBuffersObjectsIterator *freeABO;

    DrawCall *freeDrawCalls;

    SceneStructure *sceneStructure;
    SceneGrid *sceneGrid;

public:
    RenderQueueGenerator(SceneStructure *_sceneStructure);

    ~RenderQueueGenerator();

    RenderQueue generateRenderQueue(SettingsRenderQueue _settings);

    RenderQueue generateRenderQueue(SettingsRenderQueue _newSettings, RenderQueue _oldRenderQueue);

    void releaseRenderQueue(RenderQueue _renderQueue);

private:
    GLuint getBufferInstancing();

    void returnBufferInstancing(GLuint _indexBuffer);

    void returnDrawCall(DrawCall *_drawCall);

    inline DrawCall *getDrawCall();

    inline void *getFreeAdditionalBufferObjects(int _typeObjects);

    void returnAdditionalBufferObjects(int _typeObjects, void *_buffer);

private:
    void fillTagsPassesShader(Shader *_shader, TypePass _type);

    void clearTagsPassesShader(Shader *_shader, TypePass _type);

    void fillBuffer(SettingsRenderQueue _settings);

    int getLodPerSettings(SettingsRenderQueue &_settings, MeshRenderGE *_meshRender);

    void fillConcreteTypePass(RenderQueue &_renderQueue, TypePass _type, int _indexBegin);

    inline std::pair<unsigned long, unsigned long> getForcePreDrawCall(PreDrawCall &_preDrawCall, int _indexPos);
};

#endif