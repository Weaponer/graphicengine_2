#ifndef MAIN_FUNCTIONS_H
#define MAIN_FUNCTIONS_H

#include "basicCamera.h"
#include "cameraVertexesData.h"
#include "graphicOperations.h"
#include "renderEngine/resources/technicalShader/technicalShaderStorage.h"
#include "sceneStructure/sceneStructure.h"
#include "sceneStructure/entityRegister.h"
#include "renderEngine/settings/settingsGraphicPipeline.h"
#include "renderQueueGenerator.h"

class MainFunctions
{
private:
    BasicCamera basicCamera;
    CameraVertexesData cameraVertexesData;
    GraphicOperations graphicOperations;
    RenderQueueGenerator *renderQueueGenerator;

    SettingsRenderQueue mainSettingsRenderQueue;
    RenderQueue mainRenderQueue;

    TechnicalShaderStorage *technicalShaderStorage;
    SceneStructure *sceneStructure;
    EntityRegister *entityRegister;
    SettingsGraphicPipeline *settingsGraphicPipeline;

    float deltaTime;
    float allTime;

public:
    MainFunctions(TechnicalShaderStorage *_technicalShaderStorage, SceneStructure *_sceneStructure, SettingsGraphicPipeline *_settingsGraphicPipeline);

    ~MainFunctions();

    void update(float _deltaTime);

    BasicCamera *getBasicCamera();

    CameraVertexesData *getCameraVertexesData();

    GraphicOperations *getGraphicOperations();

    RenderQueueGenerator *getRenderQueueGenerator();

    RenderQueue *getMainRenderQueue();

    TechnicalShaderStorage *getTechnicalShaderStorage();

    SettingsGraphicPipeline *getSettingsGraphicPipeline();

    float getDeltaTime() const;

    float getAllTime() const;
};

#endif