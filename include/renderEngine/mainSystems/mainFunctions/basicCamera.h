#ifndef BASIC_CAMERA_H
#define BASIC_CAMERA_H

#include "cameraVertexesData.h"
#include "sceneStructure/entities/cameraGE.h"

class BasicCamera
{
private:
    CameraGE *mainCamera;
    CameraMatricesData matriciesMainCamera;

public:
    BasicCamera();

    ~BasicCamera();

    void update(CameraGE *_currentCamera);

    CameraGE *getMainCamera() const;

    const CameraMatricesData *getMatriciesMainCamera() const;
};

#endif