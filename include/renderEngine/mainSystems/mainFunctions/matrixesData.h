#ifndef MATRIXES_DATA_H
#define MATRIXES_DATA_H

#include <MTH/matrix.h>

struct MatricesData
{
    mth::mat4 matrix_M;
    mth::mat4 matrix_MV;
    mth::mat4 matrix_V;
    mth::mat4 matrix_P;
    mth::mat4 matrix_VP;
    mth::mat4 matrix_MVP;
    mth::mat4 matrixInverse_P;
    mth::mat4 matrixInverse_V;
    mth::mat4 matrixInverse_M;

    float near_plane;
    float far_plane;
};

#endif