#ifndef CAMERA_VERTEXES_DATA_H
#define CAMERA_VERTEXES_DATA_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>

#include <MTH/matrix.h>
#include <MTH/vectors.h>

#include <array>

#include "renderEngine/mainSystems/sceneStructure/entities/cameraGE.h"
#include "cameraMatricesData.h"

class CameraVertexesData
{
private:
    GLuint bufferData;
    GLuint vertexArray;
    CameraMatricesDataUBO matricesUBO;

    std::array<mth::vec2, 4> points;
    std::array<mth::vec2, 4> uvs;
    std::array<mth::vec3, 4> normals;

public:
    CameraVertexesData();

    ~CameraVertexesData();

    void enableVertexBuffers();

    void disableVertexBuffers();

    void enableUniformBlockData(GLuint _index);

    void setDataNormals(const std::array<mth::vec3, 4> &_normals);

    void setDataNormals(const CameraGE *_cameraGE);

    void setUBOMatrices(const CameraMatricesData *_matrices);

    CameraMatricesDataUBO *getUBOMatrices();

    void callDraw();

    void callDrawInstance(int _count);
};

#endif