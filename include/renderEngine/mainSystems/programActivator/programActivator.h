#ifndef PROGRAM_ACTIVATOR_H
#define PROGRAM_ACTIVATOR_H

#include <MTH/matrix.h>

#include "renderEngine/resources/material/material.h"
#include "renderEngine/resources/mesh.h"
#include "renderEngine/resources/shader/enums.h"
#include "renderEngine/resources/shader/shader.h"
#include "renderEngine/resources/shader/properties/intProperty.h"
#include "renderEngine/resources/shader/properties/floatProperty.h"
#include "renderEngine/resources/shader/properties/vectorProperty.h"
#include "renderEngine/resources/shader/properties/colorProperty.h"
#include "renderEngine/resources/shader/properties/textureProperty.h"
#include "renderEngine/resources/shader/properties/cubeMapProperty.h"

#include "renderEngine/resources/shader/shaderPropertiesData/propertiesRegistry.h"
#include "internalPropertiesActivator.h"
#include "renderEngine/resources/shader/uniformBlocksRegistry.h"

#include "renderEngine/mainSystems/mainFunctions/renderQueue.h"
#include "renderEngine/mainSystems/mainFunctions/cameraMatricesData.h"

#include "renderEngine/graphicsPipeline/datas/skyRenderData.h"

#include "programActivatorDatas.h"

#define MATRICES_CAMERA_UNIFORM_BLOCK_N "matricesCamera"
#define MATRICES_OBJECT_UNIFORM_BLOCK_N "matricesObject"
#define SKY_DATA_UNIFORM_BLOCK_N "skyData"
#define ALL_POINT_LIHGT_UNIFORM_BLOCK_N "allPointsLightsDataBlock"

class GraphicEngine;
class GraphicPipeline;
class MainFunctions;

typedef void(ProgramActivator::*FucntionEnableUBO)(int);

class ProgramActivator
{
private:
    PropertiesRegistry *registryProperty;
    InternalPropertiesActivator defaultProperties;
    GraphicPipeline *gprahicPipeline;
    MainFunctions *mainFunctions;

private:
    UniformBlocksRegistry *uniformBlocksRegistry;
    int indexBindingMatricesCamera;
    int indexBindingMatricesObject;

    std::array<void (ProgramActivator::*)(const Property *, Program *), COUNT_PROPERTIES> functionProperties;
    std::vector<FucntionEnableUBO> functionsEnableUBO;

public:
    ProgramActivator();

    ~ProgramActivator();

    void setRegistryProperty(PropertiesRegistry *_registryProperty);

    void setGraphicPipeline(GraphicPipeline *_gprahicPipeline);

    void setMainFunctions(MainFunctions *_mainFunctions);

    MainFunctions *getMainFunctions();

    void setUniformBlocksRegistry(UniformBlocksRegistry *_uniformBlocksRegistry);

    void drawRenderQueue(RenderQueue _renderQueue, CameraMatricesDataUBO *_cameraMatricesDataUBO, SettingsDrawRenderQueue _settings);

    void drawCall(Material *_material, Mesh *_mesh, TypePass _typePass, int _numPass);

    void enableProgramInMaterial(Material *_material, TypePass _typePass, int _numPass, GLuint *_pipeline);

private:
    void enableDefaultPass(Pass *_pass, Shader *_shader, Material *_material);

    void updateMaterialForDefaultPass(Pass *_pass, Material *_material);

    void fillProgramsProperties(Material *_material, Pass *_pass, Program **_programs, int _countPrograms);

    void pushInternalProperty(Pass *_pass, Program *_program, int _indexProperty);

private:
    const Property *getCorrectProperty(const Property *_localProperty);

    void pushIntProperty(const Property *_property, Program *_program);

    void pushFloatProperty(const Property *_property, Program *_program);

    void pushToggleProperty(const Property *_property, Program *_program);

    void pushVectorProperty(const Property *_property, Program *_program);

    void pushColorProperty(const Property *_property, Program *_program);

    void pushTextureProperty(const Property *_property, Program *_program);

    void pushCubeMapProperty(const Property *_property, Program *_program);

private:
    void enableBufferForBaseColor();

private:
    void enableUseUnifromBlocks(Program *_program);

    void enableUBOMatricesCamera(int _bindingIndex);

    void enableUBOMatricesObject(int _bindingIndex);

    void enableUBOSkyData(int _bindingIndex);

    void enableUBOAllPointLight(int _bindingIndex);
};

#endif