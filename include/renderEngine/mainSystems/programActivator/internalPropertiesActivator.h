#ifndef INTERNAL_PROPERTIES_ACTIVATOR_H
#define INTERNAL_PROPERTIES_ACTIVATOR_H

#define GL_GLEXT_PROTOTYPES

#include<GL/gl.h>
#include<GL/glu.h>

#include <vector>
#include <string>
#include <tuple>

#include "mainFunctions/matrixesData.h"
#include "renderEngine/resources/shader/enums.h"
#include "renderEngine/resources/shader/pass.h"
#include "renderEngine/resources/shader/program.h"
#include "renderEngine/resources/shader/shaderPropertiesData/propertiesRegistry.h"

#include "programActivatorDatas.h"


class InternalPropertiesActivator;
class ProgramActivator;
class GraphicEngine;
class GraphicPipeline;

typedef void(InternalPropertiesActivator::*SettingFunction)(Pass *, Program *, int);

typedef std::tuple<std::string, SettingFunction, std::vector<RenderStage>> DefaultProperty;

typedef std::pair<SettingFunction, std::vector<RenderStage>> SettingFunctionDefine;

class InternalPropertiesActivator
{
private:
    RenderStage renderStage;

    std::vector<DefaultProperty> properties;
    std::vector<SettingFunctionDefine> settingFunctions;

    ProgramActivator *programActivator;
    MatricesData *matricesData;
    GraphicPipeline *graphicPipeline;

public:
    InternalPropertiesActivator();

    ~InternalPropertiesActivator();

    void initPropertiesRegistry(ProgramActivator *_programActivator, PropertiesRegistry *_registry);

    void setGraphicPipeline(GraphicPipeline *_graphicPipeline);

    void setRenderStage(RenderStage _renderStage);

    RenderStage getRenderStage() const;

    void pushInternalProperty(Pass *_pass, Program *_program, int _indexProperty);

private:
    void pushTime(Pass *_pass, Program *_program, int _indexProperty);

    void pushEnvironmentBuffer(Pass *_pass, Program *_program, int _indexProperty);

    void pushDepthBuffer(Pass *_pass, Program *_program, int _indexProperty);

    void pushNormalBuffer(Pass *_pass, Program *_program, int _indexProperty);

    void pushViewSize(Pass *_pass, Program *_program, int _indexProperty);

    void pushModelLight(Pass *_pass, Program *_program, int _indexProperty);
};

#endif