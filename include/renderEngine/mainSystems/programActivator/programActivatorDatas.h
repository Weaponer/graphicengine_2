#ifndef PROGRAM_ACTIVATOR_DATAS_H
#define PROGRAM_ACTIVATOR_DATAS_H

#include "renderEngine/resources/shader/enums.h"

enum RenderStage
{
    RS_DEPTH_STAGE = 0,
    RS_NORMAL_STAGE = 1,
    RS_NORMAL_DEPTH_STAGE = 2,
    RS_BASIC_COLOR_STAGE = 3,
    RS_SHADOW_STAGE = 4,
    RS_SURFACE_STAGE = 5,
};

struct SettingsDrawRenderQueue
{
    RenderStage renderStage;
    TypePass typePass;
    int maskRenderTypes;
    int maskQueueRender;

    bool useInstancing;
};


#endif