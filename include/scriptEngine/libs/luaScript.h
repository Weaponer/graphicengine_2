#ifndef LUA_SCRIPT_H
#define LUA_SCRIPT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

class LuaVector
{
public:
    static void luaLoadVector(lua_State *L);

    static void pushLuaScript(lua_State *L);

private:
    static int start(lua_State *L);

    static int update(lua_State *L);

    static int lateUpdate(lua_State *L);

    static int destroy(lua_State *L);
};

#endif