#ifndef LUA_COLOR_H
#define LUA_COLOR_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include <MTH/color.h>
#include <MTH/vectors.h>

#define COLOR_METADATA "ColorMeta"
#define CHECK_COLOR(L, P) (mth::col*)luaL_checkudata(L, P, COLOR_METADATA)

class LuaColor
{
public:
    static void luaLoadColor(lua_State *L);

    static void pushColor(lua_State *L, const mth::col &col);

private:
    static int newColor(lua_State *L);
    
    static int normalized(lua_State *L);

    static int white(lua_State *L);

    static int black(lua_State *L);

    static int red(lua_State *L);

    static int green(lua_State *L);
    
    static int blue(lua_State *L);

    static int cyan(lua_State *L);
    
    static int gray(lua_State *L);

    static int magenta(lua_State *L);
    
    static int yellow(lua_State *L);

private:
    static int setColor(lua_State *L);

    static int setColorByte(lua_State *L);

    static int toHSVA(lua_State *L);

    static int setHSVA(lua_State *L);

    static int r(lua_State *L);

    static int g(lua_State *L);

    static int b(lua_State *L);

    static int a(lua_State *L);

    static int setR(lua_State *L);

    static int setG(lua_State *L);

    static int setB(lua_State *L);

    static int setA(lua_State *L);

    static int add(lua_State *L);

    static int sub(lua_State *L);

    static int mul(lua_State *L);

    static int div(lua_State *L);

    static int toString(lua_State *L);

    static int normalize(lua_State *L);

    static int equality(lua_State *L);

};

#endif