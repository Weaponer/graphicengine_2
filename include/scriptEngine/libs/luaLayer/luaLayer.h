#ifndef LUA_LAYER_H
#define LUA_LAYER_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "layerManager/layer.h"
#include "layerManager/layerManager.h"

#define LAYER_NAME "Layer"
#define LAYER_METADATA "LayerMeta"
#define CHECK_LAYER(L, P) *((Layer *)luaL_checkudata(L, P, LAYER_METADATA))

class LuaLayer
{
public:
    static void luaLoadLayer(lua_State *L);

    static void pushLayer(lua_State *L, Layer layer);

private:
    static int name(lua_State *L);

    static int number(lua_State *L);

    static int tostring(lua_State *L);

private:
    static int layerByName(lua_State *L);

    static int layerByNumber(lua_State *L);

    static int defaultLayer(lua_State *L);

};

#endif