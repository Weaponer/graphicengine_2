#ifndef LUA_SCENE_H
#define LUA_SCENE_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "engineBase/sandboxGameObjects/scene.h"

#define SCENE_METADATA "SceneMeta"
#define CHECK_SCENE(L, P) (Scene *)(((luaObject *)luaL_checkudata(L, P, SCENE_METADATA))->object)

class LuaScene
{
public:
    static void luaLoadScene(lua_State *L);

    static int getScene(lua_State *L, Scene *_scene);

    static void pushScene(lua_State *L, Scene *_scene);

private:
    static int name(lua_State *L);

    static int setName(lua_State *L);

};
#endif