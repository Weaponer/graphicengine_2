#ifndef LUA_SCENE_MANAGER_H
#define LUA_SCENE_MANAGER_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h> 
#include <lua5.2/lualib.h>

#include "engineBase/sandboxGameObjects/scene.h"
#include "engineBase/sandboxGameObjects/sceneManager.h"

class LuaSceneManager
{
public:
    static void luaLoadSceneManager(lua_State *L);

private:
    static int createScene(lua_State *L);

    static int removeScene(lua_State *L);

    static int getCurrentScene(lua_State *L);

    static int getSceneByName(lua_State *L);

    static int setSceneDefault(lua_State *L);

};
#endif