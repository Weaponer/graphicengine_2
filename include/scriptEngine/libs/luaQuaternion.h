#ifndef LUA_QUATERNION_H
#define LUA_QUATERNION_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "gameObject/gameObject.h"
#include "MTH/quaternion.h"

#define QUATERNION_METADATA "QuaternionMeta"
#define CHECK_QUATERNION(L) (mth::quat*)luaL_checkudata(L, 1, QUATERNION_METADATA)

class LuaQuaternion
{
public:
    static void luaLoadQuaternion(lua_State *L);

public:
    static void pushQuaternion(lua_State *L, const mth::quat &quat);

private:
    static int newQuaternion(lua_State *L);

    static int setDirect(lua_State *L);

    static int setMatrix(lua_State *L);

    static int setEuler(lua_State *L);

    static int getEuler(lua_State *L);

    static int getMatrix(lua_State *L);

    static int magnitude(lua_State *L);

    static int squareMagnitude(lua_State *L);

    static int normalize(lua_State *L);

    static int inverse(lua_State *L);

    static int mul(lua_State *L);

    static int equality(lua_State *L);

    static int addScaledVector(lua_State *L);

    static int rotateByVector(lua_State *L);

    static int rotateVector(lua_State *L);

    static int x(lua_State *L);

    static int y(lua_State *L);

    static int z(lua_State *L);

    static int w(lua_State *L);

    static int setX(lua_State *L);

    static int setY(lua_State *L);

    static int setZ(lua_State *L);

    static int setW(lua_State *L);

    static int toString(lua_State *L);
};

#endif