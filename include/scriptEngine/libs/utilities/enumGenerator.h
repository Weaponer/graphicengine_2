#ifndef ENUM_GENERATOR_H
#define ENUM_GENERATOR_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "basicFunctions.h"

template <typename T>
class EnumGenerator
{
private:
    const char *currentName;
    const char *itemMetadata;

private:
    static int toStringItem(lua_State *L)
    {
        T *val =  (T *)lua_touserdata(L, 1);
        lua_getfield(L, -1, "__enum");
        const char *enumName = lua_tostring(L, -1);
        lua_remove(L, -1);
        lua_getglobal(L, enumName);
        lua_rawgeti(L, -1, *val);
        lua_remove(L, -2);
        return 1;
    }

public:
    void startNewEnum(lua_State *L, const char *currentName, const char *itemMetadata)
    {
        this->currentName = currentName;
        this->itemMetadata = itemMetadata;

        luaL_newmetatable(L, itemMetadata);
        lua_pushvalue(L, -1);
        lua_setfield(L, -2, "__index");
        lua_pushfstring(L, "%s", currentName);
        lua_setfield(L, -2, "__enum");
        luaL_Reg itemFunc[] =
            {
                {"__tostring", EnumGenerator<T>::toStringItem},
                {NULL, NULL}};
        luaL_setfuncs(L, itemFunc, 0);
        lua_remove(L, -1);

        luaL_Reg enumFunc[] =
            {
                {NULL, NULL}};
        luaL_newlib(L, enumFunc);
    }

    void addParameter(lua_State *L, const T &value, const char* name)
    {
        T *v = (T *)lua_newuserdata(L, sizeof(T));
        *v = value;
        luaL_getmetatable(L, itemMetadata);
        lua_setmetatable(L, -2);
        lua_setfield(L, -2, name);

        lua_pushfstring(L, "%s.%s", currentName, name);
        lua_rawseti(L, -2, *v);
    }

    void endEnum(lua_State *L)
    {
        lua_setglobal(L, currentName);
    }
};

#endif