#ifndef BASIC_FUNCTIONS_H
#define BASIC_FUNCTIONS_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "object.h"
#include "scriptEngine/base/scriptEngine.h"


class BasicFunctions
{
public:
    static ScriptEngine *getScriptEngine(lua_State *L);

    static int getLuaObject(lua_State *L, const char *metadata, Object *object);

    template <typename T>
    static T *pushTObject(lua_State *L, const char *metadata, const T& val)
    {
        T *v = (T *)lua_newuserdata(L, sizeof(T));
        *v = val;
        luaL_getmetatable(L, metadata);
        lua_setmetatable(L, -2);
        return v;
    }

    static void pushVoidObject(lua_State *L, const char *metadata, void *object);

    static void createLibUserdata(lua_State *L, const char *metadata, luaL_Reg *functions);

    static void createStaticLib(lua_State *L, const char *name, luaL_Reg *functions);

    static void createSubStaticLib(lua_State *L, const char *baseLib, const char *name, luaL_Reg *functions);

    static Object *checkLuaObject(lua_State *L, int _inx, const char *_metadata);

    static Object *checkLuaObjectOrNil(lua_State *L, int _inx, const char *_metadata);

    static void pushCompareLuaObjects(lua_State *L, int _inx_1, int _inx_2, const char *_metadata);

    static Object *checkLuaObject(lua_State *L, int _inx);

    static void pushCompareLuaObjects(lua_State *L, int _inx_1, int _inx_2);
};

#endif