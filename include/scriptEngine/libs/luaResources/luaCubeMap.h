#ifndef LUA_CUBE_MAP_H
#define LUA_CUBE_MAP_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "renderEngine/resources/cubeMap.h"

#define CUBE_MAP_METADATA "CubeMapMeta"
#define CHECK_CUBE_MAP(L, P) ((CubeMap *)BasicFunctions::checkLuaObject(L, P, CUBE_MAP_METADATA))
#define CHECK_CUBE_MAP_OR_NULL(L, P) ((CubeMap *)BasicFunctions::checkLuaObjectOrNil(L, P, CUBE_MAP_METADATA))


class LuaCubeMap
{
public:
    static void luaLoadCubeMap(lua_State *L);

    static int getCubeMap(lua_State *L, CubeMap *cubeMap);

    static void pushCubeMap(lua_State *L, CubeMap *_cubeMap);

private:
    static int tostring(lua_State *L);

};

#endif