#ifndef LUA_RESOURCE_ASYNC_HANDLER_H
#define LUA_RESOURCE_ASYNC_HANDLER_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "resourceLoader/resourceAsyncHandler.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/utilities/enumGenerator.h"

#define STATUS_LOAD_ENUM "StatusLoad"
#define STATUS_LOAD_ITEM_METADATA "StatusLoadItemMeta"

#define RESOURCE_ASYNC_HANDLER_METADATA "ResourceAsyncHandlerMeta"
#define CHECK_RESOURCE_ASYNC_HANDLER(L, P) ((ResourceAsyncHandler *)BasicFunctions::checkLuaObject(L, P, RESOURCE_ASYNC_HANDLER_METADATA))


class LuaResourceAsyncHandler
{
public:
    static void luaLoadResourceAsyncHandler(lua_State *L);

    static void pushResourceAsyncHandler(lua_State *L, ResourceAsyncHandler *_data);

    static int getResourceAsyncHandler(lua_State *L, ResourceAsyncHandler *_data);

    static void pushStatusLoad(lua_State *L, StatusLoad _status);

private:
    static int path(lua_State *L);

    static int status(lua_State *L);

    static int type(lua_State *L);

    static int resource(lua_State *L);

    static int discard(lua_State *L);

    static int isDiscard(lua_State *L);

    static int tostring(lua_State *L);

};

#endif