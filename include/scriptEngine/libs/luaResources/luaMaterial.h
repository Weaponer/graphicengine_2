#ifndef LUA_MATERIAL_H
#define LUA_MATERIAL_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "renderEngine/resources/material/material.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

#define MATERIAL_METADATA "MaterialMeta"
#define CHECK_MATERIAL(L, P) ((Material *)BasicFunctions::checkLuaObject(L, P, MATERIAL_METADATA))
#define CHECK_MATERIAL_OR_NULL(L, P) ((Material *)BasicFunctions::checkLuaObjectOrNil(L, P, MATERIAL_METADATA))


class LuaMaterial
{
public:
    static void luaLoadMaterial(lua_State *L);

    static int getMaterial(lua_State *L, Material *model);

private:
    static int name(lua_State *L);

    static int setName(lua_State *L);

    static int shader(lua_State *L);

    static int setInt(lua_State *L);

    static int setFloat(lua_State *L);

    static int setToggle(lua_State *L);

    static int setVector(lua_State *L);

    static int setColor(lua_State *L);

    static int setTexture(lua_State *L);

    static int setCubeMap(lua_State *L);

    static int getInt(lua_State *L);

    static int getFloat(lua_State *L);

    static int getToggle(lua_State *L);

    static int getVector(lua_State *L);

    static int getColor(lua_State *L);

    static int getTexture(lua_State *L);

    static int getCubeMap(lua_State *L);

    static int generateInstancing(lua_State *L);

    static int setGenerateInstancing(lua_State *L);

    static int tostring(lua_State *L);
    
};

#endif