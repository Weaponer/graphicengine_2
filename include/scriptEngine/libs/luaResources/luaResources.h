#ifndef LUA_RESOURCES_H
#define LUA_RESOURCES_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "resourceLoader/typeResources.h"
#include "resourceLoader/resourceLoader.h"

#include "scriptEngine/libs/luaResources/luaModel.h"
#include "scriptEngine/libs/luaResources/luaMesh.h"
#include "scriptEngine/libs/luaResources/luaMaterial.h"
#include "scriptEngine/libs/luaResources/luaTexture.h"
#include "scriptEngine/libs/luaResources/luaCubeMap.h"
#include "scriptEngine/libs/luaResources/luaShader.h"

#define RESOURCE_ENUM "Resource"
#define RESOURCE_ITEM_METADATA "ResourceItemMeta"
#define CHECK_RESOURCE(L, P) (int *)luaL_checkudata(L, P, RESOURCE_ITEM_METADATA)


class LuaResources
{
private:
    struct dataLuaResource
    {
        const char *metadata;
        const char *name;
        uint code;
        int (*functGetUData)(lua_State *L, Object *obj);
    };

    static dataLuaResource data[];

public:
    static void luaLoadResources(lua_State *L);

    static void pushTypeResource(lua_State *L, TypeResource type);

    static int pushResource(lua_State *L, Object *_object, TypeResource _type);

private:
    static int checkCode(int code);

    static int getModel(lua_State *L, Object *obj);

    static int getMaterial(lua_State *L, Object *obj);

    static int getTexture(lua_State *L, Object *obj);

    static int getCubeMap(lua_State *L, Object *obj);

    static int getShader(lua_State *L, Object *obj);

    static int loadResource(lua_State *L);

    static int unloadResource(lua_State *L);

    static int loadAsyncResource(lua_State *L);

    static int unloadAsyncResource(lua_State *L);
};

#endif