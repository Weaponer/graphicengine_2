#ifndef LUA_MODEL_H
#define LUA_MODEL_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>


#include "renderEngine/resources/model.h"

#define MODEL_METADATA "ModelMeta"
#define CHECK_MODEL(L, P) ((Model *)BasicFunctions::checkLuaObject(L, P, MODEL_METADATA))
#define CHECK_MODEL_OR_NULL(L, P) ((Model *)BasicFunctions::checkLuaObjectOrNil(L, P, MODEL_METADATA))


class LuaModel
{
public:
    static void luaLoadModel(lua_State *L);

    static int getModel(lua_State *L, Model *model);

private:
    static int countMeshes(lua_State *L);

    static int getMeshOnIndex(lua_State *L);

    static int getMeshOnName(lua_State *L);

    static int toString(lua_State *L);

};

#endif