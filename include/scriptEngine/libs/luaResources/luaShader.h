#ifndef LUA_SHADER_H
#define LUA_SHADER_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "renderEngine/resources/shader/shader.h"

#define SHADER_STATIC_LIB "Shader"
#define SHADER_METADATA "ShaderMeta"
#define CHECK_SHADER(L, P) ((Shader *)BasicFunctions::checkLuaObject(L, P, SHADER_METADATA))
#define CHECK_SHADER_OR_NULL(L, P) ((Shader *)BasicFunctions::checkLuaObjectOrNil(L, P, SHADER_METADATA))

class LuaShader
{
public:
    static void luaLoadShader(lua_State *L);

    static int getShader(lua_State *L, Shader *_shader);

    static void pushShader(lua_State *L, Shader *_shader);

private:
    static int find(lua_State *L);

    static int findPropertyInt(lua_State *L);

    static int findPropertyFloat(lua_State *L);

    static int findPropertyVector(lua_State *L);

    static int findPropertyColor(lua_State *L);

    static int findPropertyTexture(lua_State *L);

    static int findPropertyCubeMap(lua_State *L);

    static int setUseGlobalProperty(lua_State *L);

    static int useGlobalProperty(lua_State *L);

    static int globalPropertyInt(lua_State *L);

    static int globalPropertyFloat(lua_State *L);

    static int globalPropertyVector(lua_State *L);

    static int globalPropertyColor(lua_State *L);

    static int globalPropertyTexture(lua_State *L);

    static int globalPropertyCubeMap(lua_State *L);

    static int setGlobalPropertyInt(lua_State *L);

    static int setGlobalPropertyFloat(lua_State *L);

    static int setGlobalPropertyVector(lua_State *L);

    static int setGlobalPropertyColor(lua_State *L);

    static int setGlobalPropertyTexture(lua_State *L);

    static int setGlobalPropertyCubeMap(lua_State *L);

    static int nameProperty(lua_State *L);

private:
    static int getCodeProperty(lua_State *L);

private:
    static int name(lua_State *L);

    static int countProperties(lua_State *L);

    static int haveProperty(lua_State *L);

    static int tostring(lua_State *L);
};

#endif