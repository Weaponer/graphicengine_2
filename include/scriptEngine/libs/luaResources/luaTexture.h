#ifndef LUA_TEXTURE_H
#define LUA_TEXTURE_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "renderEngine/resources/texture.h"

#define TEXTURE_METADATA "TextureMeta"
#define CHECK_TEXTURE(L, P) ((Texture *)BasicFunctions::checkLuaObject(L, P, TEXTURE_METADATA))
#define CHECK_TEXTURE_OR_NULL(L, P) ((Texture *)BasicFunctions::checkLuaObjectOrNil(L, P, TEXTURE_METADATA))


class LuaTexture
{
public:
    static void luaLoadTexture(lua_State *L);

    static int getTexture(lua_State *L, Texture *texture);

    static void pushTexture(lua_State *L, Texture *texture);

private:
    static int tostring(lua_State *L);

};

#endif