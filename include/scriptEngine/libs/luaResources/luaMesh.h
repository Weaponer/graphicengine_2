#ifndef LUA_MESH_H
#define LUA_MESH_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "renderEngine/resources/mesh.h"
#include "scriptEngine/base/scriptEngine.h"

#define MESH_METADATA "MeshMeta"
#define CHECK_MESH(L) (*((Mesh **)luaL_checkudata(L, 1, MESH_METADATA)))


class LuaMesh
{
public:
    static void luaLoadMesh(lua_State *L);

    static void pushMesh(lua_State *L, Mesh *mesh);

private:
    static int name(lua_State *L);

    static int countIndexes(lua_State *L);

    static int eq(lua_State *L);
};

#endif