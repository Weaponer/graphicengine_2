#ifndef LUA_VECTOR_H
#define LUA_VECTOR_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "MTH/vectors.h"

#define VECTOR_4_METADATA "Vector4Meta"
#define CHECK_VECTOR_4(L, P) (mth::vec4*)luaL_checkudata(L, P, VECTOR_4_METADATA)

#define VECTOR_3_METADATA "Vector3Meta"
#define CHECK_VECTOR_3(L, P) (mth::vec3*)luaL_checkudata(L, P, VECTOR_3_METADATA)

#define VECTOR_2_METADATA "Vector2Meta"
#define CHECK_VECTOR_2(L, P) (mth::vec2*)luaL_checkudata(L, P, VECTOR_2_METADATA)


class LuaVector
{
public:
    static void luaLoadVector(lua_State *L);

public:
    static void pushVector4(lua_State *L, const mth::vec4 &vec);

    static void pushVector3(lua_State *L, const mth::vec3 &vec);

    static void pushVector2(lua_State *L, const mth::vec2 &vec);

private:
    static int newVector4(lua_State *L);
    
    static int dotVector4(lua_State *L);

    static int normalizedVector4(lua_State *L);

    static int x4(lua_State *L);

    static int y4(lua_State *L);

    static int z4(lua_State *L);

    static int w4(lua_State *L);

    static int setX4(lua_State *L);

    static int setY4(lua_State *L);

    static int setZ4(lua_State *L);

    static int setW4(lua_State *L);

    static int addVector4(lua_State *L);

    static int subVector4(lua_State *L);

    static int mulVector4(lua_State *L);

    static int divVector4(lua_State *L);

    static int toString4(lua_State *L);

    static int magnitude4(lua_State *L);

    static int normalize4(lua_State *L);

    static int equality4(lua_State *L);

private:
    static int newVector3(lua_State *L);
    
    static int dotVector3(lua_State *L);

    static int crossVector3(lua_State *L);

    static int normalizedVector3(lua_State *L);

    static int x3(lua_State *L);

    static int y3(lua_State *L);

    static int z3(lua_State *L);

    static int setX3(lua_State *L);

    static int setY3(lua_State *L);

    static int setZ3(lua_State *L);

    static int addVector3(lua_State *L);

    static int subVector3(lua_State *L);

    static int mulVector3(lua_State *L);

    static int divVector3(lua_State *L);

    static int toString3(lua_State *L);

    static int magnitude3(lua_State *L);

    static int normalize3(lua_State *L);

    static int equality3(lua_State *L);

private:
    static int newVector2(lua_State *L);
    
    static int dotVector2(lua_State *L);

    static int perpendecular2(lua_State *L);

    static int normalizedVector2(lua_State *L);

    static int x2(lua_State *L);

    static int y2(lua_State *L);

    static int z2(lua_State *L);

    static int setX2(lua_State *L);

    static int setY2(lua_State *L);

    static int setZ2(lua_State *L);

    static int addVector2(lua_State *L);

    static int subVector2(lua_State *L);

    static int mulVector2(lua_State *L);

    static int divVector2(lua_State *L);

    static int toString2(lua_State *L);

    static int magnitude2(lua_State *L);

    static int normalize2(lua_State *L);

    static int equality2(lua_State *L);
};

#endif