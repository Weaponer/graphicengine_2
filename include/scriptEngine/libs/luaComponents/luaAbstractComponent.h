#ifndef LUA_ABSTRACT_COMPONENT_H
#define LUA_ABSTRACT_COMPONENT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "sandboxGameObjects/scene.h"
#include "engineBase/sandboxGameObjects/sandboxGameObjects.h"


#define GET_ABSTRACT_COMPONENT(L, P) ((Component *)BasicFunctions::checkLuaObject(L, P))

class LuaAbstractComponent
{
public:
    static void luaLoadAllComponents(lua_State *L);

private:
    static int gameObject(lua_State *L);

    static int discard(lua_State *L);

    static int active(lua_State *L);

    static int localActive(lua_State *L);

    static int setActive(lua_State *L);
    
};


#endif