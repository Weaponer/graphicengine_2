#ifndef LUA_BOX_COLLIDER_COMPONENT_H
#define LUA_BOX_COLLIDER_COMPONENT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "gameObject/component/component.h"
#include "gameObject/physicsComponents/boxColliderComponent.h"

#define BOX_COLLIDER_COMPONENT_METADATA "BoxColliderComponentMeta"
#define CHECK_BOX_COLLIDER_COMPONENT(L, P) ((BoxColliderComponent *)BasicFunctions::checkLuaObject(L, P, BOX_COLLIDER_COMPONENT_METADATA))

class LuaBoxColliderComponent
{
public:
    static void luaLoadBoxColliderComponent(lua_State *L, int size, const luaL_Reg *functions);

    static void pushBoxColliderComponent(lua_State *L, Component *component);

    static Component *addComponent(lua_State *L, GameObject *_gameObject);

private:
    static int toString(lua_State *L);

    static int size(lua_State *L);

    static int setSize(lua_State *L);

};
#endif