#ifndef LUA_SPHERE_COLLIDER_COMPONENT_H
#define LUA_SPHERE_COLLIDER_COMPONENT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "gameObject/component/component.h"
#include "gameObject/physicsComponents/sphereColliderComponent.h"

#define SPHERE_COLLIDER_COMPONENT_METADATA "SphereColliderComponentMeta"
#define CHECK_SPHERE_COLLIDER_COMPONENT(L, P) ((SphereColliderComponent *)BasicFunctions::checkLuaObject(L, P, SPHERE_COLLIDER_COMPONENT_METADATA))

class LuaSphereColliderComponent
{
public:
    static void luaLoadSphereColliderComponent(lua_State *L, int size, const luaL_Reg *functions);

    static void pushSphereColliderComponent(lua_State *L, Component *component);

    static Component *addComponent(lua_State *L, GameObject *_gameObject);

private:
    static int toString(lua_State *L);

    static int radius(lua_State *L);

    static int setRadius(lua_State *L);

};
#endif