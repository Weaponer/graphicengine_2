#ifndef LUA_RIGIDBODY_COMPONENT_H
#define LUA_RIGIDBODY_COMPONENT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "gameObject/component/component.h"
#include "gameObject/physicsComponents/rigidbodyComponent.h"

#define RIGIDBODY_COMPONENT_METADATA "RigidbodyComponentMeta"
#define CHECK_RIGIDBODY_COMPONENT(L, P) ((Rigidbody *)BasicFunctions::checkLuaObject(L, P, RIGIDBODY_COMPONENT_METADATA))

class LuaRigidbodyComponent
{
public:
    static void luaLoadRigidbodyComponent(lua_State *L, int size, const luaL_Reg *functions);

    static void pushRigidbodyComponent(lua_State *L, Component *component);

    static Component *addComponent(lua_State *L, GameObject *_gameObject);

private:
    static int toString(lua_State *L);

    static int connectCollider(lua_State *L);

    static int disconnectCollider(lua_State *L);

    static int mass(lua_State *L);

    static int setMass(lua_State *L);

    static int kinematic(lua_State *L);

    static int setKinematic(lua_State *L);

    static int addForce(lua_State *L);

    static int addForceAtPoint(lua_State *L);
    
    static int addTorque(lua_State *L);

    static int velocity(lua_State *L);

    static int angularVelocity(lua_State *L);

    static int setVelocity(lua_State *L);

    static int setAngularVelocity(lua_State *L);
};
#endif