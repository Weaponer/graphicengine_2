#ifndef LUA_ABSTRACT_COLLIDER_COMPONENT_H
#define LUA_ABSTRACT_COLLIDER_COMPONENT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "gameObject/physicsComponents/colliderComponent.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

#define GET_ABSTRACT_COLLIDER_COMPONENT(L, P) ((ColliderComponent *)BasicFunctions::checkLuaObject(L, P))

class LuaAbstractColliderComponent
{
public:
    static void luaLoadAllAbstractCollidersComponents(lua_State *L, int size, const luaL_Reg *functions);

private:
    static int origin(lua_State *L);

    static int setOrigin(lua_State *L);
};


#endif