#ifndef LUA_MESH_RENDERER_COMPONENT_H
#define LUA_MESH_RENDERER_COMPONENT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "gameObject/component/component.h"
#include "gameObject/graphicComponents/meshRenderer.h"

#define MESH_RENDERER_COMPONENT_METADATA "MeshRendererComponentMeta"
#define CHECK_MESH_RENDERER_COMPONENT(L, P) ((MeshRenderer *)BasicFunctions::checkLuaObject(L, P, MESH_RENDERER_COMPONENT_METADATA))

class LuaMeshRendererComponent
{
public:
    static void luaLoadMeshRendererComponent(lua_State *L, int size, const luaL_Reg *functions);

    static void pushMeshRendererComponent(lua_State *L, Component *component);

    static Component *addComponent(lua_State *L, GameObject *_gameObject);

private:
    static int toString(lua_State *L);

    static int mesh(lua_State *L);

    static int setMesh(lua_State *L);

    static int material(lua_State *L);

    static int setMaterial(lua_State *L);

    static int countLods(lua_State *L);

    static int addLod(lua_State *L);
    
    static int removeLod(lua_State *L);

    static int meshInLod(lua_State *L);

    static int materialInLod(lua_State *L);
    
    static int heightCoefInLod(lua_State *L);

    static int setMeshInLod(lua_State *L);

    static int setMaterialInLod(lua_State *L);
    
    static int setHeightCoefInLod(lua_State *L);

    static int generateShadows(lua_State *L);
    
    static int setGenerateShadows(lua_State *L);

};
#endif