#ifndef LUA_POINT_LIGHT_COMPONENT_H
#define LUA_POINT_LIGHT_COMPONENT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "gameObject/component/component.h"
#include "gameObject/graphicComponents/lightPoint.h"

#define POINT_LIGHT_COMPONENT_METADATA "PointLightComponentMeta"
#define CHECK_POINT_LIGHT_COMPONENT(L, P) ((LightPoint *)BasicFunctions::checkLuaObject(L, P, POINT_LIGHT_COMPONENT_METADATA))

class LuaPointLightComponent
{
public:
    static void luaLoadPointLightComponent(lua_State *L, int size, const luaL_Reg *functions);

    static void pushPointLightComponent(lua_State *L, Component *component);

    static Component *addComponent(lua_State *L, GameObject *_gameObject);

private:
    static int toString(lua_State *L);

    static int range(lua_State *L);

    static int setRange(lua_State *L);

    static int intensivity(lua_State *L);

    static int setIntensivity(lua_State *L);

    static int isCreateShadow(lua_State *L);

    static int setIsCreateShadow(lua_State *L);

    static int color(lua_State *L);

    static int setColor(lua_State *L);
};
#endif