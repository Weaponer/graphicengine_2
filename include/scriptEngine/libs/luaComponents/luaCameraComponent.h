#ifndef LUA_CAMERA_COMPONENT_H
#define LUA_CAMERA_COMPONENT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "gameObject/component/component.h"
#include "gameObject/graphicComponents/camera.h"

#define CAMERA_COMPONENT_METADATA "CameraComponentMeta"
#define CHECK_CAMERA_COMPONENT(L, P) ((Camera *)BasicFunctions::checkLuaObject(L, P, CAMERA_COMPONENT_METADATA))

class LuaCameraComponent
{
public:
    static void luaLoadCameraComponent(lua_State *L, int size, const luaL_Reg *functions);

    static void pushCameraComponent(lua_State *L, Component *component);

    static Component *addComponent(lua_State *L, GameObject *_gameObject);

private:
    static int toString(lua_State *L);

    static int setOrtographicProjection(lua_State *L);

    static int setPerspectiveProjection(lua_State *L);

    static int projectionMatrix(lua_State *L);
    
};
#endif