#ifndef LUA_DENSITY_VOLUME_COMPONENT_H
#define LUA_DENSITY_VOLUME_COMPONENT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "gameObject/component/component.h"
#include "gameObject/graphicComponents/densityVolume.h"

#define DENSITY_VOLUME_COMPONENT_METADATA "DensityVolumeComponentMeta"
#define CHECK_DENSITY_VOLUME_COMPONENT(L, P) ((DensityVolume *)BasicFunctions::checkLuaObject(L, P, DENSITY_VOLUME_COMPONENT_METADATA))

#define ENUM_DENSITY_VOLUME_TYPE "DensityVolumeType"
#define ENUM_DENSITY_VOLUME_TYPE_ITEM_METADATA "DensityVolumeTypeItemMeta"
#define CHECK_ENUM_DENSITY_VOLUME_TYPE_ITEM(L, P) ((int *)luaL_checkudata(L, P, ENUM_DENSITY_VOLUME_TYPE_ITEM_METADATA))

#define ENUM_DENSITY_VOLUME_PLACEMENT "DensityVolumePlacement"
#define ENUM_DENSITY_VOLUME_PLACEMENT_ITEM_METADATA "DensityVolumePlacementItemMeta"
#define CHECK_ENUM_DENSITY_VOLUME_PLACEMENT_ITEM(L, P) ((int *)luaL_checkudata(L, P, ENUM_DENSITY_VOLUME_PLACEMENT_ITEM_METADATA))

class LuaDensityVolumeComponent
{
public:
    static void luaLoadDensityVolumeComponent(lua_State *L, int size, const luaL_Reg *functions);

    static void pushDensityVolumeComponent(lua_State *L, Component *component);

    static Component *addComponent(lua_State *L, GameObject *_gameObject);

private:
    static void pushDensityVolumeType(lua_State *L, DensityVolume::DENSITY_VOLUME_TYPE _densityVolumeType);

    static void pushDensityVolumePlacement(lua_State *L, DensityVolume::DENSITY_VOLUME_PLACEMENT_PLANE _densityVolumePlacement);

    static int toString(lua_State *L);

    static int setDensityVolumeType(lua_State *L);

    static int densityVolumeType(lua_State *L);

    static int setDensityVolumePlacementPlane(lua_State *L);

    static int densityVolumePlacementPlane(lua_State *L);

    static int setDensity(lua_State *L);

    static int density(lua_State *L);

    static int setColorVolume(lua_State *L);

    static int colorVolume(lua_State *L);

    static int setPowGradient(lua_State *L);

    static int powGradient(lua_State *L);

    static int setPlacementPlaneGradient(lua_State *L);

    static int placementPlaneGradient(lua_State *L);

    static int setStartDecay(lua_State *L);

    static int startDecay(lua_State *L);

    static int setEndDecay(lua_State *L);

    static int endDecay(lua_State *L);

    static int setEdgeFade(lua_State *L);

    static int edgeFade(lua_State *L);
};
#endif