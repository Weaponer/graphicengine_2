#ifndef LUA_SCRIPT_COMPONENT_H
#define LUA_SCRIPT_COMPONENT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "gameObject/component/component.h"
#include "gameObject/scriptComponents/scriptComponent.h"

#define SCRIPT_COMPONENT_METADATA "ScriptComponentMeta"
#define CHECK_SCRIPT_COMPONENT(L, P) ((ScriptComponent *)BasicFunctions::checkLuaObject(L, P, SCRIPT_COMPONENT_METADATA))

class LuaScriptComponent
{
public:
    static void luaLoadScriptComponent(lua_State *L, int size, const luaL_Reg *functions);

    static void pushScripComponent(lua_State *L, Component *component);

    static Component *addComponent(lua_State *L, GameObject *_gameObject);
    
private:
    static int setType(lua_State *L);

    static int toString(lua_State *L);
};


#endif