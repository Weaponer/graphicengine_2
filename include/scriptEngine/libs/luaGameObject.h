#ifndef LUA_GAME_OBJECT_H
#define LUA_GAME_OBJECT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "gameObject/gameObject.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

#define GAME_OBJECT_METADATA "GameObjectMeta"
#define CHECK_GAME_OBJECT(L, P) ((GameObject *)BasicFunctions::checkLuaObject(L, P, GAME_OBJECT_METADATA))

#define ENUM_COMPONENT "Component"
#define COMPONENT_ITEM_METADATA "ComponentItemMeta"
#define CHECK_COMPONENT_ITEM(L, P) ((int *)luaL_checkudata(L, P, COMPONENT_ITEM_METADATA))

class ScriptEngine;

class LuaGameObject
{
private:
    static struct componentsTypes
    {
        const char *metadata;
        int code;
        Component *(*addComponent)(lua_State *L, GameObject *object);
        Component *(*getComponent)(GameObject *object);
        int (*getComponents)(GameObject *object, std::vector<Component *> *vector);
        void (*pushComponent)(lua_State *L, Component *component);
    } components[];

public:
    static void luaLoadGameObject(lua_State *L);

    static int getGameObject(lua_State *L, GameObject *osb);

    static void pushGameObject(lua_State *L, GameObject *osb);

private:
    static int getComponent(lua_State *L);

    static int getComponents(lua_State *L);

    static int addComponent(lua_State *L);

    template <typename T>
    static Component *getComponent(GameObject *object)
    {
        return object->getComponent<T>();
    }

    template<typename T>
    static int getComponents(GameObject *object, std::vector<Component *> *vector)
    {
        int size = object->getComponents()->size();
        vector->reserve(size);
        Component **first = &((*vector)[0]);
        return object->getComponents<T>(first, size);
    }

private:
    static int create(lua_State *L);

    static int destroy(lua_State *L);

    static int makeDontDestroy(lua_State *L);

    static int moveOnAnotherScene(lua_State *L);

private:
    static int getName(lua_State *L);

    static int setName(lua_State *L);

    static int active(lua_State *L);

    static int localActive(lua_State *L);

    static int setActive(lua_State *L);

    static int pos(lua_State *L);

    static int setPos(lua_State *L);

    static int localPos(lua_State *L);

    static int setLocalPos(lua_State *L);

    static int rotate(lua_State *L);

    static int rotateE(lua_State *L);

    static int setRotate(lua_State *L);

    static int setRotateE(lua_State *L);

    static int localRotate(lua_State *L);

    static int localRotateE(lua_State *L);

    static int setLocalRotate(lua_State *L);

    static int setLocalRotateE(lua_State *L);

    static int localScale(lua_State *L);

    static int setLocalScale(lua_State *L);

    static int localMatrix(lua_State *L);

    static int globalMatrix(lua_State *L);

    static int parent(lua_State *L);

    static int setParent(lua_State *L);

    static int countChildren(lua_State *L);

    static int getChild(lua_State *L);

    static int layer(lua_State *L);

    static int setLayer(lua_State *L);

    static int tostring(lua_State *L);
};
#endif