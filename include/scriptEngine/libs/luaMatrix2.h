#ifndef LUA_MATRIX_2_H
#define LUA_MATRIX_2_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include <MTH/matrix.h>
#include <MTH/vectors.h>

#define MATRIX_2_METADATA "Matrix2Meta"
#define CHECK_MATRIX_2(L) (mth::mat2 *)luaL_checkudata(L, 1, MATRIX_2_METADATA)

class LuaMatrix2
{
public:
    static void luaLoadMatrix2(lua_State *L);

public:
    static void pushMatrix2(lua_State *L, const mth::mat2 &mat);

private:
    static int newMatrix2(lua_State *L);

    static int setOrientation(lua_State *L);

    static int transpose(lua_State *L);

    static int setSingleMatrix(lua_State *L);

    static int transform(lua_State *L);

    static int transformTranspose(lua_State *L);

    static int inverse(lua_State *L);

    static int inverseRotate(lua_State *L);

    static int getVector(lua_State *L);

    static int getVectorTranspose(lua_State *L);

    static int getRadian(lua_State *L);

    static int add(lua_State *L);

    static int sub(lua_State *L);

    static int mul(lua_State *L);

    static int div(lua_State *L);

    static int get(lua_State *L);

    static int set(lua_State *L);

    static int toString(lua_State *L);
};

#endif