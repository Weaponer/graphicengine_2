#ifndef LUA_INPUT_H
#define LUA_INPUT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>


class LuaInput
{
public:
    static void luaLoadInput(lua_State *L);

    static int mouse(lua_State *L);

    static int keyboard(lua_State *L);
    
};

#endif