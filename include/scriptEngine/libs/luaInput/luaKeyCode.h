#ifndef LUA_KEY_CODE_H
#define LUA_KEY_CODE_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include <SDL2/SDL_scancode.h>

#define KEY_CODE_ENUM "KeyCode"
#define KEY_CODE_METADATA "KeyCodeMeta"
#define CHECK_KEY_CODE(L, P) (Uint16 *)luaL_checkudata(L, P, KEY_CODE_METADATA)

class LuaKeyCode
{
public:
    static void luaLoadKeyCode(lua_State *L);

};

#endif