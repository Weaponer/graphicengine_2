#ifndef LUA_KEYBOARD_H
#define LUA_KEYBOARD_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "window/keyboard.h"
#include "luaKeyCode.h"

#define KEYBOARD_METADATA "KeyboardMeta"
#define CHECK_KEYBOARD(L, P) (Keyboard **)luaL_checkudata(L, P, KEYBOARD_METADATA)


class LuaKeyboard
{
public:
    static void luaLoadKeyboard(lua_State *L);

    static void luaPushKeyboard(lua_State *L, Keyboard *keyboard);

private:
    static int getButton(lua_State *L);

    static int getButtonDown(lua_State *L);

    static int getButtonUp(lua_State *L);

    static int toString(lua_State *L);

    static int eq(lua_State *L);
};


#endif