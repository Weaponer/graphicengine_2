#ifndef LUA_MOUSE_H
#define LUA_MOUSE_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include <SDL2/SDL_mouse.h>
#include "window/mouse.h"

#define MOUSE_METADATA "MouseMeta"
#define CHECK_MOUSE(L, P) (Mouse **)luaL_checkudata(L, P, MOUSE_METADATA)


#define MOUSE_BUTTONS_ENUM "MouseButtons"
#define MOUSE_BUTTON_METADATA "MouseButtonMeta"
#define CHECK_MOUSE_BUTTON(L, P) (Uint8 *)luaL_checkudata(L, P, MOUSE_BUTTON_METADATA)

class LuaMouse
{
public:
    static void luaLoadMouse(lua_State *L);

    static void luaPushMouse(lua_State *L, Mouse *mouse);

private:
    static void luaLoadMouseButtons(lua_State *L);

private:
    static int position(lua_State *L);

    static int deltaPosition(lua_State *L);

    static int getButton(lua_State *L);

    static int getButtonDown(lua_State *L);

    static int getButtonUp(lua_State *L);

    static int toString(lua_State *L);

    static int eq(lua_State *L);
};

#endif