#ifndef lUA_DEBUG_H
#define lUA_DEBUG_H


#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "debugOutput/debugOutput.h"
#include "debugOutput/threadDebugOutput.h"

#define DEBUG_TABLE "Debug"

class LuaDebug
{
public:
    static void luaLoadDebug(lua_State *L);

private:
    static int drawLine(lua_State *L);

    static int drawCube(lua_State *L);

    static int drawRect(lua_State *L);

    static int log(lua_State *L);

    static int logWarning(lua_State *L);

    static int logError(lua_State *L);

};

#endif