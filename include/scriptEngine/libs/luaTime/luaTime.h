#ifndef LUA_TIME_H
#define LUA_TIME_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#define TIME_TABLE "Time"

class LuaTime
{
public:
    static void luaLoadTime(lua_State *L);

private:
    static int deltaTime(lua_State *L);
    
    static int lock(lua_State *L);

    static int setLock(lua_State *L);
};

#endif