#ifndef LUA_MATRIX_4_H
#define LUA_MATRIX_4_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include <MTH/matrix.h>
#include <MTH/vectors.h>

#define MATRIX_4_METADATA "Matrix4Meta"
#define CHECK_MATRIX_4(L) (mth::mat4 *)luaL_checkudata(L, 1, MATRIX_4_METADATA)

class LuaMatrix4
{
public:
    static void luaLoadMatrix4(lua_State *L);

public:
    static void pushMatrix4(lua_State *L, const mth::mat4 &mat);

private:
    static int newMatrix4(lua_State *L);

    static int matrix3FromMatrix4(lua_State *L);
    
private:
    static int transpose(lua_State *L);

    static int setSingleMatrix(lua_State *L);

    static int transform(lua_State *L);

    static int transformTranspose(lua_State *L);

    static int transformNoMove(lua_State *L);

    static int transformTransposeNoMove(lua_State *L);

    static int add(lua_State *L);

    static int sub(lua_State *L);

    static int mul(lua_State *L);

    static int div(lua_State *L);

    static int get(lua_State *L);

    static int set(lua_State *L);

    static int toString(lua_State *L);
};

#endif