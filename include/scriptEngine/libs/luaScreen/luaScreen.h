#ifndef LUA_SCREEN_H
#define LUA_SCREEN_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#define SCREEN_TABLE "Screen"


class LuaScreen
{
public:
    static void luaLoadScreen(lua_State *L);

private:
    static int width(lua_State *L);
    
    static int height(lua_State *L);

    static int mouseLock(lua_State *L);

    static int setMouseLock(lua_State *L);
};

#endif