#ifndef LUA_ARRAY_BOOL_H
#define LUA_ARRAY_BOOL_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#define ARRAY_BOOL_TABLE "ArrayBool"
#define ARRAY_BOOL_METADATA "ArrayBoolMeta"
#define CHECK_ARRAY_BOOL(L, P) (void *)luaL_checkudata(L, P, ARRAY_BOOL_METADATA)

class LuaArrayBool
{
public:
    static void luaLoadArrayBool(lua_State *L);

public:
    static int newArrayBool(lua_State *L);

    static int get(lua_State *L);

    static int set(lua_State *L);

    static int length(lua_State *L);

    static int toString(lua_State *L);

    static int equality(lua_State *L);
};

#endif