#ifndef LUA_ARRAY_FLOAT_H
#define LUA_ARRAY_FLOAT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#define ARRAY_FLOAT_TABLE "ArrayFloat"
#define ARRAY_FLOAT_METADATA "ArrayFloatMeta"
#define CHECK_ARRAY_FLOAT(L, P) (void *)luaL_checkudata(L, P, ARRAY_FLOAT_METADATA)

class LuaArrayFloat
{
public:
    static void luaLoadArrayFloat(lua_State *L);

public:
    static int newArrayFloat(lua_State *L);

    static int get(lua_State *L);

    static int set(lua_State *L);

    static int length(lua_State *L);

    static int toString(lua_State *L);

    static int equality(lua_State *L);
};

#endif