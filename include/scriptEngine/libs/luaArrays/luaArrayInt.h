#ifndef LUA_ARRAY_INT_H
#define LUA_ARRAY_INT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#define ARRAY_INT_TABLE "ArrayInt"
#define ARRAY_INT_METADATA "ArrayIntMeta"
#define CHECK_ARRAY_INT(L, P) (void *)luaL_checkudata(L, P, ARRAY_INT_METADATA)

class LuaArrayInt
{
public:
    static void luaLoadArrayInt(lua_State *L);

public:
    static int newArrayInt(lua_State *L);

    static int get(lua_State *L);

    static int set(lua_State *L);

    static int length(lua_State *L);

    static int toString(lua_State *L);

    static int equality(lua_State *L);
};

#endif