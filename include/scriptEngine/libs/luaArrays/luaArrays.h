#ifndef LUA_ARRAYS_H
#define LUA_ARRAYS_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

class LuaArrays
{
public:
    static void luaLoadArrays(lua_State *L);
};

#endif