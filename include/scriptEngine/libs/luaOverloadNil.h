#ifndef LUA_OVERLOAD_NIL_H
#define LUA_OVERLOAD_NIL_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#define NIL_METADATA "nilMetadata"
#define NIL_NAME "nil"

class LuaOverloadNil
{
public:
    static void luaOverloadNil(lua_State *L);

    static void luaMakeMetadataNil(lua_State *L, int _inx);

private:
    static int eq(lua_State *L);

    static int name(lua_State *L);

    static int tostring(lua_State *L);

    static int isValid(lua_State *L);


};

#endif