#ifndef LUA_SHADOW_DIRECTION_LIGHT_H
#define LUA_SHADOW_DIRECTION_LIGHT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "renderEngine/graphicEngine.h"
#include "renderEngine/settings/settingsGraphicPipeline.h"


#define SHADOW_SUB_TABLE "Shadow"
#define REDUCTION_TYPE_ENUM "ReductionType"
#define REDUCTION_TYPE_ITEM_METADATA "ReductionTypeItemMeta"
#define CHECK_REDUCTION_TYPE(L, P) (SDLD_ReductionType)*(int *)luaL_checkudata(L, P, REDUCTION_TYPE_ITEM_METADATA)

class LuaShadowDirectionLight
{
public:
    static void luaLoadShadowDirectionLight(lua_State *L);

private:
    static void pushReductionType(lua_State *L, SDLD_ReductionType _type);

    static SettingsGraphicPipeline *getSettingsGraphicPipeline(lua_State *L);

    static int getShadowBias(lua_State *L);

    static int setShadowBias(lua_State *L);

    static int shadowDistance(lua_State *L);

    static int setShadowDistance(lua_State *L);

    static int maxUpShadowDistance(lua_State *L);

    static int setMaxUpShadowDistance(lua_State *L);

    static int degreePartition(lua_State *L);

    static int setDegreePartition(lua_State *L);

    static int sizeSide(lua_State *L);
    
    static int setSizeSide(lua_State *L);

    static int getStartFadeDistance(lua_State *L);
    
    static int setStartFadeDistance(lua_State *L);

    static int reductionTypePartitionVolume(lua_State *L);
    
    static int setReductionTypePartitionVolume(lua_State *L);
    
    static int reductionTypeSizePartition(lua_State *L);
    
    static int setReductionTypeSizePartition(lua_State *L);
};


#endif