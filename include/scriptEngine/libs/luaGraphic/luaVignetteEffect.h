#ifndef LUA_VIGNETTE_EFFECT_H
#define LUA_VIGNETTE_EFFECT_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "renderEngine/graphicEngine.h"
#include "renderEngine/settings/settingsGraphicPipeline.h"

#define VIGNETTE_EFFECT_SUB_TABLE "VignetteEffect"

class LuaVignetteEffect
{
public:
    static void luaLoadVignetteEffect(lua_State *L);

private:
    static int setGenerateVignetteEffect(lua_State *L);

    static int generateVignetteEffect(lua_State *L);

    static int color(lua_State *L);

    static int setColor(lua_State *L);

    static int radius(lua_State *L);

    static int setRadius(lua_State *L);

    static int pow(lua_State *L);

    static int setPow(lua_State *L);
};

#endif