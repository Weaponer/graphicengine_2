#ifndef LUA_SSAO_H
#define LUA_SSAO_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "renderEngine/graphicEngine.h"
#include "renderEngine/settings/settingsGraphicPipeline.h"

#define SSAO_SUB_TABLE "SSAO"

class LuaSSAO
{
public:
    static void luaLoadSSAO(lua_State *L);

private:
    static int setGenerateSSAO(lua_State *L);

    static int generateSSAO(lua_State *L);
};

#endif