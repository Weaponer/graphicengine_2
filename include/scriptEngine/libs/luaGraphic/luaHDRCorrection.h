#ifndef LUA_HDR_CORRECTION_H
#define LUA_HDR_CORRECTION_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "renderEngine/graphicEngine.h"
#include "renderEngine/settings/settingsGraphicPipeline.h"

#define HDR_CORRECTION_SUB_TABLE "HDRCorrection"

class LuaHDRCorrection
{
public:
    static void luaLoadHDRCorrection(lua_State *L);

private:
    static int setDynamicHDRCorrection(lua_State *L);

    static int dynamicHDRCorrection(lua_State *L);

    static int staticValue(lua_State *L);

    static int setStaticValue(lua_State *L);

    static int minimumLogLuminance(lua_State *L);

    static int setMinimumLogLuminance(lua_State *L);

    static int inverseLogLuminanceRange(lua_State *L);

    static int setInverseLogLuminanceRange(lua_State *L);
};

#endif