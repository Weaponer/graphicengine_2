#ifndef LUA_BLOOM_H
#define LUA_BLOOM_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "renderEngine/graphicEngine.h"

#define BLOOM_SUB_TABLE "Bloom"

class LuaBloom
{
public:
    static void luaLoadBloom(lua_State *L);

private:
    static int generateBloom(lua_State *L);

    static int setGenerateBloom(lua_State *L);

    static int countSamples(lua_State *L);

    static int setCountSamples(lua_State *L);

    static int force(lua_State *L);

    static int setForce(lua_State *L);
};

#endif