#ifndef LUA_GRAPHIC_H
#define LUA_GRAPHIC_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "renderEngine/graphicEngine.h"

#define GRAPHIC_TABLE "Graphic"
#define SKY_SUB_TABLE "Sky"

class LuaGraphic
{
public:
    static void luaLoadGraphic(lua_State *L);


private:
    static int intensity(lua_State *L);

    static int setIntensity(lua_State *L);

    static int directionSun(lua_State *L);

    static int setDirectionSun(lua_State *L);

    static int colorSun(lua_State *L);

    static int setColorSun(lua_State *L);

    static int colorSky(lua_State *L);

    static int setColorSky(lua_State *L);

    static int colorGround(lua_State *L);

    static int setColorGround(lua_State *L);

    static int colorSunSet(lua_State *L);

    static int setColorSunSet(lua_State *L);
};


#endif