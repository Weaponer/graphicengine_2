#ifndef LUA_VOLUMETRIC_FOG_H
#define LUA_VOLUMETRIC_FOG_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "renderEngine/graphicEngine.h"
#include "renderEngine/settings/settingsGraphicPipeline.h"

#define VOLUMETRIC_FOG_SUB_TABLE "VolumetricFog"

class LuaVolumetricFog
{
public:
    static void luaLoadVolumetricFog(lua_State *L);

private:
    static SettingsGraphicPipeline *getSettingsGraphicPipeline(lua_State *L);

    static int generateFog(lua_State *L);

    static int setGenerateFog(lua_State *L);

    static int intensivityFog(lua_State *L);

    static int setIntensivityFog(lua_State *L);

    static int anisotropic(lua_State *L);

    static int setAnisotropic(lua_State *L);

    static int scattering(lua_State *L);

    static int setScattering(lua_State *L);

    static int absorption(lua_State *L);

    static int setAbsorption(lua_State *L);

    static int sizeGridFog(lua_State *L);

    static int setSizeGridFog(lua_State *L); 

    static int colorFog(lua_State *L);

    static int setColorFog(lua_State *L);

    static int startDistanceFog(lua_State *L);

    static int setStartDistanceFog(lua_State *L);

    static int endDistanceFog(lua_State *L);

    static int setEndDistanceFog(lua_State *L);

    static int usePointsLightForce(lua_State *L);

    static int setUsePointsLightForce(lua_State *L);

    static int generateRaysDirectionLight(lua_State *L);

    static int setGenerateRaysDirectionLight(lua_State *L);

    static int maxDistanceRay(lua_State *L);

    static int setMaxDistanceRay(lua_State *L);
};

#endif