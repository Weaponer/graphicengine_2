#ifndef LUA_LOD_SETTINGS_H
#define LUA_LOD_SETTINGS_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "renderEngine/graphicEngine.h"
#include "renderEngine/settings/settingsGraphicPipeline.h"

#define LOD_SETTINGS_TABLE "LODSettings"

class LuaLODSettings
{
public:
    static void luaLoadLODSettings(lua_State *L);

private:
    static int useLods(lua_State *L);

    static int setUseLods(lua_State *L);

    static int useLodsForShadow(lua_State *L);

    static int setUseLodsForShadow(lua_State *L);

    static int lodBias(lua_State *L);

    static int setLodBias(lua_State *L);

    static int lodShadowBias(lua_State *L);

    static int setLodShadowBias(lua_State *L);
};

#endif