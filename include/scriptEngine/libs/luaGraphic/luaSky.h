#ifndef LUA_SKY_H
#define LUA_SKY_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "renderEngine/graphicEngine.h"
#include "renderEngine/settings/settingsGraphicPipeline.h"

#define SKY_SUB_TABLE "Sky"
#define TYPE_UPDATE_ENUM "TypeUpdateSky"
#define TYPE_UPDATE_ITEM_METADATA "TypeUpdateSkyItemMeta"
#define CHECK_TYPE_UPDATE_SKY(L, P) (int *)luaL_checkudata(L, P, TYPE_UPDATE_ITEM_METADATA)

class LuaSky
{
public:
    static void luaLoadSky(lua_State *L);

private:
    static void pushTypeUpdateSky(lua_State *L, TypeUpdateSky _type);

    static int typeUpdateSky(lua_State *L);

    static int setTypeUpdateSky(lua_State *L);

    static int material(lua_State *L);

    static int setMaterial(lua_State *L);

    static int directionSun(lua_State *L);

    static int setDirectionSun(lua_State *L);

    static int colorSun(lua_State *L);

    static int setColorSun(lua_State *L);

    static int intensity(lua_State *L);

    static int setIntensity(lua_State *L);

    static int ambientLightColor(lua_State *L);

    static int setAmbientLightColor(lua_State *L);

    static int intensityAmbientLight(lua_State *L);

    static int setIntensityAmbientLight(lua_State *L);

    static int environmentBufferAsAmbientLight(lua_State *L);

    static int setEnvironmentBufferAsAmbientLight(lua_State *L);

    static int callUpdate(lua_State *L);
};


#endif