#ifndef SCRIPT_INSTALLER_H
#define SCRIPT_INSTALLER_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "script.h"
#include "gameObject/component/component.h"

#define INDEX_TYPE  "type"
#define INDEX_PROTOTYPE "prototype"
#define INDEX_OBJECT "object"
#define INDEX_FUNCTION_GENERATION "generator"

#define BASIC_TYPE "script"


class ScriptInstaller
{
private:
    int pointTableTypes;

public:
    void initScriptInstaller(lua_State *L);

    int instalScript(lua_State *L, Script *script);

    int pushNewInstanceType(lua_State *L, const char *name_type, Component *component);

private:
    int pushBasicType(lua_State *L, Component *component);

};

#endif