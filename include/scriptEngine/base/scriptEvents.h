#ifndef SCRIPT_EVENTS_H
#define SCRIPT_EVENTS_H

#include "objectRegister/objectRegisterEvents.h"
#include "objectRegister/object.h"

class ScriptEngine;

class ScriptEvents : public ObjectRegisterEvents
{
private:
    ScriptEngine *engine;

public:
    ScriptEvents(ScriptEngine *engine) : engine(engine) {}

    virtual ~ScriptEvents() {}

    void onRegisterAnyObject(Object *obj) override;

    void onRemoveAnyObject(Object *obj) override;
};

#endif