#ifndef SCRIPT_ENGINE_H
#define SCRIPT_ENGINE_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "indexedtable.h"
#include "easyStack.h"

#include "objectRegister/objectRegister.h"
#include "resourceLoader/resourceLoader.h"

#include "gameObject/component/component.h"

#include "specialSystems.h"
#include "script.h"
#include "objectScriptData.h"
#include "scriptInstaller.h"
#include "scriptEvents.h"
#include "stackCalls.h"

#define SCRIPT_ENGINE_INDEX_IN_REGISTER 3

struct luaObject
{
    Object *object;
    int ref;
};

class ScriptEngine
{
private:
    lua_State *L;


    ResourceLoader *resourceLoader;

    SpecialSystems *specialSystems;

    ScriptInstaller scriptInstaller;

    StackCalls stackCalls;

    ScriptEvents receivingEvents;

    Indexedtable<Script *> scripts;

    Indexedtable<ObjectScriptData *> osds;

public:

    ObjectRegister *objectRegiser;

    ScriptEngine(ObjectRegister *objectRegiser, ResourceLoader *resourceLoader, SpecialSystems *specialSystems);

    ~ScriptEngine();

    void update();

    void lateUpdate();

    ObjectScriptData *newObjectScriptData(const char *type, Component *component);

    void removeObjectScriptData(ObjectScriptData *osd);

    void onRemoveObject(Object *obj);

    const SpecialSystems *getSpecialSystems() { return specialSystems; }

    void callMethod(ObjectScriptData *osd, const char *_name);

    static void stackDump(lua_State *L);

public:
    void addScript(const char *pathToScript);

public:
    luaObject *getObjectRepresentative(Object *obj);

    luaObject *createUDataForObject(Object *obj, const char *metadata);

private:
    void removeObjectRepresentative(Object *obj);

private:
    Script *findScript(const char *path);

    Script *loadScript(const char *path);

    void unloadScript(Script *script);
};

#endif