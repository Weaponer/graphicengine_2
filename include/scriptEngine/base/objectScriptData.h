#ifndef OBJECT_SCRIPT_DATA_H
#define OBJECT_SCRIPT_DATA_H

#include "gameObject/gameObject.h"
#include "script.h"

#define ON_ENABLE_METHOD_NAME "onEnable"
#define ON_DISABLE_METHOD_NAME "onDisable"
#define ON_DISCARD_METHOD_NAME "onDiscard"
#define START_METHOD_NAME "start"
#define UPDATE_METHOD_NAME "update"
#define LATE_UPDATE_METHOD_NAME "lateUpdate"

class ObjectScriptData
{
private:
    bool enable;
    uint indexMemory;
    int pointData;

    GameObject *sceneObject;

    bool callStart;

public:
    ObjectScriptData(uint indexMemory) : indexMemory(indexMemory), sceneObject(NULL), callStart(false) {}

    ~ObjectScriptData() {}

    uint getIndexMemory()
    {
        return indexMemory;
    }

    void setGameObject(GameObject *so)
    {
        sceneObject = so;
    }

    GameObject *getGameObject() const
    {
        return sceneObject;
    }

    void setPointData(int ptData)
    {
        pointData = ptData;
    }

    int getPointData() const
    {
        return pointData;
    }

    bool isCalledStart() const
    {
        return callStart;
    }

    void setCallStart()
    {
        callStart = true;
    }

    void setEnable(bool _active)
    {
        enable = _active;
    }

    bool getEnable() const
    {
        return enable;
    }
};

#endif