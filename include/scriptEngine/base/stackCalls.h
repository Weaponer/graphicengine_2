#ifndef STACK_CALLS_H
#define STACK_CALLS_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "easyStack.h"
#include "objectScriptData.h"


struct call
{
    ObjectScriptData *osd;
    const char *nameMethod;
};

class StackCalls
{
private:
    lua_State *L;

    EasyStack<call> calls;

public:
    StackCalls();

    ~StackCalls();
    
    void setLuaState(lua_State *state)
    {
        L = state;
    }

    void callMethod(ObjectScriptData *osd, const char *method);

};

#endif