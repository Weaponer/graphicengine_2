#ifndef SCRIPT_H
#define SCRIPT_H

#include "object.h"

class Script : public Object
{
private:
    char *path;
    char *text;

    int functionGenerate;

public:
    Script(const char *_save_path, char *_no_save_text);

    ~Script();

    const char *getPath()
    {
        return path;
    }

    const char *getText()
    {
        return text;
    }

    void setFunctionGenerate(int _functionGenerate)
    {
        functionGenerate = _functionGenerate;
    }

    int getFunctionGenerate()
    {
        return functionGenerate;
    }
};

#endif