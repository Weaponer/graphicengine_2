#ifndef OBJECT_H
#define OBJECT_H

class ObjectRegister;

class Object
{
private:
    unsigned long long index;

public:
    Object()  {}

    virtual ~Object() {}

    friend ObjectRegister;
};


#endif