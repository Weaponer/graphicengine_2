#ifndef OBJECT_REGISTER_EVENTS_H
#define OBJECT_REGISTER_EVENTS_H

#include "object.h"
#include "gameObject/gameObject.h"


class ObjectRegisterEvents
{
public:
    ObjectRegisterEvents() {}

    virtual ~ObjectRegisterEvents() {}

    virtual void onRegisterGameObject(GameObject *obj) {}

    virtual void onRemoveGameObject(GameObject *obj) {}

    virtual void onRegisterResource(Object *obj) {}

    virtual void onRemoveResource(Object *obj) {}

    virtual void onRegisterExtraObject(Object *obj) {}

    virtual void onRemoveExtraObject(Object *obj) {}   

    virtual void onRegisterAnyObject(Object *obj) {}

    virtual void onRemoveAnyObject(Object *obj) {}  
};

#endif