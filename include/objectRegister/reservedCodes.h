#ifndef RESERVED_CODES_H
#define RESERVED_CODES_H

enum ReservedCodes : int
{
    SCRIPT_DATA = 1 << 0,
    RESOURCE_DATA = 1 << 1,
    PATH_ON_RESOURCE = 1 << 2
};

#endif