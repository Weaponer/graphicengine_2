#ifndef OBJECT_REGISTER_H
#define OBJECT_REGISTER_H

#include "object.h"
#include "iteratorByGameObjects.h"
#include "gameObject/gameObject.h"
#include "paterns/stack/indexedtable.h"
#include "objectRegisterEvents.h"

struct ObjectReference
{
    Object *object;
    int indexAdditionalData;
};

class ObjectRegister
{
private:
    struct IteratorOnData
    {
        int next;
        int back;
        void *data;
        int code;
        bool isNull;
        sizeM index;
    };

private:
    ObjectRegisterEvents *events;

    Indexedtable<ObjectReference> resources;

    Indexedtable<ObjectReference> gameObjects;

    Indexedtable<ObjectReference> extraObjects;

private:
    Indexedtable<IteratorOnData> iteratorsOnDatas;

public:
    ObjectRegister();

    ~ObjectRegister();

    void setPointEvents(ObjectRegisterEvents *e);

    void discardPointEvents();

    long long getCodeRegistration(Object *obj);

    void registerResource(Object *obj);

    Object *getResourceByIndex(uint index);

    void registerGameObject(GameObject *obj);

    void registerExtraObject(Object *obj);

    void removeObject(Object *obj);

public:
    IteratorByGameObjects getIteratorByGameObjects();

public:
    void addSetAdditionalData(Object *obj, int code, void *data);

    void *getAdditionalData(Object *obj, int code);

    void removeAdditionalData(Object *obj, int code);

public:
    void clear();

    void clearGameObjects();

    void clearResources();

    void clearExtraObjects();

private:
    Indexedtable<ObjectReference> *getTableWithObject(Object *object);

    ObjectReference *getObjectReference(Object *object);

private:
    IteratorOnData *getIretator(Object *obj, int code);
};

#endif