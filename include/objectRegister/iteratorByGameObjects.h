#ifndef ITERATOR_BY_GAME_OBJECTS_H
#define ITERATOR_BY_GAME_OBJECTS_H

#include "paterns/stack/indexedtable.h"
#include "gameObject/gameObject.h"

class ObjectReference;

class IteratorByGameObjects
{
private:
    Indexedtable<ObjectReference> *table;
    sizeM pos;
    bool end;

public:
    GameObject *getNext();

    bool isEnd();

    void Reset();

public:
    IteratorByGameObjects(Indexedtable<ObjectReference> *_table);

    ~IteratorByGameObjects();

};


#endif