#ifndef RIGIDBODY_ACTOR_H
#define RIGIDBODY_ACTOR_H

#include <MTH/vectors.h>

#include "colliderConstruct.h"
#include "contactData.h"
#include "transform.h"
#include "layerManager/layer.h"

class PhysicsEngine;
class CollisionsSystem;

class RigidbodyActor
{
private:
    mth::vec3 position;

    mth::quat orientation;

    mth::vec3 lastFrameAcceleration;

public:
    bool enable;

    float mass;

    float drag;

    float inverseMass;

    bool kinematic;

    bool sleep;

    mth::vec3 velocity;

    mth::vec3 angularVelocity;

    mth::vec3 acceleration;

    mth::vec3 angularAcceleration;

    mth::mat3 inverseInertiaTensor;

    mth::mat3 inverseInertiaTensorWorld;

    Layer layer;

    PhysicsEngine *engine;

    ColliderConstruct colliderConstruct;

    ContactData contactData;


public:
    RigidbodyActor(PhysicsEngine *engine, CollisionsSystem *collisionsSystem);

    virtual ~RigidbodyActor();

    void update(float duration);

    void setMass(float _mass);

    void addForce(const mth::vec3 &force);

    void addForceAtPoint(const mth::vec3 &force, const mth::vec3 &point);

    void addTorque(const mth::vec3 &torque);

    mth::vec3 getPosition() const { return position; }

    void setPosition(const mth::vec3& pos) { position = pos; }

    mth::quat getOrientation() const { return orientation; }

    void setOrientation(const mth::quat& rot) { orientation = rot; }

    mth::vec3 getLastFrameAcceleration() const { return lastFrameAcceleration; }

    void calculateGlobalInverseInertialTensor();
    
private:
    bool checkChangeInverseInertialTensor();

    void calculateInverseInertialTensor();

    
};

#endif