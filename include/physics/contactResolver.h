#ifndef CONTACT_RESOLVER_H
#define CONTACT_RESOLVER_H

#include <list>

#include "easyStack.h"
#include "contact.h"

#include "debugOutput/threadDebugOutput.h"

class ContactResolver
{
private:
    unsigned velocityIterationsUsed;

    unsigned positionIterationsUsed;

    unsigned velocityIterations;

    unsigned positionIterations;

    float velocityEpsilon;

    float positionEpsilon;

    ThreadDebugOutput *debugOutput;

public:
    ContactResolver(ThreadDebugOutput *debugOutput);

    ~ContactResolver();

    void resolveContacts(const EasyStack<Contact> *contacts, std::list<RigidbodyActor *> *actors, float duration);

private:
    void swapContact(Contact *contact);

    void pushContactsToActors(const EasyStack<Contact> *contacts);

    void prepareContacts(Contact *contacts, int numContacts, float duration);

    void adjustPositions(Contact *contacts, int numContacts, float duration);

    void adjustVelocities(Contact *contacts, int numContacts, float duration);
    
    //contact methods
private:
    void calculateInternals(Contact *contact, float duration);

    void calculateContactBasis(Contact *contact);

    mth::vec3 calculateLocalVelocity(Contact *contact, unsigned bodyIndex, float duration);

    void calculateDesiredDeltaVelocity(Contact *contact, float duration);

    void applyPositionChange(Contact *contact, mth::vec3 linearChange[2], mth::vec3 angularChange[2], float penetration);

    void applyVelocityChange(Contact *contact, mth::vec3 velocityChange[2], mth::vec3 rotationChange[2]);

    mth::vec3 calculateFrictionlessImpulse(Contact *contact, const mth::mat3 *inverseInertialTensor);

    mth::vec3 calculateFrictionImpulse(Contact *contact, const mth::mat3 *inverseInertialTensor);
};

#endif