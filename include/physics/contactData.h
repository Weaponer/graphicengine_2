#ifndef CONTACT_DATA_H
#define CONTACT_DATA_H

#define MAX_CONTACTS 10

class Contact;

class ContactData
{
private:
    Contact *contacts[MAX_CONTACTS];

    unsigned count;

public:
    ContactData() {}

    ~ContactData() {}

    Contact *getContact(unsigned index) const;

    void addContact(Contact *contact);

    void clearContacts();

    unsigned getCountContacts() const { return count; }
};

#endif