#ifndef GRAVITATION_H
#define GRAVITATION_H

#include <MTH/vectors.h>

#define GRAVITATION_CONST 9.8f
#define DIRECTION_GRAVITATION mth::vec3(0, -1, 0)

extern "C"
{

    float getFroceGravitation(float mass)
    {
        return GRAVITATION_CONST * mass;
    }
}

#endif