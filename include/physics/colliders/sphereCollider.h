#ifndef SPHERE_COLLIDER_H
#define SPHERE_COLLIDER_H

#include <MTH/boundSphere.h>
#include <MTH/vectors.h>

#include "collider.h"

class SphereCollider : public Collider
{
private:
    float radius;
    

public:
    SphereCollider();

    virtual ~SphereCollider();

    inline float getRadius() const { return radius; }

    void setRadius(float radius);

    mth::boundSphere generateGlobalBound() override;

};

#endif