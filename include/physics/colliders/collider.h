#ifndef COLLIDER_H
#define COLLIDER_H

#include <MTH/vectors.h>
#include <MTH/boundSphere.h>

class ColliderConstruct;

enum TypeCollider : char
{
    SPHERE_COLLIDER = 1,
    BOX_COLLIDER = 2,
};

class Collider
{
protected:
    mth::vec3 origin;

public:
    int index;

    TypeCollider type;

    bool kinematic;

    bool enable;

    /*
    for detection contacts
    */
    uint numUpdate;

    ColliderConstruct *constuct;

    Collider(TypeCollider type) : origin(0, 0, 0), index(-1), type(type), kinematic(true), enable(true), constuct(NULL) {}

    virtual ~Collider();

    /*
    for worldGrid system;
    */
    virtual mth::boundSphere generateGlobalBound() = 0;

    mth::vec3 getOrigin() const { return origin; }

    void setOrigin(const mth::vec3 &pos) { origin = pos; }
};

#endif