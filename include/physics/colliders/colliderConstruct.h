#ifndef COLLIDER_CONSTRUCT_H
#define COLLIDER_CONSTRUCT_H

#include <MTH/vectors.h>
#include <MTH/boundSphere.h>
#include <list>

#include "easyStack.h"

#include "collisionDetection/collisionsSystem.h"
#include "collider.h"

class RigidbodyActor;

class ColliderConstruct
{
private:
    std::list<Collider *> listColliders;

    RigidbodyActor *actor;

    CollisionsSystem *collisionsSystem;

public:
    ColliderConstruct(RigidbodyActor *actor, CollisionsSystem *collisionsSystem);

    ~ColliderConstruct();

    void connectCollider(Collider *collider);

    void removeCollider(Collider *collider);

    std::list<Collider *> *getListColliders() { return &listColliders; }

    RigidbodyActor *getActor() { return actor; }
};

#endif