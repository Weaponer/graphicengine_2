#ifndef BOX_COLLIDER_H
#define BOX_COLLIDER_H

#include <MTH/boundSphere.h>
#include <MTH/boundOBB.h>
#include <MTH/vectors.h>

#include "collider.h"

class BoxCollider : public Collider
{
private:
    mth::vec3 size;
    
public:
    BoxCollider();

    virtual ~BoxCollider();

    inline mth::vec3 getSize() const { return size; }

    void setSize(mth::vec3 size);
    
    mth::boundOBB calculateBoundOBB() const;

    mth::boundSphere generateGlobalBound() override;
};

#endif