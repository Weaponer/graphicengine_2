#ifndef PHYSICS_ENGINE_H
#define PHYSICS_ENGINE_H

#include <list>

#include "rigidbodyActor.h"
#include "collider.h"
#include "collisionsSystem.h"
#include "contactResolver.h"
#include "layerManager/layerManager.h"
#include "debugOutput/threadDebugOutput.h"

class PhysicsEngine
{
private:
    std::list<RigidbodyActor *> actors;

    CollisionsSystem collisionsSystem;

    ContactResolver contactResolver;

    LayerManager *layerManager;

    ThreadDebugOutput *debugOutput;

public:
    PhysicsEngine(LayerManager *layerManager, ThreadDebugOutput *debugOutput);

    virtual ~PhysicsEngine();

    void update(float duration);

    RigidbodyActor *createActor();

    void removeActor(RigidbodyActor *);

    void addCollider(Collider *collider)
    {
        collisionsSystem.addCollider(collider);
    }

    void removeCollider(Collider *collider)
    {
        collisionsSystem.removeCollider(collider);
    }

    void setKinematic(RigidbodyActor *actor, bool _b);

    ThreadDebugOutput *getDebugOutput() const { return debugOutput; }
};

#endif