#ifndef POTENTIAL_CONTACT_GENERATOR_H
#define POTENTIAL_CONTACT_GENERATOR_H

#include "potentialContact.h"
#include "worldGrid.h"
#include "collider.h"
#include "checkCounter.h"
#include "easyStack.h"
#include "layerManager/layerManager.h"
#include "debugOutput/threadDebugOutput.h"

class PotentialContactGenerator
{
private:
    CheckCounter *checkCounter;

    WorldGrid *worldGrid;

    EasyStack<PotentialContact> bufferPotentialContacts;

    LayerManager *layerManager;

    ThreadDebugOutput *debugOutput;

public:
    PotentialContactGenerator(CheckCounter *checkCounter, WorldGrid *worldGrid, LayerManager *layerManager, ThreadDebugOutput *debugOutput);

    ~PotentialContactGenerator();

    void generatePotentialContacts(const EasyStack<Collider *> *dynamicColliderBuffer);

    const EasyStack<PotentialContact> *getBufferPotentialContacts() const { return &bufferPotentialContacts; }

private:
    void workFromCell(const GridCell *cell, Collider *collider);

    bool checkNoHavePotentialContact(Collider *one, Collider *two);

    void pushPotentialContact(Collider *one, Collider *two);
};

#endif