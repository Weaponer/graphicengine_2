#ifndef CONTACT_GENERATOR_H
#define CONTACT_GENERATOR_H

#include <MTH/vectors.h>

#include "easyStack.h"
#include "potentialContact.h"
#include "contact.h"


class RigidbodyActor;

class ContactGenerator
{
private:
    EasyStack<Contact> contacts;

public:
    ContactGenerator();

    ~ContactGenerator();

    void generateContacts(const EasyStack<PotentialContact> *pContacts);

    const EasyStack<Contact> *getContacts() const { return &contacts; }

private:
    void createContact(const PotentialContact *pContact);

    PotentialContact swapColliders(const PotentialContact *pContact);

private:
    void generateSphereAndSphere(const PotentialContact *pContact);

    void generateSphereAndBox(const PotentialContact *pContact);

    void generateBoxAndBox(const PotentialContact *pContact);
};

#endif