#ifndef CHECK_COUNTER_H
#define CHECK_COUNTER_H

#include "collider.h"

class CheckCounter
{
private:
    unsigned int actualNumUpdate;

public:
    CheckCounter() : actualNumUpdate(0) {}

    ~CheckCounter() {}

    void reset();

    void nextUpdate();

    void updateCollider(Collider *collider);

    bool checkCollider(const Collider *collider) const;
};

#endif
