#ifndef COLLISIONS_SYSTEM_H
#define COLLISIONS_SYSTEM_H

#include <list>

#include "easyStack.h"
#include "indexedtable.h"
#include "collider.h"
#include "dataAnalyticsUpdate.h"
#include "worldGrid.h"
#include "checkCounter.h"
#include "potentialContactGenerator.h"
#include "contactGenerator.h"
#include "layerManager/layerManager.h"
#include "debugOutput/threadDebugOutput.h"

class CollisionsSystem
{
private:
    DataAnalyticsUpdate analytics;

    Indexedtable<Collider *> colliders;

    EasyStack<Collider *> dynamicColliderBuffer;
    EasyStack<Collider *> staticColliderBuffer;

    CheckCounter checkCounter;

    WorldGrid worldGrid;

    PotentialContactGenerator potentialContactGenerator;

    ContactGenerator contactGenerator;

public:
    CollisionsSystem(LayerManager *layerManager, ThreadDebugOutput *debugOutput);

    ~CollisionsSystem();

    void update();

    void addCollider(Collider *collider);
    
    void removeCollider(Collider *collider);

    void enableCollider(Collider *collider);

    void disableCollider(Collider *collider);

    void setKinematicCollider(Collider *collider);

    void setDynamicCollider(Collider *collider);

    const EasyStack<Contact> *getContacts() const { return contactGenerator.getContacts(); }

private:
    void generateListsColliders();
};

#endif