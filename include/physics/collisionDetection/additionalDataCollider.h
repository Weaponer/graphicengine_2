#ifndef ADDITIONAL_DATA_COLLIDER_H
#define ADDITIONAL_DATA_COLLIDER_H

#include <MTH/boundSphere.h>

struct AdditionalDataCollider
{
    mth::boundSphere currentBound;
    mth::boundSphere newBound;

    bool isKinematic;

    bool isChange;
};


#endif