#ifndef DATA_ANALYTICS_UPDATE_H
#define DATA_ANALYTICS_UPDATE_H

struct DataAnalyticsUpdate
{
    float maxRadiusDynamic;
    bool changeDynamicColliders;

    bool changeKinematicColliders;
    int potentialAmountStaticCells;

    void clear()
    {
        maxRadiusDynamic = 0;
        potentialAmountStaticCells = 0;
        changeDynamicColliders = false;
        changeKinematicColliders = false;
    }
};


#endif