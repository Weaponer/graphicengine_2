#ifndef POTENTIAL_CONTACT_H
#define POTENTIAL_CONTACT_H

class Collider;

struct PotentialContact
{
    Collider *colliders[2];
};

#endif