#ifndef WORLD_GRID_H
#define WORLD_GRID_H

#include "easyStack.h"
#include "indexedtable.h"
#include "additionalDataCollider.h"
#include "grid/grid.h"
#include "grid/gridCell.h"
#include "dataAnalyticsUpdate.h"
#include "debugOutput/threadDebugOutput.h"

#define DEFAULT_DYNAMIC_CELL_SIZE 1
#define DEFAULT_STATIC_CELL_SIZE 2

class WorldGrid
{
private:
    Grid dynamicGrid;
    Grid staticGrid;
    EasyStack<AdditionalDataCollider> collidersAdditionalData;

    DataAnalyticsUpdate analytics;

    ThreadDebugOutput *debugOutput;

public:
    WorldGrid(ThreadDebugOutput *debugOutput);

    ~WorldGrid();

    void update(const EasyStack<Collider *> *dynamicColliders, const EasyStack<Collider *> *kinematicColliders);

    void updateDynamicColliders(const EasyStack<Collider *> *dynamicColliders);

    void updateKinematicColliders(const EasyStack<Collider *> *kinematicColliders);

    void addCollider(const Collider *collider);

    void removeCollider(const Collider *collider);

    AdditionalDataCollider *getAdditionalData(const Collider *collider);

    void popColliderToKinematic(Collider *collider);

    void popColliderToDynamic(Collider *collider);

    void pushColliderToKinematic(Collider *collider);

    void pushColliderToDynamic(Collider *collider);

public:
    void getDynamicCellsAroundPoint(const mth::vec3 &point, GridCell **bufferCells);

public:
    int maxCountCellsForRadiusKinematic(float radius);

    GridCell *getKinematicCell(int iX, int iY, int iZ);

    void getAreaKinematicCells(const mth::boundSphere &globalBound, int &minX, int &minY, int &minZ, int &maxX, int &maxY, int &maxZ);

    int getSizeKinematicCell() const { return staticGrid.getSizeCell(); }

private:
    void reconstructDynamicGrid(const EasyStack<Collider *> *dynamicColliders);

    void reconstructKinematicGrid(const EasyStack<Collider *> *kinematicColliders);
};

#endif