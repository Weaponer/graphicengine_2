#ifndef GRID_H
#define GRID_H

#include <MTH/vectors.h>

#include "gridCell.h"
#include "easyStack.h"
#include "iteratorCell.h"

#define DEFAULT_SIZE 30
#define DEFAULT_SIZE_CELL 0

class Grid
{
private:
    int size;

    int countUseCells;

    int sizeCell;

    EasyStack<GridCell> gridCells;
    EasyStack<GridCell> extraGridCells;
    EasyStack<GridCell *> noUseExtraGridCells;

    EasyStack<IteratorCell> stackIterators;
    EasyStack<IteratorCell *> stackNoUseIterators;

public:
    Grid();

    ~Grid();

    void clear();

    GridCell *getCell(int iX, int iY, int iZ);

    GridCell *getCellOrCreate(int iX, int iY, int iZ);

    void clearForRestruct(int _size, int countCells);

    void getPointCell(const mth::vec3 &pos, int &iX, int &iY, int &iZ);

    int getSize() const { return size; }

    int getSizeCell() const { return sizeCell; }

    int getCountUseCells() const { return countUseCells; }

    const EasyStack<GridCell> *getStackBasicCells() const { return &gridCells; }

    const EasyStack<GridCell> *getStackExtraCells() const { return &extraGridCells; }

public:
    void eraseColliderInCell(GridCell *cell, Collider *collider);

    IteratorCell *addColliderInCell(GridCell *cell, Collider *collider);

    void clearCell(GridCell *cell);

private:
    IteratorCell *assignmentIterator();

    void removeIterator(IteratorCell **iter);

private:
    int generateCode(int iX, int iY, int iZ);

    int getHashIndex(int n);

    GridCell *getCellAndOther(int iX, int iY, int iZ, uint &cod, GridCell **backCell);

private:
    GridCell *getExtraGridCell();

    void clearExtraGridCell(GridCell *cell);

};

#endif