#ifndef ITERATOR_CELL_H
#define ITERATOR_CELL_H

class Collider;

struct IteratorCell
{
    IteratorCell *back;
    IteratorCell *next;
    Collider *collider;
};

#endif