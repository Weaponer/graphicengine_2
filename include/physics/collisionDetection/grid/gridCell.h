#ifndef GRID_CELL_H
#define GRID_CELL_H

#include "collider.h"
#include "iteratorCell.h"

class GridCell
{
public:
    int iX;
    int iY;
    int iZ;

    int typeMemory;
    
    GridCell *parentCell;
    GridCell *nextCell;

    IteratorCell *begin;
    IteratorCell *end;

    GridCell();

    ~GridCell();

    void clearCell();

    bool isEmpty() const { return begin == NULL && end == NULL; }
};

#endif