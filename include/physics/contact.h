#ifndef CONTACT_H
#define CONTACT_H

#include <MTH/vectors.h>
#include <MTH/matrix.h>

class RigidbodyActor;

struct Contact
{
    RigidbodyActor *rigidbodies[2];
    mth::vec3 point;
    mth::vec3 normal;
    mth::mat3 basis;
    float penetration;

    mth::vec3 contactVelocity;
    float desiredDeltaVelocity;
    mth::vec3 relativeContactPosition[2];
};

#endif