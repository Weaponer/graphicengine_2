#ifndef ENGINE_BASE_H
#define ENGINE_BASE_H

#include "engineBase/configManager/configManager.h"
#include "renderEngine/settings/graphicConfig.h"

#include "window/window.h"
#include "objectRegister/objectRegister.h"
#include "specialSystems.h"

#include "resourceLoader/resourceLoader.h"
#include "layerManager/layerManager.h"

#include "renderEngine/graphicEngine.h"
#include "debugOutput/debugOutput.h"

#include "physics/physicsEngine.h"
#include "engineBase/sandboxGameObjects/sandboxGameObjects.h"

#include "scriptEngine/base/scriptEngine.h"

#include "engineBase/timeData.h"

class EngineBase
{
private:
    ObjectRegister *globalRegister;
    SpecialSystems *specialSystems;
    ResourceLoader *resourceLoader;

    ConfigManager configManager;
    GraphicConfig graphicConfig;

    Window *window;
    LayerManager *layerManager;

    SettingsMainSystems settingsMainSystems;
    GraphicEngine *graphicEngine;
    DebugOutput *debugOutput;

    PhysicsEngine *physicsEngine;

    TimeData timeData;

    ScriptEngine *scriptEngine;

    SandboxGameObjects *sandboxGameObjects;

public:
    void init();

    void destroy();

    void startMainLoop();

private:
    void loadConfigs();

    void loadConfig(const char *_path);

    void loadShaders();

    void loadGraphicSettings();

    void loadScripts();

    void loadTechnicalShader(const char *_path);

    void loadModelLight(const char *_path);

    void loadModelLights();

    void initGraphyicEngine();
};

#endif