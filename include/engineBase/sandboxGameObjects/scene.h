#ifndef SCENE_H
#define SCENE_H

#include <list>

#include "gameObject/gameObject.h"
#include "gameObject/component/component.h"
#include "object.h"

#include "objectRegister/objectRegister.h"

enum TypeScene
{
    NONE_INIT_SCENE,
    CURRENT_SCENE,
    DONT_DESTROY_SCENE,
    ADDITIONAL_SCENE
};

class Scene : public Object
{
private:
    TypeScene typeScene;
    std::string name;

    std::list<GameObject *> objects;

public:
    Scene(const char *_name);

    ~Scene();

    const char *getName() const { return name.c_str(); }

    void setName(const char *_name) { name = std::string(_name); }

    void setTypeScene(TypeScene _typeScene) { typeScene = _typeScene; }

    TypeScene getTypeScene() const { return typeScene; };

    void addObject(GameObject *_obj);

    void removeObject(GameObject *_obj);

    const std::list<GameObject *> *getArrayObjects() { return &objects; }

};

#endif