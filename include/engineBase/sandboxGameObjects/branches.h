#ifndef BRANCHES_H
#define BRANCHES_H

#include "paterns/stack/easyStack.h" 

class Branches
{
private:
    int nextBranch;
    EasyStack<int> freeBranches;

public:
    Branches();

    ~Branches();

    void freeBranch(int _branch);

    int getBranch();

};

#endif