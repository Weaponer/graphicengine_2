#ifndef SANDBOX_GAME_OBJECTS_H
#define SANDBOX_GAME_OBJECTS_H

#include "paterns/stack/indexedtable.h"
#include "sceneManager.h"
#include "scene.h"
#include "objectRegister/objectRegister.h"
#include "gameObject/gameObject.h"
#include "gameObject/component/component.h"
#include "layerManager/layerManager.h"
#include "branches.h"

class SandboxGameObjects
{
private:
    Branches branches;
    SceneManager sceneManager;
    
    ObjectRegister *objectRegister;
    LayerManager *layerManager;

public:
    SandboxGameObjects(ObjectRegister *_objectRegister, LayerManager *_layerManager);

    ~SandboxGameObjects();

public:
    GameObject *createNewGameObject(std::string _name = "GameObject");

    GameObject *createNewGameObject(Scene *_scene, std::string _name = "GameObject");

    int parentGameObject(GameObject *_gameObject, GameObject *_destination);

    void unparentGameObject(GameObject *_gameObject);

    void removeGameObject(GameObject *_gameObject);

    void removeAllGameObjectsInScene(Scene *_scene);

    void setGameObjectDontDestroy(GameObject *_gameObject);

    void moveGameObjectOnAnotherScene(GameObject *_gameObject, Scene *_scene);

    SceneManager *getSceneManager() { return &sceneManager; }

public:
    void addComponent(GameObject *_gameObject, Component *component);

    void removeComponent(Component *_component);

    void updateComponents();

    void clearMasksGameObjects();

private:
    int getFreeBranch();

    void freeBranch(int _branch);
};

#endif