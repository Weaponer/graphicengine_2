#ifndef SCENE_MANAGER_H
#define SCENE_MANAGER_H

#include <list>
#include "objectRegister/objectRegister.h"
#include "scene.h"

class SandboxGameObjects;

class SceneManager
{
private:
    SandboxGameObjects *sandboxGameObjects;
    ObjectRegister *objectRegister;
    std::list<Scene *> scenes;

    Scene *currentScene;
    Scene *dontDestroyScene;

public:
    SceneManager(ObjectRegister *_objectRegister, SandboxGameObjects *_sandboxGameObjects);

    ~SceneManager();

    Scene *createNewScene(const char *_name);

    bool removeScene(Scene *_scene);

    Scene *getCurrentScene() const;

    Scene *getDontDestroyScene() const;

    Scene *getSceneByName(const char *_name) const;

    void setSceneDefault(Scene *_scene);

    const std::list<Scene *> *getListScenes() const;

private:
    void pushDefaultScene();

    void destroyScene(Scene *_scene);
};

#endif