#ifndef SPECIAL_SYSTEMS_H
#define SPECIAL_SYSTEMS_H

class ConfigManager;
class GraphicConfig;
class LayerManager;
class ResourceLoader;
class SandboxGameObjects;
class InputSystem;
class GraphicEngine;
class PhysicsEngine;
class Window;
class ThreadDebugOutput;
class TimeData;

class SpecialSystems
{
public:
    ConfigManager *configManager;
    GraphicConfig *graphicConfig;
    LayerManager *layerManager;
    ResourceLoader *resourceLoader;
    SandboxGameObjects *sandboxGameObjects;
    InputSystem *inputSystem;
    GraphicEngine *graphicEngine;
    PhysicsEngine *physicsEngine;
    Window *window;
    ThreadDebugOutput *mainDebugOutput;
    TimeData *timeData;
};

#endif