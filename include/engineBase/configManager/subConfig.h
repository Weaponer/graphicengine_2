#ifndef SUB_CONFIG_H
#define SUB_CONFIG_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

class ConfigManager;

class SubConfig
{
protected:
    ConfigManager *configManager;

public:
    SubConfig();

    virtual ~SubConfig();

protected:
    virtual void initCategories(lua_State *L) = 0;

    friend ConfigManager;
};

#endif