#ifndef CONFIG_MANAGER_H
#define CONFIG_MANAGER_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include "subConfig.h"
#include "configTable.h"
#include "resourceLoader/resources/textFile.h"

enum TypeConfigData
{
    INT_TYPE_CONFIG_DATA = 0,
    FLOAT_TYPE_CONFIG_DATA = 1,
    STRING_TYPE_CONFIG_DATA = 2,
    BOOL_TYPE_CONFIG_DATA = 3
};

class ConfigManager
{
private:
    lua_State *L;

public:
    ConfigManager();

    ~ConfigManager();

    bool readConfigFile(const TextFile *_file);

    ConfigTable getConfigTable(const char *_name);

    ConfigTable getMainTable();

    void addSubConfig(SubConfig *_subConfig);
};

#endif