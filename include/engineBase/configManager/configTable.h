#ifndef CONFIG_TABLE_H
#define CONFIG_TABLE_H

#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

#include <string>
#include <vector>

class ConfigTable
{
private:
    lua_State *L;
    int indexTable;

public:
    ConfigTable();

    ConfigTable(lua_State *L, int _indexTable);

    ~ConfigTable();

    ConfigTable toTable(const char *_name);

    int toInt(const char *_name);

    float toFloat(const char *_name);

    const char *toString(const char *_name);

    bool toBool(const char *_name);

    void toArrayStrings(const char *_name, std::vector<std::string> *_strings);
};

#endif