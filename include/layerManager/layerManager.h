#ifndef LAYER_MANAGER_H
#define LAYER_MANAGER_H

#include <string>

#include "paterns/stack/easyStack.h"
#include "layerManager/layer.h"
#include "layerManager/layerData.h"

class LayerManager
{
private:
    struct layerInfo
    {
        std::string name;
        Layer layer;
    };
    

    std::vector<layerInfo> layerData;

    bool **dataPhysics;
    unsigned int countDataPhysics;
    
    unsigned int countLayersInit;

public:
    LayerManager();

    ~LayerManager();

    void initLayerManager(const LayerData *_data);

    bool isSingleLayer(Layer _layer) const;

    const char *getNameLayer(Layer _layer) const;

    Layer getLayerByName(const char *_name) const;

    Layer getLayerByNumber(int _number) const;

    Layer getDefaultLayer() const;

    unsigned int getNumberLayer(Layer _layer) const;

    bool isUsePhysics(Layer _first, Layer _second) const;

};

#endif