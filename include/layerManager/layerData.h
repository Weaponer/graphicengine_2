#ifndef LAYER_DATA_H
#define LAYER_DATA_H

#include <vector>
#include <string>

#include "objectRegister/object.h"

class LayerData : public Object
{
public:
    std::vector<std::string> layers;

    std::vector<std::pair<unsigned int, unsigned int>> physicsEnable;

    LayerData() {}

    virtual~LayerData() {}


};

#endif