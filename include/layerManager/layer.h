#ifndef LAYER_H
#define LAYER_H

typedef unsigned long Layer;

#define MAX_COUNT_LAYERS sizeof(Layer) * 8

#endif