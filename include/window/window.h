#ifndef WINDOW_H
#define WINDOW_H

#include <SDL2/SDL.h>

#include "window/inputSystem.h"


class Window
{
private:
    int width;
    int height;
    SDL_Window *wind;
    InputSystem inputSystem;
    SDL_GLContext glContext;
    bool isMouse;

public:
    Window(int widht, int height);

    ~Window();

    int updateEvent();

    SDL_GLContext *getContext() { return &glContext; }

    SDL_Window *getWindow() { return wind; }

    void swapBuffers();

    InputSystem *getInputSystem() { return &inputSystem; }

    int getWidth() const { return width; }

    int getHeight() const { return height; }

    bool isMouseLock() const { return isMouse; };

    void setMouseLock(bool isLock);
};

#endif