#ifndef KEYBOARD_H
#define KEYBOARD_H

#include <SDL2/SDL_scancode.h>

#include "registerPressedButtons.h"

class Keyboard
{
private:
    RegisterPressedButtons buttonsData;

public:
    Keyboard();

    ~Keyboard();

    void nextUpdate();

    void updateByEvent(SDL_Event *event);

    bool getKeyDown(SDL_Scancode code) const;

    bool getKey(SDL_Scancode code) const;

    bool getKeyUp(SDL_Scancode code) const;
};

#endif