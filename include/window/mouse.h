#ifndef MOUSE_H
#define MOUSE_H

#include <MTH/vectors.h>
#include <SDL2/SDL_mouse.h>

#include "registerPressedButtons.h"

class Mouse
{
private:
    mth::vec2 position;
    mth::vec2 delta;
    
    RegisterPressedButtons buttonsData;

public:
    Mouse();

    ~Mouse();

    void nextUpdate();

    void updateByEvent(SDL_Event *event);

    mth::vec2 getMousePosition() const;

    mth::vec2 getMouseDeltaMove() const;

    bool getMouseButtonDown(Uint8 code) const;

    bool getMouseButton(Uint8 code) const;

    bool getMouseButtonUp(Uint8 code) const;
};

#endif