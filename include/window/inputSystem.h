#ifndef INPUT_SYSTEM_H
#define INPUT_SYSTEM_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>

#include "mouse.h"
#include "keyboard.h"

class InputSystem
{
private:
    Mouse mouse;
    Keyboard keyboard;

public:
    InputSystem();

    ~InputSystem();

    void startNewUpdates();

    void updateByEvent(SDL_Event *event);

    Mouse *getMouse() { return &mouse; }

    Keyboard *getKeyboard() { return &keyboard; }

};

#endif