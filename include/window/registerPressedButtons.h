#ifndef REGISTER_PRESSED_BUTTONS_H
#define REGISTER_PRESSED_BUTTONS_H

#include <vector>
#include <SDL2/SDL.h>

class RegisterPressedButtons
{
private:
    struct pressData
    {
        bool down;
        bool current;
        bool up;
    };

    std::vector<pressData> buttonsData;

public:
    RegisterPressedButtons() {}

    ~RegisterPressedButtons() {}

    void nextUpdate();

    void setDownButton(Uint32 button);

    void setUpButton(Uint32 button);

    bool getButtonDown(Uint32 button) const;

    bool getButton(Uint32 button) const;

    bool getButtonUp(Uint32 button) const;

    void setCountButtons(Uint32 button);
};

#endif