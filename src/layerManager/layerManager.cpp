#include "layerManager/layerManager.h"

LayerManager::LayerManager()
{
    int countLayers = MAX_COUNT_LAYERS;
    layerData.reserve(countLayers);
    countDataPhysics = countLayers;
    dataPhysics = new bool *[countDataPhysics];
    int set = countDataPhysics;
    for (unsigned int i = 0; i < countDataPhysics; i++)
    {
        dataPhysics[i] = new bool[set];
        for (int i2 = 0; i2 < set; i2++)
        {
            dataPhysics[i][i2] = false;
        }
        set--;
    }
}

LayerManager::~LayerManager()
{
    for (unsigned int i = 0; i < countDataPhysics; i++)
    {
        delete[] dataPhysics[i];
    }
    delete[] dataPhysics;
}

void LayerManager::initLayerManager(const LayerData *_data)
{
    Layer curL = 1;
    countLayersInit = std::min(_data->layers.size(), MAX_COUNT_LAYERS);
    for (int i = 0; i < (int)_data->layers.size() && i < (int)MAX_COUNT_LAYERS; i++)
    {
        layerInfo info;
        info.name = _data->layers[i];
        info.layer = curL;
        layerData.push_back(info);
        curL = curL << 1;
    }

    for (int i = 0; i < (int)_data->physicsEnable.size(); i++)
    {
        std::pair<unsigned int, unsigned int> d = _data->physicsEnable[i];

        unsigned int f = d.first;
        unsigned int s = d.second;
        if (f < s)
        {
            std::swap(f, s);
        }
        s = countDataPhysics - s - 1;
        dataPhysics[f][s] = true;
    }
}

bool LayerManager::isSingleLayer(Layer _layer) const
{
    int count = 0;
    for (unsigned int i = 0; i < countDataPhysics;)
    {
        if (_layer && (((unsigned int)1) << i) != 0)
        {
            count++;
        }
    }
    if (count == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

const char *LayerManager::getNameLayer(Layer _layer) const
{
    unsigned int number = getNumberLayer(_layer);
    return layerData[number].name.c_str();
}

Layer LayerManager::getLayerByName(const char *_name) const
{
    int pos = layerData.size();
    for (int i = 0; i < pos; i++)
    {
        const layerInfo *data = &(layerData[i]);
        if (strcmp(_name, data->name.c_str()) == 0)
        {
            return data->layer;
        }
    }
    return getDefaultLayer();
}

Layer LayerManager::getLayerByNumber(int _number) const
{
    if((unsigned int)_number >= countLayersInit)
    {
        _number = 0;
    }
    return layerData[_number].layer;
}

unsigned int LayerManager::getNumberLayer(Layer layer) const
{
    for (unsigned int i = 0; i < countDataPhysics; i++)
    {
        Layer d = 1;
        d = d << i;
        if (layer == d)
        {
            return i;
        }
    }
    return 0;
}

Layer LayerManager::getDefaultLayer() const
{
    return 1;
}

bool LayerManager::isUsePhysics(Layer _first, Layer _second) const
{
    unsigned int f = getNumberLayer(_first);
    unsigned int s = getNumberLayer(_second);
    if (f < s)
    {
        std::swap(f, s);
    }
    s = countDataPhysics - s - 1;
    return dataPhysics[f][s];
}