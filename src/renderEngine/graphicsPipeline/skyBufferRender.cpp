#include "renderEngine/graphicsPipeline/skyBufferRender.h"
#include "renderEngine/graphicEngine.h"

SkyBufferRender::SkyBufferRender(MainSystems *_mainSystem, SettingsGraphicPipeline *_settingsGraphicPipeline)
    : mainSystems(_mainSystem),
      settingsGraphicPipeline(_settingsGraphicPipeline), technicalShaderStorage(_mainSystem->getTechnicalShaderStorage()),
      mainCameraVertexesData(_mainSystem->getMainFunctions()->getCameraVertexesData()), skyRenderData(&_settingsGraphicPipeline->sky),
      programActivator(_mainSystem->getProgramActivator())
{
    renderSkyBoxInBufferProgram.setPipeline(technicalShaderStorage->getTShader("VertexScreen"),
                                            technicalShaderStorage->getTShader("BlitSkyBox"));

    skyRenderData->skyMaterial = NULL;
    skyRenderData->intensity = 0.0f;
    skyRenderData->directionSun = mth::vec3(1, 0.8f, 0);
    skyRenderData->directionSun.normalise();
    skyRenderData->colorSun = mth::col(1, 1, 1, 1);
    skyRenderData->typeUpdateSky = TypeUpdateSky::EveryFrame;
    skyRenderData->ambientLightColor = mth::col(1, 1, 1, 1);
    skyRenderData->intensityAmbientLight = 0.13f;
    skyRenderData->environmentBufferAsAmbientLight = true;

    oldSkyData.ambientLightColor = mth::vec3(skyRenderData->ambientLightColor.r, skyRenderData->ambientLightColor.g, skyRenderData->ambientLightColor.b);
    oldSkyData.colorSun = mth::vec4(skyRenderData->colorSun.r, skyRenderData->colorSun.g, skyRenderData->colorSun.b, skyRenderData->colorSun.a);
    oldSkyData.directionSun = skyRenderData->directionSun;
    oldSkyData.environmentBufferAsAmbientLight = skyRenderData->environmentBufferAsAmbientLight;
    oldSkyData.intensity = skyRenderData->intensity;
    oldSkyData.intensityAmbientLight = skyRenderData->intensityAmbientLight;

    glGenBuffers(1, &uboSkyDataBuffer);
    glBindBuffer(GL_UNIFORM_BUFFER, uboSkyDataBuffer);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(SkyDataBuffer), &oldSkyData, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    onUpdate = false;

    createVertexBuffer();
    createEnvironmentMap();

    generatorSubMapProgram.setPipeline(technicalShaderStorage->getTShader("EnvironmentMapGeneratorVert"),
                                       technicalShaderStorage->getTShader("EnvironmentMapGeneratorFrag"));
}

SkyBufferRender::~SkyBufferRender()
{
    clearEnvironmentMap();
    destroyVertexBuffer();
    glDeleteBuffers(1, &uboSkyDataBuffer);
}

void SkyBufferRender::renderSkyBoxInBuffer(ColorFrameBuffer *_dest)
{
    _dest->enable();
    mainCameraVertexesData->enableVertexBuffers();
    renderSkyBoxInBufferProgram.enablePipeline();

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, environmentMapBuffer);

    mainCameraVertexesData->callDraw();
    mainCameraVertexesData->disableVertexBuffers();
    _dest->disable();
}

void SkyBufferRender::update()
{
    Material *skyMaterial = skyRenderData->skyMaterial;
    if (skyMaterial == NULL || skyMaterial->getShader() == NULL)
    {
        return;
    }
    if (skyMaterial->getShader()->getCountPasses(TypePass::BaseColor) < 1)
    {
        return;
    }

    if (skyRenderData->typeUpdateSky == TypeUpdateSky::Call && !onUpdate)
    {
        return;
    }

    SkyRenderData skyData = *skyRenderData;
    SkyDataBuffer newSkyData;
    newSkyData.ambientLightColor = mth::vec3(skyData.ambientLightColor.r, skyData.ambientLightColor.g, skyData.ambientLightColor.b);
    newSkyData.colorSun = mth::vec4(skyData.colorSun.r, skyData.colorSun.g, skyData.colorSun.b, skyData.colorSun.a);
    newSkyData.directionSun = skyData.directionSun;
    newSkyData.environmentBufferAsAmbientLight = skyData.environmentBufferAsAmbientLight;
    newSkyData.intensity = skyData.intensity;
    newSkyData.intensityAmbientLight = skyData.intensityAmbientLight;
    newSkyData.environmentBufferMaxLevel = environmentFrameBuffers.size();

    if (std::memcmp(&newSkyData, &oldSkyData, sizeof(SkyDataBuffer)) != 0)
    {
        oldSkyData = newSkyData;
        glBindBuffer(GL_UNIFORM_BUFFER, uboSkyDataBuffer);
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(SkyDataBuffer), &oldSkyData);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
    }

    onUpdate = false;

    GLuint pipeline = 0;
    programActivator->enableProgramInMaterial(skyMaterial, TypePass::BaseColor, 0, &pipeline);
    glBindProgramPipeline(pipeline);

    enableVertexBuffer();

    GLenum drawBuffers[6] = {
        GL_COLOR_ATTACHMENT0,
        GL_COLOR_ATTACHMENT1,
        GL_COLOR_ATTACHMENT2,
        GL_COLOR_ATTACHMENT3,
        GL_COLOR_ATTACHMENT4,
        GL_COLOR_ATTACHMENT5,
    };

    glBindFramebuffer(GL_FRAMEBUFFER, environmentFrameBuffers[0].first);
    glDrawBuffers(6, drawBuffers);
    glViewport(0, 0, environmentFrameBuffers[0].second, environmentFrameBuffers[0].second);

    callDrawVertexBuffer();

    generatorSubMapProgram.enablePipeline();
    int countSubMaps = environmentFrameBuffers.size();
    for (int i = 1; i < countSubMaps; i++)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, environmentFrameBuffers[i].first);
        glDrawBuffers(6, drawBuffers);
        glViewport(0, 0, environmentFrameBuffers[i].second, environmentFrameBuffers[i].second);
        glBindImageTexture(0, environmentMapBuffer, i - 1, GL_TRUE, 6, GL_READ_ONLY, GL_RGBA16F);
        callDrawVertexBuffer();
    }

    disableVertexBuffer();
    glBindProgramPipeline(0);
    glViewport(0, 0, mainSystems->getResolutionSettings().width, mainSystems->getResolutionSettings().height);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void SkyBufferRender::callUpdate()
{
    onUpdate = true;
}

void SkyBufferRender::createVertexBuffer()
{
    mth::vec3 normalsSide[4];

    mth::vec3 leftBotton = mth::vec3(-1.0, -1.0, 1);
    leftBotton.normalise();
    normalsSide[0] = leftBotton;
    mth::vec3 leftTop = mth::vec3(-1.0, 1.0, 1);
    leftTop.normalise();
    normalsSide[1] = leftTop;
    mth::vec3 rightBotton = mth::vec3(1.0, -1.0, 1);
    rightBotton.normalise();
    normalsSide[2] = rightBotton;
    mth::vec3 rightTop = mth::vec3(1.0, 1.0, 1);
    rightTop.normalise();
    normalsSide[3] = rightTop;

    mth::quat rotate;
    mth::mat4 rotates[6];
    rotate.setEuler(0, 0, -180);
    rotates[0] = rotate.getMatrix();
    rotate.setEuler(0, 180, -180);
    rotates[1] = rotate.getMatrix();
    rotate.setEuler(-90, -90, 0);
    rotates[2] = rotate.getMatrix();
    rotate.setEuler(90, -90, 0);
    rotates[3] = rotate.getMatrix();
    rotate.setEuler(0, -90, -180);
    rotates[4] = rotate.getMatrix();
    rotate.setEuler(0, 90, -180);
    rotates[5] = rotate.getMatrix();

    mth::vec2 uvs[4] = {
        mth::vec2(0.0f, 0.0f),
        mth::vec2(0.0f, 1.0f),
        mth::vec2(1.0f, 0.0f),
        mth::vec2(1.0f, 1.0f),
    };

    glGenVertexArrays(1, &vertexArray);
    glBindVertexArray(vertexArray);
    
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);
    glEnableVertexAttribArray(4);
    glEnableVertexAttribArray(5);
    glEnableVertexAttribArray(6);

    glGenBuffers(1, &bufferData);
    glBindBuffer(GL_ARRAY_BUFFER, bufferData);
    glBufferData(GL_ARRAY_BUFFER, sizeof(mth::vec2) * 4 + sizeof(mth::vec3) * 4 * 6, NULL, GL_STATIC_DRAW);

    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(mth::vec2) * 4, &uvs);

    for (int i = 0; i < 6; i++)
    {
        std::array<mth::vec3, 4> normals;
        for (int i2 = 0; i2 < 4; i2++)
        {
            normals[i2] = rotates[i].transformNoMove(normalsSide[i2]);
        }

        glBufferSubData(GL_ARRAY_BUFFER, sizeof(mth::vec2) * 4 + sizeof(mth::vec3) * 4 * i, sizeof(mth::vec3) * 4, &normals);
    }
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLuint *)0);
    for (int i = 0; i < 6; i++)
    {
        glVertexAttribPointer(1 + i, 3, GL_FLOAT, GL_FALSE, 0, (GLuint *)(sizeof(mth::vec2) * 4 + sizeof(mth::vec3) * 4 * i));
    }

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void SkyBufferRender::destroyVertexBuffer()
{
    glDeleteVertexArrays(1, &vertexArray);
    glDeleteBuffers(1, &bufferData);
}

void SkyBufferRender::enableVertexBuffer()
{
    glBindVertexArray(vertexArray);
    glBindBuffer(GL_ARRAY_BUFFER, bufferData);
}
void SkyBufferRender::disableVertexBuffer()
{
    /*glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(3);
    glDisableVertexAttribArray(4);
    glDisableVertexAttribArray(5);
    glDisableVertexAttribArray(6);*/
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void SkyBufferRender::callDrawVertexBuffer()
{
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void SkyBufferRender::createEnvironmentMap()
{
    glGenTextures(1, &environmentMapBuffer);
    glBindTexture(GL_TEXTURE_CUBE_MAP, environmentMapBuffer);
    int sizeSide = mainSystems->getSettingsMainSystems()->graphicConfig.getGraphicTable().toInt("SizeEnvironmentBox");
    int level = 0;
    while (sizeSide >= 1)
    {
        GLuint fb;
        glGenFramebuffers(1, &fb);
        glBindFramebuffer(GL_FRAMEBUFFER, fb);
        for (int i = 0; i < 6; i++)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, level, GL_RGBA16F, sizeSide, sizeSide, 0, GL_RGBA, GL_FLOAT, NULL);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, environmentMapBuffer, level);
        }

        environmentFrameBuffers.push_back(std::make_pair(fb, sizeSide));
        level++;
        sizeSide /= 2;
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_REPEAT);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

void SkyBufferRender::clearEnvironmentMap()
{
    int count = environmentFrameBuffers.size();
    for (int i = 0; i < count; i++)
    {
        glDeleteFramebuffers(1, &environmentFrameBuffers[i].first);
    }
    environmentFrameBuffers.clear();
    glDeleteTextures(1, &environmentMapBuffer);
}