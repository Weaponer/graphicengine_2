#include "renderEngine/graphicsPipeline/lightRender/shadow/shadowsPointLights.h"
#include "renderEngine/graphicEngine.h"
#include "gameObject/transform.h"

ShadowsPointLights::ShadowsPointLights(MainFunctions *_mainFunctions, ShadowGenerator *_shadowGenerator)
    : mainFunctions(_mainFunctions), shadowGenerator(_shadowGenerator)
{
}

ShadowsPointLights::~ShadowsPointLights()
{
}

void ShadowsPointLights::update()
{
    RenderQueue *renderQueue = mainFunctions->getMainRenderQueue();
    int count = renderQueue->pointLightsData->countPointLights;
    for (int i = 0; i < count; i++)
    {
        PointLightGE *lightPoint = renderQueue->pointLightsData->pointLights[i];

        if (lightPoint->createShadows == false)
        {
            if (lightPoint->shadowCubeFrameBuffer != NULL)
            {
                delete lightPoint->shadowCubeFrameBuffer;
                lightPoint->shadowCubeFrameBuffer = NULL;
            }
            continue;
        }

        if (lightPoint->shadowCubeFrameBuffer == NULL)
        {
            lightPoint->shadowCubeFrameBuffer = new ShadowCubeFrameBuffer(1024);
        }
        mth::vec3 pos = lightPoint->position;
        lightPoint->shadowCubeFrameBuffer->setPosition(&pos, lightPoint->range);
        shadowGenerator->generateShadowCubeMap(lightPoint->shadowCubeFrameBuffer);
    }
}