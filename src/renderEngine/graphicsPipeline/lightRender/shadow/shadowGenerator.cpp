#include "renderEngine/graphicsPipeline/lightRender/shadow/shadowGenerator.h"
#include "renderEngine/graphicEngine.h"

#include "debugOutput/threadDebugOutput.h"

ShadowGenerator::ShadowGenerator(MainFunctions *_mainFunctions, ProgramActivator *_programActivator, TechnicalShaderStorage *_technicalShaderStorage)
    : mainFunctions(_mainFunctions), programActivator(_programActivator)
{
}

ShadowGenerator::~ShadowGenerator()
{
}

void ShadowGenerator::generateShadowMap(ShadowFrameBuffer *_shadowFrameBuffer)
{
    SettingsRenderQueue settingsQueue;
    settingsQueue.searchArea.bound = _shadowFrameBuffer->getBoundOBB();
    settingsQueue.searchArea.countClippingPlanes = 0;
    settingsQueue.searchArea.clippingPlanes = NULL;
    settingsQueue.sort = false;
    settingsQueue.getPointLights = false;
    settingsQueue.checkCamera = false;
    settingsQueue.maskPasses = TypePass::Shadow;
    settingsQueue.maskQueuesRender = QueueRender::QueueRender_Geometry | QueueRender::QueueRender_AlphaTest;
    settingsQueue.maskRenderTypes = RenderType::RenderType_Opaque | RenderType::RenderType_TransparentCutout;
    settingsQueue.generateInstancing = true;
    SettingsGraphicPipeline *settings = mainFunctions->getSettingsGraphicPipeline();
    if (settings->lodSettings.useLodsForShadow)
    {
        settingsQueue.useLodGroups = true;
        settingsQueue.cameraForCheckLod = mainFunctions->getBasicCamera()->getMainCamera();
        settingsQueue.lodBias = settings->lodSettings.getLodShadowBias();
    }
    else
    {
        settingsQueue.useLodGroups = false;
    }

    SettingsDrawRenderQueue settingsDrawRenderQueue;
    settingsDrawRenderQueue.renderStage = RS_SHADOW_STAGE;
    settingsDrawRenderQueue.typePass = TypePass::Shadow;
    settingsDrawRenderQueue.useInstancing = true;
    settingsDrawRenderQueue.maskQueueRender = QueueRender::QueueRender_Geometry | QueueRender::QueueRender_AlphaTest;
    settingsDrawRenderQueue.maskRenderTypes = RenderType::RenderType_Opaque | RenderType::RenderType_TransparentCutout;


    RenderQueue queue = mainFunctions->getRenderQueueGenerator()->generateRenderQueue(settingsQueue);

    mth::Vector2Int sizeTex = _shadowFrameBuffer->getSize();
    glViewport(0, 0, sizeTex.x, sizeTex.y);
    glEnable(GL_DEPTH_TEST);
    _shadowFrameBuffer->clear();
    glBindFramebuffer(GL_FRAMEBUFFER, _shadowFrameBuffer->getFrameBuffer());

    programActivator->drawRenderQueue(queue, _shadowFrameBuffer->getCameraMatricesData(), settingsDrawRenderQueue);
    mainFunctions->getRenderQueueGenerator()->releaseRenderQueue(queue);

    glDisable(GL_DEPTH_TEST);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void ShadowGenerator::generateShadowCubeMap(ShadowCubeFrameBuffer *_shadowCubeFrameBuffer)
{
    testCubeFrameBuffer = _shadowCubeFrameBuffer;
    int size = _shadowCubeFrameBuffer->getSizeSide();
    glViewport(0, 0, size, size);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glDepthMask(true);

    _shadowCubeFrameBuffer->clear();

    SettingsDrawRenderQueue settingsDrawRenderQueue;
    settingsDrawRenderQueue.renderStage = RS_SHADOW_STAGE;
    settingsDrawRenderQueue.typePass = TypePass::Shadow;
    settingsDrawRenderQueue.useInstancing = true;
    settingsDrawRenderQueue.maskQueueRender = QueueRender::QueueRender_Geometry | QueueRender::QueueRender_AlphaTest;
    settingsDrawRenderQueue.maskRenderTypes = RenderType::RenderType_Opaque | RenderType::RenderType_TransparentCutout;

    SettingsRenderQueue settingsQueue;
    settingsQueue.sort = false;
    settingsQueue.getPointLights = false;
    settingsQueue.checkCamera = false;
    settingsQueue.generateInstancing = true;
    settingsQueue.maskPasses = TypePass::Shadow;
    settingsQueue.maskQueuesRender = QueueRender::QueueRender_Geometry | QueueRender::QueueRender_AlphaTest;
    settingsQueue.maskRenderTypes = RenderType::RenderType_Opaque | RenderType::RenderType_TransparentCutout;
    SettingsGraphicPipeline *settings = mainFunctions->getSettingsGraphicPipeline();
    if (settings->lodSettings.useLodsForShadow)
    {
        settingsQueue.useLodGroups = true;
        settingsQueue.cameraForCheckLod = mainFunctions->getBasicCamera()->getMainCamera();
        settingsQueue.lodBias = settings->lodSettings.getLodShadowBias();
    }
    else
    {
        settingsQueue.useLodGroups = false;
    }

    RenderQueueGenerator *queueGenerator = mainFunctions->getRenderQueueGenerator();
    for (int i = 0; i < 6; i++)
    {
        settingsQueue.searchArea.bound = _shadowCubeFrameBuffer->getBoundOBB(i);
        settingsQueue.searchArea.countClippingPlanes = 4;
        std::array<mth::plane, 4> planes = *_shadowCubeFrameBuffer->getClipingPlanes(i);
        settingsQueue.searchArea.clippingPlanes = &(planes[0]);

        RenderQueue queue = queueGenerator->generateRenderQueue(settingsQueue);

        glBindFramebuffer(GL_FRAMEBUFFER, _shadowCubeFrameBuffer->getCubeFrameBuffer(i));
        programActivator->drawRenderQueue(queue, _shadowCubeFrameBuffer->getMatricesUBOData(i), settingsDrawRenderQueue);
        queueGenerator->releaseRenderQueue(queue);
    }

    glDisable(GL_DEPTH_TEST);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}