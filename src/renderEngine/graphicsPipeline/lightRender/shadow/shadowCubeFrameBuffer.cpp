#include <MTH/glGen_matrix.h>

#include "renderEngine/graphicsPipeline/lightRender/shadow/shadowCubeFrameBuffer.h"

ShadowCubeFrameBuffer::ShadowCubeFrameBuffer(float _sizeSide)
{
    sizeSide = _sizeSide;
    glGenFramebuffers(6, cubeFrameBuffers);
    glGenTextures(1, &cubeTexture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubeTexture);

    for (int i = 0; i < 6; i++)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, cubeFrameBuffers[i]);
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT32, sizeSide, sizeSide, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, cubeTexture, 0);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    nearPlane = 0.01f;
}

ShadowCubeFrameBuffer::~ShadowCubeFrameBuffer()
{
    glDeleteFramebuffers(6, cubeFrameBuffers);
    glDeleteTextures(1, &cubeTexture);
}

void ShadowCubeFrameBuffer::setPosition(const mth::vec3 *_position, float _distance)
{
    if (positionCenter != *_position || _distance != farPlane)
    {
        positionCenter = *_position;
        farPlane = _distance;
        calculate();
    }
}

const mth::vec3 *ShadowCubeFrameBuffer::getPosition() const
{
    return &positionCenter;
}

float ShadowCubeFrameBuffer::getSizeSide() const
{
    return sizeSide;
}

GLuint ShadowCubeFrameBuffer::getCubeFrameBuffer(int _index)
{
    return cubeFrameBuffers[_index];
}

GLuint ShadowCubeFrameBuffer::getCubeTexture()
{
    return cubeTexture;
}

const std::array<mth::plane, 4> *ShadowCubeFrameBuffer::getClipingPlanes(int _index) const
{
    return &planes[_index];
}

mth::boundOBB ShadowCubeFrameBuffer::getBoundOBB(int _index) const
{
    return bounds[_index];
}

CameraMatricesDataUBO *ShadowCubeFrameBuffer::getMatricesUBOData(int _index)
{
    return &(matricesSidesUBO[_index]);
}

float ShadowCubeFrameBuffer::getFarPlane() const
{
    return farPlane;
}

float ShadowCubeFrameBuffer::getNearPlane() const
{
    return nearPlane;
}

void ShadowCubeFrameBuffer::clear()
{
    for (int i = 0; i < 6; i++)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, cubeFrameBuffers[i]);
        glClear(GL_DEPTH_BUFFER_BIT);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void ShadowCubeFrameBuffer::calculate()
{

    mth::quat rotate;
    mth::mat4 translate = mth::translate(-positionCenter.x, -positionCenter.y, -positionCenter.z);
    rotate.setEuler(0, -90, 180);
    std::array<mth::mat4, 6> matricesV;
    matricesV[0] = rotate.getMatrix();
    matricesV[0].transpose();

    rotate.setEuler(0, 90, 180);
    matricesV[1] = rotate.getMatrix();
    matricesV[1].transpose();

    rotate.setEuler(-90, 180, 0);
    matricesV[2] = rotate.getMatrix();
    matricesV[2].transpose();

    rotate.setEuler(90, 180, 0);
    matricesV[3] = rotate.getMatrix();
    matricesV[3].transpose();

    rotate.setEuler(0, 0, 180);
    matricesV[4] = rotate.getMatrix();
    matricesV[4].transpose();

    rotate.setEuler(0, 180, 180);
    matricesV[5] = rotate.getMatrix();
    matricesV[5].transpose();

    float angle = 90.0f;
    mth::mat4 projectionMatrix = mth::perspective(angle, 1, nearPlane, farPlane);
    mth::mat4 invProjectionMatrix = mth::inversePerspective(angle, 1, nearPlane, farPlane);
    for (int i = 0; i < 6; i++)
    {
        CameraMatricesData cameraMatrices;
        cameraMatrices.direction = matricesV[i].transformNoMove(mth::vec3(0, 0, 1));
        cameraMatrices.matrix_V = matricesV[i] * translate;
        cameraMatrices.matrix_P = projectionMatrix;
        cameraMatrices.matrix_VP = projectionMatrix * cameraMatrices.matrix_V;
        cameraMatrices.matrixInverse_V = mth::inverseTRS(cameraMatrices.matrix_V);
        cameraMatrices.matrixInverse_P = invProjectionMatrix;
        cameraMatrices.position = positionCenter;
        cameraMatrices.far_plane = farPlane;
        cameraMatrices.near_plane = nearPlane;

        matricesSidesUBO[i].data = cameraMatrices;
        matricesSidesUBO[i].update();
    }

    std::array<mth::plane, 4> basicPlanes;

    float ab = farPlane * tan(angle * RADIAN / 2);
    mth::vec2 rightP = mth::vec2(-ab, farPlane);
    rightP = rightP.perpendecular();
    rightP.normalise();
    mth::vec2 leftP = mth::vec2(-rightP.x, rightP.y);
    basicPlanes[0] = mth::plane(0, mth::vec3(leftP.x, 0, leftP.y));
    basicPlanes[1] = mth::plane(0, mth::vec3(rightP.x, 0, rightP.y));

    mth::vec2 bottomP = mth::vec2(-ab, farPlane);
    bottomP = bottomP.perpendecular();
    bottomP.normalise();

    mth::vec2 topP = mth::vec2(-bottomP.x, bottomP.y);

    basicPlanes[2] = mth::plane(0, mth::vec3(0, topP.x, topP.y));
    basicPlanes[3] = mth::plane(0, mth::vec3(0, bottomP.x, bottomP.y));

    mth::boundOBB mainBound;
    mainBound.center = mth::vec3(0, 0, nearPlane + (farPlane - nearPlane));

    float halfSize = (farPlane - nearPlane) / 2;
    float fullDist = nearPlane + halfSize;
    for (int i = 0; i < 6; i++)
    {
        mth::vec3 dir = mth::vec3(0, 0, 1);
        dir = matricesV[i].transformNoMove(dir);
        bounds[i].center = dir * (fullDist) + positionCenter;
        bounds[i].size = mth::vec3(fullDist, fullDist, halfSize);
        bounds[i].rotate = mth::matrix3FromMatrix4(matricesV[i]);

        for (int i2 = 0; i2 < 4; i2++)
        {
            mth::vec3 normal = matricesV[i].transformNoMove(basicPlanes[i2].normal);
            planes[i][i2] = mth::plane(positionCenter, normal);
        }
    }
}