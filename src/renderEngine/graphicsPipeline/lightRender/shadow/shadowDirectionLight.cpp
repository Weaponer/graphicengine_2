#include <MTH/boundOBB.h>

#include "debugOutput/globalProfiler.h"
#include "renderEngine/graphicsPipeline/lightRender/shadow/shadowDirectionLight.h"
#include "renderEngine/graphicEngine.h"

ShadowDirectionLight::ShadowDirectionLight(SettingsGraphicPipeline *_settingsGraphicPipeline, ShadowGenerator *_shadowGenerator, MainSystems *_mainSystems)
    : settingsGraphicPipeline(_settingsGraphicPipeline), shadowGenerator(_shadowGenerator), mainSystems(_mainSystems)
{
    settingsGraphicPipeline->shadowDirectionLight.setShadowDistance(100);
    settingsGraphicPipeline->shadowDirectionLight.setStartFadeDistance(70);

    settingsGraphicPipeline->shadowDirectionLight.setMaxUpShadowDistance(100.0f);
    settingsGraphicPipeline->shadowDirectionLight.setSizeSide(2048);
    settingsGraphicPipeline->shadowDirectionLight.setDegreePartition(3);
    settingsGraphicPipeline->shadowDirectionLight.setShadowBias(0, 0.05f);
    settingsGraphicPipeline->shadowDirectionLight.setShadowBias(1, 2.4f);
    settingsGraphicPipeline->shadowDirectionLight.setShadowBias(2, 4.9f);
    settingsGraphicPipeline->shadowDirectionLight.reductionTypePartitionVolume = SDLD_ReductionType::SDLD_REDUCTION_TYPE_SQUARE;
    settingsGraphicPipeline->shadowDirectionLight.reductionTypeSizePartition = SDLD_ReductionType::SDLD_REDUCTION_TYPE_SQUARE;


    data = settingsGraphicPipeline->shadowDirectionLight;
    updateShadowBuffersFromData();
    dataUpdate = false;

    valueCrossShadowBlocks = _mainSystems->getSettingsMainSystems()->graphicConfig.getGraphicTable().toFloat("ValueCrossShadowBlocks");
}

ShadowDirectionLight::~ShadowDirectionLight()
{
    clear();
}

const SettingsGraphicPipeline::ShadowDirectionLightSettings *ShadowDirectionLight::getShadowDirectionLightData() const
{
    return &data;
}

int ShadowDirectionLight::getCountShadowBuffers() const
{
    return countUseShadowBuffers;
}

ShadowFrameBuffer *ShadowDirectionLight::getShadowBuffer(int _index)
{
    return shadowBuffers[_index];
}

float ShadowDirectionLight::getDistanceShadowCascade(int _index)
{
    return maxDistancesParts[_index];
}

mth::vec3 ShadowDirectionLight::getDirectionBoundAlongCamera()
{
    return directionBoundAlongCamera;
}

void ShadowDirectionLight::update()
{
    SettingsGraphicPipeline::ShadowDirectionLightSettings newData = settingsGraphicPipeline->shadowDirectionLight;

    if (std::memcmp(&newData, &data, sizeof(SettingsGraphicPipeline::ShadowDirectionLightSettings)) != 0)
    {
        data = newData;
        updateShadowBuffersFromData();
    }
    fillShadowBuffers();
}

void ShadowDirectionLight::clear()
{
    int count = shadowBuffers.size();
    for (int i = 0; i < count; i++)
    {
        delete shadowBuffers[i];
    }
    shadowBuffers.clear();
}

void ShadowDirectionLight::updateShadowBuffersFromData()
{
    clear();
    int degreePartition = data.getDegreePartition();
    int sizeSide = data.getSizeSide();
    for (int i = 0; i < degreePartition; i++)
    {
        int size = sizeSide;
        if (data.reductionTypeSizePartition == SDLD_REDUCTION_TYPE_LINEAR)
        {
            size = sizeSide - sizeSide / degreePartition * i;
        }
        else
        {
            if (i != 0)
            {
                size = shadowBuffers[i - 1]->getStandartSize().x / 2;
            }
        }

        if (size < 16)
        {
            size = 16;
        }
        ShadowFrameBuffer *shadowFB = new ShadowFrameBuffer(size, size);
        shadowBuffers.push_back(shadowFB);
    }

    float shadowDistance = data.getShadowDistance();
    distancesParts[0] = 0;
    if (data.reductionTypePartitionVolume == SDLD_REDUCTION_TYPE_LINEAR)
    {
        float x = 1.0f / degreePartition;

        for (int i = 1; i <= degreePartition; i++)
        {
            distancesParts[i] = distancesParts[i - 1] + x;
        }
    }
    else
    {
        distancesParts[degreePartition] = 1.0f;
        int c = 1;
        for (int i = 1; i < degreePartition; i++)
        {
            c += pow(2, i);
        }
        float x = 1.0f / c;
        for (int i = 0; i < degreePartition; i++)
        {
            distancesParts[i + 1] = distancesParts[i] + x * pow(2, i);
        }
    }

    float maxDisatnce = settingsGraphicPipeline->shadowDirectionLight.getShadowDistance();
    for (int i = 0; i < MAX_SHADOWS_ON_DIRECTION_LIGHTS; i++)
    {
        maxDistancesParts[i] = distancesParts[i + 1] * maxDisatnce;
    }
    float _ = 0.0f;
}

void ShadowDirectionLight::fillShadowBuffers()
{

    int degreePartition = settingsGraphicPipeline->shadowDirectionLight.getDegreePartition();
    CameraGE *camera = mainSystems->getMainFunctions()->getBasicCamera()->getMainCamera();

    std::array<mth::boundOBB, MAX_SHADOWS_ON_DIRECTION_LIGHTS> boundsShadows;
    std::array<mth::mat4, MAX_SHADOWS_ON_DIRECTION_LIGHTS> shadowsMatV;
    std::array<mth::mat4, MAX_SHADOWS_ON_DIRECTION_LIGHTS> shadowsMatP;
    std::array<mth::mat4, MAX_SHADOWS_ON_DIRECTION_LIGHTS> shadowsMatInvV;
    std::array<mth::mat4, MAX_SHADOWS_ON_DIRECTION_LIGHTS> shadowsMatInvP;
    std::array<float, MAX_SHADOWS_ON_DIRECTION_LIGHTS> shadowsFarPlanes;
    std::array<float, MAX_SHADOWS_ON_DIRECTION_LIGHTS> shadowsNearPlanes;
    std::array<float, MAX_SHADOWS_ON_DIRECTION_LIGHTS> shadowsAspects;
    countUseShadowBuffers = testNewGenerationBoundsDirectionLight(&boundsShadows[0], &shadowsMatV[0], &shadowsMatP[0],
                                                                  &shadowsMatInvV[0], &shadowsMatInvP[0], &shadowsFarPlanes[0], &shadowsNearPlanes[0], &shadowsAspects[0]);
    for (int i = 0; i < countUseShadowBuffers; i++)
    {
        CameraMatricesDataUBO *cameraMatricesDataUBO = shadowBuffers[i]->getCameraMatricesData();
        cameraMatricesDataUBO->data.matrix_V = shadowsMatV[i];
        cameraMatricesDataUBO->data.matrix_P = shadowsMatP[i];
        cameraMatricesDataUBO->data.matrix_VP = shadowsMatP[i] * shadowsMatV[i];
        cameraMatricesDataUBO->data.matrixInverse_P = shadowsMatInvP[i];
        cameraMatricesDataUBO->data.matrixInverse_V = shadowsMatInvV[i];
        cameraMatricesDataUBO->data.far_plane = shadowsFarPlanes[i];
        cameraMatricesDataUBO->data.near_plane = shadowsNearPlanes[i];

        mth::vec3 e = camera->rotate.getEuler();
        mth::vec3 p = camera->position;

        shadowBuffers[i]->setBoundOBB(boundsShadows[i]);
        shadowBuffers[i]->updateDataCameraMatrices();
        shadowBuffers[i]->setUseAspect(shadowsAspects[i]);

        shadowGenerator->generateShadowMap(shadowBuffers[i]);
    }
}

mth::mat4 ShadowDirectionLight::generateBasisDirectionLight(const mth::vec3 &normal)
{
    mth::mat3 mat;
    mth::vec3 up = mth::vec3(0, 1, 0);
    mth::vec3 right;
    if (up == normal)
    {
        up = mth::vec3(-1, 0, 0);
        right = normal.vectorProduct(up);
    }
    else if (mth::vec3(0, -1, 0) == normal)
    {
        up = mth::vec3(-1, 0, 0);
        right = up.vectorProduct(normal);
    }
    else
    {
        right = normal.vectorProduct(up);
        right.normalise();
        up = right.vectorProduct(normal);
    }
    mat.setOrientation(right, up, normal);
    return mth::mat4(mat);
}

float ShadowDirectionLight::getYOnLines(float _x, int _countLines, const std::array<mth::vec3, 3> *_lines)
{
    if (_x >= _lines->at(0).x)
    {
        return _lines->at(0).y;
    }
    else
    {
        int maxCount = _countLines - 1;
        if (_lines->at(maxCount).x >= _x)
        {
            return _lines->at(maxCount).y;
        }

        mth::vec3 oneVec = _lines->at(0);
        mth::vec3 twoVec = _lines->at(1);
        if (maxCount == 1)
        {

            float value = (_x - oneVec.x) / (twoVec.x - oneVec.x);
            return oneVec.y * (1.0f - value) + twoVec.y * value;
        }
        else
        {
            if (oneVec.x >= _x && twoVec.x <= _x)
            {
                float value = (_x - oneVec.x) / (twoVec.x - oneVec.x);
                return oneVec.y * (1.0f - value) + twoVec.y * value;
            }
            else
            {
                oneVec = _lines->at(1);
                twoVec = _lines->at(2);
                float value = (_x - oneVec.x) / (twoVec.x - oneVec.x);
                return oneVec.y * (1.0f - value) + twoVec.y * value;
            }
        }
    }
}

int ShadowDirectionLight::testNewGenerationBoundsDirectionLight(mth::boundOBB *_boundsShadow, mth::mat4 *_shadow_V, mth::mat4 *_shadow_P,
                                                                mth::mat4 *_shadowInverse_V, mth::mat4 *_shadowInverse_P,
                                                                float *_shadowFarPlane, float *_shadowNearPlane, float *_shadowAspects)
{
    CameraGE *camera = mainSystems->getMainFunctions()->getBasicCamera()->getMainCamera();
    mth::vec3 position = mth::vec3(0, 0, 0);
    mth::quat rotate = mth::quat(0.209226f, -0.670981f, 0.206207f, 0.680805f);
   rotate.setEuler(30, 225, 0);

    rotate = camera->rotate;
    position = camera->position;
    /*std::cout << rotate.x << "f, " << rotate.y << "f, " << rotate.z << "f, " << rotate.w << "f, " << std::endl;
    std::cout << position.x << "f, " << position.y << "f, " << position.z << "f, " << std::endl;*/
    mth::vec3 direction = rotate.rotateVector(mth::vec3(0, 0, 1));

    mth::vec3 fPoints[4];
    float far = std::min(camera->farPlane, settingsGraphicPipeline->shadowDirectionLight.getShadowDistance());
    for (int i = 0; i < 4; i++)
    {
        mth::vec3 dir = camera->pointsPiramidClipping[i + 4] - camera->pointsPiramidClipping[i];
        dir.normalise();
        dir = rotate.rotateVector(dir);
        float ang = (dir * direction);
        float h = far / ang;
        fPoints[i] = position + dir * h;
    }

    /*mth::mat4 rot = mth::rotate(56.3863f, 16.6245f, 4.00958e-06f);
    mth::vec3 position = mth::vec3(-11.8276f, 40.4425f, -20.6013f);
    mth::vec3 direction = rot.transform(mth::vec3(0, 0, 1));
    float far = 200.0f;
    mth::vec3 fPoints[4];

    mth::vec3 normals[4];
    normals[0] = mth::vec3(0.8f, 0.55f, 1);
    normals[0].normalise();
    normals[0] = rot.transform(normals[0]);
    normals[1] = mth::vec3(0.8f, -0.55f, 1);
    normals[1].normalise();
    normals[1] = rot.transform(normals[1]);
    normals[2] = mth::vec3(-0.8f, 0.55f, 1);
    normals[2].normalise();
    normals[2] = rot.transform(normals[2]);
    normals[3] = mth::vec3(-0.8f, -0.55f, 1);
    normals[3].normalise();
    normals[3] = rot.transform(normals[3]);*/

    for (int i = 0; i < 4; i++)
    {
        // fPoints[i] = position + normals[i] * far;
        //ThreadDebugOutput::mainThreadDebugOutput->drawLine(position, fPoints[i], mth::vec3(0, 0, 1));
    }

    mth::vec3 F0 = position + direction * far;

    mth::vec3 directionLight = settingsGraphicPipeline->sky.directionSun;
    directionLight.invert();

    mth::mat4 basis;
    bool subBounds = false;
    float angle = abs(directionLight * direction);
    int countBounds = settingsGraphicPipeline->shadowDirectionLight.getDegreePartition();

    if (angle <= 0.45f && countBounds != 1)
    {
        mth::vec3 cross = direction % directionLight;
        cross.normalise();
        mth::vec3 T = cross % directionLight;
        T.normalise();
        mth::mat3 mat3Basis;
        mat3Basis.setOrientation(T, cross, directionLight);
        basis = mth::mat4(mat3Basis);
        directionBoundAlongCamera = T * -1;
    }
    else
    {
        subBounds = true;
        basis = generateBasisDirectionLight(directionLight);
        directionBoundAlongCamera = directionLight;
    }

    //ThreadDebugOutput::mainThreadDebugOutput->drawLine(position,position + directionBoundAlongCamera * data.getShadowDistance(), mth::vec3(1, 0, 0));

    mth::mat4 rotateBasis = basis;
    basis = mth::translate(position.x, position.y, position.z) * rotateBasis;

    mth::mat4 invRotateBasis = rotateBasis;
    invRotateBasis.transpose();
    mth::mat4 invBasis = invRotateBasis * mth::translate(-position.x, -position.y, -position.z);

    //ThreadDebugOutput::mainThreadDebugOutput->drawLine(position, position + rotateBasis.transform(mth::vec3(1, 0, 0)), mth::vec3(1, 0, 0));
    //ThreadDebugOutput::mainThreadDebugOutput->drawLine(position, position + rotateBasis.transform(mth::vec3(0, 1, 0)), mth::vec3(0, 1, 0));
    //ThreadDebugOutput::mainThreadDebugOutput->drawLine(position, position + rotateBasis.transform(mth::vec3(0, 0, 1)), mth::vec3(0, 0, 1));

    mth::vec3 pointsInBasis[5];
    pointsInBasis[0] = mth::vec3(0, 0, 0);
    for (int i = 1; i < 5; i++)
    {
        pointsInBasis[i] = invBasis.transform(fPoints[i - 1]);
    }

    float minX = pointsInBasis[0].x;
    float minY = pointsInBasis[0].y;
    float minZ = pointsInBasis[0].z;

    float maxX = pointsInBasis[0].x;
    float maxY = pointsInBasis[0].y;
    float maxZ = pointsInBasis[0].z;

    for (int i = 1; i < 5; i++)
    {
        if (pointsInBasis[i].x < minX)
        {
            minX = pointsInBasis[i].x;
        }
        else if (pointsInBasis[i].x > maxX)
        {
            maxX = pointsInBasis[i].x;
        }

        if (pointsInBasis[i].y < minY)
        {
            minY = pointsInBasis[i].y;
        }
        else if (pointsInBasis[i].y > maxY)
        {
            maxY = pointsInBasis[i].y;
        }

        if (pointsInBasis[i].z < minZ)
        {
            minZ = pointsInBasis[i].z;
        }
        else if (pointsInBasis[i].z > maxZ)
        {
            maxZ = pointsInBasis[i].z;
        }
    }

    float shadowDistance = data.getShadowDistance();
    float maxUpShadowDistance = data.getMaxUpShadowDistance();

    if(!subBounds)
    {
        minX = std::max(minX, -shadowDistance);
    }

    minZ -= maxUpShadowDistance;

    mth::vec3 maxGlobalBound = mth::vec3(maxX, maxY, maxZ);
    mth::vec3 minGlobalBound = mth::vec3(minX, minY, minZ);
    mth::vec3 centerGlobalBound = (maxGlobalBound + minGlobalBound) / 2;
    mth::vec3 sizeGlobalBound = (maxGlobalBound - minGlobalBound) / 2.0f;
    sizeGlobalBound = mth::vec3(abs(sizeGlobalBound.x), abs(sizeGlobalBound.y), abs(sizeGlobalBound.z));

    mth::boundOBB bounds[8];
    if (subBounds)
    {

        for (int i = 0; i < countBounds; i++)
        {
            float value = distancesParts[i + 1];
            mth::vec3 pos = centerGlobalBound;
            pos.x *= value;
            pos.y *= value;
            mth::vec3 size = sizeGlobalBound;
            size.x *= value;
            size.y *= value;
            bounds[i] = mth::boundOBB(basis.transform(pos), mth::matrix3FromMatrix4(rotateBasis), size);
        }
    }
    else
    {
        int countUpLines = 0;
        std::array<mth::vec3, 3> upYLines;
        int countDownLines = 0;
        std::array<mth::vec3, 3> downYLines;
        for (int i = 0; i < 5; i++)
        {
            if (pointsInBasis[i].y >= 0)
            {
                upYLines[countUpLines] = pointsInBasis[i];
                countUpLines++;
            }
            if (pointsInBasis[i].y <= 0)
            {
                downYLines[countDownLines] = pointsInBasis[i];
                countDownLines++;
            }
        }
        std::sort(upYLines.begin(), upYLines.end(), [](const mth::vec3 &_vec1, const mth::vec3 &_vec2)
                  { return _vec1.x > _vec2.x; });
        std::sort(downYLines.begin(), downYLines.end(), [](const mth::vec3 &_vec1, const mth::vec3 &_vec2)
                  { return _vec1.x > _vec2.x; });

        int resCountUpLines = 0;
        int resCountDownLines = 0;
        std::array<mth::vec3, 3> resUpYLines;
        std::array<mth::vec3, 3> resDownYLines;

        if (countUpLines < 3 || !(upYLines[0].y <= upYLines[1].y && upYLines[2].y >= upYLines[1].y))
        {
            resUpYLines = upYLines;
            resCountUpLines = countUpLines;
        }
        else
        {
            resCountUpLines = 2;
            resUpYLines[0] = upYLines[0];
            resUpYLines[1] = upYLines[2];
        }

        if (countDownLines < 3 || !(downYLines[0].y >= downYLines[1].y && downYLines[2].y <= downYLines[1].y))
        {
            resDownYLines = downYLines;
            resCountDownLines = countDownLines;
        }
        else
        {
            resCountDownLines = 2;
            resDownYLines[0] = downYLines[0];
            resDownYLines[1] = downYLines[2];
        }

        for (int i = 0; i < resCountUpLines - 1; i++)
        {
            mth::vec3 posOne = resUpYLines[i];
            posOne.z = 0;
            posOne = basis.transform(posOne);
            mth::vec3 posTwo = resUpYLines[i + 1];
            posTwo.z = 0;
            posTwo = basis.transform(posTwo);
            //ThreadDebugOutput::mainThreadDebugOutput->drawLine(posOne, posTwo, mth::vec3(1, 1, 0));
        }

        for (int i = 0; i < resCountDownLines; i++)
        {
            mth::vec3 posOne = resDownYLines[i];
            posOne.z = 0;
            posOne = basis.transform(posOne);
            mth::vec3 posTwo = resDownYLines[i + 1];
            posTwo.z = 0;
            posTwo = basis.transform(posTwo);
            //ThreadDebugOutput::mainThreadDebugOutput->drawLine(posOne, posTwo, mth::vec3(0, 1, 1));
        }

        for (int i = 0; i < countBounds; i++)
        {
            float distOne = distancesParts[i];
            float distTwo = distancesParts[i + 1] + valueCrossShadowBlocks;

            float xMin = maxX * (1.0f - distOne) + minX * distOne;
            float xMax = maxX * (1.0f - distTwo) + minX * distTwo;

            mth::vec3 minLocalBound = minGlobalBound;
            mth::vec3 maxLocalBound = maxGlobalBound;

            minLocalBound.x = xMin;
            maxLocalBound.x = xMax;

            float t1 = getYOnLines(xMax, resCountDownLines, &resDownYLines);
            float t2 = getYOnLines(xMin, resCountDownLines, &resDownYLines);

            minLocalBound.y = std::min(t1, t2);
            maxLocalBound.y = std::max(getYOnLines(xMax, resCountUpLines, &resUpYLines),
                                       getYOnLines(xMin, resCountUpLines, &resUpYLines));

            mth::vec3 centerLocalBound = (maxLocalBound + minLocalBound) / 2;
            mth::vec3 sizeLocalBound = (maxLocalBound - minLocalBound) / 2.0f;
            sizeLocalBound = mth::vec3(abs(sizeLocalBound.x), abs(sizeLocalBound.y), abs(sizeLocalBound.z));

            bounds[i] = mth::boundOBB(basis.transform(centerLocalBound), mth::matrix3FromMatrix4(rotateBasis), sizeLocalBound);
        }
    }

    for (int i = 0; i < countBounds; i++)
    {
        //ThreadDebugOutput::mainThreadDebugOutput->drawCube(bounds[i].center, bounds[i].size, rotateBasis, mth::vec3(0, 1, 0));
    }

    for (int i = 0; i < countBounds; i++)
    {
        _boundsShadow[i] = bounds[i];
        mth::vec3 size = bounds[i].size;
        float lenght = size.z * 2;
        mth::vec3 pos = bounds[i].center - bounds[i].rotate.getVector(2) * size.z;
        _shadow_V[i] = invRotateBasis * mth::translate(-pos.x, -pos.y, -pos.z);
        _shadowInverse_V[i] = mth::translate(pos.x, pos.y, pos.z) * rotateBasis;

        _shadow_P[i] = mth::ortho(-size.x, size.x, -size.y, size.y, 0, lenght);
        _shadowInverse_P[i] = mth::inverseOrtho(-size.x, size.x, -size.y, size.y, 0, lenght);
        if (!subBounds)
        {
            float aspect = size.y / size.x;
            _shadowAspects[i] = aspect;
        }
        else
        {
            _shadowAspects[i] = 1.0f;
        }

        _shadowFarPlane[i] = lenght;
        _shadowNearPlane[i] = 0;

        // ThreadDebugOutput::mainThreadDebugOutput->drawLine(pos, pos + rotateBasis.transform(mth::vec3(1, 0, 0)), mth::vec3(1, 0, 0));
        // ThreadDebugOutput::mainThreadDebugOutput->drawLine(pos, pos + rotateBasis.transform(mth::vec3(0, 1, 0)), mth::vec3(0, 1, 0));
        // ThreadDebugOutput::mainThreadDebugOutput->drawLine(pos, pos + rotateBasis.transform(mth::vec3(0, 0, 1)), mth::vec3(0, 0, 1));
    }

    // ThreadDebugOutput::mainThreadDebugOutput->drawLine(mth::vec3(), (mth::vec3(1, 0, 0)), mth::vec3(1, 0, 0));
    // ThreadDebugOutput::mainThreadDebugOutput->drawLine(mth::vec3(), (mth::vec3(0, 1, 0)), mth::vec3(0, 1, 0));
    // ThreadDebugOutput::mainThreadDebugOutput->drawLine(mth::vec3(), (mth::vec3(0, 0, 1)), mth::vec3(0, 0, 1));
    return countBounds;
}