#include "renderEngine/graphicsPipeline/lightRender/shadow/shadowFrameBuffer.h"

ShadowFrameBuffer::ShadowFrameBuffer(int _widht, int _height) : matricesDataUBO(GL_DYNAMIC_DRAW)
{
    aspectTextures[0] = 1.0f;
    aspectTextures[1] = 1.5f;
    aspectTextures[2] = 1.75f;
    aspectTextures[3] = 2.0f;
    for (int i = 0; i < MAX_COUNT_VARIANTS_ASPECTS_SHADOW; i++)
    {
        texturesSizes[i] = mth::Vector2Int(_widht, _height * aspectTextures[i]);
    }

    currentUseIndex = 0;

    for (int i = 0; i < MAX_COUNT_VARIANTS_ASPECTS_SHADOW; i++)
    {
        glGenFramebuffers(1, &(framebuffers[i]));
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffers[i]);
        glGenTextures(1, &(textures[i]));
        glBindTexture(GL_TEXTURE_2D, textures[i]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, texturesSizes[i].x, texturesSizes[i].y, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, textures[i], 0);
        glClearDepthf(1.0f);
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
}

ShadowFrameBuffer::~ShadowFrameBuffer()
{
    glDeleteTextures(MAX_COUNT_VARIANTS_ASPECTS_SHADOW, &textures[0]);
    glDeleteFramebuffers(MAX_COUNT_VARIANTS_ASPECTS_SHADOW, &framebuffers[0]);
}

void ShadowFrameBuffer::setBoundOBB(const mth::boundOBB &_boundOBB)
{
    boundOBB = _boundOBB;
}

mth::boundOBB ShadowFrameBuffer::getBoundOBB() const
{
    return boundOBB;
}

CameraMatricesDataUBO *ShadowFrameBuffer::getCameraMatricesData()
{
    return &matricesDataUBO;
}

void ShadowFrameBuffer::updateDataCameraMatrices()
{
    matricesDataUBO.update();
}

mth::Vector2Int ShadowFrameBuffer::getSize() const
{
    return texturesSizes[currentUseIndex];
}

mth::Vector2Int ShadowFrameBuffer::getStandartSize() const
{
    return texturesSizes[0];
}

GLuint ShadowFrameBuffer::getFrameBuffer() const
{
    return framebuffers[currentUseIndex];
}

GLuint ShadowFrameBuffer::getShadowTexture() const
{
    return textures[currentUseIndex];
}

void ShadowFrameBuffer::setUseAspect(float _aspect)
{
    if (_aspect <= aspectTextures[0])
    {
        currentUseIndex = 0;
        return;
    }
    for (int i = 0; i < MAX_COUNT_VARIANTS_ASPECTS_SHADOW - 1; i++)
    {
        if (_aspect > aspectTextures[i] && _aspect <= aspectTextures[i + 1])
        {
            currentUseIndex = i;
            return;
        }
    }
    currentUseIndex = MAX_COUNT_VARIANTS_ASPECTS_SHADOW - 1;
}

void ShadowFrameBuffer::clear()
{
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffers[currentUseIndex]);
    glClear(GL_DEPTH_BUFFER_BIT);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}