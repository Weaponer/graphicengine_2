#include "renderEngine/graphicEngine.h"
#include "renderEngine/graphicsPipeline/lightRender/combinerResultLightBuffer.h"

CombinerResultLightBuffer::CombinerResultLightBuffer(ResolutionSettings _resolution, SettingsGraphicPipeline *_settingsGraphicPipeline, CameraVertexesData *_CVD,
                                                     ColorFrameBuffer *_resultLightBuffer, TechnicalShaderStorage *_technicalShaderStorage)
    : defaultResolution(_resolution), settingsGraphicPipeline(_settingsGraphicPipeline), CVD(_CVD), resultFrameBuffer(_resultLightBuffer)
{
    return;
    
    combiner.setPipeline(_technicalShaderStorage->getTShader("VertexScreen"),
                         _technicalShaderStorage->getTShader("CombineBeforeBloom"));

    combinerData.enableFog = _settingsGraphicPipeline->volumetricFog.generateFog;
    combinerData._m1[0] = { false };
    combinerData._m1[1] = { false };
    combinerData._m1[2] = { false };
    combinerData.enableSSAO = _settingsGraphicPipeline->postEffects.SSAO.enable;
    combinerData._m2[0] = { false };
    combinerData._m2[1] = { false };
    combinerData._m2[2] = { false };
    combinerData.enableBloom = _settingsGraphicPipeline->postEffects.bloom.enable;
    combinerData._m3[0] = { false };
    combinerData._m3[1] = { false };
    combinerData._m3[2] = { false };

    glGenBuffers(1, &combinerDataUBO);
    glBindBuffer(GL_UNIFORM_BUFFER, combinerDataUBO);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(CombinerData), &combinerData, GL_STATIC_DRAW);

    bloomGenerator.setPipeline(_technicalShaderStorage->getTShader("VertexScreen"),
                               _technicalShaderStorage->getTShader("BloomLight"));

    generateTextures();

    bloomCombiner.setPipeline(_technicalShaderStorage->getTShader("VertexScreen"),
                              _technicalShaderStorage->getTShader("CombineWithBloom"));

    for (int i = 1; i <= MAX_SAMPLES_BLOOM_EFFECT; i++)
    {
        GLint location = bloomCombiner.getLocUniformFragment(("_bloomSubMaps[" + std::to_string(i) + "]").c_str());
        glProgramUniform1i(bloomCombiner.getFragmentProgram(), location, i);
    }

    bloomCombinerData.countSubMaps = _settingsGraphicPipeline->postEffects.bloom.getCountSamples();
    bloomCombinerData.forceBloom = _settingsGraphicPipeline->postEffects.bloom.getForce();

    glGenBuffers(1, &bloomCombinerDataUBO);
    glBindBuffer(GL_UNIFORM_BUFFER, bloomCombinerDataUBO);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(BloomCombinerData), &bloomCombinerData, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

CombinerResultLightBuffer::~CombinerResultLightBuffer()
{
    glDeleteBuffers(1, &combinerDataUBO);
    glDeleteBuffers(1, &bloomCombinerDataUBO);

    glDeleteFramebuffers(MAX_SAMPLES_BLOOM_EFFECT, framebuffersBloom);
    glDeleteTextures(MAX_SAMPLES_BLOOM_EFFECT, texturesBloom);

    glDeleteFramebuffers(1, &framebufferFull);
    glDeleteTextures(1, &textureFull);
}

void CombinerResultLightBuffer::setOtherFrameBuffers(ColorFrameBuffer *_skyBuffer,
                                                     ColorFrameBuffer *_basicColorBuffer,
                                                     SSAOBufferRender *_ssaoBuffer,
                                                     ColorFrameBuffer *_volumetricFogBuffer)
{
    skyBuffer = _skyBuffer;
    basicColorBuffer = _basicColorBuffer;
    ssaoBuffer = _ssaoBuffer;
    volumetricFogBuffer = _volumetricFogBuffer;
}

void CombinerResultLightBuffer::update()
{
    CombinerData newCombinerData;
    std::memcpy(&newCombinerData, &combinerData, sizeof(CombinerData));
    newCombinerData.enableFog = settingsGraphicPipeline->volumetricFog.generateFog;
    newCombinerData.enableSSAO = settingsGraphicPipeline->postEffects.SSAO.enable;
    newCombinerData.enableBloom = settingsGraphicPipeline->postEffects.bloom.enable;

    bool updateUniforms = false;

    if (std::memcmp(&newCombinerData, &combinerData, sizeof(CombinerData)) != 0)
    {
        std::memcpy(&combinerData, &newCombinerData, sizeof(CombinerData));
        updateUniforms = true;
    }

    BloomCombinerData newBloomCombinerData;
    std::memcpy(&newBloomCombinerData, &bloomCombinerData, sizeof(BloomCombinerData));
    newBloomCombinerData.countSubMaps = settingsGraphicPipeline->postEffects.bloom.getCountSamples();
    newBloomCombinerData.forceBloom = settingsGraphicPipeline->postEffects.bloom.getForce();

    if (std::memcmp(&newBloomCombinerData, &bloomCombinerData, sizeof(BloomCombinerData)) != 0)
    {
        std::memcpy(&bloomCombinerData, &newBloomCombinerData, sizeof(BloomCombinerData));
        updateUniforms = true;
    }

    if (updateUniforms)
    {
        updateUniformBuffer();
    }
    CVD->enableVertexBuffers();

    if (combinerData.enableBloom == false)
    {
        resultFrameBuffer->enable();
        combiner.enablePipeline();
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, combinerDataUBO);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, skyBuffer->getColorTexture());
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, basicColorBuffer->getColorTexture());
        glActiveTexture(GL_TEXTURE2);
        //glBindTexture(GL_TEXTURE_2D, ssaoBuffer->getResultTexture());
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, volumetricFogBuffer->getColorTexture());
        
        CVD->callDraw();
        resultFrameBuffer->disable();
    }
    else
    {
        glBindFramebuffer(GL_FRAMEBUFFER, framebufferFull);
        combiner.enablePipeline();
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, combinerDataUBO);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, skyBuffer->getColorTexture());
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, basicColorBuffer->getColorTexture());
        glActiveTexture(GL_TEXTURE2);
        //glBindTexture(GL_TEXTURE_2D, ssaoBuffer->getResultTexture());
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, volumetricFogBuffer->getColorTexture());

        GLenum buffers[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
        glDrawBuffers(2, buffers);

        CVD->callDraw();

        glDrawBuffers(1, buffers);

        bloomGenerator.enablePipeline();
        for (int i = 1; i < bloomCombinerData.countSubMaps; i++)
        {
            glBindFramebuffer(GL_FRAMEBUFFER, framebuffersBloom[i]);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, texturesBloom[i - 1]);
            glViewport(0, 0, sizes[i].x, sizes[i].y);
            CVD->callDraw();
        }

        glViewport(0, 0, defaultResolution.width, defaultResolution.height);
        resultFrameBuffer->enable();

        bloomCombiner.enablePipeline();
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, bloomCombinerDataUBO);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureFull);
        for (int i = 0; i < bloomCombinerData.countSubMaps; i++)
        {
            glActiveTexture(GL_TEXTURE1 + i);
            glBindTexture(GL_TEXTURE_2D, texturesBloom[i]);
        }
        CVD->callDraw();
        resultFrameBuffer->disable();
    }

    CVD->disableVertexBuffers();
}

void CombinerResultLightBuffer::updateUniformBuffer()
{
    glBindBuffer(GL_UNIFORM_BUFFER, combinerDataUBO);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(CombinerData), &combinerData);

    glBindBuffer(GL_UNIFORM_BUFFER, bloomCombinerDataUBO);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(BloomCombinerData), &bloomCombinerData);

    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void CombinerResultLightBuffer::generateTextures()
{
    oldSize = mth::Vector2Int(defaultResolution.width, defaultResolution.height);

    glGenFramebuffers(MAX_SAMPLES_BLOOM_EFFECT, framebuffersBloom);
    glGenTextures(MAX_SAMPLES_BLOOM_EFFECT, texturesBloom);
    for (int i = 0; i < MAX_SAMPLES_BLOOM_EFFECT; i++)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffersBloom[i]);
        sizes[i] = mth::Vector2Int(oldSize.x / (i + 1), oldSize.y / (i + 1));
        glBindTexture(GL_TEXTURE_2D, texturesBloom[i]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_R11F_G11F_B10F, sizes[i].x, sizes[i].y, 0, GL_RGB, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texturesBloom[i], 0);
    }

    glGenTextures(1, &textureFull);

    glBindTexture(GL_TEXTURE_2D, textureFull);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, oldSize.x, oldSize.y, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glGenFramebuffers(1, &framebufferFull);
    glBindFramebuffer(GL_FRAMEBUFFER, framebufferFull);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureFull, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, texturesBloom[0], 0);

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}