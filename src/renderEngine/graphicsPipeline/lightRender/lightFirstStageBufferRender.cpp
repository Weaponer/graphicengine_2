#include "renderEngine/graphicsPipeline/lightRender/lightFirstStageBufferRender.h"
#include "renderEngine/graphicEngine.h"

LightFirstStageBufferRender::LightFirstStageBufferRender(MainFunctions *_mainFunctions, TechnicalShaderStorage *_TSS,
                                                         RenderShadowDirectionLight *_renderShadowDirectionLight, LightBasicFrameBuffer *_lightBasicFrameBuffer,
                                                         ColorFrameBuffer *_normalBuffer, MainDepthBuffer *_depthBuffer)
    : mainFunctions(_mainFunctions), renderShadowDirectionLight(_renderShadowDirectionLight),
      lightBasicFrameBuffer(_lightBasicFrameBuffer), normalBuffer(_normalBuffer), depthBuffer(_depthBuffer)
{
    pointLightPassProgram.setPipeline(_TSS->getTShader("VertexScreen"), _TSS->getTShader("PointLightPass"));
    pointLightShadowPassProgram.setPipeline(_TSS->getTShader("VertexScreen"), _TSS->getTShader("PointLightShadowPass"));
    mainLightCombinerProgram.setPipeline(_TSS->getTShader("VertexScreen"), _TSS->getTShader("MainLightCombiner"));

    glGenBuffers(1, &allLightsPointsDataUBO);
    glGenBuffers(1, &lightsPointsWithoutShadowDataUBO);
    glGenBuffers(MAX_COUNT_POINT_LIGHTS, pointsLightsShadowsDatasUBO);
    glBindBuffer(GL_UNIFORM_BUFFER, allLightsPointsDataUBO);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(PointsLightsData), NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, lightsPointsWithoutShadowDataUBO);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(PointsLightsData), NULL, GL_DYNAMIC_DRAW);
    for (int i = 0; i < MAX_COUNT_POINT_LIGHTS; i++)
    {
        glBindBuffer(GL_UNIFORM_BUFFER, pointsLightsShadowsDatasUBO[i]);
        glBufferData(GL_UNIFORM_BUFFER, sizeof(PointLightShadow), NULL, GL_DYNAMIC_DRAW);
    }
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

LightFirstStageBufferRender::~LightFirstStageBufferRender()
{
    glDeleteBuffers(1, &allLightsPointsDataUBO);
    glDeleteBuffers(1, &lightsPointsWithoutShadowDataUBO);
    glDeleteBuffers(MAX_COUNT_POINT_LIGHTS, pointsLightsShadowsDatasUBO);
}

void LightFirstStageBufferRender::update()
{
    fillDatas();

    CameraVertexesData *cvd = mainFunctions->getCameraVertexesData();
    lightBasicFrameBuffer->enable();
    lightBasicFrameBuffer->clear();

    pointLightPassProgram.enablePipeline();

    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, depthBuffer->getDepthtexture());
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, normalBuffer->getColorTexture());

    cvd->enableVertexBuffers();

    cvd->enableUniformBlockData(0);

    glBindBuffer(GL_UNIFORM_BUFFER, lightsPointsWithoutShadowDataUBO);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(PointsLightsData), &lightsPointsWithoutShadowData);
    glBindBufferBase(GL_UNIFORM_BUFFER, 2, lightsPointsWithoutShadowDataUBO);

    cvd->callDraw();

    pointLightShadowPassProgram.enablePipeline();

       for (int i = 0; i < countPointsLightsWithShadow; i++)
    {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_CUBE_MAP, shadowMaps[i]);
        glBindBuffer(GL_UNIFORM_BUFFER, pointsLightsShadowsDatasUBO[i]);
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(PointLightShadow), &(pointsLightsShadowsData.pointsLightsShadows[i]));
        glBindBufferBase(GL_UNIFORM_BUFFER, 3, pointsLightsShadowsDatasUBO[i]);

        cvd->callDraw();
    }
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, renderShadowDirectionLight->getTextureBuffer());

    mainLightCombinerProgram.enablePipeline();
    cvd->callDraw();

    glBindBuffer(GL_UNIFORM_BUFFER, allLightsPointsDataUBO);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(PointsLightsData), &allLightsPointsData);

    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    cvd->disableVertexBuffers();

    glDisable(GL_BLEND);
    lightBasicFrameBuffer->disable();
}

GLuint LightFirstStageBufferRender::getUBOAllLightsPoints()
{
    return allLightsPointsDataUBO;
}

void LightFirstStageBufferRender::fillDatas()
{
    RenderQueue *renderQueue = mainFunctions->getMainRenderQueue();
    PointLightGE **pointsLights = &(renderQueue->pointLightsData->pointLights[0]);
    int count = renderQueue->pointLightsData->countPointLights;

    allLightsPointsData.countPointLights = count;
    int posWithoutShadows = 0;
    int posWithShadows = 0;

    for (int i = 0; i < count; i++)
    {
        PointLightGE *pointLight = pointsLights[i];
        allLightsPointsData.pointsLights[i].color = pointLight->color;
        allLightsPointsData.pointsLights[i].intensivity = pointLight->intensivity;
        allLightsPointsData.pointsLights[i].range = pointLight->range;
        allLightsPointsData.pointsLights[i].position = pointLight->position;

        if (pointLight->createShadows == false)
        {
            lightsPointsWithoutShadowData.pointsLights[posWithoutShadows] = allLightsPointsData.pointsLights[i];
            posWithoutShadows++;
        }
        else
        {
            pointsLightsShadowsData.pointsLightsShadows[posWithShadows].color = pointLight->color;
            pointsLightsShadowsData.pointsLightsShadows[posWithShadows].position = pointLight->position;
            pointsLightsShadowsData.pointsLightsShadows[posWithShadows].intensivity = pointLight->intensivity;
            pointsLightsShadowsData.pointsLightsShadows[posWithShadows].range = pointLight->range;
            ShadowCubeFrameBuffer *shadow = pointsLights[i]->shadowCubeFrameBuffer;

            pointsLightsShadowsData.pointsLightsShadows[posWithShadows].far = shadow->getFarPlane();
            pointsLightsShadowsData.pointsLightsShadows[posWithShadows].near = shadow->getNearPlane();
            pointsLightsShadowsData.pointsLightsShadows[posWithShadows].matrix_P = shadow->getMatricesUBOData(0)->data.matrix_P;
            pointsLightsShadowsData.pointsLightsShadows[posWithShadows].matrixInverse_P = shadow->getMatricesUBOData(0)->data.matrixInverse_P;

            for (int i2 = 0; i2 < 6; i2++)
            {
                CameraMatricesDataUBO *ubo = shadow->getMatricesUBOData(i2);
                pointsLightsShadowsData.pointsLightsShadows[posWithShadows].matrix_V[i2] = ubo->data.matrix_V;
                pointsLightsShadowsData.pointsLightsShadows[posWithShadows].matrixInverse_V[i2] = ubo->data.matrixInverse_V;
            }
            shadowMaps[posWithShadows] = shadow->getCubeTexture();
            posWithShadows++;
        }
    }
    countPointsLightsWithShadow = posWithShadows;
    lightsPointsWithoutShadowData.countPointLights = posWithoutShadows;
}