#include <MTH/glGen_matrix.h>

#include "renderEngine/graphicsPipeline/lightRender/volumetricFogRender.h"
#include "renderEngine/graphicsPipeline/lightRender/shadow/shadowDirectionLight.h"
#include "renderEngine/graphicEngine.h"
#include "renderEngine/settings/specialDefines.h"

static mth::vec3 projectToWorldPoint(CameraVertexesData *_CVD, mth::vec3 _projectPoint)
{
    mth::vec4 viewPos = _CVD->getUBOMatrices()->data.matrixInverse_P.transform(mth::vec4(_projectPoint.x, _projectPoint.y, _projectPoint.z, 1));
    viewPos = viewPos / viewPos.w;
    mth::vec4 globalPos = _CVD->getUBOMatrices()->data.matrixInverse_V.transform(viewPos);
    return mth::vec3(globalPos.x, globalPos.y, globalPos.z);
}

VolumetricFogRender::VolumetricFogRender(MainSystems *_mainSystems, SettingsGraphicPipeline *_settingsGraphicPipeline,
                                         LightFirstStageBufferRender *_lightFirstStageRender,
                                         RenderShadowDirectionLight *_renderShadowDirectionLight,
                                         MainDepthBuffer *_depthBuffer, ColorFrameBuffer *_resultBuffer)

    : mainSystems(_mainSystems), settingsGraphicPipeline(_settingsGraphicPipeline), lightFirstStageRender(_lightFirstStageRender),
      renderShadowDirectionLight(_renderShadowDirectionLight),
      depthBuffer(_depthBuffer), resultBuffer(_resultBuffer)

{
    currentSizeGrid = {168, 90, 128};

    ResolutionSettings defaultResolution = _mainSystems->getResolutionSettings();

    glGenFramebuffers(1, &framebufferSupporting);
    glGenTextures(1, &textureSupporting);
    glBindFramebuffer(GL_FRAMEBUFFER, framebufferSupporting);
    glBindTexture(GL_TEXTURE_2D, textureSupporting);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, defaultResolution.width, defaultResolution.height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureSupporting, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    SettingsGraphicPipeline::VolumetricFogData fogSettings;

    fogSettings.generateFog = true;
    fogSettings.colorFog = mth::col(1.0f, 1.0f, 1.0f, 1);
    fogSettings.setSizeFogGrid(currentSizeGrid);
    fogSettings.generateRaysDirectionLight = true;
    fogSettings.setMaxDistRaysPerPixel(200);

    fogSettings.usePointsLightForce = true;
    fogSettings.setStartDist(0.01f);
    fogSettings.setEndFogDist(120);
    fogSettings.setStartDist(0.3f);
    fogSettings.setIntensivityFog(1.4f);
    fogSettings.setAnisotropic(0.25f);
    fogSettings.setAbsorption(0.1f);
    fogSettings.setScattering(0.1f);
    settingsGraphicPipeline->volumetricFog = fogSettings;

    ///

    TechnicalShaderStorage *shaderStorage = mainSystems->getTechnicalShaderStorage();

    noiseGenerator.setPipeline(shaderStorage->getTShader("NoiseFogGenerator"));
    locationRandomSeed = noiseGenerator.getLocUniformCompute("_randomSeed");

    needGenerateNoise = true;

    fogFillerDensityProgram.setPipeline(shaderStorage->getTShader("FogFillerDensity"));
    fogDensityVolumeFillerProgram.setPipeline(shaderStorage->getTShader("DensityVolumeFiller"));

    fogBasicLightProgram.setPipeline(shaderStorage->getTShader("FogBasicLight"));

    fogShadowDirectionLight.setPipeline(shaderStorage->getTShader("FogShadowDirectionLight"));
    locationIndexShadow = fogShadowDirectionLight.getLocUniformCompute("_index");

    fogPointLightsProgram.setPipeline(shaderStorage->getTShader("FogPointLight"));

    fogRayMarchSumatorProgram.setPipeline(shaderStorage->getTShader("FogRayMarchSumator"));

    fogCombinerProgram.setPipeline(shaderStorage->getTShader("VertexScreen"), shaderStorage->getTShader("TransparentCombinerWithFog"));

    glGenBuffers(1, &fogDataUBO);
    glBindBuffer(GL_UNIFORM_BUFFER, fogDataUBO);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(fogData), NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    glGenBuffers(1, &computShaderDataUBO);
    glBindBuffer(GL_UNIFORM_BUFFER, computShaderDataUBO);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(ComputShaderData), NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    createTextures();
    updateGridSizeForAllPrograms();
}

VolumetricFogRender::~VolumetricFogRender()
{
    clearTextures();

    glDeleteBuffers(1, &fogDataUBO);
    glDeleteBuffers(1, &computShaderDataUBO);

    glDeleteTextures(1, &textureSupporting);
    glDeleteFramebuffers(1, &framebufferSupporting);
}

void VolumetricFogRender::update()
{
    checkAndUpdateSizeGrid();

    checkAndUpdateFogData();

    std::array<int, 3> sizeGrid = settingsGraphicPipeline->volumetricFog.getSizeFogGrid();

    int countGroupsX = (int)ceil((float)sizeGrid[0] / (8));
    int countGroupsY = (int)ceil((float)sizeGrid[1] / (8));
    int countGroupsZ = (int)ceil((float)sizeGrid[2] / (8));

    if (needGenerateNoise)
    {
        needGenerateNoise = false;
        noiseGenerator.enablePipeline();
        glBindImageTexture(0, noiseTexture, 0, false, 0, GL_WRITE_ONLY, GL_R8);

        glProgramUniform1i(noiseGenerator.getComputeProgram(), locationRandomSeed, rand());

        noiseGenerator.dispatch(countGroupsX, countGroupsY, 1);
    }

    CameraVertexesData *CVD = mainSystems->getMainFunctions()->getCameraVertexesData();
    CVD->enableUniformBlockData(INDEX_BLOCK_CAMERA_DATA);
    glBindBufferBase(GL_UNIFORM_BUFFER, INDEX_BLOCK_FOG_DATA, fogDataUBO);

    fogFillerDensityProgram.enablePipeline();
    glBindImageTexture(0, textureFogDensityGrid, 0, false, 0, GL_WRITE_ONLY, GL_RGBA16F);
    fogFillerDensityProgram.dispatch(countGroupsX, countGroupsY, countGroupsZ);

    glBindImageTexture(0, textureFogDensityGrid, 0, false, 0, GL_READ_WRITE, GL_RGBA16F);
    glBindImageTexture(2, noiseTexture, 0, false, 0, GL_READ_ONLY, GL_R8);

    int countVolumes = mainSystems->getMainFunctions()->getMainRenderQueue()->densityVolumesData->countDensityVolumes;
    std::array<DensityVolumeGE *, MAX_COUNT_DENSITY_VOLUMES> densityVolumes = mainSystems->getMainFunctions()->getMainRenderQueue()->densityVolumesData->densityVolumes;

    if (countVolumes != 0)
    {
        fogDensityVolumeFillerProgram.enablePipeline();
    }
    for (int i = 0; i < countVolumes; i++)
    {
        DensityVolumeGE *densityVolume = densityVolumes[i];
        if (densityVolume->isWasUpdate())
        {
            densityVolume->updateUBO();
        }
        std::array<mth::vec3, 2> boundOnScreen = boundAABBToBoundScreen(densityVolume->bound, CVD);

        //mth::vec4 screenRect = mth::vec4(boundOnScreen[0].x, boundOnScreen[0].y, boundOnScreen[1].x, boundOnScreen[1].y);
        //ThreadDebugOutput::mainThreadDebugOutput->drawLine(projectToWorldPoint(CVD, mth::vec3(screenRect.x, screenRect.y, 0.5f)), projectToWorldPoint(CVD, mth::vec3(screenRect.x, screenRect.w, 0.5f)));
        //ThreadDebugOutput::mainThreadDebugOutput->drawLine(projectToWorldPoint(CVD, mth::vec3(screenRect.z, screenRect.y, 0.5f)), projectToWorldPoint(CVD, mth::vec3(screenRect.z, screenRect.w, 0.5f)));
        //ThreadDebugOutput::mainThreadDebugOutput->drawLine(projectToWorldPoint(CVD, mth::vec3(screenRect.z, screenRect.y, 0.5f)), projectToWorldPoint(CVD, mth::vec3(screenRect.x, screenRect.y, 0.5f)));
        //ThreadDebugOutput::mainThreadDebugOutput->drawLine(projectToWorldPoint(CVD, mth::vec3(screenRect.z, screenRect.w, 0.5f)), projectToWorldPoint(CVD, mth::vec3(screenRect.x, screenRect.w, 0.5f)));

        mth::vec3 minUV = (mth::vec3(boundOnScreen[0].x, boundOnScreen[0].y, boundOnScreen[0].z) + mth::vec3(1.0f, 1.0f, 1.0f)) / 2.0f;
        mth::vec3 maxUV = (mth::vec3(boundOnScreen[1].x, boundOnScreen[1].y, boundOnScreen[1].z) + mth::vec3(1.0f, 1.0f, 1.0f)) / 2.0f;

        minUV = mth::vec3((std::max(minUV.x * sizeGrid[0], 0.0f)), (std::max(minUV.y * sizeGrid[1], 0.0f)), (std::max(getGridZFromDepth(minUV.z, CVD), 0.0f)));
        maxUV = mth::vec3((std::max(maxUV.x * sizeGrid[0], 0.0f)), (std::max(maxUV.y * sizeGrid[1], 0.0f)), (std::max(getGridZFromDepth(maxUV.z, CVD), 0.0f)));

        int startVolumeDensity[3] = {std::round(minUV.x), std::round(minUV.y), std::round(minUV.z)};
        int sizeVolumeDensity[3] = {std::max((int)std::ceil((maxUV.x - startVolumeDensity[0]) / SIZE_LOCAL_GROUP_DENSITY_VOLUME), 1),
                                    std::max((int)std::ceil((maxUV.y - startVolumeDensity[1]) / SIZE_LOCAL_GROUP_DENSITY_VOLUME), 1),
                                    std::max((int)std::ceil((maxUV.z - startVolumeDensity[2]) / SIZE_LOCAL_GROUP_DENSITY_VOLUME), 1)};

        glBindBuffer(GL_UNIFORM_BUFFER, densityVolume->uboDensityVolumeData);
        glBindBufferBase(GL_UNIFORM_BUFFER, INDEX_DENSITY_VOLUME_UBO, densityVolume->uboDensityVolumeData);

        glBindBuffer(GL_UNIFORM_BUFFER, computShaderDataUBO);
        std::memcpy(computShaderData.startID_DensityVolume, startVolumeDensity, sizeof(startVolumeDensity));
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(ComputShaderData), &computShaderData);
        glBindBufferBase(GL_UNIFORM_BUFFER, INDEX_COMPUTE_SHADER_DATA_UBO, computShaderDataUBO);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
        fogDensityVolumeFillerProgram.dispatch(sizeVolumeDensity[0], sizeVolumeDensity[1], sizeVolumeDensity[2]);

        //ThreadDebugOutput::mainThreadDebugOutput->drawCube(densityVolume->bound.origin, densityVolume->bound.size, mth::mat3(1, 0, 0, 0, 1, 0, 0, 0, 1), mth::vec3(0, 1, 1));
    }

    fogBasicLightProgram.enablePipeline();
    glBindImageTexture(0, textureFogDensityGrid, 0, false, 0, GL_READ_ONLY, GL_RGBA16F);
    glBindImageTexture(1, textureFogLightGrid, 0, false, 0, GL_WRITE_ONLY, GL_RGBA16F);
    fogBasicLightProgram.dispatch(countGroupsX, countGroupsY, countGroupsZ);

    if (settingsGraphicPipeline->volumetricFog.generateRaysDirectionLight)
    {
        fogShadowDirectionLight.enablePipeline();

        int countShadows = renderShadowDirectionLight->countDirectionLightShadows();
        std::array<GLuint, MAX_SHADOWS_ON_DIRECTION_LIGHTS> shadows = renderShadowDirectionLight->getTexturesDirectionLightShadows();

        glBindBufferBase(GL_UNIFORM_BUFFER, INDEX_BLOCK_SHADOW_DIRECTION_LIGHT_DATA, renderShadowDirectionLight->getUBODirectionLightShadows());
        glBindImageTexture(0, textureFogDensityGrid, 0, false, 0, GL_READ_ONLY, GL_RGBA16F);
        glBindImageTexture(1, textureFogLightGrid, 0, false, 0, GL_WRITE_ONLY, GL_RGBA16F);

        for (int i = countShadows - 1; i >= 0; i--)
        {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, shadows[i]);
            glProgramUniform1i(fogShadowDirectionLight.getComputeProgram(), locationIndexShadow, i);
            fogShadowDirectionLight.dispatch(countGroupsX, countGroupsY, countGroupsZ);
        }
    }

    if (settingsGraphicPipeline->volumetricFog.usePointsLightForce)
    {
        GLuint pointLights = lightFirstStageRender->getUBOAllLightsPoints();
        fogPointLightsProgram.enablePipeline();
        glBindBufferBase(GL_UNIFORM_BUFFER, INDEX_BLOCK_ALL_POINT_LIGHTS_DATA, pointLights);
        glBindImageTexture(1, textureFogLightGrid, 0, false, 0, GL_READ_WRITE, GL_RGBA16F);
        fogPointLightsProgram.dispatch(countGroupsX, countGroupsY, countGroupsZ);
    }

    fogRayMarchSumatorProgram.enablePipeline();
    glBindImageTexture(0, textureFogLightGrid, 0, false, 0, GL_READ_WRITE, GL_RGBA16F);
    fogRayMarchSumatorProgram.dispatch(countGroupsX, countGroupsY, 1);
}

void VolumetricFogRender::combineWithTransparent(TransparentBufferRender *_transparentBufferRender)
{
    CameraVertexesData *CVD = mainSystems->getMainFunctions()->getCameraVertexesData();
    CVD->enableUniformBlockData(INDEX_BLOCK_CAMERA_DATA);
    glBindBufferBase(GL_UNIFORM_BUFFER, INDEX_BLOCK_FOG_DATA, fogDataUBO);

    ResolutionSettings defaultResolution = mainSystems->getResolutionSettings();

    mainSystems->getMainFunctions()->getGraphicOperations()->blitToTexture(defaultResolution, defaultResolution, resultBuffer->getColorTexture(), framebufferSupporting);

    fogCombinerProgram.enablePipeline();

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, depthBuffer->getDepthtexture());

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, textureSupporting);

    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_3D, textureFogLightGrid);

    _transparentBufferRender->enableTransparentTextures();

    CVD->enableVertexBuffers();

    resultBuffer->enable();
    CVD->callDraw();
    resultBuffer->disable();

    CVD->disableVertexBuffers();
}

static bool isIntersectingLines(mth::vec2 a, mth::vec2 b, mth::vec2 c, mth::vec2 d)
{
    float denominator = ((b.x - a.x) * (d.y - c.y)) - ((b.y - a.y) * (d.x - c.x));
    float numerator1 = ((a.y - c.y) * (d.x - c.x)) - ((a.x - c.x) * (d.y - c.y));
    float numerator2 = ((a.y - c.y) * (b.x - a.x)) - ((a.x - c.x) * (b.y - a.y));

    if (denominator == 0) return numerator1 == 0 && numerator2 == 0;
    
    float r = numerator1 / denominator;
    float s = numerator2 / denominator;

    return (r >= 0 && r <= 1) && (s >= 0 && s <= 1);
}

std::array<mth::vec3, 2> VolumetricFogRender::boundAABBToBoundScreen(mth::boundAABB _aabb, CameraVertexesData *_CVD) const
{
    std::array<mth::vec3, 2> result;

    std::array<mth::vec3, 8> points;
    points[0] = _aabb.origin + mth::vec3(-_aabb.size.x, _aabb.size.y, _aabb.size.z);
    points[1] = _aabb.origin + mth::vec3(_aabb.size.x, _aabb.size.y, _aabb.size.z);
    points[2] = _aabb.origin + mth::vec3(_aabb.size.x, _aabb.size.y, -_aabb.size.z);
    points[3] = _aabb.origin + mth::vec3(-_aabb.size.x, _aabb.size.y, -_aabb.size.z);
    points[4] = _aabb.origin + mth::vec3(-_aabb.size.x, -_aabb.size.y, _aabb.size.z);
    points[5] = _aabb.origin + mth::vec3(_aabb.size.x, -_aabb.size.y, _aabb.size.z);
    points[6] = _aabb.origin + mth::vec3(_aabb.size.x, -_aabb.size.y, -_aabb.size.z);
    points[7] = _aabb.origin + mth::vec3(-_aabb.size.x, -_aabb.size.y, -_aabb.size.z);

    mth::vec3 positionCam = _CVD->getUBOMatrices()->data.position;
    mth::vec3 directionCam = _CVD->getUBOMatrices()->data.direction;
    mth::mat4 matrixV = _CVD->getUBOMatrices()->data.matrix_V;
    mth::mat4 matrixP = _CVD->getUBOMatrices()->data.matrix_P;

    for (int i = 0; i < 8; i++)
    {
        mth::vec3 point = points[i];
        mth::vec4 pointInView = matrixV.transform(mth::vec4(point.x, point.y, point.z, 1));
        pointInView.z = std::max(pointInView.z, 0.0f);
        mth::vec4 project = matrixP.transform(pointInView);
        project = project / abs(project.w);
        point.x = project.x;
        point.y = project.y;
        point.z = project.z;
        points[i] = point;
    }

    result = {mth::vec3(points[0].x, points[0].y, points[0].z),
              mth::vec3(points[0].x, points[0].y, points[0].z)};
    for (int i = 1; i < 8; i++)
    {
        float x = points[i].x;
        if (x < result[0].x)
        {
            result[0].x = x;
        }
        if (x > result[1].x)
        {
            result[1].x = x;
        }
        float y = points[i].y;
        if (y < result[0].y)
        {
            result[0].y = y;
        }
        if (y > result[1].y)
        {
            result[1].y = y;
        }
        float z = points[i].z;
        if (z < result[0].z)
        {
            result[0].z = z;
        }
        if (z > result[1].z)
        {
            result[1].z = z;
        }
    }

    if (_aabb.checkPointInside(_CVD->getUBOMatrices()->data.position))
    {
        result[0] = mth::vec3(-1, -1, result[0].z);
        result[0].z = std::clamp(result[0].z, -1.0f, 1.0f);
        result[1] = mth::vec3(1, 1, result[1].z);
        result[1].z = std::clamp(result[1].z, -1.0f, 1.0f);
    }
    else
    {
        result[0].x = std::clamp(result[0].x, -1.0f, 1.0f);
        result[0].y = std::clamp(result[0].y, -1.0f, 1.0f);
        result[0].z = std::clamp(result[0].z, -1.0f, 1.0f);
        result[1].x = std::clamp(result[1].x, -1.0f, 1.0f);
        result[1].y = std::clamp(result[1].y, -1.0f, 1.0f);
        result[1].z = std::clamp(result[1].z, -1.0f, 1.0f);
    }

    return result;
}

float VolumetricFogRender::getGridZFromDepth(float _depthProject, CameraVertexesData *_CVD) const
{
    _depthProject = _depthProject * 2.0f - 1.0f;
    mth::vec4 zPos = _CVD->getUBOMatrices()->data.matrixInverse_P.transform(mth::vec4(0, 0, _depthProject, 1));

    float far = std::min(_CVD->getUBOMatrices()->data.far_plane, fogData.farPlaneFog);
    float near = _CVD->getUBOMatrices()->data.near_plane + fogData.nearPlaneFog;
    float depth = ((zPos.z / zPos.w) - near) / (far - near);
    mth::vec2 params = mth::vec2(float(currentSizeGrid[2]) / std::log2(far / near),
                                 -(float(currentSizeGrid[2]) * std::log2(near) / std::log2(far / near)));

    float viewZ = std::max(depth * far, 0.0f);
    depth = std::max(std::log2(viewZ) * params.x + params.y, 0.0f);
    return depth;
}

void VolumetricFogRender::updateGridSizeForProgram(GLuint _program, int _x, int _y, int _z)
{
    glProgramUniform3ui(_program, glGetUniformLocation(_program, "_sizeGrid"), _x, _y, _z);
}

void VolumetricFogRender::updateGridSizeForAllPrograms()
{
    updateGridSizeForProgram(fogFillerDensityProgram.getComputeProgram(), currentSizeGrid[0], currentSizeGrid[1], currentSizeGrid[2]);
    updateGridSizeForProgram(fogDensityVolumeFillerProgram.getComputeProgram(), currentSizeGrid[0], currentSizeGrid[1], currentSizeGrid[2]);
    updateGridSizeForProgram(fogBasicLightProgram.getComputeProgram(), currentSizeGrid[0], currentSizeGrid[1], currentSizeGrid[2]);
    updateGridSizeForProgram(fogShadowDirectionLight.getComputeProgram(), currentSizeGrid[0], currentSizeGrid[1], currentSizeGrid[2]);
    updateGridSizeForProgram(fogPointLightsProgram.getComputeProgram(), currentSizeGrid[0], currentSizeGrid[1], currentSizeGrid[2]);
    updateGridSizeForProgram(fogRayMarchSumatorProgram.getComputeProgram(), currentSizeGrid[0], currentSizeGrid[1], currentSizeGrid[2]);
    updateGridSizeForProgram(fogCombinerProgram.getFragmentProgram(), currentSizeGrid[0], currentSizeGrid[1], currentSizeGrid[2]);
}

void VolumetricFogRender::checkAndUpdateFogData()
{
    FogData newData{};

    newData.directionSun = settingsGraphicPipeline->sky.directionSun;
    mth::col colSun = settingsGraphicPipeline->sky.colorSun;
    newData.colorSun = mth::vec4(colSun.r, colSun.g, colSun.b, settingsGraphicPipeline->sky.intensity);
    mth::col colEnv = settingsGraphicPipeline->sky.ambientLightColor;
    mth::col colFog = settingsGraphicPipeline->volumetricFog.colorFog;
    newData.colorFog = mth::vec3(colFog.r, colFog.g, colFog.b);
    newData.environmentLight = mth::vec4(colEnv.r, colEnv.g, colEnv.b, settingsGraphicPipeline->sky.intensityAmbientLight);
    newData.anisotropic = 0.2f;
    newData.absorption = 0.10f;
    newData.scattering = 0.10f;
    newData.density = settingsGraphicPipeline->volumetricFog.getIntensivityFog();

    newData.nearPlaneFog = settingsGraphicPipeline->volumetricFog.getStartDist();
    newData.farPlaneFog = settingsGraphicPipeline->volumetricFog.getEndFogDist();
    newData.farPlaneRays = settingsGraphicPipeline->volumetricFog.getMaxDistRaysPerPixel();

    if (std::memcmp(&newData, &fogData, sizeof(FogData)) != 0)
    {
        std::memcpy(&fogData, &newData, sizeof(FogData));
        glBindBuffer(GL_UNIFORM_BUFFER, fogDataUBO);
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(FogData), &fogData);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
    }
}

void VolumetricFogRender::checkAndUpdateSizeGrid()
{
    if (currentSizeGrid != settingsGraphicPipeline->volumetricFog.getSizeFogGrid())
    {
        currentSizeGrid = settingsGraphicPipeline->volumetricFog.getSizeFogGrid();

        clearTextures();
        createTextures();
        updateGridSizeForAllPrograms();
    }
}

void VolumetricFogRender::clearTextures()
{
    glDeleteTextures(1, &noiseTexture);

    glDeleteTextures(1, &textureFogDensityGrid);
    glDeleteTextures(1, &textureFogLightGrid);
}

void VolumetricFogRender::createTextures()
{
    glGenTextures(1, &noiseTexture);
    glBindTexture(GL_TEXTURE_2D, noiseTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, currentSizeGrid[0], currentSizeGrid[1], 0, GL_RED, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    glGenTextures(1, &textureFogDensityGrid);
    glBindTexture(GL_TEXTURE_3D, textureFogDensityGrid);
    glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA16F, currentSizeGrid[0], currentSizeGrid[1], currentSizeGrid[2], 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glGenTextures(1, &textureFogLightGrid);
    glBindTexture(GL_TEXTURE_3D, textureFogLightGrid);
    glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA16F, currentSizeGrid[0], currentSizeGrid[1], currentSizeGrid[2], 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_3D, 0);
}