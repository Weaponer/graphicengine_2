#include "renderEngine/graphicsPipeline/lightRender/renderShadowDirecitonLight.h"
#include "renderEngine/graphicEngine.h"

RenderShadowDirectionLight::RenderShadowDirectionLight(MainFunctions *_mainFunctions, TechnicalShaderStorage *_TSS, SettingsGraphicPipeline *_settingsGraphicPipeline,
                                                       ShadowDirectionLight *_shadowDirectionLight, LightBasicFrameBuffer *_lightBasicFrameBuffer, MainDepthBuffer *_depthBuffer)
    : mainFunctions(_mainFunctions), settingsGraphicPipeline(_settingsGraphicPipeline), shadowDirectionLight(_shadowDirectionLight),
      lightBasicFrameBuffer(_lightBasicFrameBuffer), depthBuffer(_depthBuffer)
{

    defaultResolution.width = lightBasicFrameBuffer->getWidth();
    defaultResolution.height = lightBasicFrameBuffer->getHeight();

    directionLightShadowProgram.setPipeline(_TSS->getTShader("VertexScreen"), _TSS->getTShader("ShadowDirectionLightPass"));
    locationFragIndexPartion = directionLightShadowProgram.getLocUniformFragment("indexPartion");

    for (int i = 0; i < 8; i++)
    {
        GLint location = directionLightShadowProgram.getLocUniformFragment(("_shadowMaps[" + std::to_string(i) + "]").c_str());
        if (location != -1)
        {
            glProgramUniform1i(directionLightShadowProgram.getFragmentProgram(), location, i + 1);
        }
    }

    glGenBuffers(1, &directionLightShadowDataUBO);
    glBindBuffer(GL_UNIFORM_BUFFER, directionLightShadowDataUBO);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(DirectionLightShadowData), NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    glGenFramebuffers(1, &frameBuffer);
    glGenTextures(1, &textureBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glBindTexture(GL_TEXTURE_2D, textureBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, defaultResolution.width, defaultResolution.height, 0, GL_RED, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureBuffer, 0);

    
    int indexDepthBuffer = _depthBuffer->getDepthtexture();
    glBindTexture(GL_TEXTURE_2D, indexDepthBuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, indexDepthBuffer, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

RenderShadowDirectionLight::~RenderShadowDirectionLight()
{
    glDeleteFramebuffers(1, &frameBuffer);
    glDeleteTextures(1, &textureBuffer);

    glDeleteBuffers(1, &directionLightShadowDataUBO);
}

GLuint RenderShadowDirectionLight::getTextureBuffer()
{
    return textureBuffer;
}

void RenderShadowDirectionLight::update()
{
    glViewport(0, 0, defaultResolution.width, defaultResolution.height);
    CameraVertexesData *cvd = mainFunctions->getCameraVertexesData();
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glDepthFunc(GL_GEQUAL);

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    directionLightShadowProgram.enablePipeline();

    cvd->enableVertexBuffers();
    cvd->enableUniformBlockData(0);
    fillDatas();

    glBindBufferBase(GL_UNIFORM_BUFFER, 1, directionLightShadowDataUBO);

    for(int i = directionLightShadowData.countPartition - 1; i >= 0; i--)
    {
        glProgramUniform1i(directionLightShadowProgram.getFragmentProgram(), locationFragIndexPartion, i);
        cvd->callDraw();
    }
    cvd->disableVertexBuffers();

    glDisable(GL_DEPTH_TEST);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

GLuint RenderShadowDirectionLight::getUBODirectionLightShadows()
{
    return directionLightShadowDataUBO;
}

int RenderShadowDirectionLight::countDirectionLightShadows()
{
    return shadowDirectionLight->getCountShadowBuffers();
}

std::array<GLuint, MAX_SHADOWS_ON_DIRECTION_LIGHTS> RenderShadowDirectionLight::getTexturesDirectionLightShadows()
{
    int count = countDirectionLightShadows();
    std::array<GLuint, MAX_SHADOWS_ON_DIRECTION_LIGHTS> textures;
    for (int i = 0; i < count; i++)
    {
        ShadowFrameBuffer *shadowFrameBuffer = shadowDirectionLight->getShadowBuffer(i);
        textures[i] = shadowFrameBuffer->getShadowTexture();
    }
    return textures;
}

void RenderShadowDirectionLight::fillDatas()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, depthBuffer->getDepthtexture());

    int count = shadowDirectionLight->getCountShadowBuffers();

    directionLightShadowData.countPartition = count;
    directionLightShadowData.startFadeDistance = shadowDirectionLight->getShadowDirectionLightData()->getStartFadeDistance();
    directionLightShadowData.endFadeDistance = shadowDirectionLight->getShadowDirectionLightData()->getShadowDistance();
    directionLightShadowData.directionBoundAlongCamera = shadowDirectionLight->getDirectionBoundAlongCamera();

    int numTexture = 1;
    for (int i = 0; i < count; i++)
    {
        ShadowFrameBuffer *shadowFrameBuffer = shadowDirectionLight->getShadowBuffer(i);
        CameraMatricesData data = shadowFrameBuffer->getCameraMatricesData()->data;
        float bias = settingsGraphicPipeline->shadowDirectionLight.getShadowBias(i);
        float maxDistance = shadowDirectionLight->getDistanceShadowCascade(i);
        directionLightShadowData._shadowClipDistancesBias[i] = mth::vec4(data.near_plane, data.far_plane, bias, maxDistance);
        directionLightShadowData._shadowMatrix_VP[i] = data.matrix_VP;
        glActiveTexture(GL_TEXTURE0 + numTexture);
        glBindTexture(GL_TEXTURE_2D, shadowFrameBuffer->getShadowTexture());
        numTexture++;
    }
    glBindBuffer(GL_UNIFORM_BUFFER, directionLightShadowDataUBO);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(DirectionLightShadowData), &directionLightShadowData);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}