#include "renderEngine/graphicsPipeline/lightRender/lightBufferRender.h"

LightBufferRender::LightBufferRender(MainSystems *_mainSystems, SettingsGraphicPipeline *_settingsGraphicPipeline,
                                     TransparentBufferRender *_transparentBufferRender,
                                     ColorFrameBuffer *_resultLightBuffer, LightBasicFrameBuffer *_lightBasicFrameBuffer,
                                     MainDepthBuffer *_depthBuffer, ColorFrameBuffer *_normalBuffer)
    : shadowGenerator(_mainSystems->getMainFunctions(), _mainSystems->getProgramActivator(), _mainSystems->getTechnicalShaderStorage()),
      shadowDirectionLight(_settingsGraphicPipeline, &shadowGenerator, _mainSystems),
      shadowsPointLights(_mainSystems->getMainFunctions(), &shadowGenerator),
      renderShadowDirectionLight(_mainSystems->getMainFunctions(), _mainSystems->getTechnicalShaderStorage(), _settingsGraphicPipeline, &shadowDirectionLight, _lightBasicFrameBuffer, _depthBuffer),
      lightFirstStageBufferRender(_mainSystems->getMainFunctions(), _mainSystems->getTechnicalShaderStorage(), &renderShadowDirectionLight, _lightBasicFrameBuffer, _normalBuffer, _depthBuffer),
      volumetricForRender(_mainSystems, _settingsGraphicPipeline, &lightFirstStageBufferRender, &renderShadowDirectionLight,
                          _depthBuffer, _resultLightBuffer),
      combinerResultLightBuffer(_mainSystems->getResolutionSettings(), _settingsGraphicPipeline,
                                _mainSystems->getMainFunctions()->getCameraVertexesData(), _resultLightBuffer, _mainSystems->getTechnicalShaderStorage()),
      supportBuffer(GL_RGBA16F, mth::vec4(0, 0, 0, 0), _mainSystems->getResolutionSettings(), NULL),
      mainSystems(_mainSystems), settingsGraphicPipeline(_settingsGraphicPipeline), transparentBufferRender(_transparentBufferRender),
      normalBuffer(_normalBuffer), depthBuffer(_depthBuffer), lightBasicFrameBuffer(_lightBasicFrameBuffer), resultLightBuffer(_resultLightBuffer)
{
    TechnicalShaderStorage *shaderStorage = _mainSystems->getTechnicalShaderStorage();
    combinerWithTransparentProgram.setPipeline(shaderStorage->getTShader("VertexScreen"),
                                               shaderStorage->getTShader("TransparentCombinerWithoutFog"));
}

LightBufferRender::~LightBufferRender()
{
}

void LightBufferRender::update()
{
    shadowDirectionLight.update();
    shadowsPointLights.update();
    renderShadowDirectionLight.update();
    lightFirstStageBufferRender.update();
}

void LightBufferRender::updateFog()
{
    if (settingsGraphicPipeline->volumetricFog.generateFog == false)
    {
        return;
    }

    volumetricForRender.update();
}

void LightBufferRender::combineWithTransparentAndFog()
{
    if (settingsGraphicPipeline->volumetricFog.generateFog)
    {
        volumetricForRender.combineWithTransparent(transparentBufferRender);
    }
    else
    {
        mainSystems->getMainFunctions()->getGraphicOperations()->blitToTexture(mainSystems->getResolutionSettings(),
                                                                               resultLightBuffer->getColorTexture(), &supportBuffer);

        CameraVertexesData *CVD = mainSystems->getMainFunctions()->getCameraVertexesData();

        combinerWithTransparentProgram.enablePipeline();

        transparentBufferRender->enableTransparentTextures();

        CVD->enableVertexBuffers();
        CVD->enableUniformBlockData(INDEX_BLOCK_CAMERA_DATA);

        resultLightBuffer->enable();
        CVD->callDraw();
        resultLightBuffer->disable();

        CVD->disableVertexBuffers();
    }
}

ShadowGenerator *LightBufferRender::getShadowGenerator()
{
    return &shadowGenerator;
}

ShadowDirectionLight *LightBufferRender::getShadowDirectionLight()
{
    return &shadowDirectionLight;
}

LightFirstStageBufferRender *LightBufferRender::getLightFirstStageBufferRender()
{
    return &lightFirstStageBufferRender;
}

RenderShadowDirectionLight *LightBufferRender::getRenderShadowDirectionLight()
{
    return &renderShadowDirectionLight;
}

VolumetricFogRender *LightBufferRender::getVolumetricFogRender()
{
    return &volumetricForRender;
}

CombinerResultLightBuffer *LightBufferRender::getCombinerResultLightBuffer()
{
    return &combinerResultLightBuffer;
}