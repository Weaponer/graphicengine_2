#include "renderEngine/graphicsPipeline/postEffects/bloomRender.h"

#include <cstring>

BloomRender::BloomRender(CameraVertexesData *_CVD, TechnicalShaderStorage *_technicalShaderStorage,
                         SettingsGraphicPipeline *_settingsGraphicPipeline, ColorFrameBuffer *_resultBuffer) : CVD(_CVD), technicalShaderStorage(_technicalShaderStorage),
                                                                                                               settingsGraphicPipeline(_settingsGraphicPipeline), resultBuffer(_resultBuffer)
{
    generateTextures();
    bloomPreparation.setPipeline(_technicalShaderStorage->getTShader("VertexScreen"),
                                 _technicalShaderStorage->getTShader("BloomPreparation"));

    bloomGenerator.setPipeline(_technicalShaderStorage->getTShader("VertexScreen"),
                               _technicalShaderStorage->getTShader("BloomLight"));

    bloomCombiner.setPipeline(_technicalShaderStorage->getTShader("VertexScreen"),
                              _technicalShaderStorage->getTShader("BloomCombiner"));

    for (int i = 0; i < MAX_SAMPLES_BLOOM_EFFECT; i++)
    {
        GLint location = bloomCombiner.getLocUniformFragment(("_bloomSubMaps[" + std::to_string(i) + "]").c_str());
        glProgramUniform1i(bloomCombiner.getFragmentProgram(), location, i);
    }

    bloomCombinerData.countSubMaps = _settingsGraphicPipeline->postEffects.bloom.getCountSamples();
    bloomCombinerData.forceBloom = _settingsGraphicPipeline->postEffects.bloom.getForce();

    glGenBuffers(1, &bloomCombinerDataUBO);
    glBindBuffer(GL_UNIFORM_BUFFER, bloomCombinerDataUBO);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(BloomCombinerData), &bloomCombinerData, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

BloomRender::~BloomRender()
{
    glDeleteBuffers(1, &bloomCombinerDataUBO);

    glDeleteFramebuffers(MAX_SAMPLES_BLOOM_EFFECT, framebuffersBloom);
    glDeleteTextures(MAX_SAMPLES_BLOOM_EFFECT, texturesBloom);
}

void BloomRender::update()
{
    BloomCombinerData newBloomCombinerData;
    std::memcpy(&newBloomCombinerData, &bloomCombinerData, sizeof(BloomCombinerData));
    newBloomCombinerData.countSubMaps = settingsGraphicPipeline->postEffects.bloom.getCountSamples();
    newBloomCombinerData.forceBloom = settingsGraphicPipeline->postEffects.bloom.getForce();

    if (std::memcmp(&newBloomCombinerData, &bloomCombinerData, sizeof(BloomCombinerData)) != 0)
    {
        std::memcpy(&bloomCombinerData, &newBloomCombinerData, sizeof(BloomCombinerData));
        updateUniformBuffer();
    }

    glBindFramebuffer(GL_FRAMEBUFFER, framebuffersBloom[0]);
    glViewport(0, 0, sizes[0].x, sizes[0].y);
    
    CVD->enableVertexBuffers();

    bloomPreparation.enablePipeline();

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, resultBuffer->getColorTexture());

    CVD->callDraw();

    bloomGenerator.enablePipeline();
    for (int i = 1; i < newBloomCombinerData.countSubMaps; i++)
    {
        glViewport(0, 0, sizes[i].x, sizes[i].y);
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffersBloom[i]);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texturesBloom[i - 1]);
        CVD->callDraw();
    }

    glViewport(0, 0, resultBuffer->getWidth(), resultBuffer->getHeight());
    glBindFramebuffer(GL_FRAMEBUFFER, resultBuffer->getFrameBuffer());

    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);

    bloomCombiner.enablePipeline();

    glBindBufferBase(GL_UNIFORM_BUFFER, 0, bloomCombinerDataUBO);
    for (int i = 0; i < newBloomCombinerData.countSubMaps; i++)
    {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, texturesBloom[i]);
    }
    CVD->callDraw();

    glDisable(GL_BLEND);
    
    CVD->disableVertexBuffers();
}

void BloomRender::generateTextures()
{
    glGenFramebuffers(MAX_SAMPLES_BLOOM_EFFECT, framebuffersBloom);
    glGenTextures(MAX_SAMPLES_BLOOM_EFFECT, texturesBloom);

    mth::Vector2Int size = mth::Vector2Int(resultBuffer->getWidth(), resultBuffer->getHeight());
    for (int i = 0; i < MAX_SAMPLES_BLOOM_EFFECT; i++)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffersBloom[i]);
        sizes[i] = mth::Vector2Int((float)size.x / (i + 1), (float)size.y / (i + 1));
        glBindTexture(GL_TEXTURE_2D, texturesBloom[i]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_R11F_G11F_B10F, sizes[i].x, sizes[i].y, 0, GL_RGB, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texturesBloom[i], 0);
    }
}

void BloomRender::updateUniformBuffer()
{
    glBindBuffer(GL_UNIFORM_BUFFER, bloomCombinerDataUBO);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(BloomCombinerData), &bloomCombinerData);

    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}