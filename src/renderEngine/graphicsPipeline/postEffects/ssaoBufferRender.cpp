#include "renderEngine/graphicsPipeline/postEffects/ssaoBufferRender.h"
#include "renderEngine/graphicEngine.h"

SSAOBufferRender::SSAOBufferRender(MainSystems *_mainSystems, ColorFrameBuffer *_normalDepthBuffer, ColorFrameBuffer *_resultBuffer)
    : mainSystems(_mainSystems), resolution(_mainSystems->getResolutionSettings()),
      normalDepthBuffer(_normalDepthBuffer), CVD(_mainSystems->getMainFunctions()->getCameraVertexesData()), resultBuffer(_resultBuffer)
{
    depthBuffer = normalDepthBuffer->getMainDepthBuffer()->getDepthtexture();
    normalBuffer = normalDepthBuffer->getColorTexture();

    int rotateCombination = _mainSystems->getSettingsMainSystems()->graphicConfig.getGraphicTable().toInt("SSAOrotateCombination");
    int countSubSSAOBuffers = _mainSystems->getSettingsMainSystems()->graphicConfig.getGraphicTable().toInt("SSAOsubBuffers");
    generateOcclusionData.countIterations = countSubSSAOBuffers * rotateCombination;

    programGenerateOcclusion.setPipeline(_mainSystems->getTechnicalShaderStorage()->getTShader("VertexScreen"),
                                         _mainSystems->getTechnicalShaderStorage()->getTShader("SSAOGenerator"));

    generateOcclusionData.size = mth::Vector2Int((int)resolution.width, (int)resolution.height);
    generateOcclusionData.radius = _mainSystems->getSettingsMainSystems()->graphicConfig.getGraphicTable().toFloat("SSAOradius");

    float step = 90.0f / rotateCombination;
    for (int i = 0; i < countSubSSAOBuffers; i++)
    {
        float rot = 0;
        for (int i2 = 0; i2 < rotateCombination; i2++)
        {
            int index = i * rotateCombination + i2;

            mth::mat2 matRotate;
            matRotate.setOrientation(rot * RADIAN);
            rot += step;

            generateOcclusionData.iterations[index].countSubIterations = 2;
            generateOcclusionData.iterations[index].scale = (i + 1) * 2;
            generateOcclusionData.iterations[index].rotate = matRotate;
        }
    }

    generateData();

    programCombineResult.setPipeline(_mainSystems->getTechnicalShaderStorage()->getTShader("VertexScreen"),
                                     _mainSystems->getTechnicalShaderStorage()->getTShader("SSAOCombiner"));
}

SSAOBufferRender::~SSAOBufferRender()
{
    glDeleteFramebuffers(1, &ssaoFrameBuffers);
    glDeleteTextures(1, &ssaoTextureBuffers);
    glDeleteBuffers(1, &generateOcclusionDataUBO);
}

void SSAOBufferRender::update()
{
    glViewport(0, 0, sizeFramebuffer.x, sizeFramebuffer.y);
    glBindFramebuffer(GL_FRAMEBUFFER, ssaoFrameBuffers);

    programGenerateOcclusion.enablePipeline();

    CVD->enableVertexBuffers();
    CVD->enableUniformBlockData(0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, depthBuffer);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, normalBuffer);

    glBindBufferBase(GL_UNIFORM_BUFFER, 1, generateOcclusionDataUBO);

    CVD->callDraw();

    glViewport(0, 0, resolution.width, resolution.height);
    glBindFramebuffer(GL_FRAMEBUFFER, resultBuffer->getFrameBuffer());
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, ssaoTextureBuffers);

    glEnable(GL_BLEND);
    glBlendFunc(GL_ZERO, GL_SRC_COLOR);
    programCombineResult.enablePipeline();
    CVD->callDraw();

    glDisable(GL_BLEND);

    CVD->disableVertexBuffers();
}

GLuint SSAOBufferRender::getTextureSSAOBuffer()
{
    return ssaoTextureBuffers;
}

void SSAOBufferRender::generateData()
{
    mth::vec2 size = (mth::vec2(resolution.width, resolution.height) * 0.6f);
    sizeFramebuffer = mth::Vector2Int(size.x, size.y);
    glGenFramebuffers(1, &ssaoFrameBuffers);
    glGenTextures(1, &ssaoTextureBuffers);
    glBindFramebuffer(GL_FRAMEBUFFER, ssaoFrameBuffers);
    glBindTexture(GL_TEXTURE_2D, ssaoTextureBuffers);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, sizeFramebuffer.x, sizeFramebuffer.y, 0, GL_RED, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssaoTextureBuffers, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glGenBuffers(1, &generateOcclusionDataUBO);
    glBindBuffer(GL_UNIFORM_BUFFER, generateOcclusionDataUBO);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(SSAOGeneratorData), &generateOcclusionData, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}