#include "renderEngine/graphicsPipeline/postEffects/vignetteEffect.h"

VignetteEffect::VignetteEffect(CameraVertexesData *_CVD, TechnicalShaderStorage *_technicalShaderStorage,
                               SettingsGraphicPipeline *_settingsGraphicPipeline, ColorFrameBuffer *_resultBuffer)
    : CVD(_CVD), technicalShaderStorage(_technicalShaderStorage), settingsGraphicPipeline(_settingsGraphicPipeline), resultBuffer(_resultBuffer)
{
    settingsGraphicPipeline->postEffects.vignetteEffect.useVignetteEffect = true;
    settingsGraphicPipeline->postEffects.vignetteEffect.color = mth::col(0.0f, 0.0f, 0.0f, 0.5);
    settingsGraphicPipeline->postEffects.vignetteEffect.setRadius(0.0f);
    settingsGraphicPipeline->postEffects.vignetteEffect.setPow(1.2f);

    vignetteEffectProgram.setPipeline(technicalShaderStorage->getTShader("VertexScreen"),
                                      technicalShaderStorage->getTShader("VignetteEffect"));
    generateBuffer();
}

VignetteEffect::~VignetteEffect()
{
    deleteBuffer();
}

void VignetteEffect::update()
{
    if (settingsGraphicPipeline->postEffects.vignetteEffect.useVignetteEffect)
    {
        updateSettingsBuffer();

        resultBuffer->enable();
        CVD->enableVertexBuffers();

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        vignetteEffectProgram.enablePipeline();
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, settingsVignetteDataBuffer);
        CVD->callDraw();

        glDisable(GL_BLEND);

        CVD->disableVertexBuffers();
        resultBuffer->disable();
    }
}

void VignetteEffect::generateBuffer()
{
    glGenBuffers(1, &settingsVignetteDataBuffer);
    glBindBuffer(GL_UNIFORM_BUFFER, settingsVignetteDataBuffer);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(SettingsVignetteData), NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void VignetteEffect::deleteBuffer()
{
    glDeleteBuffers(1, &settingsVignetteDataBuffer);
}

void VignetteEffect::updateSettingsBuffer()
{
    SettingsVignetteData newData;
    newData.color = settingsGraphicPipeline->postEffects.vignetteEffect.color;
    newData.radius = settingsGraphicPipeline->postEffects.vignetteEffect.getRadius();
    newData.pow = settingsGraphicPipeline->postEffects.vignetteEffect.getPow();

    if (std::memcmp(&newData, &settingsVignetteData, sizeof(SettingsVignetteData)) != 0)
    {
        std::memcpy(&settingsVignetteData, &newData, sizeof(SettingsVignetteData));
        glBindBuffer(GL_UNIFORM_BUFFER, settingsVignetteDataBuffer);
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(SettingsVignetteData), &settingsVignetteData);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
    }
}