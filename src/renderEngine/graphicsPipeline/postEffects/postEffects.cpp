#include "renderEngine/graphicsPipeline/postEffects/postEffects.h"

PostEffects::PostEffects(MainSystems *_mainSystems, ColorFrameBuffer *_normalDepthBuffer,
                         SettingsGraphicPipeline *_settingsGraphicPipeline, ColorFrameBuffer *_resultBuffer)
    : ssaoBufferRender(_mainSystems, _normalDepthBuffer, _resultBuffer),
      bloomRender(_mainSystems->getMainFunctions()->getCameraVertexesData(), _mainSystems->getMainFunctions()->getTechnicalShaderStorage(),
                  _settingsGraphicPipeline, _resultBuffer),
      hdrCorrection(_mainSystems->getMainFunctions(), _settingsGraphicPipeline, _resultBuffer),
      vignetteEffect(_mainSystems->getMainFunctions()->getCameraVertexesData(),
                     _mainSystems->getMainFunctions()->getTechnicalShaderStorage(), _settingsGraphicPipeline, _resultBuffer),
      settingsGraphicPipeline(_settingsGraphicPipeline),
      resultBuffer(_resultBuffer)
{
}

PostEffects::~PostEffects()
{
}

SSAOBufferRender *PostEffects::getSSAOBufferRender()
{
    return &ssaoBufferRender;
}

void PostEffects::update()
{
    if (settingsGraphicPipeline->postEffects.SSAO.enable)
    {
        ssaoBufferRender.update();
    }
}

void PostEffects::updateBloom()
{
    if (settingsGraphicPipeline->postEffects.bloom.enable)
    {
        bloomRender.update();
    }
}

void PostEffects::updateHDR()
{
    hdrCorrection.update();
}

void PostEffects::updateOtherEffects()
{
    vignetteEffect.update();
}