#include "renderEngine/graphicsPipeline/postEffects/hdrCorrection.h"

HDRCorrection::HDRCorrection(MainFunctions *_mainFunctions, SettingsGraphicPipeline *_settingsGprahicPipeline, ColorFrameBuffer *_resultBuffer)
    : CVD(_mainFunctions->getCameraVertexesData()), technicalShaderStorage(_mainFunctions->getTechnicalShaderStorage()),
      mainFunctions(_mainFunctions), settingsGprahicPipeline(_settingsGprahicPipeline), resultBuffer(_resultBuffer)
{
    hdrCorrectionPipeline.setPipeline(technicalShaderStorage->getTShader("VertexScreen"),
                                      technicalShaderStorage->getTShader("HDRCorrectionCombiner"));

    if (_resultBuffer->getWidth() % SIZE_SQRT_GROUP != 0)
    {
        countWorkGroup.x = _resultBuffer->getWidth() / SIZE_SQRT_GROUP + 1;
    }
    else
    {
        countWorkGroup.x = _resultBuffer->getWidth() / SIZE_SQRT_GROUP;
    }

    if (_resultBuffer->getHeight() % SIZE_SQRT_GROUP != 0)
    {
        countWorkGroup.y = _resultBuffer->getHeight() / SIZE_SQRT_GROUP + 1;
    }
    else
    {
        countWorkGroup.y = _resultBuffer->getHeight() / SIZE_SQRT_GROUP;
    }

    histogramGeneratorPipeline.setPipeline(technicalShaderStorage->getTShader("HDRHistogramGenerator"),
                                           countWorkGroup.x, countWorkGroup.y, 1);

    averageCalculatePipeline.setPipeline(technicalShaderStorage->getTShader("HDRAverageCaclulate"),
                                         1, 1, 1);

    settingsGprahicPipeline->postEffects.hdrCorrection.useStaticValue = false;
    settingsGprahicPipeline->postEffects.hdrCorrection.setStaticValue(1.0f);
    settingsGprahicPipeline->postEffects.hdrCorrection.setMinimumLogLuminance(1.1f);
    settingsGprahicPipeline->postEffects.hdrCorrection.setInverseLogLuminanceRange(4.6f);

    generateBuffers();

    checkAndUpdateBuffer();
}

HDRCorrection::~HDRCorrection()
{
    deleteBuffers();
}

void HDRCorrection::update()
{
    checkAndUpdateBuffer();

    glBindFramebuffer(GL_FRAMEBUFFER, tempFrameBuffer);

    if (!useStaticValue)
    {

        histogramGeneratorPipeline.enablePipeline();
        glBindImageTexture(0, resultBuffer->getColorTexture(), 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA16F);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, bufferHistogram);
        histogramGeneratorPipeline.dispatch();

        averageCalculatePipeline.enablePipeline();
        glBindImageTexture(0, imageResultValue, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R16F);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, bufferHistogram);
        averageCalculatePipeline.dispatch();
    }

    CVD->enableVertexBuffers();
    glBindImageTexture(0, resultBuffer->getColorTexture(), 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA16F);
    glBindImageTexture(1, imageResultValue, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R16F);
    hdrCorrectionPipeline.enablePipeline();
    CVD->callDraw();

    ResolutionSettings settings;
    settings.width = resultBuffer->getWidth();
    settings.height = resultBuffer->getHeight();
    mainFunctions->getGraphicOperations()->blitToTexture(settings, tempTexture, resultBuffer);
    CVD->disableVertexBuffers();
}

#define DEFAULT_MINIMUM_LOG_2_LUMINANCE 1.3f
#define DEFAULT_INVERSE_LOG_2_LUMINANCE_RANGE 4.6f

void HDRCorrection::generateBuffers()
{
    glGenFramebuffers(1, &tempFrameBuffer);
    glGenTextures(1, &tempTexture);
    glBindFramebuffer(GL_FRAMEBUFFER, tempFrameBuffer);
    glBindTexture(GL_TEXTURE_2D, tempTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, resultBuffer->getWidth(), resultBuffer->getHeight(), 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tempTexture, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glGenBuffers(1, &bufferHistogram);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, bufferHistogram);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(uint) * SIZE_GROUP, NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    clearHistogramBuffer();

    glGenTextures(1, &imageResultValue);
    glBindTexture(GL_TEXTURE_2D, imageResultValue);
    float defaultValue = 0.0f;
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R16F, 1, 1, 0, GL_RED, GL_FLOAT, NULL);
    glBindTexture(GL_TEXTURE_2D, 0);

    params = mth::vec4(DEFAULT_MINIMUM_LOG_2_LUMINANCE, DEFAULT_INVERSE_LOG_2_LUMINANCE_RANGE, 0.0f,
                       resultBuffer->getWidth() * resultBuffer->getHeight());

    locationParamsHisogramGenerator = histogramGeneratorPipeline.getLocUniformCompute("params");
    GLuint program = histogramGeneratorPipeline.getComputeProgram();
    glProgramUniform4f(program, locationParamsHisogramGenerator, params.x, params.y, params.z, params.w);

    locationParamsAverageCalculate = averageCalculatePipeline.getLocUniformCompute("params");
    program = averageCalculatePipeline.getComputeProgram();
    glProgramUniform4f(program, locationParamsAverageCalculate, params.x, params.y, params.z, params.w);
}

void HDRCorrection::deleteBuffers()
{
    glDeleteFramebuffers(1, &tempFrameBuffer);
    glDeleteTextures(1, &tempTexture);

    glDeleteBuffers(1, &bufferHistogram);
    glDeleteTextures(1, &imageResultValue);
}

void HDRCorrection::clearHistogramBuffer()
{
    uint clearData[SIZE_GROUP];
    std::memset(clearData, 0, sizeof(uint) * SIZE_GROUP);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, bufferHistogram);
    glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(uint) * SIZE_GROUP, clearData);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

void HDRCorrection::checkAndUpdateBuffer()
{
    bool useStaticValueNow = settingsGprahicPipeline->postEffects.hdrCorrection.useStaticValue;
    if (useStaticValueNow)
    {
        float nowValue = settingsGprahicPipeline->postEffects.hdrCorrection.getStaticValue();
        if (useStaticValueNow != useStaticValue || nowValue != staticHDRValue)
        {
            glBindTexture(GL_TEXTURE_2D, imageResultValue);
            glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 1, 1, GL_RED, GL_FLOAT, &nowValue);
            glBindTexture(GL_TEXTURE_2D, 0);
        }
        staticHDRValue = nowValue;
    }
    else
    {
        mth::vec4 newParams = mth::vec4(settingsGprahicPipeline->postEffects.hdrCorrection.getMinimumLogLuminance(),
                                        settingsGprahicPipeline->postEffects.hdrCorrection.getInverseLogLuminanceRange(), mainFunctions->getDeltaTime(), params.w);
        params = newParams;
        GLuint program = histogramGeneratorPipeline.getComputeProgram();
        glProgramUniform4f(program, locationParamsHisogramGenerator, params.x, params.y, params.z, params.w);
        program = averageCalculatePipeline.getComputeProgram();
        glProgramUniform4f(program, locationParamsAverageCalculate, params.x, params.y, params.z, params.w);
    }

    useStaticValue = useStaticValueNow;
}