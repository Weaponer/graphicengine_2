#include "renderEngine/graphicsPipeline/GBufferRender.h"

#include "globalProfiler.h"

GBufferRender::GBufferRender(GBuffer *_gBuffer, MainFunctions *_mainFunctions, ProgramActivator *_programActivator)
    : gBuffer(_gBuffer), mainFunctions(_mainFunctions), programActivator(_programActivator), usedAdditionalLayers(0)
{
}

GBufferRender::~GBufferRender()
{
}

bool GBufferRender::wasRenderSurfacePasses() const
{
    return renderedSurfacePasses;
}

int GBufferRender::getUsedAdditionalLayers() const
{
    return usedAdditionalLayers;
}

const bool *GBufferRender::getUsedModelLight() const
{
    return usedModelLight;
}

void GBufferRender::update()
{
    RenderQueue *queue = mainFunctions->getMainRenderQueue();

    renderedSurfacePasses = false;
    if (queue->beginDrawCall[SURFACE_PASS_INDEX] == NULL)
    {
        return;
    }
    renderedSurfacePasses = true;

    for (int i = 0; i < MAX_COUNT_MODEL_LIGHT; i++)
    {
        usedModelLight[i] = false;
    }

    char useAdditionalLayers = 0;
    DrawCall *drawCall = queue->beginDrawCall[SURFACE_PASS_INDEX];
    while (drawCall != NULL)
    {
        useAdditionalLayers = useAdditionalLayers | USE_ADDITIONAL_LAYERS_PASS(drawCall->pass);
        usedModelLight[MODEL_LIGHT_PASS(drawCall->pass) - OFFSET_INDEX_MODEL_LIGHT] = true;
        drawCall = drawCall->nextDrawCall;
    }

    useAdditionalLayers = 0 | G_BUFFER_LAYER_1 | G_BUFFER_LAYER_2 | (useAdditionalLayers << G_BUFFER_OFFSET_ADDITIONAL_BUFFERS);

    gBuffer->enableFramebuffer((int)useAdditionalLayers);
    gBuffer->clearLayers();

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_EQUAL);

    SettingsDrawRenderQueue settings;
    settings.typePass = TypePass::Surface;
    settings.renderStage = RenderStage::RS_SURFACE_STAGE;
    settings.useInstancing = true;
    settings.maskRenderTypes = RenderType::RenderType_Opaque | RenderType::RenderType_TransparentCutout;
    settings.maskQueueRender = QueueRender::QueueRender_Geometry | QueueRender::QueueRender_AlphaTest;

    CameraVertexesData *cameraVertexesData = mainFunctions->getCameraVertexesData();
    programActivator->drawRenderQueue(*mainFunctions->getMainRenderQueue(), cameraVertexesData->getUBOMatrices(), settings);

    glDepthFunc(GL_LEQUAL);
    glDisable(GL_DEPTH_TEST);

    gBuffer->disableFramebuffer();
}