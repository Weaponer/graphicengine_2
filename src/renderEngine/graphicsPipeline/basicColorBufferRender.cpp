#include "renderEngine/graphicsPipeline/basicColorBufferRender.h"
#include "renderEngine/graphicEngine.h"

BasicColorBufferRender::BasicColorBufferRender(ColorFrameBuffer *_colorBuffer, MainFunctions *_mainFunctions, ProgramActivator *_programActivator)
    : colorBuffer(_colorBuffer), mainFunctions(_mainFunctions), programActivator(_programActivator)
{
}

BasicColorBufferRender::~BasicColorBufferRender()
{
}

void BasicColorBufferRender::update()
{
    colorBuffer->enable();
    glDepthMask(GL_FALSE);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_EQUAL);

    SettingsDrawRenderQueue settings;
    settings.typePass = TypePass::BaseColor;
    settings.renderStage = RenderStage::RS_BASIC_COLOR_STAGE;
    settings.useInstancing = true;
    settings.maskRenderTypes = RenderType::RenderType_Opaque | RenderType::RenderType_TransparentCutout;
    settings.maskQueueRender = QueueRender::QueueRender_Geometry | QueueRender::QueueRender_AlphaTest;

    CameraVertexesData *cameraVertexesData = mainFunctions->getCameraVertexesData();
    programActivator->drawRenderQueue(*mainFunctions->getMainRenderQueue(), cameraVertexesData->getUBOMatrices(), settings);

    glDepthFunc(GL_LEQUAL);
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    colorBuffer->disable();
}
