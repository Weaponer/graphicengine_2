#include "renderEngine/graphicsPipeline/normalDepthBufferRender.h"

#include "renderEngine/graphicEngine.h"

NormalDepthBufferRender::NormalDepthBufferRender(ColorFrameBuffer *_normalDepthBuffer, MainFunctions *_mainFunctions, ProgramActivator *_programActivator)
    : normalDepthBuffer(_normalDepthBuffer), mainFunctions(_mainFunctions), programActivator(_programActivator)
{
}

NormalDepthBufferRender::~NormalDepthBufferRender()
{
}

void NormalDepthBufferRender::update()
{
    MainDepthBuffer *depthBuffer = normalDepthBuffer->getMainDepthBuffer();
    depthBuffer->enable();
    glDepthMask(GL_TRUE);
    glEnable(GL_DEPTH_TEST);

    depthBuffer->clear();

    SettingsDrawRenderQueue settings;
    settings.typePass = TypePass::Depth;
    settings.renderStage = RenderStage::RS_DEPTH_STAGE;
    settings.useInstancing = true;
    settings.maskRenderTypes = RenderType::RenderType_Opaque | RenderType::RenderType_TransparentCutout;
    settings.maskQueueRender = QueueRender::QueueRender_Geometry | QueueRender::QueueRender_AlphaTest;

    CameraVertexesData *cameraVertexesData = mainFunctions->getCameraVertexesData();
    programActivator->drawRenderQueue(*mainFunctions->getMainRenderQueue(), cameraVertexesData->getUBOMatrices(), settings);

    normalDepthBuffer->enable();
    normalDepthBuffer->clear();

    settings.typePass = TypePass::DepthNormal;
    settings.renderStage = RenderStage::RS_NORMAL_DEPTH_STAGE;

    programActivator->drawRenderQueue(*mainFunctions->getMainRenderQueue(), cameraVertexesData->getUBOMatrices(), settings);

    glDepthMask(GL_FALSE);

    settings.typePass = TypePass::Normal;
    settings.renderStage = RenderStage::RS_NORMAL_STAGE;

    programActivator->drawRenderQueue(*mainFunctions->getMainRenderQueue(), cameraVertexesData->getUBOMatrices(), settings);

    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    normalDepthBuffer->disable();
}