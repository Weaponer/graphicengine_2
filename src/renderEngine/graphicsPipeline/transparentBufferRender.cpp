#include "renderEngine/graphicsPipeline/transparentBufferRender.h"

TransparentBufferRender::TransparentBufferRender(MainSystems *_mainSystems, ProgramActivator *_programActivator, MainDepthBuffer *_mainDepthBuffer)
    : mainSystems(_mainSystems), mainFunctions(_mainSystems->getMainFunctions()), programActivator(_programActivator), mainDepthBuffer(_mainDepthBuffer),
      testTexture(GL_RGBA16F, mth::vec4(1, 0, 0, 1), mainSystems->getResolutionSettings(), mainDepthBuffer)
{
    ResolutionSettings resolution = mainSystems->getResolutionSettings();

    glGenFramebuffers(1, &framebufferTransparent);
    glBindFramebuffer(GL_FRAMEBUFFER, framebufferTransparent);
    glGenTextures(1, &mainColorTexture);
    glBindTexture(GL_TEXTURE_2D, mainColorTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, resolution.width, resolution.height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mainColorTexture, 0);
    glBindTexture(GL_TEXTURE_2D, mainDepthBuffer->getDepthtexture());
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, mainDepthBuffer->getDepthtexture(), 0);

    glGenTextures(1, &alphaMulTexture);
    glBindTexture(GL_TEXTURE_2D, alphaMulTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, resolution.width, resolution.height, 0, GL_RED, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    GLint swizzleMask[] = {GL_ZERO, GL_ZERO, GL_ZERO, GL_RED};
    glTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, swizzleMask);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, alphaMulTexture, 0);

    glGenTextures(1, &sumZPosAndCountTexture);
    glBindTexture(GL_TEXTURE_2D, sumZPosAndCountTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32F, resolution.width, resolution.height, 0, GL_RG, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, sumZPosAndCountTexture, 0);

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

TransparentBufferRender::~TransparentBufferRender()
{
    glDeleteFramebuffers(1, &framebufferTransparent);
    glDeleteTextures(1, &mainColorTexture);
    glDeleteTextures(1, &alphaMulTexture);
    glDeleteTextures(1, &sumZPosAndCountTexture);
}

void TransparentBufferRender::update()
{
    glBindFramebuffer(GL_FRAMEBUFFER, framebufferTransparent);
    GLenum buffers[3] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
    glDrawBuffers(3, buffers);

    glDepthMask(GL_FALSE);
    glEnable(GL_DEPTH_TEST);

    glEnable(GL_BLEND);
    glBlendFuncSeparatei(0, GL_ONE, GL_ONE, GL_ONE, GL_ONE);
    glBlendFuncSeparatei(1, GL_ZERO, GL_SRC_ALPHA, GL_ZERO, GL_SRC_ALPHA);
    glBlendFuncSeparatei(2, GL_ONE, GL_ONE, GL_ONE, GL_ONE);

    float zeroClear[4] = {0.0f, 0.0f, 0.0f, 0.0f};
    glClearBufferfv(GL_COLOR, 0, zeroClear);
    float oneClear[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    glClearBufferfv(GL_COLOR, 1, oneClear);
    glClearBufferfv(GL_COLOR, 2, zeroClear);

    SettingsDrawRenderQueue settings;
    settings.typePass = TypePass::BaseColor;
    settings.renderStage = RenderStage::RS_BASIC_COLOR_STAGE;
    settings.useInstancing = true;
    settings.maskRenderTypes = RenderType::RenderType_Transparent;
    settings.maskQueueRender = QueueRender::QueueRender_Transparent;

    CameraVertexesData *cameraVertexesData = mainFunctions->getCameraVertexesData();
    programActivator->drawRenderQueue(*mainFunctions->getMainRenderQueue(), cameraVertexesData->getUBOMatrices(), settings);

    GLenum defaultBuffers[1] = {GL_COLOR_ATTACHMENT0};
    glDrawBuffers(1, defaultBuffers);

    glDisable(GL_BLEND);

    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

GLuint TransparentBufferRender::getMainColorTexture()
{
    return mainColorTexture;
}

GLuint TransparentBufferRender::getAlphaMulTexture()
{
    return alphaMulTexture;
}

GLuint TransparentBufferRender::getSumZposAndCountTexture()
{
    return sumZPosAndCountTexture;
}

void TransparentBufferRender::enableTransparentTextures()
{
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, mainColorTexture);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, alphaMulTexture);

    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, sumZPosAndCountTexture);
}