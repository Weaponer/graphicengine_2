#include "renderEngine/graphicsPipeline/debugLineRender.h"
#include "renderEngine/graphicEngine.h"

DebugLineRender::DebugLineRender(ColorFrameBuffer *_colorBuffer, TechnicalShaderStorage *_techincalShaderStorage,
                                 CameraVertexesData *_mainCameraVertexesData, EntityRegister *_entityRegister)
    : colorBuffer(_colorBuffer), mainCameraVertexesData(_mainCameraVertexesData), entityRegister(_entityRegister)
{
    disableDebug();
    
    glGenVertexArrays(1, &vertexArrays);
    glBindVertexArray(vertexArrays);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &bufferArray);
    glBindBuffer(GL_ARRAY_BUFFER, bufferArray);
    glBufferData(GL_ARRAY_BUFFER, sizeof(mth::vec3) * 2 * DEBUG_RENDERER_STACK_SIZE, NULL, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLuint *)0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (GLuint *)(sizeof(mth::vec3) * DEBUG_RENDERER_STACK_SIZE));

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    debugLinesProgram.setPipeline(_techincalShaderStorage->getTShader("DebugLinesVert"), _techincalShaderStorage->getTShader("DebugLinesFrag"));
    ResolutionSettings resolution;
    resolution.width = colorBuffer->getWidth();
    resolution.height = colorBuffer->getHeight();
    glProgramUniform2i(debugLinesProgram.getFragmentProgram(), debugLinesProgram.getLocUniformFragment("_viewSize"), resolution.width, resolution.height);
}

DebugLineRender::~DebugLineRender()
{
    glDeleteVertexArrays(1, &vertexArrays);
    glDeleteBuffers(1, &bufferArray);
}

void DebugLineRender::enableDebug()
{
    isEnable = true;
}

void DebugLineRender::disableDebug()
{
    isEnable = false;
}

void DebugLineRender::update()
{
    if (isEnable == false)
    {
        return;
    }

    colorBuffer->enable();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    debugLinesProgram.enablePipeline();
    mainCameraVertexesData->enableUniformBlockData(0);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, colorBuffer->getMainDepthBuffer()->getDepthtexture());

    glBindBuffer(GL_ARRAY_BUFFER, bufferArray);
    glBindVertexArray(vertexArrays);
    glLineWidth(1.5f);

    /////
    std::array<mth::vec3, DEBUG_RENDERER_STACK_SIZE> positions;
    std::array<mth::vec3, DEBUG_RENDERER_STACK_SIZE> colors;
    int count = 0;

    const std::list<DebugRendererGE *> *meshRS = entityRegister->getListDebugRenderers();
    auto end = meshRS->end();
    for (auto i = meshRS->begin(); i != end; ++i)
    {
        mth::vec3 endColor = mth::vec3(1, 1, 1);
        int countDatas = (*i)->getCountDates();
        count = 0;

        for (int i2 = 0; i2 < countDatas; i2++)
        {
            if ((*i)->getVectorData(i2).code == CODE_DEBUG_RENDERER_COLOR)
            {
                endColor = (*i)->getVectorData(i2).vec;
            }
            else
            {
                positions[count] = (*i)->getVectorData(i2).vec;
                colors[count] = endColor;
                count++;
            }
        }

        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(mth::vec3) * count, &(positions[0]));
        glBufferSubData(GL_ARRAY_BUFFER, sizeof(mth::vec3) * DEBUG_RENDERER_STACK_SIZE, sizeof(mth::vec3) * count, &(colors[0]));
        glDrawArrays(GL_LINES, 0, count);
    }
    /*glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);*/
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDisable(GL_BLEND);
    colorBuffer->disable();
}