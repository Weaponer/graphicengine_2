#include "renderEngine/graphicsPipeline/renderLightGBuffer.h"

RenderLightGBuffer::RenderLightGBuffer(GBuffer *_gBuffer, GBufferRender *_gBufferRender, ModelLightShaderStorage *_modelLightShaderStorage,
                                       CameraVertexesData *_cameraVertexesData, const LightRenderData *_lightRenderData, ColorFrameBuffer *_resultHDRBuffer)
    : gBuffer(_gBuffer), gBufferRender(_gBufferRender),
      modelLightShaderStorage(_modelLightShaderStorage), cameraVertexesData(_cameraVertexesData),
      lightRenderData(_lightRenderData), resultHDRBuffer(_resultHDRBuffer)
{
    resolution.width = _resultHDRBuffer->getWidth();
    resolution.height = _resultHDRBuffer->getHeight();
}

RenderLightGBuffer::~RenderLightGBuffer()
{
}

void RenderLightGBuffer::update()
{
    if (!gBufferRender->wasRenderSurfacePasses())
    {
        return;
    }
    int useAdditionalLayers = gBufferRender->getUsedAdditionalLayers();
    enableDataForPrograms(useAdditionalLayers);
    const bool *useModelLight = gBufferRender->getUsedModelLight();
    for (int i = 0; i < MAX_COUNT_MODEL_LIGHT; i++)
    {
        if (useModelLight[i])
        {
            ModelLightShader *modelLight = modelLightShaderStorage->getModelLightShader(i + OFFSET_INDEX_MODEL_LIGHT);
            glBindProgramPipeline(modelLight->getPipeline());
            cameraVertexesData->callDraw();
            glMemoryBarrier(GL_FRAMEBUFFER_BARRIER_BIT);
        }
    }
    glBindProgramPipeline(0);
    disableDataForPrograms(useAdditionalLayers);
}

void RenderLightGBuffer::enableDataForPrograms(int _useAdditionalLayers)
{
    resultHDRBuffer->enable();
    cameraVertexesData->enableVertexBuffers();

    glBindBufferBase(GL_UNIFORM_BUFFER, 0, cameraVertexesData->getUBOMatrices()->indexUBOBuffer);
    glBindBufferBase(GL_UNIFORM_BUFFER, 2, lightRenderData->skyDataUBO);

    glBindImageTexture(ML_POINT_AND_LIGHT_DIRECTION_FORCE_LOC, lightRenderData->pointLightsAndDirectionLightForceBuffer, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA16F);
    glBindImageTexture(ML_LIGHT_POINT_DIRECTION_LOC, lightRenderData->pointLightsNormalsBuffer, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA16F);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, lightRenderData->environmentBuffer);
    
    int nextNumLocationImage = ML_DEPTH_BUFFER_LOC;
    glActiveTexture(GL_TEXTURE0 + nextNumLocationImage);
    glBindTexture(GL_TEXTURE_2D, gBuffer->getDepthBuffer()->getDepthtexture());
    nextNumLocationImage++;
    glActiveTexture(GL_TEXTURE0 + nextNumLocationImage);
    glBindTexture(GL_TEXTURE_2D, gBuffer->getNormalsBuffer()->getColorTexture());
    nextNumLocationImage++;

    for (int i = 0; i < G_BUFFER_COUNT_BUFFERS - G_BUFFER_COUNT_ADDITIONAL_BUFFERS; i++)
    {
        glBindImageTexture(nextNumLocationImage, gBuffer->getLayer(i), 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA16F);
        nextNumLocationImage++;
    }

    for (int i = G_BUFFER_OFFSET_ADDITIONAL_BUFFERS; i < G_BUFFER_COUNT_BUFFERS; i++)
    {
        if ((_useAdditionalLayers & (1 << i)) != 0)
        {
            glBindImageTexture(nextNumLocationImage, gBuffer->getLayer(i), 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA16F);
        }
        nextNumLocationImage++;
    }
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_SRC_ALPHA);
}

void RenderLightGBuffer::disableDataForPrograms(int _useAdditionalLayers)
{
    glDisable(GL_BLEND);

    for (int i = 0; i < ML_MODEL_LIGHT_LOC; i++)
    {
        glBindImageTexture(i, 0, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32F);
    }

    glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
    glBindBufferBase(GL_UNIFORM_BUFFER, 1, 0);

    cameraVertexesData->disableVertexBuffers();
    resultHDRBuffer->disable();
}