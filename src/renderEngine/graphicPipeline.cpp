#include "renderEngine/graphicPipeline.h"

#include <debugOutput/globalProfiler.h>

GraphicPipeline::GraphicPipeline(SettingsGraphicPipeline *_settingsGraphicPipeline, MainSystems *_mainSystems)
    : settingsGraphicPipeline(_settingsGraphicPipeline), mainSystems(_mainSystems), mainFunctions(_mainSystems->getMainFunctions()),
      mainDepthBuffer(mainSystems->getResolutionSettings()),
      normalBuffer(GL_RGB, mth::vec4(0.5f, 0.5f, 1.0f, 1), mainSystems->getResolutionSettings(), &mainDepthBuffer),
      lightBasicFrameBuffer(mainSystems->getResolutionSettings()),
      gBuffer(mainSystems->getResolutionSettings(), &mainDepthBuffer, &normalBuffer),
      resultColorBuffer(GL_RGBA16F, mth::vec4(0, 0, 0, 1), mainSystems->getResolutionSettings(), &mainDepthBuffer),

      normalDepthBufferRender(&normalBuffer, mainFunctions, _mainSystems->getProgramActivator()),
      gBufferRender(&gBuffer, mainFunctions, _mainSystems->getProgramActivator()),
      skyBufferRender(_mainSystems, _settingsGraphicPipeline),
      lightBufferRender(_mainSystems, _settingsGraphicPipeline, &transparentBufferRender,
                        &resultColorBuffer, &lightBasicFrameBuffer, &mainDepthBuffer, &normalBuffer),
      renderLightGBuffer(&gBuffer, &gBufferRender, _mainSystems->getModelLightStorage(),
                         _mainSystems->getMainFunctions()->getCameraVertexesData(), &lightRenderData, &resultColorBuffer),
      basicColorBufferRender(&resultColorBuffer, _mainSystems->getMainFunctions(), _mainSystems->getProgramActivator()),
      transparentBufferRender(_mainSystems, _mainSystems->getProgramActivator(), &mainDepthBuffer),
      postEffectsRender(_mainSystems, &normalBuffer, _settingsGraphicPipeline, &resultColorBuffer),
      debugLineRender(&resultColorBuffer, _mainSystems->getTechnicalShaderStorage(),
                      _mainSystems->getMainFunctions()->getCameraVertexesData(), _mainSystems->getSceneStructure()->getEntityRegister())
{

    lightBufferRender.getCombinerResultLightBuffer()->setOtherFrameBuffers(&resultColorBuffer, &resultColorBuffer,
                                                                           postEffectsRender.getSSAOBufferRender(),
                                                                           NULL);

    lightRenderData.environmentBuffer = skyBufferRender.getEnvironmentCubeMap();
    lightRenderData.pointLightsAndDirectionLightForceBuffer = lightBasicFrameBuffer.getPointsAndDirectionalLightTexture();
    lightRenderData.pointLightsNormalsBuffer = lightBasicFrameBuffer.getPointLightsDirectionTexture();
    lightRenderData.skyDataUBO = skyBufferRender.getUBOSkyData();
    lightRenderData.allPointLightsUBO = lightBufferRender.getLightFirstStageBufferRender()->getUBOAllLightsPoints();

    settingsGraphicPipeline->postEffects.SSAO.enable = true;
    settingsGraphicPipeline->postEffects.bloom.enable = true;
    settingsGraphicPipeline->postEffects.bloom.setCountSamples(12);
    settingsGraphicPipeline->postEffects.bloom.setForce(0.2f);
    settingsGraphicPipeline->lodSettings.useLods = true;
    settingsGraphicPipeline->lodSettings.useLodsForShadow = false;
    settingsGraphicPipeline->lodSettings.setLodBias(2.0f);
    settingsGraphicPipeline->lodSettings.setLodShadowBias(2.0f);

    debugLineRender.enableDebug();

    initOptionsOpenGL();
}

GraphicPipeline::~GraphicPipeline()
{
}

void GraphicPipeline::update()
{
    resultColorBuffer.enable();
    resultColorBuffer.clear();
    resultColorBuffer.disable();

    GlobalProfiler::startSample("Graphic: normal-depth buffer render");
    normalDepthBufferRender.update();
    GlobalProfiler::stopSample("Graphic: normal-depth buffer render");

    GlobalProfiler::startSample("Graphic: G buffer render");
    gBufferRender.update();
    GlobalProfiler::stopSample("Graphic: G buffer render");

    GlobalProfiler::startSample("Graphic: sky buffer render");
    skyBufferRender.update();
    GlobalProfiler::stopSample("Graphic: sky buffer render");

    GlobalProfiler::startSample("Graphic: sky box render in buffer");
    skyBufferRender.renderSkyBoxInBuffer(&resultColorBuffer);
    GlobalProfiler::stopSample("Graphic: sky box render in buffer");

    GlobalProfiler::startSample("Graphic: light buffer update");
    lightBufferRender.update();
    GlobalProfiler::stopSample("Graphic: light buffer update");

    GlobalProfiler::startSample("Graphic: render light G buffer update");
    renderLightGBuffer.update();
    GlobalProfiler::stopSample("Graphic: render light G buffer update");

    GlobalProfiler::startSample("Graphic: basic color buffer update");
    basicColorBufferRender.update();
    GlobalProfiler::stopSample("Graphic: basic color buffer update");

    GlobalProfiler::startSample("Graphic: transparent buffer update");
    transparentBufferRender.update();
    GlobalProfiler::stopSample("Graphic: transparent buffer update");

    GlobalProfiler::startSample("Graphic: fog update");
    lightBufferRender.updateFog();
    GlobalProfiler::stopSample("Graphic: fog update");

    GlobalProfiler::startSample("Graphic: post effects update");
    postEffectsRender.update();
    GlobalProfiler::stopSample("Graphic: post effects update");

    GlobalProfiler::startSample("Graphic: combine with transparent and fog");
    lightBufferRender.combineWithTransparentAndFog();
    GlobalProfiler::stopSample("Graphic: combine with transparent and fog");

    GlobalProfiler::startSample("Graphic: bloom update");
    postEffectsRender.updateBloom();
    GlobalProfiler::stopSample("Graphic: bloom update");

    GlobalProfiler::startSample("Graphic: hdr correction update");
    postEffectsRender.updateHDR();
    GlobalProfiler::stopSample("Graphic: hdr correction update");

    GlobalProfiler::startSample("Graphic: other effects update");
    postEffectsRender.updateOtherEffects();
    GlobalProfiler::stopSample("Graphic: other effects update");

    // mainFunctions->getGraphicOperations()->blitToTexture(mainSystems->getResolutionSettings(), lightRenderData.pointLightsAndDirectionLightForceBuffer, &resultColorBuffer);

    // mainFunctions->getGraphicOperations()->blitLitght(mainSystems->getResolutionSettings(),
    //                                                       &lightBasicFrameBuffer, &resultColorBuffer);

    //mainFunctions->getGraphicOperations()->blitToTexture(mainSystems->getResolutionSettings(),
    //                                                      gBuffer.getNormalsBuffer()->getColorTexture(), &resultColorBuffer);
    
    // mainFunctions->getGraphicOperations()->blitToTexture(mainSystems->getResolutionSettings(),
    //                                                          lightBufferRender.getVolumetricFogRender()->getTextureRays(), &resultColorBuffer);

    //mainFunctions->getGraphicOperations()->blitDepthToTexture(mainSystems->getResolutionSettings(),
     //                                                         &mainDepthBuffer, &resultColorBuffer);

    //mainFunctions->getGraphicOperations()->blitToTexture(mainSystems->getResolutionSettings(), postEffectsRender.getSSAOBufferRender()->getTextureSSAOBuffer(), &resultColorBuffer);
    //mainFunctions->getGraphicOperations()->blitToTexture(mainSystems->getResolutionSettings(), lightBufferRender.getRenderShadowDirectionLight()->getTextureBuffer(), &resultColorBuffer);
    //mainFunctions->getGraphicOperations()->blitShadow(mainSystems->getResolutionSettings(), lightBufferRender.getShadowDirectionLight()->getShadowBuffer(0), &resultColorBuffer);
    
    if (lightBufferRender.getShadowGenerator()->getTestCubeFrameBuffer() != NULL)
    {
    }
    debugLineRender.update();
}

GLuint GraphicPipeline::getResultColorBuffer()
{
    return resultColorBuffer.getColorTexture();
}

GLuint GraphicPipeline::getDepthBuffer()
{
    return mainDepthBuffer.getDepthtexture();
}

GLuint GraphicPipeline::getNormalBuffer()
{
    return normalBuffer.getColorTexture();
}

const LightRenderData *GraphicPipeline::getLightRenderData() const
{
    return &lightRenderData;
}

SkyBufferRender *GraphicPipeline::getSkyBufferRender()
{
    return &skyBufferRender;
}

static void GLAPIENTRY MessageCallback(GLenum source,
                                       GLenum type,
                                       GLuint id,
                                       GLenum severity,
                                       GLsizei length,
                                       const GLchar *message,
                                       const void *userParam)
{
    if (type == GL_DEBUG_TYPE_ERROR)
    {
        fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
                (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
                type, severity, message);
    }
}

void GraphicPipeline::initOptionsOpenGL()
{
    glFrontFace(GL_CW);
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(MessageCallback, 0);
}