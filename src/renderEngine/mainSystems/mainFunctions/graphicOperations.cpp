#include "renderEngine/mainSystems/mainFunctions/graphicOperations.h"

GraphicOperations::GraphicOperations(TechnicalShaderStorage *_techincalShaderStorage, CameraVertexesData *_mainCameraVertexesData)
    : techincalShaderStorage(_techincalShaderStorage), mainCameraVertexesData(_mainCameraVertexesData)
{
    blitPipeline.setPipeline(_techincalShaderStorage->getTShader("VertexScreen"),
                             _techincalShaderStorage->getTShader("Blit"));
    blitStencilPipeline.setPipeline(_techincalShaderStorage->getTShader("VertexScreen"),
                                    _techincalShaderStorage->getTShader("BlitStencil"));
    blitDepthPipeline.setPipeline(_techincalShaderStorage->getTShader("VertexScreen"),
                                  _techincalShaderStorage->getTShader("BlitDepth"));
    blitLightPipeline.setPipeline(_techincalShaderStorage->getTShader("VertexScreen"),
                                  _techincalShaderStorage->getTShader("BlitLight"));
    blitShadowPipeline.setPipeline(_techincalShaderStorage->getTShader("VertexScreen"),
                                   _techincalShaderStorage->getTShader("BlitShadowBuffer"));
    blitCubeShadowPipeline.setPipeline(_techincalShaderStorage->getTShader("VertexScreen"),
                                       _techincalShaderStorage->getTShader("BlitShadowCubeBuffer"));
    blitTransparentPipeline.setPipeline(_techincalShaderStorage->getTShader("VertexScreen"),
                                        _techincalShaderStorage->getTShader("BlitTransparentBuffer"));
}

GraphicOperations::~GraphicOperations()
{
}

void GraphicOperations::blitToContext(GLuint _sourceTexture)
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    blitPipeline.enablePipeline();

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _sourceTexture);

    mainCameraVertexesData->enableVertexBuffers();
    mainCameraVertexesData->callDraw();
    mainCameraVertexesData->disableVertexBuffers();
}

void GraphicOperations::blitToTexture(ResolutionSettings _sourceSize, GLuint _sourceTexture, ColorFrameBuffer *_colorFrameBuffer)
{
    _colorFrameBuffer->enable();
    glViewport(0, 0, _colorFrameBuffer->getWidth(), _colorFrameBuffer->getHeight());

    blitPipeline.enablePipeline();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _sourceTexture);

    mainCameraVertexesData->enableVertexBuffers();
    mainCameraVertexesData->callDraw();
    mainCameraVertexesData->disableVertexBuffers();

    glViewport(0, 0, _sourceSize.width, _sourceSize.height);
    _colorFrameBuffer->disable();
}

void GraphicOperations::blitToTexture(ResolutionSettings _sourceSize, ResolutionSettings _destSize, GLuint _sourceTexture, GLuint framebuffer)
{
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    glViewport(0, 0, _destSize.width, _destSize.height);

    blitPipeline.enablePipeline();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _sourceTexture);

    mainCameraVertexesData->enableVertexBuffers();
    mainCameraVertexesData->callDraw();
    mainCameraVertexesData->disableVertexBuffers();

    glViewport(0, 0, _sourceSize.width, _sourceSize.height);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GraphicOperations::blitStencil(ResolutionSettings _sourceSize, GLuint _sourceTexture, ColorFrameBuffer *_colorFrameBuffer)
{
    _colorFrameBuffer->enable();
    glViewport(0, 0, _colorFrameBuffer->getWidth(), _colorFrameBuffer->getHeight());

    blitStencilPipeline.enablePipeline();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _sourceTexture);

    mainCameraVertexesData->enableVertexBuffers();
    mainCameraVertexesData->callDraw();
    mainCameraVertexesData->disableVertexBuffers();

    glViewport(0, 0, _sourceSize.width, _sourceSize.height);
    _colorFrameBuffer->disable();
}

void GraphicOperations::blitDepthToTexture(ResolutionSettings _sourceSize, MainDepthBuffer *_mainDepthBuffer, ColorFrameBuffer *_colorFrameBuffer)
{
    _colorFrameBuffer->enable();
    glViewport(0, 0, _colorFrameBuffer->getWidth(), _colorFrameBuffer->getHeight());

    blitDepthPipeline.enablePipeline();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _mainDepthBuffer->getDepthtexture());

    mainCameraVertexesData->enableVertexBuffers();
    mainCameraVertexesData->callDraw();
    mainCameraVertexesData->disableVertexBuffers();

    glViewport(0, 0, _sourceSize.width, _sourceSize.height);
    _colorFrameBuffer->disable();
}

void GraphicOperations::blitLitght(ResolutionSettings _sourceSize, LightBasicFrameBuffer *_lightFrameBuffer, ColorFrameBuffer *_colorFrameBuffer)
{
    _colorFrameBuffer->enable();
    glViewport(0, 0, _colorFrameBuffer->getWidth(), _colorFrameBuffer->getHeight());

    blitLightPipeline.enablePipeline();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _lightFrameBuffer->getPointsAndDirectionalLightTexture());
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, _lightFrameBuffer->getPointLightsDirectionTexture());

    mainCameraVertexesData->enableVertexBuffers();
    mainCameraVertexesData->callDraw();
    mainCameraVertexesData->disableVertexBuffers();

    _colorFrameBuffer->disable();
}

void GraphicOperations::blitShadow(ResolutionSettings _sourceSize, ShadowFrameBuffer *_shadowFrameBuffer, ColorFrameBuffer *_colorFrameBuffer)
{
    _colorFrameBuffer->enable();
    glViewport(0, 0, _colorFrameBuffer->getWidth(), _colorFrameBuffer->getHeight());

    blitShadowPipeline.enablePipeline();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _shadowFrameBuffer->getShadowTexture());

    mainCameraVertexesData->enableVertexBuffers();
    mainCameraVertexesData->callDraw();
    mainCameraVertexesData->disableVertexBuffers();

    _colorFrameBuffer->disable();
}

void GraphicOperations::blitTransparent(ResolutionSettings _sourceSize, TransparantFrameBuffer *_source, ColorFrameBuffer *_colorFrameBuffer)
{
    _colorFrameBuffer->enable();
    glViewport(0, 0, _colorFrameBuffer->getWidth(), _colorFrameBuffer->getHeight());

    blitTransparentPipeline.enablePipeline();

    mainCameraVertexesData->enableVertexBuffers();
    mainCameraVertexesData->callDraw();
    mainCameraVertexesData->disableVertexBuffers();

    _colorFrameBuffer->disable();
}

void GraphicOperations::blitCubeShadow(ResolutionSettings _sourceSize, ShadowCubeFrameBuffer *_shadowCubeFrameBuffer, ColorFrameBuffer *_colorFrameBuffer)
{
    _colorFrameBuffer->enable();
    glViewport(0, 0, _colorFrameBuffer->getWidth(), _colorFrameBuffer->getHeight());

    blitCubeShadowPipeline.enablePipeline();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, _shadowCubeFrameBuffer->getCubeTexture());

    mainCameraVertexesData->enableVertexBuffers();
    mainCameraVertexesData->callDraw();
    mainCameraVertexesData->disableVertexBuffers();

    _colorFrameBuffer->disable();
}