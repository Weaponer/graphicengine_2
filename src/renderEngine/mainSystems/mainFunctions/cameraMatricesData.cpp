#include "renderEngine/mainSystems/mainFunctions/cameraMatricesData.h"

CameraMatricesDataUBO::CameraMatricesDataUBO()
{
    glGenBuffers(1, &indexUBOBuffer);
    glBindBuffer(GL_UNIFORM_BUFFER, indexUBOBuffer);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(CameraMatricesData), NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

CameraMatricesDataUBO::CameraMatricesDataUBO(GLenum _typeMemory)
{
    glGenBuffers(1, &indexUBOBuffer);
    glBindBuffer(GL_UNIFORM_BUFFER, indexUBOBuffer);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(CameraMatricesData), NULL, _typeMemory);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

CameraMatricesDataUBO::~CameraMatricesDataUBO()
{
    glDeleteBuffers(1, &indexUBOBuffer);
}

void CameraMatricesDataUBO::update()
{
    glBindBuffer(GL_UNIFORM_BUFFER, indexUBOBuffer);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(CameraMatricesData), &data);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}