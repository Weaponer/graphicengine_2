#include "renderEngine/mainSystems/mainFunctions/cameraVertexesData.h"

CameraVertexesData::CameraVertexesData() : matricesUBO(GL_STATIC_DRAW)
{
    glGenVertexArrays(1, &vertexArray);
    glBindVertexArray(vertexArray);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glGenBuffers(1, &bufferData);
    glBindBuffer(GL_ARRAY_BUFFER, bufferData);
    glBufferData(GL_ARRAY_BUFFER, sizeof(mth::vec2) * 4 * 2 + sizeof(mth::vec3) * 4, NULL, GL_STATIC_DRAW);
    points = {mth::vec2(-1.0, -1.0), mth::vec2(-1.0, 1.0),
              mth::vec2(1.0, -1.0), mth::vec2(1.0, 1.0)};

    uvs = {mth::vec2(0, 0), mth::vec2(0, 1.0),
           mth::vec2(1.0, 0), mth::vec2(1.0, 1.0)};

    normals = {mth::vec3(0, 0, 1), mth::vec3(0, 0, 1),
               mth::vec3(0, 0, 1), mth::vec3(0, 0, 1)};

    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(mth::vec2) * 4, &points);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(mth::vec2) * 4, sizeof(mth::vec2) * 4, &uvs);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(mth::vec2) * 4 * 2, sizeof(mth::vec3) * 4, &normals);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLuint *)0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLuint *)(sizeof(mth::vec2) * 4));
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (GLuint *)(sizeof(mth::vec2) * 4 * 2));
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

CameraVertexesData::~CameraVertexesData()
{
    glDeleteVertexArrays(1, &vertexArray);
    glDeleteBuffers(1, &bufferData);
}

void CameraVertexesData::enableVertexBuffers()
{
    glBindVertexArray(vertexArray);
    glBindBuffer(GL_ARRAY_BUFFER, bufferData);
}

void CameraVertexesData::disableVertexBuffers()
{
    /*glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);*/
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void CameraVertexesData::enableUniformBlockData(GLuint _index)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, _index, matricesUBO.indexUBOBuffer);
}

void CameraVertexesData::setDataNormals(const std::array<mth::vec3, 4> &_normals)
{
    normals = _normals;
    glBindBuffer(GL_ARRAY_BUFFER, bufferData);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(mth::vec2) * 4 * 2, sizeof(mth::vec3) * 4, &normals);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void CameraVertexesData::setDataNormals(const CameraGE *_cameraGE)
{
    const std::array<mth::vec3, 8> &pointsPiramidClipping = _cameraGE->pointsPiramidClipping;
    normals[0] = pointsPiramidClipping[6] - pointsPiramidClipping[2];
    normals[0].normalise();
    normals[1] = pointsPiramidClipping[4] - pointsPiramidClipping[0];
    normals[1].normalise();
    normals[2] = pointsPiramidClipping[7] - pointsPiramidClipping[3];
    normals[2].normalise();
    normals[3] = pointsPiramidClipping[5] - pointsPiramidClipping[1];
    normals[3].normalise();

    mth::mat4 rot = _cameraGE->rotate.getMatrix();
    for (int i = 0; i < 4; i++)
    {
        normals[i] = rot.transform(normals[i]);
    }

    glBindBuffer(GL_ARRAY_BUFFER, bufferData);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(mth::vec2) * 4 * 2, sizeof(mth::vec3) * 4, &normals);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void CameraVertexesData::setUBOMatrices(const CameraMatricesData *_matrices)
{
    matricesUBO.data = *_matrices;
    matricesUBO.update();
}

CameraMatricesDataUBO *CameraVertexesData::getUBOMatrices()
{
    return &matricesUBO;
}

void CameraVertexesData::callDraw()
{
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void CameraVertexesData::callDrawInstance(int _count)
{
    glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, _count);
}