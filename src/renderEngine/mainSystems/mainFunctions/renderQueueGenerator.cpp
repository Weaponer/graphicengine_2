#include "renderEngine/mainSystems/mainFunctions/renderQueueGenerator.h"

#include "debugOutput/globalProfiler.h"
#include "debugOutput/threadDebugOutput.h"

#include "renderEngine/mainSystems/sceneStructure/entities/meshRenderGE.h"

RenderQueueGenerator::RenderQueueGenerator(SceneStructure *_sceneStructure)
    : freeABOIterators(NULL),
      freeABO(NULL),
      freeDrawCalls(NULL),
      sceneStructure(_sceneStructure),
      sceneGrid(_sceneStructure->getSceneGrid())
{
    countObjects = 0;
    countMeshRenderers = 0;
    countPointLights = 0;
    countUseShaders = 0;
    countUsePasses = 0;
    countUseMaterials = 0;
    countUsePreDrawCalls = 0;

    for (int i = 0; i < MAX_COUNT_OBJECTS_PER_QUEUE; i++)
    {
        bufferIndexes[i] = i;
    }
}

RenderQueueGenerator::~RenderQueueGenerator()
{
    while (freeDrawCalls != NULL)
    {
        DrawCall *drawCall = freeDrawCalls;
        freeDrawCalls = drawCall->nextDrawCall;
        delete drawCall;
    }
    if (freeBuffersInstancings.getPos() != 0)
    {
        glDeleteBuffers(freeBuffersInstancings.getPos(), freeBuffersInstancings.getMemory(0));
    }

    AdditionalBuffersObjectsIterator *nextBuffer = freeABO;
    while (nextBuffer != NULL)
    {
        if (nextBuffer->buffer != NULL)
        {
            delete nextBuffer->buffer;
        }
        AdditionalBuffersObjectsIterator *oldIterator = nextBuffer;
        nextBuffer = nextBuffer->next;
        delete oldIterator;
    }
    freeABOIterators = NULL;

    AdditionalBuffersObjectsIterator *nextIterator = freeABOIterators;
    while (nextBuffer != NULL)
    {
        AdditionalBuffersObjectsIterator *oldIterator = nextBuffer;
        nextBuffer = nextBuffer->next;
        delete oldIterator;
    }
    freeABOIterators = NULL;
}

RenderQueue RenderQueueGenerator::generateRenderQueue(SettingsRenderQueue _settings)
{
    countMeshRenderers = 0;
    countUseShaders = 0;
    countUsePasses = 0;
    countUseMaterials = 0;
    countUseMeshs = 0;
    countUsePreDrawCalls = 0;

    RenderQueue queue;
    queue = {};

    queue.settings = _settings;

    fillBuffer(_settings);
    for (int i = 0; i < countMeshRenderers; i++)
    {
        MeshRenderGE *meshRenderer = bufferMeshRenderes[i];

        Material *mat = bufferUseMeshRenderesData[i].first;
        Mesh *mesh = bufferUseMeshRenderesData[i].second;
        Shader *shader = mat->getShader();
        if (mat->getRefOnTag() == NULL)
        {
            if (countUseMaterials >= MAX_COUNT_MATERIALS_PER_QUEUE)
            {
                continue;
            }
            mat->setRefOnTag(&(bufferIndexes[countUseMaterials]));
            countUseMaterials++;
        }
        if (mesh->getRefOnTag() == NULL)
        {
            if (countUseMeshs >= MAX_COUNT_MESHS_PER_QUEUE)
            {
                continue;
            }
            mesh->setRefOnTag(&(bufferIndexes[countUseMeshs]));
            countUseMeshs++;
        }
        if (shader->getRefOnTag() == NULL)
        {
            if (countUseShaders >= MAX_COUNT_SHADERS_PER_QUEUE)
            {
                continue;
            }
            shader->setRefOnTag(&(bufferIndexes[countUseShaders]));
            bufferShaders[countUseShaders] = shader;
            countUseShaders++;
        }
    }

    int maskTypePass = _settings.maskPasses;

    if ((TypePass::Depth & maskTypePass) != 0)
    {
        fillConcreteTypePass(queue, TypePass::Depth, DEPTH_PASS_INDEX);
    }
    if ((TypePass::Normal & maskTypePass) != 0)
    {
        fillConcreteTypePass(queue, TypePass::Normal, NORMAL_PASS_INDEX);
    }
    if ((TypePass::DepthNormal & maskTypePass) != 0)
    {
        fillConcreteTypePass(queue, TypePass::DepthNormal, DEPTH_NORMAL_PASS_INDEX);
    }
    if ((TypePass::BaseColor & maskTypePass) != 0)
    {
        fillConcreteTypePass(queue, TypePass::BaseColor, BASE_COLOR_PASS_INDEX);
    }
    if ((TypePass::Shadow & maskTypePass) != 0)
    {
        fillConcreteTypePass(queue, TypePass::Shadow, SHADOW_PASS_INDEX);
    }
    if ((TypePass::Surface & maskTypePass) != 0)
    {
        fillConcreteTypePass(queue, TypePass::Surface, SURFACE_PASS_INDEX);
    }

    for (int i = 0; i < countMeshRenderers; i++)
    {
        MeshRenderGE *meshRenderer = bufferMeshRenderes[i];

        Material *mat = bufferUseMeshRenderesData[i].first;
        Mesh *mesh = bufferUseMeshRenderesData[i].second;
        Shader *shader = mat->getShader();
        mat->clearRefOnTag();
        mesh->clearRefOnTag();
        shader->clearRefOnTag();
    }

    if (_settings.getPointLights)
    {
        RenderQueue::PointLights *pointLightsData = (RenderQueue::PointLights *)getFreeAdditionalBufferObjects(POINT_LIGHT_GRAPHIC_ENTITY);
        if (pointLightsData == NULL)
        {
            pointLightsData = new RenderQueue::PointLights();
        }
        pointLightsData->countPointLights = countPointLights;
        pointLightsData->pointLights = bufferPointLights;
        queue.pointLightsData = pointLightsData;
    }
    else
    {
        queue.pointLightsData = NULL;
    }

    if (_settings.getDensityVolumes)
    {
        RenderQueue::DensityVolumes *densityVolumesData = (RenderQueue::DensityVolumes *)getFreeAdditionalBufferObjects(DENSITY_VOLUME_GRAPHIC_ENTITY);
        if (densityVolumesData == NULL)
        {
            densityVolumesData = new RenderQueue::DensityVolumes();
        }
        densityVolumesData->countDensityVolumes = countDensityVolumes;
        densityVolumesData->densityVolumes = bufferDensityVolumes;
        queue.densityVolumesData = densityVolumesData;
    }
    else
    {
        queue.densityVolumesData = NULL;
    }
    return queue;
}

RenderQueue RenderQueueGenerator::generateRenderQueue(SettingsRenderQueue _newSettings, RenderQueue _oldRenderQueue)
{
    releaseRenderQueue(_oldRenderQueue);
    return generateRenderQueue(_newSettings);
}

void RenderQueueGenerator::releaseRenderQueue(RenderQueue _renderQueue)
{
    for (int i = 0; i < COUNT_TYPE_PASSES; i++)
    {
        if (_renderQueue.beginDrawCall[i] == NULL || _renderQueue.endDrawCall[i] == NULL)
        {
            continue;
        }

        if (_renderQueue.settings.generateInstancing)
        {
            DrawCall *next = _renderQueue.beginDrawCall[i];
            while (next != NULL)
            {
                if (next->countInstances > 1)
                {
                    returnBufferInstancing(next->uboMatricesModel);
                }
                next = next->nextDrawCall;
            }
        }

        _renderQueue.endDrawCall[i]->nextDrawCall = freeDrawCalls;
        freeDrawCalls = _renderQueue.beginDrawCall[i];
        _renderQueue.endDrawCall[i] = NULL;
        _renderQueue.beginDrawCall[i] = NULL;
    }

    if (_renderQueue.pointLightsData != NULL)
    {
        returnAdditionalBufferObjects(POINT_LIGHT_GRAPHIC_ENTITY, _renderQueue.pointLightsData);
        _renderQueue.pointLightsData = NULL;
    }

    if (_renderQueue.densityVolumesData != NULL)
    {
        returnAdditionalBufferObjects(DENSITY_VOLUME_GRAPHIC_ENTITY, _renderQueue.densityVolumesData);
        _renderQueue.densityVolumesData = NULL;
    }
}

GLuint RenderQueueGenerator::getBufferInstancing()
{
    if (freeBuffersInstancings.getPos() == 0)
    {
        GLuint buffer = 0;
        glGenBuffers(1, &buffer);
        glBindBuffer(GL_UNIFORM_BUFFER, buffer);
        glBufferData(GL_UNIFORM_BUFFER, sizeof(MeshRenderGE::MatricesData) * MAX_COUNT_INSTANCE_OBJECTS, NULL, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
        return buffer;
    }
    else
    {
        return *freeBuffersInstancings.getMemoryAndPop();
    }
}

void RenderQueueGenerator::returnBufferInstancing(GLuint _indexBuffer)
{
    freeBuffersInstancings.assignmentMemoryAndCopy(&_indexBuffer);
}

void RenderQueueGenerator::returnDrawCall(DrawCall *_drawCall)
{
    _drawCall->nextDrawCall = freeDrawCalls;
    freeDrawCalls = _drawCall;
}

inline DrawCall *RenderQueueGenerator::getDrawCall()
{
    DrawCall *drawCall = NULL;
    if (freeDrawCalls != NULL)
    {
        drawCall = freeDrawCalls;
        freeDrawCalls = drawCall->nextDrawCall;
    }
    else
    {
        drawCall = new DrawCall();
    }
    return drawCall;
}

inline void *RenderQueueGenerator::getFreeAdditionalBufferObjects(int _typeObjects)
{
    if (freeABO == NULL)
    {
        return NULL;
    }
    AdditionalBuffersObjectsIterator *nextBuffer = freeABO;
    AdditionalBuffersObjectsIterator *backBuffer = NULL;
    while (nextBuffer != NULL)
    {
        if (nextBuffer->typeObject == _typeObjects)
        {
            if (backBuffer == NULL)
            {
                freeABO = nextBuffer->next;
                nextBuffer->next = freeABOIterators;
                freeABOIterators = nextBuffer;
                void *buffer = nextBuffer->buffer;
                nextBuffer->buffer = NULL;
                nextBuffer->typeObject = -1;
                return buffer;
            }
            else
            {
                backBuffer->next = nextBuffer->next;

                nextBuffer->next = freeABOIterators;
                freeABOIterators = nextBuffer;

                void *buffer = nextBuffer->buffer;
                nextBuffer->buffer = NULL;
                nextBuffer->typeObject = -1;
                return buffer;
            }
        }
        backBuffer = nextBuffer;
        nextBuffer = nextBuffer->next;
    }

    return NULL;
}

void RenderQueueGenerator::returnAdditionalBufferObjects(int _typeObjects, void *_buffer)
{
    AdditionalBuffersObjectsIterator *iter = NULL;
    if (freeABOIterators == NULL)
    {
        iter = new AdditionalBuffersObjectsIterator();
        iter->next = freeABO;
        freeABO = iter;
        iter->buffer = _buffer;
        iter->typeObject = _typeObjects;
    }
    else
    {
        iter = freeABOIterators;
        freeABOIterators = iter->next;

        iter->next = freeABO;
        freeABO = iter;
        iter->typeObject = _typeObjects;
        iter->buffer = _buffer;
    }
}

void RenderQueueGenerator::fillTagsPassesShader(Shader *_shader, TypePass _type)
{
    int countPasses = _shader->getCountPasses(_type);
    for (int i = 0; i < countPasses; i++)
    {
        if (countUsePasses == MAX_COUNT_SHADER_PASSES_PER_QUEUE)
        {
            return;
        }
        _shader->getPass(_type, i)->setRefOnTag(&(bufferIndexes[countUsePasses]));
        countUsePasses++;
    }
}

void RenderQueueGenerator::clearTagsPassesShader(Shader *_shader, TypePass _type)
{
    int countPasses = _shader->getCountPasses(_type);
    for (int i = 0; i < countPasses; i++)
    {
        _shader->getPass(_type, i)->clearRefOnTag();
    }
}

void RenderQueueGenerator::fillBuffer(SettingsRenderQueue _settings)
{
    SearchAreaData searchArea;
    searchArea.maskTypeObjects = TypeGraphicEntity::MESH_RENDER_GRAPHIC_ENTITY;
    if (_settings.getPointLights)
    {
        searchArea.maskTypeObjects = searchArea.maskTypeObjects | TypeGraphicEntity::POINT_LIGHT_GRAPHIC_ENTITY;
    }
    if (_settings.getDensityVolumes)
    {
        searchArea.maskTypeObjects = searchArea.maskTypeObjects | TypeGraphicEntity::DENSITY_VOLUME_GRAPHIC_ENTITY;
    }
    searchArea.countClippingPlanes = 0;
    searchArea.clippingPlanes = NULL;
    searchArea.searchAreaType = SearchAreaType::OBB_SEARCH_BOUND;

    std::array<mth::plane, 4> planes;
    if (_settings.checkCamera)
    {
        mth::boundOBB camBound;
        mth::mat3();
        camBound.rotate = mth::matrix3FromMatrix4(_settings.camera->rotate.getMatrix());
        float near = _settings.camera->nearPlane;
        float far = _settings.camera->farPlane;

        float halfDistance = (far - near) / 2;
        mth::vec3 direction = camBound.rotate.transform(mth::vec3(0, 0, 1));
        direction.normalise();
        mth::vec3 camPos = _settings.camera->position;
        camBound.center = camPos + direction * (halfDistance + near);
        camBound.size = mth::vec3(_settings.camera->halfWidth, _settings.camera->halfHeight, halfDistance);
        searchArea.bound.obb = camBound;

        for (int i = 0; i < 4; i++)
        {
            mth::plane plane = _settings.camera->clippingPlanes[i + 2];
            plane = mth::plane(camPos, _settings.camera->rotate.rotateVector(plane.normal));
            planes[i] = plane;
        }
        searchArea.countClippingPlanes = 4;
        searchArea.clippingPlanes = &(planes[0]);
    }
    else
    {
        searchArea.bound.obb = _settings.searchArea.bound;
        searchArea.countClippingPlanes = _settings.searchArea.countClippingPlanes;
        searchArea.clippingPlanes = _settings.searchArea.clippingPlanes;
    }
    countObjects = sceneGrid->getObjectsInSearchArea(searchArea, MAX_COUNT_OBJECTS_PER_QUEUE, &(bufferObjectDatas[0]));
    countMeshRenderers = 0;
    countPointLights = 0;
    countDensityVolumes = 0;

    for (int i = 0; i < countObjects && countMeshRenderers < MAX_COUNT_OBJECTS_PER_QUEUE; i++)
    {
        ObjectData *objectData = bufferObjectDatas[i];
        void *object = objectData->mainObject;
        int typeObject = objectData->maskObjectType;
        if (typeObject == TypeGraphicEntity::MESH_RENDER_GRAPHIC_ENTITY)
        {
            MeshRenderGE *meshRender = (MeshRenderGE *)(object);
            int lod = getLodPerSettings(_settings, meshRender);
            std::pair<Material *, Mesh *> dataMeshRender = meshRender->getLod(lod);
            if (dataMeshRender.first != NULL && dataMeshRender.second != NULL)
            {
                bufferMeshRenderes[countMeshRenderers] = meshRender;
                bufferUseMeshRenderesData[countMeshRenderers] = std::make_pair(dataMeshRender.first, dataMeshRender.second);
                countMeshRenderers++;
            }
        }
    }

    if (_settings.getPointLights)
    {
        for (int i = 0; i < countObjects && countPointLights < MAX_COUNT_POINT_LIGHTS; i++)
        {
            ObjectData *objectData = bufferObjectDatas[i];
            void *object = objectData->mainObject;
            int typeObject = objectData->maskObjectType;

            if (typeObject == TypeGraphicEntity::POINT_LIGHT_GRAPHIC_ENTITY)
            {
                bufferPointLights[countPointLights] = (PointLightGE *)(object);
                countPointLights++;
            }
        }
    }

    if (_settings.getDensityVolumes)
    {
        for (int i = 0; i < countObjects && countDensityVolumes < MAX_COUNT_DENSITY_VOLUMES; i++)
        {
            ObjectData *objectData = bufferObjectDatas[i];
            void *object = objectData->mainObject;
            int typeObject = objectData->maskObjectType;
            
            if (typeObject == TypeGraphicEntity::DENSITY_VOLUME_GRAPHIC_ENTITY)
            {
                DensityVolumeGE *density = (DensityVolumeGE *)(object);
                bufferDensityVolumes[countDensityVolumes] = density;
                countDensityVolumes++;
            }
        }
    }
}

int RenderQueueGenerator::getLodPerSettings(SettingsRenderQueue &_settings, MeshRenderGE *_meshRender)
{
    if (_settings.useLodGroups == false)
    {
        return 0;
    }

    if (_meshRender->countLods() <= 1)
    {
        return 0;
    }

    CameraGE *camera = _settings.cameraForCheckLod;
    if (camera == NULL)
    {
        camera = _settings.camera;
    }

    mth::vec3 size = _meshRender->bound.size;
    float squareSize = size.magnitude();
    mth::vec3 pos = _meshRender->bound.origin;
    float squareDist = (pos - camera->position).magnitude();
    mth::vec4 p = mth::vec4(0.0f, squareSize, squareDist, 1.0f);
    mth::vec4 resultP = camera->matrixPojection.transform(p);
    float heigth = abs(resultP.y / resultP.w);
    heigth *= _settings.lodBias;
    return _meshRender->getLodPerCoef(heigth);
}

void RenderQueueGenerator::fillConcreteTypePass(RenderQueue &_renderQueue, TypePass _type, int _indexBegin)
{
    for (int i = 0; i < countUseShaders; i++)
    {
        fillTagsPassesShader(bufferShaders[i], _type);
    }

    countUsePreDrawCalls = 0;
    int maskQueuesRender = _renderQueue.settings.maskQueuesRender;
    int maskRenderType = _renderQueue.settings.maskRenderTypes;

    bool generateDistances = _renderQueue.settings.sort && _renderQueue.settings.checkCamera;
    float nearPlane = 0;
    float farPlane = 0;
    mth::vec3 posCamera = mth::vec3();
    if (generateDistances)
    {
        CameraGE *camera = _renderQueue.settings.camera;
        nearPlane = camera->nearPlane;
        nearPlane = nearPlane * nearPlane;
        farPlane = camera->farPlane;
        farPlane = farPlane * farPlane;
        posCamera = camera->position;
    }

    for (int i = 0; i < countMeshRenderers; i++)
    {
        MeshRenderGE *meshRenderer = bufferMeshRenderes[i];

        if (meshRenderer->generateShadows == false && _type == TypePass::Shadow)
        {
            continue;
        }

        Material *material = bufferUseMeshRenderesData[i].first;
        Mesh *mesh = bufferUseMeshRenderesData[i].second;
        Shader *shader = material->getShader();
        int countNeedPasses = shader->getCountPasses(_type);
        float distance = 0;
        if (countNeedPasses != 0 && generateDistances)
        {
            mth::vec3 posMeshRenderer = meshRenderer->position;
            mth::vec3 diff = posMeshRenderer - posCamera;
            distance = 1.0f - ((diff.x * diff.x + diff.y * diff.y + diff.z * diff.z) - nearPlane) / (farPlane - nearPlane);
        }

        for (int i2 = 0; i2 < countNeedPasses; i2++)
        {
            if (countUsePreDrawCalls >= MAX_COUNT_DRAW_CALLS_PER_TYPE_PASS)
            {
                goto END_FILL_PRE_DRAW_CALLS;
            }
            Pass *pass = shader->getPass(_type, i2);
            if ((pass->getQueueRender() & maskQueuesRender) == 0 ||
                (pass->getRenderType() & maskRenderType) == 0 || pass->getRefOnTag() == NULL)
            {
                continue;
            }
            PreDrawCall preDrawCall;
            preDrawCall.material = material;
            preDrawCall.shader = shader;
            preDrawCall.mesh = mesh;
            preDrawCall.uboMatrices = meshRenderer->bufferMatricesData;
            preDrawCall.matricesData = &meshRenderer->matrices;
            preDrawCall.pass = shader->getPass(_type, i2);
            preDrawCall.numPass = i2;
            pass->getIndexesPrograms(material->getMaskDefineProperties(), &preDrawCall.numVertProgram, &preDrawCall.numFragProgram);
            preDrawCall.distance = distance;

            bufferValueDrawCalls[countUsePreDrawCalls] = getForcePreDrawCall(preDrawCall, countUsePreDrawCalls);
            bufferPreDrawCalls[countUsePreDrawCalls] = preDrawCall;
            countUsePreDrawCalls++;
        }
    }
END_FILL_PRE_DRAW_CALLS:

    if (countUsePreDrawCalls != 0)
    {
        std::sort(&bufferValueDrawCalls[0], &bufferValueDrawCalls[countUsePreDrawCalls]);
    }
    else
    {
        goto END_FILL_DRAW_CALLS;
    }
    if (_renderQueue.settings.generateInstancing && countUsePreDrawCalls != 1)
    {
        DrawCall *refDrawCall = getDrawCall();
        refDrawCall->nextDrawCall = NULL;
        _renderQueue.endDrawCall[_indexBegin] = refDrawCall;
        _renderQueue.beginDrawCall[_indexBegin] = refDrawCall;

        for (int i = 0; i < countUsePreDrawCalls; i++)
        {
            unsigned long index = bufferValueDrawCalls[i].second & (unsigned long)(MAX_COUNT_DRAW_CALLS_PER_TYPE_PASS - 1);
            PreDrawCall preDrawCall = bufferPreDrawCalls[index];
            Pass *pass = preDrawCall.shader->getPass(_type, preDrawCall.numPass);

            int startI = i;

            int countInstances = 1;
            if (i != countUsePreDrawCalls - 1 && pass->generateInstancingOn() && preDrawCall.material->getGenerateInstancing())
            {
                bufferReferencesOnMatrices[0] = preDrawCall.matricesData;
                for (int i2 = i + 1; i2 < countUsePreDrawCalls; i2++)
                {
                    unsigned long indexNext = bufferValueDrawCalls[i2].second & (unsigned long)(MAX_COUNT_DRAW_CALLS_PER_TYPE_PASS - 1);
                    PreDrawCall preDrawCallNext = bufferPreDrawCalls[indexNext];
                    if (countInstances < MAX_COUNT_INSTANCE_OBJECTS && preDrawCallNext.material == preDrawCall.material &&
                        pass == preDrawCallNext.pass && preDrawCall.mesh == preDrawCallNext.mesh)
                    {
                        bufferReferencesOnMatrices[countInstances] = preDrawCallNext.matricesData;
                        countInstances++;
                        i = i2;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            DrawCall drawCallForFill;
            drawCallForFill.material = preDrawCall.material;
            drawCallForFill.shader = preDrawCall.shader;
            drawCallForFill.mesh = preDrawCall.mesh;
            drawCallForFill.renderType = pass->getRenderType();
            drawCallForFill.typeQueueRender = pass->getQueueRender();
            drawCallForFill.pass = pass;
            drawCallForFill.numVertexShader = preDrawCall.numVertProgram;
            drawCallForFill.numFragmentShader = preDrawCall.numFragProgram;
            drawCallForFill.pipeline = pass->getProgramPipeline(drawCallForFill.numVertexShader, drawCallForFill.numFragmentShader);
            drawCallForFill.nextDrawCall = NULL;

            if (countInstances == 1)
            {
                drawCallForFill.uboMatricesModel = preDrawCall.uboMatrices;
                drawCallForFill.countInstances = 1;
            }
            else
            {
                GLuint uboMatrices = getBufferInstancing();
                glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
                for (int i2 = 0; i2 < countInstances; i2++)
                {
                    bufferMatricesForInstancing[i2] = *bufferReferencesOnMatrices[i2];
                }
                glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(MeshRenderGE::MatricesData) * countInstances, &(bufferMatricesForInstancing[0]));
                drawCallForFill.uboMatricesModel = uboMatrices;
                drawCallForFill.countInstances = countInstances;
            }

            *refDrawCall = drawCallForFill;

            refDrawCall->nextDrawCall = _renderQueue.beginDrawCall[_indexBegin];
            _renderQueue.beginDrawCall[_indexBegin] = refDrawCall;
            refDrawCall = getDrawCall();
            refDrawCall->nextDrawCall = NULL;
        }
        glBindBuffer(GL_UNIFORM_BUFFER, 0);

        returnDrawCall(refDrawCall);
        _renderQueue.endDrawCall[_indexBegin]->nextDrawCall = NULL;
    }
    else
    {
        DrawCall *refDrawCall = getDrawCall();
        refDrawCall->nextDrawCall = NULL;
        _renderQueue.endDrawCall[_indexBegin] = refDrawCall;
        _renderQueue.beginDrawCall[_indexBegin] = refDrawCall;

        for (int i = 0; i < countUsePreDrawCalls; i++)
        {
            unsigned long index = bufferValueDrawCalls[i].second & (unsigned long)(MAX_COUNT_DRAW_CALLS_PER_TYPE_PASS - 1);
            PreDrawCall preDrawCall = bufferPreDrawCalls[index];
            Pass *pass = preDrawCall.shader->getPass(_type, preDrawCall.numPass);
            DrawCall drawCallForFill;
            drawCallForFill.material = preDrawCall.material;
            drawCallForFill.shader = preDrawCall.shader;
            drawCallForFill.mesh = preDrawCall.mesh;
            drawCallForFill.renderType = pass->getRenderType();
            drawCallForFill.typeQueueRender = pass->getQueueRender();
            drawCallForFill.pass = pass;
            drawCallForFill.numVertexShader = preDrawCall.numVertProgram;
            drawCallForFill.numFragmentShader = preDrawCall.numFragProgram;
            drawCallForFill.uboMatricesModel = preDrawCall.uboMatrices;
            drawCallForFill.pipeline = pass->getProgramPipeline(drawCallForFill.numVertexShader, drawCallForFill.numFragmentShader);
            drawCallForFill.countInstances = 1;
            drawCallForFill.nextDrawCall = NULL;

            *refDrawCall = drawCallForFill;

            refDrawCall->nextDrawCall = _renderQueue.beginDrawCall[_indexBegin];
            _renderQueue.beginDrawCall[_indexBegin] = refDrawCall;
            refDrawCall = getDrawCall();
            refDrawCall->nextDrawCall = NULL;
        }
        returnDrawCall(refDrawCall);
        _renderQueue.endDrawCall[_indexBegin]->nextDrawCall = NULL;
    }

END_FILL_DRAW_CALLS:
    for (int i = 0; i < countUseShaders; i++)
    {
        clearTagsPassesShader(bufferShaders[i], _type);
    }
}

inline std::pair<unsigned long, unsigned long> RenderQueueGenerator::getForcePreDrawCall(PreDrawCall &_preDrawCall, int _indexPos)
{
    unsigned long indexPass = *((int *)_preDrawCall.pass->getRefOnTag()) & (unsigned long)(MAX_COUNT_SHADER_PASSES_PER_QUEUE - 1);
    unsigned long indexVertexProgram = _preDrawCall.numVertProgram & 255UL;
    unsigned long indexFragmentProgram = _preDrawCall.numFragProgram & 255UL;
    unsigned long indexMaterial = *((int *)_preDrawCall.material->getRefOnTag()) & (unsigned long)(MAX_COUNT_MATERIALS_PER_QUEUE - 1);
    unsigned long indexMesh = ((unsigned long)_preDrawCall.mesh->getRefOnTag()) & (unsigned long)(MAX_COUNT_MESHS_PER_QUEUE - 1);
    unsigned long distance = ((unsigned long)(_preDrawCall.distance * 4095)) & 4095UL;
    unsigned long indexPos = (unsigned long)_indexPos & (unsigned long)(MAX_COUNT_DRAW_CALLS_PER_TYPE_PASS - 1);

    unsigned long firstData = ((((0 | indexPass) << 8 | indexVertexProgram) << 8 | indexFragmentProgram) << 12 | indexMaterial) << 12 | indexMesh;
    unsigned long secondData = (0 | distance) << 14 | indexPos;

    std::pair<unsigned long, unsigned long> result = std::make_pair(firstData, secondData);

    return result;
}