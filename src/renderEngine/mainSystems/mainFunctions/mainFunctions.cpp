#include "renderEngine/mainSystems/mainFunctions/mainFunctions.h"
#include "debugOutput/globalProfiler.h"

MainFunctions::MainFunctions(TechnicalShaderStorage *_technicalShaderStorage, SceneStructure *_sceneStructure, SettingsGraphicPipeline *_settingsGraphicPipeline)
    : basicCamera(),
      cameraVertexesData(),
      graphicOperations(_technicalShaderStorage, &cameraVertexesData),
      renderQueueGenerator(new RenderQueueGenerator(_sceneStructure)),
      technicalShaderStorage(_technicalShaderStorage),
      sceneStructure(_sceneStructure),
      entityRegister(_sceneStructure->getEntityRegister()),
      settingsGraphicPipeline(_settingsGraphicPipeline),
      deltaTime(0),
      allTime(0)
{
    mainSettingsRenderQueue = {};

    mainSettingsRenderQueue.checkCamera = true;
    mainSettingsRenderQueue.maskPasses = TypePass::Depth | TypePass::DepthNormal | TypePass::Normal | TypePass::BaseColor | TypePass::Surface;
    mainSettingsRenderQueue.maskQueuesRender = QueueRender::QueueRender_AlphaTest | QueueRender::QueueRender_Geometry |
                                               QueueRender::QueueRender_Overlay | QueueRender::QueueRender_Transparent;
    mainSettingsRenderQueue.maskRenderTypes = RenderType::RenderType_Opaque | RenderType::RenderType_Transparent | RenderType::RenderType_TransparentCutout;
    mainSettingsRenderQueue.sort = true;
    mainSettingsRenderQueue.generateInstancing = true;
    mainSettingsRenderQueue.getPointLights = true;
    mainSettingsRenderQueue.getDensityVolumes = true;

    mainRenderQueue = {};
}

MainFunctions::~MainFunctions()
{
    renderQueueGenerator->releaseRenderQueue(mainRenderQueue);
    delete renderQueueGenerator;
}

void MainFunctions::update(float _deltaTime)
{
    deltaTime = _deltaTime;
    allTime += _deltaTime;

    basicCamera.update(entityRegister->getActiveCamera());
    cameraVertexesData.setDataNormals(basicCamera.getMainCamera());
    cameraVertexesData.setUBOMatrices(basicCamera.getMatriciesMainCamera());

    mainSettingsRenderQueue.camera = basicCamera.getMainCamera();

    if (settingsGraphicPipeline->lodSettings.useLods)
    {
        mainSettingsRenderQueue.useLodGroups = true;
        mainSettingsRenderQueue.lodBias = settingsGraphicPipeline->lodSettings.getLodBias();
        mainSettingsRenderQueue.cameraForCheckLod = mainSettingsRenderQueue.camera;
    }
    else
    {
        mainSettingsRenderQueue.useLodGroups = false;
    }

    mainSettingsRenderQueue.cameraForCheckLod = basicCamera.getMainCamera();

    GlobalProfiler::startSample("Graphic: generation main render queue");
    mainRenderQueue = renderQueueGenerator->generateRenderQueue(mainSettingsRenderQueue, mainRenderQueue);
    GlobalProfiler::stopSample("Graphic: generation main render queue");
}

BasicCamera *MainFunctions::getBasicCamera()
{
    return &basicCamera;
}

CameraVertexesData *MainFunctions::getCameraVertexesData()
{
    return &cameraVertexesData;
}

GraphicOperations *MainFunctions::getGraphicOperations()
{
    return &graphicOperations;
}

RenderQueueGenerator *MainFunctions::getRenderQueueGenerator()
{
    return renderQueueGenerator;
}

RenderQueue *MainFunctions::getMainRenderQueue()
{
    return &mainRenderQueue;
}

TechnicalShaderStorage *MainFunctions::getTechnicalShaderStorage()
{
    return technicalShaderStorage;
}

SettingsGraphicPipeline *MainFunctions::getSettingsGraphicPipeline()
{
    return settingsGraphicPipeline;
}

float MainFunctions::getDeltaTime() const
{
    return deltaTime;
}

float MainFunctions::getAllTime() const
{
    return allTime;
}