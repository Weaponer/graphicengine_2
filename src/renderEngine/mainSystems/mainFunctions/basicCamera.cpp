#include <MTH/matrix.h>
#include <MTH/glGen_matrix.h>

#include "renderEngine/mainSystems/mainFunctions/basicCamera.h"

BasicCamera::BasicCamera() : mainCamera(NULL)
{
}

BasicCamera::~BasicCamera()
{
}

void BasicCamera::update(CameraGE *_currentCamera)
{
    mainCamera = _currentCamera;
    matriciesMainCamera.near_plane = mainCamera->nearPlane;
    matriciesMainCamera.far_plane = mainCamera->farPlane;
    matriciesMainCamera.position = mainCamera->position;
    const mth::vec3 dirVec = mth::vec3(0, 0, 1);
    matriciesMainCamera.direction = mainCamera->rotate.rotateVector(dirVec);
    matriciesMainCamera.matrix_P = mainCamera->matrixPojection;
    matriciesMainCamera.matrixInverse_P = mainCamera->inverseMatrixPojection;

    matriciesMainCamera.matrixInverse_V = mth::TRS(mainCamera->position, mainCamera->rotate, mth::vec3(1, 1, 1));
    matriciesMainCamera.matrix_V = mth::inverseTRS(matriciesMainCamera.matrixInverse_V);
    matriciesMainCamera.matrix_VP = matriciesMainCamera.matrix_P * matriciesMainCamera.matrix_V;
}

CameraGE *BasicCamera::getMainCamera() const
{
    return mainCamera;
}

const CameraMatricesData *BasicCamera::getMatriciesMainCamera() const
{
    return &matriciesMainCamera;
}