#include <iostream>

#include "globalProfiler.h"
#include "renderEngine/mainSystems/programActivator/programActivator.h"
#include "renderEngine/graphicPipeline.h"

ProgramActivator::ProgramActivator()
{
    functionProperties[(int)(TypeProperty::Texture_P)] = &ProgramActivator::pushTextureProperty;
    functionProperties[(int)(TypeProperty::Int_P)] = &ProgramActivator::pushIntProperty;
    functionProperties[(int)(TypeProperty::Float_P)] = &ProgramActivator::pushFloatProperty;
    functionProperties[(int)(TypeProperty::Toggle_P)] = &ProgramActivator::pushToggleProperty;
    functionProperties[(int)(TypeProperty::CubeMap_P)] = &ProgramActivator::pushCubeMapProperty;
    functionProperties[(int)(TypeProperty::Vector_P)] = &ProgramActivator::pushVectorProperty;
    functionProperties[(int)(TypeProperty::Color_P)] = &ProgramActivator::pushColorProperty;
}

ProgramActivator::~ProgramActivator()
{
}

void ProgramActivator::setRegistryProperty(PropertiesRegistry *_registryProperty)
{
    defaultProperties.initPropertiesRegistry(this, _registryProperty);
}

void ProgramActivator::setGraphicPipeline(GraphicPipeline *_gprahicPipeline)
{
    gprahicPipeline = _gprahicPipeline;
    defaultProperties.setGraphicPipeline(_gprahicPipeline);
}

void ProgramActivator::setMainFunctions(MainFunctions *_mainFunctions)
{
    mainFunctions = _mainFunctions;
}

MainFunctions *ProgramActivator::getMainFunctions()
{
    return mainFunctions;
}

void ProgramActivator::setUniformBlocksRegistry(UniformBlocksRegistry *_uniformBlocksRegistry)
{
    uniformBlocksRegistry = _uniformBlocksRegistry;
    _uniformBlocksRegistry->addNewReserveUniformBLock(MATRICES_CAMERA_UNIFORM_BLOCK_N);
    _uniformBlocksRegistry->addNewReserveUniformBLock(MATRICES_OBJECT_UNIFORM_BLOCK_N);
    _uniformBlocksRegistry->addNewReserveUniformBLock(SKY_DATA_UNIFORM_BLOCK_N);
    _uniformBlocksRegistry->addNewReserveUniformBLock(ALL_POINT_LIHGT_UNIFORM_BLOCK_N);

    indexBindingMatricesCamera = _uniformBlocksRegistry->getIndexReserveUniformBlock(MATRICES_CAMERA_UNIFORM_BLOCK_N);
    indexBindingMatricesObject = _uniformBlocksRegistry->getIndexReserveUniformBlock(MATRICES_OBJECT_UNIFORM_BLOCK_N);

    functionsEnableUBO.push_back(&ProgramActivator::enableUBOMatricesCamera);
    functionsEnableUBO.push_back(&ProgramActivator::enableUBOMatricesObject);
    functionsEnableUBO.push_back(&ProgramActivator::enableUBOSkyData);
    functionsEnableUBO.push_back(&ProgramActivator::enableUBOAllPointLight);
}

void ProgramActivator::drawRenderQueue(RenderQueue _renderQueue, CameraMatricesDataUBO *_cameraMatricesDataUBO, SettingsDrawRenderQueue _settings)
{

    DrawCall *beginDrawCall = NULL;
    switch (_settings.typePass)
    {
    case TypePass::Depth:
        beginDrawCall = _renderQueue.beginDrawCall[DEPTH_PASS_INDEX];
        break;
    case TypePass::Normal:
        beginDrawCall = _renderQueue.beginDrawCall[NORMAL_PASS_INDEX];
        break;
    case TypePass::DepthNormal:
        beginDrawCall = _renderQueue.beginDrawCall[DEPTH_NORMAL_PASS_INDEX];
        break;
    case TypePass::BaseColor:
        beginDrawCall = _renderQueue.beginDrawCall[BASE_COLOR_PASS_INDEX];
        enableBufferForBaseColor();
        break;
    case TypePass::Shadow:
        beginDrawCall = _renderQueue.beginDrawCall[SHADOW_PASS_INDEX];
        break;
    case TypePass::Surface:
        beginDrawCall = _renderQueue.beginDrawCall[SURFACE_PASS_INDEX];
        DrawCall *test = beginDrawCall;
        break;
    }

    while (beginDrawCall != NULL)
    {
        if ((beginDrawCall->typeQueueRender & _settings.maskQueueRender) != 0 && (beginDrawCall->renderType & _settings.maskRenderTypes) != 0)
        {
            break;
        }
        beginDrawCall = beginDrawCall->nextDrawCall;
    }

    if (beginDrawCall == NULL)
    {
        return;
    }

    defaultProperties.setRenderStage(_settings.renderStage);

    DrawCall *drawCall = beginDrawCall;
    Shader *shader = drawCall->shader;
    Material *material = drawCall->material;
    Pass *pass = drawCall->pass;
    int numVertexShader = drawCall->numVertexShader;
    int numFragmentShader = drawCall->numFragmentShader;

    Program *vertProgram = NULL;
    Program *fragProgram = NULL;
    Program *programs[2] = {vertProgram, fragProgram};
    if (pass->checkIsDefaultPass())
    {
        enableDefaultPass(pass, shader, material);
    }
    else
    {
        vertProgram = pass->getVertexProgram(numVertexShader);
        fragProgram = pass->getFragmentProgram(numFragmentShader);

        programs[0] = vertProgram;
        programs[1] = fragProgram;
        fillProgramsProperties(material, pass, &(programs[0]), 2);
        enableUseUnifromBlocks(vertProgram);
        enableUseUnifromBlocks(fragProgram);
    }

    if (pass->zTestOn())
    {
        glDepthFunc(GL_LEQUAL);
    }
    else
    {
        glDepthFunc(GL_ALWAYS);
    }

    bool cullEnabled = false;

    CullType cullType = pass->getCullType();
    switch (cullType)
    {
    case CullType::CullType_Back:
        glEnable(GL_CULL_FACE);
        cullEnabled = true;
        glCullFace(GL_BACK);
        break;
    case CullType::CullType_Front:
        glEnable(GL_CULL_FACE);
        cullEnabled = true;
        glCullFace(GL_FRONT);
        break;
    }

    CullType oldCullType = cullType;

    glBindBufferBase(GL_UNIFORM_BUFFER, indexBindingMatricesCamera, _cameraMatricesDataUBO->indexUBOBuffer);

    glBindProgramPipeline(drawCall->pipeline);

    Mesh *mesh = drawCall->mesh;
    mesh->enableBuffers();

    uint countInstances = drawCall->countInstances;

    if (countInstances == 1)
    {
        glBindBufferBase(GL_UNIFORM_BUFFER, indexBindingMatricesObject, drawCall->uboMatricesModel);
        glDrawElements(GL_TRIANGLES, mesh->getCountIndexes(), GL_UNSIGNED_SHORT, (GLvoid *)0);
    }
    else
    {
        glBindBufferRange(GL_UNIFORM_BUFFER, indexBindingMatricesObject, drawCall->uboMatricesModel, 0, sizeof(MeshRenderGE::MatricesData) * countInstances);
        glDrawElementsInstanced(GL_TRIANGLES, mesh->getCountIndexes(), GL_UNSIGNED_SHORT, (GLvoid *)0, countInstances);
    }

    drawCall = drawCall->nextDrawCall;

    while (drawCall != NULL)
    {
        if ((drawCall->typeQueueRender & _settings.maskQueueRender) == 0 || (drawCall->renderType & _settings.maskRenderTypes) == 0)
        {
            drawCall = drawCall->nextDrawCall;
            continue;
        }

        bool changeShader = true;
        bool changeMaterial = true;
        bool changePass = true;
        bool changeMesh = true;
        Shader *newShader = drawCall->shader;
        if (newShader != shader)
        {
            changeMesh = true;
            shader = newShader;
            changePass = true;
            pass = drawCall->pass;
            numVertexShader = drawCall->numVertexShader;
            numFragmentShader = drawCall->numFragmentShader;
        }
        else if (pass != drawCall->pass || numVertexShader != drawCall->numVertexShader || numFragmentShader != drawCall->numFragmentShader)
        {
            changePass = true;
            pass = drawCall->pass;
            numVertexShader = drawCall->numVertexShader;
            numFragmentShader = drawCall->numFragmentShader;
        }

        Material *newMaterial = drawCall->material;
        if (newMaterial != material)
        {
            changeMaterial = true;
            material = newMaterial;
        }

        Mesh *newMesh = drawCall->mesh;
        if (newMesh != mesh)
        {
            changeMesh = true;
            mesh = newMesh;
        }

        if (changeShader | changePass)
        {
            if (pass->checkIsDefaultPass())
            {
                enableDefaultPass(pass, shader, material);
            }
            else
            {
                vertProgram = pass->getVertexProgram(numVertexShader);
                fragProgram = pass->getFragmentProgram(numFragmentShader);
                programs[0] = vertProgram;
                programs[1] = fragProgram;
                fillProgramsProperties(material, pass, &(programs[0]), 2);
                enableUseUnifromBlocks(vertProgram);
                enableUseUnifromBlocks(fragProgram);
            }

            if (pass->zTestOn())
            {
                glDepthFunc(GL_LEQUAL);
            }
            else
            {
                glDepthFunc(GL_ALWAYS);
            }

            cullType = pass->getCullType();
            if (cullType != oldCullType)
            {
                switch (cullType)
                {
                case CullType::CullType_Back:
                    if (!cullEnabled)
                    {
                        glEnable(GL_CULL_FACE);
                        cullEnabled = true;
                    }
                    glCullFace(GL_BACK);
                    break;
                case CullType::CullType_Front:
                    if (!cullEnabled)
                    {
                        glEnable(GL_CULL_FACE);
                        cullEnabled = true;
                    }
                    glCullFace(GL_FRONT);
                    break;
                case CullType::CullType_Off:
                    if (cullEnabled)
                    {
                        glDisable(GL_CULL_FACE);
                        cullEnabled = false;
                    }
                    break;
                }

                oldCullType = cullType;
            }

            glBindProgramPipeline(drawCall->pipeline);
        }
        else if (changeMaterial)
        {
            if (pass->checkIsDefaultPass())
            {
                updateMaterialForDefaultPass(pass, material);
            }
            else
            {
                fillProgramsProperties(material, pass, &(programs[0]), 2);
            }
        }

        if (changeMesh)
        {
            mesh->enableBuffers();
        }

        countInstances = drawCall->countInstances;
        if (countInstances == 1)
        {
            glBindBufferBase(GL_UNIFORM_BUFFER, indexBindingMatricesObject, drawCall->uboMatricesModel);
            glDrawElements(GL_TRIANGLES, mesh->getCountIndexes(), GL_UNSIGNED_SHORT, (GLvoid *)0);
        }
        else
        {
            glBindBufferRange(GL_UNIFORM_BUFFER, indexBindingMatricesObject, drawCall->uboMatricesModel, 0, sizeof(MeshRenderGE::MatricesData) * countInstances);
            glDrawElementsInstanced(GL_TRIANGLES, mesh->getCountIndexes(), GL_UNSIGNED_SHORT, (GLvoid *)0, countInstances);
        }
        drawCall = drawCall->nextDrawCall;
    }
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindProgramPipeline(0);

    if (cullEnabled)
    {
        glDisable(GL_CULL_FACE);
    }
}

void ProgramActivator::drawCall(Material *_material, Mesh *_mesh, TypePass _typePass, int _numPass)
{
    Shader *shader = _material->getShader();
    GLuint pipeline = 0;
    enableProgramInMaterial(_material, _typePass, _numPass, &pipeline);
    Pass *pass = shader->getPass(_typePass, _numPass);
    if (pass->zTestOn())
    {
        glDepthFunc(GL_LEQUAL);
    }
    else
    {
        glDepthFunc(GL_ALWAYS);
    }

    CullType cullType = pass->getCullType();
    bool cullEnabled = false;
    switch (cullType)
    {
    case CullType::CullType_Back:
        glEnable(GL_CULL_FACE);
        cullEnabled = true;
        glCullFace(GL_BACK);
        break;
    case CullType::CullType_Front:
        glEnable(GL_CULL_FACE);
        cullEnabled = true;
        glCullFace(GL_FRONT);
        break;
    }

    glBindProgramPipeline(pipeline);

    _mesh->enableBuffers();
    glDrawElements(GL_TRIANGLES, _mesh->getCountIndexes(), GL_UNSIGNED_SHORT, (GLvoid *)0);
    _mesh->disableBuffers();

    glBindProgramPipeline(0);

    if (cullEnabled)
    {
        glDisable(GL_CULL_FACE);
    }
}

void ProgramActivator::enableProgramInMaterial(Material *_material, TypePass _typePass, int _numPass, GLuint *_pipeline)
{
    Shader *shader = _material->getShader();
    Pass *_pass = shader->getPass(_typePass, _numPass);

    if (_pass->checkIsDefaultPass())
    {
        enableDefaultPass(_pass, shader, _material);
        *_pipeline = _pass->getProgramPipeline(0, 0);
        return;
    }

    int countProperties = _material->getCount();
    int mask = 0;
    for (int i = 0; i < countProperties; i++)
    {
        const Property *property = _material->getProperty(i);
        if (property->getType() == TypeProperty::Toggle_P)
        {
            property = getCorrectProperty(property);
            const ToggleProperty *toggle = reinterpret_cast<const ToggleProperty *>(property);
            int localmask = shader->getMaskToggle(toggle->getCodeData());
            if (toggle->getValue())
            {
                mask = mask | localmask;
            }
        }
    }

    int indexVertProg;
    int indexFragProg;

    _pass->getIndexesPrograms(mask, &indexVertProg, &indexFragProg);
    Program *vertexProgram = _pass->getVertexProgram(indexVertProg);
    Program *fragmentProgram = _pass->getFragmentProgram(indexFragProg);

    GLuint pipeline = _pass->getProgramPipeline(indexVertProg, indexFragProg);

    for (int i = 0; i < countProperties; i++)
    {
        const Property *property = _material->getProperty(i);
        TypeProperty type = property->getType();
        (*this.*functionProperties[(int)type])(property, vertexProgram);
        (*this.*functionProperties[(int)type])(property, fragmentProgram);
    }

    if (_typePass == TypePass::Depth)
    {
        defaultProperties.setRenderStage(RenderStage::RS_DEPTH_STAGE);
    }
    else if (_typePass == TypePass::Normal)
    {
        defaultProperties.setRenderStage(RenderStage::RS_NORMAL_STAGE);
    }
    else if (_typePass == TypePass::Shadow)
    {
        defaultProperties.setRenderStage(RenderStage::RS_SHADOW_STAGE);
    }
    else
    {
        defaultProperties.setRenderStage(RenderStage::RS_BASIC_COLOR_STAGE);
        enableBufferForBaseColor();
    }

    countProperties = vertexProgram->getCountInternalProperties();
    for (int i = 0; i < countProperties; i++)
    {
        pushInternalProperty(_pass, vertexProgram, vertexProgram->getIndexInternalProperty(i));
    }
    countProperties = fragmentProgram->getCountInternalProperties();
    for (int i = 0; i < countProperties; i++)
    {
        pushInternalProperty(_pass, fragmentProgram, fragmentProgram->getIndexInternalProperty(i));
    }

    enableUseUnifromBlocks(vertexProgram);
    enableUseUnifromBlocks(fragmentProgram);

    *_pipeline = pipeline;
}

void ProgramActivator::enableDefaultPass(Pass *_pass, Shader *_shader, Material *_material)
{
    char dataPass = _pass->getDataDefaultPassSettings();
    bool useClippings = dataPass & (1) != 0;
    if (useClippings)
    {
        glBindBufferBase(GL_UNIFORM_BUFFER, 2, _shader->getUBOClippingSettings());
        Texture *clippingTexture = _material->getSpecialClippingTexture();
        if (clippingTexture != NULL)
        {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, clippingTexture->getIndexTexture());
        }
    }
    bool useNormalMaps = (dataPass & (1 << 1) != 0) && (dataPass & (1 << 2) == 0);

    Texture *normalMap = _material->getSpecialNormalMap();
    if (normalMap != NULL)
    {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, normalMap->getIndexTexture());
    }
}

void ProgramActivator::updateMaterialForDefaultPass(Pass *_pass, Material *_material)
{
    char dataPass = _pass->getDataDefaultPassSettings();
    bool useClippings = dataPass & (1) != 0;
    if (useClippings)
    {
        Texture *clippingTexture = _material->getSpecialClippingTexture();
        if (clippingTexture != NULL)
        {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, clippingTexture->getIndexTexture());
        }
    }
    bool useNormalMaps = (dataPass & (1 << 1) != 0) && (dataPass & (1 << 2) == 0);
    Texture *normalMap = _material->getSpecialNormalMap();
    if (normalMap != NULL)
    {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, normalMap->getIndexTexture());
    }
}

void ProgramActivator::fillProgramsProperties(Material *_material, Pass *_pass, Program **_programs, int _countPrograms)
{
    int countProperties = _material->getCount();

    for (int i = 0; i < countProperties; i++)
    {
        const Property *property = _material->getProperty(i);
        TypeProperty type = property->getType();
        for (int i2 = 0; i2 < _countPrograms; i2++)
        {
            (*this.*functionProperties[(int)type])(property, _programs[i2]);
        }
    }

    for (int i = 0; i < _countPrograms; i++)
    {
        int countProperties = _programs[i]->getCountInternalProperties();
        for (int i2 = 0; i2 < countProperties; i2++)
        {
            pushInternalProperty(_pass, _programs[i], _programs[i]->getIndexInternalProperty(i2));
        }
    }
}

void ProgramActivator::pushInternalProperty(Pass *_pass, Program *_program, int _indexProperty)
{
    defaultProperties.pushInternalProperty(_pass, _program, _indexProperty);
}

const Property *ProgramActivator::getCorrectProperty(const Property *_localProperty)
{
    const Property *global = _localProperty->getGlobalSetting();
    if (global == NULL || _localProperty->getUseGlobalSetting() == false)
    {
        return _localProperty;
    }
    else
    {
        return global;
    }
}

void ProgramActivator::pushIntProperty(const Property *_property, Program *_program)
{
    GLuint program = _program->getProgram();
    GLint location = _program->getPropertyInProgram(_property->getCodeData());
    if (location != -1)
    {
        const IntProperty *prop = reinterpret_cast<const IntProperty *>(_property);
        glProgramUniform1i(program, location, prop->getValue());
    }
}

void ProgramActivator::pushFloatProperty(const Property *_property, Program *_program)
{
    GLuint program = _program->getProgram();
    GLint location = _program->getPropertyInProgram(_property->getCodeData());
    if (location != -1)
    {
        const FloatProperty *prop = reinterpret_cast<const FloatProperty *>(_property);
        glProgramUniform1f(program, location, prop->getValue());
    }
}

void ProgramActivator::pushToggleProperty(const Property *_property, Program *_program)
{
    return;
}

void ProgramActivator::pushVectorProperty(const Property *_property, Program *_program)
{
    GLuint program = _program->getProgram();
    GLint location = _program->getPropertyInProgram(_property->getCodeData());
    GLenum typeVector = _program->getTypeVector(_property->getCodeData());
    if (typeVector != GL_NONE)
    {
        const VectorProperty *prop = reinterpret_cast<const VectorProperty *>(_property);
        mth::vec4 vec = prop->getValue();
        if (typeVector == GL_FLOAT_VEC2)
        {
            glProgramUniform2f(program, location, vec.x, vec.y);
        }
        else if (typeVector == GL_FLOAT_VEC3)
        {
            glProgramUniform3f(program, location, vec.x, vec.y, vec.z);
        }
        else if (typeVector == GL_FLOAT_VEC4)
        {
            glProgramUniform4f(program, location, vec.x, vec.y, vec.z, vec.w);
        }
    }
}

void ProgramActivator::pushColorProperty(const Property *_property, Program *_program)
{
    GLuint program = _program->getProgram();
    GLint location = _program->getPropertyInProgram(_property->getCodeData());

    const ColorProperty *prop = reinterpret_cast<const ColorProperty *>(_property);
    mth::col col = prop->getValue();
    if (location != -1)
    {
        glProgramUniform4f(program, location, col.r, col.g, col.b, col.a);
    }
}

void ProgramActivator::pushTextureProperty(const Property *_property, Program *_program)
{
    const TextureProperty *prop = reinterpret_cast<const TextureProperty *>(_property);
    GLuint program = _program->getProgram();
    GLint numTexture = _program->getNumberSamplerTexture(_property->getCodeData());
    if (numTexture != -1 && numTexture <= GL_TEXTURE31)
    {
        glActiveTexture(GL_TEXTURE0 + numTexture);
        Texture *tex = prop->getValue();
        if (tex == NULL)
        {
            glBindTexture(GL_TEXTURE_2D, 0);
        }
        else
        {
            glBindTexture(GL_TEXTURE_2D, tex->getIndexTexture());
        }
    }
}

void ProgramActivator::pushCubeMapProperty(const Property *_property, Program *_program)
{
    const CubeMapProperty *prop = reinterpret_cast<const CubeMapProperty *>(_property);
    GLuint program = _program->getProgram();
    GLint numTexture = _program->getNumberSamplerTexture(_property->getCodeData());
    if (numTexture != -1 && numTexture <= GL_TEXTURE31)
    {
        CubeMap *cubeMap = prop->getValue();
        glActiveTexture(GL_TEXTURE0 + numTexture);
        if (cubeMap != NULL)
        {
            glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMap->getIndexCubeMap());
        }
        else
        {
            glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
        }
    }
}

void ProgramActivator::enableBufferForBaseColor()
{
    const LightRenderData *lightData = gprahicPipeline->getLightRenderData();

    glBindImageTexture(0, lightData->pointLightsAndDirectionLightForceBuffer, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA16F);
    glBindImageTexture(1, lightData->pointLightsNormalsBuffer, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA16F);
}

void ProgramActivator::enableUseUnifromBlocks(Program *_program)
{
    int count = _program->getCountUseUniformBlocks();
    const int *data = _program->getUseUniformBlocks();
    for (int i = 0; i < count; i++)
    {
        (*this.*functionsEnableUBO[data[i]])(data[i]);
    }
}

void ProgramActivator::enableUBOMatricesCamera(int _bindingIndex)
{
    // see draw queue
}

void ProgramActivator::enableUBOMatricesObject(int _bindingIndex)
{
    // see draw queue
}

void ProgramActivator::enableUBOSkyData(int _bindingIndex)
{
    const LightRenderData *lightData = gprahicPipeline->getLightRenderData();
    glBindBufferBase(GL_UNIFORM_BUFFER, _bindingIndex, lightData->skyDataUBO);
}

void ProgramActivator::enableUBOAllPointLight(int _bindingIndex)
{
    const LightRenderData *lightData = gprahicPipeline->getLightRenderData();
    glBindBufferBase(GL_UNIFORM_BUFFER, _bindingIndex, lightData->allPointLightsUBO);
}