#include "renderEngine/mainSystems/programActivator/internalPropertiesActivator.h"
#include "renderEngine/mainFrameBuffers/transparentFrameBuffer.h"

#include "renderEngine/mainSystems/programActivator/programActivator.h"
#include "renderEngine/graphicPipeline.h"
#include "renderEngine/mainSystems/mainFunctions/mainFunctions.h"

InternalPropertiesActivator::InternalPropertiesActivator()
{
    properties = {
        {"_Time", &InternalPropertiesActivator::pushTime, {}},
        // Buffers
        {"_environmentBuffer", &InternalPropertiesActivator::pushEnvironmentBuffer, //
         {RenderStage::RS_SHADOW_STAGE, RenderStage::RS_DEPTH_STAGE, RenderStage::RS_NORMAL_STAGE}},
        {"_depthBuffer", &InternalPropertiesActivator::pushDepthBuffer, //
         {RenderStage::RS_SHADOW_STAGE, RenderStage::RS_DEPTH_STAGE, RenderStage::RS_NORMAL_STAGE}},
        {"_normalBuffer", &InternalPropertiesActivator::pushNormalBuffer, //
         {RenderStage::RS_SHADOW_STAGE, RenderStage::RS_DEPTH_STAGE, RenderStage::RS_NORMAL_STAGE}},

        {"_viewSize", &InternalPropertiesActivator::pushViewSize, {}},
        {"_modelLight", &InternalPropertiesActivator::pushModelLight, //
         {RenderStage::RS_SHADOW_STAGE, RenderStage::RS_DEPTH_STAGE, RenderStage::RS_NORMAL_STAGE, RenderStage::RS_NORMAL_DEPTH_STAGE, RenderStage::RS_BASIC_COLOR_STAGE}},
    };
}

InternalPropertiesActivator::~InternalPropertiesActivator()
{
}

void InternalPropertiesActivator::initPropertiesRegistry(ProgramActivator *_programActivator, PropertiesRegistry *_registry)
{
    programActivator = _programActivator;
    _registry->clearProperties();

    settingFunctions.clear();
    int count = properties.size();
    for (int i = 0; i < count; i++)
    {
        const char *str = std::get<0>(properties[i]).c_str();
        _registry->addNewProperties(str, TypeProperty::Internal_P);
        settingFunctions.push_back(std::make_pair(std::get<1>(properties[i]), std::get<2>(properties[i])));
    }
}

void InternalPropertiesActivator::setGraphicPipeline(GraphicPipeline *_graphicPipeline)
{
    graphicPipeline = _graphicPipeline;
}

void InternalPropertiesActivator::setRenderStage(RenderStage _renderStage)
{
    renderStage = _renderStage;
}

RenderStage InternalPropertiesActivator::getRenderStage() const
{
    return renderStage;
}

void InternalPropertiesActivator::pushInternalProperty(Pass *_pass, Program *_program, int _indexProperty)
{
    if ((int)settingFunctions.size() <= _indexProperty)
    {
        return;
    }

    SettingFunctionDefine *settingFinction = &(settingFunctions.at(_indexProperty));
    int count = settingFinction->second.size();
    for (int i = 0; i < count; i++)
    {
        if (renderStage == settingFinction->second[i])
        {
            return;
        }
    }

    SettingFunction func = settingFinction->first;
    (this->*func)(_pass, _program, _indexProperty);
}

void InternalPropertiesActivator::pushTime(Pass *_pass, Program *_program, int _indexProperty)
{
    float time = programActivator->getMainFunctions()->getAllTime();
    glProgramUniform4f(_program->getProgram(), _program->getPropertyInProgram(_indexProperty), time / 20.0f, time, time / 2.0f, time / 3.0f);
}

void InternalPropertiesActivator::pushEnvironmentBuffer(Pass *_pass, Program *_program, int _indexProperty)
{
    GLint numTexture = _program->getNumberSamplerTexture(_indexProperty);
    glActiveTexture(GL_TEXTURE0 + numTexture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, graphicPipeline->getLightRenderData()->environmentBuffer);
}

void InternalPropertiesActivator::pushDepthBuffer(Pass *_pass, Program *_program, int _indexProperty)
{
    GLint numTexture = _program->getNumberSamplerTexture(_indexProperty);
    glActiveTexture(GL_TEXTURE0 + numTexture);
    glBindTexture(GL_TEXTURE_2D, graphicPipeline->getDepthBuffer());
}

void InternalPropertiesActivator::pushNormalBuffer(Pass *_pass, Program *_program, int _indexProperty)
{
    GLint numTexture = _program->getNumberSamplerTexture(_indexProperty);
    glActiveTexture(GL_TEXTURE0 + numTexture);
    glBindTexture(GL_TEXTURE_2D, graphicPipeline->getNormalBuffer());
}

void InternalPropertiesActivator::pushViewSize(Pass *_pass, Program *_program, int _indexProperty)
{
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    glProgramUniform2i(_program->getProgram(), _program->getPropertyInProgram(_indexProperty),
                       viewport[2], viewport[3]);
}

void InternalPropertiesActivator::pushModelLight(Pass *_pass, Program *_program, int _indexProperty)
{
    glProgramUniform1i(_program->getProgram(), _program->getPropertyInProgram(_indexProperty), MODEL_LIGHT_PASS(_pass));
}