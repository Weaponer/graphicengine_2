#include "changeChecker.h"

ChangeChecker::ChangeChecker()
{
    currectState = 0;
}

ChangeChecker::~ChangeChecker()
{
}

void ChangeChecker::nextStep()
{
    currectState++;
}

void ChangeChecker::synchronize(ChangeCheckerData *_changeChekerData) const
{
    _changeChekerData->lastEvent = currectState;
}

bool ChangeChecker::wasCheck(ChangeCheckerData *_changeChekerData)
{
    
    if(_changeChekerData->lastEvent == currectState)
    {
        return true;
    }
    else
    {
        _changeChekerData->lastEvent = currectState;
        return false;
    }
}