#include <random>
#include <MTH/bounds.h>
#include <MTH/boundSphere.h>
#include <MTH/boundAABB.h>

#include <MTH/color.h>

#include <iostream>

#include "gridController.h"

void checkGrid(GridData *_grid)
{
    int countActive = _grid->countActiveCells;
    int countNow = 0;
    for (int i = 0; i < _grid->countCells; i++)
    {
        CellData *cell = _grid->hashCells[i];
        if (cell->begin != NULL || cell->subGrid != NULL)
        {
            countNow++;
        }

        cell = _grid->additionalCells[i];
        if (cell->begin != NULL || cell->subGrid != NULL)
        {
            countNow++;
        }
    }

    std::cout << "Check grid: " << countNow << " | " << countActive << std::endl;
}

GridController::GridController(ThreadDebugOutput *_debugOutput) : debugOutput(_debugOutput)
{
    mainGrid = NULL;
    configGrid.countLayers = 3;
    configGrid.maxSizeCell = 64;
    configGrid.stepDived = 2;
    configGrid.scaleFactor = 1;
    setConfig(configGrid);
    return;
}

GridController::~GridController()
{
    if (mainGrid != NULL)
    {
        memoryManager.freeGrid(mainGrid);
        mainGrid = NULL;
    }
}

void GridController::setConfig(ConfigGrid _configGrid)
{
    configGrid = _configGrid;
    if (mainGrid != NULL)
    {
        memoryManager.freeGrid(mainGrid);
    }
    mainGrid = memoryManager.getGrid();
    mainGrid->mainCell = NULL;

    sizesCells.clear();
    sizesCells.push_back(configGrid.maxSizeCell);
    for (int i = 1; i < configGrid.countLayers; i++)
    {
        int value = sizesCells[i - 1] / configGrid.stepDived;
        sizesCells.push_back(value);
    }
}

void GridController::clear()
{
    setConfig(configGrid);
}

void GridController::addObjectSphereBound(ObjectData *_object, mth::vec3 _pos, float _radius)
{
    changeChecker.synchronize(&_object->changeCheckerData);
    int needLayer = getNeedLayer(_radius);
    int sizeCell = sizesCells[needLayer];
    bool allInside = false;

    mth::vec3 min = _pos - mth::vec3(_radius, _radius, _radius);
    mth::vec3 max = _pos + mth::vec3(_radius, _radius, _radius);
    HashPosition hashMin = getHash(needLayer, min);
    HashPosition hashMax = getHash(needLayer, max);

    if (_radius >= sizeCell && hashMax == hashMin)
    {
        allInside = true;
    }
    ObjectData object = *_object;
    object.allInside = allInside;
    object.position = _pos;
    object.boundType = BoundObjectType::SPHEARE_BOUND_OBJECT;
    object.boundData.x = _radius;
    object.layerGrid = needLayer;
    object.minBound = hashMin;
    object.maxBound = hashMax;
    *_object = object;

    if (allInside)
    {
        addObject(_object, needLayer, mth::vec3(hashMin.x, hashMin.y, hashMin.z));
    }
    else
    {
        int distCell = sizeCell * 2;
        mth::boundSphere sphere = mth::boundSphere(_pos, _radius);
        for (int i = hashMin.x; i <= hashMax.x; i += distCell)
        {
            for (int i2 = hashMin.y; i2 <= hashMax.y; i2 += distCell)
            {
                for (int i3 = hashMin.z; i3 <= hashMax.z; i3 += distCell)
                {
                    mth::boundAABB aabb = mth::boundAABB(mth::vec3(i, i2, i3), mth::vec3(sizeCell, sizeCell, sizeCell));
                    if (mth::test_AABB_Sphere(aabb, sphere))
                    {
                        addObject(_object, needLayer, mth::vec3(i, i2, i3));
                    }
                }
            }
        }
    }
}

void GridController::addObjectAABBBound(ObjectData *_object, mth::vec3 _pos, mth::vec3 _size)
{
    changeChecker.synchronize(&_object->changeCheckerData);
    float maxSize = std::max(_size.x, std::max(_size.y, _size.z));
    int needLayer = getNeedLayer(maxSize);
    int sizeCell = sizesCells[needLayer];
    bool allInside = false;

    mth::vec3 min = _pos - _size;
    mth::vec3 max = _pos + _size;
    HashPosition hashMin = getHash(needLayer, min);
    HashPosition hashMax = getHash(needLayer, max);
    // debugOutput->drawCube(_pos, _size, mth::vec3(1, 0, 0), 100);
    if (maxSize >= sizeCell && hashMax == hashMin)
    {
        allInside = true;
    }

    ObjectData object = *_object;
    object.allInside = allInside;
    object.position = _pos;
    object.boundType = BoundObjectType::AABB_BOUND_OBJECT;
    object.boundData = _size;
    object.layerGrid = needLayer;
    object.minBound = hashMin;
    object.maxBound = hashMax;
    *_object = object;

    if (allInside)
    {
        addObject(_object, needLayer, mth::vec3(hashMin.x, hashMin.y, hashMin.z));
    }
    else
    {
        int distCell = sizeCell * 2;
        for (int i = hashMin.x; i <= hashMax.x; i += distCell)
        {
            for (int i2 = hashMin.y; i2 <= hashMax.y; i2 += distCell)
            {
                for (int i3 = hashMin.z; i3 <= hashMax.z; i3 += distCell)
                {
                    // debugOutput->drawCube(mth::vec3(i, i2, i3), mth::vec3(sizeCell, sizeCell, sizeCell), mth::vec3(0, 1, 0), 100);
                    addObject(_object, needLayer, mth::vec3(i, i2, i3));
                }
            }
        }
    }
}

bool GridController::removeObject(ObjectData *_object)
{
    mth::vec3 posObject = _object->position;
    int layer = _object->layerGrid;

    if (_object->allInside)
    {
        return removeObject(_object, layer, posObject);
    }
    else
    {
        BoundObjectType boundType = _object->boundType;

        int sizeCell = sizesCells[layer];
        int distCell = sizeCell * 2;

        HashPosition hashMin = _object->minBound;
        HashPosition hashMax = _object->maxBound;
        bool anyRemove = false;
        if (boundType == BoundObjectType::SPHEARE_BOUND_OBJECT)
        {
            mth::boundSphere sphere = mth::boundSphere(posObject, _object->boundData.x);
            for (int i = hashMin.x; i <= hashMax.x; i += distCell)
            {
                for (int i2 = hashMin.y; i2 <= hashMax.y; i2 += distCell)
                {
                    for (int i3 = hashMin.z; i3 <= hashMax.z; i3 += distCell)
                    {
                        mth::boundAABB aabb = mth::boundAABB(mth::vec3(i, i2, i3), mth::vec3(sizeCell, sizeCell, sizeCell));
                        if (mth::test_AABB_Sphere(aabb, sphere))
                        {
                            anyRemove = anyRemove | removeObject(_object, layer, mth::vec3(i, i2, i3));
                        }
                    }
                }
            }
        }
        else
        {
            for (int i = hashMin.x; i <= hashMax.x; i += distCell)
            {
                for (int i2 = hashMin.y; i2 <= hashMax.y; i2 += distCell)
                {
                    for (int i3 = hashMin.z; i3 <= hashMax.z; i3 += distCell)
                    {
                        anyRemove = anyRemove | removeObject(_object, layer, mth::vec3(i, i2, i3));
                    }
                }
            }
        }
        return anyRemove;
    }
}

void GridController::moveResizeObject(ObjectData *_object, mth::vec3 _newPos, BoundObjectType _boundType, mth::vec3 _boundData)
{
    //ThreadDebugOutput::mainThreadDebugOutput->drawCube(_newPos, _boundData, mth::vec3(1, 0, 0), 120);
    if (_object->boundType == _boundType)
    {
        float maxSize = 0;
        mth::vec3 size;
        if (_boundType == BoundObjectType::AABB_BOUND_OBJECT)
        {
            size = _boundData;
            maxSize = std::max(_boundData.x, std::max(_boundData.y, _boundData.z));
        }
        else
        {
            size = mth::vec3(_boundData.x, _boundData.x, _boundData.x);
            maxSize = getNeedLayer(_boundData.x);
        }
        unsigned int needLayer = getNeedLayer(maxSize);

        mth::vec3 min = _newPos - size;
        mth::vec3 max = _newPos + size;
        HashPosition hashMin = getHash(needLayer, min);
        HashPosition hashMax = getHash(needLayer, max);

        if (hashMin == _object->minBound && hashMax == _object->maxBound && _object->layerGrid == needLayer)
        {
            _object->position = _newPos;
            _object->boundData = _boundData;
            return;
        }
    }

    removeObject(_object);
    if (_boundType == BoundObjectType::SPHEARE_BOUND_OBJECT)
    {
        addObjectSphereBound(_object, _newPos, _boundData.x);
    }
    else
    {
        addObjectAABBBound(_object, _newPos, _boundData);
    }
}

inline unsigned int GridController::getNeedLayer(float _size)
{
    unsigned int needLayer = 0;
    for (int i = configGrid.countLayers - 1; i >= 0; --i)
    {
        if (_size < sizesCells[i])
        {
            needLayer = i;
            break;
        }
    }
    return needLayer;
}

inline int GridController::getPos(float _v, int size)
{
    float doubleSize = size * 2;
    int side = 1;
    if (_v < 0)
    {
        side = -1;
    }
    _v = abs(_v);
    int v = std::max(0.0f, _v - size) / doubleSize;
    v = size + v * doubleSize;
    int v2 = v + doubleSize;
    if (_v - v < v2 - _v)
    {
        return v * side;
    }
    else
    {
        return v2 * side;
    }
}

inline HashPosition GridController::getHash(int _layer, mth::vec3 _position)
{
    HashPosition hash;
    const uint h1 = 0x8da6b343;
    const uint h2 = 0xd8163841;
    const uint h3 = 0xcb1ab31f;
    float size = sizesCells[_layer];
    hash.x = getPos(_position.x, size);
    hash.y = getPos(_position.y, size);
    hash.z = getPos(_position.z, size);
    hash.hash = abs((long long)(h1 * hash.x + h2 * hash.y + h3 * hash.z));
    return hash;
}

GridController::FindCellData GridController::findCell(int _layer, mth::vec3 _position)
{
    FindCellData findData;
    GridData *currentGrid = mainGrid;
    for (int i = 0; i <= _layer; i++)
    {
        HashPosition hash = getHash(i, _position);
        int index = hash.hash % currentGrid->countCells;
        CellData *needCell = currentGrid->hashCells[index];
        bool needHash = needCell->hash == hash;
        if ((i == _layer && checkCellIsFree(needCell)) || (!needHash && needCell->nextCell == NULL))
        {
            findData.hash = hash;
            findData.endCell = needCell;
            findData.endGrid = currentGrid;
            findData.endLayer = i;
            findData.result = NULL;
            return findData;
        }
        else
        {
            bool isNormal = false;
            if (needHash)
            {
                isNormal = true;
            }
            else
            {
                while (needCell->nextCell != NULL)
                {
                    needCell = needCell->nextCell;
                    if (needCell->hash == hash)
                    {
                        isNormal = true;
                        break;
                    }
                }
            }
            if (!isNormal)
            {
                findData.hash = hash;
                findData.endCell = needCell;
                findData.endGrid = currentGrid;
                findData.endLayer = i;
                findData.result = NULL;
                return findData;
            }
        }

        if (i == _layer)
        {
            findData.hash = hash;
            findData.result = needCell;
            findData.endCell = needCell;
            findData.endGrid = currentGrid;
            findData.endLayer = i;
            return findData;
        }
        else
        {
            GridData *subGrid = needCell->subGrid;
            if (subGrid != NULL)
            {
                currentGrid = subGrid;
            }
            else
            {
                findData.hash = hash;
                findData.endCell = needCell;
                findData.endGrid = currentGrid;
                findData.endLayer = i;
                findData.result = NULL;
                return findData;
            }
        }
    }
    findData.hash.hash = findData.hash.x = findData.hash.y = findData.hash.z = 0;
    findData.result = NULL;
    findData.endCell = NULL;
    findData.endGrid = NULL;
    findData.endLayer = 0;
    return findData;
}

inline CellData *GridController::checkCell(int _layer, mth::vec3 _position)
{
    return findCell(_layer, _position).result;
}

CellData *GridController::getOrCreateCell(int _layer, mth::vec3 _position)
{
    FindCellData findData = findCell(_layer, _position);
    if (findData.result != NULL)
    {
        return findData.result;
    }

    GridData *grid = findData.endGrid;
    CellData *endCell = NULL;
    if (checkCellIsFree(findData.endCell))
    {
        endCell = findData.endCell;
        endCell->hash = findData.hash;
        // debugOutput->drawCube(mth::vec3(findData.hash.x, findData.hash.y, findData.hash.z),
        //                       mth::vec3(sizesCells[findData.endLayer], sizesCells[findData.endLayer], sizesCells[findData.endLayer]), mth::vec3(1, 0, 0), 100);
    }
    else if (findData.endCell->hash == findData.hash)
    {
        endCell = findData.endCell;
        // debugOutput->drawCube(mth::vec3(findData.hash.x, findData.hash.y, findData.hash.z),
        //                       mth::vec3(sizesCells[findData.endLayer], sizesCells[findData.endLayer], sizesCells[findData.endLayer]), mth::vec3(1, 0, 0), 100);
    }
    else
    {
        endCell = findData.endCell;
        CellData *freeCell = NULL;
        while (true)
        {
            while (endCell->nextCell != NULL)
            {
                endCell = endCell->nextCell;
            }
            if (checkCellIsFree(endCell))
            {
                freeCell = endCell;
            }
            else
            {
                for (int i = 0; i < grid->countCells; i++)
                {
                    CellData *cellAdd = grid->additionalCells[i];
                    if (cellAdd->begin == NULL && cellAdd->subGrid == NULL)
                    {
                        freeCell = cellAdd;
                        if (freeCell->nextCell != NULL)
                        {
                            freeCell->nextCell->mainCell = freeCell->mainCell;
                            freeCell->mainCell->nextCell = freeCell->nextCell;
                            freeCell->nextCell = NULL;
                            freeCell->mainCell = NULL;
                        }
                        else if (freeCell->mainCell != NULL)
                        {
                            freeCell->mainCell->nextCell = NULL;
                        }
                        endCell->nextCell = freeCell;
                        freeCell->mainCell = endCell;
                        break;
                    }
                }
            }

            if (freeCell == NULL)
            {
                memoryManager.resizeGrid(grid, grid->countActiveCells * 2);
                freeCell = NULL;
                endCell = grid->hashCells[findData.hash.hash % grid->countCells];
                if (checkCellIsFree(endCell))
                {
                    freeCell = endCell;
                    break;
                }
                continue;
            }
            else
            {
                break;
            }
        }
        freeCell->hash = findData.hash;
        // debugOutput->drawCube(mth::vec3(findData.hash.x, findData.hash.y, findData.hash.z),
        //                       mth::vec3(sizesCells[findData.endLayer], sizesCells[findData.endLayer], sizesCells[findData.endLayer]), mth::vec3(1, 0, 0), 100);
        endCell = freeCell;
    }

    if (findData.endLayer == _layer)
    {
        return endCell;
    }
    else
    {
        CellData *newCell = NULL;
        for (int i = findData.endLayer; i < _layer; i++)
        {
            GridData *newGrid = memoryManager.getGrid();
            newGrid->mainCell = endCell;
            endCell->subGrid = newGrid;

            HashPosition subHash = getHash(i + 1, _position);
            int location = subHash.hash % newGrid->countCells;
            newCell = newGrid->hashCells[location];
            newCell->hash = subHash;
            endCell = newCell;

            // debugOutput->drawCube(mth::vec3(subHash.x, subHash.y, subHash.z),
            // mth::vec3(sizesCells[i + 1], sizesCells[i + 1], sizesCells[i + 1]), mth::vec3(1, 0, 0), 100);
        }
        return newCell;
    }
}

bool GridController::checkCellIsFree(CellData *_cell)
{
    if (_cell->begin != NULL || _cell->subGrid != NULL || _cell->nextCell != NULL)
    {
        return false;
    }
    else
    {
        return true;
    }
}

void GridController::optimizeCell(CellData *_cell)
{
    GridData *subGrid = _cell->subGrid;

    if (subGrid != NULL)
    {
        _cell->subGrid = NULL;
        memoryManager.freeGrid(subGrid);
    }
    GridData *mainGrid = _cell->mainGrid;
    CellData *mainCell = mainGrid->mainCell;
    if (mainCell != NULL && mainCell->activeSubGrid == false && mainCell->begin == NULL)
    {
        optimizeCell(mainGrid->mainCell);
    }
}

void GridController::addObject(ObjectData *_object, int _layer, mth::vec3 _posCell)
{
    CellData *cell = getOrCreateCell(_layer, _posCell);

    bool wasNoneActive = !cell->activeSubGrid && cell->begin == NULL;

    addObjectToCell(cell, _object);
    if (wasNoneActive)
    {
        cellNowActive(cell);
    }
}

bool GridController::removeObject(ObjectData *_object, int _layer, mth::vec3 _posCell)
{
    FindCellData findData = findCell(_layer, _posCell);
    CellData *cell = findData.result;
    if (cell == NULL)
    {
        std::cout << "dont remove bacouse cell not found" << std::endl;
        return false;
    }
    bool wasActive = cell->activeSubGrid || cell->begin != NULL;
    bool result = removeObjectFromCell(cell, _object);
    if (result)
    {
        bool nowActive = cell->activeSubGrid || cell->begin != NULL;
        if (wasActive && !nowActive)
        {
            cellNowNoneActive(cell);
            optimizeCell(cell);
        }
    }
    if (!result)
    {
        std::cout << "dont remove" << std::endl;
    }
    return result;
}

void GridController::addObjectToCell(CellData *_cell, ObjectData *_object)
{
    if (_cell->beginFreeIterators == NULL)
    {
        memoryManager.addFreeIterationsCell(_cell, 1);
    }

    Iterator *iter = _cell->beginFreeIterators;
    _cell->beginFreeIterators = iter->next;

    iter->next = _cell->begin;
    _cell->begin = iter;

    iter->data = _object;
}

bool GridController::removeObjectFromCell(CellData *_cell, ObjectData *_object)
{
    Iterator *iter = _cell->begin;
    if (iter == NULL)
    {
        return false;
    }
    if (iter->data == _object)
    {
        _cell->begin = iter->next;
        iter->data = NULL;
        iter->next = _cell->beginFreeIterators;
        _cell->beginFreeIterators = iter;
        return true;
    }

    while (iter->next != NULL)
    {
        if (iter->next->data == _object)
        {
            Iterator *needIter = iter->next;
            iter->next = needIter->next;
            needIter->data = NULL;
            needIter->next = _cell->beginFreeIterators;
            _cell->beginFreeIterators = needIter;
            return true;
        }
        iter = iter->next;
    }
    return false;
}

void GridController::cellNowActive(CellData *_cell)
{
    GridData *grid = _cell->mainGrid;
    int oldCount = grid->countActiveCells;
    grid->countActiveCells++;
    if (oldCount == 0)
    {
        CellData *upCell = grid->mainCell;
        if (upCell != NULL)
        {
            upCell->activeSubGrid = true;
            if (upCell->begin == NULL)
            {
                cellNowActive(upCell);
            }
        }
    }
}

void GridController::cellNowNoneActive(CellData *_cell)
{
    GridData *grid = _cell->mainGrid;
    grid->countActiveCells--;

    CellData *upCell = grid->mainCell;
    if (grid->countActiveCells == 0 && upCell != NULL)
    {
        upCell->activeSubGrid = false;
        if (upCell->begin == NULL)
        {
            cellNowNoneActive(upCell);
        }
    }
}

unsigned int GridController::getObjectsInArea(SearchAreaData _searchAreaData, unsigned int _maxCount, ObjectData **_objects, bool _includeNoneActive)
{
    changeChecker.nextStep();
    unsigned int countObjects = 0;

    mth::vec3 min;
    mth::vec3 max;
    if (_searchAreaData.searchAreaType == SearchAreaType::AABB_SEARCH_BOUND)
    {
        min = _searchAreaData.bound.aabb.min;
        max = _searchAreaData.bound.aabb.max;
    }
    else if (_searchAreaData.searchAreaType == SearchAreaType::OBB_SEARCH_BOUND)
    {
        mth::vec3 localSize = _searchAreaData.bound.obb.size;
        mth::vec3 xDir = _searchAreaData.bound.obb.rotate.transform(mth::vec3(localSize.x, localSize.y, localSize.z));
        mth::vec3 yDir = _searchAreaData.bound.obb.rotate.transform(mth::vec3(localSize.x, -localSize.y, localSize.z));
        mth::vec3 zDir = _searchAreaData.bound.obb.rotate.transform(mth::vec3(localSize.x, localSize.y, -localSize.z));
        mth::vec3 wDir = _searchAreaData.bound.obb.rotate.transform(mth::vec3(-localSize.x, localSize.y, localSize.z));
        mth::vec3 halfSize = mth::vec3(std::max(abs(xDir.x), std::max(abs(yDir.x), std::max(abs(zDir.x), abs(wDir.x)))),
                                       std::max(abs(xDir.y), std::max(abs(yDir.y), std::max(abs(zDir.y), abs(wDir.y)))),
                                       std::max(abs(xDir.z), std::max(abs(yDir.z), std::max(abs(zDir.z), abs(wDir.z)))));
        min = _searchAreaData.bound.obb.center - halfSize;
        max = _searchAreaData.bound.obb.center + halfSize;
    }
    else
    {
        float radius = _searchAreaData.bound.sphere.radius;
        min = _searchAreaData.bound.sphere.origin - mth::vec3(radius, radius, radius);
        max = _searchAreaData.bound.sphere.origin + mth::vec3(radius, radius, radius);
    }
    HashPosition hashMin = getHash(0, min);
    HashPosition hashMax = getHash(0, max);
    int sizeCell = sizesCells[0];
    int distCell = sizeCell * 2;
    // std::cout << "HashMin: " << min.x << " " << min.y << " " << min.z << std::endl;
    // std::cout << "hashMax: " << max.x << " " << max.y << " " << max.z << std::endl;
    for (int i = hashMin.x; i <= hashMax.x; i += distCell)
    {
        for (int i2 = hashMin.y; i2 <= hashMax.y; i2 += distCell)
        {
            for (int i3 = hashMin.z; i3 <= hashMax.z; i3 += distCell)
            {
                mth::vec3 posCell = mth::vec3(i, i2, i3);
                CellData *cell = checkCell(0, posCell);
                if (cell == NULL || (cell->begin == NULL && (cell->subGrid == NULL || cell->activeSubGrid == false)))
                {
                    // debugOutput->drawCube(posCell, mth::vec3(sizeCell, sizeCell, sizeCell), mth::vec3(1, 0, 0), 0);
                    continue;
                }

                mth::boundAABB aabbCell = mth::boundAABB(posCell, mth::vec3(sizeCell, sizeCell, sizeCell));
                int contactResult = aabbInsideSearchArea(_searchAreaData, aabbCell);
                bool onlyIntersection = false;
                bool allInside = false;
                if (contactResult >= 1)
                {
                    onlyIntersection = true;
                    if (contactResult > 1)
                    {
                        allInside = true;
                    }
                }
                if (allInside)
                {
                    // debugOutput->drawCube(posCell, mth::vec3(sizeCell, sizeCell, sizeCell), mth::vec3(0, 1, 0), 0);
                }
                else
                {
                    // debugOutput->drawCube(posCell, mth::vec3(sizeCell, sizeCell, sizeCell), mth::vec3(0, 0, 1), 0);
                }

                if (!onlyIntersection)
                {
                    continue;
                }

                if (checkCellAndPush(_searchAreaData, _maxCount, _objects, countObjects, cell, allInside, _includeNoneActive))
                {
                    return countObjects;
                }

                if (cell->activeSubGrid)
                {
                    if (getObjectsInCells(_searchAreaData, _maxCount, _objects, countObjects, cell, 1, allInside, _includeNoneActive))
                    {
                        return countObjects;
                    }
                }
            }
        }
    }
    return countObjects;
}

bool GridController::getObjectsInCells(SearchAreaData &_searchAreaData, unsigned int _maxCount, ObjectData **_objects, unsigned int &_currentCount,
                                       CellData *_cell, unsigned int _layer, bool _allInside, bool _includeNoneActive)
{
    GridData *grid = _cell->subGrid;
    int countActiveCells = grid->countActiveCells;
    int countCells = grid->countCells;
    int countFound = 0;
    for (int i = 0; i < countCells * 2; i++)
    {
        int index = i;
        CellData **cells = grid->hashCells;
        if (i >= countCells)
        {
            cells = grid->additionalCells;
            index -= countCells;
        }

        CellData *subCell = cells[index];

        if (subCell->begin == NULL && subCell->subGrid == NULL)
        {
            continue;
        }

        countFound++;
        bool subContact = false;
        bool subAllInside = false;
        float sizeCell = sizesCells[_layer];
        if (_allInside)
        {
            subAllInside = true;
            subContact = true;
        }
        else
        {
            HashPosition hash = subCell->hash;
            mth::vec3 positionCell = mth::vec3(hash.x, hash.y, hash.z);
            mth::boundAABB aabb = mth::boundAABB(positionCell, mth::vec3(sizeCell, sizeCell, sizeCell));

            int resCheck = aabbInsideSearchArea(_searchAreaData, aabb);

            if (resCheck >= 1)
            {
                subContact = true;
                if (resCheck >= 2)
                {
                    subAllInside = true;
                }
            }
        }

        if (subAllInside)
        {
            // debugOutput->drawCube(mth::vec3(subCell->hash.x, subCell->hash.y, subCell->hash.z),
            // mth::vec3(sizeCell, sizeCell, sizeCell), mth::vec3(0, 1, 0), 0);
        }
        else if (subContact)
        {
            // debugOutput->drawCube(mth::vec3(subCell->hash.x, subCell->hash.y, subCell->hash.z),
            // mth::vec3(sizeCell, sizeCell, sizeCell), mth::vec3(0, 0, 1), 0);
        }
        if (!subContact)
        {
            // debugOutput->drawCube(mth::vec3(subCell->hash.x, subCell->hash.y, subCell->hash.z),
            // mth::vec3(sizeCell, sizeCell, sizeCell), mth::vec3(1, 0, 0), 0);
            continue;
        }
        if (checkCellAndPush(_searchAreaData, _maxCount, _objects, _currentCount, subCell, subAllInside, _includeNoneActive))
        {
            return true;
        }

        if (subCell->activeSubGrid)
        {
            if (getObjectsInCells(_searchAreaData, _maxCount, _objects, _currentCount, subCell, _layer + 1, subAllInside, _includeNoneActive))
            {
                return true;
            }
        }

        if (countFound == countActiveCells)
        {
            return false;
        }
    }
    return false;
}

inline int GridController::aabbInsideSearchArea(SearchAreaData &_searchAreaData, mth::boundAABB &_aabb)
{
    int result = 0;
    switch (_searchAreaData.searchAreaType)
    {
    case SearchAreaType::AABB_SEARCH_BOUND:
    {
        if (mth::test_AABB_AABB(_searchAreaData.bound.aabb, _aabb))
        {
            result += 1;
            if (_searchAreaData.bound.aabb.max >= _aabb.max && _searchAreaData.bound.aabb.min <= _aabb.min)
            {
                result++;
            }

            if (result == 2)
            {
                for (int i = 0; i < _searchAreaData.countClippingPlanes; i++)
                {
                    mth::plane plane = _searchAreaData.clippingPlanes[i];
                    plane.normal.invert();
                    if (mth::test_AABB_Plane(_aabb, plane))
                    {
                        result = 1;
                        break;
                    }
                }
            }
        }
        return result;
    }
    case SearchAreaType::OBB_SEARCH_BOUND:
    {
        mth::boundOBB obb = mth::boundOBB(_aabb.origin, _aabb.size);
        if (mth::test_OBB_OBB(obb, _searchAreaData.bound.obb))
        {
            result++;
            bool allInside = true;
            for (int i = 1; i >= -1; i -= 2)
            {
                for (int i2 = 1; i2 >= -1; i2 -= 2)
                {
                    for (int i3 = 1; i3 >= -1; i3 -= 2)
                    {
                        mth::vec3 point = _aabb.origin + mth::vec3(_aabb.size.x * i, _aabb.size.y * i2, _aabb.size.z * i3);
                        if (!_searchAreaData.bound.obb.checkPointInside(point))
                        {
                            allInside = false;
                            goto END_CHECK_INSIDE_OBB;
                        }

                        for (int i = 0; i < _searchAreaData.countClippingPlanes; i++)
                        {
                            mth::plane plane = _searchAreaData.clippingPlanes[i];
                            plane.normal.invert();
                            if (plane.checkPointOutside(point))
                            {
                                allInside = false;
                                goto END_CHECK_INSIDE_OBB;
                            }
                        }
                    }
                }
            }
        END_CHECK_INSIDE_OBB:
            if (allInside)
            {
                result++;
            }
        }
        return result;
    }
    case SearchAreaType::SPHERE_SEARCH_BOUND:
    {
        if (mth::test_AABB_Sphere(_aabb, _searchAreaData.bound.sphere))
        {
            result++;
            bool allInside = true;
            for (int i = 1; i >= -1; i -= 2)
            {
                for (int i2 = 1; i2 >= -1; i2 -= 2)
                {
                    for (int i3 = 1; i3 >= -1; i3 -= 2)
                    {
                        mth::vec3 point = _aabb.origin + mth::vec3(_aabb.size.x * i, _aabb.size.y * i2, _aabb.size.z * i3);
                        if (!_searchAreaData.bound.sphere.checkPointInside(point))
                        {
                            allInside = false;
                            goto END_CHECK_INSIDE_SPHERE;
                        }

                        for (int i = 0; i < _searchAreaData.countClippingPlanes; i++)
                        {
                            mth::plane plane = _searchAreaData.clippingPlanes[i];
                            plane.normal.invert();
                            if (plane.checkPointOutside(point))
                            {
                                allInside = false;
                                goto END_CHECK_INSIDE_SPHERE;
                            }
                        }
                    }
                }
            }
        END_CHECK_INSIDE_SPHERE:
            if (allInside)
            {
                result++;
            }
        }
        return result;
    }
    }
    return result;
}

inline bool GridController::checkCellAndPush(SearchAreaData &_searchAreaData, unsigned int _maxCount, ObjectData **_objects,
                                             unsigned int &_currentCount, CellData *_cell, bool _allInside, bool _includeNoneActive)
{
    if (_cell->begin == NULL)
    {
        return false;
    }

    Iterator *iter = _cell->begin;
    while (iter != NULL)
    {
        ObjectData *obj = iter->data;
        if (changeChecker.wasCheck(&obj->changeCheckerData) || (_searchAreaData.maskTypeObjects & obj->maskObjectType) == 0 || !(!_includeNoneActive || obj->active))
        {
            iter = iter->next;
            continue;
        }
        if (!_allInside)
        {
            mth::vec3 objPos = obj->position;
            mth::vec3 bounData = obj->boundData;
            bool contact = false;
            SearchAreaType typeSearch = _searchAreaData.searchAreaType;

            if (obj->boundType == BoundObjectType::AABB_BOUND_OBJECT)
            {
                mth::boundAABB aabb = mth::boundAABB(objPos, bounData);

                switch (typeSearch)
                {
                case SearchAreaType::AABB_SEARCH_BOUND:
                {
                    if (mth::test_AABB_AABB(aabb, _searchAreaData.bound.aabb))
                    {
                        contact = true;
                    }
                    break;
                }
                case SearchAreaType::OBB_SEARCH_BOUND:
                {
                    mth::boundOBB obb = mth::boundOBB(aabb.origin, aabb.size);
                    if (mth::test_OBB_OBB(obb, _searchAreaData.bound.obb))
                    {
                        contact = true;
                    }
                    break;
                }
                default:
                {
                    if (mth::test_AABB_Sphere(aabb, _searchAreaData.bound.sphere))
                    {
                        contact = true;
                    }
                    break;
                }
                }

                if (contact)
                {
                    for (int i = 0; i < _searchAreaData.countClippingPlanes; i++)
                    {
                        mth::plane plane = _searchAreaData.clippingPlanes[i];
                        if (!mth::test_AABB_Plane(aabb, plane))
                        {
                            contact = false;
                            break;
                        }
                    }
                }
            }
            else
            {
                mth::boundSphere sphere = mth::boundSphere(objPos, bounData.x);

                switch (typeSearch)
                {
                case SearchAreaType::AABB_SEARCH_BOUND:
                {
                    if (mth::test_AABB_Sphere(_searchAreaData.bound.aabb, sphere))
                    {
                        contact = true;
                    }
                    break;
                }
                case SearchAreaType::OBB_SEARCH_BOUND:
                {
                    if (mth::test_OBB_Sphere(_searchAreaData.bound.obb, sphere))
                    {
                        contact = true;
                    }
                    break;
                }
                default:
                {
                    if (mth::test_Sphere_Sphere(sphere, _searchAreaData.bound.sphere))
                    {
                        contact = true;
                    }
                    break;
                }
                }

                if (contact)
                {
                    for (int i = 0; i < _searchAreaData.countClippingPlanes; i++)
                    {
                        mth::plane plane = _searchAreaData.clippingPlanes[i];
                        if (!mth::test_Sphere_Plane(sphere, plane))
                        {
                            contact = false;
                            break;
                        }
                    }
                }
            }

            if (!contact)
            {
                iter = iter->next;
                continue;
            }
        }
        _objects[_currentCount] = iter->data;
        _currentCount++;
        if (_currentCount == _maxCount)
        {
            return true;
        }
        iter = iter->next;
    }
    return false;
}