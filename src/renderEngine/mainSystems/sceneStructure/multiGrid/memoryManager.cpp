#include "memoryManager.h"

MemoryManager::MemoryManager()
{
    _freeIterators = NULL;
    freeCells = NULL;
}

MemoryManager::~MemoryManager()
{
    while (!_freeGrids.empty())
    {
        GridData *grid = _freeGrids.top();
        _freeGrids.pop();
        delete grid;
    }

    while (_freeIterators != NULL)
    {
        Iterator *iter = _freeIterators;
        _freeIterators = _freeIterators->next;
        delete iter;
    }

    while (freeCells != NULL)
    {
        CellData *cell = freeCells;
        freeCells = freeCells->nextCell;
        delete cell;
    }

    while (!_freeCellArrays.empty())
    {
        std::pair<unsigned int, std::pair<CellData **, CellData **>> data = _freeCellArrays.top();
        delete[] data.second.first;
        delete[] data.second.second;
        _freeCellArrays.pop();
    }

}

GridData *MemoryManager::getGrid()
{
    GridData *grid = NULL;
    if (!_freeGrids.empty())
    {
        grid = _freeGrids.top();
        _freeGrids.pop();
    }
    else
    {
        grid = new GridData();
    }

    if (!_freeCellArrays.empty())
    {
        std::pair<unsigned int, std::pair<CellData **, CellData **>> data = _freeCellArrays.top();
        _freeCellArrays.pop();

        grid->countCells = data.first;
        grid->hashCells = data.second.first;
        grid->additionalCells = data.second.second;
    }
    else
    {
        grid->countCells = 1;
        grid->hashCells = new CellData *[1];
        grid->additionalCells = new CellData *[1];
    }

    for (int i = 0; i < grid->countCells; i++)
    {
        CellData *cell = getFreeCell();
        cell->mainGrid = grid;
        grid->hashCells[i] = cell;
        cell = getFreeCell();
        cell->mainGrid = grid;
        grid->additionalCells[i] = cell;
    }

    grid->mainCell = NULL;
    grid->countActiveCells = 0;

    return grid;
}

void MemoryManager::freeGrid(GridData *_grid)
{
    for (int i = 0; i < _grid->countCells; i++)
    {
        CellData *cell = _grid->hashCells[i];
        clearCell(cell);
        returnCell(cell);
        cell = _grid->additionalCells[i];
        clearCell(cell);
        returnCell(cell);
    }
    _freeCellArrays.push(std::make_pair(_grid->countCells, std::make_pair(_grid->hashCells, _grid->additionalCells)));

    GridData grid;
    grid.mainCell = NULL;
    grid.countActiveCells = 0;
    grid.countCells = 0;
    grid.hashCells = NULL;
    grid.additionalCells = NULL;
    *_grid = grid;
    _freeGrids.push(_grid);
}

void MemoryManager::resizeGrid(GridData *_grid, unsigned int _minimumNewSize)
{
    int oldSize = _grid->countCells;
    CellData **oldHashCells = _grid->hashCells;
    CellData **oldAdditionalCells = _grid->additionalCells;
    CellData **newHashCells = NULL;
    CellData **newAdditionalCells = NULL;

    if (!_freeCellArrays.empty() && _freeCellArrays.top().first >= _minimumNewSize)
    {
        std::pair<unsigned int, std::pair<CellData **, CellData **>> data = _freeCellArrays.top();
        _freeCellArrays.pop();
        _minimumNewSize = data.first;
        newHashCells = data.second.first;
        newAdditionalCells = data.second.second;
    }
    else
    {
        newHashCells = new CellData *[_minimumNewSize];
        newAdditionalCells = new CellData *[_minimumNewSize];
    }

    //std::cout << "Resize: " << _minimumNewSize << std::endl;

    for (unsigned int i = 0; i < _minimumNewSize; i++)
    {
        CellData *cell = getFreeCell();
        cell->mainGrid = _grid;
        newHashCells[i] = cell;
        cell = getFreeCell();
        cell->mainGrid = _grid;
        newAdditionalCells[i] = cell;
    }

    for (int i = 0; i < oldSize * 2; i++)
    {
        int index = i;
        CellData **cells = oldHashCells;
        if (i >= oldSize)
        {
            cells = oldAdditionalCells;
            index -= oldSize;
        }

        CellData *cell = cells[index];

        if (cell->begin != NULL || cell->subGrid != NULL)
        {
            HashPosition hash = cell->hash;
            int newPos = hash.hash % _minimumNewSize;

            CellData *newCell = newHashCells[newPos];
            if (newCell->begin != NULL || newCell->subGrid != NULL || newCell->nextCell != NULL)
            {
                while (newCell->nextCell != NULL)
                {
                    newCell = newCell->nextCell;
                }
                if (newCell->begin != NULL || newCell->subGrid != NULL)
                {
                    CellData *freeCell = NULL;

                    for (unsigned int i2 = 0; i2 < _minimumNewSize; i2++)
                    {
                        if (newAdditionalCells[i2]->begin == NULL && newAdditionalCells[i2]->subGrid == NULL && newAdditionalCells[i2]->nextCell == NULL)
                        {
                            freeCell = newAdditionalCells[i2];
                            break;
                        }
                    }

                    newCell->nextCell = freeCell;
                    freeCell->mainCell = newCell;

                    newCell = freeCell;
                }
            }

            newCell->activeSubGrid = cell->activeSubGrid;
            cell->activeSubGrid = false;
            newCell->subGrid = cell->subGrid;
            if (newCell->subGrid != NULL)
            {
                newCell->subGrid->mainCell = newCell;
            }
            cell->subGrid = NULL;
            newCell->begin = cell->begin;
            cell->begin = NULL;
            newCell->mainGrid = cell->mainGrid;
            cell->mainGrid = NULL;
            newCell->hash = hash;
        }
        clearCell(cell);
    }
    _grid->countCells = _minimumNewSize;
    _grid->hashCells = newHashCells;
    _grid->additionalCells = newAdditionalCells;
    for(int i = 0; i < oldSize; i++)
    {
        CellData *cell = oldHashCells[i];
        fillDefaultCell(cell);
        returnCell(cell);
        cell = oldAdditionalCells[i];
        fillDefaultCell(cell);
        returnCell(cell);
    }
    if(oldSize <= 10)
    {
        _freeCellArrays.push(std::make_pair(oldSize, std::make_pair(oldHashCells, oldAdditionalCells)));
    }
    else
    {
        delete []oldHashCells;
        delete []oldAdditionalCells;
    }
}

void MemoryManager::addFreeIterationsCell(CellData *_cell, int _countIterations)
{
    if (_cell->beginFreeIterators == NULL)
    {
        _cell->beginFreeIterators = getFreeIterator();
        _countIterations--;
    }

    for (int i = 0; i < _countIterations; i++)
    {
        Iterator *iter = getFreeIterator();
        iter->next = _cell->beginFreeIterators;
        _cell->beginFreeIterators = iter;
    }
}

CellData *MemoryManager::getFreeCell()
{
    if (freeCells == NULL)
    {
        CellData *cell = new CellData();
        fillDefaultCell(cell);
        return cell;
    }
    else
    {
        CellData *cell = freeCells;
        freeCells = cell->nextCell;
        cell->nextCell = NULL;
        return cell;
    }
}

void MemoryManager::returnCell(CellData *_cell)
{
    _cell->nextCell = freeCells;
    freeCells = _cell;
}

void MemoryManager::fillDefaultCell(CellData *_cell)
{
    CellData cell;
    cell.activeSubGrid = false;
    cell.begin = NULL;
    cell.beginFreeIterators = NULL;
    cell.hash.hash = 0;
    cell.hash.x = 0;
    cell.hash.y = 0;
    cell.hash.z = 0;
    cell.mainCell = NULL;
    cell.subGrid = NULL;
    cell.mainGrid = NULL;
    cell.nextCell = NULL;
    *_cell = cell;
}

void MemoryManager::clearCell(CellData *_cell)
{
    if (_cell->subGrid != NULL)
    {
        freeGrid(_cell->subGrid);
        _cell->subGrid = NULL;
    }
    while (_cell->begin != NULL)
    {
        Iterator *iter = _cell->begin;
        _cell->begin = _cell->begin->next;
        returnIterator(iter);
    }
    while (_cell->beginFreeIterators != NULL)
    {
        Iterator *iter = _cell->beginFreeIterators;
        _cell->beginFreeIterators = _cell->beginFreeIterators->next;
        returnIterator(iter);
    }
    fillDefaultCell(_cell);
}

void MemoryManager::returnIterator(Iterator *_iterator)
{
    _iterator->next = NULL;
    _iterator->data = NULL;
    if (_freeIterators == NULL)
    {
        _freeIterators = _iterator;
    }
    else
    {
        Iterator *iter = _freeIterators;
        _freeIterators = _iterator;
        _iterator->next = iter;
    }
}

Iterator *MemoryManager::getFreeIterator()
{
    if (_freeIterators == NULL)
    {
        Iterator *iter = new Iterator();
        iter->data = NULL;
        iter->next = NULL;
        return iter;
    }
    else
    {
        Iterator *iter = _freeIterators;
        _freeIterators = iter->next;
        iter->next = NULL;
        return iter;
    }
}