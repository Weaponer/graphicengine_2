#include "renderEngine/mainSystems/sceneStructure/sceneStructure.h"

SceneStructure::SceneStructure(GraphicConfig *_graphicConfig) : sceneGrid(_graphicConfig)
{
}

SceneStructure::~SceneStructure()
{
}

void SceneStructure::addEntityGE(TypeGraphicEntity _type, Entity *_entity)
{
    entityRegister._addEntityGE(_type, _entity);
    if(sceneGrid.checkTypeEntityHaveGridLocation(_type))
    {
        sceneGrid.addEntity(_entity);
    }
}

void SceneStructure::removeEntityGE(Entity *_entity)
{
    entityRegister._removeEntityGE(_entity);
    if(sceneGrid.checkTypeEntityHaveGridLocation(_entity->typeEntity))
    {
        sceneGrid.removeEntity(_entity);
    }
}

void SceneStructure::changeEntityOrientation(Entity *_entity)
{
    sceneGrid.changeOrientation(_entity);
}

EntityRegister *SceneStructure::getEntityRegister()
{
    return &entityRegister;
}

SceneGrid *SceneStructure::getSceneGrid()
{
    return &sceneGrid;
}