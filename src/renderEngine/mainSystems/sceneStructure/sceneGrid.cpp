#include "renderEngine/mainSystems/sceneStructure/sceneGrid.h"

SceneGrid::SceneGrid(GraphicConfig *_graphicConfig) : gridController(ThreadDebugOutput::mainThreadDebugOutput)
{
    ConfigGrid configGrid;
    configGrid.countLayers = _graphicConfig->getGraphicInternalTable().toInt("GridCountLayers");
    configGrid.maxSizeCell = _graphicConfig->getGraphicInternalTable().toInt("GridMaxSizeCell");
    configGrid.scaleFactor = 1;
    configGrid.stepDived = _graphicConfig->getGraphicInternalTable().toInt("GridStepDived");
    gridController.setConfig(configGrid);
}

SceneGrid::~SceneGrid()
{
}

bool SceneGrid::checkTypeEntityHaveGridLocation(TypeGraphicEntity _type) const
{
    return _type == TypeGraphicEntity::MESH_RENDER_GRAPHIC_ENTITY ||
           _type == TypeGraphicEntity::POINT_LIGHT_GRAPHIC_ENTITY ||
           _type == TypeGraphicEntity::DENSITY_VOLUME_GRAPHIC_ENTITY;
}

void SceneGrid::addEntity(Entity *_entity)
{
    ObjectData *_objectDataRef = NULL;

    OrientationData orientationData = getOrientationData(_entity, &_objectDataRef);

    _objectDataRef->maskObjectType = _entity->typeEntity;
    _objectDataRef->mainObject = _entity;

    if (orientationData.boundType == BoundObjectType::AABB_BOUND_OBJECT)
    {
        gridController.addObjectAABBBound(_objectDataRef, orientationData.position, orientationData.boundData);
    }
    else
    {
        gridController.addObjectSphereBound(_objectDataRef, orientationData.position, orientationData.boundData.x);
    }
}

void SceneGrid::removeEntity(Entity *_entity)
{
    ObjectData *_objectDataRef = NULL;
    getOrientationData(_entity, &_objectDataRef);
    gridController.removeObject(_objectDataRef);
}

void SceneGrid::changeOrientation(Entity *_entity)
{
    ObjectData *_objectDataRef = NULL;
    OrientationData orientationData = getOrientationData(_entity, &_objectDataRef);

    gridController.moveResizeObject(_objectDataRef, orientationData.position, orientationData.boundType, orientationData.boundData);
}

unsigned int SceneGrid::getObjectsInSearchArea(SearchAreaData _searchAreaData, unsigned int _maxCount, ObjectData **_objects)
{
    return gridController.getObjectsInArea(_searchAreaData, _maxCount, _objects);
}

SceneGrid::OrientationData SceneGrid::getOrientationData(Entity *_entity, ObjectData **_objectDataRef)
{
    switch (_entity->typeEntity)
    {
    case TypeGraphicEntity::POINT_LIGHT_GRAPHIC_ENTITY:
    {
        PointLightGE *entity = reinterpret_cast<PointLightGE *>(_entity);
        *_objectDataRef = &entity->objectGridData;

        OrientationData data;
        data.boundType = BoundObjectType::SPHEARE_BOUND_OBJECT;
        data.boundData.x = entity->range;
        data.position = entity->position;
        return data;
    }
    case TypeGraphicEntity::MESH_RENDER_GRAPHIC_ENTITY:
    {
        MeshRenderGE *entity = reinterpret_cast<MeshRenderGE *>(_entity);
        *_objectDataRef = &entity->objectGridData;

        OrientationData data;
        data.boundType = BoundObjectType::AABB_BOUND_OBJECT;
        mth::boundAABB bound = entity->bound;
        data.boundData = bound.size;
        data.position = bound.origin;
        return data;
        break;
    }
    case TypeGraphicEntity::DENSITY_VOLUME_GRAPHIC_ENTITY:
    {
        DensityVolumeGE *entity = reinterpret_cast<DensityVolumeGE *>(_entity);
        *_objectDataRef = &entity->objectGridData;

        OrientationData data;
        data.boundType = BoundObjectType::AABB_BOUND_OBJECT;
        mth::boundAABB bound = entity->bound;
        data.boundData = bound.size;
        data.position = bound.origin;
        return data;
        break;
    }
    }
    return OrientationData();
}