#include "renderEngine/mainSystems/sceneStructure/sceneStructure.h"

EntityRegister::EntityRegister()
{
}

EntityRegister::~EntityRegister()
{
}

void EntityRegister::_addEntityGE(TypeGraphicEntity _type, Entity *_entity)
{
    switch (_type)
    {
    case TypeGraphicEntity::CAMERA_GRAPHIC_ENTITY:
        cameras.push_back((CameraGE *)_entity);
        return;
    case TypeGraphicEntity::DEBUG_RENDER_GRAPHIC_ENTITY:
        debugRenders.push_back((DebugRendererGE *)_entity);
        return;
    case TypeGraphicEntity::MESH_RENDER_GRAPHIC_ENTITY:
        meshRenders.push_back((MeshRenderGE *)_entity);
        return;
    case TypeGraphicEntity::DENSITY_VOLUME_GRAPHIC_ENTITY:
        densityVolumes.push_back((DensityVolumeGE *)_entity);
        return;
    }
}

void EntityRegister::_removeEntityGE(Entity *_entity)
{
    switch (_entity->typeEntity)
    {
    case TypeGraphicEntity::CAMERA_GRAPHIC_ENTITY:
        cameras.remove((CameraGE *)_entity);
        return;
    case TypeGraphicEntity::DEBUG_RENDER_GRAPHIC_ENTITY:
        debugRenders.remove((DebugRendererGE *)_entity);
        return;
    case TypeGraphicEntity::MESH_RENDER_GRAPHIC_ENTITY:
        meshRenders.remove((MeshRenderGE *)_entity);
        return;
    case TypeGraphicEntity::POINT_LIGHT_GRAPHIC_ENTITY:
        pointLights.remove((PointLightGE *)_entity);
        return;
    case TypeGraphicEntity::DENSITY_VOLUME_GRAPHIC_ENTITY:
        densityVolumes.remove((DensityVolumeGE *)_entity);
        return;
    }
}

CameraGE *EntityRegister::getActiveCamera() const
{
    if (cameras.empty())
    {
        return NULL;
    }
    auto end = cameras.end();
    for (auto i = cameras.begin(); i != end; ++i)
    {
        CameraGE *camera = *i;
        if (camera->enable)
        {
            return camera;
        }
    }
    return NULL;
}

const std::list<DebugRendererGE *> *EntityRegister::getListDebugRenderers() const
{
    return &debugRenders;
}

const std::list<MeshRenderGE *> *EntityRegister::getListMeshRenders() const
{
    return &meshRenders;
}

const std::list<PointLightGE *> *EntityRegister::getListPointLights() const
{
    return &pointLights;
}

const std::list<DensityVolumeGE *> *EntityRegister::getListDensityVolumes() const
{
    return &densityVolumes;
}