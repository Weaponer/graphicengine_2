#include <MTH/glGen_matrix.h>

#include "renderEngine/mainSystems/sceneStructure/entities/debugRenderGE.h"

DebugRendererGE::DebugRendererGE()
{
    mxTransforms.setSingleMatrix();
    clear();
}

DebugRendererGE::~DebugRendererGE()
{
}

void DebugRendererGE::setGlobalMatrix()
{
    mxTransforms.setSingleMatrix();
}

void DebugRendererGE::setLocalMatrix(const mth::mat4 &matrix)
{
    mxTransforms = matrix;
}

void DebugRendererGE::setMatrixFromTransform(const Transform &trasform)
{
    mth::quat q = trasform.getQuaternion();
    mth::vec3 pos = trasform.getPosition();
    mth::vec3 scale = trasform.getScale();

    mxTransforms = mth::translate(pos.x, pos.y, pos.z) * q.getMatrix() * mth::scale(scale.x, scale.y, scale.z);
}

const DebugRendererGE::_vector_data &DebugRendererGE::getVectorData(int index)
{
    return dates[index];
}

void DebugRendererGE::clear()
{
    currentPos = -1;
}

bool DebugRendererGE::isThereEnoughSpace(size_t count)
{
    return getCountDates() + count <= DEBUG_RENDERER_STACK_SIZE;
}

bool DebugRendererGE::setColor(mth::vec3 color)
{
    if (!isThereEnoughSpace(1))
    {
        return false;
    }
    currentPos++;
    dates[currentPos].code = CODE_DEBUG_RENDERER_COLOR;
    dates[currentPos].vec = color;
    return true;
}

bool DebugRendererGE::drawCube(mth::vec3 pos, mth::vec3 size)
{
    if (!isThereEnoughSpace(24))
    {
        return false;
    }
    drawLine(pos + mxTransforms.transform(mth::vec3(size.x, -size.y, -size.z)),
             pos + mxTransforms.transform(mth::vec3(size.x, -size.y, size.z)), false);
    drawLine(pos + mxTransforms.transform(mth::vec3(-size.x, -size.y, -size.z)),
             pos + mxTransforms.transform(mth::vec3(-size.x, -size.y, size.z)), false);

    drawLine(pos + mxTransforms.transform(mth::vec3(size.x, -size.y, size.z)),
             pos + mxTransforms.transform(mth::vec3(-size.x, -size.y, size.z)), false);
    drawLine(pos + mxTransforms.transform(mth::vec3(size.x, -size.y, -size.z)),
             pos + mxTransforms.transform(mth::vec3(-size.x, -size.y, -size.z)), false);

    drawLine(pos + mxTransforms.transform(mth::vec3(size.x, size.y, -size.z)),
             pos + mxTransforms.transform(mth::vec3(size.x, size.y, size.z)), false);
    drawLine(pos + mxTransforms.transform(mth::vec3(-size.x, size.y, -size.z)),
             pos + mxTransforms.transform(mth::vec3(-size.x, size.y, size.z)), false);

    drawLine(pos + mxTransforms.transform(mth::vec3(size.x, size.y, size.z)),
             pos + mxTransforms.transform(mth::vec3(-size.x, size.y, size.z)), false);
    drawLine(pos + mxTransforms.transform(mth::vec3(size.x, size.y, -size.z)),
             pos + mxTransforms.transform(mth::vec3(-size.x, size.y, -size.z)), false);

    drawLine(pos + mxTransforms.transform(mth::vec3(size.x, size.y, size.z)),
             pos + mxTransforms.transform(mth::vec3(size.x, -size.y, size.z)), false);
    drawLine(pos + mxTransforms.transform(mth::vec3(size.x, size.y, -size.z)),
             pos + mxTransforms.transform(mth::vec3(size.x, -size.y, -size.z)), false);

    drawLine(pos + mxTransforms.transform(mth::vec3(-size.x, size.y, size.z)),
             pos + mxTransforms.transform(mth::vec3(-size.x, -size.y, size.z)), false);
    drawLine(pos + mxTransforms.transform(mth::vec3(-size.x, size.y, -size.z)),
             pos + mxTransforms.transform(mth::vec3(-size.x, -size.y, -size.z)), false);
    return true;
}

bool DebugRendererGE::drawLine(mth::vec3 start, mth::vec3 end, bool _useMatetric)
{
    if (!isThereEnoughSpace(2))
    {
        return false;
    }
    currentPos++;
    dates[currentPos].code = CODE_DEBUG_RENDERER_START_POS;
    if(_useMatetric)
    {
        dates[currentPos].vec = mxTransforms.transform(start);
    }
    else
    {
        dates[currentPos].vec = start;
    }
    currentPos++;

    dates[currentPos].code = CODE_DEBUG_RENDERER_END_POS;
    if(_useMatetric)
    {
        dates[currentPos].vec = mxTransforms.transform(end);
    }
    else
    {
        dates[currentPos].vec = end;
    }
    return true;
}

bool DebugRendererGE::drawRect(mth::vec3 pos, mth::vec2 size)
{
    if (!isThereEnoughSpace(8))
    {
        return false;
    }
    drawLine(pos + mth::vec3(size.x, 0, size.y), pos + mth::vec3(-size.x, 0, size.y));
    drawLine(pos + mth::vec3(size.x, 0, -size.y), pos + mth::vec3(-size.x, 0, -size.y));

    drawLine(pos + mth::vec3(size.x, 0, size.y), pos + mth::vec3(size.x, 0, -size.y));
    drawLine(pos + mth::vec3(-size.x, 0, size.y), pos + mth::vec3(-size.x, 0, -size.y));
    return true;
}