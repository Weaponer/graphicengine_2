#include "renderEngine/mainSystems/sceneStructure/entities/meshRenderGE.h"

MeshRenderGE::MeshRenderGE()
{
    staticRender = false;
    generateShadows = true;
    glGenBuffers(1, &bufferMatricesData);
    changedTypeRender();
}

MeshRenderGE::~MeshRenderGE()
{
    glDeleteBuffers(1, &bufferMatricesData);
}

void MeshRenderGE::matricesUpdated()
{
    glBindBuffer(GL_UNIFORM_BUFFER, bufferMatricesData);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(MatricesData), &matrices);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void MeshRenderGE::changedTypeRender()
{
    glBindBuffer(GL_UNIFORM_BUFFER, bufferMatricesData);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(MatricesData), &matrices, staticRender ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

int MeshRenderGE::getLodPerCoef(float _heightCoef) const
{
    int count = lods.size();
    if (count == 0)
    {
        return 0;
    }
    else if(count == 1)
    {
        if(lods[0].minimalHeightCoef >= _heightCoef)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        if (_heightCoef > lods[0].minimalHeightCoef)
        {
            return 0;
        }
        int minCount = count - 1; 
        if(lods[minCount].minimalHeightCoef >= _heightCoef)
        {
            return minCount + 1;
        }
        for (int i = 0; i < minCount; i++)
        {
            if (lods[i].minimalHeightCoef >= _heightCoef && lods[i + 1].minimalHeightCoef < _heightCoef)
            {
                return i + 1;
            }
        }
    }
    return 0;
}

std::pair<Material *, Mesh *> MeshRenderGE::getLod(int _lod) const
{
    if (_lod == 0)
    {
        return std::make_pair(material, mesh);
    }
    else
    {
        _lod -= 1;
        return std::make_pair(lods[_lod].materials, lods[_lod].mesh);
    }
}

float MeshRenderGE::getHeightCoefLod(int _lod) const
{
    if (_lod == 0)
    {
        return 1.0f;
    }
    else
    {
        _lod -= 1;
        return lods[_lod].minimalHeightCoef;
    }
}

int MeshRenderGE::countLods() const
{
    return lods.size() + 1;
}

void MeshRenderGE::removeLod(int _index)
{
    if (_index != 0 && _index <= lods.size())
    {
        lods.erase(lods.begin() + _index - 1);
    }
}

void MeshRenderGE::addLod()
{
    LOD lod = LOD();
    lod.materials = NULL;
    lod.mesh = NULL;
    lod.minimalHeightCoef = 0.0f;
    lods.push_back(lod);
}

void MeshRenderGE::setMaterialInLod(int _lod, Material *_material)
{
    if (_lod == 0)
    {
        material = _material;
    }
    else
    {
        _lod -= 1;
        lods[_lod].materials = _material;
    }
}

void MeshRenderGE::setMeshInLod(int _lod, Mesh *_mesh)
{
    if (_lod == 0)
    {
        mesh = _mesh;
    }
    else
    {
        _lod -= 1;
        lods[_lod].mesh = _mesh;
    }
}

void MeshRenderGE::setHeightCoefInLod(int _lod, float _heightCoef)
{
    if (_lod != 0)
    {
        _lod -= 1;
        lods[_lod].minimalHeightCoef = _heightCoef;
    }
}