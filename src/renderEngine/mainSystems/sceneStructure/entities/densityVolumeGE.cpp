#include "renderEngine/mainSystems/sceneStructure/entities/densityVolumeGE.h"

DensityVolumeGE::DensityVolumeGE()
{
    rotate = mth::quat();
    position = mth::vec3();
    scale = mth::vec3(1.0f, 1.0f, 1.0f);

    typeVolume = DENSITY_VOLUME_GE_TYPE_CUBE;
    placementPlaneVolume = DENSITY_VOLUME_GE_PLACEMENT_PLANE_XY;
    density = 1.0f;
    colorVolume = mth::col(1.0f, 1.0f, 1.0f, 1.0f);
    powGradient = 0.0f;
    placementPlaneGradient = DENSITY_VOLUME_GE_PLACEMENT_PLANE_XY;

    startDecay = 10.0f;
    startDecay = 15.0f;
    edgeFade = 0.2f;
    wasUpdate = true;

    glGenBuffers(1, &uboDensityVolumeData);
    glBindBuffer(GL_UNIFORM_BUFFER, uboDensityVolumeData);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(DensityVolumeData), NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

DensityVolumeGE::~DensityVolumeGE()
{
    glDeleteBuffers(1, &uboDensityVolumeData);
}

void DensityVolumeGE::updateUBO()
{
    DensityVolumeData data;
    mth::mat4 rot = rotate.getMatrix();
    mth::mat4 inverseRot = rot;
    inverseRot.transpose();
    data.toLocalSpace = mth::scale(1.0f / scale.x, 1.0f / scale.y, 1.0f / scale.z) * inverseRot * mth::translate(-position.x, -position.y, -position.z);
    data.density_pow_startD_endD = mth::vec4(density, powGradient, startDecay, endDecay);
    data.color = colorVolume;
    data.typeVolume_PPV_PPG[0] = typeVolume;
    data.typeVolume_PPV_PPG[1] = placementPlaneVolume;
    data.typeVolume_PPV_PPG[2] = placementPlaneGradient;
    data.edgeFade = edgeFade;

    glBindBuffer(GL_UNIFORM_BUFFER, uboDensityVolumeData);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(DensityVolumeData), &data);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    wasUpdate = false;
}

void DensityVolumeGE::setWasUpdate()
{
    wasUpdate = true;
}

bool DensityVolumeGE::isWasUpdate() const
{
    return wasUpdate;
}