#include "renderEngine/mainSystems/mainSystems.h"

MainSystems::MainSystems(SettingsMainSystems *_settings, SettingsGraphicPipeline *_settingsGraphicPipeline)
    : settings(_settings),
      settingsGraphicPipeline(_settingsGraphicPipeline),
      mainFunctions(&_settings->techincalShaderStorage, &sceneStructure, _settingsGraphicPipeline),
      shaderStorage(&_settings->techincalShaderStorage),
      programActivator(),
      sceneStructure(&_settings->graphicConfig)
{
    programActivator.setRegistryProperty(shaderStorage.getPropertiesRegistry());
    programActivator.setUniformBlocksRegistry(shaderStorage.getUniformBlocksRegistry());
    programActivator.setMainFunctions(&mainFunctions);
}

MainSystems::~MainSystems()
{
}

void MainSystems::update(float _deltaTime)
{
    mainFunctions.update(_deltaTime);
}

MainFunctions *MainSystems::getMainFunctions()
{
    return &mainFunctions;
}

ShadersStorage *MainSystems::getShadersStorage()
{
    return &shaderStorage;
}

ProgramActivator *MainSystems::getProgramActivator()
{
    return &programActivator;
}

SceneStructure *MainSystems::getSceneStructure()
{
    return &sceneStructure;
}

TechnicalShaderStorage *MainSystems::getTechnicalShaderStorage() const
{
    return &settings->techincalShaderStorage;
}
ModelLightShaderStorage *MainSystems::getModelLightStorage() const
{
    return &settings->modelLightShaderStorage;
}

ResolutionSettings MainSystems::getResolutionSettings() const
{
    return settings->resolutionSettings;
}

SettingsMainSystems *MainSystems::getSettingsMainSystems()
{
    return settings;
}

SettingsGraphicPipeline *MainSystems::getSettingsGraphicPipeline()
{
    return settingsGraphicPipeline;
}