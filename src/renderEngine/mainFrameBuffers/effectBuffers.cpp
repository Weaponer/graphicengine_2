#include <MTH/vectors.h>

#include "renderEngine/mainFrameBuffers/effectBuffers.h"

EffectBuffers::EffectBuffers()
{
    glGenVertexArrays(1, &vertexBuffer);
    glBindVertexArray(vertexBuffer);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &bufferVectors);
    glBindBuffer(GL_ARRAY_BUFFER, bufferVectors);
    glBufferData(GL_ARRAY_BUFFER, sizeof(mth::vec2) * 4 * 2, NULL, GL_STATIC_DRAW);
    mth::vec2 pointsScreen[4] = {
        mth::vec2(-1.0, -1.0), mth::vec2(-1.0, 1.0),
        mth::vec2(1.0, -1.0), mth::vec2(1.0, 1.0)};

    mth::vec2 uvScreen[4] = {
        mth::vec2(0, 0), mth::vec2(0, 1.0),
        mth::vec2(1.0, 0), mth::vec2(1.0, 1.0)};

    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(mth::vec2) * 4, pointsScreen);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(mth::vec2) * 4, sizeof(mth::vec2) * 4, uvScreen);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLuint *)0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLuint *)(sizeof(mth::vec2) * 4));

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

EffectBuffers::~EffectBuffers()
{
    glDeleteVertexArrays(1, &vertexBuffer);
    glDeleteBuffers(1, &bufferVectors);
}

void EffectBuffers::enable()
{
    glBindVertexArray(vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, bufferVectors);
}

void EffectBuffers::callDraw()
{
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void EffectBuffers::disable()
{
    /*glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);*/
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}