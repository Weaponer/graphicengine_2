#include "renderEngine/mainFrameBuffers/GBuffer.h"

GBuffer::GBuffer(ResolutionSettings resolution, MainDepthBuffer *_depthBuffer, ColorFrameBuffer *_normalsBuffer)
{
    normalsBuffer = _normalsBuffer;
    depthBuffer = _depthBuffer;

    enabledLayers = 0;

    glGenFramebuffers(1, &frameBuffer);
    glGenTextures(G_BUFFER_COUNT_BUFFERS, layers);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _normalsBuffer->getColorTexture(), 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, _depthBuffer->getDepthtexture(), 0);

    for (int i = 0; i < G_BUFFER_COUNT_BUFFERS; i++)
    {
        glBindTexture(GL_TEXTURE_2D, layers[i]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, resolution.width, resolution.height, 0, GL_RGBA, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1 + i, GL_TEXTURE_2D, layers[i], 0);
    }

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

GBuffer::~GBuffer()
{
    glDeleteFramebuffers(1, &frameBuffer);
    glDeleteTextures(G_BUFFER_COUNT_BUFFERS, layers);
}

void GBuffer::clearLayers()
{
    GLfloat clearCol[4] = {0.0f, 0.0f, 0.0f, 0.0f};
    for (int i = 0; i < G_BUFFER_COUNT_BUFFERS; i++)
    {
        if ((enabledLayers & (1 << 0)) != 0)
        {
            glClearBufferfv(GL_COLOR, i + 1, clearCol);
        }
    }
}

void GBuffer::enableFramebuffer(int _layers)
{
    enabledLayers = _layers;
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    GLenum drawBuffers[G_BUFFER_COUNT_BUFFERS + 1];
    drawBuffers[0] = GL_COLOR_ATTACHMENT0;

    int count = 1;
    for (int i = 0; i < G_BUFFER_COUNT_BUFFERS; i++)
    {
        if ((enabledLayers & (1 << i)) != 0)
        {
            drawBuffers[count] = GL_COLOR_ATTACHMENT1 + i;
            count++;
        }
    }
    glDrawBuffers(count, drawBuffers);
}

void GBuffer::disableFramebuffer()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

ColorFrameBuffer *GBuffer::getNormalsBuffer()
{
    return normalsBuffer;
}

MainDepthBuffer *GBuffer::getDepthBuffer()
{
    return depthBuffer;
}

GLuint GBuffer::getLayer(int _index)
{
    return layers[_index];
}

GLuint GBuffer::getLayer1()
{
    return layers[0];
}

GLuint GBuffer::getLayer2()
{
    return layers[1];
}

GLuint GBuffer::getLayer3()
{
    return layers[2];
}

GLuint GBuffer::getLayer4()
{
    return layers[3];
}