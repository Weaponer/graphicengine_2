#include <iostream>

#include "renderEngine/mainFrameBuffers/mainDepthBuffer.h"

MainDepthBuffer::MainDepthBuffer(ResolutionSettings _resolution)
{
    width = _resolution.width;
    height = _resolution.height;

    glGenFramebuffers(1, &frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glGenTextures(1, &depthTexture);
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTexture, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

MainDepthBuffer::~MainDepthBuffer()
{
    glDeleteTextures(1, &depthTexture);
    glDeleteFramebuffers(1, &frameBuffer);
}

int MainDepthBuffer::getWidth() const
{
    return width;
}

int MainDepthBuffer::getHeight() const
{
    return height;
}

GLuint MainDepthBuffer::getFrameBuffer()
{
    return frameBuffer;
}

GLuint MainDepthBuffer::getDepthtexture()
{
    return depthTexture;
}

void MainDepthBuffer::clear()
{
    glClear(GL_DEPTH_BUFFER_BIT);
}

void MainDepthBuffer::resize(const ResolutionSettings *_resolution)
{
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTexture, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void MainDepthBuffer::enable()
{
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
}

void MainDepthBuffer::disable()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}