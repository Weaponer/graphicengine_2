#include "renderEngine/mainFrameBuffers/colorFrameBuffer.h"

ColorFrameBuffer::ColorFrameBuffer(GLint _format, mth::vec4 _clearColor, ResolutionSettings _resolution, MainDepthBuffer *_mainDetphBuffer)
{
    width = _resolution.width;
    height = _resolution.height;
    format = _format;
    clearColor = _clearColor;
    mainDepthBuffer = _mainDetphBuffer;

    glGenFramebuffers(1, &frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glGenTextures(1, &textureBuffer);
    glBindTexture(GL_TEXTURE_2D, textureBuffer);

    GLint f = _format;
    if (_format == GL_RGBA16F)
    {
        f = GL_RGBA;
    }
    else if (_format == GL_RGB16F)
    {
        f = GL_RGB;
    }
    else if (_format == GL_RG16F)
    {
        f = GL_RG;
    }
    else if (_format == GL_R16F)
    {
        f = GL_RED;
    }
    glTexImage2D(GL_TEXTURE_2D, 0, _format, width, height, 0, f, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureBuffer, 0);
    if (mainDepthBuffer != NULL)
    {
        glBindTexture(GL_TEXTURE_2D, mainDepthBuffer->getDepthtexture());
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, mainDepthBuffer->getDepthtexture(), 0);
    }
    glClearColor(clearColor.x, clearColor.y, clearColor.z, clearColor.w);
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

ColorFrameBuffer::~ColorFrameBuffer()
{
    glDeleteTextures(1, &textureBuffer);
    glDeleteFramebuffers(1, &frameBuffer);
}

int ColorFrameBuffer::getWidth() const
{
    return width;
}

int ColorFrameBuffer::getHeight() const
{
    return height;
}

GLint ColorFrameBuffer::getFormat() const
{
    return format;
}

GLuint ColorFrameBuffer::getFrameBuffer()
{
    return frameBuffer;
}

MainDepthBuffer *ColorFrameBuffer::getMainDepthBuffer()
{
    return mainDepthBuffer;
}

GLuint ColorFrameBuffer::getColorTexture()
{
    return textureBuffer;
}

void ColorFrameBuffer::clear()
{
    glClearColor(clearColor.x, clearColor.y, clearColor.z, clearColor.w);
    glClear(GL_COLOR_BUFFER_BIT);
}

mth::vec4 ColorFrameBuffer::getClearColor() const
{
    return clearColor;
}

void ColorFrameBuffer::setClearColor(const mth::vec4 *_vec)
{
    clearColor = *_vec;
    glClearColor(_vec->x, _vec->y, _vec->z, _vec->w);
}

void ColorFrameBuffer::resize(const ResolutionSettings *_resolution)
{
    width = _resolution->width;
    height = _resolution->height;
    glBindTexture(GL_TEXTURE_2D, textureBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureBuffer, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glBindTexture(GL_TEXTURE_2D, 0);
}

void ColorFrameBuffer::enable()
{
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
}

void ColorFrameBuffer::disable()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}