#include <ios>
#include "renderEngine/mainFrameBuffers/lightBasicFrameBuffer.h"

LightBasicFrameBuffer::LightBasicFrameBuffer(ResolutionSettings _resolution)
{
    width = _resolution.width;
    height = _resolution.height;

    glGenFramebuffers(1, &frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glGenTextures(1, &pointsAndDirectionalLightBuffer);
    glBindTexture(GL_TEXTURE_2D, pointsAndDirectionalLightBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, pointsAndDirectionalLightBuffer, 0);
    glClearColor(0, 0, 0, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glGenTextures(1, &pointLightsDirectionBuffer);
    glBindTexture(GL_TEXTURE_2D, pointLightsDirectionBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, pointLightsDirectionBuffer, 0);
    glClearColor(0, 0, 0, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

LightBasicFrameBuffer::~LightBasicFrameBuffer()
{
    glDeleteTextures(1, &pointsAndDirectionalLightBuffer);
    glDeleteTextures(1, &pointLightsDirectionBuffer);
    glDeleteFramebuffers(1, &frameBuffer);
}

int LightBasicFrameBuffer::getWidth() const
{
    return width;
}

int LightBasicFrameBuffer::getHeight() const
{
    return height;
}

GLuint LightBasicFrameBuffer::getFrameBuffer()
{
    return frameBuffer;
}

GLuint LightBasicFrameBuffer::getPointsAndDirectionalLightTexture()
{
    return pointsAndDirectionalLightBuffer;
}

GLuint LightBasicFrameBuffer::getPointLightsDirectionTexture()
{
    return pointLightsDirectionBuffer;
}

void LightBasicFrameBuffer::clear()
{
    GLenum buffers[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
    glDrawBuffers(2, buffers);
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT);
}

void LightBasicFrameBuffer::resize(const ResolutionSettings *_resolution)
{
}

void LightBasicFrameBuffer::enable()
{
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    GLenum buffers[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
    glDrawBuffers(2, buffers);
}

void LightBasicFrameBuffer::disable()
{
    GLenum buffers[1] = {GL_COLOR_ATTACHMENT0};
    glDrawBuffers(1, buffers);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
