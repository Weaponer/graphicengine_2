#include "renderEngine/mainFrameBuffers/transparentFrameBuffer.h"

TransparantFrameBuffer::TransparantFrameBuffer(const ResolutionSettings *_resolution, MainDepthBuffer *_mainDetphBuffer)
    : mainDepthBuffer(_mainDetphBuffer)
{
    glGenFramebuffers(1, &frameBuffer);
    glGenBuffers(1, &bufferNodes);
    glGenBuffers(1, &bufferLightNodes);
    glGenBuffers(1, &bufferResultNodes);
    glGenTextures(1, &uimageBuffer);
    glGenTextures(1, &imageCountBuffer);
    resize(_resolution);

    glGenBuffers(1, &atomicCounterNodes);
    glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, atomicCounterNodes);
    GLuint value = 0;
    glBufferData(GL_ATOMIC_COUNTER_BUFFER, sizeof(uint), &value, GL_STATIC_DRAW);
    glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, atomicCounterNodes);
    glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);
}

TransparantFrameBuffer::~TransparantFrameBuffer()
{
    glDeleteFramebuffers(1, &frameBuffer);
    glDeleteBuffers(1, &bufferNodes);
    glDeleteBuffers(1, &bufferLightNodes);
    glDeleteBuffers(1, &bufferResultNodes);
    glDeleteTextures(1, &uimageBuffer);
    glDeleteTextures(1, &imageCountBuffer);
    glDeleteBuffers(1, &atomicCounterNodes);
}

int TransparantFrameBuffer::getWidth() const
{
    return width;
}

int TransparantFrameBuffer::getHeight() const
{
    return height;
}

GLuint TransparantFrameBuffer::getFrameBuffer()
{
    return frameBuffer;
}

GLuint TransparantFrameBuffer::getUImageBuffer()
{
    return uimageBuffer;
}

GLuint TransparantFrameBuffer::getUImageCountBuffer()
{
    return imageCountBuffer;
}

GLuint TransparantFrameBuffer::getBufferNodes()
{
    return bufferNodes;
}

GLuint TransparantFrameBuffer::getAtomicCounterNodes()
{
    return atomicCounterNodes;
}

GLuint TransparantFrameBuffer::getCountNodes()
{
    return countNodes;
}

void TransparantFrameBuffer::clear()
{
    GLuint value = 0xFFFFFFFF;
    glClearTexImage(uimageBuffer, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, &value);

    value = MAX_COUNT_FRAGMENTS_PER_PIXEL;
    glClearTexImage(imageCountBuffer, 0, GL_RED_INTEGER, GL_INT, &value);

    value = 0;
    glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, atomicCounterNodes);
    glBufferSubData(GL_ATOMIC_COUNTER_BUFFER, 0, sizeof(uint), &value);
    glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);
}

void TransparantFrameBuffer::resize(const ResolutionSettings *_resolution)
{
    width = _resolution->width;
    height = _resolution->height;

    countNodes = width * height * MAX_COUNT_FRAGMENTS_PER_PIXEL;

    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, mainDepthBuffer->getDepthtexture(), 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, bufferNodes);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(FragNode) * countNodes, NULL, GL_STATIC_DRAW);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, bufferNodes);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, bufferLightNodes);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(FragLightNode) * countNodes, NULL, GL_STATIC_DRAW);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, bufferLightNodes);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, bufferResultNodes);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(FragResultNode) * countNodes, NULL, GL_STATIC_DRAW);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, bufferResultNodes);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    glBindTexture(GL_TEXTURE_2D, uimageBuffer);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32UI, width, height);
    glBindImageTexture(0, uimageBuffer, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
    glBindTexture(GL_TEXTURE_2D, 0);

    glBindTexture(GL_TEXTURE_2D, imageCountBuffer);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32I, width, height);
    glBindImageTexture(1, imageCountBuffer, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32I);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void TransparantFrameBuffer::enable()
{
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
}

void TransparantFrameBuffer::disable()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}