#define GL_GLEXT_PROTOTYPES

#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <MTH/glGen_matrix.h>

#include <debugOutput/globalProfiler.h>

#include "graphicEngine.h"
#include "model.h"

GraphicEngine::GraphicEngine(SettingsMainSystems *_settingsMainSystems)
    : settingsMainSystems(_settingsMainSystems),
      settingsGraphicPipeline(),
      mainSystems(_settingsMainSystems, &settingsGraphicPipeline),
      graphicPipeline(&settingsGraphicPipeline, &mainSystems)
{
    mainSystems.getProgramActivator()->setGraphicPipeline(&graphicPipeline);
}

GraphicEngine::~GraphicEngine()
{
}

void GraphicEngine::connectSDLWindow(Window *window)
{
    win = window;

    SDL_GL_SwapWindow(win->getWindow());
}

void GraphicEngine::render(float _deltaTime)
{
    if (win == NULL)
    {
        return;
    }

    GlobalProfiler::startSample("Graphic: main systems");
    mainSystems.update(_deltaTime);
    GlobalProfiler::stopSample("Graphic: main systems");

    GlobalProfiler::startSample("Graphic: graphic pipeline");
    graphicPipeline.update();
    mainSystems.getMainFunctions()->getGraphicOperations()->blitToContext(graphicPipeline.getResultColorBuffer());
    glFlush();
    GlobalProfiler::stopSample("Graphic: graphic pipeline");
}

void GraphicEngine::endRender()
{
    GlobalProfiler::startSample("Graphic: end render");
    glFinish();
    GlobalProfiler::stopSample("Graphic: end render");
}

void GraphicEngine::swapWindow()
{
    GlobalProfiler::startSample("Graphic: Swap");
    SDL_GL_SwapWindow(win->getWindow());
    GlobalProfiler::stopSample("Graphic: Swap");
}

mth::vec2 GraphicEngine::getSizeWindow()
{
    return mth::vec2(settingsMainSystems->resolutionSettings.width, settingsMainSystems->resolutionSettings.height);
}