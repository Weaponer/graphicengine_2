#include "renderEngine/resources/cubeMap.h"

CubeMap::CubeMap(std::array<SDL_Surface *, 6> &_surfaces)
{
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_CUBE_MAP, tex);
    for (int i = 0; i < 6; i++)
    {
        GLint format = GL_RGB;
        if (_surfaces[i]->format->BytesPerPixel == 1)
        {
            format = GL_ALPHA;
        }
        else if (_surfaces[i]->format->BytesPerPixel == 2)
        {
            format = GL_RG;
        }
        else if (_surfaces[i]->format->BytesPerPixel == 3)
        {
            format = GL_BGR;
        }
        else
        {
            format = GL_BGRA;
        }

        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, _surfaces[i]->w,
                     _surfaces[i]->h, 0, format, GL_UNSIGNED_BYTE, _surfaces[i]->pixels);
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    GLint g_nMaxAnisotropy;
    glGetIntegerv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &g_nMaxAnisotropy);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_ANISOTROPY_EXT, g_nMaxAnisotropy - 0.1f);

    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    for(int i = 0; i < 6; i++)
    {
        SDL_FreeSurface(_surfaces[i]);
    }
}

CubeMap::~CubeMap()
{
    if (tex != 0)
    {
        glDeleteTextures(1, &tex);
    }
}