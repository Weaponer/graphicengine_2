#include "renderEngine/resources/shader/properties/colorProperty.h"

ColorProperty::ColorProperty(int _codeData, PropertiesRegistry *_registry) : Property(_codeData, _registry, TypeProperty::Color_P), value(0, 0, 0, 1) 
{
}

ColorProperty::~ColorProperty()
{
}

mth::col ColorProperty::getValue() const
{
    return value;
}

void ColorProperty::setValue(const mth::col *_value)
{
    value = *_value;
}

void ColorProperty::pastValue(Property *_property) const
{
    ColorProperty *prop = reinterpret_cast<ColorProperty *>(_property);
    prop->value = value;
    prop->registry = registry;
    prop->codeData = codeData;
}

Property *ColorProperty::copy() const
{
    ColorProperty *prop = new ColorProperty(codeData, registry);
    prop->value = value;
    return prop;
}