#include "renderEngine/resources/shader/properties/toggleProperty.h"

ToggleProperty::ToggleProperty(int _codeData, PropertiesRegistry *_registry) : Property(_codeData, _registry, TypeProperty::Toggle_P), value(false) 
{
}

ToggleProperty::~ToggleProperty()
{
}

bool ToggleProperty::getValue() const
{
    return value;
}

void ToggleProperty::setValue(bool _value)
{
    value = _value;
}

void ToggleProperty::pastValue(Property *_property) const
{
    ToggleProperty *prop = reinterpret_cast<ToggleProperty *>(_property);
    prop->value = value;
    prop->registry = registry;
    prop->codeData = codeData;
}

Property *ToggleProperty::copy() const
{
    ToggleProperty *prop = new ToggleProperty(codeData, registry);
    prop->value = value;
    return prop;
}