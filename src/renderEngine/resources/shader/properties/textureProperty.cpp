#include "renderEngine/resources/shader/properties/textureProperty.h"

TextureProperty::TextureProperty(int _codeData, PropertiesRegistry *_registry) : Property(_codeData, _registry, TypeProperty::Texture_P), value(NULL)
{
}

TextureProperty::~TextureProperty()
{
}

Texture *TextureProperty::getValue() const
{
    return value;
}

void TextureProperty::setValue(Texture *_value)
{
    value = _value;
}

void TextureProperty::pastValue(Property *_property) const
{
    TextureProperty *prop = reinterpret_cast<TextureProperty *>(_property);
    prop->value = value;
    prop->registry = registry;
    prop->codeData = codeData;
}

Property *TextureProperty::copy() const
{
    TextureProperty *prop = new TextureProperty(codeData, registry);
    prop->value = value;
    return prop;
}