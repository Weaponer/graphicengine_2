#include "renderEngine/resources/shader/properties/internalProperty.h"

InternalProperty::InternalProperty(int _codeData, PropertiesRegistry *_registry) : Property(_codeData, _registry, TypeProperty::Internal_P)
{
}

InternalProperty::~InternalProperty()
{
}

void InternalProperty::pastValue(Property *_property) const
{
    InternalProperty *prop = reinterpret_cast<InternalProperty *>(_property);
    prop->registry = registry;
    prop->codeData = codeData;
}

Property *InternalProperty::copy() const
{
    return new InternalProperty(codeData, registry);
}