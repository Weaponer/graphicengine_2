#include "renderEngine/resources/shader/properties/floatProperty.h"

FloatProperty::FloatProperty(int _codeData, PropertiesRegistry *_registry) : Property(_codeData, _registry, TypeProperty::Float_P), value(0) 
{
}

FloatProperty::~FloatProperty()
{
}

float FloatProperty::getValue() const
{
    return value;
}

void FloatProperty::setValue(float _value)
{
    value = _value;
}

void FloatProperty::pastValue(Property *_property) const
{
    FloatProperty *prop = reinterpret_cast<FloatProperty *>(_property);
    prop->value = value;
    prop->registry = registry;
    prop->codeData = codeData;
}

Property *FloatProperty::copy() const
{
    FloatProperty *prop = new FloatProperty(codeData, registry);
    prop->value = value;
    return prop;
}