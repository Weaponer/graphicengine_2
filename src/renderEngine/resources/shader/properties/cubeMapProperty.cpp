#include "renderEngine/resources/shader/properties/cubeMapProperty.h"

CubeMapProperty::CubeMapProperty(int _codeData, PropertiesRegistry *_registry) : Property(_codeData, _registry, TypeProperty::CubeMap_P), value(NULL) 
{
}

CubeMapProperty::~CubeMapProperty()
{
}

CubeMap *CubeMapProperty::getValue() const
{
    return value;
}

void CubeMapProperty::setValue(CubeMap *_value)
{
    value = _value;
}

void CubeMapProperty::pastValue(Property *_property) const
{
    CubeMapProperty *prop = reinterpret_cast<CubeMapProperty *>(_property);
    prop->value = value;
    prop->registry = registry;
    prop->codeData = codeData;
}

Property *CubeMapProperty::copy() const
{
    CubeMapProperty *prop = new CubeMapProperty(codeData, registry);
    prop->value = value;
    return prop;
}