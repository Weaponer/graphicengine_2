#include "renderEngine/resources/shader/properties/intProperty.h"

IntProperty::IntProperty(int _codeData, PropertiesRegistry *_registry) : Property(_codeData, _registry, TypeProperty::Int_P), value(0) 
{
}

IntProperty::~IntProperty()
{
}

int IntProperty::getValue() const
{
    return value;
}

void IntProperty::setValue(int _value)
{
    value = _value;
}

void IntProperty::pastValue(Property *_property) const
{
    IntProperty *prop = reinterpret_cast<IntProperty *>(_property);
    prop->value = value;
    prop->registry = registry;
    prop->codeData = codeData;
}

Property *IntProperty::copy() const
{
    IntProperty *prop = new IntProperty(codeData, registry);
    prop->value = value;
    return prop;
}