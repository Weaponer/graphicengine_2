#include "renderEngine/resources/shader/properties/vectorProperty.h"

VectorProperty::VectorProperty(int _codeData, PropertiesRegistry *_registry) : Property(_codeData, _registry, TypeProperty::Vector_P),  value(0, 0, 0, 0)
{
}

VectorProperty::~VectorProperty()
{
}

mth::vec4 VectorProperty::getValue() const
{
    return value;
}

void VectorProperty::setValue(const mth::vec4 *_value)
{
    value = *_value;
}

void VectorProperty::pastValue(Property *_property) const
{
    VectorProperty *prop = reinterpret_cast<VectorProperty *>(_property);
    prop->value = value;
    prop->registry = registry;
    prop->codeData = codeData;
}

Property *VectorProperty::copy() const
{
    VectorProperty *prop = new VectorProperty(codeData, registry);
    prop->value = value;
    return prop;
}