#include "renderEngine/resources/shader/properties/property.h"
#include "renderEngine/resources/shader/shaderPropertiesData/propertiesRegistry.h"


Property::Property(int _codeData, PropertiesRegistry *_registry, TypeProperty _type) : codeData(_codeData), registry(_registry), type(_type)
{
}

Property::~Property()
{
}

int Property::getCodeData() const
{
    return codeData;
}

TypeProperty Property::getType() const
{
    return type;
}

const char *Property::getName() const
{
    return registry->getNameProperty(codeData);
}

const Property *Property::getGlobalSetting() const
{
    return registry->getGlobalSettingProperty(codeData);
}

bool Property::getUseGlobalSetting() const
{
    return registry->getUseGlobalProperty(codeData);
}