#include "renderEngine/resources/shader/shaderPropertiesData/propertiesRegistry.h"

PropertiesRegistry::PropertiesRegistry()
{
}

PropertiesRegistry::~PropertiesRegistry()
{
    clearProperties();
}

void PropertiesRegistry::clearProperties()
{
    int count = defineProperties.getPosBuffer();
    for (int i = 0; i < count; i++)
    {
        PropertyData **prop = defineProperties.getMemory(i);
        if (*prop != NULL)
        {
            delete *prop;
        }
    }
    defineProperties.clearMemory();
    indexPropertiesByNames.clear();
}

bool PropertiesRegistry::checkPropertyExist(const char *_name, TypeProperty _type)
{
    DefineProperty define = {_name, _type};
    return indexPropertiesByNames.find(define) != indexPropertiesByNames.end();
}

void PropertiesRegistry::addNewProperties(const char *_name, TypeProperty _type)
{
    if (checkPropertyExist(_name, _type) == true)
    {
        return;
    }
    DefineProperty define = {_name, _type};
    PropertyData *property = new PropertyData();
    property->setName(_name);

    sizeM index = 0;
    defineProperties.assignmentMemoryAndCopy(&property, index);

    indexPropertiesByNames[define] = index;
    property->setExampleProperty(createDefaultProperty(_name, _type));
}

void PropertiesRegistry::setUseGlobalProperty(int _codeProperty, bool _use)
{
    if (_codeProperty < 0 || (sizeM)_codeProperty >= defineProperties.getPosBuffer())
    {
        return;
    }
    PropertyData *data = *defineProperties.getMemory(_codeProperty);
    TypeProperty type = data->getPropertyForExample()->getType();
    if (type == TypeProperty::Internal_P || type == TypeProperty::Toggle_P)
    {
        return;
    }
    data->setUseGlobalData(_use);
}

bool PropertiesRegistry::getUseGlobalProperty(int _codeProperty)
{
    if (_codeProperty < 0 || (sizeM)_codeProperty >= defineProperties.getPosBuffer())
    {
        return false;
    }
    PropertyData *data = *defineProperties.getMemory(_codeProperty);
    TypeProperty type = data->getPropertyForExample()->getType();
    if (type == TypeProperty::Internal_P || type == TypeProperty::Toggle_P)
    {
        return false;
    }
    return data->getUseGlobalData();
}

void PropertiesRegistry::setGlobalPropertyValue(int _codeProperty, const Property *_property)
{
    if (_codeProperty < 0 || (sizeM)_codeProperty >= defineProperties.getPosBuffer())
    {
        return;
    }
    PropertyData *data = *defineProperties.getMemory(_codeProperty);
    TypeProperty type = data->getPropertyForExample()->getType();
    if (type == TypeProperty::Internal_P || type == TypeProperty::Toggle_P ||
        type != _property->getType())
    {
        return;
    }
    _property->pastValue(data->getPropertyForExample());
}

void PropertiesRegistry::setGlobalPropertyValue(const char *_name, TypeProperty _type, const Property *_property)
{
    DefineProperty define = {_name, _type};
    auto found = indexPropertiesByNames.find(define);

    if (found == indexPropertiesByNames.end())
    {
        return;
    }
    int num = (*found).second;
    PropertyData *data = *defineProperties.getMemory(num);
    _property->pastValue(data->getPropertyForExample());
}

int PropertiesRegistry::getPropertyCodeRegistry(const char *_name, TypeProperty _type)
{
    DefineProperty define = {_name, _type};
    auto found = indexPropertiesByNames.find(define);

    if (found == indexPropertiesByNames.end())
    {
        return -1;
    }

    return (*found).second;
}

const char *PropertiesRegistry::getNameProperty(int _codeProperty)
{
    if (_codeProperty < 0 || (sizeM)_codeProperty >= defineProperties.getPosBuffer())
    {
        return NULL;
    }
    PropertyData *data = *defineProperties.getMemory(_codeProperty);
    return data->getName();
}

const Property *PropertiesRegistry::getGlobalSettingProperty(int _codeProperty)
{
    if (_codeProperty < 0 || (sizeM)_codeProperty >= defineProperties.getPosBuffer())
    {
        return NULL;
    }
    PropertyData *data = *defineProperties.getMemory(_codeProperty);
    return data->getPropertyForExample();
}

const Property *PropertiesRegistry::getGlobalSettingProperty(const char *_name, TypeProperty _type)
{
    DefineProperty define = {_name, _type};
    auto found = indexPropertiesByNames.find(define);
    if (found == indexPropertiesByNames.end())
    {
        return NULL;
    }
    return (*defineProperties.getMemory((*found).second))->getPropertyForExample();
}

Property *PropertiesRegistry::createDefaultProperty(const char *_name, TypeProperty _type)
{
    DefineProperty define = {_name, _type};
    auto found = indexPropertiesByNames.find(define);
    if (found == indexPropertiesByNames.end())
    {
        return NULL;
    }
    Property *property = NULL;

    if (_type == TypeProperty::Int_P)
    {
        property = new IntProperty((*found).second, this);
    }
    else if (_type == TypeProperty::Float_P)
    {
        property = new FloatProperty((*found).second, this);
    }
    else if (_type == TypeProperty::Toggle_P)
    {
        property = new ToggleProperty((*found).second, this);
    }
    else if (_type == TypeProperty::Internal_P)
    {
        property = new InternalProperty((*found).second, this);
    }
    else if (_type == TypeProperty::Internal_P)
    {
        property = new InternalProperty((*found).second, this);
    }
    else if (_type == TypeProperty::Vector_P)
    {
        property = new VectorProperty((*found).second, this);
    }
    else if (_type == TypeProperty::Color_P)
    {
        property = new ColorProperty((*found).second, this);
    }
    else if (_type == TypeProperty::Texture_P)
    {
        property = new TextureProperty((*found).second, this);
    }
    else if (_type == TypeProperty::CubeMap_P)
    {
        property = new CubeMapProperty((*found).second, this);
    }
    return property;
}

InternalProperty *PropertiesRegistry::createInternalProperty(const char *_name)
{
    return reinterpret_cast<InternalProperty *>(createDefaultProperty(_name, TypeProperty::Internal_P));
}

IntProperty *PropertiesRegistry::createIntProperty(const char *_name, int _val)
{
    Property *prop = createDefaultProperty(_name, TypeProperty::Int_P);
    if (prop == NULL)
    {
        return NULL;
    }
    IntProperty *vprop = reinterpret_cast<IntProperty *>(prop);
    vprop->setValue(_val);
    return vprop;
}

FloatProperty *PropertiesRegistry::createFloatProperty(const char *_name, float _val)
{
    Property *prop = createDefaultProperty(_name, TypeProperty::Float_P);
    if (prop == NULL)
    {
        return NULL;
    }
    FloatProperty *vprop = reinterpret_cast<FloatProperty *>(prop);
    vprop->setValue(_val);
    return vprop;
}

ToggleProperty *PropertiesRegistry::createToggleProperty(const char *_name, bool _val)
{
    Property *prop = createDefaultProperty(_name, TypeProperty::Toggle_P);
    if (prop == NULL)
    {
        return NULL;
    }
    ToggleProperty *vprop = reinterpret_cast<ToggleProperty *>(prop);
    vprop->setValue(_val);
    return vprop;
}

VectorProperty *PropertiesRegistry::createVectorProperty(const char *_name, mth::vec4 _val)
{
    Property *prop = createDefaultProperty(_name, TypeProperty::Vector_P);
    if (prop == NULL)
    {
        return NULL;
    }
    VectorProperty *vprop = reinterpret_cast<VectorProperty *>(prop);
    vprop->setValue(&_val);
    return vprop;
}

ColorProperty *PropertiesRegistry::createColorProperty(const char *_name, mth::col _val)
{
    Property *prop = createDefaultProperty(_name, TypeProperty::Color_P);
    if (prop == NULL)
    {
        return NULL;
    }
    ColorProperty *vprop = reinterpret_cast<ColorProperty *>(prop);
    vprop->setValue(&_val);
    return vprop;
}

TextureProperty *PropertiesRegistry::createTextureProperty(const char *_name, Texture *_val)
{
    Property *prop = createDefaultProperty(_name, TypeProperty::Texture_P);
    if (prop == NULL)
    {
        return NULL;
    }
    TextureProperty *vprop = reinterpret_cast<TextureProperty *>(prop);
    vprop->setValue(_val);
    return vprop;
}

CubeMapProperty *PropertiesRegistry::createCubeMapProperty(const char *_name, CubeMap *_val)
{
    Property *prop = createDefaultProperty(_name, TypeProperty::CubeMap_P);
    if (prop == NULL)
    {
        return NULL;
    }
    CubeMapProperty *vprop = reinterpret_cast<CubeMapProperty *>(prop);
    vprop->setValue(_val);
    return vprop;
}