#include "renderEngine/resources/shader/shaderPropertiesData/propertyData.h"

PropertyData::PropertyData() : name(""), useGlobalData(false), exampleProperty(NULL)
{
}

PropertyData::~PropertyData()
{
    delete exampleProperty;
}

void PropertyData::setName(const char *_name)
{
    name = std::string(_name);
}

const char *PropertyData::getName() const
{
    return name.c_str();
}

void PropertyData::setExampleProperty(Property *_exampleProperty)
{
    exampleProperty = _exampleProperty;
}

void PropertyData::setUseGlobalData(bool _enable)
{
    useGlobalData = true;
}

bool PropertyData::getUseGlobalData() const
{
    return useGlobalData;
}

Property *PropertyData::getPropertyForExample() const
{
    return exampleProperty;
}