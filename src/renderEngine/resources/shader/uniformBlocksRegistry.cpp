#include "renderEngine/resources/shader/uniformBlocksRegistry.h"

UniformBlocksRegistry::UniformBlocksRegistry()
{
}

UniformBlocksRegistry::~UniformBlocksRegistry()
{
}

void UniformBlocksRegistry::addNewReserveUniformBLock(const char *_name)
{
    auto found = indexesByName.find(_name);
    if (found != indexesByName.end())
    {
        return;
    }

    int count = indexesByName.size();
    indexesByName[_name] = count;
}

int UniformBlocksRegistry::getIndexReserveUniformBlock(const char *_name) const
{
    auto found = indexesByName.find(_name);
    if (found != indexesByName.end())
    {
        return (*found).second;
    }
    else
    {
        return -1;
    }
}