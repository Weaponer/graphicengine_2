#include "renderEngine/resources/shader/pass.h"

Pass::Pass(const char *_name, TypePass _typePass, QueueRender _queue, RenderType _renderType, CullType _cullType, bool _zTest, bool _zWrite, bool _generateInstancing)
    : name(_name), typePass(_typePass), queue(_queue), renderType(_renderType), cullType(_cullType),
      zTest(_zTest), zWrite(_zWrite), generateInstancing(_generateInstancing), isDefaultPass(false), settingsDefaultPass(0x00),
      sizeAdditionalData(0), additionalData(NULL)
{
    oldCodeForPipeline = -1;
    oldRenderPipeline = 0;
}

Pass::~Pass()
{
    clearPrograms();
    auto end = renderPipelines.end();
    for (auto i = renderPipelines.begin(); i != end; ++i)
    {
        GLuint pipeline = (*i).second;
        glDeleteProgramPipelines(1, &pipeline);
    }
    if (additionalData != NULL)
    {
        delete[] additionalData;
    }
}

const char *Pass::getName() const
{
    return name.c_str();
}

TypePass Pass::getTypePass() const
{
    return typePass;
}

QueueRender Pass::getQueueRender() const
{
    return queue;
}

RenderType Pass::getRenderType() const
{
    return renderType;
}

CullType Pass::getCullType() const
{
    return cullType;
}

bool Pass::zTestOn() const
{
    return zTest;
}

bool Pass::zWriteOn() const
{
    return zWrite;
}

bool Pass::generateInstancingOn() const
{
    return generateInstancing;
}

void Pass::setMasks(int _maskUsingDefinesVertex, int _maskUsingDefinesFragment)
{
    maskUsingDefinesVertex = _maskUsingDefinesVertex;
    maskUsingDefinesFragment = _maskUsingDefinesFragment;
}

void Pass::clearPrograms()
{
    int count = vertexPrograms.size();
    for (int i = 0; i < count; i++)
    {
        delete vertexPrograms[i];
    }
    vertexPrograms.clear();

    count = fragmentPrograms.size();
    for (int i = 0; i < count; i++)
    {
        delete fragmentPrograms[i];
    }
    fragmentPrograms.clear();
}

void Pass::connectProgram(Program *_program, GLenum _type)
{
    if (_type == GL_VERTEX_SHADER && vertexPrograms.size() <= MAX_COUNT_PROGRAMS)
    {
        vertexPrograms.push_back(_program);
    }
    else if (_type == GL_FRAGMENT_SHADER && fragmentPrograms.size() <= MAX_COUNT_PROGRAMS)
    {
        fragmentPrograms.push_back(_program);
    }
}

void Pass::getIndexesPrograms(int _mask, int *_indexVertexProgram, int *_indexFragmentProgram)
{
    int maskVertexProgram = _mask & maskUsingDefinesVertex;
    int maskFragmentProgram = _mask & maskUsingDefinesFragment;

    *_indexVertexProgram = 0;
    *_indexFragmentProgram = 0;

    if (isDefaultPass)
    {
        return;
    }

    int count = vertexPrograms.size();
    for (int i = 0; i < count; i++)
    {
        if (vertexPrograms[i]->getMask() == maskVertexProgram)
        {
            *_indexVertexProgram = i;
            break;
        }
    }
    count = fragmentPrograms.size();
    for (int i = 0; i < count; i++)
    {
        if (fragmentPrograms[i]->getMask() == maskFragmentProgram)
        {
            *_indexFragmentProgram = i;
            break;
        }
    }
}

GLuint Pass::getProgramPipeline(int _indexVertexProgram, int _indexFragmentProgram)
{
    short code = (((short)_indexVertexProgram) << 8) | ((short)_indexFragmentProgram);
    if (code == oldCodeForPipeline)
    {
        return oldRenderPipeline;
    }
    oldCodeForPipeline = code;
    auto found = renderPipelines.find(code);
    if (found == renderPipelines.end())
    {
        GLuint pipeline = 0;
        glGenProgramPipelines(1, &pipeline);
        glUseProgramStages(pipeline, GL_VERTEX_SHADER_BIT, vertexPrograms[_indexVertexProgram]->getProgram());
        glUseProgramStages(pipeline, GL_FRAGMENT_SHADER_BIT, fragmentPrograms[_indexFragmentProgram]->getProgram());
        renderPipelines[code] = pipeline;
        oldRenderPipeline = pipeline;
        return pipeline;
    }
    else
    {
        oldRenderPipeline = (*found).second;
        return oldRenderPipeline;
    }
}

Program *Pass::getVertexProgram(int _indexVertexProgram)
{
    return vertexPrograms[_indexVertexProgram];
}

Program *Pass::getFragmentProgram(int _indexFragmentProgram)
{
    return fragmentPrograms[_indexFragmentProgram];
}

void Pass::setPassIsDefault(char _data)
{
    isDefaultPass = true;
    settingsDefaultPass = _data;
}

bool Pass::checkIsDefaultPass()
{
    return isDefaultPass;
}

char Pass::getDataDefaultPassSettings()
{
    return settingsDefaultPass;
}

void Pass::initAdditionalData(int _size, char *_data)
{
    sizeAdditionalData = _size;
    additionalData = _data;
}

const char *Pass::getAdditionalData() const
{
    return additionalData;
}