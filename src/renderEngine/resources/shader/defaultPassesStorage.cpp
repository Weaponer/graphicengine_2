#include "resources/shader/defaultPassesStorage.h"

DefaultPassesStorage::DefaultPassesStorage(TechnicalShaderStorage *_technicalShaderStorage)
{
    addDefaultPassShadow(_technicalShaderStorage);
    addDefaultPassDepth(_technicalShaderStorage);
    addDefaultPassNormal(_technicalShaderStorage);
    addDefaultPassDepthNormal(_technicalShaderStorage);
}

DefaultPassesStorage::~DefaultPassesStorage()
{
    for (int i = 0; i < defaultPasses.size(); i++)
    {
        delete defaultPasses[i].first;
    }
    defaultPasses.clear();
}

Pass *DefaultPassesStorage::getDefaultPass(TypePass _type, bool _useClipping, bool _useNormalMaps)
{
    int count = defaultPasses.size();
    for (int i = 0; i < count; i++)
    {
        std::pair<Pass *, char> pass = defaultPasses[i];
        if (pass.first->getTypePass() == _type && checkCorrectData(_useClipping, _useNormalMaps, pass.second))
        {
            return pass.first;
        }
    }

    return NULL;
}

char DefaultPassesStorage::getPassData(bool _useClipping, bool _useNormalMap, bool _noneHaveNormalMap)
{
    char data = 0;
    if (_useClipping)
    {
        data = data | 1;
    }
    if (_useNormalMap)
    {
        data = data | (1 << 1);
    }
    if (_noneHaveNormalMap)
    {
        data = data | (1 << 2);
    }
    return data;
}

bool DefaultPassesStorage::checkCorrectData(bool _useClipping, bool _useNormalMap, char _dataForCheck)
{
    if (_useClipping == ((_dataForCheck & 1) != 0) &&
        ((_useNormalMap == ((_dataForCheck & (1 << 1)) != 0)) || (_dataForCheck & (1 << 2)) != 0))
    {
        if (_useNormalMap && ((_dataForCheck & (1 << 1)) != 0))
        {
            return true;
        }
        else if (_useNormalMap == false && ((_dataForCheck & (1 << 2)) != 0))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

void DefaultPassesStorage::pushVertexProgramToPass(TechnicalShaderStorage *_technicalShaderStorage, Pass *_pass)
{
    TShader *vertexShader = _technicalShaderStorage->getTShader("DefaultVertex");
    Program *vertProgram = new Program();
    vertProgram->setCompiledProgram(vertexShader->getProgram(), vertexShader->getType());
    _pass->connectProgram(vertProgram, vertexShader->getType());
}

void DefaultPassesStorage::pushFragmentProgramToPass(TechnicalShaderStorage *_technicalShaderStorage,
                                                     const char *_nameFragmentProgram, Pass *_pass)
{
    TShader *fragmentShader = _technicalShaderStorage->getTShader(_nameFragmentProgram);
    Program *fragProgram = new Program();
    fragProgram->setCompiledProgram(fragmentShader->getProgram(), fragmentShader->getType());
    _pass->connectProgram(fragProgram, fragmentShader->getType());
}

void DefaultPassesStorage::pushPassesWithoutNormals(TechnicalShaderStorage *_technicalShaderStorage, TypePass _typePass, CullType _cullType,
                                                    const char *_name)
{
    Pass *pass = new Pass(std::string(_name).c_str(), _typePass, QueueRender::QueueRender_Geometry,
                          RenderType::RenderType_Opaque, _cullType, true, true, true);
    char data = getPassData(false, false, true);
    pass->setPassIsDefault(data);

    pushVertexProgramToPass(_technicalShaderStorage, pass);
    pushFragmentProgramToPass(_technicalShaderStorage, "DefaultFragment", pass);

    defaultPasses.push_back(std::make_pair(pass, data));

    Pass *passWithClipping = new Pass((std::string(_name).c_str() + std::string("Clipping")).c_str(),
                                      _typePass, QueueRender::QueueRender_Geometry,
                                      RenderType::RenderType_Opaque, _cullType, true, true, true);
    data = getPassData(true, false, true);
    passWithClipping->setPassIsDefault(data);

    pushVertexProgramToPass(_technicalShaderStorage, passWithClipping);
    pushFragmentProgramToPass(_technicalShaderStorage, "DefaultFragmentWithClipping", passWithClipping);

    defaultPasses.push_back(std::make_pair(passWithClipping, data));
}

void DefaultPassesStorage::pushPassesWithoutNormalMaps(TechnicalShaderStorage *_technicalShaderStorage,
                                                       TypePass _typePass, CullType _cullType, const char *_name)
{
    Pass *pass = new Pass(std::string(_name).c_str(), _typePass, QueueRender::QueueRender_Geometry,
                          RenderType::RenderType_Opaque, _cullType, true, true, true);
    char data = getPassData(false, false, true);
    pass->setPassIsDefault(data);

    pushVertexProgramToPass(_technicalShaderStorage, pass);
    pushFragmentProgramToPass(_technicalShaderStorage, "DefaultFragmentNormalNoTex", pass);

    defaultPasses.push_back(std::make_pair(pass, data));

    Pass *passWithClipping = new Pass((std::string(_name).c_str() + std::string("Clipping")).c_str(),
                                      _typePass, QueueRender::QueueRender_Geometry,
                                      RenderType::RenderType_Opaque, _cullType, true, true, true);
    data = getPassData(true, false, true);
    passWithClipping->setPassIsDefault(data);

    pushVertexProgramToPass(_technicalShaderStorage, passWithClipping);
    pushFragmentProgramToPass(_technicalShaderStorage, "DefaultFragmentNormalNoTexWithClipping", passWithClipping);

    defaultPasses.push_back(std::make_pair(passWithClipping, data));
}

void DefaultPassesStorage::pushPassesWithNormalMaps(TechnicalShaderStorage *_technicalShaderStorage,
                                                    TypePass _typePass, CullType _cullType, const char *_name)
{
    Pass *pass = new Pass(std::string(_name).c_str(), _typePass, QueueRender::QueueRender_Geometry,
                          RenderType::RenderType_Opaque, _cullType, true, true, true);
    char data = getPassData(false, true, false);
    pass->setPassIsDefault(data);

    pushVertexProgramToPass(_technicalShaderStorage, pass);
    pushFragmentProgramToPass(_technicalShaderStorage, "DefaultFragmentNormal", pass);

    defaultPasses.push_back(std::make_pair(pass, data));

    Pass *passWithClipping = new Pass((std::string(_name).c_str() + std::string("Clipping")).c_str(),
                                      _typePass, QueueRender::QueueRender_Geometry,
                                      RenderType::RenderType_Opaque, _cullType, true, true, true);
    data = getPassData(true, true, false);
    passWithClipping->setPassIsDefault(data);

    pushVertexProgramToPass(_technicalShaderStorage, passWithClipping);
    pushFragmentProgramToPass(_technicalShaderStorage, "DefaultFragmentNormalWithClipping", passWithClipping);

    defaultPasses.push_back(std::make_pair(passWithClipping, data));
}

void DefaultPassesStorage::addDefaultPassShadow(TechnicalShaderStorage *_technicalShaderStorage)
{
    pushPassesWithoutNormals(_technicalShaderStorage, TypePass::Shadow, CullType::CullType_Front, "DefaultShadowPass");
}

void DefaultPassesStorage::addDefaultPassDepth(TechnicalShaderStorage *_technicalShaderStorage)
{
    pushPassesWithoutNormals(_technicalShaderStorage, TypePass::Depth, CullType::CullType_Back, "DefaultDepthPass");
}

void DefaultPassesStorage::addDefaultPassNormal(TechnicalShaderStorage *_technicalShaderStorage)
{
    pushPassesWithoutNormalMaps(_technicalShaderStorage, TypePass::Normal, CullType::CullType_Back, "DefaultNormalPass");
    pushPassesWithNormalMaps(_technicalShaderStorage, TypePass::Normal, CullType::CullType_Back, "DefaultNormalPass");
}

void DefaultPassesStorage::addDefaultPassDepthNormal(TechnicalShaderStorage *_technicalShaderStorage)
{
    pushPassesWithoutNormalMaps(_technicalShaderStorage, TypePass::DepthNormal, CullType::CullType_Back, "DefaultDepthNormalPassWNM");
    pushPassesWithNormalMaps(_technicalShaderStorage, TypePass::DepthNormal, CullType::CullType_Back, "DefaultDepthNormalPassWM");
}