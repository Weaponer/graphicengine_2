#include "renderEngine/resources/shader/shadersStorage.h"

ShadersStorage::ShadersStorage(TechnicalShaderStorage *_technicalShaderStorage) : shadersByName(), propertiesRegistry(),
                                                                                  defaultPassesStorage(_technicalShaderStorage),
                                                                                  uniformBlocksRegistry()
{
}

ShadersStorage::~ShadersStorage()
{
    while (shadersByName.size())
    {
        removeShader((*shadersByName.begin()).second);
    }
}

void ShadersStorage::addNewShader(Shader *_shader, int _countProperties, const std::vector<std::string *> *_propertiesNames, const std::vector<TypeProperty> *_propertiesTypes)
{
    correctNameShader(_shader);

    shadersByName[_shader->getName()] = _shader;
    _shader->setShadersStorage(this);
    for (int i = 0; i < _countProperties; i++)
    {
        propertiesRegistry.addNewProperties(_propertiesNames->at(i)->c_str(), _propertiesTypes->at(i));
    }
}

void ShadersStorage::removeShader(Shader *_shader)
{
    shadersByName.erase(_shader->getName());
    _shader->setShadersStorage(NULL);
}

Shader *ShadersStorage::findShader(const char *_name)
{
    auto found = shadersByName.find(_name);
    if (found == shadersByName.end())
    {
        return NULL;
    }
    else
    {
        return (*found).second;
    }
}

PropertiesRegistry *ShadersStorage::getPropertiesRegistry()
{
    return &propertiesRegistry;
}

DefaultPassesStorage *ShadersStorage::getDefaultPassesStorage()
{
    return &defaultPassesStorage;
}

UniformBlocksRegistry *ShadersStorage::getUniformBlocksRegistry()
{
    return &uniformBlocksRegistry;
}

void ShadersStorage::correctNameShader(Shader *_shader)
{
    if (shadersByName.count(_shader->getName()) != 0)
    {
        _shader->setName((std::string(_shader->getName()) + "_Clone").c_str());
        correctNameShader(_shader);
    }
}