#include "renderEngine/resources/shader/setInitializedTextures.h"

SetInitializedTextures::SetInitializedTextures() : initializedTextures(), errorTry(false)
{
}

SetInitializedTextures::~SetInitializedTextures()
{
}

bool SetInitializedTextures::wasErrorTry() const
{
    return errorTry;
}

int SetInitializedTextures::tryTakeTexturePosition(int _indexProperty)
{
    int count = initializedTextures.size();
    if ((GL_TEXTURE31 - GL_TEXTURE0) < count)
    {
        errorTry = true;
        return -1;
    }
    else
    {
        if (initializedTextures.find(_indexProperty) != initializedTextures.end())
        {
            return initializedTextures[_indexProperty];
        }
        else
        {
            initializedTextures[_indexProperty] = count;
            return count;
        }
    }
}