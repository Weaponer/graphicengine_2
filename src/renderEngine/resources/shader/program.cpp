#include <vector>
#include <iostream>
#include <cstring>

#include "renderEngine/resources/shader/program.h"

Program::Program() : mask(0), program(0), noneAutoRemove(false), typeShader(GL_VERTEX_SHADER),
                     propertiesInProgram(std::map<int, DefaultGLint>()), sizeVectorInProgram(std::map<int, DefaultGLenum>()),
                     numTextureInProgram(std::map<int, DefaultGLint>()), internalProperties(std::vector<int>())
{
}

Program::~Program()
{
    if (!noneAutoRemove)
    {
        glDeleteProgram(program);
    }
}

void Program::setProgram(const char *_codeShaedr, GLenum _type)
{
    if (program != 0)
    {
        return;
    }
    typeShader = _type;

    program = glCreateShaderProgramv(_type, 1, &_codeShaedr);
    GLint linked = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);

    if (linked == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetShaderiv(program, GL_INFO_LOG_LENGTH, &maxLength);

        std::vector<GLchar> infoLog(maxLength);
        glGetShaderInfoLog(program, maxLength, &maxLength, &infoLog[0]);
        std::string error = std::string(infoLog.begin(), infoLog.end());
        std::cout << "Fail in shader: " + error << std::endl;
    }
}

void Program::setCompiledProgram(GLuint _program, GLenum _type)
{
    program = _program;
    typeShader = _type;
    noneAutoRemove = true;
}

void Program::setProperties(int _count, const Property **_properties, SetInitializedTextures *_setInitializedTextures)
{
    std::vector<std::pair<GLint, GLenum>> dataProperties = std::vector<std::pair<GLint, GLenum>>(_count, std::make_pair(-1, GL_NONE));

    int countActiveUniforms = 0;
    glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &countActiveUniforms);

    char propName[256];
    for (int i = 0; i < countActiveUniforms; i++)
    {
        GLint size = 0;
        GLenum type = 0;
        int lenghtName = 0;
        glGetActiveUniform(program, i, 256, &lenghtName, &size, &type, propName);
        for (int i2 = 0; i2 < _count; i2++)
        {
            const Property *property = _properties[i2];
            const char *name = property->getName();
            if (std::strcmp(name, propName) == 0)
            {
                dataProperties[i2] = std::make_pair(i, type);
            }
        }
    }

    for (int i = 0; i < _count; i++)
    {
        const Property *property = _properties[i];
        int codeData = property->getCodeData();
        const char *name = property->getName();
        TypeProperty typeProperty = property->getType();

        GLint locationUniform = glGetUniformLocation(program, name);
        GLenum type = dataProperties[i].second;
        if (typeProperty == TypeProperty::Texture_P)
        {
            int indexTexture = _setInitializedTextures->tryTakeTexturePosition(codeData);

            if(indexTexture >= 0)
            {
                numTextureInProgram[codeData] = {indexTexture};
                glProgramUniform1i(program, locationUniform, indexTexture);
            }
        }
        else if (typeProperty == TypeProperty::CubeMap_P)
        {
            int indexTexture = _setInitializedTextures->tryTakeTexturePosition(codeData);

            if(indexTexture >= 0)
            {
                numTextureInProgram[codeData] = {indexTexture};
                glProgramUniform1i(program, locationUniform, indexTexture);
            }
        }
        else if (typeProperty == TypeProperty::Vector_P)
        {
            GLenum type = 0;
            GLint size = 0;
            glGetActiveUniform(program, locationUniform, 0, NULL, &size, &type, NULL);
            if (type == GL_FLOAT_VEC2)
            {
                sizeVectorInProgram[codeData] = {GL_FLOAT_VEC2};
            }
            else if (type == GL_FLOAT_VEC3)
            {
                sizeVectorInProgram[codeData] = {GL_FLOAT_VEC3};
            }
            else
            {
                sizeVectorInProgram[codeData] = {GL_FLOAT_VEC4};
            }
        }
        else if (typeProperty == TypeProperty::Internal_P)
        {
            if (type == GL_SAMPLER_1D || type == GL_SAMPLER_2D || type == GL_SAMPLER_3D || type == GL_SAMPLER_CUBE ||
                type == GL_SAMPLER_1D_SHADOW || type == GL_SAMPLER_2D_SHADOW || type == GL_SAMPLER_CUBE_SHADOW)
            {
                int indexTexture = _setInitializedTextures->tryTakeTexturePosition(codeData);

                if(indexTexture >= 0)
                {
                    numTextureInProgram[codeData] = {indexTexture};
                    glProgramUniform1i(program, locationUniform, indexTexture);
                }
            }

            internalProperties.push_back(codeData);
        }
        propertiesInProgram[codeData] = {locationUniform};
    }
}

void Program::setActiveUniformBlock(const char *_nameUniformBlock, GLuint _binding)
{
    glUniformBlockBinding(program, glGetUniformBlockIndex(program, _nameUniformBlock), _binding);
    useUniformBlocks.push_back(_binding);
}

int Program::getCountUseUniformBlocks()
{
    return useUniformBlocks.size();
}

const int *Program::getUseUniformBlocks()
{
    return useUniformBlocks.data();
}

void Program::setMask(int _mask)
{
    mask = _mask;
}

int Program::getMask() const
{
    return mask;
}

GLuint Program::getProgram() const
{
    return program;
}

GLint Program::getPropertyInProgram(int _indexProperty)
{
    return propertiesInProgram[_indexProperty].glint;
}

GLint Program::getNumberSamplerTexture(int _indexProperty)
{
    GLint value = numTextureInProgram[_indexProperty].glint;
    return value;
}

GLenum Program::getTypeVector(int _indexProperty)
{
    return sizeVectorInProgram[_indexProperty].glenum;
}

int Program::getCountInternalProperties()
{
    return internalProperties.size();
}

int Program::getIndexInternalProperty(int _num)
{
    return internalProperties[_num];
}