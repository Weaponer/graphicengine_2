#include "renderEngine/resources/shader/shader.h"

#include "renderEngine/resources/shader/shadersStorage.h"

Shader::Shader(const char *_name) : name(_name), shadersStorage(NULL), defaultProperties(std::vector<Property *>())
{
    glGenBuffers(1, &uboSettings);
}

Shader::~Shader()
{
    glDeleteBuffers(1, &uboSettings);

    clearDefaultProperties();
    clearPasses();

    if (shadersStorage != NULL)
    {
        shadersStorage->removeShader(this);
    }
}

void Shader::setSettings(const ShaderSettings &_settings)
{
    settings = _settings;
    glBindBuffer(GL_UNIFORM_BUFFER, uboSettings);

    struct DataSettings
    {
        mth::vec4 maskClippingChanels;
        float valueClipping;
        bool inverse;
    } dataSettings;

    switch (settings.typeClippingChanel)
    {
    case Type_Clipping_Chanel_R:
        dataSettings.maskClippingChanels = mth::vec4(1.0, 0.0, 0.0, 0.0);
        break;
    case Type_Clipping_Chanel_G:
        dataSettings.maskClippingChanels = mth::vec4(0.0, 1.0, 0.0, 0.0);
        break;
    case Type_Clipping_Chanel_B:
        dataSettings.maskClippingChanels = mth::vec4(0.0, 0.0, 1.0, 0.0);
        break;
    case Type_Clipping_Chanel_A:
        dataSettings.maskClippingChanels = mth::vec4(0.0, 0.0, 0.0, 1.0);
        break;
    }
    dataSettings.valueClipping = settings.clippingValue;
    dataSettings.inverse = settings.inverseClipping;

    glBufferData(GL_UNIFORM_BUFFER, sizeof(DataSettings), &dataSettings, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

GLuint Shader::getUBOClippingSettings()
{
    return uboSettings;
}

ShaderSettings Shader::getSettings() const
{
    return settings;
}

void Shader::setShadersStorage(ShadersStorage *_shadersStorage)
{
    shadersStorage = _shadersStorage;
}

ShadersStorage *Shader::getShadersStorage() const
{
    return shadersStorage;
}

const char *Shader::getName() const
{
    return name.c_str();
}

void Shader::setName(const char *_correctName)
{
    name = std::string(_correctName);
}

QueueRender Shader::getQueue() const
{
    return maxQueue;
}

void Shader::setDefaultProperties(int _count, Property **_properties)
{
    clearDefaultProperties();
    defaultProperties.reserve(_count);
    for (int i = 0; i < _count; i++)
    {
        defaultProperties.push_back(_properties[i]);
    }
}

int Shader::getCountProperties() const
{
    return defaultProperties.size();
}

Property *Shader::getProperty(int _num) const
{
    return defaultProperties[_num];
}

bool Shader::checkHaveProperty(int _code) const
{
    int count = defaultProperties.size();
    for (int i = 0; i < count; i++)
    {
        if (defaultProperties[i]->getCodeData() == _code)
        {
            return true;
        }
    }
    return false;
}

int Shader::getMaskToggle(int _indexToggle) const
{
    return masksToggleProperties.at(_indexToggle);
}

void Shader::setMaskToggle(int _indexToggle, int _mask)
{
    masksToggleProperties[_indexToggle] = _mask;
}

void Shader::addPass(Pass *_pass)
{
    if (_pass->getQueueRender() > maxQueue)
    {
        maxQueue = _pass->getQueueRender();
    }

    if (_pass->getTypePass() == TypePass::BaseColor && baseColorPasses.size() <= MAX_COUNT_PASSES)
    {
        baseColorPasses.push_back(_pass);
    }
    else if (_pass->getTypePass() == TypePass::Depth && baseColorPasses.size() <= MAX_COUNT_PASSES)
    {
        depthPasses.push_back(_pass);
    }
    else if (_pass->getTypePass() == TypePass::Normal && baseColorPasses.size() <= MAX_COUNT_PASSES)
    {
        normalPasses.push_back(_pass);
    }
    else if (_pass->getTypePass() == TypePass::DepthNormal && baseColorPasses.size() <= MAX_COUNT_PASSES)
    {
        depthNormalPasses.push_back(_pass);
    }
    else if (_pass->getTypePass() == TypePass::Surface && baseColorPasses.size() <= MAX_COUNT_PASSES)
    {
        surfacePasses.push_back(_pass);
    }
    else if (shadowPasses.size() <= MAX_COUNT_PASSES)
    {
        shadowPasses.push_back(_pass);
    }
}

int Shader::getCountPasses(TypePass _typePass) const
{
    if (_typePass == TypePass::BaseColor)
    {
        return baseColorPasses.size();
    }
    else if (_typePass == TypePass::Depth)
    {
        return depthPasses.size();
    }
    else if (_typePass == TypePass::Normal)
    {
        return normalPasses.size();
    }
    else if (_typePass == TypePass::DepthNormal)
    {
        int count = depthNormalPasses.size();
        return count;
    }
    else if (_typePass == TypePass::Surface)
    {
        return surfacePasses.size();
    }
    else
    {
        return shadowPasses.size();
    }
}

Pass *Shader::getPass(TypePass _typePass, int _num) const
{
    if (_typePass == TypePass::BaseColor)
    {
        return baseColorPasses[_num];
    }
    else if (_typePass == TypePass::Depth)
    {
        return depthPasses[_num];
    }
    else if (_typePass == TypePass::Normal)
    {
        return normalPasses[_num];
    }
    else if (_typePass == TypePass::DepthNormal)
    {
        return depthNormalPasses[_num];
    }
    else if (_typePass == TypePass::Surface)
    {
        return surfacePasses[_num];
    }
    else
    {
        return shadowPasses[_num];
    }
}

void Shader::clearDefaultProperties()
{
    int count = defaultProperties.size();
    for (int i = 0; i < count; i++)
    {
        delete defaultProperties[i];
    }
    defaultProperties.clear();
    masksToggleProperties.clear();
}

void Shader::clearPasses()
{
    clearSpecTypePasses(&baseColorPasses);
    clearSpecTypePasses(&depthPasses);
    clearSpecTypePasses(&depthNormalPasses);
    clearSpecTypePasses(&normalPasses);
    clearSpecTypePasses(&shadowPasses);
    clearSpecTypePasses(&surfacePasses);
}

void Shader::clearSpecTypePasses(std::vector<Pass *> *_passes)
{
    int count = _passes->size();
    for (int i = 0; i < count; i++)
    {
        Pass *pass = (*_passes)[i];
        if (!pass->checkIsDefaultPass())
        {
            delete (*_passes)[i];
        }
    }
    _passes->clear();
}