#include <ios>
#include "renderEngine/resources/templateTag.h"

TemplateTag::TemplateTag() : refOnTag(NULL)
{
}

TemplateTag::~TemplateTag()
{
    refOnTag = NULL;
}

void *TemplateTag::getRefOnTag() const
{
    return refOnTag;
}

void TemplateTag::setRefOnTag(void *_refOnTag)
{
    refOnTag = _refOnTag;
}

void TemplateTag::clearRefOnTag()
{
    refOnTag = NULL;
}