#define GL_GLEXT_PROTOTYPES
#include <MTH/vectors.h>
#include <MTH/boundAABB.h>
#include <MTH/bounds.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <vector>

#include "mesh.h"

Mesh::Mesh(std::string name, std::vector<GLushort> *indexes, std::vector<mth::vec3> *positions, std::vector<mth::vec2> *uvs,
           std::vector<mth::vec3> *normals, std::vector<mth::vec3> *tangents)
{
    nameMesh = name;
    bufferV = 0;
    arrayV = 0;
    arrayE = 0;
    countIndexes = indexes->size();
    printf("model load_: indexes: %i positions: %i uvs: %i normals: %i tangents: %i\n", (int)indexes->size(), (int)positions->size(),
           (int)uvs->size(), (int)normals->size(), (int)tangents->size());

    uint count = positions->size();

    glGenVertexArrays(1, &arrayV); //generate params read buffer vertex
    glBindVertexArray(arrayV);

    glGenBuffers(1, &bufferV); //push params vertex to buffer
    glBindBuffer(GL_ARRAY_BUFFER, bufferV);
    glBufferData(GL_ARRAY_BUFFER, (sizeof(mth::vec3) * 3 + sizeof(mth::vec2)) * count, NULL, GL_STATIC_DRAW);

    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(mth::vec3) * count, &(*positions)[0]);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(mth::vec3) * count, sizeof(mth::vec2) * count, &(*uvs)[0]);
    glBufferSubData(GL_ARRAY_BUFFER, (sizeof(mth::vec3) + sizeof(mth::vec2)) * count, sizeof(mth::vec3) * count, &(*normals)[0]);
    glBufferSubData(GL_ARRAY_BUFFER, (sizeof(mth::vec3) * 2 + sizeof(mth::vec2)) * count, sizeof(mth::vec3) * count, &(*tangents)[0]);

    glGenBuffers(1, &arrayE); //push indexes model
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, arrayE);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * indexes->size(), &(*indexes)[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid *)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(sizeof(mth::vec3) * count));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid *)((sizeof(mth::vec3) + sizeof(mth::vec2)) * count));
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid *)((sizeof(mth::vec3) * 2 + sizeof(mth::vec2)) * count));

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    boundAABB = mth::generate_AABB_from_points(positions->size(), &(*(positions->begin())));
}

void Mesh::enableBuffers()
{
    glBindVertexArray(arrayV);
}

void Mesh::disableBuffers()
{
    glBindVertexArray(0);
}

Mesh::~Mesh()
{
    glDeleteBuffers(1, &bufferV);
    glDeleteBuffers(1, &arrayE);
    glDeleteVertexArrays(1, &arrayV);
}