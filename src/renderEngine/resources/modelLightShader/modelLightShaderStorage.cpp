#include <cstring>

#include "renderEngine/resources/modelLightShader/modelLightShaderStorage.h"

ModelLightShaderStorage::ModelLightShaderStorage() : modelLightsShaders()
{
}

ModelLightShaderStorage::~ModelLightShaderStorage()
{
    modelLightsShaders.clear();
}

void ModelLightShaderStorage::addModelLight(ModelLightShader *_modelLightShader)
{
    if (MAX_COUNT_MODEL_LIGHT <= modelLightsShaders.size())
    {
        return;
    }

    modelLightsShaders.push_back(_modelLightShader);
    _modelLightShader->setIndexModelLight(modelLightsShaders.size());
}

int ModelLightShaderStorage::getIndexModelLight(const char *_name) const
{
    for (int i = 0; i < modelLightsShaders.size(); i++)
    {
        if (std::strcmp(modelLightsShaders[i]->getName(), _name) == 0)
        {
            return modelLightsShaders[i]->getIndexModelLight();
        }
    }
    return OFFSET_INDEX_MODEL_LIGHT;
}

ModelLightShader *ModelLightShaderStorage::getModelLightShader(int _index)
{
    return modelLightsShaders[_index - 1];
}