#include "renderEngine/resources/modelLightShader/modelLightShader.h"

#include <iostream>
#include <vector>

ModelLightShader::ModelLightShader(const char *_name, const char *_program, const char *_vertexProgram, int _countUseLayers)
    : name(_name), pipeline(0), program(0), vertexProgram(0), countUseLayers(_countUseLayers)
{
    vertexProgram = glCreateShaderProgramv(GL_VERTEX_SHADER, 1, &_vertexProgram);
    program = glCreateShaderProgramv(GL_FRAGMENT_SHADER, 1, &_program);
    glGenProgramPipelines(1, &pipeline);

    bool hasError = false;
    std::vector<std::string> errorsBuffer;

    GLuint programs[2] = {vertexProgram, program};
    for (int i = 0; i < 2; i++)
    {
        GLuint prog = programs[i];

        GLint link = 0;
        glGetProgramiv(prog, GL_LINK_STATUS, &link);

        if (link == false)
        {
            hasError = true;
            GLint maxLength = 0;
            glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &maxLength);
            std::vector<char> bufferError;
            bufferError.resize(maxLength + 1);
            glGetProgramInfoLog(prog, maxLength, &maxLength, bufferError.data());

            if (prog == vertexProgram)
            {
                errorsBuffer.push_back(std::string("Vertex program: ") + std::string(bufferError.data() + std::string("\n") + std::string(_vertexProgram)));
            }
            else
            {
                errorsBuffer.push_back(std::string("Fragment program: ") + std::string(bufferError.data() ));
            }
        }
    }

    if (hasError)
    {
        for (int i = 0; i < errorsBuffer.size(); i++)
        {
            std::cout << errorsBuffer[i].c_str() << std::endl;
        }
    }

    glBindProgramPipeline(pipeline);
    glUseProgramStages(pipeline, GL_VERTEX_SHADER_BIT, vertexProgram);
    glUseProgramStages(pipeline, GL_FRAGMENT_SHADER_BIT, program);
    glBindProgramPipeline(0);
}

ModelLightShader::~ModelLightShader()
{
    glDeleteProgram(program);
    glDeleteProgram(vertexProgram);
    glDeleteProgramPipelines(1, &pipeline);
}

void ModelLightShader::setIndexModelLight(int _index)
{
    indexModelLight = _index;
    glProgramUniform1ui(program, ML_MODEL_LIGHT_LOC, (GLuint)indexModelLight);
}

int ModelLightShader::getIndexModelLight() const
{
    return indexModelLight;
}

const char *ModelLightShader::getName() const
{
    return name.c_str();
}

int ModelLightShader::getCountUseLayers() const
{
    return countUseLayers;
}

GLuint ModelLightShader::getProgram()
{
    return program;
}

GLuint ModelLightShader::getPipeline()
{
    return pipeline;
}