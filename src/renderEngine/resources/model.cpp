#define GL_GLEXT_PROTOTYPES
#include <MTH/vectors.h>
#include <MTH/boundAABB.h>
#include <MTH/bounds.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <vector>
#include <algorithm>

#include "model.h"
#include "mesh.h"

Model::Model(std::vector<Mesh *> &meshs) : meshs(meshs)
{
}

Model::~Model()
{
    std::for_each(meshs.begin(), meshs.end(), [](Mesh *mesh)
                  { delete mesh; });
}

Mesh *Model::getMeshAtIndex(uint index)
{
    if(meshs.size() > index)
    {
        return meshs[index];
    }
    else
    {
        return NULL;
    }
}

Mesh *Model::getMeshAtName(const std::string *name)
{
    auto end = meshs.end();
    for (auto i = meshs.begin(); i != end; ++i)
    {
        if ((*(*i)->getName()) == (*name))
        {
            return *i;
        }
    }
    return NULL;
}