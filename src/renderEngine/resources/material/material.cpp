#include <cstring>

#include "renderEngine/resources/material/material.h"
#include "renderEngine/resources/shader/properties/intProperty.h"
#include "renderEngine/resources/shader/properties/floatProperty.h"
#include "renderEngine/resources/shader/properties/toggleProperty.h"
#include "renderEngine/resources/shader/properties/colorProperty.h"
#include "renderEngine/resources/shader/properties/vectorProperty.h"
#include "renderEngine/resources/shader/properties/textureProperty.h"
#include "renderEngine/resources/shader/properties/cubeMapProperty.h"

Material::Material(Shader *_shader) : name("Material"), shader(_shader), generateInstancing(true),
                                      normalMapProperty(NULL), clippingTextureProperty(NULL)
{
    int countProperties = _shader->getCountProperties();
    properties.reserve(countProperties);
    for (int i = 0; i < countProperties; i++)
    {
        properties.push_back(_shader->getProperty(i)->copy());
    }

    updateMaskDefineProperties();

    ShaderSettings settings = _shader->getSettings();
    if (settings.normalMapIndex != -1)
    {
        normalMapProperty = reinterpret_cast<TextureProperty *>(properties[settings.normalMapIndex]);
    }
    if (settings.clippingTextureIndex != -1)
    {
        clippingTextureProperty = reinterpret_cast<TextureProperty *>(properties[settings.clippingTextureIndex]);
    }
}

Material::~Material()
{
    for (int i = 0; i < (int)properties.size(); i++)
    {
        delete properties[i];
    }
    properties.clear();
}

void Material::setName(const char *_name)
{
    name = std::string(_name);
}

const char *Material::getName() const
{
    return name.c_str();
}

Shader *Material::getShader() const
{
    return shader;
}

int Material::getCount() const
{
    return properties.size();
}

Property *Material::getProperty(int _index) const
{
    return properties[_index];
}

void Material::setInt(const char *_name, int value)
{
    IntProperty *prop = reinterpret_cast<IntProperty *>(getPropertyByName(_name, TypeProperty::Int_P));
    if (prop != NULL)
    {
        prop->setValue(value);
    }
}

void Material::setFloat(const char *_name, float value)
{
    FloatProperty *prop = reinterpret_cast<FloatProperty *>(getPropertyByName(_name, TypeProperty::Float_P));
    if (prop != NULL)
    {
        prop->setValue(value);
    }
}

void Material::setToggle(const char *_name, bool value)
{
    ToggleProperty *prop = reinterpret_cast<ToggleProperty *>(getPropertyByName(_name, TypeProperty::Toggle_P));
    if (prop != NULL)
    {
        prop->setValue(value);
        int localMask = shader->getMaskToggle(prop->getCodeData());
        if (value)
        {
            maskDefineProperties = maskDefineProperties | localMask;
        }
        else
        {
            maskDefineProperties = maskDefineProperties & (!localMask);
        }
    }
}

void Material::setVector(const char *_name, const mth::vec4 *value)
{
    VectorProperty *prop = reinterpret_cast<VectorProperty *>(getPropertyByName(_name, TypeProperty::Vector_P));
    if (prop != NULL)
    {
        prop->setValue(value);
    }
}

void Material::setColor(const char *_name, const mth::col *value)
{
    ColorProperty *prop = reinterpret_cast<ColorProperty *>(getPropertyByName(_name, TypeProperty::Color_P));
    if (prop != NULL)
    {
        prop->setValue(value);
    }
}

void Material::setTexture(const char *_name, Texture *value)
{
    TextureProperty *prop = reinterpret_cast<TextureProperty *>(getPropertyByName(_name, TypeProperty::Texture_P));
    if (prop != NULL)
    {
        prop->setValue(value);
    }
}

void Material::setCubeMap(const char *_name, CubeMap *value)
{
    CubeMapProperty *prop = reinterpret_cast<CubeMapProperty *>(getPropertyByName(_name, TypeProperty::CubeMap_P));
    if (prop != NULL)
    {
        prop->setValue(value);
    }
}

int Material::getInt(const char *_name)
{
    IntProperty *prop = reinterpret_cast<IntProperty *>(getPropertyByName(_name, TypeProperty::Int_P));
    if (prop != NULL)
    {
        return prop->getValue();
    }
    else
    {
        return 0;
    }
}

float Material::getFloat(const char *_name)
{
    FloatProperty *prop = reinterpret_cast<FloatProperty *>(getPropertyByName(_name, TypeProperty::Float_P));
    if (prop != NULL)
    {
        return prop->getValue();
    }
    else
    {
        return 0;
    }
}

bool Material::getToggle(const char *_name)
{
    ToggleProperty *prop = reinterpret_cast<ToggleProperty *>(getPropertyByName(_name, TypeProperty::Toggle_P));
    if (prop != NULL)
    {
        return prop->getValue();
    }
    else
    {
        return false;
    }
}

mth::vec4 Material::getVector(const char *_name)
{
    VectorProperty *prop = reinterpret_cast<VectorProperty *>(getPropertyByName(_name, TypeProperty::Vector_P));
    if (prop != NULL)
    {
        return prop->getValue();
    }
    else
    {
        return mth::vec4(0, 0, 0, 0);
    }
}

mth::col Material::getColor(const char *_name)
{
    ColorProperty *prop = reinterpret_cast<ColorProperty *>(getPropertyByName(_name, TypeProperty::Color_P));
    if (prop != NULL)
    {
        return prop->getValue();
    }
    else
    {
        return mth::col(0, 0, 0, 1);
    }
}

Texture *Material::getTexture(const char *_name)
{
    TextureProperty *prop = reinterpret_cast<TextureProperty *>(getPropertyByName(_name, TypeProperty::Texture_P));
    if (prop != NULL)
    {
        return prop->getValue();
    }
    else
    {
        return NULL;
    }
}

CubeMap *Material::getCubeMap(const char *_name)
{
    CubeMapProperty *prop = reinterpret_cast<CubeMapProperty *>(getPropertyByName(_name, TypeProperty::CubeMap_P));
    if (prop != NULL)
    {
        return prop->getValue();
    }
    else
    {
        return NULL;
    }
}

TypeProperty Material::getTypeProperty(const char *_name)
{
    for (int i = 0; i < (int)properties.size(); i++)
    {
        if (std::strcmp(_name, properties[i]->getName()) == 0)
        {
            return properties[i]->getType();
        }
    }
    return TypeProperty::Internal_P;
}

int Material::getMaskDefineProperties() const
{
    return maskDefineProperties;
}

Texture *Material::getSpecialNormalMap()
{
    if (normalMapProperty == NULL)
    {
        return NULL;
    }
    else
    {
        const TextureProperty *prop = reinterpret_cast<const TextureProperty *>(getCorrectProperty(normalMapProperty));
        return prop->getValue();
    }
}

Texture *Material::getSpecialClippingTexture()
{
    if (clippingTextureProperty == NULL)
    {
        return NULL;
    }
    else
    {
        const TextureProperty *prop = reinterpret_cast<const TextureProperty *>(getCorrectProperty(clippingTextureProperty));
        return prop->getValue();
    }
}

const Property *Material::getCorrectProperty(const Property *_localProperty)
{
    const Property *global = _localProperty->getGlobalSetting();
    if (global == NULL || _localProperty->getUseGlobalSetting() == false)
    {
        return _localProperty;
    }
    else
    {
        return global;
    }
}

bool Material::getGenerateInstancing() const
{
    return generateInstancing;
}

void Material::setGenerateInstancing(bool _enable)
{
    generateInstancing = _enable;
}

Property *Material::getPropertyByName(const char *_name, TypeProperty _typeProperty)
{
    for (int i = 0; i < (int)properties.size(); i++)
    {
        if (properties[i]->getType() == _typeProperty && std::strcmp(_name, properties[i]->getName()) == 0)
        {
            return properties[i];
        }
    }
    return NULL;
}

void Material::updateMaskDefineProperties()
{
    maskDefineProperties = 0;
    int countProperties = (int)properties.size();
    for (int i = 0; i < countProperties; i++)
    {
        Property *property = properties[i];
        if (property->getType() == TypeProperty::Toggle_P)
        {
            ToggleProperty *prop = reinterpret_cast<ToggleProperty *>(property);
            if (prop->getValue())
            {
                int localMask = shader->getMaskToggle(prop->getCodeData());
                maskDefineProperties = maskDefineProperties | localMask;
            }
        }
    }
}