#include "renderEngine/resources/technicalShader/technicalComputePipeline.h"

TechnicalComputePipeline::TechnicalComputePipeline() : computeProgram(NULL),
                                                       programPipeline(0),
                                                       countXGroups(1),
                                                       countYGroups(1),
                                                       countZGroups(1)
{
    glGenProgramPipelines(1, &programPipeline);
}

TechnicalComputePipeline::~TechnicalComputePipeline()
{
    glDeleteProgramPipelines(1, &programPipeline);
}

void TechnicalComputePipeline::setPipeline(TShader *_computeProgram, GLuint _countXGroups, GLuint _countYGroups, GLuint _countZGroups)
{
    computeProgram = _computeProgram;
    indexComputeProgram = computeProgram->getProgram();
    glBindProgramPipeline(programPipeline);
    glUseProgramStages(programPipeline, GL_COMPUTE_SHADER_BIT, indexComputeProgram);
    glBindProgramPipeline(0);

    countXGroups = _countXGroups;
    countYGroups = _countYGroups;
    countZGroups = _countZGroups;
}

TShader *TechnicalComputePipeline::getComputeTShader() const
{
    return computeProgram;
}

GLuint TechnicalComputePipeline::getComputeProgram() const
{
    return indexComputeProgram;
}

GLint TechnicalComputePipeline::getLocUniformCompute(const char *_name) const
{
    return glGetUniformLocation(indexComputeProgram, _name);
}

GLint TechnicalComputePipeline::getLocUniformBlockCompute(const char *_name) const
{
    return glGetUniformBlockIndex(indexComputeProgram, _name);
}

void TechnicalComputePipeline::enablePipeline() const
{
    glBindProgramPipeline(programPipeline);
}

void TechnicalComputePipeline::dispatch()
{
    glDispatchCompute(countXGroups, countYGroups, countZGroups);
}

void TechnicalComputePipeline::dispatch(GLuint _countXGroups, GLuint _countYGroups, GLuint _countZGroups)
{
    glDispatchCompute(_countXGroups, _countYGroups, _countZGroups);
}