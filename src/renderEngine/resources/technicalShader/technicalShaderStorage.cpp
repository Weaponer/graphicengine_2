#include "renderEngine/resources/technicalShader/technicalShaderStorage.h"

TechnicalShaderStorage::TechnicalShaderStorage() : shaders()
{
}

TechnicalShaderStorage::~TechnicalShaderStorage()
{
    shaders.clear();
}

bool TechnicalShaderStorage::addTechnicalShader(const char *_name, TShader *_shader)
{
    if (shaders.find(_name) != shaders.end())
    {
        return false;
    }

    shaders[_name] = _shader;
    return true;
}

bool TechnicalShaderStorage::removeTechnicalShader(const char *_name)
{
    auto found = shaders.find(_name);
    if (found == shaders.end())
    {
        return false;
    }
    shaders.erase(found);
    return true;
}

TShader *TechnicalShaderStorage::getTShader(const char *_name) const
{
    auto found = shaders.find(_name);
    if (found == shaders.end())
    {
        return NULL;
    }
    else
    {
        return (*found).second;
    }
}