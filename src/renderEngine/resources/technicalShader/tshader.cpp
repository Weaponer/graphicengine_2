#include <iostream>

#include "tshader.h"

#define _DEBUG

TShader::TShader(const char *_name, GLenum _type, const GLchar *_source) : name(_name), type(_type)
{
    program = glCreateShaderProgramv(_type, 1, &_source);

    GLint link = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &link);

    if (link == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

        char *errorLog = new char[maxLength + 1];
        errorLog[maxLength] = 0;
        glGetProgramInfoLog(program, maxLength, &maxLength, errorLog);

        if (maxLength != 0)
        {
            std::string error = std::string(errorLog);
            printf("TShader error: %s : %s\n", _name, error.c_str());
        }
        else
        {
            printf("TShader error: None define error\n");
        }
        delete[] errorLog;
        glDeleteShader(program);
        program = 0;
        return;
    }
}

TShader::~TShader()
{
    if (program != 0)
    {
        glDeleteProgram(program);
    }
}