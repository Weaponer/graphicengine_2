#include "renderEngine/resources/technicalShader/technicalProgramPipeline.h"

TechnicalProgramPipeline::TechnicalProgramPipeline() : vertexProgram(NULL),
                                                       fragmentProgram(NULL),
                                                       programPipeline(0),
                                                       indexVertexProgram(0),
                                                       indexFragmentProgram(0)
{
    glGenProgramPipelines(1, &programPipeline);
}

TechnicalProgramPipeline::~TechnicalProgramPipeline()
{
    glDeleteProgramPipelines(1, &programPipeline);
}

void TechnicalProgramPipeline::setPipeline(TShader *_vertexProgram, TShader *_fragmentProgram)
{
    vertexProgram = _vertexProgram;
    fragmentProgram = _fragmentProgram;
    indexVertexProgram = _vertexProgram->getProgram();
    indexFragmentProgram = _fragmentProgram->getProgram();
    glBindProgramPipeline(programPipeline);
    glUseProgramStages(programPipeline, GL_VERTEX_SHADER_BIT, indexVertexProgram);
    glUseProgramStages(programPipeline, GL_FRAGMENT_SHADER_BIT, indexFragmentProgram);
    glBindProgramPipeline(0);
}

TShader *TechnicalProgramPipeline::getVertexTShader() const
{
    return vertexProgram;
}

TShader *TechnicalProgramPipeline::getFragmentTShader() const
{
    return fragmentProgram;
}

GLuint TechnicalProgramPipeline::getVertexProgram() const
{
    return indexVertexProgram;
}

GLuint TechnicalProgramPipeline::getFragmentProgram() const
{
    return indexFragmentProgram;
}

GLint TechnicalProgramPipeline::getLocUniformVertex(const char *_name) const
{
    return glGetUniformLocation(indexVertexProgram, _name);
}

GLint TechnicalProgramPipeline::getLocUniformFragment(const char *_name) const
{
    return glGetUniformLocation(indexFragmentProgram, _name);
}

GLint TechnicalProgramPipeline::getLocUniformBlockVertex(const char *_name) const
{
    return glGetUniformBlockIndex(indexVertexProgram, _name);
}

GLint TechnicalProgramPipeline::getLocUniformBlockFragment(const char *_name) const
{
    return glGetUniformBlockIndex(indexFragmentProgram, _name);
}

void TechnicalProgramPipeline::enablePipeline() const
{
    glBindProgramPipeline(programPipeline);
}
