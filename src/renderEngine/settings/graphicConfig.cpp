#include "renderEngine/settings/graphicConfig.h"
#include "engineBase/configManager/configManager.h"

GraphicConfig::GraphicConfig()
{
}

GraphicConfig::~GraphicConfig()
{
}

ConfigTable GraphicConfig::getGraphicTable()
{
    return configManager->getConfigTable(GRAPHIC_CONFIG_MAINT_TABLE);
}

ConfigTable GraphicConfig::getGraphicInternalTable()
{
    return getGraphicTable().toTable(GRAPHIC_CONFIG_INTERNAL_TABLE);
}

ConfigTable GraphicConfig::getGraphicCompilerTable()
{
    return getGraphicTable().toTable(GRAPHIC_CONFIG_COMPILER_TABLE);
}

void GraphicConfig::initCategories(lua_State *L)
{
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setglobal(L, GRAPHIC_CONFIG_MAINT_TABLE);
    lua_newtable(L);
    lua_setfield(L, -2, GRAPHIC_CONFIG_COMPILER_TABLE);
    lua_newtable(L);
    lua_setfield(L, -2, GRAPHIC_CONFIG_INTERNAL_TABLE);
    lua_remove(L, -1);
}
