#include "debugOutput/debugOutput.h"
#include "debugOutput/threadDebugOutput.h"

ThreadDebugOutput *ThreadDebugOutput::mainThreadDebugOutput;

ThreadDebugOutput::ThreadDebugOutput(DebugOutput *debugOutput) : debugOutput(debugOutput)
{
    currentBuffer = new EasyStack<dataDraw>();
    nextBuffer = new EasyStack<dataDraw>();
}

ThreadDebugOutput::~ThreadDebugOutput()
{
    delete currentBuffer;
    delete nextBuffer;
}

void ThreadDebugOutput::debugLog(const char *c_str, ...)
{
    printf("Log: ");
    va_list arg;
    va_start(arg, c_str);
    vprintf(c_str, arg);
    va_end(arg);
    printf("\n");
}

void ThreadDebugOutput::debugWarning(const char *c_str, ...)
{
    printf("Warning: ");
    va_list arg;
    va_start(arg, c_str);
    vprintf(c_str, arg);
    va_end(arg);
    printf("\n");
}

void ThreadDebugOutput::debugError(const char *c_str, ...)
{
    printf("Error: ");
    va_list arg;
    va_start(arg, c_str);
    vprintf(c_str, arg);
    va_end(arg);
    printf("\n");
}

void ThreadDebugOutput::drawLine(mth::vec3 start, mth::vec3 end, mth::vec3 color, float time)
{
    mth::mat4 m;
    m.setSingleMatrix();
    pushData(start, end, m, color, dataDraw::LINE, time);
}

void ThreadDebugOutput::drawLine(mth::vec3 start, mth::vec3 end, mth::mat4 mat, mth::vec3 color, float time)
{
    pushData(start, end, mat, color, dataDraw::LINE, time);
}

void ThreadDebugOutput::drawCube(mth::vec3 pos, mth::vec3 size, mth::vec3 color, float time)
{
    mth::mat4 m;
    m.setSingleMatrix();
    pushData(pos, size, m, color, dataDraw::CUBE, time);
}

void ThreadDebugOutput::drawCube(mth::vec3 pos, mth::vec3 size, mth::mat4 mat, mth::vec3 color, float time)
{
    pushData(pos, size, mat, color, dataDraw::CUBE, time);
}

void ThreadDebugOutput::drawRect(mth::vec3 pos, mth::vec3 size, mth::vec3 color, float time)
{
    mth::mat4 m;
    m.setSingleMatrix();
    pushData(pos, size, m, color, dataDraw::RECT, time);
}

void ThreadDebugOutput::drawRect(mth::vec3 pos, mth::vec3 size, mth::mat4 mat, mth::vec3 color, float time)
{
    pushData(pos, size, mat, color, dataDraw::RECT, time);
}

void ThreadDebugOutput::swapBuffer()
{
    std::swap(currentBuffer, nextBuffer);
    nextBuffer->clearMemory();
}

const EasyStack<dataDraw> *ThreadDebugOutput::getBufferWithData()
{
    return currentBuffer;
}

void ThreadDebugOutput::discard()
{
    debugOutput->removeSubDebugOutput(this);
}

void ThreadDebugOutput::pushData(mth::vec3 &first, mth::vec3 &second, mth::mat4 &mat, mth::vec3 &color, dataDraw::TypeDraw type, float &time)
{
    dataDraw draw;
    draw.color = color;
    draw.firstV = first;
    draw.secondV = second;
    draw.mat = mat;
    draw.type = type;
    draw.time = time;
    nextBuffer->assignmentMemoryAndCopy(&draw);
}
