#include "debugOutput/debugOutput.h"

DebugOutput::DebugOutput()
{
    graphicEngine = NULL;
    debugOutputs.setUseDefault(true);
    ThreadDebugOutput *def = NULL;
    debugOutputs.setDefault(&def);

    dataDraw data;
    data.time = 0;
    data.type = dataDraw::NONE;
    data.firstV = mth::vec3();
    data.secondV = mth::vec3();
    data.color = mth::vec3();
    data.mat = mth::mat4();

    datas.setUseDefault(true);
    datas.setDefault(&data);

    mainDebugOutput = createSubDebugOutput();
}

DebugOutput::~DebugOutput()
{
    int count = debugOutputs.getPosBuffer();
    for (int i = 0; i < count; i++)
    {
        ThreadDebugOutput **mem = debugOutputs.getMemory(i);
        if (*mem != NULL)
        {
            delete (*mem);
            *mem = NULL;
        }
    }

    graphicServices.clearMemory();
    
    auto end = generatedDebugRenderers.end();
    for(auto i = generatedDebugRenderers.begin(); i != end; ++i)
    {
        graphicEngine->getMainSystems()->getSceneStructure()->removeEntityGE((*i));
        delete (*i);
    }
}

void DebugOutput::connectGraphicEngine(GraphicEngine *graphicEngine)
{
    this->graphicEngine = graphicEngine;
}

void DebugOutput::update(float timeDelta)
{
    if (graphicEngine == NULL)
    {
        return;
    }
    int pos = datas.getPosBuffer();
    for (int i = 0; i < pos; i++)
    {
        dataDraw *data = datas.getMemory(i);
        if (data->time <= 0 && data->type != dataDraw::NONE)
        {
            datas.popMemory(i);
        }
        else if (data->time > 0)
        {
            data->time -= timeDelta;
        }
    }

    pos = debugOutputs.getPosBuffer();
    for (int i = 0; i < pos; i++)
    {
        ThreadDebugOutput *debugOutput = *debugOutputs.getMemory(i);
        if (debugOutput == NULL)
        {
            continue;
        }

        debugOutput->swapBuffer();
        const EasyStack<dataDraw> *data = debugOutput->getBufferWithData();
        int p2 = data->getPos();
        for (int i2 = 0; i2 < p2; i2++)
        {
            dataDraw draw = *data->getMemory(i2);
            sizeM index = 0;
            datas.assignmentMemoryAndCopy(&draw, index);
        }
    }

    clearAllDataInServices();

    int currentServiceNum = 0;
    DebugRendererGE *currentService = NULL;
    if (graphicServices.getPos() == 0)
    {
        currentService = pushNewGraphicServices();
        currentServiceNum++;
    }
    else
    {
        currentService = *graphicServices.getMemory(currentServiceNum);
        currentServiceNum++;
    }

    int countData = datas.getPosBuffer();
    for (int i = 0; i < countData; i++)
    {
        dataDraw *draw = datas.getMemory(i);
        if (draw->type == dataDraw::NONE)
        {
            continue;
        }
        bool result = pushDataInService(draw, currentService);
        if (result == true)
        {
            continue;
        }
        else
        {
            if (currentServiceNum < (int)(graphicServices.getPos()))
            {
                currentService = *graphicServices.getMemory(currentServiceNum);
                currentServiceNum++;
            }
            else
            {
                currentService = pushNewGraphicServices();
                currentServiceNum++;
            }
            pushDataInService(draw, currentService);
        }
    }
}

void DebugOutput::clearAllDataInServices()
{
    int pos = graphicServices.getPos();
    for (int i = 0; i < pos; i++)
    {
        (*graphicServices.getMemory(i))->clear();
    }
}

DebugRendererGE *DebugOutput::pushNewGraphicServices()
{
    DebugRendererGE *newDebugRenderer = new DebugRendererGE();
    generatedDebugRenderers.push_back(newDebugRenderer);
    newDebugRenderer->typeEntity = TypeGraphicEntity::DEBUG_RENDER_GRAPHIC_ENTITY;
    graphicEngine->getMainSystems()->getSceneStructure()->addEntityGE(TypeGraphicEntity::DEBUG_RENDER_GRAPHIC_ENTITY, newDebugRenderer);

    graphicServices.assignmentMemoryAndCopy(&newDebugRenderer);
    return newDebugRenderer;
}

bool DebugOutput::pushDataInService(dataDraw *data, DebugRendererGE *service)
{
    if (data->type == dataDraw::LINE)
    {
        if (service->setColor(data->color))
        {
            service->setLocalMatrix(data->mat);
            if (service->drawLine(data->firstV, data->secondV))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    else if (data->type == dataDraw::CUBE)
    {
        if (service->setColor(data->color))
        {
            service->setLocalMatrix(data->mat);
            if (service->drawCube(data->firstV, data->secondV))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        if (service->setColor(data->color))
        {
            service->setLocalMatrix(data->mat);
            if (service->drawRect(data->firstV, mth::vec2(data->secondV.x, data->secondV.y)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}

ThreadDebugOutput *DebugOutput::createSubDebugOutput()
{
    ThreadDebugOutput *debugOutput = new ThreadDebugOutput(this);
    sizeM index = 0;
    debugOutputs.assignmentMemoryAndCopy(&debugOutput, index);
    return debugOutput;
}

void DebugOutput::removeSubDebugOutput(ThreadDebugOutput *subDebugOutput)
{
    int pos = debugOutputs.getPosBuffer();
    for (int i = 0; i < pos; i++)
    {
        ThreadDebugOutput **mem = debugOutputs.getMemory(i);
        if ((*mem) != NULL && (*mem) == subDebugOutput)
        {
            delete subDebugOutput;
            (*mem) = NULL;
            return;
        }
    }
}