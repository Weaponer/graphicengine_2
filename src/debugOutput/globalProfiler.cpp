#include <SDL2/SDL.h>
#include <chrono>

#include "debugOutput/globalProfiler.h"

std::map<std::string, GlobalProfiler::dataSamples *> GlobalProfiler::data = std::map<std::string, GlobalProfiler::dataSamples *>();

void GlobalProfiler::startSample(const char *_name)
{
    if(data.count(_name) != 0)
    {
        GlobalProfiler::dataSamples *sample = data.at(_name);
        if(sample->isStart)
        {
            return;
        }
        else
        {
            sample->isStart = true;        
            sample->startTime = std::chrono::high_resolution_clock::now();
        }
    }
    else
    {
        GlobalProfiler::dataSamples *sample = new GlobalProfiler::dataSamples();
        sample->name = _name;
        sample->allTime = 0;
        sample->countSamples = 0;
        sample->isStart = true;
        sample->startTime = std::chrono::high_resolution_clock::now();

        data[_name] = sample;
    }
}

float GlobalProfiler::stopSample(const char *_name)
{
    if(data.count(_name) == 0)
    {
        return 0.0f;
    }

    GlobalProfiler::dataSamples *sample = data.at(_name);
    if(sample->isStart)
    {
        sample->isStart = false;
        auto end = std::chrono::high_resolution_clock::now();
        float deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(end - sample->startTime).count() / 1000000.0f;
        sample->countSamples++;
        sample->allTime += deltaTime;
        return deltaTime;
    }
    return 0.0f;
}

void GlobalProfiler::writeResultInFile(const char *_path)
{
    SDL_RWops *file = SDL_RWFromFile(_path, "w");
    if(file == NULL)
    {
        return;
    }

    for(auto i = data.begin(); i != data.end(); ++i)
    {
        std::pair<std::string, GlobalProfiler::dataSamples *> p = *i;
        std::string line = p.first + " : " + std::to_string(p.second->allTime / p.second->countSamples) + " \n";
        file->write(file, line.c_str(), sizeof(char), line.length());
    } 

    SDL_RWclose(file);
}