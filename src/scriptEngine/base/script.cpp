#include <stdlib.h>
#include <string.h>

#include "scriptEngine/base/script.h"

Script::Script(const char *_save_path, char *_no_save_text)
{
    path = (char *)malloc(strlen(_save_path) + 1);
    strcpy(path, _save_path);
    text = _no_save_text;
}

Script::~Script()
{
    free(path);
    free(text);
}