#include "scriptEngine/base/scriptEvents.h"
#include "scriptEngine/base/scriptEngine.h"

void ScriptEvents::onRegisterAnyObject(Object *obj) 
{

}

void ScriptEvents::onRemoveAnyObject(Object *obj)
{
    engine->onRemoveObject(obj);
}