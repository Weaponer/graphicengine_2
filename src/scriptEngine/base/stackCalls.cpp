#include "scriptEngine/base/stackCalls.h"
#include "scriptEngine/base/scriptEngine.h"

StackCalls::StackCalls()
{
    calls.setCapacity(2);
}

StackCalls::~StackCalls()
{
}

void StackCalls::callMethod(ObjectScriptData *osd, const char *method)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, osd->getPointData());
    lua_getfield(L, -1, method);
    
    if(lua_type(L, -1) != LUA_TFUNCTION)
    {
        lua_settop(L, -3);
        return;
    }
    call c;
    c.osd = osd;
    c.nameMethod = method;
    calls.assignmentMemoryAndCopy(&c);
    lua_pushnil(L);
    lua_copy(L, -3, -1);
    lua_remove(L, -3);
    int error = lua_pcall(L, 1, 0, 0);
    if(error)
    {
        printf("Method %s error: %s\n", method, lua_tostring(L, -1));
        lua_settop(L, -2);
    }
    calls.popMemory();
}
