#include "scriptEngine/base/scriptEngine.h"
#include "objectRegister/reservedCodes.h"
#include "scriptEngine/base/scriptEngineDefinitions.h"

#include "scriptEngine/libs/luaOverloadNil.h"
#include "scriptEngine/libs/luaLayer/luaLayer.h"

#include "scriptEngine/libs/luaGameObject.h"
#include "scriptEngine/libs/luaVector.h"
#include "scriptEngine/libs/luaColor.h"
#include "scriptEngine/libs/luaQuaternion.h"
#include "scriptEngine/libs/luaMatrix2.h"
#include "scriptEngine/libs/luaMatrix3.h"
#include "scriptEngine/libs/luaMatrix4.h"

#include "scriptEngine/libs/luaResources/luaResources.h"
#include "scriptEngine/libs/luaComponents/luaAbstractComponent.h"

#include "scriptEngine/libs/luaInput/luaInput.h"
#include "scriptEngine/libs/luaScreen/luaScreen.h"
#include <assert.h>

#include "scriptEngine/libs/luaTime/luaTime.h"
#include "scriptEngine/libs/luaGraphic/luaGraphic.h"

#include "scriptEngine/libs/luaDebug/luaDebug.h"
#include "scriptEngine/libs/luaSceneManager/luaSceneManager.h"

#include "scriptEngine/libs/luaArrays/luaArrays.h"


ScriptEngine::ScriptEngine(ObjectRegister *objectRegiser, ResourceLoader *resourceLoader, SpecialSystems *specialSystems)
    : resourceLoader(resourceLoader), specialSystems(specialSystems), scriptInstaller(), stackCalls(), 
    receivingEvents(this), objectRegiser(objectRegiser)
       
{
    L = luaL_newstate();
    lua_pushlightuserdata(L, this);
    int pos = luaL_ref(L, LUA_REGISTRYINDEX);
    assert(pos == SCRIPT_ENGINE_INDEX_IN_REGISTER);

    luaL_openlibs(L);
    scriptInstaller.initScriptInstaller(L);

    LuaLayer::luaLoadLayer(L);
    LuaGameObject::luaLoadGameObject(L);
    LuaColor::luaLoadColor(L);
    LuaVector::luaLoadVector(L);
    LuaQuaternion::luaLoadQuaternion(L);
    LuaMatrix2::luaLoadMatrix2(L);
    LuaMatrix3::luaLoadMatrix3(L);
    LuaMatrix4::luaLoadMatrix4(L);
    LuaResources::luaLoadResources(L);
    LuaAbstractComponent::luaLoadAllComponents(L);

    LuaInput::luaLoadInput(L);
    LuaScreen::luaLoadScreen(L);
    LuaTime::luaLoadTime(L);
    LuaGraphic::luaLoadGraphic(L);
    LuaDebug::luaLoadDebug(L);
    LuaSceneManager::luaLoadSceneManager(L);
    LuaOverloadNil::luaOverloadNil(L);
    LuaArrays::luaLoadArrays(L);

    stackCalls.setLuaState(L);

    //stackDump(L);
    objectRegiser->setPointEvents(&receivingEvents);
}

ScriptEngine::~ScriptEngine()
{
    objectRegiser->discardPointEvents();
    lua_close(L);
    L = NULL;
}

void ScriptEngine::update()
{
    for (int i = 0; i < (int)osds.getPosBuffer(); i++)
    {
        ObjectScriptData *osd = *osds.getMemory(i);
        if (osd != NULL && osd->getEnable() && osd->isCalledStart() == false)
        {
            osd->setCallStart();
            stackCalls.callMethod(osd, ON_ENABLE_METHOD_NAME);
            stackCalls.callMethod(osd, START_METHOD_NAME);
        }
    }

    for (int i = 0; i < (int)osds.getPosBuffer(); i++)
    {
        ObjectScriptData *osd = *osds.getMemory(i);
        if (osd != NULL && osd->getEnable())
        {
            stackCalls.callMethod(osd, UPDATE_METHOD_NAME);
        }
    }
}

void ScriptEngine::lateUpdate()
{
    for (int i = 0; i < (int)osds.getPosBuffer(); i++)
    {
        ObjectScriptData *osd = *osds.getMemory(i);
        if (osd != NULL && osd->getEnable())
        {
            stackCalls.callMethod(osd, LATE_UPDATE_METHOD_NAME);
        }
    }
}

void ScriptEngine::onRemoveObject(Object *obj)
{
    removeObjectRepresentative(obj);
}

luaObject *ScriptEngine::getObjectRepresentative(Object *obj)
{
    void *data = objectRegiser->getAdditionalData(obj, ReservedCodes::SCRIPT_DATA);
    if (data != NULL)
    {
        return (luaObject *)data;
    }
    return NULL;
}

luaObject *ScriptEngine::createUDataForObject(Object *obj, const char *metadata)
{
    removeObjectRepresentative(obj);

    luaObject *lObj = (luaObject *)lua_newuserdata(L, sizeof(luaObject));
    luaL_getmetatable(L, metadata);
    lua_setmetatable(L, -2);
    int newCod = luaL_ref(L, LUA_REGISTRYINDEX);
    lObj->ref = newCod;
    lObj->object = obj;
    objectRegiser->addSetAdditionalData(obj, ReservedCodes::SCRIPT_DATA, lObj);
    return lObj;
}

void ScriptEngine::removeObjectRepresentative(Object *obj)
{
    luaObject *curLO = getObjectRepresentative(obj);
    if (curLO != NULL)
    {
        objectRegiser->removeAdditionalData(obj, ReservedCodes::SCRIPT_DATA);
        lua_rawgeti(L, LUA_REGISTRYINDEX, curLO->ref);
        LuaOverloadNil::luaMakeMetadataNil(L, -1);
        lua_remove(L, -1);
        luaL_unref(L, LUA_REGISTRYINDEX, curLO->ref);
        curLO->object = NULL;
        curLO->ref = -1;
    }
}

ObjectScriptData *ScriptEngine::newObjectScriptData(const char *type, Component *component)
{
    if (scriptInstaller.pushNewInstanceType(L, type, component))
    {
        return NULL;
    }
    sizeM index;
    ObjectScriptData **osd = osds.assignmentMemory(index);
    *osd = new ObjectScriptData(index);
    (*osd)->setPointData(luaL_ref(L, LUA_REGISTRYINDEX));
    (*osd)->setGameObject(component->getGameObject());
    int refToObject = LuaGameObject::getGameObject(L, (*osd)->getGameObject());
    printf("---- %i\n", refToObject);
    return *osd;
}

void ScriptEngine::removeObjectScriptData(ObjectScriptData *osd)
{
    stackCalls.callMethod(osd, ON_DISCARD_METHOD_NAME);

    //set tabels in nil
    lua_rawgeti(L, LUA_REGISTRYINDEX, osd->getPointData());
    if (lua_type(L, -1) == LUA_TTABLE)
    {
        while (true)
        {
            lua_getmetatable(L, -1);
            LuaOverloadNil::luaMakeMetadataNil(L, -2);
            lua_remove(L, -2);
            lua_getfield(L, -1, "__index");
            lua_remove(L, -2);
            if (lua_type(L, -1) == LUA_TUSERDATA)
            {
                break;
            }
        }
    }
    lua_remove(L, -1);
    //
    luaL_unref(L, LUA_REGISTRYINDEX, osd->getPointData());
    *osds.getMemory(osd->getIndexMemory()) = NULL;
    osds.popMemory(osd->getIndexMemory());
    delete osd;
}

void ScriptEngine::callMethod(ObjectScriptData *osd, const char *_name)
{
    stackCalls.callMethod(osd, _name);
}

void ScriptEngine::addScript(const char *pathToScript)
{
    Script *foundScript = findScript(pathToScript);
    if (foundScript == NULL)
    {
        foundScript = loadScript(pathToScript);
        if (foundScript == NULL)
        {
            return;
        }
    }
    int resultErrorInstal = scriptInstaller.instalScript(L, foundScript);
    if (resultErrorInstal)
    {
        unloadScript(foundScript);
        return;
    }
    uint index = 0;
    scripts.assignmentMemoryAndCopy(&foundScript, index);
}

Script *ScriptEngine::findScript(const char *path)
{
    int len = (int)scripts.getPosBuffer();
    for (int i = 0; i < len; i++)
    {
        Script **script = scripts.getMemory(i);
        if (*script != NULL && strcmp((*script)->getPath(), path) == 0)
        {
            return *script;
        }
    }
    return NULL;
}

Script *ScriptEngine::loadScript(const char *path)
{
    Script *script = (Script *)resourceLoader->loadResource(path, TypeResource::SCRIPT);
    if (script != NULL)
    {
        return script;
    }
    else
    {
        return NULL;
    }
}

void ScriptEngine::unloadScript(Script *script)
{
    resourceLoader->unloadResource(script);
}

void ScriptEngine::stackDump(lua_State *L)
{
    printf("----stack dump----\n");
    int top = lua_gettop(L);
    for (int i = 1; i <= top; i++)
    {
        int type = lua_type(L, i);
        if (LUA_TSTRING == type)
        {
            printf("%s", lua_tostring(L, i));
        }
        else if (LUA_TBOOLEAN == type)
        {
            printf(lua_toboolean(L, i) ? "true" : "false");
        }
        else if (LUA_TNUMBER == type)
        {
            printf("%g", lua_tonumber(L, i));
        }
        else
        {
            printf("%s", lua_typename(L, type));
        }
        printf("\n");
    }
    printf("\n");
}