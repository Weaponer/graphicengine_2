#include "scriptEngine/base/scriptInstaller.h"
#include "scriptEngine/base/scriptEngine.h"
#include "scriptEngine/base/scriptEngineDefinitions.h"

#include "scriptEngine/libs/luaComponents/luaScriptComponent.h"
#include "scriptEngine/libs/luaGameObject.h"

void ScriptInstaller::initScriptInstaller(lua_State *L)
{
    lua_newtable(L);
    pointTableTypes = luaL_ref(L, LUA_REGISTRYINDEX);
}

int ScriptInstaller::instalScript(lua_State *L, Script *script)
{
    int error = luaL_loadstring(L, script->getText());
    if (error)
    {
        printf("Script installer: %s: %s\n", script->getPath(), lua_tostring(L, -1));
        lua_remove(L, -1);
        return ERROR_NO_COMPILATION;
    }
    if (lua_type(L, -1) != LUA_TFUNCTION)
    {
        lua_remove(L, -1);
        return ERROR_NO_COMPILATION;
    }
    lua_pushvalue(L, -1);

    error = lua_pcall(L, 0, 1, 0);
    if (error)
    {
        printf("Script installer: %s: %s\n", script->getPath(), lua_tostring(L, -1));
        lua_settop(L, -2);
        return ERROR_NO_COMPILATION;
    }

    if (lua_type(L, -1) != LUA_TTABLE)
    {
        lua_settop(L, -2);
        return 0;
    }

    // first function generation
    // second table object
    lua_getfield(L, -1, INDEX_TYPE);      // thirth - type name
    lua_getfield(L, -2, INDEX_PROTOTYPE); // fourth - prototype name

    if (lua_type(L, -2) != LUA_TSTRING)
    {
        lua_settop(L, -4);
        printf("Script installer: %s: table not have type.\n", script->getPath());
        return ERROR_NO_COMPILATION;
    }
    else if (lua_type(L, -1) != LUA_TSTRING)
    {
        lua_settop(L, -4);
        printf("Script installer: %s: table not have prototype.\n", script->getPath());
        return ERROR_NO_COMPILATION;
    }

    lua_rawgeti(L, LUA_REGISTRYINDEX, pointTableTypes);
    lua_insert(L, -5);
    lua_newtable(L);
    lua_insert(L, -5);
    lua_setfield(L, -5, INDEX_PROTOTYPE);
    lua_pushvalue(L, -1);
    lua_insert(L, -5);
    lua_setfield(L, -4, INDEX_TYPE);
    lua_remove(L, -1);
    lua_setfield(L, -2, INDEX_FUNCTION_GENERATION);
    lua_settable(L, -3);
    lua_remove(L, -1);
    //ScriptEngine::stackDump(L);
    return 0;
}

int ScriptInstaller::pushNewInstanceType(lua_State *L, const char *name_type, Component *component)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, pointTableTypes);
    lua_getfield(L, -1, name_type);
    if (lua_type(L, -1) == LUA_TNIL)
    {
        lua_settop(L, -3);
        printf("Generate new instance type: I haven't this type:%s.\n", name_type);
        return 1;
    }
    lua_getfield(L, -1, INDEX_PROTOTYPE);
    lua_getfield(L, -2, INDEX_FUNCTION_GENERATION);
    lua_remove(L, -3);
    lua_remove(L, -3);
    lua_pcall(L, 0, 1, 0);
    const char *prototype = lua_tostring(L, -2);
    if (strcmp(prototype, BASIC_TYPE) == 0)
    {
        pushBasicType(L, component);
    }
    else
    {
        if (pushNewInstanceType(L, prototype, component))
        {
            lua_settop(L, -3);
            return 1;
        }
    }
    lua_remove(L, -3);
    lua_newtable(L);
    lua_insert(L, -2);
    lua_setfield(L, -2, "__index");
    lua_setmetatable(L, -2);
    lua_rawgeti(L, LUA_REGISTRYINDEX, LuaGameObject::getGameObject(L, component->getGameObject()));
    lua_setfield(L, -2, INDEX_OBJECT);
    printf("-----------------\n");
    //ScriptEngine::stackDump(L);
    return 0;
}

int ScriptInstaller::pushBasicType(lua_State *L, Component *component)
{
    LuaScriptComponent::pushScripComponent(L, component);
    return 0;
}