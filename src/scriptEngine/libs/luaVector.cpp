#include "scriptEngine/libs/luaVector.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

void LuaVector::luaLoadVector(lua_State *L)
{
    // Vector4
    luaL_Reg metaFuncsVec4[] =
        {
            {"__add", LuaVector::addVector4},
            {"__sub", LuaVector::subVector4},
            {"__mul", LuaVector::mulVector4},
            {"__div", LuaVector::divVector4},
            {"__eq", LuaVector::equality4},
            {"__tostring", LuaVector::toString4},
            {"__len", LuaVector::magnitude4},
            {"magnitude", LuaVector::magnitude4},
            {"normalize", LuaVector::normalize4},
            {"x", LuaVector::x4},
            {"y", LuaVector::y4},
            {"z", LuaVector::z4},
            {"w", LuaVector::w4},
            {"setX", LuaVector::setX4},
            {"setY", LuaVector::setY4},
            {"setZ", LuaVector::setZ4},
            {"setW", LuaVector::setW4},
            {NULL, NULL}};

    BasicFunctions::createLibUserdata(L, VECTOR_4_METADATA, metaFuncsVec4);

    luaL_Reg vec4Funcs[] =
        {
            {"new", LuaVector::newVector4},
            {"dot", LuaVector::dotVector4},
            {"normalized", LuaVector::normalizedVector4},
            {NULL, NULL}};
    BasicFunctions::createStaticLib(L, "Vector4", vec4Funcs);

    // Vector3
    luaL_Reg metaFuncs[] =
        {
            {"__add", LuaVector::addVector3},
            {"__sub", LuaVector::subVector3},
            {"__mul", LuaVector::mulVector3},
            {"__div", LuaVector::divVector3},
            {"__eq", LuaVector::equality3},
            {"__tostring", LuaVector::toString3},
            {"__len", LuaVector::magnitude3},
            {"magnitude", LuaVector::magnitude3},
            {"normalize", LuaVector::normalize3},
            {"x", LuaVector::x3},
            {"y", LuaVector::y3},
            {"z", LuaVector::z3},
            {"setX", LuaVector::setX3},
            {"setY", LuaVector::setY3},
            {"setZ", LuaVector::setZ3},
            {NULL, NULL}};

    BasicFunctions::createLibUserdata(L, VECTOR_3_METADATA, metaFuncs);

    luaL_Reg vec3Funcs[] =
        {
            {"new", LuaVector::newVector3},
            {"dot", LuaVector::dotVector3},
            {"cross", LuaVector::crossVector3},
            {"normalized", LuaVector::normalizedVector3},
            {NULL, NULL}};
    BasicFunctions::createStaticLib(L, "Vector3", vec3Funcs);

    // Vector2
    luaL_Reg metaFuncsVec2[] =
        {
            {"__add", LuaVector::addVector2},
            {"__sub", LuaVector::subVector2},
            {"__mul", LuaVector::mulVector2},
            {"__div", LuaVector::divVector2},
            {"__eq", LuaVector::equality2},
            {"__tostring", LuaVector::toString2},
            {"__len", LuaVector::magnitude2},
            {"magnitude", LuaVector::magnitude2},
            {"normalize", LuaVector::normalize2},
            {"x", LuaVector::x2},
            {"y", LuaVector::y2},
            {"setX", LuaVector::setX2},
            {"setY", LuaVector::setY2},
            {NULL, NULL}};
    BasicFunctions::createLibUserdata(L, VECTOR_2_METADATA, metaFuncsVec2);

    luaL_Reg vec2Funcs[] =
        {
            {"new", LuaVector::newVector2},
            {"dot", LuaVector::dotVector2},
            {"perpendecular", LuaVector::perpendecular2},
            {"normalized", LuaVector::normalizedVector2},
            {NULL, NULL}};
    BasicFunctions::createStaticLib(L, "Vector2", vec2Funcs);
}

void LuaVector::pushVector4(lua_State *L, const mth::vec4 &vec)
{
    BasicFunctions::pushTObject<mth::vec4>(L, VECTOR_4_METADATA, vec);
}

void LuaVector::pushVector3(lua_State *L, const mth::vec3 &vec)
{
    BasicFunctions::pushTObject<mth::vec3>(L, VECTOR_3_METADATA, vec);
}

void LuaVector::pushVector2(lua_State *L, const mth::vec2 &vec)
{
    BasicFunctions::pushTObject<mth::vec2>(L, VECTOR_2_METADATA, vec);
}

int LuaVector::newVector4(lua_State *L)
{
    int count = lua_gettop(L);
    float x = 0;
    float y = 0;
    float z = 0;
    float w = 0;
    if (count >= 4)
    {
        x = luaL_checknumber(L, 1);
        y = luaL_checknumber(L, 2);
        z = luaL_checknumber(L, 3);
        w = luaL_checknumber(L, 4);
    }
    else if (count >= 1)
    {
        if (lua_type(L, 1) == LUA_TUSERDATA)
        {
            mth::vec4 *vec = CHECK_VECTOR_4(L, 1);
            if (vec == NULL)
            {
                return 0;
            }
            else
            {
                x = vec->x;
                y = vec->y;
                z = vec->z;
                w = vec->w;
            }
        }
        else
        {
            z = y = x = w = luaL_checknumber(L, 1);
        }
    }

    pushVector4(L, mth::vec4(x, y, z, w));
    return 1;
}

int LuaVector::dotVector4(lua_State *L)
{
    mth::vec4 *oneVec = (mth::vec4 *)luaL_checkudata(L, 1, VECTOR_4_METADATA);
    mth::vec4 *twoVec = (mth::vec4 *)luaL_checkudata(L, 2, VECTOR_4_METADATA);
    if (oneVec == NULL || twoVec == NULL)
    {
        return 0;
    }
    lua_pushnumber(L, (*oneVec) * (*twoVec));
    return 1;
}

int LuaVector::normalizedVector4(lua_State *L)
{
    mth::vec4 *oneVec = CHECK_VECTOR_4(L, 1);
    if (oneVec == NULL)
    {
        return 0;
    }
    mth::vec4 vec = *oneVec;
    vec.normalise();
    pushVector4(L, vec);
    return 1;
}

int LuaVector::x4(lua_State *L)
{
    mth::vec4 *vec = CHECK_VECTOR_4(L, 1);
    lua_pushnumber(L, vec->x);
    return 1;
}

int LuaVector::y4(lua_State *L)
{
    mth::vec4 *vec = CHECK_VECTOR_4(L, 1);
    lua_pushnumber(L, vec->y);
    return 1;
}

int LuaVector::z4(lua_State *L)
{
    mth::vec4 *vec = CHECK_VECTOR_4(L, 1);
    lua_pushnumber(L, vec->z);
    return 1;
}

int LuaVector::w4(lua_State *L)
{
    mth::vec4 *vec = CHECK_VECTOR_4(L, 1);
    lua_pushnumber(L, vec->w);
    return 1;
}

int LuaVector::setX4(lua_State *L)
{
    mth::vec4 *vec = CHECK_VECTOR_4(L, 1);
    float v = lua_tonumber(L, 2);
    (*vec).x = v;
    return 0;
}

int LuaVector::setY4(lua_State *L)
{
    mth::vec4 *vec = CHECK_VECTOR_4(L, 1);
    float v = lua_tonumber(L, 2);
    (*vec).y = v;
    return 0;
}

int LuaVector::setZ4(lua_State *L)
{
    mth::vec4 *vec = CHECK_VECTOR_4(L, 1);
    float v = lua_tonumber(L, 2);
    (*vec).z = v;
    return 0;
}

int LuaVector::setW4(lua_State *L)
{
    mth::vec4 *vec = CHECK_VECTOR_4(L, 1);
    float v = lua_tonumber(L, 2);
    (*vec).w = v;
    return 0;
}

int LuaVector::addVector4(lua_State *L)
{
    mth::vec4 *oneVec = (mth::vec4 *)luaL_checkudata(L, 1, VECTOR_4_METADATA);
    mth::vec4 *twoVec = (mth::vec4 *)luaL_checkudata(L, 2, VECTOR_4_METADATA);
    if (oneVec == NULL || twoVec == NULL)
    {
        return 0;
    }
    pushVector4(L, (*oneVec) + (*twoVec));
    return 1;
}

int LuaVector::subVector4(lua_State *L)
{
    mth::vec4 *oneVec = (mth::vec4 *)luaL_checkudata(L, 1, VECTOR_4_METADATA);
    mth::vec4 *twoVec = (mth::vec4 *)luaL_checkudata(L, 2, VECTOR_4_METADATA);
    if (oneVec == NULL || twoVec == NULL)
    {
        return 0;
    }
    pushVector4(L, (*oneVec) - (*twoVec));
    return 1;
}

int LuaVector::mulVector4(lua_State *L)
{
    mth::vec4 *oneVec = (mth::vec4 *)luaL_checkudata(L, 2, VECTOR_4_METADATA);
    float num = luaL_checknumber(L, 1);
    if (oneVec == NULL)
    {
        return 0;
    }
    pushVector4(L, (*oneVec) * num);
    return 1;
}

int LuaVector::divVector4(lua_State *L)
{
    mth::vec4 *oneVec = (mth::vec4 *)luaL_checkudata(L, 2, VECTOR_4_METADATA);
    float num = luaL_checknumber(L, 1);
    if (oneVec == NULL)
    {
        return 0;
    }
    pushVector4(L, (*oneVec) / num);
    return 1;
}

int LuaVector::toString4(lua_State *L)
{
    mth::vec4 *vec = CHECK_VECTOR_4(L, 1);
    lua_pushfstring(L, "(%f, %f, %f, %f)", vec->x, vec->y, vec->z, vec->w);
    return 1;
}

int LuaVector::magnitude4(lua_State *L)
{
    mth::vec4 *vec = CHECK_VECTOR_4(L, 1);
    float mag = vec->magnitude();
    lua_pushnumber(L, mag);
    return 1;
}

int LuaVector::normalize4(lua_State *L)
{
    mth::vec4 *vec = CHECK_VECTOR_4(L, 1);
    vec->normalise();
    return 0;
}

int LuaVector::equality4(lua_State *L)
{
    mth::vec4 *oneVec = (mth::vec4 *)luaL_checkudata(L, 1, VECTOR_4_METADATA);
    mth::vec4 *twoVec = (mth::vec4 *)luaL_checkudata(L, 2, VECTOR_4_METADATA);
    if (oneVec == NULL || twoVec == NULL)
    {
        return 0;
    }
    if ((*oneVec) == (*twoVec))
    {
        lua_pushboolean(L, true);
    }
    else
    {
        lua_pushboolean(L, false);
    }
    return 1;
}

int LuaVector::newVector3(lua_State *L)
{
    int count = lua_gettop(L);
    float x = 0;
    float y = 0;
    float z = 0;
    if (count >= 3)
    {
        x = luaL_checknumber(L, 1);
        y = luaL_checknumber(L, 2);
        z = luaL_checknumber(L, 3);
    }
    else if (count >= 1)
    {
        if (lua_type(L, 1) == LUA_TUSERDATA)
        {
            mth::vec3 *vec = CHECK_VECTOR_3(L, 1);
            if (vec == NULL)
            {
                return 0;
            }
            else
            {
                x = vec->x;
                y = vec->y;
                z = vec->z;
            }
        }
        else
        {
            z = y = x = luaL_checknumber(L, 1);
        }
    }

    pushVector3(L, mth::vec3(x, y, z));
    return 1;
}

int LuaVector::dotVector3(lua_State *L)
{
    mth::vec3 *oneVec = (mth::vec3 *)luaL_checkudata(L, 1, VECTOR_3_METADATA);
    mth::vec3 *twoVec = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    if (oneVec == NULL || twoVec == NULL)
    {
        return 0;
    }
    lua_pushnumber(L, (*oneVec) * (*twoVec));
    return 1;
}

int LuaVector::crossVector3(lua_State *L)
{
    mth::vec3 *oneVec = (mth::vec3 *)luaL_checkudata(L, 1, VECTOR_3_METADATA);
    mth::vec3 *twoVec = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    if (oneVec == NULL || twoVec == NULL)
    {
        return 0;
    }
    pushVector3(L, oneVec->componentProduct((*twoVec)));
    return 1;
}

int LuaVector::normalizedVector3(lua_State *L)
{
    mth::vec3 *oneVec = CHECK_VECTOR_3(L, 1);
    if (oneVec == NULL)
    {
        return 0;
    }
    mth::vec3 vec = *oneVec;
    vec.normalise();
    pushVector3(L, vec);
    return 1;
}

int LuaVector::x3(lua_State *L)
{
    mth::vec3 *vec = CHECK_VECTOR_3(L, 1);
    lua_pushnumber(L, vec->x);
    return 1;
}

int LuaVector::y3(lua_State *L)
{
    mth::vec3 *vec = CHECK_VECTOR_3(L, 1);
    lua_pushnumber(L, vec->y);
    return 1;
}

int LuaVector::z3(lua_State *L)
{
    mth::vec3 *vec = CHECK_VECTOR_3(L, 1);
    lua_pushnumber(L, vec->z);
    return 1;
}

int LuaVector::setX3(lua_State *L)
{
    if (lua_gettop(L) >= 2)
    {
        mth::vec3 *vec = CHECK_VECTOR_3(L, 1);
        float v = lua_tonumber(L, 2);
        (*vec).x = v;
    }
    else
    {
        luaL_error(L, "setX: not enough arguments.");
    }
    return 0;
}

int LuaVector::setY3(lua_State *L)
{
    if (lua_gettop(L) >= 2)
    {
        mth::vec3 *vec = CHECK_VECTOR_3(L, 1);
        float v = luaL_checknumber(L, 2);
        (*vec).y = v;
    }
    else
    {
        luaL_error(L, "setY: not enough arguments.");
    }
    return 0;
}

int LuaVector::setZ3(lua_State *L)
{
    if (lua_gettop(L) >= 2)
    {
        mth::vec3 *vec = CHECK_VECTOR_3(L, 1);
        float v = lua_tonumber(L, 2);
        (*vec).z = v;
    }
    else
    {
        luaL_error(L, "setZ: not enough arguments.");
    }
    return 0;
}

int LuaVector::toString3(lua_State *L)
{
    mth::vec3 *vec = CHECK_VECTOR_3(L, 1);
    if (vec != NULL)
    {
        lua_pushfstring(L, "(%f, %f, %f)", vec->x, vec->y, vec->z);
        return 1;
    }
    return 0;
}

int LuaVector::addVector3(lua_State *L)
{
    mth::vec3 *oneVec = (mth::vec3 *)luaL_checkudata(L, 1, VECTOR_3_METADATA);
    mth::vec3 *twoVec = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    if (oneVec == NULL || twoVec == NULL)
    {
        return 0;
    }
    pushVector3(L, (*oneVec) + (*twoVec));
    return 1;
}

int LuaVector::subVector3(lua_State *L)
{
    mth::vec3 *oneVec = (mth::vec3 *)luaL_checkudata(L, 1, VECTOR_3_METADATA);
    mth::vec3 *twoVec = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    if (oneVec == NULL || twoVec == NULL)
    {
        return 0;
    }
    pushVector3(L, (*oneVec) - (*twoVec));
    return 1;
}

int LuaVector::mulVector3(lua_State *L)
{
    mth::vec3 *oneVec = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    float num = luaL_checknumber(L, 1);
    if (oneVec == NULL)
    {
        return 0;
    }
    pushVector3(L, (*oneVec) * num);
    return 1;
}

int LuaVector::divVector3(lua_State *L)
{
    mth::vec3 *oneVec = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    float num = luaL_checknumber(L, 1);
    if (oneVec == NULL)
    {
        return 0;
    }
    if (num == 0)
    {
        luaL_error(L, "operation /: found division on zero.");
        return 0;
    }
    pushVector3(L, (*oneVec) / num);
    return 1;
}

int LuaVector::equality3(lua_State *L)
{
    mth::vec3 *oneVec = (mth::vec3 *)luaL_checkudata(L, 1, VECTOR_3_METADATA);
    mth::vec3 *twoVec = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    if (oneVec == NULL || twoVec == NULL)
    {
        return 0;
    }
    if ((*oneVec) == (*twoVec))
    {
        lua_pushboolean(L, true);
        return 1;
    }
    else
    {
        lua_pushboolean(L, false);
        return 1;
    }
}

int LuaVector::magnitude3(lua_State *L)
{
    mth::vec3 *vec = CHECK_VECTOR_3(L, 1);
    float mag = vec->magnitude();
    lua_pushnumber(L, mag);
    return 1;
}

int LuaVector::normalize3(lua_State *L)
{
    mth::vec3 *vec = CHECK_VECTOR_3(L, 1);
    vec->normalise();
    return 0;
}

// vec2 ---

int LuaVector::newVector2(lua_State *L)
{
    int count = lua_gettop(L);
    float x = 0;
    float y = 0;
    if (count >= 2)
    {
        x = luaL_checknumber(L, 1);
        y = luaL_checknumber(L, 2);
    }
    else if (count >= 1)
    {
        if (lua_type(L, 1) == LUA_TUSERDATA)
        {
            mth::vec2 *vec = CHECK_VECTOR_2(L, 1);
            if (vec == NULL)
            {
                return 0;
            }
            else
            {
                x = vec->x;
                y = vec->y;
            }
        }
        else
        {
            y = x = luaL_checknumber(L, 1);
        }
    }
    pushVector2(L, mth::vec2(x, y));
    return 1;
}

int LuaVector::dotVector2(lua_State *L)
{
    mth::vec2 *oneVec = (mth::vec2 *)luaL_checkudata(L, 1, VECTOR_2_METADATA);
    mth::vec2 *twoVec = (mth::vec2 *)luaL_checkudata(L, 2, VECTOR_2_METADATA);
    if (oneVec == NULL || twoVec == NULL)
    {
        return 0;
    }
    lua_pushnumber(L, (*oneVec) * (*twoVec));
    return 1;
}

int LuaVector::perpendecular2(lua_State *L)
{
    mth::vec2 *oneV = CHECK_VECTOR_2(L, 1);
    if (oneV != NULL)
    {
        pushVector2(L, oneV->perpendecular());
        return 1;
    }
    return 0;
}

int LuaVector::normalizedVector2(lua_State *L)
{
    mth::vec2 *oneVec = CHECK_VECTOR_2(L, 1);
    if (oneVec == NULL)
    {
        return 0;
    }
    mth::vec2 vec = *oneVec;
    vec.normalise();
    pushVector2(L, vec);
    return 1;
}

int LuaVector::x2(lua_State *L)
{
    mth::vec2 *vec = CHECK_VECTOR_2(L, 1);
    lua_pushnumber(L, vec->x);
    return 1;
}

int LuaVector::y2(lua_State *L)
{
    mth::vec2 *vec = CHECK_VECTOR_2(L, 1);
    lua_pushnumber(L, vec->y);
    return 1;
}

int LuaVector::setX2(lua_State *L)
{
    if (lua_gettop(L) >= 2)
    {
        mth::vec2 *vec = CHECK_VECTOR_2(L, 1);
        float v = lua_tonumber(L, 2);
        (*vec).x = v;
    }
    else
    {
        luaL_error(L, "setX: not enough arguments.");
    }
    return 0;
}

int LuaVector::setY2(lua_State *L)
{
    if (lua_gettop(L) >= 2)
    {
        mth::vec2 *vec = CHECK_VECTOR_2(L, 1);
        float v = luaL_checknumber(L, 2);
        (*vec).y = v;
    }
    else
    {
        luaL_error(L, "setY: not enough arguments.");
    }
    return 0;
}

int LuaVector::toString2(lua_State *L)
{
    mth::vec2 *vec = CHECK_VECTOR_2(L, 1);
    if (vec != NULL)
    {
        lua_pushfstring(L, "(%f, %f)", vec->x, vec->y);
        return 1;
    }
    return 0;
}

int LuaVector::addVector2(lua_State *L)
{
    mth::vec2 *oneVec = (mth::vec2 *)luaL_checkudata(L, 1, VECTOR_2_METADATA);
    mth::vec2 *twoVec = (mth::vec2 *)luaL_checkudata(L, 2, VECTOR_2_METADATA);
    if (oneVec == NULL || twoVec == NULL)
    {
        return 0;
    }
    pushVector2(L, (*oneVec) + (*twoVec));
    return 1;
}

int LuaVector::subVector2(lua_State *L)
{
    mth::vec2 *oneVec = (mth::vec2 *)luaL_checkudata(L, 1, VECTOR_2_METADATA);
    mth::vec2 *twoVec = (mth::vec2 *)luaL_checkudata(L, 2, VECTOR_2_METADATA);
    if (oneVec == NULL || twoVec == NULL)
    {
        return 0;
    }
    pushVector2(L, (*oneVec) - (*twoVec));
    return 1;
}

int LuaVector::mulVector2(lua_State *L)
{
    mth::vec2 *oneVec = (mth::vec2 *)luaL_checkudata(L, 2, VECTOR_2_METADATA);
    float num = luaL_checknumber(L, 1);
    if (oneVec == NULL)
    {
        return 0;
    }
    pushVector2(L, (*oneVec) * num);
    return 1;
}

int LuaVector::divVector2(lua_State *L)
{
    mth::vec2 *oneVec = (mth::vec2 *)luaL_checkudata(L, 2, VECTOR_2_METADATA);
    float num = luaL_checknumber(L, 1);
    if (oneVec == NULL)
    {
        return 0;
    }
    if (num == 0)
    {
        luaL_error(L, "operation /: found division on zero.");
        return 0;
    }
    pushVector2(L, (*oneVec) / num);
    return 1;
}

int LuaVector::equality2(lua_State *L)
{
    mth::vec2 *oneVec = (mth::vec2 *)luaL_checkudata(L, 1, VECTOR_2_METADATA);
    mth::vec2 *twoVec = (mth::vec2 *)luaL_checkudata(L, 2, VECTOR_2_METADATA);
    if (oneVec == NULL || twoVec == NULL)
    {
        return 0;
    }
    if ((*oneVec) == (*twoVec))
    {
        lua_pushboolean(L, true);
        return 1;
    }
    else
    {
        lua_pushboolean(L, false);
        return 1;
    }
}

int LuaVector::magnitude2(lua_State *L)
{
    mth::vec2 *vec = CHECK_VECTOR_2(L, 1);
    float mag = vec->magnitude();
    lua_pushnumber(L, mag);
    return 1;
}

int LuaVector::normalize2(lua_State *L)
{
    mth::vec2 *vec = CHECK_VECTOR_2(L, 1);
    vec->normalise();
    return 0;
}