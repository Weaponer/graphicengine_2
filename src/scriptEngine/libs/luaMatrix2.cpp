#include "scriptEngine/libs/luaMatrix2.h"
#include "scriptEngine/libs/luaVector.h"

#include "scriptEngine/libs/utilities/basicFunctions.h"

void LuaMatrix2::luaLoadMatrix2(lua_State *L)
{
    luaL_Reg metaFuncs[] =
        {
            {"__add", LuaMatrix2::add},
            {"__sub", LuaMatrix2::sub},
            {"__mul", LuaMatrix2::mul},
            {"__div", LuaMatrix2::div},
            {"__tostring", LuaMatrix2::toString},
            {"setOrientation", LuaMatrix2::setOrientation},
            {"transpose", LuaMatrix2::transpose},
            {"setSingleMatrix", LuaMatrix2::setSingleMatrix},
            {"transform", LuaMatrix2::transform},
            {"transformTranspose", LuaMatrix2::transformTranspose},
            {"inverse", LuaMatrix2::inverse},
            {"inverseRotate", LuaMatrix2::inverseRotate},
            {"getVector", LuaMatrix2::getVector},
            {"getVectorTranspose", LuaMatrix2::getVectorTranspose},
            {"getRadian", LuaMatrix2::getRadian},
            {"get", LuaMatrix2::get},
            {"set", LuaMatrix2::set},
            {NULL, NULL}};
    BasicFunctions::createLibUserdata(L, MATRIX_2_METADATA, metaFuncs);

    luaL_Reg mat2Funcs[] =
        {
            {"new", LuaMatrix2::newMatrix2},
            {NULL, NULL}};
    BasicFunctions::createStaticLib(L, "Matrix2", mat2Funcs);
}

void LuaMatrix2::pushMatrix2(lua_State *L, const mth::mat2 &mat)
{
    BasicFunctions::pushTObject<mth::mat2>(L, MATRIX_2_METADATA, mat);
}

int LuaMatrix2::newMatrix2(lua_State *L)
{
    int count = lua_gettop(L);
    mth::mat2 m;
    if (count >= 4)
    {
        m.m[0] = luaL_checknumber(L, 1);
        m.m[1] = luaL_checknumber(L, 2);
        m.m[2] = luaL_checknumber(L, 3);
        m.m[3] = luaL_checknumber(L, 4);
    }
    else if (count >= 1)
    {
        mth::mat2 *mat = CHECK_MATRIX_2(L);
        m = *mat;
    }
    pushMatrix2(L, m);
    return 1;
}

int LuaMatrix2::setOrientation(lua_State *L)
{
    int count = lua_gettop(L);
    if (count >= 3)
    {
        mth::mat2 *m = CHECK_MATRIX_2(L);
        mth::vec2 *v1 = (mth::vec2 *)luaL_checkudata(L, 2, VECTOR_2_METADATA);
        mth::vec2 *v2 = (mth::vec2 *)luaL_checkudata(L, 3, VECTOR_2_METADATA);
        m->setOrientation(*v1, *v2);
    }
    else if (count >= 2)
    {
        mth::mat2 *m = CHECK_MATRIX_2(L);
        float rad = luaL_checknumber(L, 2);
        m->setOrientation(rad);
    }
    else
    {
        luaL_error(L, "setOrientation: not enough arguments.");
    }
    return 0;
}

int LuaMatrix2::transpose(lua_State *L)
{
    mth::mat2 *m = CHECK_MATRIX_2(L);
    m->transpose();
    return 0;
}

int LuaMatrix2::setSingleMatrix(lua_State *L)
{
    mth::mat2 *m = CHECK_MATRIX_2(L);
    m->setSingleMatrix();
    return 0;
}

int LuaMatrix2::transform(lua_State *L)
{
    mth::mat2 *m = CHECK_MATRIX_2(L);
    mth::vec2 *v = (mth::vec2 *)luaL_checkudata(L, 2, VECTOR_2_METADATA);
    LuaVector::pushVector2(L, m->transform(*v));
    return 1;
}

int LuaMatrix2::transformTranspose(lua_State *L)
{
    mth::mat2 *m = CHECK_MATRIX_2(L);
    mth::vec2 *v = (mth::vec2 *)luaL_checkudata(L, 2, VECTOR_2_METADATA);
    LuaVector::pushVector2(L, m->transformTranspose(*v));
    return 1;
}

int LuaMatrix2::inverse(lua_State *L)
{
    mth::mat2 *m = CHECK_MATRIX_2(L);
    m->inverse();
    return 0;
}

int LuaMatrix2::inverseRotate(lua_State *L)
{
    mth::mat2 *m = CHECK_MATRIX_2(L);
    m->inverseRotate();
    return 0;
}

int LuaMatrix2::getVector(lua_State *L)
{
    mth::mat2 *m = CHECK_MATRIX_2(L);
    int n = luaL_checknumber(L, 2);
    LuaVector::pushVector2(L, m->getVector(n));
    return 1;
}

int LuaMatrix2::getVectorTranspose(lua_State *L)
{
    mth::mat2 *m = CHECK_MATRIX_2(L);
    int n = luaL_checknumber(L, 2);
    LuaVector::pushVector2(L, m->getVectorTranspose(n));
    return 1;
}

int LuaMatrix2::getRadian(lua_State *L)
{
    mth::mat2 *m = CHECK_MATRIX_2(L);
    lua_pushnumber(L, m->getRadian());
    return 1;
}

int LuaMatrix2::add(lua_State *L)
{
    mth::mat2 *m1 = (mth::mat2 *)luaL_checkudata(L, 1, MATRIX_2_METADATA);
    mth::mat2 *m2 = (mth::mat2 *)luaL_checkudata(L, 2, MATRIX_2_METADATA);
    mth::mat2 m = (*m1) + (*m2);
    pushMatrix2(L, m);
    return 1;
}

int LuaMatrix2::sub(lua_State *L)
{
    mth::mat2 *m1 = CHECK_MATRIX_2(L);
    mth::mat2 *m2 = (mth::mat2 *)luaL_checkudata(L, 2, MATRIX_2_METADATA);
    pushMatrix2(L, (*m1) - (*m2));
    return 1;
}

int LuaMatrix2::mul(lua_State *L)
{
    if (lua_type(L, 1) == LUA_TNUMBER)
    {
        float num = luaL_checknumber(L, 1);
        mth::mat2 *m2 = (mth::mat2 *)luaL_checkudata(L, 2, MATRIX_2_METADATA);
        pushMatrix2(L, ((*m2) * num));
        return 1;
    }
    else
    {
        mth::mat2 *m1 = CHECK_MATRIX_2(L);
        mth::mat2 *m2 = (mth::mat2 *)luaL_checkudata(L, 2, MATRIX_2_METADATA);
        pushMatrix2(L, (*m1) * (*m2));
        return 1;
    }
}

int LuaMatrix2::div(lua_State *L)
{
    float num = luaL_checknumber(L, 1);
    mth::mat2 *m2 = (mth::mat2 *)luaL_checkudata(L, 2, MATRIX_2_METADATA);
    pushMatrix2(L, (*m2) / num);
    return 1;
}


int LuaMatrix2::get(lua_State *L)
{
    mth::mat2 *m1 = CHECK_MATRIX_2(L);
    int num = luaL_checknumber(L, 2);
    lua_pushnumber(L, m1->m[num]);
    return 1;
}

int LuaMatrix2::set(lua_State *L)
{
    mth::mat2 *m1 = CHECK_MATRIX_2(L);
    int num = luaL_checknumber(L, 2);
    float val = luaL_checknumber(L, 3);
    m1->m[num] = val;
    return 0;
}

int LuaMatrix2::toString(lua_State *L)
{
    mth::mat2 *m1 = CHECK_MATRIX_2(L);
    lua_pushfstring(L, "%f %f\n%f %f", m1->m[0], m1->m[1], m1->m[2], m1->m[3]);
    return 1;
}