#include "scriptEngine/libs/luaDebug/luaDebug.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaVector.h"
#include "scriptEngine/libs/luaMatrix4.h"

void LuaDebug::luaLoadDebug(lua_State *L)
{
    luaL_Reg funcs[] = {
        {"log", LuaDebug::log},
        {"logWarning", LuaDebug::logWarning},
        {"logError", LuaDebug::logError},
        {"drawLine", LuaDebug::drawLine},
        {"drawCube", LuaDebug::drawCube},
        {"drawRect", LuaDebug::drawRect},
        {NULL, NULL}};
    BasicFunctions::createStaticLib(L, DEBUG_TABLE, funcs);
}

int LuaDebug::drawLine(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    mth::vec3 *one = CHECK_VECTOR_3(L, 1);
    mth::vec3 *two = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    if (lua_gettop(L) >= 4)
    {
        float time = luaL_checknumber(L, 4);
        mth::vec3 color = *((mth::vec3 *)luaL_checkudata(L, 3, VECTOR_3_METADATA));
        engine->getSpecialSystems()->mainDebugOutput->drawLine(*one, *two, color, time);
    }
    else if (lua_gettop(L) >= 3)
    {
        float time = 0;
        mth::vec3 color = mth::vec3(1, 1, 1);
        if (lua_type(L, 3) == LUA_TNUMBER)
        {
            time = lua_tonumber(L, 3);
        }
        else
        {
            color = *((mth::vec3 *)luaL_checkudata(L, 3, VECTOR_3_METADATA));
        }
        engine->getSpecialSystems()->mainDebugOutput->drawLine(*one, *two, color, time);
    }
    else
    {
        engine->getSpecialSystems()->mainDebugOutput->drawLine(*one, *two);
    }
    return 0;
}

int LuaDebug::drawCube(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    mth::vec3 *pos = CHECK_VECTOR_3(L, 1);
    mth::vec3 *size = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    mth::mat4 mat = mth::mat4();
    mat.setSingleMatrix();
    mth::vec3 color = mth::vec3(1, 1, 1);
    float time = 0;
    int top = lua_gettop(L);
    bool takeTime = false;
    bool takeColor = false;
    bool takeMatrix = false;

    if(top >= 3)
    {
        if(lua_type(L, 3) == LUA_TNUMBER)
        {
            takeTime = true;
            time = lua_tonumber(L, 3);
        }
        else if(luaL_testudata(L, 3, VECTOR_3_METADATA))
        {
            takeColor = true;
            color = *((mth::vec3 *)luaL_checkudata(L, 3, VECTOR_3_METADATA));
        }
        else if(luaL_testudata(L, 3, MATRIX_4_METADATA))
        {
            takeMatrix = true;
            mat = *((mth::mat4 *)luaL_checkudata(L, 3, MATRIX_4_METADATA));
        }
    }

    if(top >= 4)
    {
        if(lua_type(L, 4) == LUA_TNUMBER && !takeTime)
        {
            takeTime = true;
            time = lua_tonumber(L, 4);
        }
        else if(luaL_testudata(L, 4, VECTOR_3_METADATA) && !takeColor)
        {
            takeColor = true;
            color = *((mth::vec3 *)luaL_checkudata(L, 4, VECTOR_3_METADATA));
        }
        else if(luaL_testudata(L, 4, MATRIX_4_METADATA) && !takeMatrix)
        {
            takeMatrix = true;
            mat = *((mth::mat4 *)luaL_checkudata(L, 4, MATRIX_4_METADATA));
        }
    }

    if(top >= 5)
    {
        if(lua_type(L, 5) == LUA_TNUMBER && !takeTime)
        {
            takeTime = true;
            time = lua_tonumber(L, 5);
        }
        else if(luaL_testudata(L, 5, VECTOR_3_METADATA) && !takeColor)
        {
            takeColor = true;
            color = *((mth::vec3 *)luaL_checkudata(L, 5, VECTOR_3_METADATA));
        }
        else if(luaL_testudata(L, 5, MATRIX_4_METADATA) && !takeMatrix)
        {
            takeMatrix = true;
            mat = *((mth::mat4 *)luaL_checkudata(L, 5, MATRIX_4_METADATA));
        }
    }
    engine->getSpecialSystems()->mainDebugOutput->drawCube(*pos, *size, mat, color, time);
    return 0;
}

int LuaDebug::drawRect(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    mth::vec3 *pos = CHECK_VECTOR_3(L, 1);
    mth::vec3 *size = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    mth::mat4 mat = mth::mat4();
    mat.setSingleMatrix();
    mth::vec3 color = mth::vec3(1, 1, 1);
    float time = 0;
    int top = lua_gettop(L);
    bool takeTime = false;
    bool takeColor = false;
    bool takeMatrix = false;

    if(top >= 3)
    {
        if(lua_type(L, 3) == LUA_TNUMBER)
        {
            takeTime = true;
            time = lua_tonumber(L, 3);
        }
        else if(luaL_testudata(L, 3, VECTOR_3_METADATA))
        {
            takeColor = true;
            color = *((mth::vec3 *)luaL_checkudata(L, 3, VECTOR_3_METADATA));
        }
        else if(luaL_testudata(L, 3, MATRIX_4_METADATA))
        {
            takeMatrix = true;
            mat = *((mth::mat4 *)luaL_checkudata(L, 3, MATRIX_4_METADATA));
        }
    }

    if(top >= 4)
    {
        if(lua_type(L, 4) == LUA_TNUMBER && !takeTime)
        {
            takeTime = true;
            time = lua_tonumber(L, 4);
        }
        else if(luaL_testudata(L, 4, VECTOR_3_METADATA) && !takeColor)
        {
            takeColor = true;
            color = *((mth::vec3 *)luaL_checkudata(L, 4, VECTOR_3_METADATA));
        }
        else if(luaL_testudata(L, 4, MATRIX_4_METADATA) && !takeMatrix)
        {
            takeMatrix = true;
            mat = *((mth::mat4 *)luaL_checkudata(L, 4, MATRIX_4_METADATA));
        }
    }

    if(top >= 5)
    {
        if(lua_type(L, 5) == LUA_TNUMBER && !takeTime)
        {
            takeTime = true;
            time = lua_tonumber(L, 5);
        }
        else if(luaL_testudata(L, 5, VECTOR_3_METADATA) && !takeColor)
        {
            takeColor = true;
            color = *((mth::vec3 *)luaL_checkudata(L, 5, VECTOR_3_METADATA));
        }
        else if(luaL_testudata(L, 5, MATRIX_4_METADATA) && !takeMatrix)
        {
            takeMatrix = true;
            mat = *((mth::mat4 *)luaL_checkudata(L, 5, MATRIX_4_METADATA));
        }
    }
    engine->getSpecialSystems()->mainDebugOutput->drawRect(*pos, *size, mat, color, time);
    return 0;
}

int LuaDebug::log(lua_State *L)
{
    const char *log = luaL_checkstring(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->mainDebugOutput->debugLog(log);
    return 0;
}

int LuaDebug::logWarning(lua_State *L)
{
    const char *log = luaL_checkstring(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->mainDebugOutput->debugWarning(log);
    return 0;
}

int LuaDebug::logError(lua_State *L)
{
    const char *log = luaL_checkstring(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->mainDebugOutput->debugError(log);
    return 0;
}