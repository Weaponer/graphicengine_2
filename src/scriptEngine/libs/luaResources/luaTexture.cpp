#include "scriptEngine/base/scriptEngine.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaResources/luaTexture.h"

void LuaTexture::luaLoadTexture(lua_State *L)
{
    luaL_Reg metaFuncs[] =
        {
            {"__tostring", LuaTexture::tostring},
            {NULL, NULL}};
    BasicFunctions::createLibUserdata(L, TEXTURE_METADATA, metaFuncs);
}

int LuaTexture::getTexture(lua_State *L, Texture *texture)
{
    return BasicFunctions::getLuaObject(L, TEXTURE_METADATA, texture);
}

void LuaTexture::pushTexture(lua_State *L, Texture *texture)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, getTexture(L, texture));
}

int LuaTexture::tostring(lua_State *L)
{
    lua_pushfstring(L, "Object.Texture");
    return 1;
}