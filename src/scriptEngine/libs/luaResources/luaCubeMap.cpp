#include "scriptEngine/libs/luaResources/luaCubeMap.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

void LuaCubeMap::luaLoadCubeMap(lua_State *L)
{
    luaL_Reg metaFuncs[] =
        {
            {"__tostring", LuaCubeMap::tostring},
            {NULL, NULL}};
    BasicFunctions::createLibUserdata(L, CUBE_MAP_METADATA, metaFuncs);
}

int LuaCubeMap::getCubeMap(lua_State *L, CubeMap *_cubeMap)
{
    return BasicFunctions::getLuaObject(L, CUBE_MAP_METADATA, _cubeMap);
}

void LuaCubeMap::pushCubeMap(lua_State *L, CubeMap *_cubeMap)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, getCubeMap(L, _cubeMap));
}

int LuaCubeMap::tostring(lua_State *L)
{
    lua_pushfstring(L, "Object.CubeMap");
    return 1;
}