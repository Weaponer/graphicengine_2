#include "scriptEngine/base/scriptEngine.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaResources/luaMaterial.h"
#include "scriptEngine/libs/luaResources/luaCubeMap.h"
#include "scriptEngine/libs/luaResources/luaTexture.h"
#include "scriptEngine/libs/luaResources/luaShader.h"
#include "scriptEngine/libs/luaVector.h"
#include "scriptEngine/libs/luaColor.h"

void LuaMaterial::luaLoadMaterial(lua_State *L)
{
    luaL_Reg metaFuncs[] =
        {
            {"name", LuaMaterial::name},
            {"setName", LuaMaterial::setName},
            {"shader", LuaMaterial::shader},
            {"setInt", LuaMaterial::setInt},
            {"setFloat", LuaMaterial::setFloat},
            {"setToggle", LuaMaterial::setToggle},
            {"setVector", LuaMaterial::setVector},
            {"setColor", LuaMaterial::setColor},
            {"setTexture", LuaMaterial::setTexture},
            {"setCubeMap", LuaMaterial::setCubeMap},
            {"getInt", LuaMaterial::getInt},
            {"getFloat", LuaMaterial::getFloat},
            {"getToggle", LuaMaterial::getToggle},
            {"getVector", LuaMaterial::getVector},
            {"getColor", LuaMaterial::getColor},
            {"getTexture", LuaMaterial::getTexture},
            {"getCubeMap", LuaMaterial::getCubeMap},
            {"generateInstancing", LuaMaterial::generateInstancing},
            {"setGenerateInstancing", LuaMaterial::setGenerateInstancing},
            {"__tostring", LuaMaterial::tostring},
            {NULL, NULL}};
    BasicFunctions::createLibUserdata(L, MATERIAL_METADATA, metaFuncs);
}

int LuaMaterial::getMaterial(lua_State *L, Material *material)
{
    return BasicFunctions::getLuaObject(L, MATERIAL_METADATA, material);
}

int LuaMaterial::name(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    lua_pushstring(L, mat->getName());
    return 1;
}

int LuaMaterial::setName(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    const char *name = luaL_checkstring(L, 2);
    mat->setName(name);
    return 0;
}

int LuaMaterial::shader(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    if (mat->getShader() == NULL)
    {
        lua_pushnil(L);
    }
    else
    {
        LuaShader::pushShader(L, mat->getShader());
    }
    return 1;
}

int LuaMaterial::setInt(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    const char *propName = luaL_checkstring(L, 2);
    int val = luaL_checkinteger(L, 3);
    mat->setInt(propName, val);
    return 0;
}

int LuaMaterial::setFloat(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    const char *propName = luaL_checkstring(L, 2);
    float val = luaL_checknumber(L, 3);
    mat->setFloat(propName, val);
    return 0;
}

int LuaMaterial::setToggle(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    const char *propName = luaL_checkstring(L, 2);
    bool val = lua_toboolean(L, 3);
    mat->setToggle(propName, val);
    return 0;
}

int LuaMaterial::setVector(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    const char *propName = luaL_checkstring(L, 2);
    mth::vec4 *val = CHECK_VECTOR_4(L, 3);
    mat->setVector(propName, val);
    return 0;
}

int LuaMaterial::setColor(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    const char *propName = luaL_checkstring(L, 2);
    mth::col *val = CHECK_COLOR(L, 3);
    mat->setColor(propName, val);
    return 0;
}

int LuaMaterial::setTexture(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    const char *propName = luaL_checkstring(L, 2);
    Texture *val = CHECK_TEXTURE_OR_NULL(L, 3);
    mat->setTexture(propName, val);
    return 0;
}

int LuaMaterial::setCubeMap(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    const char *propName = luaL_checkstring(L, 2);
    CubeMap *val = CHECK_CUBE_MAP_OR_NULL(L, 3);
    mat->setCubeMap(propName, val);
    return 0;
}

int LuaMaterial::getInt(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    const char *propName = luaL_checkstring(L, 2);
    lua_pushinteger(L, mat->getInt(propName));
    return 1;
}

int LuaMaterial::getFloat(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    const char *propName = luaL_checkstring(L, 2);
    lua_pushnumber(L, mat->getFloat(propName));
    return 1;
}

int LuaMaterial::getToggle(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    const char *propName = luaL_checkstring(L, 2);
    lua_pushboolean(L, mat->getToggle(propName));
    return 1;
}

int LuaMaterial::getVector(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    const char *propName = luaL_checkstring(L, 2);
    LuaVector::pushVector4(L, mat->getVector(propName));
    return 1;
}

int LuaMaterial::getColor(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    const char *propName = luaL_checkstring(L, 2);
    LuaColor::pushColor(L, mat->getColor(propName));
    return 1;
}

int LuaMaterial::getTexture(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    const char *propName = luaL_checkstring(L, 2);
    Texture *tex = mat->getTexture(propName);
    if (tex == NULL)
    {
        lua_pushnil(L);
    }
    else
    {
        LuaTexture::pushTexture(L, tex);
    }
    return 1;
}

int LuaMaterial::getCubeMap(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    const char *propName = luaL_checkstring(L, 2);
    CubeMap *cubeMap = mat->getCubeMap(propName);
    if (cubeMap == NULL)
    {
        lua_pushnil(L);
    }
    else
    {
        LuaCubeMap::pushCubeMap(L, cubeMap);
    }
    return 1;
}

int LuaMaterial::generateInstancing(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    lua_pushboolean(L, mat->getGenerateInstancing());
    return 1;
}

int LuaMaterial::setGenerateInstancing(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    bool val = lua_toboolean(L, 2);
    mat->setGenerateInstancing(val);
    return 0;
}

int LuaMaterial::tostring(lua_State *L)
{
    Material *mat = CHECK_MATERIAL(L, 1);
    lua_pushfstring(L, "Object.Material %s", mat->getName());
    return 1;
}