#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaResources/luaResources.h"
#include "scriptEngine/base/scriptEngine.h"
#include "scriptEngine/libs/utilities/enumGenerator.h"
#include "scriptEngine/libs/luaResources/luaResourceAsyncHandler.h"

LuaResources::dataLuaResource LuaResources::data[] =
    {
        {MODEL_METADATA, "modelOBJ", TypeResource::MODEL_OBJ, LuaResources::getModel},
        {MATERIAL_METADATA, "material", TypeResource::MATERIAL, LuaResources::getMaterial},
        {TEXTURE_METADATA, "texture", TypeResource::TEXTURE, LuaResources::getTexture},
        {TEXTURE_METADATA, "hdrTexture", TypeResource::HDR_TEXTURE, LuaResources::getTexture},
        {CUBE_MAP_METADATA, "cubeMap", TypeResource::CUBE_MAP, LuaResources::getCubeMap},
        {SHADER_METADATA, "shader", TypeResource::SHADER, LuaResources::getShader}};

void LuaResources::luaLoadResources(lua_State *L)
{
    LuaModel::luaLoadModel(L);
    LuaMaterial::luaLoadMaterial(L);
    LuaTexture::luaLoadTexture(L);
    LuaCubeMap::luaLoadCubeMap(L);
    LuaMesh::luaLoadMesh(L);
    LuaShader::luaLoadShader(L);
    LuaResourceAsyncHandler::luaLoadResourceAsyncHandler(L);

    EnumGenerator<int> enumResources;
    enumResources.startNewEnum(L, RESOURCE_ENUM, RESOURCE_ITEM_METADATA);
    enumResources.addParameter(L, TypeResource::MATERIAL, "material");
    enumResources.addParameter(L, TypeResource::MODEL_OBJ, "modelOBJ");
    enumResources.addParameter(L, TypeResource::TEXTURE, "texture");
    enumResources.addParameter(L, TypeResource::HDR_TEXTURE, "hdrTexture");
    enumResources.addParameter(L, TypeResource::CUBE_MAP, "cubeMap");
    enumResources.addParameter(L, TypeResource::SHADER, "shader");
    enumResources.endEnum(L);

    luaL_Reg resourcesFuncs[] =
        {
            {"loadResource", LuaResources::loadResource},
            {"unloadResource", LuaResources::unloadResource},
            {"loadAsyncResource", LuaResources::loadAsyncResource},
            {"unloadAsyncResource", LuaResources::unloadAsyncResource},
            {NULL, NULL}};
    BasicFunctions::createStaticLib(L, "Resources", resourcesFuncs);
}

void LuaResources::pushTypeResource(lua_State *L, TypeResource type)
{
    lua_getglobal(L, RESOURCE_ENUM);
    int count = (sizeof(data) / sizeof(LuaResources::dataLuaResource));
    for (int i = 0; i < count; i++)
    {
        if (data[i].code == type)
        {
            lua_getfield(L, -1, data[i].name);
            lua_remove(L, -2);
            return;
        }
    }
    lua_remove(L, -1);
    lua_pushnil(L);
}

int LuaResources::pushResource(lua_State *L, Object *_object, TypeResource _type)
{
    int count = (sizeof(data) / sizeof(LuaResources::dataLuaResource));
    for (int i = 0; i < count; i++)
    {
        if (data[i].code == _type)
        {
            int ref = data[i].functGetUData(L, _object);
            lua_rawgeti(L, LUA_REGISTRYINDEX, ref);
            return 1;
        }
    }
    lua_pushnil(L);
    return 1;
}

int LuaResources::checkCode(int code)
{
    int count = (sizeof(data) / sizeof(LuaResources::dataLuaResource));
    for (int i = 0; i < count; i++)
    {
        if (data[i].code == (uint)code)
        {
            return i;
        }
    }
    return -1;
};

int LuaResources::getModel(lua_State *L, Object *obj)
{
    Model *model = (Model *)obj;
    return LuaModel::getModel(L, model);
}

int LuaResources::getMaterial(lua_State *L, Object *obj)
{
    Material *material = (Material *)obj;
    return LuaMaterial::getMaterial(L, material);
}

int LuaResources::getTexture(lua_State *L, Object *obj)
{
    Texture *texture = (Texture *)obj;
    return LuaTexture::getTexture(L, texture);
}

int LuaResources::getCubeMap(lua_State *L, Object *obj)
{
    CubeMap *cubeMap = (CubeMap *)obj;
    return LuaCubeMap::getCubeMap(L, cubeMap);
}

int LuaResources::getShader(lua_State *L, Object *obj)
{
    Shader *shader = (Shader *)obj;
    return LuaShader::getShader(L, shader);
}

int LuaResources::loadResource(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    int code = *CHECK_RESOURCE(L, 1);
    const char *path = luaL_checkstring(L, 2);

    int pos = checkCode(code);
    if (pos == -1)
    {
        return 0;
    }
    Object *load = engine->getSpecialSystems()->resourceLoader->loadResource(path, (TypeResource)code);
    if (load == NULL)
    {
        return 0;
    }
    else
    {
        int index = data[pos].functGetUData(L, load);
        lua_rawgeti(L, LUA_REGISTRYINDEX, index);
        return 1;
    }
}

int LuaResources::unloadResource(lua_State *L)
{
    if (lua_gettop(L) == 1)
    {
        int count = (sizeof(data) / sizeof(LuaResources::dataLuaResource));
        void *udata = NULL;
        int pos = -1;
        for (int i = 0; i < count; i++)
        {
            udata = luaL_testudata(L, -1, data[i].metadata);
            if (udata != NULL)
            {
                pos = i;
                break;
            }
        }
        if (pos >= 0)
        {
            Object *obj = ((luaObject *)lua_touserdata(L, -1))->object;

            ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
            engine->getSpecialSystems()->resourceLoader->unloadResource(obj);
            printf("unload resource\n");
        }
    }
    return 0;
}

int LuaResources::loadAsyncResource(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    int code = *CHECK_RESOURCE(L, 1);
    const char *path = luaL_checkstring(L, 2);
    ResourceAsyncHandler *handler = engine->getSpecialSystems()->resourceLoader->loadAsyncResource(path, (TypeResource)code);
    LuaResourceAsyncHandler::pushResourceAsyncHandler(L, handler);
    return 1;
}

int LuaResources::unloadAsyncResource(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    ResourceAsyncHandler *handler = CHECK_RESOURCE_ASYNC_HANDLER(L, 1);
    engine->getSpecialSystems()->resourceLoader->unloadAsyncResource(handler);
    return 0;
}