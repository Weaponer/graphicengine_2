#include "scriptEngine/base/scriptEngine.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaResources/luaModel.h"
#include "scriptEngine/libs/luaResources/luaMesh.h"

void LuaModel::luaLoadModel(lua_State *L)
{
    luaL_Reg metaFuncs[] =
        {
            {"__tostring", LuaModel::toString},
            {"names", LuaModel::toString},
            {"countMeshes", LuaModel::countMeshes},
            {"meshOnIndex", LuaModel::getMeshOnIndex},
            {"meshOnName", LuaModel::getMeshOnName},
            {NULL, NULL}};
    BasicFunctions::createLibUserdata(L, MODEL_METADATA, metaFuncs);
}

int LuaModel::getModel(lua_State *L, Model *model)
{
    return BasicFunctions::getLuaObject(L, MODEL_METADATA, model);
}

int LuaModel::countMeshes(lua_State *L)
{
    Model *model = CHECK_MODEL(L, 1);
    lua_pushnumber(L, model->countMeshs());
    return 1;
}

int LuaModel::getMeshOnIndex(lua_State *L)
{
    Model *model = CHECK_MODEL(L, 1);
    int index = lua_tointeger(L, 2);
    Mesh *mesh = model->getMeshAtIndex(index);
    if (mesh == NULL || model->countMeshs() < index)
    {
        lua_pushnil(L);
    }
    else
    {
        LuaMesh::pushMesh(L, mesh);
    }
    return 1;
}

int LuaModel::getMeshOnName(lua_State *L)
{
    Model *model = CHECK_MODEL(L, 1);
    const char *name = lua_tostring(L, 2);
    std::string str = std::string(name);
    Mesh *mesh = model->getMeshAtName(&str);
    if (mesh == NULL)
    {
        lua_pushnil(L);
    }
    else
    {
        LuaMesh::pushMesh(L, mesh);
    }
    return 1;
}

int LuaModel::toString(lua_State *L)
{
    Model *model = CHECK_MODEL(L, 1);
    int count = model->countMeshs();
    for (int i = 0; i < count; i++)
    {
        lua_pushfstring(L, "%s", (model->getMeshAtIndex(i))->getName()->c_str());
    }
    return count;
}