#include "scriptEngine/libs/luaResources/luaResourceAsyncHandler.h"
#include "scriptEngine/libs/luaResources/luaResources.h"

void LuaResourceAsyncHandler::luaLoadResourceAsyncHandler(lua_State *L)
{
    EnumGenerator<int> enumStatusLoad;
    enumStatusLoad.startNewEnum(L, STATUS_LOAD_ENUM, STATUS_LOAD_ITEM_METADATA);
    enumStatusLoad.addParameter(L, StatusLoad::SL_None, "none");
    enumStatusLoad.addParameter(L, StatusLoad::SL_Failed, "failed");
    enumStatusLoad.addParameter(L, StatusLoad::SL_Succeed, "succeed");
    enumStatusLoad.endEnum(L);

    luaL_Reg metaFuncs[] =
        {
            {"path", LuaResourceAsyncHandler::path},
            {"status", LuaResourceAsyncHandler::status},
            {"resource", LuaResourceAsyncHandler::resource},
            {"discard", LuaResourceAsyncHandler::discard},
            {"isDiscard", LuaResourceAsyncHandler::isDiscard},
            {"__tostring", LuaResourceAsyncHandler::tostring},
            {NULL, NULL}};
    BasicFunctions::createLibUserdata(L, RESOURCE_ASYNC_HANDLER_METADATA, metaFuncs);
}

void LuaResourceAsyncHandler::pushResourceAsyncHandler(lua_State *L, ResourceAsyncHandler *_data)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, getResourceAsyncHandler(L, _data));
}

int LuaResourceAsyncHandler::getResourceAsyncHandler(lua_State *L, ResourceAsyncHandler *_data)
{
    return BasicFunctions::getLuaObject(L, RESOURCE_ASYNC_HANDLER_METADATA, _data);
}

void LuaResourceAsyncHandler::pushStatusLoad(lua_State *L, StatusLoad _status)
{
    lua_getglobal(L, STATUS_LOAD_ENUM);
    switch (_status)
    {
    case StatusLoad::SL_None:
        lua_getfield(L, -1, "none");
        lua_remove(L, -2);
        return;
    case StatusLoad::SL_Failed:
        lua_getfield(L, -1, "failed");
        lua_remove(L, -2);
        return;
    case StatusLoad::SL_Succeed:
        lua_getfield(L, -1, "succeed");
        lua_remove(L, -2);
        return;
    default:
        return;
    }
}

int LuaResourceAsyncHandler::path(lua_State *L)
{
    ResourceAsyncHandler *handler = CHECK_RESOURCE_ASYNC_HANDLER(L, 1);
    lua_pushfstring(L, "%s", handler->getPath());
    return 1;
}

int LuaResourceAsyncHandler::status(lua_State *L)
{
    ResourceAsyncHandler *handler = CHECK_RESOURCE_ASYNC_HANDLER(L, 1);
    pushStatusLoad(L, handler->getStatus());
    return 1;
}

int LuaResourceAsyncHandler::type(lua_State *L)
{
    ResourceAsyncHandler *handler = CHECK_RESOURCE_ASYNC_HANDLER(L, 1);
    LuaResources::pushTypeResource(L, handler->getType());
    return 1;
}

int LuaResourceAsyncHandler::resource(lua_State *L)
{
    ResourceAsyncHandler *handler = CHECK_RESOURCE_ASYNC_HANDLER(L, 1);
    if (handler->getResource() == NULL)
    {
        lua_pushnil(L);
        return 1;
    }
    else
    {
        LuaResources::pushResource(L, handler->getResource(), handler->getType());
        return 1;
    }
}

int LuaResourceAsyncHandler::discard(lua_State *L)
{
    ResourceAsyncHandler *handler = CHECK_RESOURCE_ASYNC_HANDLER(L, 1);
    handler->discard();
    return 0;
}

int LuaResourceAsyncHandler::isDiscard(lua_State *L)
{
    ResourceAsyncHandler *handler = CHECK_RESOURCE_ASYNC_HANDLER(L, 1);
    lua_pushboolean(L, handler->getDiscardState());
    return 1;
}

int LuaResourceAsyncHandler::tostring(lua_State *L)
{
    lua_pushfstring(L, "Object.ResourceAsyncHandler");
    return 1;
}