#include "scriptEngine/libs/luaResources/luaShader.h"
#include "scriptEngine/libs/luaVector.h"
#include "scriptEngine/libs/luaColor.h"
#include "scriptEngine/libs/luaResources/luaTexture.h"
#include "scriptEngine/libs/luaResources/luaCubeMap.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

#include "renderEngine/graphicEngine.h"
#include "renderEngine/resources/shader/shader.h"
#include "renderEngine/resources/shader/shaderPropertiesData/propertiesRegistry.h"
#include "renderEngine/resources/shader/properties/intProperty.h"
#include "renderEngine/resources/shader/properties/floatProperty.h"
#include "renderEngine/resources/shader/properties/toggleProperty.h"
#include "renderEngine/resources/shader/properties/vectorProperty.h"
#include "renderEngine/resources/shader/properties/colorProperty.h"
#include "renderEngine/resources/shader/properties/textureProperty.h"
#include "renderEngine/resources/shader/properties/cubeMapProperty.h"

void LuaShader::luaLoadShader(lua_State *L)
{
    luaL_Reg funcs[] =
        {
            {"find", LuaShader::find},
            {"findPropertyInt", LuaShader::findPropertyInt},
            {"findPropertyFloat", LuaShader::findPropertyFloat},
            {"findPropertyVector", LuaShader::findPropertyVector},
            {"findPropertyColor", LuaShader::findPropertyColor},
            {"findPropertyTexture", LuaShader::findPropertyTexture},
            {"findPropertyCubeMap", LuaShader::findPropertyCubeMap},
            {"setUseGlobalProperty", LuaShader::setUseGlobalProperty},
            {"useGlobalProperty", LuaShader::useGlobalProperty},
            {"globalPropertyInt", LuaShader::globalPropertyInt},
            {"globalPropertyFloat", LuaShader::globalPropertyFloat},
            {"globalPropertyVector", LuaShader::globalPropertyVector},
            {"globalPropertyColor", LuaShader::globalPropertyColor},
            {"globalPropertyTexture", LuaShader::globalPropertyTexture},
            {"globalPropertyCubeMap", LuaShader::globalPropertyCubeMap},
            {"setGlobalPropertyInt", LuaShader::setGlobalPropertyInt},
            {"setGlobalPropertyFloat", LuaShader::setGlobalPropertyFloat},
            {"setGlobalPropertyVector", LuaShader::setGlobalPropertyVector},
            {"setGlobalPropertyColor", LuaShader::setGlobalPropertyColor},
            {"setGlobalPropertyTexture", LuaShader::setGlobalPropertyTexture},
            {"setGlobalPropertyCubeMap", LuaShader::setGlobalPropertyCubeMap},
            {"nameProperty", LuaShader::nameProperty},
            {NULL, NULL}};
    BasicFunctions::createStaticLib(L, SHADER_STATIC_LIB, funcs);

    luaL_Reg metaFuncs[] =
        {
            {"__tostring", LuaShader::tostring},
            {"name", LuaShader::name},
            {"countProperties", LuaShader::countProperties},
            {"haveProperty", LuaShader::haveProperty},
            {NULL, NULL}};
    BasicFunctions::createLibUserdata(L, SHADER_METADATA, metaFuncs);
}

int LuaShader::getShader(lua_State *L, Shader *_shader)
{
    return BasicFunctions::getLuaObject(L, SHADER_METADATA, _shader);
}

void LuaShader::pushShader(lua_State *L, Shader *_shader)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, getShader(L, _shader));
}

int LuaShader::find(lua_State *L)
{
    const char *name = luaL_checkstring(L, 1);

    GraphicEngine *graphic = BasicFunctions::getScriptEngine(L)->getSpecialSystems()->graphicEngine;
    Shader *shader = graphic->getMainSystems()->getShadersStorage()->findShader(name);
    if (shader == NULL)
    {
        lua_pushnil(L);
    }
    else
    {
        pushShader(L, shader);
    }
    return (1);
}

static int findProperty(lua_State *L, TypeProperty _type)
{
    const char *name = luaL_checkstring(L, 1);

    GraphicEngine *graphic = BasicFunctions::getScriptEngine(L)->getSpecialSystems()->graphicEngine;
    int code = graphic->getMainSystems()->getShadersStorage()->getPropertiesRegistry()->getPropertyCodeRegistry(name, _type);
    if (code == -1)
    {
        lua_pushnil(L);
    }
    else
    {
        lua_pushinteger(L, code);
    }
    return 1;
}

int LuaShader::findPropertyInt(lua_State *L)
{
    return findProperty(L, TypeProperty::Int_P);
}

int LuaShader::findPropertyFloat(lua_State *L)
{
    return findProperty(L, TypeProperty::Float_P);
}

int LuaShader::findPropertyVector(lua_State *L)
{
    return findProperty(L, TypeProperty::Vector_P);
}

int LuaShader::findPropertyColor(lua_State *L)
{
    return findProperty(L, TypeProperty::Color_P);
}

int LuaShader::findPropertyTexture(lua_State *L)
{
    return findProperty(L, TypeProperty::Texture_P);
}

int LuaShader::findPropertyCubeMap(lua_State *L)
{
    return findProperty(L, TypeProperty::CubeMap_P);
}

int LuaShader::setUseGlobalProperty(lua_State *L)
{
    int code = luaL_checknumber(L, 1);
    bool use = lua_toboolean(L, 2);
    GraphicEngine *graphic = BasicFunctions::getScriptEngine(L)->getSpecialSystems()->graphicEngine;
    graphic->getMainSystems()->getShadersStorage()->getPropertiesRegistry()->setUseGlobalProperty(code, use);
    return 0;
}

int LuaShader::useGlobalProperty(lua_State *L)
{
    int code = luaL_checknumber(L, 1);
    GraphicEngine *graphic = BasicFunctions::getScriptEngine(L)->getSpecialSystems()->graphicEngine;
    lua_pushboolean(L, graphic->getMainSystems()->getShadersStorage()->getPropertiesRegistry()->getUseGlobalProperty(code));
    return 1;
}

static const Property *getProperty(lua_State *L, TypeProperty _type)
{
    GraphicEngine *graphic = BasicFunctions::getScriptEngine(L)->getSpecialSystems()->graphicEngine;

    if (lua_type(L, 1) == LUA_TNUMBER)
    {
        int code = luaL_checkinteger(L, 1);
        return graphic->getMainSystems()->getShadersStorage()->getPropertiesRegistry()->getGlobalSettingProperty(code);
    }
    else
    {
        const char *name = luaL_checkstring(L, 1);
        return graphic->getMainSystems()->getShadersStorage()->getPropertiesRegistry()->getGlobalSettingProperty(name, _type);
    }
}

int LuaShader::globalPropertyInt(lua_State *L)
{
    const Property *prop = getProperty(L, TypeProperty::Int_P);
    if (prop == NULL)
    {
        lua_pushinteger(L, 0);
    }
    else
    {
        lua_pushinteger(L, reinterpret_cast<const IntProperty *>(prop)->getValue());
    }
    return 1;
}

int LuaShader::globalPropertyFloat(lua_State *L)
{
    const Property *prop = getProperty(L, TypeProperty::Float_P);
    if (prop == NULL)
    {
        lua_pushnumber(L, 0);
    }
    else
    {
        lua_pushnumber(L, reinterpret_cast<const FloatProperty *>(prop)->getValue());
    }
    return 1;
}

int LuaShader::globalPropertyVector(lua_State *L)
{
    const Property *prop = getProperty(L, TypeProperty::Vector_P);
    if (prop == NULL)
    {
        LuaVector::pushVector4(L, mth::vec4());
    }
    else
    {
        LuaVector::pushVector4(L, reinterpret_cast<const VectorProperty *>(prop)->getValue());
    }
    return 1;
}

int LuaShader::globalPropertyColor(lua_State *L)
{
    const Property *prop = getProperty(L, TypeProperty::Color_P);
    if (prop == NULL)
    {
        LuaColor::pushColor(L, mth::col());
    }
    else
    {
        LuaColor::pushColor(L, reinterpret_cast<const ColorProperty *>(prop)->getValue());
    }
    return 1;
}

int LuaShader::globalPropertyTexture(lua_State *L)
{
    const Property *prop = getProperty(L, TypeProperty::Texture_P);
    if (prop == NULL)
    {
        lua_pushnil(L);
    }
    else
    {
        const TextureProperty *tprop = reinterpret_cast<const TextureProperty *>(prop);
        if (tprop->getValue() == NULL)
        {
            lua_pushnil(L);
        }
        else
        {
            LuaTexture::pushTexture(L, tprop->getValue());
        }
    }
    return 1;
}

int LuaShader::globalPropertyCubeMap(lua_State *L)
{
    const Property *prop = getProperty(L, TypeProperty::CubeMap_P);
    if (prop == NULL)
    {
        lua_pushnil(L);
    }
    else
    {
        const CubeMapProperty *cprop = reinterpret_cast<const CubeMapProperty *>(prop);
        if (cprop->getValue() == NULL)
        {
            lua_pushnil(L);
        }
        else
        {
            LuaCubeMap::pushCubeMap(L, cprop->getValue());
        }
    }
    return 1;
}

static int getIndexGlobalPropertyForSet(lua_State *L, GraphicEngine *_graphic, TypeProperty _type)
{
    if (lua_type(L, 1) == LUA_TNUMBER)
    {
        int code = luaL_checkinteger(L, 1);
        return code;
    }
    else
    {
        const char *name = luaL_checkstring(L, 1);
        return _graphic->getMainSystems()->getShadersStorage()->getPropertiesRegistry()->getPropertyCodeRegistry(name, _type);
    }
}

int LuaShader::setGlobalPropertyInt(lua_State *L)
{
    GraphicEngine *graphic = BasicFunctions::getScriptEngine(L)->getSpecialSystems()->graphicEngine;
    int code = getIndexGlobalPropertyForSet(L, graphic, TypeProperty::Int_P);
    int value = luaL_checkinteger(L, 2);
    PropertiesRegistry *propReg = graphic->getMainSystems()->getShadersStorage()->getPropertiesRegistry();
    IntProperty iprop = IntProperty(code, propReg);
    iprop.setValue(value);
    propReg->setGlobalPropertyValue(code, &iprop);
    return 0;
}

int LuaShader::setGlobalPropertyFloat(lua_State *L)
{
    GraphicEngine *graphic = BasicFunctions::getScriptEngine(L)->getSpecialSystems()->graphicEngine;
    int code = getIndexGlobalPropertyForSet(L, graphic, TypeProperty::Float_P);
    float value = luaL_checknumber(L, 2);
    PropertiesRegistry *propReg = graphic->getMainSystems()->getShadersStorage()->getPropertiesRegistry();
    FloatProperty fprop = FloatProperty(code, propReg);
    fprop.setValue(value);
    propReg->setGlobalPropertyValue(code, &fprop);
    return 0;
}

int LuaShader::setGlobalPropertyVector(lua_State *L)
{
    GraphicEngine *graphic = BasicFunctions::getScriptEngine(L)->getSpecialSystems()->graphicEngine;
    int code = getIndexGlobalPropertyForSet(L, graphic, TypeProperty::Vector_P);
    mth::vec4 value = *CHECK_VECTOR_4(L, 2);
    PropertiesRegistry *propReg = graphic->getMainSystems()->getShadersStorage()->getPropertiesRegistry();
    VectorProperty vprop = VectorProperty(code, propReg);
    vprop.setValue(&value);
    propReg->setGlobalPropertyValue(code, &vprop);
    return 0;
}

int LuaShader::setGlobalPropertyColor(lua_State *L)
{
    GraphicEngine *graphic = BasicFunctions::getScriptEngine(L)->getSpecialSystems()->graphicEngine;
    int code = getIndexGlobalPropertyForSet(L, graphic, TypeProperty::Color_P);
    mth::col value = *CHECK_COLOR(L, 2);
    PropertiesRegistry *propReg = graphic->getMainSystems()->getShadersStorage()->getPropertiesRegistry();
    ColorProperty cprop = ColorProperty(code, propReg);
    cprop.setValue(&value);
    propReg->setGlobalPropertyValue(code, &cprop);
    return 0;
}

int LuaShader::setGlobalPropertyTexture(lua_State *L)
{
    GraphicEngine *graphic = BasicFunctions::getScriptEngine(L)->getSpecialSystems()->graphicEngine;
    int code = getIndexGlobalPropertyForSet(L, graphic, TypeProperty::Texture_P);
    Texture *value = CHECK_TEXTURE_OR_NULL(L, 2);
    PropertiesRegistry *propReg = graphic->getMainSystems()->getShadersStorage()->getPropertiesRegistry();
    TextureProperty textprop = TextureProperty(code, propReg);
    textprop.setValue(value);
    propReg->setGlobalPropertyValue(code, &textprop);
    return 0;
}

int LuaShader::setGlobalPropertyCubeMap(lua_State *L)
{
    GraphicEngine *graphic = BasicFunctions::getScriptEngine(L)->getSpecialSystems()->graphicEngine;
    int code = getIndexGlobalPropertyForSet(L, graphic, TypeProperty::CubeMap_P);
    CubeMap *value = CHECK_CUBE_MAP_OR_NULL(L, 2);
    PropertiesRegistry *propReg = graphic->getMainSystems()->getShadersStorage()->getPropertiesRegistry();
    CubeMapProperty cubeprop = CubeMapProperty(code, propReg);
    cubeprop.setValue(value);
    propReg->setGlobalPropertyValue(code, &cubeprop);
    return 0;
}

int LuaShader::nameProperty(lua_State *L)
{
    GraphicEngine *graphic = BasicFunctions::getScriptEngine(L)->getSpecialSystems()->graphicEngine;
    int code = luaL_checkinteger(L, 1);
    PropertiesRegistry *propReg = graphic->getMainSystems()->getShadersStorage()->getPropertiesRegistry();
    lua_pushstring(L, propReg->getNameProperty(code));
    return 1;
}

int LuaShader::getCodeProperty(lua_State *L)
{
    Shader *shader = CHECK_SHADER(L, 1);
    int num = luaL_checkinteger(L, 2);
    if (num > 0 && num < shader->getCountProperties())
    {
        lua_pushinteger(L, shader->getProperty(num)->getCodeData());
    }
    else
    {
        luaL_error(L, "No correct index");
    }
    return 1;
}

int LuaShader::name(lua_State *L)
{
    Shader *shader = CHECK_SHADER(L, 1);
    lua_pushfstring(L, "%s", shader->getName());
    return 1;
}

int LuaShader::countProperties(lua_State *L)
{
    Shader *shader = CHECK_SHADER(L, 1);
    lua_pushinteger(L, shader->getCountProperties());
    return 1;
}

int LuaShader::haveProperty(lua_State *L)
{
    Shader *shader = CHECK_SHADER(L, 1);
    int code = luaL_checkinteger(L, 2);
    lua_pushboolean(L, shader->checkHaveProperty(code));
    return 1;
}

int LuaShader::tostring(lua_State *L)
{
    Shader *shader = CHECK_SHADER(L, 1);
    lua_pushfstring(L, "Object.Shader %s", shader->getName());
    return 1;
}