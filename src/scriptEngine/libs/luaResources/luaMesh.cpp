#include "scriptEngine/base/scriptEngine.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaResources/luaMesh.h"

void LuaMesh::luaLoadMesh(lua_State *L)
{
    luaL_Reg metaFuncs[] =
        {
            {"__tostring", LuaMesh::name},
            {"__len", LuaMesh::countIndexes},
            {"__eq", LuaMesh::eq},
            {"name", LuaMesh::name},
            {"indexes", LuaMesh::countIndexes},
            {NULL, NULL}};
    BasicFunctions::createLibUserdata(L, MESH_METADATA, metaFuncs);
}

void LuaMesh::pushMesh(lua_State *L, Mesh *mesh)
{
    Mesh **pointer = (Mesh **)lua_newuserdata(L, sizeof(void *));
    *pointer = mesh;
    luaL_getmetatable(L, MESH_METADATA);
    lua_setmetatable(L, -2);
}

int LuaMesh::name(lua_State *L)
{
    Mesh *mesh = CHECK_MESH(L);
    lua_pushfstring(L, "%s", mesh->getName()->c_str());
    return 1;
}

int LuaMesh::countIndexes(lua_State *L)
{
    Mesh *mesh = CHECK_MESH(L);
    lua_pushnumber(L, mesh->getCountIndexes());
    return 1;
}

int LuaMesh::eq(lua_State *L)
{
    Mesh *mesh1 = CHECK_MESH(L);
    Mesh *mesh2 = (*((Mesh **)luaL_checkudata(L, 2, MESH_METADATA)));
    if (mesh1 == mesh2)
    {
        lua_pushboolean(L, 1);
        return 1;
    }
    else
    {
        lua_pushboolean(L, 0);
        return 1;
    }
}