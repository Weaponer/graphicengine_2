#include <vector>
#include "engineBase/sandboxGameObjects/sandboxGameObjects.h"

#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaComponents/luaPointLightComponent.h"
#include "scriptEngine/libs/luaVector.h"
#include "scriptEngine/base/scriptEngine.h"


void LuaPointLightComponent::luaLoadPointLightComponent(lua_State *L, int size, const luaL_Reg *functions)
{
    std::vector<luaL_Reg> funcs;
    for (int i = 0; i < size; i++)
    {
        funcs.push_back(functions[i]);
    }
    funcs.push_back({"range", LuaPointLightComponent::range});
    funcs.push_back({"setRange", LuaPointLightComponent::setRange});
    funcs.push_back({"intensivity", LuaPointLightComponent::intensivity});
    funcs.push_back({"setIntensivity", LuaPointLightComponent::setIntensivity});
    funcs.push_back({"isCreateShadow", LuaPointLightComponent::isCreateShadow});
    funcs.push_back({"setIsCreateShadow", LuaPointLightComponent::setIsCreateShadow});
    funcs.push_back({"color", LuaPointLightComponent::color});
    funcs.push_back({"setColor", LuaPointLightComponent::setColor});
    funcs.push_back({"__tostring", LuaPointLightComponent::toString});
    funcs.push_back({NULL, NULL});

    BasicFunctions::createLibUserdata(L, POINT_LIGHT_COMPONENT_METADATA, &(funcs[0]));
}

void LuaPointLightComponent::pushPointLightComponent(lua_State *L, Component *component)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, BasicFunctions::getLuaObject(L, POINT_LIGHT_COMPONENT_METADATA, component));
}

Component *LuaPointLightComponent::addComponent(lua_State *L, GameObject *_gameObject)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    const SpecialSystems *systems = engine->getSpecialSystems();
    LightPoint *lightPoint = new LightPoint(systems->graphicEngine);
    systems->sandboxGameObjects->addComponent(_gameObject, lightPoint);
    return lightPoint;
}

int LuaPointLightComponent::range(lua_State *L)
{
    LightPoint *lightPoint = CHECK_POINT_LIGHT_COMPONENT(L, 1);
    lua_pushnumber(L, lightPoint->getRange());
    return 1;
}

int LuaPointLightComponent::setRange(lua_State *L)
{
    LightPoint *lightPoint = CHECK_POINT_LIGHT_COMPONENT(L, 1);
    float value = lua_tonumber(L, 2);
    lightPoint->setRange(value);
    return 0;
}

int LuaPointLightComponent::intensivity(lua_State *L)
{
    LightPoint *lightPoint = CHECK_POINT_LIGHT_COMPONENT(L, 1);
    lua_pushnumber(L, lightPoint->getIntensivity());
    return 1;
}

int LuaPointLightComponent::setIntensivity(lua_State *L)
{
    LightPoint *lightPoint = CHECK_POINT_LIGHT_COMPONENT(L, 1);
    float value = lua_tonumber(L, 2);
    lightPoint->setIntensivity(value);
    return 0;
}

int LuaPointLightComponent::isCreateShadow(lua_State *L)
{
    LightPoint *lightPoint = CHECK_POINT_LIGHT_COMPONENT(L, 1);
    lua_pushboolean(L, lightPoint->getIsCreateShadows());
    return 1;
}

int LuaPointLightComponent::setIsCreateShadow(lua_State *L)
{
    LightPoint *lightPoint = CHECK_POINT_LIGHT_COMPONENT(L, 1);
    bool value = lua_toboolean(L, 2);
    lightPoint->setIsCreateShadows(value);
    return 0;
}

int LuaPointLightComponent::color(lua_State *L)
{
    LightPoint *lightPoint = CHECK_POINT_LIGHT_COMPONENT(L, 1);
    LuaVector::pushVector3(L, lightPoint->getColor());
    return 1;
}

int LuaPointLightComponent::setColor(lua_State *L)
{
    LightPoint *lightPoint = CHECK_POINT_LIGHT_COMPONENT(L, 1);
    mth::vec3 *vec = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    lightPoint->setColor(*vec);
    return 0;
}

int LuaPointLightComponent::toString(lua_State *L)
{
    lua_pushstring(L, "Point light component");
    return 1;
}