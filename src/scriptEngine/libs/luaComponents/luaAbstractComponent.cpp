#include "scriptEngine/base/scriptInstaller.h"
#include "engineBase/sandboxGameObjects/sandboxGameObjects.h"

#include "scriptEngine/libs/luaOverloadNil.h"
#include "scriptEngine/libs/luaComponents/luaAbstractComponent.h"
#include "scriptEngine/libs/luaGameObject.h"

#include "scriptEngine/libs/luaComponents/luaScriptComponent.h"
#include "scriptEngine/libs/luaComponents/luaPointLightComponent.h"
#include "scriptEngine/libs/luaComponents/luaMeshRendererComponent.h"
#include "scriptEngine/libs/luaComponents/luaDensityVolumeComponent.h"
#include "scriptEngine/libs/luaComponents/luaCameraComponent.h"
#include "scriptEngine/libs/luaComponents/luaPhysicsComponents/luaAbstractColliderComponent.h"
#include "scriptEngine/libs/luaComponents/luaPhysicsComponents/luaRigidbodyComponent.h"

void LuaAbstractComponent::luaLoadAllComponents(lua_State *L)
{
    luaL_Reg basicFunctions[] = {
        {"gameObject", LuaAbstractComponent::gameObject},
        {"active", LuaAbstractComponent::active},
        {"localActive", LuaAbstractComponent::localActive},
        {"setActive", LuaAbstractComponent::setActive},
        {"discard", LuaAbstractComponent::discard}};
    int size = (sizeof(basicFunctions) / sizeof(luaL_Reg));
    LuaScriptComponent::luaLoadScriptComponent(L, size, basicFunctions);
    LuaPointLightComponent::luaLoadPointLightComponent(L, size, basicFunctions);
    LuaMeshRendererComponent::luaLoadMeshRendererComponent(L, size, basicFunctions);
    LuaDensityVolumeComponent::luaLoadDensityVolumeComponent(L, size, basicFunctions);
    LuaCameraComponent::luaLoadCameraComponent(L, size, basicFunctions);

    LuaAbstractColliderComponent::luaLoadAllAbstractCollidersComponents(L, size, basicFunctions);
    LuaRigidbodyComponent::luaLoadRigidbodyComponent(L, size, basicFunctions);
}

int LuaAbstractComponent::gameObject(lua_State *L)
{
    if (lua_type(L, 1) == LUA_TTABLE)
    {
        lua_getfield(L, -1, INDEX_OBJECT);
        return 1;
    }
    else
    {
        Component *component = GET_ABSTRACT_COMPONENT(L, 1);
        lua_rawgeti(L, LUA_REGISTRYINDEX, LuaGameObject::getGameObject(L, component->getGameObject()));
        return 1;
    }
}

int LuaAbstractComponent::active(lua_State *L)
{
    if (lua_type(L, 1) == LUA_TTABLE)
    {
        while (true)
        {
            lua_getmetatable(L, -1);
            lua_remove(L, -2);
            lua_getfield(L, -1, "__index");
            lua_remove(L, -2);
            if (lua_type(L, -1) == LUA_TUSERDATA)
            {
                break;
            }
        }
    }
    Component *component = GET_ABSTRACT_COMPONENT(L, 1);
    lua_pushboolean(L, component->getActive());
    return 1;
}

int LuaAbstractComponent::localActive(lua_State *L)
{
    if (lua_type(L, 1) == LUA_TTABLE)
    {
        while (true)
        {
            lua_getmetatable(L, -1);
            lua_remove(L, -2);
            lua_getfield(L, -1, "__index");
            lua_remove(L, -2);
            if (lua_type(L, -1) == LUA_TUSERDATA)
            {
                break;
            }
        }
    }
    Component *component = GET_ABSTRACT_COMPONENT(L, 1);
    lua_pushboolean(L, component->getLocalActive());
    return 1;
}

int LuaAbstractComponent::setActive(lua_State *L)
{
    if (!lua_isboolean(L, 2))
    {
        return 0;
    }
    if (lua_type(L, 1) == LUA_TTABLE)
    {
        while (true)
        {
            lua_getmetatable(L, -1);
            lua_remove(L, -2);
            lua_getfield(L, -1, "__index");
            lua_remove(L, -2);
            if (lua_type(L, -1) == LUA_TUSERDATA)
            {
                break;
            }
        }
    }
    Component *component = GET_ABSTRACT_COMPONENT(L, 1);
    component->setActive(lua_toboolean(L, 2));
    return 0;
}

int LuaAbstractComponent::discard(lua_State *L)
{
    if (lua_type(L, 1) == LUA_TTABLE)
    {
        while (true)
        {
            lua_getmetatable(L, -1);
            lua_remove(L, -2);
            lua_getfield(L, -1, "__index");
            lua_remove(L, -2);
            if (lua_type(L, -1) == LUA_TUSERDATA)
            {
                break;
            }
        }
    }
    Component *component = GET_ABSTRACT_COMPONENT(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->sandboxGameObjects->removeComponent(component);
    return 0;
}