#include <vector>
#include "engineBase/sandboxGameObjects/sandboxGameObjects.h"

#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaComponents/luaScriptComponent.h"

void LuaScriptComponent::luaLoadScriptComponent(lua_State *L, int size, const luaL_Reg *functions)
{
    std::vector<luaL_Reg> funcs;
    for(int i = 0; i < size; i++)
    {
        funcs.push_back(functions[i]);
    }
    funcs.push_back({"setType", LuaScriptComponent::setType});
    funcs.push_back({"__tostring", LuaScriptComponent::toString});
    funcs.push_back({NULL, NULL});
    BasicFunctions::createLibUserdata(L, SCRIPT_COMPONENT_METADATA, &(funcs[0]));
}

void LuaScriptComponent::pushScripComponent(lua_State *L, Component *component)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, BasicFunctions::getLuaObject(L, SCRIPT_COMPONENT_METADATA, component));
}

Component *LuaScriptComponent::addComponent(lua_State *L, GameObject *_gameObject)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    const SpecialSystems *systems = engine->getSpecialSystems();
    ScriptComponent *scriptComponent = new ScriptComponent(engine);
    systems->sandboxGameObjects->addComponent(_gameObject, scriptComponent);
    return scriptComponent;
}

int LuaScriptComponent::setType(lua_State *L)
{
    ScriptComponent *scriptComp = CHECK_SCRIPT_COMPONENT(L, 1);
    const char *path = luaL_checkstring(L, 2);
    scriptComp->setType(path);
    return 0;
}

int LuaScriptComponent::toString(lua_State *L)
{
    lua_getfield(L, -1, INDEX_TYPE);
    return 1;
}