#include <vector>
#include <MTH/color.h>

#include "scriptEngine/libs/luaComponents/luaDensityVolumeComponent.h"
#include "scriptEngine/libs/luaColor.h"

#include "scriptEngine/libs/utilities/enumGenerator.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

#include "engineBase/sandboxGameObjects/sandboxGameObjects.h"

#include "scriptEngine/base/scriptEngine.h"

void LuaDensityVolumeComponent::luaLoadDensityVolumeComponent(lua_State *L, int size, const luaL_Reg *functions)
{
    std::vector<luaL_Reg> funcs;
    for (int i = 0; i < size; i++)
    {
        funcs.push_back(functions[i]);
    }
    funcs.push_back({"setDensityVolumeType", LuaDensityVolumeComponent::setDensityVolumeType});
    funcs.push_back({"densityVolumeType", LuaDensityVolumeComponent::densityVolumeType});
    funcs.push_back({"setDensityVolumePlacementPlane", LuaDensityVolumeComponent::setDensityVolumePlacementPlane});
    funcs.push_back({"densityVolumePlacementPlane", LuaDensityVolumeComponent::densityVolumePlacementPlane});
    funcs.push_back({"setDensity", LuaDensityVolumeComponent::setDensity});
    funcs.push_back({"density", LuaDensityVolumeComponent::density});
    funcs.push_back({"setColorVolume", LuaDensityVolumeComponent::setColorVolume});
    funcs.push_back({"colorVolume", LuaDensityVolumeComponent::colorVolume});
    funcs.push_back({"setPowGradient", LuaDensityVolumeComponent::setPowGradient});
    funcs.push_back({"powGradient", LuaDensityVolumeComponent::powGradient});
    funcs.push_back({"setPlacementPlaneGradient", LuaDensityVolumeComponent::setPlacementPlaneGradient});
    funcs.push_back({"placementPlaneGradient", LuaDensityVolumeComponent::placementPlaneGradient});
    funcs.push_back({"setStartDecay", LuaDensityVolumeComponent::setStartDecay});
    funcs.push_back({"startDecay", LuaDensityVolumeComponent::startDecay});
    funcs.push_back({"setEndDecay", LuaDensityVolumeComponent::setEndDecay});
    funcs.push_back({"endDecay", LuaDensityVolumeComponent::endDecay});
    funcs.push_back({"setEdgeFade", LuaDensityVolumeComponent::setEdgeFade});
    funcs.push_back({"edgeFade", LuaDensityVolumeComponent::edgeFade});
    funcs.push_back({"__tostring", LuaDensityVolumeComponent::toString});
    funcs.push_back({NULL, NULL});

    BasicFunctions::createLibUserdata(L, DENSITY_VOLUME_COMPONENT_METADATA, &(funcs[0]));

    EnumGenerator<int> enumDensityVolumeType;
    enumDensityVolumeType.startNewEnum(L, ENUM_DENSITY_VOLUME_TYPE, ENUM_DENSITY_VOLUME_TYPE_ITEM_METADATA);
    enumDensityVolumeType.addParameter(L, DensityVolume::DENSITY_VOLUME_TYPE_CUBE, "cube");
    enumDensityVolumeType.addParameter(L, DensityVolume::DENSITY_VOLUME_TYPE_SPHERE, "sphere");
    enumDensityVolumeType.addParameter(L, DensityVolume::DENSITY_VOLUME_TYPE_CYLINDER, "cylinder");
    enumDensityVolumeType.endEnum(L);

    EnumGenerator<int> enumDensityVolumePlacement;
    enumDensityVolumePlacement.startNewEnum(L, ENUM_DENSITY_VOLUME_PLACEMENT, ENUM_DENSITY_VOLUME_PLACEMENT_ITEM_METADATA);
    enumDensityVolumePlacement.addParameter(L, DensityVolume::DENSITY_VOLUME_PLACEMENT_PLANE_XY, "planeXY");
    enumDensityVolumePlacement.addParameter(L, DensityVolume::DENSITY_VOLUME_PLACEMENT_PLANE_XZ, "planeXZ");
    enumDensityVolumePlacement.addParameter(L, DensityVolume::DENSITY_VOLUME_PLACEMENT_PLANE_YZ, "planeYZ");
    enumDensityVolumePlacement.endEnum(L);
}

void LuaDensityVolumeComponent::pushDensityVolumeComponent(lua_State *L, Component *component)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, BasicFunctions::getLuaObject(L, DENSITY_VOLUME_COMPONENT_METADATA, component));
}

Component *LuaDensityVolumeComponent::addComponent(lua_State *L, GameObject *_gameObject)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    const SpecialSystems *systems = engine->getSpecialSystems();
    DensityVolume *densityVolume = new DensityVolume(systems->graphicEngine);
    systems->sandboxGameObjects->addComponent(_gameObject, densityVolume);
    return densityVolume;
}

void LuaDensityVolumeComponent::pushDensityVolumeType(lua_State *L, DensityVolume::DENSITY_VOLUME_TYPE _densityVolumeType)
{
    lua_getglobal(L, ENUM_DENSITY_VOLUME_TYPE);
    switch (_densityVolumeType)
    {
    case DensityVolume::DENSITY_VOLUME_TYPE_CUBE:
        lua_getfield(L, -1, "cube");
        lua_remove(L, -2);
        return;
    case DensityVolume::DENSITY_VOLUME_TYPE_SPHERE:
        lua_getfield(L, -1, "sphere");
        lua_remove(L, -2);
        return;
    case DensityVolume::DENSITY_VOLUME_TYPE_CYLINDER:
        lua_getfield(L, -1, "cylinder");
        lua_remove(L, -2);
        return;
    default:
        return;
    }
}

void LuaDensityVolumeComponent::pushDensityVolumePlacement(lua_State *L, DensityVolume::DENSITY_VOLUME_PLACEMENT_PLANE _densityVolumePlacement)
{
    lua_getglobal(L, ENUM_DENSITY_VOLUME_PLACEMENT);
    switch (_densityVolumePlacement)
    {
    case DensityVolume::DENSITY_VOLUME_PLACEMENT_PLANE_XY:
        lua_getfield(L, -1, "planeXY");
        lua_remove(L, -2);
        return;
    case DensityVolume::DENSITY_VOLUME_PLACEMENT_PLANE_XZ:
        lua_getfield(L, -1, "planeXZ");
        lua_remove(L, -2);
        return;
    case DensityVolume::DENSITY_VOLUME_PLACEMENT_PLANE_YZ:
        lua_getfield(L, -1, "planeYZ");
        lua_remove(L, -2);
        return;
    default:
        return;
    }
}

int LuaDensityVolumeComponent::toString(lua_State *L)
{
    lua_pushstring(L, "Density volume component");
    return 1;
}

int LuaDensityVolumeComponent::setDensityVolumeType(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    DensityVolume::DENSITY_VOLUME_TYPE type = (DensityVolume::DENSITY_VOLUME_TYPE)*CHECK_ENUM_DENSITY_VOLUME_TYPE_ITEM(L, 2);
    densityVolume->setDensityVolumeType(type);
    return 0;
}

int LuaDensityVolumeComponent::densityVolumeType(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    pushDensityVolumeType(L, densityVolume->getDensityVolumeType());
    return 1;
}

int LuaDensityVolumeComponent::setDensityVolumePlacementPlane(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    DensityVolume::DENSITY_VOLUME_PLACEMENT_PLANE placement = (DensityVolume::DENSITY_VOLUME_PLACEMENT_PLANE)*CHECK_ENUM_DENSITY_VOLUME_PLACEMENT_ITEM(L, 2);
    densityVolume->setDensityVolumePlacementPlane(placement);
    return 0;
}

int LuaDensityVolumeComponent::densityVolumePlacementPlane(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    pushDensityVolumePlacement(L, densityVolume->getDensityVolumePlacementPlane());
    return 1;
}

int LuaDensityVolumeComponent::setDensity(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    float value = luaL_checknumber(L, 2);
    densityVolume->setDensity(value);
    return 0;
}

int LuaDensityVolumeComponent::density(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    lua_pushnumber(L, densityVolume->getDensity());
    return 1;
}

int LuaDensityVolumeComponent::setColorVolume(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    mth::col color = *CHECK_COLOR(L, 2);
    densityVolume->setColorVolume(color);
    return 0;
}

int LuaDensityVolumeComponent::colorVolume(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    LuaColor::pushColor(L, densityVolume->getColorVolume());
    return 1;
}

int LuaDensityVolumeComponent::setPowGradient(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    float value = luaL_checknumber(L, 2);
    densityVolume->setPowGradient(value);
    return 0;
}

int LuaDensityVolumeComponent::powGradient(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    lua_pushnumber(L, densityVolume->getPowGradient());
    return 1;
}

int LuaDensityVolumeComponent::setPlacementPlaneGradient(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    DensityVolume::DENSITY_VOLUME_PLACEMENT_PLANE place = (DensityVolume::DENSITY_VOLUME_PLACEMENT_PLANE)*CHECK_ENUM_DENSITY_VOLUME_PLACEMENT_ITEM(L, 2);
    densityVolume->setPlacementPlaneGradient(place);
    return 0;
}

int LuaDensityVolumeComponent::placementPlaneGradient(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    pushDensityVolumePlacement(L, densityVolume->getDensityVolumePlacementPlane());
    return 1;
}

int LuaDensityVolumeComponent::setStartDecay(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    densityVolume->setStartDecay(luaL_checknumber(L, 2));
    return 0;
}

int LuaDensityVolumeComponent::startDecay(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    lua_pushnumber(L, densityVolume->getStartDecay());
    return 1;
}

int LuaDensityVolumeComponent::setEndDecay(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    densityVolume->setEndDecay(luaL_checknumber(L, 2));
    return 0;
}

int LuaDensityVolumeComponent::endDecay(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    lua_pushnumber(L, densityVolume->getEndDecay());
    return 1;
}

int LuaDensityVolumeComponent::setEdgeFade(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    densityVolume->setEdgeFade(luaL_checknumber(L, 2));
    return 0;
}

int LuaDensityVolumeComponent::edgeFade(lua_State *L)
{
    DensityVolume *densityVolume = CHECK_DENSITY_VOLUME_COMPONENT(L, 1);
    lua_pushnumber(L, densityVolume->getEdgeFade());
    return 1;
}