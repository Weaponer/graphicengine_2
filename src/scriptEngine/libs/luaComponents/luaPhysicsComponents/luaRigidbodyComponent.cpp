#include "scriptEngine/base/scriptEngine.h"
#include "engineBase/sandboxGameObjects/sandboxGameObjects.h"

#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaComponents/luaPhysicsComponents/luaAbstractColliderComponent.h"
#include "scriptEngine/libs/luaComponents/luaPhysicsComponents/luaRigidbodyComponent.h"
#include "scriptEngine/libs/luaVector.h"

void LuaRigidbodyComponent::luaLoadRigidbodyComponent(lua_State *L, int size, const luaL_Reg *functions)
{
    std::vector<luaL_Reg> funcs;
    for (int i = 0; i < size; i++)
    {
        funcs.push_back(functions[i]);
    }
    funcs.push_back({"__tostring", LuaRigidbodyComponent::toString});
    funcs.push_back({"mass", LuaRigidbodyComponent::mass});
    funcs.push_back({"setMass", LuaRigidbodyComponent::setMass});
    funcs.push_back({"kinematic", LuaRigidbodyComponent::kinematic});
    funcs.push_back({"setKinematic", LuaRigidbodyComponent::setKinematic});
    funcs.push_back({"addForce", LuaRigidbodyComponent::addForce});
    funcs.push_back({"addForceAtPoint", LuaRigidbodyComponent::addForceAtPoint});
    funcs.push_back({"addTorque", LuaRigidbodyComponent::addTorque});
    funcs.push_back({"velocity", LuaRigidbodyComponent::velocity});
    funcs.push_back({"angularVelocity", LuaRigidbodyComponent::angularVelocity});
    funcs.push_back({"setVelocity", LuaRigidbodyComponent::setVelocity});
    funcs.push_back({"setAngularVelocity", LuaRigidbodyComponent::setAngularVelocity});
    funcs.push_back({"disconnectCollider", LuaRigidbodyComponent::disconnectCollider});
    funcs.push_back({"connectCollider", LuaRigidbodyComponent::connectCollider});
    funcs.push_back({NULL, NULL});

    BasicFunctions::createLibUserdata(L, RIGIDBODY_COMPONENT_METADATA, &(funcs[0]));
}

void LuaRigidbodyComponent::pushRigidbodyComponent(lua_State *L, Component *component)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, BasicFunctions::getLuaObject(L, RIGIDBODY_COMPONENT_METADATA, component));
}

Component *LuaRigidbodyComponent::addComponent(lua_State *L, GameObject *_gameObject)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    const SpecialSystems *systems = engine->getSpecialSystems();
    Rigidbody *rigidbody = new Rigidbody(systems->physicsEngine);
    systems->sandboxGameObjects->addComponent(_gameObject, rigidbody);
    return rigidbody;
}

int LuaRigidbodyComponent::toString(lua_State *L)
{
    lua_pushstring(L, "Rigidbody component");
    return 1;
}

int LuaRigidbodyComponent::connectCollider(lua_State *L)
{
    Rigidbody *rb = CHECK_RIGIDBODY_COMPONENT(L, 1);
    ColliderComponent *cc = GET_ABSTRACT_COLLIDER_COMPONENT(L, 2);
    rb->connectCollider(cc);
    return 0;
}

int LuaRigidbodyComponent::disconnectCollider(lua_State *L)
{
    Rigidbody *rb = CHECK_RIGIDBODY_COMPONENT(L, 1);
    ColliderComponent *cc = GET_ABSTRACT_COLLIDER_COMPONENT(L, 2);
    rb->disconnectCollider(cc);
    return 0;
}

int LuaRigidbodyComponent::mass(lua_State *L)
{
    Rigidbody *rb = CHECK_RIGIDBODY_COMPONENT(L, 1);
    lua_pushnumber(L, rb->getMass());
    return 1;
}

int LuaRigidbodyComponent::setMass(lua_State *L)
{
    Rigidbody *rb = CHECK_RIGIDBODY_COMPONENT(L, 1);
    float mass = luaL_checknumber(L, 2);
    rb->setMass(mass);
    return 0;
}

int LuaRigidbodyComponent::kinematic(lua_State *L)
{
    Rigidbody *rb = CHECK_RIGIDBODY_COMPONENT(L, 1);
    lua_pushboolean(L, rb->getKinematic());
    return 1;
}

int LuaRigidbodyComponent::setKinematic(lua_State *L)
{
    Rigidbody *rb = CHECK_RIGIDBODY_COMPONENT(L, 1);
    bool kinematic = lua_toboolean(L, 2);
    rb->setKinematic(kinematic);
    return 0;
}

int LuaRigidbodyComponent::addForce(lua_State *L)
{
    Rigidbody *rb = CHECK_RIGIDBODY_COMPONENT(L, 1);
    mth::vec3 *force = CHECK_VECTOR_3(L, 2);
    rb->addForce(*force);
    return 0;
}

int LuaRigidbodyComponent::addForceAtPoint(lua_State *L)
{
    Rigidbody *rb = CHECK_RIGIDBODY_COMPONENT(L, 1);
    mth::vec3 *force = CHECK_VECTOR_3(L, 2);
    mth::vec3 *point = CHECK_VECTOR_3(L, 3);
    rb->addForceAtPoint(*force, *point);
    return 0;
}

int LuaRigidbodyComponent::addTorque(lua_State *L)
{
    Rigidbody *rb = CHECK_RIGIDBODY_COMPONENT(L, 1);
    mth::vec3 *torque = CHECK_VECTOR_3(L, 2);
    rb->addTorque(*torque);
    return 0;
}

int LuaRigidbodyComponent::velocity(lua_State *L)
{
    Rigidbody *rb = CHECK_RIGIDBODY_COMPONENT(L, 1);
    LuaVector::pushVector3(L, rb->getVelocity());
    return 1;
}

int LuaRigidbodyComponent::angularVelocity(lua_State *L)
{
    Rigidbody *rb = CHECK_RIGIDBODY_COMPONENT(L, 1);
    LuaVector::pushVector3(L, rb->getAngularVelocity());
    return 1;
}

int LuaRigidbodyComponent::setVelocity(lua_State *L)
{
    Rigidbody *rb = CHECK_RIGIDBODY_COMPONENT(L, 1);
    mth::vec3 *velocity = CHECK_VECTOR_3(L, 2);
    rb->setVelocity(*velocity);
    return 0;
}

int LuaRigidbodyComponent::setAngularVelocity(lua_State *L)
{
    Rigidbody *rb = CHECK_RIGIDBODY_COMPONENT(L, 1);
    mth::vec3 *angularVelocity = CHECK_VECTOR_3(L, 2);
    rb->setAngularVelocity(*angularVelocity);
    return 0;
}