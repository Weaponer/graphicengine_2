#include <vector>
#include <MTH/vectors.h>

#include "engineBase/sandboxGameObjects/sandboxGameObjects.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/base/scriptEngine.h"
#include "scriptEngine/libs/luaVector.h"
#include "scriptEngine/libs/luaComponents/luaPhysicsComponents/luaBoxColliderComponent.h"

void LuaBoxColliderComponent::luaLoadBoxColliderComponent(lua_State *L, int size, const luaL_Reg *functions)
{
    std::vector<luaL_Reg> funcs;
    for (int i = 0; i < size; i++)
    {
        funcs.push_back(functions[i]);
    }
    funcs.push_back({"__tostring", LuaBoxColliderComponent::toString});
    funcs.push_back({"size", LuaBoxColliderComponent::size});
    funcs.push_back({"setSize", LuaBoxColliderComponent::setSize});
    funcs.push_back({NULL, NULL});

    BasicFunctions::createLibUserdata(L, BOX_COLLIDER_COMPONENT_METADATA, &(funcs[0]));
}

void LuaBoxColliderComponent::pushBoxColliderComponent(lua_State *L, Component *component)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, BasicFunctions::getLuaObject(L, BOX_COLLIDER_COMPONENT_METADATA, component));
}

Component *LuaBoxColliderComponent::addComponent(lua_State *L, GameObject *_gameObject)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    const SpecialSystems *systems = engine->getSpecialSystems();
    BoxColliderComponent *boxColliderComponent = new BoxColliderComponent(systems->mainDebugOutput, systems->physicsEngine);
    systems->sandboxGameObjects->addComponent(_gameObject, boxColliderComponent);
    return boxColliderComponent;
}

int LuaBoxColliderComponent::toString(lua_State *L)
{
    lua_pushstring(L, "Box collider component");
    return 1;
}

int LuaBoxColliderComponent::size(lua_State *L)
{
    BoxColliderComponent *bcc = CHECK_BOX_COLLIDER_COMPONENT(L, 1);
    LuaVector::pushVector3(L, bcc->getSize());
    return 1;
}

int LuaBoxColliderComponent::setSize(lua_State *L)
{
    BoxColliderComponent *bcc = CHECK_BOX_COLLIDER_COMPONENT(L, 1);
    mth::vec3* size = (mth::vec3*)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    bcc->setSize(*size);
    return 0;
}