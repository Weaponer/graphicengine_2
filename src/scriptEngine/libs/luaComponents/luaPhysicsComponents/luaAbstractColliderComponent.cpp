#include <vector>
#include <MTH/vectors.h>

#include "scriptEngine/base/scriptEngine.h"
#include "scriptEngine/libs/luaVector.h"
#include "scriptEngine/libs/luaComponents/luaPhysicsComponents/luaAbstractColliderComponent.h"

#include "scriptEngine/libs/luaComponents/luaPhysicsComponents/luaBoxColliderComponent.h"
#include "scriptEngine/libs/luaComponents/luaPhysicsComponents/luaSphereColliderComponent.h"

void LuaAbstractColliderComponent::luaLoadAllAbstractCollidersComponents(lua_State *L, int size, const luaL_Reg *functions)
{
    std::vector<luaL_Reg> funcs;
    for (int i = 0; i < size; i++)
    {
        funcs.push_back(functions[i]);
    }
    funcs.push_back({"origin", LuaAbstractColliderComponent::origin});
    funcs.push_back({"setOrigin", LuaAbstractColliderComponent::setOrigin});

    LuaBoxColliderComponent::luaLoadBoxColliderComponent(L, size, &(funcs[0]));
    LuaSphereColliderComponent::luaLoadSphereColliderComponent(L, size, &(funcs[0]));
}

int LuaAbstractColliderComponent::origin(lua_State *L)
{
    ColliderComponent *collider = GET_ABSTRACT_COLLIDER_COMPONENT(L, 1);
    LuaVector::pushVector3(L, collider->getPosition());
    return 1;
}

int LuaAbstractColliderComponent::setOrigin(lua_State *L)
{
    ColliderComponent *collider = GET_ABSTRACT_COLLIDER_COMPONENT(L, 1);
    mth::vec3* origin = (mth::vec3*)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    collider->setPosition(*origin);
    return 0;
}