#include <vector>

#include "engineBase/sandboxGameObjects/sandboxGameObjects.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/base/scriptEngine.h"
#include "scriptEngine/libs/luaComponents/luaPhysicsComponents/luaSphereColliderComponent.h"

void LuaSphereColliderComponent::luaLoadSphereColliderComponent(lua_State *L, int size, const luaL_Reg *functions)
{
    std::vector<luaL_Reg> funcs;
    for (int i = 0; i < size; i++)
    {
        funcs.push_back(functions[i]);
    }
    funcs.push_back({"__tostring", LuaSphereColliderComponent::toString});
    funcs.push_back({"radius", LuaSphereColliderComponent::radius});
    funcs.push_back({"setRadius", LuaSphereColliderComponent::setRadius});
    funcs.push_back({NULL, NULL});

    BasicFunctions::createLibUserdata(L, SPHERE_COLLIDER_COMPONENT_METADATA, &(funcs[0]));
}

void LuaSphereColliderComponent::pushSphereColliderComponent(lua_State *L, Component *component)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, BasicFunctions::getLuaObject(L, SPHERE_COLLIDER_COMPONENT_METADATA, component));
}

Component *LuaSphereColliderComponent::addComponent(lua_State *L, GameObject *_gameObject)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    const SpecialSystems *systems = engine->getSpecialSystems();
    SphereColliderComponent *sphereColliderComponent = new SphereColliderComponent(systems->mainDebugOutput, systems->physicsEngine);
    systems->sandboxGameObjects->addComponent(_gameObject, sphereColliderComponent);
    return sphereColliderComponent;
}

int LuaSphereColliderComponent::toString(lua_State *L)
{
    lua_pushstring(L, "Sphere collider component");
    return 1;
}

int LuaSphereColliderComponent::radius(lua_State *L)
{
    SphereColliderComponent *scc = CHECK_SPHERE_COLLIDER_COMPONENT(L, 1);
    lua_pushnumber(L, scc->getRadius());
    return 1;
}

int LuaSphereColliderComponent::setRadius(lua_State *L)
{
    SphereColliderComponent *scc = CHECK_SPHERE_COLLIDER_COMPONENT(L, 1);
    float radius = luaL_checknumber(L, 2);
    scc->setRadius(radius);
    return 0;
}