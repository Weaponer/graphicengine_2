#include "scriptEngine/libs/luaComponents/luaCameraComponent.h"

#include "engineBase/sandboxGameObjects/sandboxGameObjects.h"

#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/base/scriptEngine.h"

#include "scriptEngine/libs/luaMatrix4.h"

void LuaCameraComponent::luaLoadCameraComponent(lua_State *L, int size, const luaL_Reg *functions)
{
    std::vector<luaL_Reg> funcs;
    for (int i = 0; i < size; i++)
    {
        funcs.push_back(functions[i]);
    }
    funcs.push_back({"setOrtographicProjection", LuaCameraComponent::setOrtographicProjection});
    funcs.push_back({"setPerspectiveProjection", LuaCameraComponent::setPerspectiveProjection});
    funcs.push_back({"projectionMatrix", LuaCameraComponent::projectionMatrix});
    funcs.push_back({"__tostring", LuaCameraComponent::toString});
    funcs.push_back({NULL, NULL});

    BasicFunctions::createLibUserdata(L, CAMERA_COMPONENT_METADATA, &(funcs[0]));
}

void LuaCameraComponent::pushCameraComponent(lua_State *L, Component *component)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, BasicFunctions::getLuaObject(L, CAMERA_COMPONENT_METADATA, component));
}

Component *LuaCameraComponent::addComponent(lua_State *L, GameObject *_gameObject)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    const SpecialSystems *systems = engine->getSpecialSystems();
    Camera *camera = new Camera(systems->graphicEngine);
    systems->sandboxGameObjects->addComponent(_gameObject, camera);
    return camera;
}

int LuaCameraComponent::toString(lua_State *L)
{
    lua_pushstring(L, "Camera component");
    return 1;
}

int LuaCameraComponent::setOrtographicProjection(lua_State *L)
{
    Camera *camera = CHECK_CAMERA_COMPONENT(L, 1);
    float wight = luaL_checknumber(L, 2);
    float height = luaL_checknumber(L, 3);
    float near = luaL_checknumber(L, 4);
    float front = luaL_checknumber(L, 5);
    camera->setOrtographicProjection(wight, height, near, front);
    return 0;
}

int LuaCameraComponent::setPerspectiveProjection(lua_State *L)
{
    Camera *camera = CHECK_CAMERA_COMPONENT(L, 1);
    float angle = luaL_checknumber(L, 2);
    float sizeScreenX = luaL_checknumber(L, 3);
    float sizeScreenY = luaL_checknumber(L, 4);
    float near = luaL_checknumber(L, 5);
    float front = luaL_checknumber(L, 6);
    camera->setPerspectiveProjection(angle, sizeScreenX, sizeScreenY, near, front);
    return 0;
}

int LuaCameraComponent::projectionMatrix(lua_State *L)
{
    Camera *camera = CHECK_CAMERA_COMPONENT(L, 1);
    LuaMatrix4::pushMatrix4(L, camera->getProjectionMatrix());
    return 1;
}