#include <vector>
#include "engineBase/sandboxGameObjects/sandboxGameObjects.h"

#include "scriptEngine/libs/utilities/basicFunctions.h"
#include <scriptEngine/base/scriptEngine.h>
#include "scriptEngine/libs/luaComponents/luaMeshRendererComponent.h"
#include "scriptEngine/libs/luaResources/luaMaterial.h"
#include "scriptEngine/libs/luaResources/luaMesh.h"

#include "gameObject/graphicComponents/meshRenderer.h"
#include "renderEngine/resources/mesh.h"

void LuaMeshRendererComponent::luaLoadMeshRendererComponent(lua_State *L, int size, const luaL_Reg *functions)
{
    std::vector<luaL_Reg> funcs;
    for (int i = 0; i < size; i++)
    {
        funcs.push_back(functions[i]);
    }
    funcs.push_back({"mesh", LuaMeshRendererComponent::mesh});
    funcs.push_back({"setMesh", LuaMeshRendererComponent::setMesh});
    funcs.push_back({"material", LuaMeshRendererComponent::material});
    funcs.push_back({"setMaterial", LuaMeshRendererComponent::setMaterial});
    funcs.push_back({"countLods", LuaMeshRendererComponent::countLods});
    funcs.push_back({"addLod", LuaMeshRendererComponent::addLod});
    funcs.push_back({"removeLod", LuaMeshRendererComponent::removeLod});
    funcs.push_back({"meshInLod", LuaMeshRendererComponent::meshInLod});
    funcs.push_back({"materialInLod", LuaMeshRendererComponent::materialInLod});
    funcs.push_back({"heightCoefInLod", LuaMeshRendererComponent::heightCoefInLod});
    funcs.push_back({"setMeshInLod", LuaMeshRendererComponent::setMeshInLod});
    funcs.push_back({"setMaterialInLod", LuaMeshRendererComponent::setMaterialInLod});
    funcs.push_back({"setHeightCoefInLod", LuaMeshRendererComponent::setHeightCoefInLod});
    funcs.push_back({"generateShadows", LuaMeshRendererComponent::generateShadows});
    funcs.push_back({"setGenerateShadows", LuaMeshRendererComponent::setGenerateShadows});
    funcs.push_back({"__tostring", LuaMeshRendererComponent::toString});
    funcs.push_back({NULL, NULL});

    BasicFunctions::createLibUserdata(L, MESH_RENDERER_COMPONENT_METADATA, &(funcs[0]));
}

void LuaMeshRendererComponent::pushMeshRendererComponent(lua_State *L, Component *component)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, BasicFunctions::getLuaObject(L, MESH_RENDERER_COMPONENT_METADATA, component));
}

Component *LuaMeshRendererComponent::addComponent(lua_State *L, GameObject *_gameObject)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    const SpecialSystems *systems = engine->getSpecialSystems();
    MeshRenderer *meshRenderer = new MeshRenderer(systems->graphicEngine);
    systems->sandboxGameObjects->addComponent(_gameObject, meshRenderer);
    return meshRenderer;
}

int LuaMeshRendererComponent::toString(lua_State *L)
{
    lua_pushstring(L, "Mesh Renderer component");
    return 1;
}

int LuaMeshRendererComponent::mesh(lua_State *L)
{
    MeshRenderer *mr = CHECK_MESH_RENDERER_COMPONENT(L, 1);
    Mesh *mesh = mr->getMesh(0);
    if (mesh == NULL)
    {
        lua_pushnil(L);
        return 1;
    }
    LuaMesh::pushMesh(L, mesh);
    return 1;
}

int LuaMeshRendererComponent::setMesh(lua_State *L)
{
    MeshRenderer *mr = CHECK_MESH_RENDERER_COMPONENT(L, 1);
    Mesh *mesh = (Mesh *)(((luaObject *)luaL_checkudata(L, 2, MESH_METADATA))->object);
    mr->setMesh(mesh, 0);
    return 0;
}

int LuaMeshRendererComponent::material(lua_State *L)
{
    MeshRenderer *mr = CHECK_MESH_RENDERER_COMPONENT(L, 1);
    Material *material = mr->getMaterial(0);
    if (material == NULL)
    {
        lua_pushnil(L);
        return 1;
    }
    lua_rawgeti(L, LUA_REGISTRYINDEX, LuaMaterial::getMaterial(L, material));
    return 1;
}

int LuaMeshRendererComponent::setMaterial(lua_State *L)
{
    MeshRenderer *mr = CHECK_MESH_RENDERER_COMPONENT(L, 1);
    Material *mat = CHECK_MATERIAL_OR_NULL(L, 2);
    mr->setMaterial(mat, 0);
    return 0;
}

int LuaMeshRendererComponent::countLods(lua_State *L)
{
    MeshRenderer *mr = CHECK_MESH_RENDERER_COMPONENT(L, 1);
    lua_pushinteger(L, mr->coundLods());
    return 1;
}

int LuaMeshRendererComponent::addLod(lua_State *L)
{
    MeshRenderer *mr = CHECK_MESH_RENDERER_COMPONENT(L, 1);
    mr->addLod();
    return 0;
}

int LuaMeshRendererComponent::removeLod(lua_State *L)
{
    MeshRenderer *mr = CHECK_MESH_RENDERER_COMPONENT(L, 1);
    int index = std::max((int)luaL_checkinteger(L, 2), 0);
    mr->removeLod(index);
    return 0;
}

int LuaMeshRendererComponent::meshInLod(lua_State *L)
{
    MeshRenderer *mr = CHECK_MESH_RENDERER_COMPONENT(L, 1);
    int index = std::max((int)luaL_checkinteger(L, 2), 0);
    Mesh *mesh = mr->getMesh(index);
    if (mesh == NULL)
    {
        lua_pushnil(L);
        return 1;
    }
    LuaMesh::pushMesh(L, mesh);
    return 1;
}

int LuaMeshRendererComponent::materialInLod(lua_State *L)
{
    MeshRenderer *mr = CHECK_MESH_RENDERER_COMPONENT(L, 1);
    int index = std::max((int)luaL_checkinteger(L, 2), 0);
    Material *material = mr->getMaterial(index);
    if (material == NULL)
    {
        lua_pushnil(L);
        return 1;
    }
    lua_rawgeti(L, LUA_REGISTRYINDEX, LuaMaterial::getMaterial(L, material));
    return 1;
}

int LuaMeshRendererComponent::heightCoefInLod(lua_State *L)
{
    MeshRenderer *mr = CHECK_MESH_RENDERER_COMPONENT(L, 1);
    int index = std::max((int)luaL_checkinteger(L, 2), 0);
    lua_pushnumber(L, mr->getCoefHeightLod(index));
    return 1;
}

int LuaMeshRendererComponent::setMeshInLod(lua_State *L)
{
    MeshRenderer *mr = CHECK_MESH_RENDERER_COMPONENT(L, 1);
    int index = std::max((int)luaL_checkinteger(L, 2), 0);
    Mesh *mesh = (Mesh *)(((luaObject *)luaL_checkudata(L, 3, MESH_METADATA))->object);
    mr->setMesh(mesh, index);
    return 0;
}

int LuaMeshRendererComponent::setMaterialInLod(lua_State *L)
{
    MeshRenderer *mr = CHECK_MESH_RENDERER_COMPONENT(L, 1);
    int index = std::max((int)luaL_checkinteger(L, 2), 0);
    Material *mat = CHECK_MATERIAL_OR_NULL(L, 3);
    mr->setMaterial(mat, index);
    return 0;
}

int LuaMeshRendererComponent::setHeightCoefInLod(lua_State *L)
{
    MeshRenderer *mr = CHECK_MESH_RENDERER_COMPONENT(L, 1);
    int index = std::max((int)luaL_checkinteger(L, 2), 0);
    float heightCoef = luaL_checknumber(L, 3);
    mr->setHeightCoefLod(index, heightCoef);
    return 0;
}

int LuaMeshRendererComponent::generateShadows(lua_State *L)
{
    MeshRenderer *mr = CHECK_MESH_RENDERER_COMPONENT(L, 1);
    lua_pushboolean(L, mr->getGenerateShadows());
    return 1;
}

int LuaMeshRendererComponent::setGenerateShadows(lua_State *L)
{
    MeshRenderer *mr = CHECK_MESH_RENDERER_COMPONENT(L, 1);
    bool generate = lua_toboolean(L, 2);
    mr->setGenerateShadows(generate);
    return 0;
}