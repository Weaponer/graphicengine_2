#include "scriptEngine/libs/luaArrays/luaArrayFloat.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"


void LuaArrayFloat::luaLoadArrayFloat(lua_State *L)
{
    luaL_Reg metaFuncs[] =
        {
            {"__eq", LuaArrayFloat::equality},
            {"__tostring", LuaArrayFloat::toString},
            {"__len", LuaArrayFloat::length},
            {"get", LuaArrayFloat::get},
            {"set", LuaArrayFloat::set},
            {NULL, NULL}};

    BasicFunctions::createLibUserdata(L, ARRAY_FLOAT_METADATA, metaFuncs);

    luaL_Reg funcs[] =
        {
            {"new", LuaArrayFloat::newArrayFloat},
            {NULL, NULL}};
    BasicFunctions::createStaticLib(L, ARRAY_FLOAT_TABLE, funcs);
}

int LuaArrayFloat::newArrayFloat(lua_State *L)
{
    int count = lua_gettop(L);
    int sizeX = luaL_checkinteger(L, 1);
    int sizeY = 1;
    int sizeZ = 1;

    float defaultValue = 0;
    if(count >= 2)
    {
        sizeY = luaL_checkinteger(L, 2);
    }
    if(count >= 3)
    {
        sizeZ = luaL_checkinteger(L, 3);
    }
    if(count >= 4)
    {
        defaultValue = luaL_checknumber(L, 4);
    }
    int size = sizeX * sizeY * sizeZ;
    if(size <= 0)
    {
        luaL_error(L, "ArrayFloat: the length is less or equal to zero.");
    }
    
    int *array = (int *)lua_newuserdata(L, sizeof(int) * 3 + sizeof(float) * size);
    luaL_getmetatable(L, ARRAY_FLOAT_METADATA);
    lua_setmetatable(L, -2);

    array[0] = sizeX;
    array[1] = sizeY;
    array[2] = sizeZ;

    float *arrFloat = reinterpret_cast<float *>(array + 3);
    for(int i = 0; i < size; i++)
    {
        arrFloat[i] = defaultValue;
    }
    return 1;
}

int LuaArrayFloat::get(lua_State *L)
{
    void *array = CHECK_ARRAY_FLOAT(L, 1);
    int count = lua_gettop(L);
    int x = luaL_checkinteger(L, 2);
    int y = 0;
    int z = 0;
    if(count == 3)
    {
        y = luaL_checkinteger(L, 3);
    }
    else if(count >= 4)
    {
        z = luaL_checkinteger(L, 4);
    }
    int *arInt = reinterpret_cast<int *>(array);
    int size = arInt[0] * arInt[1] * arInt[2];
    int index = z * arInt[0] * arInt[1] + y * arInt[0] + x;
    if(index >= size)
    {
        luaL_error(L, "ArrayFloat: index goes beyond the boundaries of the array.");
    }
    float *arrFloat = reinterpret_cast<float *>(arInt + 3);
    float val = arrFloat[index];
    lua_pushnumber(L, val);
    return 1;
}

int LuaArrayFloat::set(lua_State *L)
{
    int count = lua_gettop(L);
    void *array = CHECK_ARRAY_FLOAT(L, 1);
    float value = luaL_checknumber(L, 2);

    int x = luaL_checkinteger(L, 3);
    int y = 0;
    int z = 0;
     if(count == 4)
    {
        y = luaL_checkinteger(L, 4);
    }
    else if(count >= 5)
    {
        z = luaL_checkinteger(L, 5);
    }
    int *arInt = reinterpret_cast<int *>(array);
    int size = arInt[0] * arInt[1] * arInt[2];
    int index = z * arInt[0] * arInt[1] + y * arInt[0] + x;
    if(index >= size)
    {
        luaL_error(L, "ArrayFloat: index goes beyond the boundaries of the array.");
    }
    float *arrFloat = reinterpret_cast<float *>(arInt + 3);
    arrFloat[index] = value;
    return 0;    
}   

int LuaArrayFloat::length(lua_State *L)
{
    void *array = CHECK_ARRAY_FLOAT(L, 1);
    int *arInt = reinterpret_cast<int *>(array);
    lua_pushinteger(L, arInt[0] * arInt[1] * arInt[2]);
    return 1;
}

int LuaArrayFloat::toString(lua_State *L)
{
    lua_pushstring(L, ARRAY_FLOAT_TABLE);
    return 1;
}

int LuaArrayFloat::equality(lua_State *L)
{
    lua_pushboolean(L, CHECK_ARRAY_FLOAT(L, 1) == CHECK_ARRAY_FLOAT(L, 2));
    return 1;
}