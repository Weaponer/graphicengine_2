#include "scriptEngine/libs/luaArrays/luaArrays.h"

#include "scriptEngine/libs/luaArrays/luaArrayInt.h"
#include "scriptEngine/libs/luaArrays/luaArrayFloat.h"
#include "scriptEngine/libs/luaArrays/luaArrayBool.h"

void LuaArrays::luaLoadArrays(lua_State *L)
{
    LuaArrayInt::luaLoadArrayInt(L);
    LuaArrayFloat::luaLoadArrayFloat(L);
    LuaArrayBool::luaLoadArrayBool(L);
}