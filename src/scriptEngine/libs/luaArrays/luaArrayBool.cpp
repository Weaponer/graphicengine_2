#include "scriptEngine/libs/luaArrays/luaArrayBool.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"


void LuaArrayBool::luaLoadArrayBool(lua_State *L)
{
    luaL_Reg metaFuncs[] =
        {
            {"__eq", LuaArrayBool::equality},
            {"__tostring", LuaArrayBool::toString},
            {"__len", LuaArrayBool::length},
            {"get", LuaArrayBool::get},
            {"set", LuaArrayBool::set},
            {NULL, NULL}};

    BasicFunctions::createLibUserdata(L, ARRAY_BOOL_METADATA, metaFuncs);

    luaL_Reg funcs[] =
        {
            {"new", LuaArrayBool::newArrayBool},
            {NULL, NULL}};
    BasicFunctions::createStaticLib(L, ARRAY_BOOL_TABLE, funcs);
}

int LuaArrayBool::newArrayBool(lua_State *L)
{
    int count = lua_gettop(L);
    int sizeX = luaL_checkinteger(L, 1);
    int sizeY = 1;
    int sizeZ = 1;

    bool defaultValue = false;
    if(count >= 2)
    {
        sizeY = luaL_checkinteger(L, 2);
    }
    if(count >= 3)
    {
        sizeZ = luaL_checkinteger(L, 3);
    }
    if(count >= 4)
    {
        defaultValue = lua_toboolean(L, 4);
    }
    int size = sizeX * sizeY * sizeZ;
    if(size <= 0)
    {
        luaL_error(L, "ArrayBool: the length is less or equal to zero.");
    }
    
    int *array = (int *)lua_newuserdata(L, sizeof(int) * 3 + sizeof(bool) * size);
    luaL_getmetatable(L, ARRAY_BOOL_METADATA);
    lua_setmetatable(L, -2);

    array[0] = sizeX;
    array[1] = sizeY;
    array[2] = sizeZ;

    bool *arrBool = reinterpret_cast<bool *>(array + 3);
    for(int i = 0; i < size; i++)
    {
        arrBool[i] = defaultValue;
    }
    return 1;
}

int LuaArrayBool::get(lua_State *L)
{
    void *array = CHECK_ARRAY_BOOL(L, 1);
    int count = lua_gettop(L);
    int x = luaL_checkinteger(L, 2);
    int y = 0;
    int z = 0;
    if(count == 3)
    {
        y = luaL_checkinteger(L, 3);
    }
    else if(count >= 4)
    {
        z = luaL_checkinteger(L, 4);
    }
    int *arInt = reinterpret_cast<int *>(array);
    int size = arInt[0] * arInt[1] * arInt[2];
    int index = z * arInt[0] * arInt[1] + y * arInt[0] + x;
    if(index >= size)
    {
        luaL_error(L, "ArrayBool: index goes beyond the boundaries of the array.");
    }
    bool *arrBool = reinterpret_cast<bool *>(arInt + 3);
    bool val = arrBool[index];
    lua_pushboolean(L, val);
    return 1;
}

int LuaArrayBool::set(lua_State *L)
{
    int count = lua_gettop(L);
    void *array = CHECK_ARRAY_BOOL(L, 1);
    bool value = lua_toboolean(L, 2);

    int x = luaL_checkinteger(L, 3);
    int y = 0;
    int z = 0;
     if(count == 4)
    {
        y = luaL_checkinteger(L, 4);
    }
    else if(count >= 5)
    {
        z = luaL_checkinteger(L, 5);
    }
    int *arInt = reinterpret_cast<int *>(array);
    int size = arInt[0] * arInt[1] * arInt[2];
    int index = z * arInt[0] * arInt[1] + y * arInt[0] + x;
    if(index >= size)
    {
        luaL_error(L, "ArrayBool: index goes beyond the boundaries of the array.");
    }
    bool *arrBool = reinterpret_cast<bool *>(arInt + 3);
    arrBool[index] = value;
    return 0;    
}   

int LuaArrayBool::length(lua_State *L)
{
    void *array = CHECK_ARRAY_BOOL(L, 1);
    int *arInt = reinterpret_cast<int *>(array);
    lua_pushinteger(L, arInt[0] * arInt[1] * arInt[2]);
    return 1;
}

int LuaArrayBool::toString(lua_State *L)
{
    lua_pushstring(L, ARRAY_BOOL_TABLE);
    return 1;
}

int LuaArrayBool::equality(lua_State *L)
{
    lua_pushboolean(L, CHECK_ARRAY_BOOL(L, 1) == CHECK_ARRAY_BOOL(L, 2));
    return 1;
}