#include "scriptEngine/libs/luaArrays/luaArrayInt.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

void LuaArrayInt::luaLoadArrayInt(lua_State *L)
{
    luaL_Reg metaFuncs[] =
        {
            {"__eq", LuaArrayInt::equality},
            {"__tostring", LuaArrayInt::toString},
            {"__len", LuaArrayInt::length},
            {"get", LuaArrayInt::get},
            {"set", LuaArrayInt::set},
            {NULL, NULL}};

    BasicFunctions::createLibUserdata(L, ARRAY_INT_METADATA, metaFuncs);

    luaL_Reg funcs[] =
        {
            {"new", LuaArrayInt::newArrayInt},
            {NULL, NULL}};
    BasicFunctions::createStaticLib(L, ARRAY_INT_TABLE, funcs);
}

int LuaArrayInt::newArrayInt(lua_State *L)
{
    int count = lua_gettop(L);
    int sizeX = luaL_checkinteger(L, 1);
    int sizeY = 1;
    int sizeZ = 1;

    int defaultValue = 0;
    if(count >= 2)
    {
        sizeY = luaL_checkinteger(L, 2);
    }
    if(count >= 3)
    {
        sizeZ = luaL_checkinteger(L, 3);
    }
    if(count >= 4)
    {
        defaultValue = luaL_checkinteger(L, 4);
    }
    int size = sizeX * sizeY * sizeZ;
    if(size <= 0)
    {
        luaL_error(L, "ArrayInt: the length is less or equal to zero.");
    }
    int fullSize = (size + 3);
    int *array = (int *)lua_newuserdata(L, sizeof(int) * fullSize);
    for(int i = 0; i < fullSize; i++)
    {
        array[i] = defaultValue; 
    }
    
    array[0] = sizeX;
    array[1] = sizeY;
    array[2] = sizeZ;
    luaL_getmetatable(L, ARRAY_INT_METADATA);
    lua_setmetatable(L, -2);
    return 1;
}

int LuaArrayInt::get(lua_State *L)
{
    void *array = CHECK_ARRAY_INT(L, 1);
    int count = lua_gettop(L);
    int x = luaL_checkinteger(L, 2);
    int y = 0;
    int z = 0;
    if(count == 3)
    {
        y = luaL_checkinteger(L, 3);
    }
    else if(count >= 4)
    {
        z = luaL_checkinteger(L, 4);
    }
    int *arInt = reinterpret_cast<int *>(array);
    int size = arInt[0] * arInt[1] * arInt[2];
    int index = z * arInt[0] * arInt[1] + y * arInt[0] + x;
    if(index >= size)
    {
        luaL_error(L, "ArrayInt: index goes beyond the boundaries of the array.");
    }
    lua_pushinteger(L, (arInt + 3)[index]);
    return 1;
}

int LuaArrayInt::set(lua_State *L)
{
    int count = lua_gettop(L);
    void *array = CHECK_ARRAY_INT(L, 1);
    int value = luaL_checkinteger(L, 2);

    int x = luaL_checkinteger(L, 3);
    int y = 0;
    int z = 0;
     if(count == 4)
    {
        y = luaL_checkinteger(L, 4);
    }
    else if(count >= 5)
    {
        z = luaL_checkinteger(L, 5);
    }
    int *arInt = reinterpret_cast<int *>(array);
    int size = arInt[0] * arInt[1] * arInt[2];
    int index = z * arInt[0] * arInt[1] + y * arInt[0] + x;
    if(index >= size)
    {
        luaL_error(L, "ArrayInt: index goes beyond the boundaries of the array.");
    }
    (arInt + 3)[index] = value;
    return 0;    
}   

int LuaArrayInt::length(lua_State *L)
{
    void *array = CHECK_ARRAY_INT(L, 1);
    int *arInt = reinterpret_cast<int *>(array);
    lua_pushinteger(L, arInt[0] * arInt[1] * arInt[2]);
    return 1;
}

int LuaArrayInt::toString(lua_State *L)
{
    lua_pushstring(L, ARRAY_INT_TABLE);
    return 1;
}

int LuaArrayInt::equality(lua_State *L)
{
    lua_pushboolean(L, CHECK_ARRAY_INT(L, 1) == CHECK_ARRAY_INT(L, 2));
    return 1;
}