#include "window/window.h"

#include "scriptEngine/libs/luaScreen/luaScreen.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

void LuaScreen::luaLoadScreen(lua_State *L)
{
    luaL_Reg funcs[] = {
        {"width", LuaScreen::width},
        {"height", LuaScreen::height},
        {"mouseLock", LuaScreen::mouseLock},
        {"setMouseLock", LuaScreen::setMouseLock},
        {NULL, NULL}
    };
    BasicFunctions::createStaticLib(L, SCREEN_TABLE, funcs);
}

int LuaScreen::width(lua_State *L)
{
    lua_pushnumber(L, BasicFunctions::getScriptEngine(L)->getSpecialSystems()->window->getWidth());
    return 1;
}

int LuaScreen::height(lua_State *L)
{
    lua_pushnumber(L, BasicFunctions::getScriptEngine(L)->getSpecialSystems()->window->getHeight());
    return 1;
}

int LuaScreen::mouseLock(lua_State *L)
{
    lua_pushboolean(L, BasicFunctions::getScriptEngine(L)->getSpecialSystems()->window->isMouseLock());
    return 1;
}

int LuaScreen::setMouseLock(lua_State *L)
{
    bool isLock = lua_toboolean(L, 1);
    BasicFunctions::getScriptEngine(L)->getSpecialSystems()->window->setMouseLock(isLock);
    return 0;
}