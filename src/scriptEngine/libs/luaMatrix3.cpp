#include "scriptEngine/libs/luaMatrix3.h"
#include "scriptEngine/libs/luaVector.h"

#include "scriptEngine/libs/utilities/basicFunctions.h"

void LuaMatrix3::luaLoadMatrix3(lua_State *L)
{
    luaL_Reg metaFuncs[] =
        {
            {"__add", LuaMatrix3::add},
            {"__sub", LuaMatrix3::sub},
            {"__mul", LuaMatrix3::mul},
            {"__div", LuaMatrix3::div},
            {"__tostring", LuaMatrix3::toString},
            {"setOrientation", LuaMatrix3::setOrientation},
            {"transpose", LuaMatrix3::transpose},
            {"setSingleMatrix", LuaMatrix3::setSingleMatrix},
            {"transform", LuaMatrix3::transform},
            {"transformTranspose", LuaMatrix3::transformTranspose},
            {"inverse", LuaMatrix3::inverse},
            {"inverseRotate", LuaMatrix3::inverseRotate},
            {"inverseScale", LuaMatrix3::inverseScale},
            {"getVector", LuaMatrix3::getVector},
            {"getVectorTranspose", LuaMatrix3::getVectorTranspose},
            {"get", LuaMatrix3::get},
            {"set", LuaMatrix3::set},
            {NULL, NULL}};
    BasicFunctions::createLibUserdata(L, MATRIX_3_METADATA, metaFuncs);

    luaL_Reg mat3Funcs[] =
        {
            {"new", LuaMatrix3::newMatrix3},
            {NULL, NULL}};
    BasicFunctions::createStaticLib(L, "Matrix3", mat3Funcs);
}

void LuaMatrix3::pushMatrix3(lua_State *L, const mth::mat3 &mat)
{
    BasicFunctions::pushTObject<mth::mat3>(L, MATRIX_3_METADATA, mat);
}

int LuaMatrix3::newMatrix3(lua_State *L)
{
    int count = lua_gettop(L);
    mth::mat3 m;
    if (count >= 9)
    {
        m.m[0][0] = luaL_checknumber(L, 1);
        m.m[0][1] = luaL_checknumber(L, 2);
        m.m[0][2] = luaL_checknumber(L, 3);
        m.m[1][0] = luaL_checknumber(L, 4);
        m.m[1][1] = luaL_checknumber(L, 5);
        m.m[1][2] = luaL_checknumber(L, 6);
        m.m[2][0] = luaL_checknumber(L, 7);
        m.m[2][1] = luaL_checknumber(L, 8);
        m.m[2][2] = luaL_checknumber(L, 9);
    }
    else if (count >= 1)
    {
        mth::mat3 *mat = CHECK_MATRIX_3(L);
        m = *mat;
    }
    pushMatrix3(L, m);
    return 1;
}

int LuaMatrix3::setOrientation(lua_State *L)
{
    mth::mat3 *m = CHECK_MATRIX_3(L);
    mth::vec3 *v1 = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    mth::vec3 *v2 = (mth::vec3 *)luaL_checkudata(L, 3, VECTOR_3_METADATA);
    mth::vec3 *v3 = (mth::vec3 *)luaL_checkudata(L, 4, VECTOR_3_METADATA);
    m->setOrientation(*v1, *v2, *v3);
    return 1;
}

int LuaMatrix3::transpose(lua_State *L)
{
    mth::mat3 *m = CHECK_MATRIX_3(L);
    m->transpose();
    return 0;
}

int LuaMatrix3::setSingleMatrix(lua_State *L)
{
    mth::mat3 *m = CHECK_MATRIX_3(L);
    m->setSingleMatrix();
    return 0;
}

int LuaMatrix3::transform(lua_State *L)
{
    mth::mat3 *m = CHECK_MATRIX_3(L);
    mth::vec3 *v = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    LuaVector::pushVector3(L, m->transform(*v));
    return 1;
}

int LuaMatrix3::transformTranspose(lua_State *L)
{
    mth::mat3 *m = CHECK_MATRIX_3(L);
    mth::vec3 *v = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    LuaVector::pushVector3(L, m->transformTranspose(*v));
    return 1;
}

int LuaMatrix3::inverse(lua_State *L)
{
    mth::mat3 *m = CHECK_MATRIX_3(L);
    m->inverse();
    return 0;
}

int LuaMatrix3::inverseRotate(lua_State *L)
{
    mth::mat3 *m = CHECK_MATRIX_3(L);
    m->inverseRotate();
    return 0;
}

int LuaMatrix3::inverseScale(lua_State *L)
{
    mth::mat3 *m = CHECK_MATRIX_3(L);
    m->inverseScale();
    return 0;
}

int LuaMatrix3::getVector(lua_State *L)
{
    mth::mat3 *m = CHECK_MATRIX_3(L);
    int n = luaL_checknumber(L, 2);
    LuaVector::pushVector3(L, m->getVector(n));
    return 1;
}

int LuaMatrix3::getVectorTranspose(lua_State *L)
{
    mth::mat3 *m = CHECK_MATRIX_3(L);
    int n = luaL_checknumber(L, 2);
    LuaVector::pushVector3(L, m->getVectorTranspose(n));
    return 1;
}

int LuaMatrix3::add(lua_State *L)
{
    mth::mat3 *m1 = CHECK_MATRIX_3(L);
    mth::mat3 *m2 = (mth::mat3 *)luaL_checkudata(L, 2, MATRIX_3_METADATA);
    mth::mat3 m = (*m1) + (*m2);
    pushMatrix3(L, m);
    return 1;
}

int LuaMatrix3::sub(lua_State *L)
{
    mth::mat3 *m1 = CHECK_MATRIX_3(L);
    mth::mat3 *m2 = (mth::mat3 *)luaL_checkudata(L, 2, MATRIX_3_METADATA);
    mth::mat3 m = (*m1) - (*m2);
    pushMatrix3(L, m);
    return 1;
}

int LuaMatrix3::mul(lua_State *L)
{
    if (lua_type(L, 1) == LUA_TNUMBER)
    {
        float num = lua_tonumber(L, 1);
        mth::mat3 *m2 = (mth::mat3 *)luaL_checkudata(L, 2, MATRIX_3_METADATA);
        pushMatrix3(L, (*m2) * num);
        return 1;
    }
    else
    {
        mth::mat3 *m1 = CHECK_MATRIX_3(L);
        mth::mat3 *m2 = (mth::mat3 *)luaL_checkudata(L, 2, MATRIX_3_METADATA);
        mth::mat3 m = (*m1) * (*m2);
        pushMatrix3(L, m);
        return 1;
    }
}

int LuaMatrix3::div(lua_State *L)
{
    float num = lua_tonumber(L, 1);
    mth::mat3 *m2 = (mth::mat3 *)luaL_checkudata(L, 2, MATRIX_3_METADATA);
    pushMatrix3(L, (*m2) / num);
    return 1;
}

int LuaMatrix3::get(lua_State *L)
{
    mth::mat3 *m = CHECK_MATRIX_3(L);
    int num = luaL_checkunsigned(L, 2);
    int num2 = luaL_checkunsigned(L, 3);
    lua_pushnumber(L, m->m[num][num2]);
    return 1;
}

int LuaMatrix3::set(lua_State *L)
{
    mth::mat3 *m = CHECK_MATRIX_3(L);
    int num = luaL_checkunsigned(L, 2);
    int num2 = luaL_checkunsigned(L, 3);
    float val = luaL_checknumber(L, 4);
    m->m[num][num2] = val;
    return 0;
}

int LuaMatrix3::toString(lua_State *L)
{
    mth::mat3 *m = CHECK_MATRIX_3(L);
    lua_pushfstring(L, "%f %f %f\n%f %f %f\n%f %f %f",
                    m->m[0][0], m->m[0][1], m->m[0][2],
                    m->m[1][0], m->m[1][1], m->m[1][2],
                    m->m[2][0], m->m[2][1], m->m[2][2]);
    return 1;
}