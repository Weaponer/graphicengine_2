#include "scriptEngine/libs/luaGameObject.h"
#include "scriptEngine/libs/utilities/enumGenerator.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/base/scriptInstaller.h"
#include "scriptEngine/base/scriptEngine.h"
#include "scriptEngine/libs/luaVector.h"
#include "scriptEngine/libs/luaMatrix4.h"
#include "scriptEngine/libs/luaQuaternion.h"

#include "scriptEngine/libs/luaSceneManager/luaScene.h"
#include "scriptEngine/libs/luaLayer/luaLayer.h"

#include "scriptEngine/libs/luaComponents/luaScriptComponent.h"
#include "scriptEngine/libs/luaComponents/luaPointLightComponent.h"
#include "scriptEngine/libs/luaComponents/luaMeshRendererComponent.h"
#include "scriptEngine/libs/luaComponents/luaDensityVolumeComponent.h"
#include "scriptEngine/libs/luaComponents/luaCameraComponent.h"

#include "scriptEngine/libs/luaComponents/luaPhysicsComponents/luaBoxColliderComponent.h"
#include "scriptEngine/libs/luaComponents/luaPhysicsComponents/luaSphereColliderComponent.h"
#include "scriptEngine/libs/luaComponents/luaPhysicsComponents/luaRigidbodyComponent.h"

#include "engineBase/sandboxGameObjects/sandboxGameObjects.h"

LuaGameObject::componentsTypes LuaGameObject::components[] =
    {
        //{"script", 1, LuaSceneObject::addComponent<ScriptComponent>, LuaSceneObject::getComponent<ScriptComponent>, LuaScriptComponent::pushScripComponent},
        {"pointLight", 2, LuaPointLightComponent::addComponent, LuaGameObject::getComponent<LightPoint>,
         LuaGameObject::getComponents<LightPoint>, LuaPointLightComponent::pushPointLightComponent},
        {"meshRenderer", 3, LuaMeshRendererComponent::addComponent, LuaGameObject::getComponent<MeshRenderer>,
         LuaGameObject::getComponents<MeshRenderer>, LuaMeshRendererComponent::pushMeshRendererComponent},
        {"boxCollider", 4, LuaBoxColliderComponent::addComponent, LuaGameObject::getComponent<BoxColliderComponent>,
         LuaGameObject::getComponents<BoxColliderComponent>, LuaBoxColliderComponent::pushBoxColliderComponent},
        {"sphereCollider", 5, LuaSphereColliderComponent::addComponent, LuaGameObject::getComponent<SphereColliderComponent>,
         LuaGameObject::getComponents<SphereColliderComponent>, LuaSphereColliderComponent::pushSphereColliderComponent},
        {"rigidbody", 6, LuaRigidbodyComponent::addComponent, LuaGameObject::getComponent<Rigidbody>,
         LuaGameObject::getComponents<Rigidbody>, LuaRigidbodyComponent::pushRigidbodyComponent},
        {"camera", 7, LuaCameraComponent::addComponent, LuaGameObject::getComponent<Camera>,
         LuaGameObject::getComponents<Camera>, LuaCameraComponent::pushCameraComponent},
        {"densityVolume", 8, LuaDensityVolumeComponent::addComponent, LuaGameObject::getComponent<DensityVolume>,
         LuaGameObject::getComponents<DensityVolume>, LuaDensityVolumeComponent::pushDensityVolumeComponent}};

void LuaGameObject::luaLoadGameObject(lua_State *L)
{
    luaL_Reg metaFuncs[] =
        {
            {"name", LuaGameObject::getName},
            {"setName", LuaGameObject::setName},
            {"active", LuaGameObject::active},
            {"localActive", LuaGameObject::localActive},
            {"setActive", LuaGameObject::setActive},
            {"getComponent", LuaGameObject::getComponent},
            {"getComponents", LuaGameObject::getComponents},
            {"addComponent", LuaGameObject::addComponent},
            {"pos", LuaGameObject::pos},
            {"setPos", LuaGameObject::setPos},
            {"localPos", LuaGameObject::localPos},
            {"setLocalPos", LuaGameObject::setLocalPos},
            {"rotate", LuaGameObject::rotate},
            {"rotateE", LuaGameObject::rotateE},
            {"setRotate", LuaGameObject::setRotate},
            {"setRotateE", LuaGameObject::setRotateE},
            {"localRotate", LuaGameObject::localRotate},
            {"localRotateE", LuaGameObject::localRotateE},
            {"setLocalRotate", LuaGameObject::setLocalRotate},
            {"setLocalRotateE", LuaGameObject::setLocalRotateE},
            {"localScale", LuaGameObject::localScale},
            {"setLocalScale", LuaGameObject::setLocalScale},
            {"localMatrix", LuaGameObject::localMatrix},
            {"globalMatrix", LuaGameObject::globalMatrix},
            {"parent", LuaGameObject::parent},
            {"setParent", LuaGameObject::setParent},
            {"countChildren", LuaGameObject::countChildren},
            {"getChild", LuaGameObject::getChild},
            {"layer", LuaGameObject::layer},
            {"setLayer", LuaGameObject::setLayer},
            {"__tostring", LuaGameObject::tostring},
            {NULL, NULL}};
    BasicFunctions::createLibUserdata(L, GAME_OBJECT_METADATA, metaFuncs);

    luaL_Reg sceneObjectFuncs[] =
        {
            {"create", LuaGameObject::create},
            {"destroy", LuaGameObject::destroy},
            {"makeDontDestroy", LuaGameObject::makeDontDestroy},
            {"moveOnAnotherScene", LuaGameObject::moveOnAnotherScene},
            {NULL, NULL}};
    BasicFunctions::createStaticLib(L, "GameObject", sceneObjectFuncs);

    EnumGenerator<int> enumComponents;
    enumComponents.startNewEnum(L, ENUM_COMPONENT, COMPONENT_ITEM_METADATA);
    int lenght = sizeof(components) / sizeof(componentsTypes);
    for (int i = 0; i < lenght; i++)
    {
        enumComponents.addParameter(L, components[i].code, components[i].metadata);
    }
    enumComponents.endEnum(L);
}

int LuaGameObject::create(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    const char *name = "";
    Scene *scene = engine->getSpecialSystems()->sandboxGameObjects->getSceneManager()->getCurrentScene();
    if (lua_gettop(L) >= 2)
    {
        name = luaL_checkstring(L, 1);
        scene = CHECK_SCENE(L, 2);
    }
    else if (lua_gettop(L) >= 1)
    {
        if (lua_type(L, 1) == LUA_TUSERDATA && luaL_testudata(L, 1, SCENE_METADATA))
        {
            scene = CHECK_SCENE(L, 2);
        }
        else
        {
            name = luaL_checkstring(L, 1);
        }
    }
    GameObject *obj = engine->getSpecialSystems()->sandboxGameObjects->createNewGameObject(scene, name);
    lua_rawgeti(L, LUA_REGISTRYINDEX, BasicFunctions::getLuaObject(L, GAME_OBJECT_METADATA, obj));
    return 1;
}

int LuaGameObject::destroy(lua_State *L)
{
    GameObject *obj = CHECK_GAME_OBJECT(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->sandboxGameObjects->removeGameObject(obj);
    return 0;
}

int LuaGameObject::makeDontDestroy(lua_State *L)
{
    GameObject *obj = CHECK_GAME_OBJECT(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    SandboxGameObjects *sandbox = engine->getSpecialSystems()->sandboxGameObjects;
    sandbox->setGameObjectDontDestroy(obj);
    return 0;
}

int LuaGameObject::moveOnAnotherScene(lua_State *L)
{
    GameObject *obj = CHECK_GAME_OBJECT(L, 1);
    Scene *scene = CHECK_SCENE(L, 2);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    SandboxGameObjects *sandbox = engine->getSpecialSystems()->sandboxGameObjects;
    sandbox->moveGameObjectOnAnotherScene(obj, scene);
    return 0;
}

void LuaGameObject::pushGameObject(lua_State *L, GameObject *obj)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, BasicFunctions::getLuaObject(L, GAME_OBJECT_METADATA, obj));
}

int LuaGameObject::getGameObject(lua_State *L, GameObject *obj)
{
    return BasicFunctions::getLuaObject(L, GAME_OBJECT_METADATA, obj);
}

int LuaGameObject::getComponent(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    if (lua_type(L, 2) == LUA_TSTRING)
    {
        std::vector<ScriptComponent *> ar = std::vector<ScriptComponent *>(object->getComponents()->size());

        const char *type = luaL_checkstring(L, 2);
        Component **first = (Component **)(&(ar[0]));
        int count = object->getComponents<ScriptComponent>(first, ar.size());
        for (int i = 0; i < count; i++)
        {
            ObjectScriptData *osb = ar[i]->getObjectScriptData();
            if (osb != NULL)
            {
                bool isCur = false;

                int point = osb->getPointData();
                lua_rawgeti(L, LUA_REGISTRYINDEX, point);
                lua_getfield(L, -1, INDEX_TYPE);
                const char *curType = lua_tostring(L, -1);
                if (strcmp(curType, type) == 0)
                {
                    isCur = true;
                }
                lua_settop(L, -2);
                if (isCur)
                {
                    return 1;
                }
            }
        }
        return 0;
    }
    else
    {
        int num = *CHECK_COMPONENT_ITEM(L, 2);
        int lenght = sizeof(components) / sizeof(componentsTypes);
        for (int i = 0; i < lenght; i++)
        {
            if (components[i].code == num)
            {
                components[i].pushComponent(L, components[i].getComponent(object));
                return 1;
            }
        }
    }
    return 0;
}

int LuaGameObject::getComponents(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    int pushedComponents = 0;
    if (lua_type(L, 2) == LUA_TSTRING)
    {
        std::vector<ScriptComponent *> ar = std::vector<ScriptComponent *>(object->getComponents()->size());
        const char *type = luaL_checkstring(L, 2);
        Component **first = (Component **)(&(ar[0]));
        int count = object->getComponents<ScriptComponent>(first, ar.size());

        for (int i = 0; i < count; i++)
        {
            if (lua_checkstack(L, 3) == false)
            {
                break;
            }

            ObjectScriptData *osb = ar[i]->getObjectScriptData();
            if (osb == NULL)
            {
                continue;
            }

            bool isCur = false;
            int point = osb->getPointData();
            lua_rawgeti(L, LUA_REGISTRYINDEX, point);
            lua_getfield(L, -1, INDEX_TYPE);
            const char *curType = lua_tostring(L, -1);
            if (strcmp(curType, type) == 0)
            {
                isCur = true;
            }
            lua_remove(L, -1);

            if (isCur)
            {
                pushedComponents++;
            }
        }
    }
    else
    {
        int num = *CHECK_COMPONENT_ITEM(L, 2);
        int lenght = sizeof(components) / sizeof(componentsTypes);
        for (int i = 0; i < lenght; i++)
        {
            if (components[i].code == num)
            {
                std::vector<Component *> comps;
                int countComponents = components[i].getComponents(object, &comps);

                for (int i2 = 0; i2 < countComponents; i2++)
                {
                    if (lua_checkstack(L, 3) == false)
                    {
                        break;
                    }

                    components[i].pushComponent(L, comps[i2]);
                    pushedComponents++;
                }
            }
        }
    }
    return pushedComponents;
}

int LuaGameObject::addComponent(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    if (lua_type(L, 2) == LUA_TSTRING)
    {
        const char *type = luaL_checkstring(L, 2);
        ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
        ScriptComponent *script = new ScriptComponent(engine);
        engine->getSpecialSystems()->sandboxGameObjects->addComponent(object, script);
        script->setType(type);
        lua_remove(L, -1);
        lua_rawgeti(L, LUA_REGISTRYINDEX, script->getObjectScriptData()->getPointData());
        return 1;
    }
    else
    {
        int num = *CHECK_COMPONENT_ITEM(L, 2);
        int lenght = sizeof(components) / sizeof(componentsTypes);
        for (int i = 0; i < lenght; i++)
        {
            if (components[i].code == num)
            {
                components[i].pushComponent(L, components[i].addComponent(L, object));
                return 1;
            }
        }
    }
    return 0;
}

int LuaGameObject::getName(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    lua_pushstring(L, object->getName());
    return 1;
}

int LuaGameObject::setName(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    const char *name = lua_tostring(L, 2);

    if (name != NULL)
    {
        object->setName(name);
    }
    return 0;
}

int LuaGameObject::active(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    lua_pushboolean(L, object->getActive());
    return 1;
}

int LuaGameObject::localActive(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    lua_pushboolean(L, object->getLocalActive());
    return 1;
}

int LuaGameObject::setActive(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    if (!lua_isboolean(L, 2))
    {
        return 0;
    }
    object->setActive(lua_toboolean(L, 2));
    return 0;
}

int LuaGameObject::pos(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    LuaVector::pushVector3(L, object->getTransform()->getPosition());
    return 1;
}

int LuaGameObject::setPos(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    mth::vec3 *vec = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    object->getTransform()->setPosition(*vec);
    return 0;
}

int LuaGameObject::localPos(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    LuaVector::pushVector3(L, object->getTransform()->getLocalPosition());
    return 1;
}

int LuaGameObject::setLocalPos(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    mth::vec3 *vec = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    object->getTransform()->setLocalPosition(*vec);
    return 0;
}

int LuaGameObject::rotate(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    LuaQuaternion::pushQuaternion(L, object->getTransform()->getQuaternion());
    return 1;
}

int LuaGameObject::rotateE(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    LuaVector::pushVector3(L, object->getTransform()->getQuaternion().getEuler());
    return 1;
}

int LuaGameObject::setRotate(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    mth::quat *quat = (mth::quat *)luaL_checkudata(L, 2, QUATERNION_METADATA);
    object->getTransform()->setQuaternion(*quat);
    return 0;
}

int LuaGameObject::setRotateE(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    mth::vec3 *vec = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    object->getTransform()->setRotationEuler(*vec);
    return 0;
}

int LuaGameObject::localRotate(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    LuaQuaternion::pushQuaternion(L, object->getTransform()->getLocalQuaternion());
    return 1;
}

int LuaGameObject::localRotateE(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    LuaVector::pushVector3(L, object->getTransform()->getLocalQuaternion().getEuler());
    return 1;
}

int LuaGameObject::setLocalRotate(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    mth::quat *quat = (mth::quat *)luaL_checkudata(L, 2, QUATERNION_METADATA);
    object->getTransform()->setLocalQuaternion(*quat);
    return 0;
}

int LuaGameObject::setLocalRotateE(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    mth::vec3 *vec = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    object->getTransform()->setLocalRotationEuler(*vec);
    return 0;
}

int LuaGameObject::localScale(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    LuaVector::pushVector3(L, object->getTransform()->getLocalScale());
    return 1;
}

int LuaGameObject::setLocalScale(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    mth::vec3 *vec = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    object->getTransform()->setScale(*vec);
    return 0;
}

int LuaGameObject::localMatrix(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    LuaMatrix4::pushMatrix4(L, object->getTransform()->getLocalMatrix());
    return 1;
}

int LuaGameObject::globalMatrix(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    LuaMatrix4::pushMatrix4(L, object->getTransform()->getGlobalMatrix());
    return 1;
}

int LuaGameObject::parent(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    Transform *parent = object->getTransform()->getParent();
    if (parent == NULL)
    {
        lua_pushnil(L);
    }
    else
    {
        pushGameObject(L, parent->getGameObject());
    }
    return 1;
}

int LuaGameObject::setParent(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    if (lua_gettop(L) == 2)
    {
        SandboxGameObjects *sandbox = BasicFunctions::getScriptEngine(L)->getSpecialSystems()->sandboxGameObjects;
        if (lua_type(L, 2) == LUA_TNIL)
        {
            if (object->getTransform()->getParent() != NULL)
            {
                sandbox->unparentGameObject(object);
            }
        }
        else
        {
            GameObject *destination = CHECK_GAME_OBJECT(L, 2);
            int error = sandbox->parentGameObject(object, destination);
            if (error)
            {
                luaL_error(L, "GameObjects cannot be connected.");
            }
        }
    }
    return 0;
}

int LuaGameObject::countChildren(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    lua_pushnumber(L, object->getTransform()->getCountChildren());
    return 1;
}

int LuaGameObject::getChild(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    int index = luaL_checkinteger(L, 2);
    Transform *child = object->getTransform()->getChildren(index);
    if (child == NULL)
    {
        lua_pushnil(L);
    }
    else
    {
        pushGameObject(L, child->getGameObject());
    }
    return 1;
}

int LuaGameObject::layer(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    LuaLayer::pushLayer(L, object->getLayer());
    return 1;
}

int LuaGameObject::setLayer(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    Layer layer = CHECK_LAYER(L, 2);
    object->setLayer(layer);
    return 1;
}

int LuaGameObject::tostring(lua_State *L)
{
    GameObject *object = CHECK_GAME_OBJECT(L, 1);
    lua_pushfstring(L, "GameObject: %s", object->getName());
    return 1;
}