#include "scriptEngine/libs/luaQuaternion.h"
#include "scriptEngine/libs/luaVector.h"
#include "scriptEngine/libs/luaMatrix4.h"
#include "scriptEngine/libs/luaMatrix3.h"

#include "scriptEngine/libs/utilities/basicFunctions.h"

void LuaQuaternion::luaLoadQuaternion(lua_State *L)
{
    luaL_Reg metaFuncs[] =
        {
            {"__mul", LuaQuaternion::mul},
            {"__tostring", LuaQuaternion::toString},
            {"__len", LuaQuaternion::magnitude},
            {"__eq", LuaQuaternion::equality},
            {"setDirect", LuaQuaternion::setDirect},
            {"setEuler", LuaQuaternion::setEuler},
            {"setMatrix", LuaQuaternion::setMatrix},
            {"getEuler", LuaQuaternion::getEuler},
            {"getMatrix", LuaQuaternion::getMatrix},
            {"magnitude", LuaQuaternion::magnitude},
            {"squareMagnitude", LuaQuaternion::squareMagnitude},
            {"normalize", LuaQuaternion::normalize},
            {"inverse", LuaQuaternion::inverse},
            {"addScaledVector", LuaQuaternion::addScaledVector},
            {"rotateByVector", LuaQuaternion::rotateByVector},
            {"rotateVector", LuaQuaternion::rotateVector},
            {"x", LuaQuaternion::x},
            {"y", LuaQuaternion::y},
            {"z", LuaQuaternion::z},
            {"w", LuaQuaternion::w},
            {"setX", LuaQuaternion::setX},
            {"setY", LuaQuaternion::setY},
            {"setZ", LuaQuaternion::setZ},
            {"setW", LuaQuaternion::setW},
            {NULL, NULL}};
    BasicFunctions::createLibUserdata(L, QUATERNION_METADATA, metaFuncs);

    luaL_Reg quaternionFuncs[] =
        {
            {"new", LuaQuaternion::newQuaternion},
            {NULL, NULL}};
    BasicFunctions::createStaticLib(L, "Quaternion", quaternionFuncs);
}

void LuaQuaternion::pushQuaternion(lua_State *L, const mth::quat &quat)
{
    BasicFunctions::pushTObject<mth::quat>(L, QUATERNION_METADATA, quat);
}

int LuaQuaternion::newQuaternion(lua_State *L)
{
    int count = lua_gettop(L);
    float x = 0;
    float y = 0;
    float z = 0;
    float w = 1;
    if (count >= 4)
    {
        x = luaL_checknumber(L, 1);
        y = luaL_checknumber(L, 2);
        z = luaL_checknumber(L, 3);
        w = luaL_checknumber(L, 4);
    }
    else if (count >= 1)
    {
        if (lua_type(L, 1) == LUA_TUSERDATA)
        {
            mth::quat *quat = CHECK_QUATERNION(L);
            if (quat == NULL)
            {
                return 0;
            }
            else
            {
                x = quat->x;
                y = quat->y;
                z = quat->z;
                w = quat->w;
            }
        }
        else
        {
            z = y = x = w = luaL_checknumber(L, 1);
        }
    }
    pushQuaternion(L, mth::quat(x, y, z, w));
    return 1;
}

int LuaQuaternion::setDirect(lua_State *L)
{
    if (lua_gettop(L) >= 5)
    {
        mth::quat *quat = CHECK_QUATERNION(L);
        float x = lua_tonumber(L, 2);
        float y = lua_tonumber(L, 3);
        float z = lua_tonumber(L, 4);
        float angs = lua_tonumber(L, 5);
        (*quat).setDirect(x, y, z, angs);
    }
    else
    {
        luaL_error(L, "setDirect: not enough arguments.");
    }
    return 0;
}

int LuaQuaternion::setMatrix(lua_State *L)
{
    mth::quat *quat = CHECK_QUATERNION(L);
    mth::mat3 *mat = (mth::mat3 *)luaL_checkudata(L, 2, MATRIX_3_METADATA);
    quat->setFromMatrix3(*mat);
    return 0;
}

int LuaQuaternion::setEuler(lua_State *L)
{
    if (lua_gettop(L) >= 4)
    {
        mth::quat *quat = CHECK_QUATERNION(L);
        float x = lua_tonumber(L, 2);
        float y = lua_tonumber(L, 3);
        float z = lua_tonumber(L, 4);
        (*quat).setEuler(x, y, z);
    }
    else
    {
        luaL_error(L, "setEuler: not enough arguments.");
    }
    return 0;
}

int LuaQuaternion::getEuler(lua_State *L)
{
    mth::quat *quat = CHECK_QUATERNION(L);
    LuaVector::pushVector3(L, quat->getEuler());
    return 1;
}

int LuaQuaternion::getMatrix(lua_State *L)
{
    mth::quat *quat = CHECK_QUATERNION(L);
    LuaMatrix4::pushMatrix4(L, quat->getMatrix());
    return 1;
}

int LuaQuaternion::magnitude(lua_State *L)
{
    mth::quat *quat = CHECK_QUATERNION(L);
    lua_pushnumber(L, quat->magnitude());
    return 1;
}

int LuaQuaternion::squareMagnitude(lua_State *L)
{
    mth::quat *quat = CHECK_QUATERNION(L);
    lua_pushnumber(L, quat->squareMagnitude());
    return 1;
}

int LuaQuaternion::normalize(lua_State *L)
{
    mth::quat *quat = CHECK_QUATERNION(L);
    quat->normalise();
    return 0;
}

int LuaQuaternion::inverse(lua_State *L)
{
    mth::quat *quat = CHECK_QUATERNION(L);
    quat->inverse();
    return 0;
}

int LuaQuaternion::mul(lua_State *L)
{
    mth::quat *oneQuat = (mth::quat *)luaL_checkudata(L, 1, QUATERNION_METADATA);
    mth::quat *twoQuat = (mth::quat *)luaL_checkudata(L, 2, QUATERNION_METADATA);
    if (oneQuat == NULL || twoQuat == NULL)
    {
        return 0;
    }
    mth::quat q = *oneQuat;
    q *= *twoQuat;
    pushQuaternion(L, q);
    return 1;
}

int LuaQuaternion::equality(lua_State *L)
{
    mth::quat *oneQuat = (mth::quat *)luaL_checkudata(L, 1, QUATERNION_METADATA);
    mth::quat *twoQuat = (mth::quat *)luaL_checkudata(L, 2, QUATERNION_METADATA);
    if (oneQuat == NULL || twoQuat == NULL)
    {
        return 0;
    }
    lua_pushboolean(L, ((*oneQuat) == (*twoQuat)));
    return 1;
}

int LuaQuaternion::addScaledVector(lua_State *L)
{
    float scale = 0;
    mth::vec3 *vec;
    if (lua_gettop(L) >= 3)
    {
        if (lua_type(L, 3) == LUA_TNUMBER)
        {
            scale = lua_tonumber(L, 2);
        }
        else
        {
            luaL_checknumber(L, 2);
            return 0;
        }
        vec = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
        if (vec == NULL)
        {
            return 0;
        }
    }
    else
    {
        luaL_error(L, "addScaledVector: not enough arguments.");
        return 0;
    }

    mth::quat *quat = CHECK_QUATERNION(L);
    quat->addScaledVector(*vec, scale);
    return 0;
}

int LuaQuaternion::rotateByVector(lua_State *L)
{
    mth::vec3 *vec;
    if (lua_gettop(L) >= 2)
    {
        vec = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
        if (vec == NULL)
        {
            return 0;
        }
    }
    else
    {
        luaL_error(L, "rotateByVector: not enough arguments.");
        return 0;
    }
    mth::quat *quat = CHECK_QUATERNION(L);
    quat->rotateByVector(*vec);
    return 0;
}

int LuaQuaternion::rotateVector(lua_State *L)
{
    mth::vec3 *vec;
    if (lua_gettop(L) >= 2)
    {
        vec = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
        if (vec == NULL)
        {
            return 0;
        }
    }
    else
    {
        luaL_error(L, "rotateVector: not enough arguments.");
        return 0;
    }
    mth::quat *quat = CHECK_QUATERNION(L);
    LuaVector::pushVector3(L, quat->rotateVector(*vec));
    return 1;
}

int LuaQuaternion::x(lua_State *L)
{
    mth::quat *quat = CHECK_QUATERNION(L);
    lua_pushnumber(L, quat->x);
    return 1;
}

int LuaQuaternion::y(lua_State *L)
{
    mth::quat *quat = CHECK_QUATERNION(L);
    lua_pushnumber(L, quat->y);
    return 1;
}

int LuaQuaternion::z(lua_State *L)
{
    mth::quat *quat = CHECK_QUATERNION(L);
    lua_pushnumber(L, quat->z);
    return 1;
}

int LuaQuaternion::w(lua_State *L)
{
    mth::quat *quat = CHECK_QUATERNION(L);
    lua_pushnumber(L, quat->w);
    return 1;
}

int LuaQuaternion::setX(lua_State *L)
{
    mth::quat *quat = CHECK_QUATERNION(L);
    float v = luaL_checknumber(L, 2);
    if (lua_type(L, 1) != LUA_TNUMBER)
    {
        return 0;
    }
    quat->x = v;
    return 0;
}

int LuaQuaternion::setY(lua_State *L)
{
    mth::quat *quat = CHECK_QUATERNION(L);
    float v = luaL_checknumber(L, 2);
    if (lua_type(L, 1) != LUA_TNUMBER)
    {
        return 0;
    }
    quat->y = v;
    return 0;
}

int LuaQuaternion::setZ(lua_State *L)
{
    mth::quat *quat = CHECK_QUATERNION(L);
    float v = luaL_checknumber(L, 2);
    if (lua_type(L, 1) != LUA_TNUMBER)
    {
        return 0;
    }
    quat->z = v;
    return 0;
}

int LuaQuaternion::setW(lua_State *L)
{
    mth::quat *quat = CHECK_QUATERNION(L);
    float v = luaL_checknumber(L, 2);
    if (lua_type(L, 1) != LUA_TNUMBER)
    {
        return 0;
    }
    quat->w = v;
    return 0;
}

int LuaQuaternion::toString(lua_State *L)
{
    mth::quat *quat = CHECK_QUATERNION(L);
    lua_pushfstring(L, "(%f, %f %f, %f)", quat->x, quat->y, quat->z, quat->w);
    return 1;
}