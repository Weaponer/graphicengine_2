#include "scriptEngine/libs/luaOverloadNil.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

void LuaOverloadNil::luaOverloadNil(lua_State *L)
{
    luaL_Reg nilfunc[] =
        {
            {"__eq", LuaOverloadNil::eq},
            {"__name", LuaOverloadNil::name},
            {"__tostring", LuaOverloadNil::tostring},
            {NULL, NULL}};

    BasicFunctions::createLibUserdata(L, NIL_METADATA, nilfunc);

    lua_pushnil(L);
    luaL_getmetatable(L, NIL_METADATA);
    lua_setmetatable(L, -2);
    lua_remove(L, -1);
    
    lua_pushcfunction(L, LuaOverloadNil::isValid);
    lua_setglobal(L, "isValid");
}

void LuaOverloadNil::luaMakeMetadataNil(lua_State *L, int _inx)
{
    if (_inx < 0)
    {
        _inx -= 1;
    }
    luaL_getmetatable(L, NIL_METADATA);
    lua_setmetatable(L, _inx);
}

int LuaOverloadNil::eq(lua_State *L)
{
    lua_pushboolean(L, true);
    return 1;
}

int LuaOverloadNil::name(lua_State *L)
{
    lua_pushfstring(L, NIL_NAME);
    return 1;
}

int LuaOverloadNil::tostring(lua_State *L)
{
    lua_pushfstring(L, NIL_NAME);
    return 1;
}

int LuaOverloadNil::isValid(lua_State *L)
{
    int top = lua_gettop(L);
    if (top == 1)
    {
        luaL_getmetatable(L, NIL_METADATA);
        lua_getmetatable(L, 1);
        if ((lua_type(L, -1) != LUA_TNIL && lua_compare(L, -2, -1, LUA_OPEQ)) || lua_type(L, 1) == LUA_TNIL)
        {
            lua_pushboolean(L, false);
            return 1;
        }
    }
    lua_pushboolean(L, true);
    return 1;
}