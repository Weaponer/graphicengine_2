#include "scriptEngine/libs/luaGraphic/luaSky.h"
#include "scriptEngine/libs/luaGraphic/luaGraphic.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/utilities/enumGenerator.h"
#include "scriptEngine/libs/luaResources/luaMaterial.h"
#include "scriptEngine/libs/luaVector.h"
#include "scriptEngine/libs/luaColor.h"
#include "renderEngine/graphicsPipeline/skyBufferRender.h"

void LuaSky::luaLoadSky(lua_State *L)
{
    luaL_Reg skyFunctions[] = {
        {"typeUpdateSky", LuaSky::typeUpdateSky},
        {"setTypeUpdateSky", LuaSky::setTypeUpdateSky},
        {"material", LuaSky::material},
        {"setMaterial", LuaSky::setMaterial},
        {"directionSun", LuaSky::directionSun},
        {"setDirectionSun", LuaSky::setDirectionSun},
        {"colorSun", LuaSky::colorSun},
        {"setColorSun", LuaSky::setColorSun},
        {"intensity", LuaSky::intensity},
        {"setIntensity", LuaSky::setIntensity},
        {"ambientLightColor", LuaSky::ambientLightColor},
        {"setAmbientLightColor", LuaSky::setAmbientLightColor},
        {"intensityAmbientLight", LuaSky::intensityAmbientLight},
        {"setIntensityAmbientLight", LuaSky::setIntensityAmbientLight},
        {"environmentBufferAsAmbientLight", LuaSky::environmentBufferAsAmbientLight},
        {"setEnvironmentBufferAsAmbientLight", LuaSky::setEnvironmentBufferAsAmbientLight},
        {"callUpdate", LuaSky::callUpdate},
        {NULL, NULL}};
    BasicFunctions::createSubStaticLib(L, GRAPHIC_TABLE, SKY_SUB_TABLE, skyFunctions);

    EnumGenerator<int> enumTypeUpdate;
    enumTypeUpdate.startNewEnum(L, TYPE_UPDATE_ENUM, TYPE_UPDATE_ITEM_METADATA);
    enumTypeUpdate.addParameter(L, TypeUpdateSky::EveryFrame, "everyFrame");
    enumTypeUpdate.addParameter(L, TypeUpdateSky::Call, "call");
    enumTypeUpdate.endEnum(L);
}

void LuaSky::pushTypeUpdateSky(lua_State *L, TypeUpdateSky _type)
{
    lua_getglobal(L, TYPE_UPDATE_ENUM);
    switch (_type)
    {
    case TypeUpdateSky::EveryFrame:
        lua_getfield(L, -1, "everyFrame");
        lua_remove(L, -2);
        return;
    case TypeUpdateSky::Call:
        lua_getfield(L, -1, "call");
        lua_remove(L, -2);
        return;
    default:
        return;
    }
}

int LuaSky::typeUpdateSky(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    TypeUpdateSky tus = engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->sky.typeUpdateSky;
    pushTypeUpdateSky(L, tus);
    return 1;
}

int LuaSky::setTypeUpdateSky(lua_State *L)
{
    TypeUpdateSky tus = (TypeUpdateSky)*CHECK_TYPE_UPDATE_SKY(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->sky.typeUpdateSky = tus;
    return 0;
}

int LuaSky::material(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    Material *sky = engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->sky.skyMaterial;
    if (sky == NULL)
    {
        lua_pushnil(L);
    }
    else
    {
        lua_rawgeti(L, LUA_REGISTRYINDEX, LuaMaterial::getMaterial(L, sky));
    }
    return 1;
}

int LuaSky::setMaterial(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    Material *mat = CHECK_MATERIAL_OR_NULL(L, 1);
    engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->sky.skyMaterial = mat;
    return 0;
}

int LuaSky::directionSun(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    LuaVector::pushVector3(L, engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->sky.directionSun);
    return 1;
}

int LuaSky::setDirectionSun(lua_State *L)
{
    mth::vec3 dir = *CHECK_VECTOR_3(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->sky.directionSun = dir;
    return 0;
}

int LuaSky::colorSun(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    LuaColor::pushColor(L, engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->sky.colorSun);
    return 1;
}

int LuaSky::setColorSun(lua_State *L)
{
    mth::col col = *CHECK_COLOR(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->sky.colorSun = col;
    return 0;
}

int LuaSky::intensity(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    lua_pushnumber(L, engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->sky.intensity);
    return 1;
}

int LuaSky::setIntensity(lua_State *L)
{
    float intensity = luaL_checknumber(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->sky.intensity = intensity;
    return 0;
}

int LuaSky::ambientLightColor(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    LuaColor::pushColor(L, engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->sky.ambientLightColor);
    return 1;
}

int LuaSky::setAmbientLightColor(lua_State *L)
{
    mth::col col = *CHECK_COLOR(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->sky.ambientLightColor = col;
    return 0;
}

int LuaSky::intensityAmbientLight(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    lua_pushnumber(L, engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->sky.intensityAmbientLight);
    return 1;
}

int LuaSky::setIntensityAmbientLight(lua_State *L)
{
    float intensity = luaL_checknumber(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->sky.intensityAmbientLight = intensity;
    return 0;
}

int LuaSky::environmentBufferAsAmbientLight(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    lua_pushboolean(L, engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->sky.environmentBufferAsAmbientLight);
    return 1;
}

int LuaSky::setEnvironmentBufferAsAmbientLight(lua_State *L)
{
    bool use = lua_toboolean(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->sky.environmentBufferAsAmbientLight = use;
    return 0;
}

int LuaSky::callUpdate(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->graphicEngine->getGraphicPipeline()->getSkyBufferRender()->callUpdate();
    return 0;
}