#include "scriptEngine/libs/luaGraphic/luaBloom.h"
#include "scriptEngine/libs/luaGraphic/luaGraphic.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

void LuaBloom::luaLoadBloom(lua_State *L)
{
    luaL_Reg BloomFunctions[] = {
        {"generateBloom", LuaBloom::generateBloom},
        {"setGenerateBloom", LuaBloom::setGenerateBloom},
        {"countSamples", LuaBloom::countSamples},
        {"setCountSamples", LuaBloom::setCountSamples},
        {"force", LuaBloom::force},
        {"setForce", LuaBloom::setForce},
        {NULL, NULL}};
    BasicFunctions::createSubStaticLib(L, GRAPHIC_TABLE, BLOOM_SUB_TABLE, BloomFunctions);
}

int LuaBloom::generateBloom(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    bool generateBloom = engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->postEffects.bloom.enable;
    lua_pushboolean(L, generateBloom);
    return 1;
}

int LuaBloom::setGenerateBloom(lua_State *L)
{
    bool generateBloom = lua_toboolean(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->postEffects.bloom.enable = generateBloom;
    return 0;
}

int LuaBloom::countSamples(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    int count = engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->postEffects.bloom.getCountSamples();
    lua_pushinteger(L, count);
    return 1;
}

int LuaBloom::setCountSamples(lua_State *L)
{
    int count = luaL_checkinteger(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->postEffects.bloom.setCountSamples(count);
    return 0;
}

int LuaBloom::force(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    float force = engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->postEffects.bloom.getForce();
    lua_pushnumber(L, force);
    return 1;
}

int LuaBloom::setForce(lua_State *L)
{
    float force = luaL_checknumber(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->postEffects.bloom.setForce(force);
    return 0;
}