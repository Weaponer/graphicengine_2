#include "scriptEngine/libs/luaGraphic/luaSSAO.h"
#include "scriptEngine/libs/luaGraphic/luaGraphic.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

void LuaSSAO::luaLoadSSAO(lua_State *L)
{
    luaL_Reg SSAOFunctions[] = {
        {"setGenerateSSAO", LuaSSAO::setGenerateSSAO},
        {"generateSSAO", LuaSSAO::generateSSAO},
        {NULL, NULL}};
    BasicFunctions::createSubStaticLib(L, GRAPHIC_TABLE, SSAO_SUB_TABLE, SSAOFunctions);
}

int LuaSSAO::setGenerateSSAO(lua_State *L)
{
    bool generateSSAO = lua_toboolean(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->postEffects.SSAO.enable = generateSSAO;
    return 0;
}

int LuaSSAO::generateSSAO(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    bool generateSSAO = engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline()->postEffects.SSAO.enable;
    lua_pushboolean(L, generateSSAO);
    return 1;
}