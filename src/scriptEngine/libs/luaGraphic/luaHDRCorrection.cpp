#include "scriptEngine/libs/luaGraphic/luaHDRCorrection.h"
#include "scriptEngine/libs/luaGraphic/luaGraphic.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

void LuaHDRCorrection::luaLoadHDRCorrection(lua_State *L)
{
    luaL_Reg hdrCorrectionFunctions[] = {
        {"dynamicHDRCorrection", LuaHDRCorrection::dynamicHDRCorrection},
        {"setDynamicHDRCorrection", LuaHDRCorrection::setDynamicHDRCorrection},
        {"staticValue", LuaHDRCorrection::staticValue},
        {"setStaticValue", LuaHDRCorrection::setStaticValue},
        {"minimumLogLuminance", LuaHDRCorrection::minimumLogLuminance},
        {"setMinimumLogLuminance", LuaHDRCorrection::setMinimumLogLuminance},
        {"inverseLogLuminanceRange", LuaHDRCorrection::inverseLogLuminanceRange},
        {"setInverseLogLuminanceRange", LuaHDRCorrection::setInverseLogLuminanceRange},
        {NULL, NULL}};
    BasicFunctions::createSubStaticLib(L, GRAPHIC_TABLE, HDR_CORRECTION_SUB_TABLE, hdrCorrectionFunctions);
}

static SettingsGraphicPipeline *getSettingsGraphicPipeline(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    return engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline();
}

int LuaHDRCorrection::dynamicHDRCorrection(lua_State *L)
{
    lua_pushboolean(L, getSettingsGraphicPipeline(L)->postEffects.hdrCorrection.useStaticValue);
    return 1;
}

int LuaHDRCorrection::setDynamicHDRCorrection(lua_State *L)
{
    bool staticValue = !lua_toboolean(L, 1);
    getSettingsGraphicPipeline(L)->postEffects.hdrCorrection.useStaticValue = staticValue;
    return 0;
}

int LuaHDRCorrection::staticValue(lua_State *L)
{
    lua_pushnumber(L, getSettingsGraphicPipeline(L)->postEffects.hdrCorrection.getStaticValue());
    return 1;
}

int LuaHDRCorrection::setStaticValue(lua_State *L)
{
    float val = lua_tonumber(L, 1);
    getSettingsGraphicPipeline(L)->postEffects.hdrCorrection.setStaticValue(val);
    return 0;
}

int LuaHDRCorrection::minimumLogLuminance(lua_State *L)
{
    lua_pushnumber(L, getSettingsGraphicPipeline(L)->postEffects.hdrCorrection.getMinimumLogLuminance());
    return 1;
}

int LuaHDRCorrection::setMinimumLogLuminance(lua_State *L)
{
    float val = lua_tonumber(L, 1);
    getSettingsGraphicPipeline(L)->postEffects.hdrCorrection.setMinimumLogLuminance(val);
    return 0;
}

int LuaHDRCorrection::inverseLogLuminanceRange(lua_State *L)
{
    lua_pushnumber(L, getSettingsGraphicPipeline(L)->postEffects.hdrCorrection.getInverseLogLuminanceRange());
    return 1;
}

int LuaHDRCorrection::setInverseLogLuminanceRange(lua_State *L)
{
    float val = lua_tonumber(L, 1);
    getSettingsGraphicPipeline(L)->postEffects.hdrCorrection.setInverseLogLuminanceRange(val);
    return 0;
}