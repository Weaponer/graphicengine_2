#include "scriptEngine/libs/luaGraphic/luaVolumetricFog.h"
#include "scriptEngine/libs/luaGraphic/luaGraphic.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaColor.h"
#include "scriptEngine/libs/luaVector.h"

void LuaVolumetricFog::luaLoadVolumetricFog(lua_State *L)
{
    luaL_Reg volumetricFogFunctions[] = {
        {"generateFog", LuaVolumetricFog::generateFog},
        {"setGenerateFog", LuaVolumetricFog::setGenerateFog},
        {"intensivityFog", LuaVolumetricFog::intensivityFog},
        {"setIntensivityFog", LuaVolumetricFog::setIntensivityFog},
        {"anisotropic", LuaVolumetricFog::anisotropic},
        {"setAnisotropic", LuaVolumetricFog::setAnisotropic},
        {"scattering", LuaVolumetricFog::scattering},
        {"setScattering", LuaVolumetricFog::setScattering},
        {"absorption", LuaVolumetricFog::absorption},
        {"setAbsorption", LuaVolumetricFog::setAbsorption},
        {"sizeGridFog", LuaVolumetricFog::sizeGridFog},
        {"setSizeGridFog", LuaVolumetricFog::setSizeGridFog},
        {"colorFog", LuaVolumetricFog::colorFog},
        {"setColorFog", LuaVolumetricFog::setColorFog},
        {"startDistanceFog", LuaVolumetricFog::startDistanceFog},
        {"setStartDistanceFog", LuaVolumetricFog::setStartDistanceFog},
        {"endDistanceFog", LuaVolumetricFog::endDistanceFog},
        {"setEndDistanceFog", LuaVolumetricFog::setEndDistanceFog},
        {"usePointsLightForce", LuaVolumetricFog::usePointsLightForce},
        {"setUsePointsLightForce", LuaVolumetricFog::setUsePointsLightForce},
        {"generateRaysDirectionLight", LuaVolumetricFog::generateRaysDirectionLight},
        {"setGenerateRaysDirectionLight", LuaVolumetricFog::setGenerateRaysDirectionLight},
        {"maxDistanceRay", LuaVolumetricFog::maxDistanceRay},
        {"setMaxDistanceRay", LuaVolumetricFog::setMaxDistanceRay},
        {NULL, NULL}};
    BasicFunctions::createSubStaticLib(L, GRAPHIC_TABLE, VOLUMETRIC_FOG_SUB_TABLE, volumetricFogFunctions);
}

SettingsGraphicPipeline *LuaVolumetricFog::getSettingsGraphicPipeline(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    return engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline();
}

int LuaVolumetricFog::generateFog(lua_State *L)
{
    bool generateFog = getSettingsGraphicPipeline(L)->volumetricFog.generateFog;
    lua_pushboolean(L, generateFog);
    return 1;
}

int LuaVolumetricFog::setGenerateFog(lua_State *L)
{
    bool generateFog = lua_toboolean(L, 1);
    getSettingsGraphicPipeline(L)->volumetricFog.generateFog = generateFog;
    return 0;
}

int LuaVolumetricFog::intensivityFog(lua_State *L)
{
    float intensivity = getSettingsGraphicPipeline(L)->volumetricFog.getIntensivityFog();
    lua_pushnumber(L, intensivity);
    return 1;
}

int LuaVolumetricFog::setIntensivityFog(lua_State *L)
{
    float intensivity = luaL_checknumber(L, 1);
    getSettingsGraphicPipeline(L)->volumetricFog.setIntensivityFog(intensivity);
    return 0;
}

int LuaVolumetricFog::anisotropic(lua_State *L)
{
    float anisotropic = getSettingsGraphicPipeline(L)->volumetricFog.getAnisotropic();
    lua_pushnumber(L, anisotropic);
    return 1;
}

int LuaVolumetricFog::setAnisotropic(lua_State *L)
{
    float anisotropic = luaL_checknumber(L, 1);
    getSettingsGraphicPipeline(L)->volumetricFog.setAnisotropic(anisotropic);
    return 0;
}

int LuaVolumetricFog::scattering(lua_State *L)
{
    float scattering = getSettingsGraphicPipeline(L)->volumetricFog.getScattering();
    lua_pushnumber(L, scattering);
    return 1;
}

int LuaVolumetricFog::setScattering(lua_State *L)
{
    float scattering = luaL_checknumber(L, 1);
    getSettingsGraphicPipeline(L)->volumetricFog.setScattering(scattering);
    return 0;
}

int LuaVolumetricFog::absorption(lua_State *L)
{
    float absorption = getSettingsGraphicPipeline(L)->volumetricFog.getAbsorption();
    lua_pushnumber(L, absorption);
    return 1;
}

int LuaVolumetricFog::setAbsorption(lua_State *L)
{
    float absorption = luaL_checknumber(L, 1);
    getSettingsGraphicPipeline(L)->volumetricFog.setAbsorption(absorption);
    return 0;
}

int LuaVolumetricFog::sizeGridFog(lua_State *L)
{
    std::array<int, 3> sizeGrid = getSettingsGraphicPipeline(L)->volumetricFog.getSizeFogGrid();
    LuaVector::pushVector3(L, mth::vec3(sizeGrid[0], sizeGrid[1], sizeGrid[2]));
    return 1;
}

int LuaVolumetricFog::setSizeGridFog(lua_State *L)
{
    int x = luaL_checkinteger(L, 1);
    int y = luaL_checkinteger(L, 2);
    int z = luaL_checkinteger(L, 3);
    getSettingsGraphicPipeline(L)->volumetricFog.setSizeFogGrid({ x, y, z });
    return 0;
}

int LuaVolumetricFog::colorFog(lua_State *L)
{
    mth::col color = getSettingsGraphicPipeline(L)->volumetricFog.colorFog;
    LuaColor::pushColor(L, color);
    return 1;
}

int LuaVolumetricFog::setColorFog(lua_State *L)
{
    mth::col *color = CHECK_COLOR(L, 1);
    getSettingsGraphicPipeline(L)->volumetricFog.colorFog = *color;
    return 0;
}

int LuaVolumetricFog::startDistanceFog(lua_State *L)
{
    float value = getSettingsGraphicPipeline(L)->volumetricFog.getStartDist();
    lua_pushnumber(L, value);
    return 1;
}

int LuaVolumetricFog::setStartDistanceFog(lua_State *L)
{
    float distance = luaL_checknumber(L, 1);
    getSettingsGraphicPipeline(L)->volumetricFog.setStartDist(distance);
    return 0;
}

int LuaVolumetricFog::endDistanceFog(lua_State *L)
{
    float value = getSettingsGraphicPipeline(L)->volumetricFog.getEndFogDist();
    lua_pushnumber(L, value);
    return 1;
}

int LuaVolumetricFog::setEndDistanceFog(lua_State *L)
{
    float distance = luaL_checknumber(L, 1);
    getSettingsGraphicPipeline(L)->volumetricFog.setEndFogDist(distance);
    return 0;
}

int LuaVolumetricFog::usePointsLightForce(lua_State *L)
{
    bool value = getSettingsGraphicPipeline(L)->volumetricFog.usePointsLightForce;
    lua_pushboolean(L, value);
    return 1;
}

int LuaVolumetricFog::setUsePointsLightForce(lua_State *L)
{
    bool use = lua_toboolean(L, 1);
    getSettingsGraphicPipeline(L)->volumetricFog.usePointsLightForce = use;
    return 0;
}

int LuaVolumetricFog::generateRaysDirectionLight(lua_State *L)
{
    bool value = getSettingsGraphicPipeline(L)->volumetricFog.generateRaysDirectionLight;
    lua_pushboolean(L, value);
    return 1;
}

int LuaVolumetricFog::setGenerateRaysDirectionLight(lua_State *L)
{
    bool use = lua_toboolean(L, 1);
    getSettingsGraphicPipeline(L)->volumetricFog.generateRaysDirectionLight = use;
    return 0;
}

int LuaVolumetricFog::maxDistanceRay(lua_State *L)
{
    float value = getSettingsGraphicPipeline(L)->volumetricFog.getMaxDistRaysPerPixel();
    lua_pushnumber(L, value);
    return 1;
}

int LuaVolumetricFog::setMaxDistanceRay(lua_State *L)
{
    float distance = luaL_checknumber(L, 1);
    getSettingsGraphicPipeline(L)->volumetricFog.setMaxDistRaysPerPixel(distance);
    return 0;
}