#include "scriptEngine/libs/luaGraphic/luaShadowDirectionLight.h"
#include "scriptEngine/libs/luaGraphic/luaGraphic.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/utilities/enumGenerator.h"
#include "scriptEngine/libs/luaVector.h"
#include "scriptEngine/libs/luaColor.h"

void LuaShadowDirectionLight::luaLoadShadowDirectionLight(lua_State *L)
{
    luaL_Reg skyFunctions[] = {
        {"getShadowBias", LuaShadowDirectionLight::getShadowBias},
        {"setShadowBias", LuaShadowDirectionLight::setShadowBias},
        {"shadowDistance", LuaShadowDirectionLight::shadowDistance},
        {"setShadowDistance", LuaShadowDirectionLight::setShadowDistance},
        {"maxUpShadowDistance", LuaShadowDirectionLight::maxUpShadowDistance},
        {"setMaxUpShadowDistance", LuaShadowDirectionLight::setMaxUpShadowDistance},
        {"degreePartition", LuaShadowDirectionLight::degreePartition},
        {"setDegreePartition", LuaShadowDirectionLight::setDegreePartition},
        {"sizeSide", LuaShadowDirectionLight::sizeSide},
        {"setSizeSide", LuaShadowDirectionLight::setSizeSide},
        {"getStartFadeDistance", LuaShadowDirectionLight::getStartFadeDistance},
        {"setStartFadeDistance", LuaShadowDirectionLight::setStartFadeDistance},
        {"reductionTypePartitionVolume", LuaShadowDirectionLight::reductionTypePartitionVolume},
        {"setReductionTypePartitionVolume", LuaShadowDirectionLight::setReductionTypePartitionVolume},
        {"reductionTypeSizePartition", LuaShadowDirectionLight::reductionTypeSizePartition},
        {"setReductionTypeSizePartition", LuaShadowDirectionLight::setReductionTypeSizePartition},
        {NULL, NULL}};
    BasicFunctions::createSubStaticLib(L, GRAPHIC_TABLE, SHADOW_SUB_TABLE, skyFunctions);

    EnumGenerator<int> enumReductionType;
    enumReductionType.startNewEnum(L, REDUCTION_TYPE_ENUM, REDUCTION_TYPE_ITEM_METADATA);
    enumReductionType.addParameter(L, SDLD_ReductionType::SDLD_REDUCTION_TYPE_LINEAR, "reductionLinear");
    enumReductionType.addParameter(L, SDLD_ReductionType::SDLD_REDUCTION_TYPE_SQUARE, "reductionSquare");
    enumReductionType.endEnum(L);
}

void LuaShadowDirectionLight::pushReductionType(lua_State *L, SDLD_ReductionType _type)
{
    lua_getglobal(L, REDUCTION_TYPE_ENUM);
    switch (_type)
    {
    case TypeUpdateSky::EveryFrame:
        lua_getfield(L, -1, "reductionLinear");
        lua_remove(L, -2);
        return;
    case TypeUpdateSky::Call:
        lua_getfield(L, -1, "reductionSquare");
        lua_remove(L, -2);
        return;
    default:
        return;
    }
}

SettingsGraphicPipeline *LuaShadowDirectionLight::getSettingsGraphicPipeline(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    return engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline();
}

int LuaShadowDirectionLight::getShadowBias(lua_State *L)
{
    int part = luaL_checkinteger(L, 1);
    lua_pushnumber(L, getSettingsGraphicPipeline(L)->shadowDirectionLight.getShadowBias(part));
    return 1;
}

int LuaShadowDirectionLight::setShadowBias(lua_State *L)
{
    int part = luaL_checkinteger(L, 1);
    float bias = luaL_checknumber(L, 2);
    getSettingsGraphicPipeline(L)->shadowDirectionLight.setShadowBias(part, bias);
    return 0;
}

int LuaShadowDirectionLight::shadowDistance(lua_State *L)
{
    lua_pushnumber(L, getSettingsGraphicPipeline(L)->shadowDirectionLight.getShadowDistance());
    return 1;
}

int LuaShadowDirectionLight::setShadowDistance(lua_State *L)
{
    getSettingsGraphicPipeline(L)->shadowDirectionLight.setShadowDistance(luaL_checknumber(L, 1));
    return 0;
}

int LuaShadowDirectionLight::maxUpShadowDistance(lua_State *L)
{
    lua_pushnumber(L, getSettingsGraphicPipeline(L)->shadowDirectionLight.getMaxUpShadowDistance());
    return 1;
}

int LuaShadowDirectionLight::setMaxUpShadowDistance(lua_State *L)
{
    getSettingsGraphicPipeline(L)->shadowDirectionLight.setMaxUpShadowDistance(luaL_checknumber(L, 1));
    return 0;
}

int LuaShadowDirectionLight::degreePartition(lua_State *L)
{
    lua_pushinteger(L, getSettingsGraphicPipeline(L)->shadowDirectionLight.getDegreePartition());
    return 1;
}

int LuaShadowDirectionLight::setDegreePartition(lua_State *L)
{
    getSettingsGraphicPipeline(L)->shadowDirectionLight.setDegreePartition(luaL_checkinteger(L, 1));
    return 0;
}

int LuaShadowDirectionLight::sizeSide(lua_State *L)
{
    lua_pushinteger(L, getSettingsGraphicPipeline(L)->shadowDirectionLight.getSizeSide());
    return 1;
}

int LuaShadowDirectionLight::setSizeSide(lua_State *L)
{
    getSettingsGraphicPipeline(L)->shadowDirectionLight.setSizeSide(luaL_checkinteger(L, 1));
    return 0;
}

int LuaShadowDirectionLight::getStartFadeDistance(lua_State *L)
{
    lua_pushnumber(L, getSettingsGraphicPipeline(L)->shadowDirectionLight.getStartFadeDistance());
    return 1;
}

int LuaShadowDirectionLight::setStartFadeDistance(lua_State *L)
{
    getSettingsGraphicPipeline(L)->shadowDirectionLight.setStartFadeDistance(luaL_checknumber(L, 1));
    return 0;
}

int LuaShadowDirectionLight::reductionTypePartitionVolume(lua_State *L)
{
    pushReductionType(L, getSettingsGraphicPipeline(L)->shadowDirectionLight.reductionTypePartitionVolume);
    return 1;
}

int LuaShadowDirectionLight::setReductionTypePartitionVolume(lua_State *L)
{
    getSettingsGraphicPipeline(L)->shadowDirectionLight.reductionTypePartitionVolume = CHECK_REDUCTION_TYPE(L, 1);
    return 0;
}

int LuaShadowDirectionLight::reductionTypeSizePartition(lua_State *L)
{
    pushReductionType(L, getSettingsGraphicPipeline(L)->shadowDirectionLight.reductionTypeSizePartition);
    return 1;
}

int LuaShadowDirectionLight::setReductionTypeSizePartition(lua_State *L)
{
    getSettingsGraphicPipeline(L)->shadowDirectionLight.reductionTypeSizePartition = CHECK_REDUCTION_TYPE(L, 1);
    return 0;
}