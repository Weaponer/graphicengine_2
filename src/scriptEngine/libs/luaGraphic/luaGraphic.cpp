#include "scriptEngine/libs/luaGraphic/luaGraphic.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaVector.h"

#include "scriptEngine/libs/luaGraphic/luaSky.h"
#include "scriptEngine/libs/luaGraphic/luaShadowDirectionLight.h"
#include "scriptEngine/libs/luaGraphic/luaSSAO.h"
#include "scriptEngine/libs/luaGraphic/luaVolumetricFog.h"
#include "scriptEngine/libs/luaGraphic/luaBloom.h"
#include "scriptEngine/libs/luaGraphic/luaVignetteEffect.h"
#include "scriptEngine/libs/luaGraphic/luaHDRCorrection.h"
#include "scriptEngine/libs/luaGraphic/luaLODSettings.h"

void LuaGraphic::luaLoadGraphic(lua_State *L)
{
    luaL_Reg funcs[] = {
        {NULL, NULL}};
    BasicFunctions::createStaticLib(L, GRAPHIC_TABLE, funcs);
    LuaSky::luaLoadSky(L);
    LuaShadowDirectionLight::luaLoadShadowDirectionLight(L);
    LuaSSAO::luaLoadSSAO(L);
    LuaVolumetricFog::luaLoadVolumetricFog(L);
    LuaBloom::luaLoadBloom(L);
    LuaVignetteEffect::luaLoadVignetteEffect(L);
    LuaHDRCorrection::luaLoadHDRCorrection(L);
    LuaLODSettings::luaLoadLODSettings(L);
}