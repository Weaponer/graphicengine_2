#include "scriptEngine/libs/luaGraphic/luaVignetteEffect.h"
#include "scriptEngine/libs/luaGraphic/luaGraphic.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaColor.h"

void LuaVignetteEffect::luaLoadVignetteEffect(lua_State *L)
{
    luaL_Reg vignetteEffectFunctions[] = {
        {"generateVignetteEffect", LuaVignetteEffect::generateVignetteEffect},
        {"setGenerateVignetteEffect", LuaVignetteEffect::setGenerateVignetteEffect},
        {"color", LuaVignetteEffect::color},
        {"setColor", LuaVignetteEffect::setColor},
        {"radius", LuaVignetteEffect::radius},
        {"setRadius", LuaVignetteEffect::setRadius},
        {"pow", LuaVignetteEffect::pow},
        {"setPow", LuaVignetteEffect::setPow},
        {NULL, NULL}};
    BasicFunctions::createSubStaticLib(L, GRAPHIC_TABLE, VIGNETTE_EFFECT_SUB_TABLE, vignetteEffectFunctions);
}

static SettingsGraphicPipeline *getSettingsGraphicPipeline(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    return engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline();
}

int LuaVignetteEffect::generateVignetteEffect(lua_State *L)
{
    lua_pushboolean(L, getSettingsGraphicPipeline(L)->postEffects.vignetteEffect.useVignetteEffect);
    return 1;
}

int LuaVignetteEffect::setGenerateVignetteEffect(lua_State *L)
{
    bool generate = lua_toboolean(L, 1);
    getSettingsGraphicPipeline(L)->postEffects.vignetteEffect.useVignetteEffect = generate;
    return 0;
}

int LuaVignetteEffect::color(lua_State *L)
{
    LuaColor::pushColor(L, getSettingsGraphicPipeline(L)->postEffects.vignetteEffect.color);
    return 1;
}

int LuaVignetteEffect::setColor(lua_State *L)
{
    mth::col color = *CHECK_COLOR(L, 1);
    getSettingsGraphicPipeline(L)->postEffects.vignetteEffect.color = color;
    return 0;
}

int LuaVignetteEffect::radius(lua_State *L)
{
    lua_pushnumber(L, getSettingsGraphicPipeline(L)->postEffects.vignetteEffect.getRadius());
    return 1;
}

int LuaVignetteEffect::setRadius(lua_State *L)
{
    float val = lua_tonumber(L, 1);
    getSettingsGraphicPipeline(L)->postEffects.vignetteEffect.setRadius(val);
    return 0;
}

int LuaVignetteEffect::pow(lua_State *L)
{
    lua_pushnumber(L, getSettingsGraphicPipeline(L)->postEffects.vignetteEffect.getPow());
    return 1;
}

int LuaVignetteEffect::setPow(lua_State *L)
{
    float val = lua_tonumber(L, 1);
    getSettingsGraphicPipeline(L)->postEffects.vignetteEffect.setPow(val);
    return 0;
}