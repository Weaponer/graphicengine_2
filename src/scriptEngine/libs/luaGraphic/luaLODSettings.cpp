#include "scriptEngine/libs/luaGraphic/luaLODSettings.h"
#include "scriptEngine/libs/luaGraphic/luaGraphic.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

void LuaLODSettings::luaLoadLODSettings(lua_State *L)
{
    luaL_Reg lodSettingsFunctions[] = {
        {"useLods", LuaLODSettings::useLods},
        {"setUseLods", LuaLODSettings::setUseLods},
        {"useLodsForShadow", LuaLODSettings::useLodsForShadow},
        {"setUseLodsForShadow", LuaLODSettings::setUseLodsForShadow},
        {"lodBias", LuaLODSettings::lodBias},
        {"setLodBias", LuaLODSettings::setLodBias},
        {"lodShadowBias", LuaLODSettings::lodShadowBias},
        {"setLodShadowBias", LuaLODSettings::setLodShadowBias},
        {NULL, NULL}};
    BasicFunctions::createSubStaticLib(L, GRAPHIC_TABLE, LOD_SETTINGS_TABLE, lodSettingsFunctions);
}

static SettingsGraphicPipeline *getSettingsGraphicPipeline(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    return engine->getSpecialSystems()->graphicEngine->getSettingsGraphicPipeline();
}

int LuaLODSettings::useLods(lua_State *L)
{
    lua_pushboolean(L, getSettingsGraphicPipeline(L)->lodSettings.useLods);
    return 1;
}

int LuaLODSettings::setUseLods(lua_State *L)
{
    bool val = lua_toboolean(L, 1);
    getSettingsGraphicPipeline(L)->lodSettings.useLods = val;
    return 0;
}

int LuaLODSettings::useLodsForShadow(lua_State *L)
{
    lua_pushboolean(L, getSettingsGraphicPipeline(L)->lodSettings.useLodsForShadow);
    return 1;
}

int LuaLODSettings::setUseLodsForShadow(lua_State *L)
{
    bool val = lua_toboolean(L, 1);
    getSettingsGraphicPipeline(L)->lodSettings.useLodsForShadow = val;
    return 0;
}

int LuaLODSettings::lodBias(lua_State *L)
{
    lua_pushnumber(L, getSettingsGraphicPipeline(L)->lodSettings.getLodBias());
    return 1;
}

int LuaLODSettings::setLodBias(lua_State *L)
{
    float val = luaL_checknumber(L, 1);
    getSettingsGraphicPipeline(L)->lodSettings.setLodBias(val);
    return 0;
}

int LuaLODSettings::lodShadowBias(lua_State *L)
{
    lua_pushnumber(L, getSettingsGraphicPipeline(L)->lodSettings.getLodShadowBias());
    return 1;
}

int LuaLODSettings::setLodShadowBias(lua_State *L)
{
    float val = luaL_checknumber(L, 1);
    getSettingsGraphicPipeline(L)->lodSettings.setLodShadowBias(val);
    return 0;
}