#include "scriptEngine/base/scriptEngine.h"
#include "window/inputSystem.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaInput/luaInput.h"
#include "scriptEngine/libs/luaInput/luaMouse.h"
#include "scriptEngine/libs/luaInput/luaKeyCode.h"
#include "scriptEngine/libs/luaInput/luaKeyboard.h"



void LuaInput::luaLoadInput(lua_State *L)
{
    LuaMouse::luaLoadMouse(L);
    LuaKeyCode::luaLoadKeyCode(L);
    LuaKeyboard::luaLoadKeyboard(L);
    luaL_Reg inputFuncs[] =
        {
            {"mouse", LuaInput::mouse},
            {"keyboard", LuaInput::keyboard},
            {NULL, NULL}};

    BasicFunctions::createStaticLib(L, "Input", inputFuncs);
}

int LuaInput::mouse(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    LuaMouse::luaPushMouse(L, engine->getSpecialSystems()->inputSystem->getMouse());
    return 1;
}

int LuaInput::keyboard(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    LuaKeyboard::luaPushKeyboard(L, engine->getSpecialSystems()->inputSystem->getKeyboard());
    return 1;
}