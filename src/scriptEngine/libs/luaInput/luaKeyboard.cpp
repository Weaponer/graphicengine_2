#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaInput/luaKeyboard.h"
#include "scriptEngine/libs/luaInput/luaKeyCode.h"

void LuaKeyboard::luaLoadKeyboard(lua_State *L)
{
    luaL_Reg metaFuncs[] =
        {
            {"__tostring", LuaKeyboard::toString},
            {"__eq", LuaKeyboard::eq},
            {"getButton", LuaKeyboard::getButton},
            {"getButtonDown", LuaKeyboard::getButtonDown},
            {"getButtonUp", LuaKeyboard::getButtonUp},
            {NULL, NULL}};
    BasicFunctions::createLibUserdata(L, KEYBOARD_METADATA, metaFuncs);
}

void LuaKeyboard::luaPushKeyboard(lua_State *L, Keyboard *keyboard)
{
    BasicFunctions::pushTObject<Keyboard *>(L, KEYBOARD_METADATA, keyboard);
}

int LuaKeyboard::getButton(lua_State *L)
{
    Keyboard **keyboard = CHECK_KEYBOARD(L, 1);
    Uint16 *code = CHECK_KEY_CODE(L, 2);
    lua_pushboolean(L, (*keyboard)->getKey((SDL_Scancode)(*code)));
    return 1;
}

int LuaKeyboard::getButtonDown(lua_State *L)
{
    Keyboard **keyboard = CHECK_KEYBOARD(L, 1);
    Uint16 *code = CHECK_KEY_CODE(L, 2);
    lua_pushboolean(L, (*keyboard)->getKeyDown((SDL_Scancode)(*code)));
    return 1;
}

int LuaKeyboard::getButtonUp(lua_State *L)
{
    Keyboard **keyboard = CHECK_KEYBOARD(L, 1);
    Uint16 *code = CHECK_KEY_CODE(L, 2);
    lua_pushboolean(L, (*keyboard)->getKeyUp((SDL_Scancode)(*code)));
    return 1;
}

int LuaKeyboard::toString(lua_State *L)
{
    lua_pushfstring(L, "Keyboard");
    return 1;
}

int LuaKeyboard::eq(lua_State *L)
{
    Keyboard **keyboard1 = CHECK_KEYBOARD(L, 1);
    Keyboard **keyboard2 = CHECK_KEYBOARD(L, 2);
    lua_pushboolean(L, (*keyboard1) == (*keyboard2));
    return 1;
}