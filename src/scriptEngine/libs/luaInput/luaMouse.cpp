#include "scriptEngine/base/scriptEngine.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaInput/luaMouse.h"
#include "scriptEngine/libs/luaVector.h"

#include "scriptEngine/libs/utilities/enumGenerator.h"

void LuaMouse::luaLoadMouse(lua_State *L)
{
    luaLoadMouseButtons(L);

    luaL_Reg metaFuncs[] =
        {
            {"__tostring", LuaMouse::toString},
            {"__eq", LuaMouse::eq},
            {"position", LuaMouse::position},
            {"deltaPosition", LuaMouse::deltaPosition},
            {"getButton", LuaMouse::getButton},
            {"getButtonDown", LuaMouse::getButtonDown},
            {"getButtonUp", LuaMouse::getButtonUp},
            {NULL, NULL}};
    BasicFunctions::createLibUserdata(L, MOUSE_METADATA, metaFuncs);
}

void LuaMouse::luaPushMouse(lua_State *L, Mouse *mouse)
{
    BasicFunctions::pushTObject<Mouse *>(L, MOUSE_METADATA, mouse);
}

void LuaMouse::luaLoadMouseButtons(lua_State *L)
{
    EnumGenerator<Uint8> mouseButtons;
    mouseButtons.startNewEnum(L, MOUSE_BUTTONS_ENUM, MOUSE_BUTTON_METADATA);
    mouseButtons.addParameter(L, SDL_BUTTON_LEFT, "left");
    mouseButtons.addParameter(L, SDL_BUTTON_RIGHT, "right");
    mouseButtons.addParameter(L, SDL_BUTTON_MIDDLE, "middle");
    mouseButtons.addParameter(L, SDL_BUTTON_X1, "button4");
    mouseButtons.addParameter(L, SDL_BUTTON_X2, "button5");
    mouseButtons.endEnum(L);
}

int LuaMouse::position(lua_State *L)
{
    Mouse **mouse = CHECK_MOUSE(L, 1);
    LuaVector::pushVector2(L, (*mouse)->getMousePosition());
    return 1;
}

int LuaMouse::deltaPosition(lua_State *L)
{
    Mouse **mouse = CHECK_MOUSE(L, 1);
    LuaVector::pushVector2(L, (*mouse)->getMouseDeltaMove());
    return 1;
}

int LuaMouse::getButton(lua_State *L)
{
    Mouse **mouse = CHECK_MOUSE(L, 1);
    Uint8 *code = CHECK_MOUSE_BUTTON(L, 2);
    lua_pushboolean(L, (*mouse)->getMouseButton(*code));
    return 1;
}

int LuaMouse::getButtonDown(lua_State *L)
{
    Mouse **mouse = CHECK_MOUSE(L, 1);
    Uint8 *code = CHECK_MOUSE_BUTTON(L, 2);
    lua_pushboolean(L, (*mouse)->getMouseButtonDown(*code));
    return 1;
}

int LuaMouse::getButtonUp(lua_State *L)
{
    Mouse **mouse = CHECK_MOUSE(L, 1);
    Uint8 *code = CHECK_MOUSE_BUTTON(L, 2);
    lua_pushboolean(L, (*mouse)->getMouseButtonUp(*code));
    return 1;
}

int LuaMouse::toString(lua_State *L)
{
    lua_pushfstring(L, "Mouse");
    return 1;
}

int LuaMouse::eq(lua_State *L)
{
    Mouse **mouse1 = CHECK_MOUSE(L, 1);
    Mouse **mouse2 = CHECK_MOUSE(L, 2);
    lua_pushboolean(L, (*mouse1) == (*mouse2));
    return 1;
}