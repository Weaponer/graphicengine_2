#include "scriptEngine/libs/luaTime/luaTime.h"
#include "engineBase/timeData.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

void LuaTime::luaLoadTime(lua_State *L)
{
    luaL_Reg funcs[] = {
        {"deltaTime", LuaTime::deltaTime},
        {"lock", LuaTime::lock},
        {"setLock", LuaTime::setLock},
        {NULL, NULL}};
    BasicFunctions::createStaticLib(L, TIME_TABLE, funcs);
}

int LuaTime::deltaTime(lua_State *L)
{
    lua_pushnumber(L, BasicFunctions::getScriptEngine(L)->getSpecialSystems()->timeData->deltaTime);
    return 1;
}

int LuaTime::lock(lua_State *L)
{
    lua_pushnumber(L, BasicFunctions::getScriptEngine(L)->getSpecialSystems()->timeData->lock);
    return 1;
}

int LuaTime::setLock(lua_State *L)
{
    int lock = luaL_checkinteger(L, 1);
    if (lock <= 0)
    {
        luaL_error(L, "The deltaTime limit cannot be less than one.");
        return 0;
    }
    BasicFunctions::getScriptEngine(L)->getSpecialSystems()->timeData->lock = lock;
    return 0;
}