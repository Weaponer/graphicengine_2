#include "scriptEngine/libs/luaLayer/luaLayer.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

void LuaLayer::luaLoadLayer(lua_State *L)
{
    luaL_Reg funcsStatic[] =
        {
            {"layerByName", LuaLayer::layerByName},
            {"layerByNumber", LuaLayer::layerByNumber},
            {"defaultLayer", LuaLayer::defaultLayer},
            {NULL, NULL}};
    BasicFunctions::createStaticLib(L, LAYER_NAME, funcsStatic);

    luaL_Reg funcsmeta[] =
        {
            {"name", LuaLayer::name},
            {"number", LuaLayer::number},
            {"__tostring", LuaLayer::tostring},
            {"__name", LuaLayer::tostring},
            {NULL, NULL}};
    BasicFunctions::createLibUserdata(L, LAYER_METADATA, funcsmeta);
}

void LuaLayer::pushLayer(lua_State *L, Layer _layer)
{
    Layer *layer = (Layer *)lua_newuserdata(L, sizeof(Layer));
    *layer = _layer;
    luaL_getmetatable(L, LAYER_METADATA);
    lua_setmetatable(L, -2);
}

int LuaLayer::name(lua_State *L)
{
    Layer layer = CHECK_LAYER(L, 1);
    lua_pushfstring(L, BasicFunctions::getScriptEngine(L)->getSpecialSystems()->layerManager->getNameLayer(layer));
    return 1;
}

int LuaLayer::number(lua_State *L)
{
    Layer layer = CHECK_LAYER(L, 1);
    lua_pushnumber(L, BasicFunctions::getScriptEngine(L)->getSpecialSystems()->layerManager->getNumberLayer(layer));
    return 1;
}

int LuaLayer::tostring(lua_State *L)
{
    Layer layer = CHECK_LAYER(L, 1);
    lua_pushfstring(L, "Layer.%s", BasicFunctions::getScriptEngine(L)->getSpecialSystems()->layerManager->getNameLayer(layer));
    return 1;
}

int LuaLayer::layerByName(lua_State *L)
{
    const char *name = luaL_checkstring(L, 1);
    pushLayer(L, BasicFunctions::getScriptEngine(L)->getSpecialSystems()->layerManager->getLayerByName(name));
    return 1;
}

int LuaLayer::layerByNumber(lua_State *L)
{
    int num = luaL_checknumber(L, 1);
    pushLayer(L, BasicFunctions::getScriptEngine(L)->getSpecialSystems()->layerManager->getLayerByNumber(num));
    return 1;
}

int LuaLayer::defaultLayer(lua_State *L)
{
    LuaLayer::pushLayer(L, BasicFunctions::getScriptEngine(L)->getSpecialSystems()->layerManager->getDefaultLayer());
    return 1;
}