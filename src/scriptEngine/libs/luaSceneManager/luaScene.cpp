#include "scriptEngine/libs/luaSceneManager/luaScene.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"

void LuaScene::luaLoadScene(lua_State *L)
{
    luaL_Reg metafunc[] = {
        {"name", LuaScene::name},
        {"setName", LuaScene::setName},
        {NULL, NULL}
    };  
    BasicFunctions::createLibUserdata(L, SCENE_METADATA, metafunc);
}

int LuaScene::getScene(lua_State *L, Scene *_scene)
{
    return BasicFunctions::getLuaObject(L, SCENE_METADATA, _scene);
}

void LuaScene::pushScene(lua_State *L, Scene *_scene)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, BasicFunctions::getLuaObject(L, SCENE_METADATA, _scene));
}

int LuaScene::name(lua_State *L)
{
    Scene *scene = CHECK_SCENE(L, 1);
    lua_pushfstring(L, "%s", scene->getName());
    return 1;
}

int LuaScene::setName(lua_State *L)
{
    Scene *scene = CHECK_SCENE(L, 1);
    const char *name = luaL_checkstring(L, 2);
    scene->setName(name);
    return 0;
}