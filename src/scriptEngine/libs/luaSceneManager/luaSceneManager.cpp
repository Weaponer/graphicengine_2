#include "scriptEngine/libs/luaSceneManager/luaSceneManager.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaSceneManager/luaScene.h"

#include "engineBase/sandboxGameObjects/sandboxGameObjects.h"


void LuaSceneManager::luaLoadSceneManager(lua_State *L)
{
    LuaScene::luaLoadScene(L);

    luaL_Reg funcs[] =
        {
            {"createScene", LuaSceneManager::createScene},
            {"removeScene", LuaSceneManager::removeScene},
            {"getCurrentScene", LuaSceneManager::getCurrentScene},
            {"getSceneByName", LuaSceneManager::getSceneByName},
            {"setSceneDefault", LuaSceneManager::setSceneDefault},
            {NULL, NULL}};
    BasicFunctions::createStaticLib(L, "SceneManager", funcs);
}

int LuaSceneManager::createScene(lua_State *L)
{
    const char *name = luaL_checkstring(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    LuaScene::pushScene(L, engine->getSpecialSystems()->sandboxGameObjects->getSceneManager()->createNewScene(name));
    return 1;
}

int LuaSceneManager::removeScene(lua_State *L)
{
    Scene *scene = CHECK_SCENE(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    lua_pushboolean(L, engine->getSpecialSystems()->sandboxGameObjects->getSceneManager()->removeScene(scene));
    return 1;
}

int LuaSceneManager::getCurrentScene(lua_State *L)
{
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    LuaScene::pushScene(L, engine->getSpecialSystems()->sandboxGameObjects->getSceneManager()->getCurrentScene());
    return 1;
}

int LuaSceneManager::getSceneByName(lua_State *L)
{
    const char *name = luaL_checkstring(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    LuaScene::pushScene(L, engine->getSpecialSystems()->sandboxGameObjects->getSceneManager()->getSceneByName(name));
    return 1;
}

int LuaSceneManager::setSceneDefault(lua_State *L)
{
    Scene *scene = CHECK_SCENE(L, 1);
    ScriptEngine *engine = BasicFunctions::getScriptEngine(L);
    engine->getSpecialSystems()->sandboxGameObjects->getSceneManager()->setSceneDefault(scene);
    return 0;
}