#include "scriptEngine/base/scriptEngine.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"
#include "scriptEngine/libs/luaOverloadNil.h"


ScriptEngine *BasicFunctions::getScriptEngine(lua_State *L)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, SCRIPT_ENGINE_INDEX_IN_REGISTER);
    ScriptEngine *engine = (ScriptEngine *)lua_topointer(L, -1);
    lua_remove(L, -1);
    return engine;
}

int BasicFunctions::getLuaObject(lua_State *L, const char *metadata, Object *object)
{
    lua_rawgeti(L, LUA_REGISTRYINDEX, SCRIPT_ENGINE_INDEX_IN_REGISTER);
    ScriptEngine *engine = (ScriptEngine *)lua_topointer(L, -1);
    lua_pop(L, 1);
    luaObject *lo = engine->getObjectRepresentative(object);
    if (lo == NULL)
    {
        return engine->createUDataForObject(object, metadata)->ref;
    }
    else
    {
        return lo->ref;
    }
}

void BasicFunctions::pushVoidObject(lua_State *L, const char *metadata, void *object)
{
    void **v = (void **)lua_newuserdata(L, sizeof(void *));
    *v = object;
    luaL_getmetatable(L, metadata);
    lua_setmetatable(L, -2);
}

void BasicFunctions::createLibUserdata(lua_State *L, const char *metadata, luaL_Reg *functions)
{
    luaL_newmetatable(L, metadata);
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");
    luaL_setfuncs(L, functions, 0);
    lua_remove(L, -1);
}

void BasicFunctions::createStaticLib(lua_State *L, const char *name, luaL_Reg *functions)
{
    luaL_newlib(L, functions);
    lua_setglobal(L, name);
}

void BasicFunctions::createSubStaticLib(lua_State *L, const char *baseLib, const char *name, luaL_Reg *functions)
{
    lua_getglobal(L, baseLib);
    luaL_newlib(L, functions);
    lua_setfield(L, -2, name);
    lua_remove(L, -1);
}

Object *BasicFunctions::checkLuaObject(lua_State *L, int _inx, const char *_metadata)
{
    luaObject *obj = (luaObject *)luaL_checkudata(L, _inx, _metadata);
    if (obj->object == NULL)
    {
        luaL_error(L, "LuaObject is nil.");
        return NULL;
    }
    return obj->object;
}

Object *BasicFunctions::checkLuaObjectOrNil(lua_State *L, int _inx, const char *_metadata)
{
    if(lua_type(L, _inx) == LUA_TNIL)
    {
        return NULL;
    }
    else if(luaL_testudata(L, _inx, NIL_METADATA))
    {
        return NULL;
    }
    else
    {
        return ((luaObject *)luaL_checkudata(L, _inx, _metadata))->object; 
    }
}

void BasicFunctions::pushCompareLuaObjects(lua_State *L, int _inx_1, int _inx_2, const char *_metadata)
{
    
    luaObject *obj = (luaObject *)luaL_checkudata(L, _inx_1, _metadata);
    if(lua_type(L, 2) == LUA_TNIL)
    {
        if(obj->object == NULL)
        {
            lua_pushboolean(L, true);
        }
        else
        {
            lua_pushboolean(L, false);
        }
        return;
    }
    luaObject *obj2 = (luaObject *)luaL_checkudata(L, _inx_2, _metadata);
    if (obj == obj2)
    {
        lua_pushboolean(L, true);
    }
    else
    {
        lua_pushboolean(L, false);
    }
}

Object *BasicFunctions::checkLuaObject(lua_State *L, int _inx)
{
    luaObject *obj = (luaObject *)lua_topointer(L, _inx);
    if (obj->object == NULL)
    {
        luaL_error(L, "LuaObject is nil.");
        return NULL;
    }
    return obj->object;
}

void BasicFunctions::pushCompareLuaObjects(lua_State *L, int _inx_1, int _inx_2)
{
    luaObject *obj = (luaObject *)lua_topointer(L, _inx_1);
    if(lua_type(L, 2) == LUA_TNIL)
    {
        if(obj->object == NULL)
        {
            lua_pushboolean(L, true);
        }
        else
        {
            lua_pushboolean(L, false);
        }
        return;
    }
    luaObject *obj2 = (luaObject *)lua_topointer(L, _inx_2);
    if (obj == obj2)
    {
        lua_pushboolean(L, true);
    }
    else
    {
        lua_pushboolean(L, false);
    }
}