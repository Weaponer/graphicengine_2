#include "scriptEngine/libs/luaColor.h"
#include "scriptEngine/libs/luaVector.h"
#include "scriptEngine/libs/utilities/basicFunctions.h"


void LuaColor::luaLoadColor(lua_State *L)
{
        luaL_Reg metaFuncsCol[] =
        {
            {"__add", LuaColor::add},
            {"__sub", LuaColor::sub},
            {"__mul", LuaColor::mul},
            {"__div", LuaColor::div},
            {"__eq", LuaColor::equality},
            {"__tostring", LuaColor::toString},
            {"r", LuaColor::r},
            {"g", LuaColor::g},
            {"b", LuaColor::b},
            {"a", LuaColor::a},
            {"setR", LuaColor::setR},
            {"setG", LuaColor::setG},
            {"setB", LuaColor::setB},
            {"setA", LuaColor::setA},
            {"setColor", LuaColor::setColor},
            {"setColorByte", LuaColor::setColorByte},
            {"normalize", LuaColor::normalize},
            {"toHSVA", LuaColor::toHSVA},
            {"setHSVA", LuaColor::setHSVA},
            {NULL, NULL}};

    BasicFunctions::createLibUserdata(L, COLOR_METADATA, metaFuncsCol);

    luaL_Reg colorFuncs[] =
        {
            {"new", LuaColor::newColor},
            {"normalized", LuaColor::normalized},
            {"white", LuaColor::white},
            {"black", LuaColor::black},
            {"red", LuaColor::red},
            {"green", LuaColor::green},
            {"blue", LuaColor::blue},
            {"cyan", LuaColor::cyan},
            {"gray", LuaColor::gray},
            {"magenta", LuaColor::magenta},
            {"yellow", LuaColor::yellow},
            {NULL, NULL}};
    BasicFunctions::createStaticLib(L, "Color", colorFuncs);
}

void LuaColor::pushColor(lua_State *L, const mth::col &col)
{
    BasicFunctions::pushTObject<mth::col>(L, COLOR_METADATA, col);
}

int LuaColor::newColor(lua_State *L)
{
    int count = lua_gettop(L);
    float r = 0;
    float g = 0;
    float b = 0;
    float a = 0;
    if (count >= 4)
    {
        r = luaL_checknumber(L, 1);
        g = luaL_checknumber(L, 2);
        b = luaL_checknumber(L, 3);
        a = luaL_checknumber(L, 4);
    }
    else if (count >= 1)
    {
        if (lua_type(L, 1) == LUA_TUSERDATA)
        {
            mth::col *vec = CHECK_COLOR(L, 1);
            if (vec == NULL)
            {
                return 0;
            }
            else
            {
                r = vec->r;
                g = vec->g;
                b = vec->b;
                a = vec->a;
            }
        }
        else
        {
            r = g = b = a = luaL_checknumber(L, 1);
        }
    }

    pushColor(L, mth::col(r, g, b, a));
    return 1;
}

int LuaColor::normalized(lua_State *L)
{
    mth::col oneCol = *CHECK_COLOR(L, 1);
    oneCol.normalize();
    pushColor(L, oneCol);
    return 1;
}

int LuaColor::white(lua_State *L)
{
    pushColor(L, mth::col(1, 1, 1, 1));
    return 1;
}

int LuaColor::black(lua_State *L)
{
    pushColor(L, mth::col(0, 0, 0, 1));
    return 1;
}

int LuaColor::red(lua_State *L)
{
    pushColor(L, mth::col(1, 0, 0, 1));
    return 1;
}

int LuaColor::green(lua_State *L)
{
    pushColor(L, mth::col(0, 1, 0, 1));
    return 1;
}

int LuaColor::blue(lua_State *L)
{
    pushColor(L, mth::col(0, 0, 1, 1));
    return 1;
}

int LuaColor::cyan(lua_State *L)
{
    pushColor(L, mth::col(0, 1, 1, 1));
    return 1;
}

int LuaColor::gray(lua_State *L)
{
    pushColor(L, mth::col(0.5f, 0.5f, 0.5f, 1));
    return 1;
}

int LuaColor::magenta(lua_State *L)
{
    pushColor(L, mth::col(1, 0, 1, 1));
    return 1;
}

int LuaColor::yellow(lua_State *L)
{
    pushColor(L, mth::col(1, 1, 0, 1));
    return 1;
}

int LuaColor::setColor(lua_State *L)
{
    mth::col *col = CHECK_COLOR(L, 1);
    int count = lua_gettop(L);
    float r = luaL_checknumber(L, 2);
    float g = luaL_checknumber(L, 3);
    float b = luaL_checknumber(L, 4);
    float a = 1;
    if (count >= 4)
    {
        a = luaL_checknumber(L, 5);
    }
    col->setColor(r, g, b, a);
    return 0;
}

int LuaColor::setColorByte(lua_State *L)
{
    mth::col *col = CHECK_COLOR(L, 1);
    int count = lua_gettop(L);
    int r = luaL_checkinteger(L, 2);
    int g = luaL_checkinteger(L, 3);
    int b = luaL_checkinteger(L, 4);
    int a = 1;
    if (count >= 4)
    {
        a = luaL_checkinteger(L, 5);
    }
    col->setColorByte(r, g, b, a);
    return 0;
}

int LuaColor::toHSVA(lua_State *L)
{
    mth::col *col = CHECK_COLOR(L, 1);
    LuaVector::pushVector4(L, col->getHSVA());
    return 0;
}

int LuaColor::setHSVA(lua_State *L)
{
    mth::col *col = CHECK_COLOR(L, 1);
    int count = lua_gettop(L);
    if(lua_type(L, 2) == LUA_TUSERDATA)
    {
        mth::vec4 *vec = CHECK_VECTOR_4(L, 2);
        col->setHSVA(*vec);
    }
    else
    {
        float h = luaL_checknumber(L, 2);
        float s = luaL_checknumber(L, 3);
        float v = luaL_checknumber(L, 4);
        float a = 1;
        if(count >= 4)
        {
            a = luaL_checknumber(L, 5);
        }
    }
}

int LuaColor::r(lua_State *L)
{
    mth::col *col = CHECK_COLOR(L, 1);
    lua_pushnumber(L, col->r);
    return 1;
}

int LuaColor::g(lua_State *L)
{
    mth::col *col = CHECK_COLOR(L, 1);
    lua_pushnumber(L, col->g);
    return 1;
}

int LuaColor::b(lua_State *L)
{
    mth::col *col = CHECK_COLOR(L, 1);
    lua_pushnumber(L, col->b);
    return 1;
}

int LuaColor::a(lua_State *L)
{
    mth::col *col = CHECK_COLOR(L, 1);
    lua_pushnumber(L, col->a);
    return 1;
}

int LuaColor::setR(lua_State *L)
{
    mth::col *col = CHECK_COLOR(L, 1);
    float v = luaL_checknumber(L, 2);
    col->r = v;
    return 0;
}

int LuaColor::setG(lua_State *L)
{
    mth::col *col = CHECK_COLOR(L, 1);
    float v = luaL_checknumber(L, 2);
    col->g = v;
    return 0;
}

int LuaColor::setB(lua_State *L)
{
    mth::col *col = CHECK_COLOR(L, 1);
    float v = luaL_checknumber(L, 2);
    col->b = v;
    return 0;
}

int LuaColor::setA(lua_State *L)
{
    mth::col *col = CHECK_COLOR(L, 1);
    float v = luaL_checknumber(L, 2);
    col->b = v;
    return 0;
}

int LuaColor::add(lua_State *L)
{
    mth::col *colOne = CHECK_COLOR(L, 1);
    mth::col *colTwo = CHECK_COLOR(L, 2);
    pushColor(L, (*colOne) + (*colTwo));
    return 1;
}

int LuaColor::sub(lua_State *L)
{
    mth::col *colOne = CHECK_COLOR(L, 1);
    mth::col *colTwo = CHECK_COLOR(L, 2);
    pushColor(L, (*colOne) - (*colTwo));
    return 1;
}

int LuaColor::mul(lua_State *L)
{
    mth::col *col = CHECK_COLOR(L, 2);
    if(lua_type(L, 1) == LUA_TUSERDATA)
    {
        mth::col *colTwo = CHECK_COLOR(L, 1);
        pushColor(L, (*col) * (*colTwo));
    }
    else
    {
        float v = luaL_checknumber(L, 1);
        pushColor(L, (*col) * v);
    }
    return 1;
}

int LuaColor::div(lua_State *L)
{
    mth::col *col = CHECK_COLOR(L, 2);
    float v = luaL_checknumber(L, 1);
    pushColor(L, (*col) / v);
    return 1;
}

int LuaColor::toString(lua_State *L)
{
    mth::col *col = CHECK_COLOR(L, 1);
    lua_pushfstring(L, "(%f, %f, %f, %f)", col->r, col->g, col->b, col->a);
    return 1;
}

int LuaColor::normalize(lua_State *L)
{
    mth::col *col = CHECK_COLOR(L, 1);
    col->normalize();
    return 0;
}

int LuaColor::equality(lua_State *L)
{
    mth::col *colOne = CHECK_COLOR(L, 1);
    mth::col *colTwo = CHECK_COLOR(L, 2);
    lua_pushboolean(L, (*colOne) == (*colTwo));
    return 1;
}