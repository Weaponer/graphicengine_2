#include "scriptEngine/libs/luaMatrix4.h"
#include "scriptEngine/libs/luaVector.h"

#include "scriptEngine/libs/utilities/basicFunctions.h"

void LuaMatrix4::luaLoadMatrix4(lua_State *L)
{
    luaL_Reg metaFuncs[] =
        {
            {"__add", LuaMatrix4::add},
            {"__sub", LuaMatrix4::sub},
            {"__mul", LuaMatrix4::mul},
            {"__div", LuaMatrix4::div},
            {"__tostring", LuaMatrix4::toString},
            {"transpose", LuaMatrix4::transpose},
            {"setSingleMatrix", LuaMatrix4::setSingleMatrix},
            {"transform", LuaMatrix4::transform},
            {"transformTranspose", LuaMatrix4::transformTranspose},
            {"transformNoMove", LuaMatrix4::transformNoMove},
            {"transformTransposeNoMove", LuaMatrix4::transformTransposeNoMove},
            {"get", LuaMatrix4::get},
            {"set", LuaMatrix4::set},
            {NULL, NULL}};
    BasicFunctions::createLibUserdata(L, MATRIX_4_METADATA, metaFuncs);

    luaL_Reg mat4Funcs[] =
        {
            {"new", LuaMatrix4::newMatrix4},
            {"M3fromM4", LuaMatrix4::matrix3FromMatrix4},
            {NULL, NULL}};
    BasicFunctions::createStaticLib(L, "Matrix4", mat4Funcs);
}

void LuaMatrix4::pushMatrix4(lua_State *L, const mth::mat4 &mat)
{
    BasicFunctions::pushTObject<mth::mat4>(L, MATRIX_4_METADATA, mat);
}

int LuaMatrix4::newMatrix4(lua_State *L)
{
    int count = lua_gettop(L);
    mth::mat4 m;
    if (count >= 16)
    {
        m.m[0][0] = luaL_checknumber(L, 1);
        m.m[0][1] = luaL_checknumber(L, 2);
        m.m[0][2] = luaL_checknumber(L, 3);
        m.m[0][3] = luaL_checknumber(L, 4);
        m.m[1][0] = luaL_checknumber(L, 5);
        m.m[1][1] = luaL_checknumber(L, 6);
        m.m[1][2] = luaL_checknumber(L, 7);
        m.m[1][3] = luaL_checknumber(L, 8);
        m.m[2][0] = luaL_checknumber(L, 9);
        m.m[2][1] = luaL_checknumber(L, 10);
        m.m[2][2] = luaL_checknumber(L, 11);
        m.m[2][3] = luaL_checknumber(L, 12);
        m.m[3][0] = luaL_checknumber(L, 13);
        m.m[3][1] = luaL_checknumber(L, 14);
        m.m[3][2] = luaL_checknumber(L, 15);
        m.m[3][3] = luaL_checknumber(L, 16);
    }
    else if (count >= 1)
    {
        mth::mat4 *mat = CHECK_MATRIX_4(L);
        m = *mat;
    }
    pushMatrix4(L, m);
    return 1;
}

int LuaMatrix4::matrix3FromMatrix4(lua_State *L)
{
    mth::mat4 *mat = CHECK_MATRIX_4(L);
    pushMatrix4(L, mth::matrix3FromMatrix4(*mat));
    return 1;
}

int LuaMatrix4::transpose(lua_State *L)
{
    mth::mat4 *mat = CHECK_MATRIX_4(L);
    mat->transpose();
    return 0;
}

int LuaMatrix4::setSingleMatrix(lua_State *L)
{
    mth::mat4 *mat = CHECK_MATRIX_4(L);
    mat->setSingleMatrix();
    return 0;
}

int LuaMatrix4::transform(lua_State *L)
{
    mth::mat4 *m = CHECK_MATRIX_4(L);
    mth::vec3 *v = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    LuaVector::pushVector3(L, m->transform(*v));
    return 1;
}

int LuaMatrix4::transformTranspose(lua_State *L)
{
    mth::mat4 *m = CHECK_MATRIX_4(L);
    mth::vec3 *v = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    LuaVector::pushVector3(L, m->transformTranspose(*v));
    return 1;
}

int LuaMatrix4::transformNoMove(lua_State *L)
{
    mth::mat4 *m = CHECK_MATRIX_4(L);
    mth::vec3 *v = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    LuaVector::pushVector3(L, m->transformNoMove(*v));
    return 1;
}

int LuaMatrix4::transformTransposeNoMove(lua_State *L)
{
    mth::mat4 *m = CHECK_MATRIX_4(L);
    mth::vec3 *v = (mth::vec3 *)luaL_checkudata(L, 2, VECTOR_3_METADATA);
    LuaVector::pushVector3(L, m->transformTransposeNoMove(*v));
    return 1;
}

int LuaMatrix4::add(lua_State *L)
{
    mth::mat4 *m1 = CHECK_MATRIX_4(L);
    mth::mat4 *m2 = (mth::mat4 *)luaL_checkudata(L, 2, MATRIX_4_METADATA);
    mth::mat4 m = (*m1) + (*m2);
    pushMatrix4(L, m);
    return 1;
}

int LuaMatrix4::sub(lua_State *L)
{
    mth::mat4 *m1 = CHECK_MATRIX_4(L);
    mth::mat4 *m2 = (mth::mat4 *)luaL_checkudata(L, 2, MATRIX_4_METADATA);
    mth::mat4 m = (*m1) - (*m2);
    pushMatrix4(L, m);
    return 1;
}

int LuaMatrix4::mul(lua_State *L)
{
    if (lua_type(L, 1) == LUA_TNUMBER)
    {
        float num = lua_tonumber(L, 1);
        mth::mat4 *m2 = (mth::mat4 *)luaL_checkudata(L, 2, MATRIX_4_METADATA);
        pushMatrix4(L, (*m2) * num);
        return 1;
    }
    else
    {
        mth::mat4 *m1 = CHECK_MATRIX_4(L);
        mth::mat4 *m2 = (mth::mat4 *)luaL_checkudata(L, 2, MATRIX_4_METADATA);
        mth::mat4 m = (*m1) * (*m2);
        pushMatrix4(L, m);
        return 1;
    }
}

int LuaMatrix4::div(lua_State *L)
{
    float num = lua_tonumber(L, 1);
    mth::mat4 *m2 = (mth::mat4 *)luaL_checkudata(L, 2, MATRIX_4_METADATA);
    pushMatrix4(L, (*m2) / num);
    return 1;
}

int LuaMatrix4::get(lua_State *L)
{
    mth::mat4 *m = CHECK_MATRIX_4(L);
    int num = luaL_checkunsigned(L, 2);
    int num2 = luaL_checkunsigned(L, 3);
    lua_pushnumber(L, m->m[num][num2]);
    return 1;
}

int LuaMatrix4::set(lua_State *L)
{
    mth::mat4 *m = CHECK_MATRIX_4(L);
    int num = luaL_checkunsigned(L, 2);
    int num2 = luaL_checkunsigned(L, 3);
    float val = luaL_checknumber(L, 4);
    m->m[num][num2] = val;
    return 0;
}

int LuaMatrix4::toString(lua_State *L)
{
    mth::mat4 *m = CHECK_MATRIX_4(L);
    lua_pushfstring(L, "%f %f %f %f\n%f %f %f %f\n%f %f %f %f",
                    m->m[0][0], m->m[0][1], m->m[0][2], m->m[0][3],
                    m->m[1][0], m->m[1][1], m->m[1][2], m->m[1][3],
                    m->m[2][0], m->m[2][1], m->m[2][2], m->m[2][3],
                    m->m[3][0], m->m[3][1], m->m[3][2], m->m[3][3]);
    return 1;
}