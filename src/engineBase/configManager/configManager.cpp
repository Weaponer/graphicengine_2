#include "engineBase/configManager/configManager.h"

ConfigManager::ConfigManager()
{
    L = luaL_newstate();
}

ConfigManager::~ConfigManager()
{
    lua_close(L);
}

bool ConfigManager::readConfigFile(const TextFile *_file)
{
    int error = luaL_loadstring(L, _file->getContentFile());
    if (error)
    {
        printf("Error in config: %s: %s\n", _file->getNameFile(), lua_tostring(L, -1));
        lua_settop(L, 0);
        return false;
    }
    if (lua_type(L, -1) != LUA_TFUNCTION)
    {
        lua_settop(L, 0);
        return false;
    }
    error = lua_pcall(L, 0, 1, 0);
    if (error)
    {
        printf("Error in config: %s: %s\n", _file->getNameFile(), lua_tostring(L, -1));
        lua_settop(L, 0);
        return false;
    }
    else
    {
        lua_settop(L, 0);
        return true;
    }
}

ConfigTable ConfigManager::getConfigTable(const char *_name)
{
    lua_getglobal(L, _name);
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        lua_remove(L, -1);
        return ConfigTable();
    }
    else
    {
        int index = luaL_ref(L, LUA_REGISTRYINDEX);
        return ConfigTable(L, index);
    }
}

ConfigTable ConfigManager::getMainTable()
{
    lua_pushglobaltable(L);
    int index = luaL_ref(L, LUA_REGISTRYINDEX);
    return ConfigTable(L, index);
}

void ConfigManager::addSubConfig(SubConfig *_subConfig)
{
    _subConfig->configManager = this;
    _subConfig->initCategories(L);
}