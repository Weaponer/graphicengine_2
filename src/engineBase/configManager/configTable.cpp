#include "engineBase/configManager/configTable.h"

ConfigTable::ConfigTable() : L(NULL), indexTable(0)
{
}

ConfigTable::ConfigTable(lua_State *L, int _indexTable) : L(L), indexTable(_indexTable)
{
}

ConfigTable::~ConfigTable()
{
    if (L != NULL)
    {
        luaL_unref(L, LUA_REGISTRYINDEX, indexTable);
    }
    L = NULL;
    indexTable = 0;
}

ConfigTable ConfigTable::toTable(const char *_name)
{
    if (L == NULL)
    {
        return ConfigTable();
    }

    lua_rawgeti(L, LUA_REGISTRYINDEX, indexTable);

    lua_getfield(L, -1, _name);
    if (lua_type(L, -1) == LUA_TNIL)
    {
        lua_pop(L, 2);
        return ConfigTable();
    }
    else
    {
        int index = luaL_ref(L, LUA_REGISTRYINDEX);
        lua_remove(L, -1);
        return ConfigTable(L, index);
    }
}

int ConfigTable::toInt(const char *_name)
{
    if (L == NULL)
    {
        return 0;
    }

    int val = 0;
    lua_rawgeti(L, LUA_REGISTRYINDEX, indexTable);
    lua_getfield(L, -1, _name);
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        val = lua_tointeger(L, -1);
    }
    lua_pop(L, 2);
    return val;
}

float ConfigTable::toFloat(const char *_name)
{
    if (L == NULL)
    {
        return 0.0f;
    }

    float val = 0;
    lua_rawgeti(L, LUA_REGISTRYINDEX, indexTable);
    lua_getfield(L, -1, _name);
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        val = lua_tonumber(L, -1);
    }
    lua_pop(L, 2);
    return val;
}

const char *ConfigTable::toString(const char *_name)
{
    if (L == NULL)
    {
        return "";
    }

    const char *str = "";
    lua_rawgeti(L, LUA_REGISTRYINDEX, indexTable);
    lua_getfield(L, -1, _name);
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        str = lua_tostring(L, -1);
    }
    lua_pop(L, 2);
    return str;
}

bool ConfigTable::toBool(const char *_name)
{
    if (L == NULL)
    {
        return false;
    }

    bool val = false;
    lua_rawgeti(L, LUA_REGISTRYINDEX, indexTable);
    lua_getfield(L, -1, _name);
    if (lua_type(L, -1) == LUA_TBOOLEAN)
    {
        val = lua_toboolean(L, -1);
    }
    lua_pop(L, 2);
    return val;
}

void ConfigTable::toArrayStrings(const char *_name, std::vector<std::string> *_strings)
{
    if (L == NULL)
    {
        return;
    }

    lua_rawgeti(L, LUA_REGISTRYINDEX, indexTable);
    lua_getfield(L, -1, _name);
    if (lua_type(L, -1) == LUA_TTABLE)
    {
        lua_pushnil(L);
        while (lua_next(L, -2))
        {
            if (lua_type(L, -1) == LUA_TSTRING)
            {
                const char *_str = lua_tostring(L, -1);
                _strings->push_back(_str);
            }
            lua_pop(L, 1);
        }
        lua_pop(L, 1);
    }
    lua_settop(L, 0);
}
