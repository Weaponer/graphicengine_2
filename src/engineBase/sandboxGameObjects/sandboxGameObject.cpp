#include "engineBase/sandboxGameObjects/sandboxGameObjects.h"
#include "objectRegister/iteratorByGameObjects.h"
#include "gameObject/transform.h"
#include "gameObject/gameObject.h"

SandboxGameObjects::SandboxGameObjects(ObjectRegister *_objectRegister, LayerManager *_layerManager)
    : sceneManager(_objectRegister, this), objectRegister(_objectRegister), layerManager(_layerManager)
{
}

SandboxGameObjects::~SandboxGameObjects()
{
}

GameObject *SandboxGameObjects::createNewGameObject(std::string _name)
{
    return createNewGameObject(sceneManager.getCurrentScene(), _name);
}

GameObject *SandboxGameObjects::createNewGameObject(Scene *_scene, std::string _name)
{
    GameObject *gameObject = new GameObject();
    gameObject->setName(_name.c_str());
    gameObject->setScene(_scene);
    gameObject->setLayer(layerManager->getDefaultLayer());
    _scene->addObject(gameObject);

    objectRegister->registerGameObject(gameObject);
    return gameObject;
}

int SandboxGameObjects::parentGameObject(GameObject *_gameObject, GameObject *_destination)
{
    Transform *_go = _gameObject->getTransform();
    Transform *_dest = _destination->getTransform();
    if ((_go->getBranch() == _dest->getBranch() && _go->getBranch() != -1) || _gameObject == _destination)
    {
        return 1;
    }

    if (_go->getParent() != NULL)
    {
        Transform *ptr = _go->getParent();
        _go->setBranch(-1);

        if (ptr->getParent() == NULL)
        {
            freeBranch(ptr->getBranch());
            ptr->setBranch(-1);
        }
    }

    if (_gameObject->getScene() != _destination->getScene())
    {
        moveGameObjectOnAnotherScene(_gameObject, _destination->getScene());
    }
    if (_dest->getBranch() == -1)
    {
        _dest->setBranch(getFreeBranch());
    }

    if(_go->getBranch() != -1)
    {
        freeBranch(_go->getBranch());
    }

    _go->setParent(_dest);
    _go->setBranch(_dest->getBranch());
    _go->updateDatas();
    return 0;
}

void SandboxGameObjects::unparentGameObject(GameObject *_gameObject)
{
    Transform *transform = _gameObject->getTransform();
    Transform *oldParent = transform->getParent();
    if (transform->getCountChildren() != 0)
    {
        int newBranch = getFreeBranch();
        transform->setBranch(newBranch);
        transform->setParent(NULL);
    }
    else
    {
        if (oldParent == NULL)
        {
            freeBranch(transform->getBranch());
        }
        transform->setParent(NULL);
        transform->setBranch(-1);
    }
    transform->updateDatas();

    if (oldParent != NULL && oldParent->getParent() == NULL)
    {
        freeBranch(oldParent->getBranch());
        oldParent->setBranch(-1);
    }
}

void SandboxGameObjects::removeGameObject(GameObject *_gameObject)
{
    Transform *tr = _gameObject->getTransform();
    if (tr->getParent() != NULL)
    {
        tr->setParent(NULL);
    }
    if (tr->getBranch() != -1)
    {
        freeBranch(tr->getBranch());
        tr->setBranch(-1);
    }
    while (tr->getCountChildren() != 0)
    {
        removeGameObject((tr->getChildren(0))->getGameObject());
    }


    Scene *scene = _gameObject->getScene();
    scene->removeObject(_gameObject);
    std::list<Component *> *components = _gameObject->getComponents();
    int sizeComps = components->size();
    while (sizeComps--)
    {
        removeComponent(*(components->begin()));
    }
    objectRegister->removeObject(_gameObject);
    delete _gameObject;
}

void SandboxGameObjects::removeAllGameObjectsInScene(Scene *_scene)
{
    const std::list<GameObject *> *gameObjects = _scene->getArrayObjects();
    while (gameObjects->size() != 0)
    {
        GameObject *gameObject = (*gameObjects->begin());
        removeGameObject(gameObject);
    }
}

void SandboxGameObjects::setGameObjectDontDestroy(GameObject *_gameObject)
{
    moveGameObjectOnAnotherScene(_gameObject, sceneManager.getDontDestroyScene());
}

void SandboxGameObjects::moveGameObjectOnAnotherScene(GameObject *_gameObject, Scene *_scene)
{
    Transform *tr = _gameObject->getTransform();
    if (tr->getCountChildren() != 0)
    {
        const std::list<Transform *> *children = tr->getListChildren();
        for (auto i : *children)
        {
            moveGameObjectOnAnotherScene(i->getGameObject(), _scene);
        }
    }

    Scene *oldScene = _gameObject->getScene();
    if (oldScene != _scene)
    {
        oldScene->removeObject(_gameObject);
        _gameObject->setScene(_scene);
        _scene->addObject(_gameObject);
    }
}

void SandboxGameObjects::addComponent(GameObject *_gameObject, Component *_component)
{
    objectRegister->registerExtraObject(_component);
    _gameObject->addComponent(_component);
    _component->init(_gameObject);
}

void SandboxGameObjects::removeComponent(Component *_component)
{
    GameObject *gameObject = _component->getGameObject();
    _component->destroy();
    gameObject->removeComponent(_component);
    objectRegister->removeObject(_component);
    delete _component;
}

void SandboxGameObjects::updateComponents()
{
    IteratorByGameObjects iter = objectRegister->getIteratorByGameObjects();
    GameObject *obj = NULL;
    while (true)
    {
        obj = iter.getNext();
        if (obj == NULL)
        {
            break;
        }
        std::list<Component *> *components = obj->getComponents();
        for (auto i : *components)
        {
            i->update();
        }
    }
}

void SandboxGameObjects::clearMasksGameObjects()
{
    IteratorByGameObjects iter = objectRegister->getIteratorByGameObjects();
    GameObject *obj = NULL;
    while (true)
    {
        obj = iter.getNext();
        if (obj == NULL)
        {
            break;
        }
        Transform *transform = obj->getTransform();
        transform->resetMaskChange();
    }
}

int SandboxGameObjects::getFreeBranch()
{
    return branches.getBranch();
}

void SandboxGameObjects::freeBranch(int _branch)
{
    branches.freeBranch(_branch);
}