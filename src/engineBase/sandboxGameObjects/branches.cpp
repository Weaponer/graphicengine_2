#include "engineBase/sandboxGameObjects/branches.h"

Branches::Branches()
{
    nextBranch = 0;
}

Branches::~Branches()
{
    freeBranches.clearMemory();
}

void Branches::freeBranch(int _branch)
{
    freeBranches.assignmentMemoryAndCopy(&_branch);
}

int Branches::getBranch()
{
    if (freeBranches.getPos() != 0)
    {
        return *freeBranches.getMemoryAndPop();
    }

    int branch = nextBranch;
    nextBranch++;
    return branch;
}