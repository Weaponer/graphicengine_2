#include "engineBase/sandboxGameObjects/scene.h"

Scene::Scene(const char *_name) : typeScene(TypeScene::NONE_INIT_SCENE), name(_name)
{
}

Scene::~Scene()
{
}

void Scene::addObject(GameObject *_obj)
{
    objects.push_back(_obj);
}

void Scene::removeObject(GameObject *_obj)
{
    objects.remove(_obj);
}
