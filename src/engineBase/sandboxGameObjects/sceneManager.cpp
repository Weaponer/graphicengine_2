#include <string.h>
#include "engineBase/sandboxGameObjects/sceneManager.h"
#include "engineBase/sandboxGameObjects/sandboxGameObjects.h"

SceneManager::SceneManager(ObjectRegister *_objectRegister, SandboxGameObjects *_sandboxGameObjects)
    : sandboxGameObjects(_sandboxGameObjects), objectRegister(_objectRegister)
{
    dontDestroyScene = new Scene("DontDestroy");
    dontDestroyScene->setTypeScene(TypeScene::DONT_DESTROY_SCENE);
    objectRegister->registerExtraObject(dontDestroyScene);

    pushDefaultScene();
}

SceneManager::~SceneManager()
{
    int size = scenes.size();
    while (size--)
    {
        removeScene(*scenes.begin());
    }

    destroyScene(dontDestroyScene);
}

Scene *SceneManager::createNewScene(const char *_name)
{
    Scene *scene = new Scene(_name);
    objectRegister->registerExtraObject(scene);
    scenes.push_back(scene);
    scene->setTypeScene(TypeScene::ADDITIONAL_SCENE);
    return scene;
}

bool SceneManager::removeScene(Scene *_scene)
{
    auto end = scenes.end();
    auto found = std::find(scenes.begin(), scenes.end(), _scene);
    bool needChangeCurrentScene = false;
    if (found != end)
    {
        Scene *scene = *found;
        if (scene == currentScene)
        {
            needChangeCurrentScene = true;
            currentScene = NULL;
        }
        scenes.erase(found);
        destroyScene(scene);
    }
    else
    {
        return false;
    }

    if (scenes.size() == 0)
    {
        pushDefaultScene();
    }
    else if(needChangeCurrentScene)
    {
        currentScene = *scenes.begin();
        currentScene->setTypeScene(TypeScene::CURRENT_SCENE);
    }
    return true;
}

Scene *SceneManager::getCurrentScene() const
{
    return currentScene;
}

Scene *SceneManager::getDontDestroyScene() const
{
    return dontDestroyScene;
}

Scene *SceneManager::getSceneByName(const char *_name) const
{
    for (auto i : scenes)
    {
        if (strcmp(i->getName(), _name) == 0)
        {
            return i;
        }
    }
    return NULL;
}

void SceneManager::setSceneDefault(Scene *_scene)
{
    currentScene->setTypeScene(TypeScene::ADDITIONAL_SCENE);
    currentScene = _scene;
    _scene->setTypeScene(TypeScene::CURRENT_SCENE);
}

const std::list<Scene *> *SceneManager::getListScenes() const
{
    return &scenes;
}

void SceneManager::pushDefaultScene()
{
    Scene *scene = new Scene("Default");
    objectRegister->registerExtraObject(scene);
    scene->setTypeScene(TypeScene::CURRENT_SCENE);
    currentScene = scene;
    scenes.push_back(scene);
}

void SceneManager::destroyScene(Scene *_scene)
{
    sandboxGameObjects->removeAllGameObjectsInScene(_scene);
    objectRegister->removeObject(_scene);
    delete _scene;
}