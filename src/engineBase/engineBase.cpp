#include <time.h>
#include <unistd.h>
#include <chrono>

#include "engineBase/engineBase.h"

#include "debugOutput/globalProfiler.h"

#include "renderEngine/resources/texture.h"
#include "renderEngine/resources/technicalShader/tshader.h"
#include "layerManager/layerData.h"

#include "gameObject/graphicComponents/camera.h"

#include "debugOutput/threadDebugOutput.h"

#include "gameObject/scriptComponents/scriptComponent.h"

#include "resourceLoader/resources/textFile.h"

void EngineBase::init()
{
    globalRegister = new ObjectRegister();
    specialSystems = new SpecialSystems();
    resourceLoader = new ResourceLoader(globalRegister, specialSystems);

    loadConfigs();

    window = new Window(graphicConfig.getGraphicInternalTable().toInt("Width"), graphicConfig.getGraphicInternalTable().toInt("Height"));

    layerManager = new LayerManager();
    LayerData *lData = (LayerData *)resourceLoader->loadResource("configs/layerData.cfg", TypeResource::LAYER_DATA);
    layerManager->initLayerManager(lData);
    resourceLoader->unloadResource(lData);

    debugOutput = new DebugOutput();
    ThreadDebugOutput::mainThreadDebugOutput = debugOutput->getMainDebugOutput();

    initGraphyicEngine();
    debugOutput->connectGraphicEngine(graphicEngine);

    physicsEngine = new PhysicsEngine(layerManager, debugOutput->createSubDebugOutput());

    timeData.lock = 60;
    timeData.deltaTime = 0;

    specialSystems->graphicEngine = graphicEngine;
    specialSystems->inputSystem = window->getInputSystem();
    specialSystems->mainDebugOutput = debugOutput->getMainDebugOutput();
    specialSystems->physicsEngine = physicsEngine;
    specialSystems->resourceLoader = resourceLoader;
    specialSystems->timeData = &timeData;
    specialSystems->window = window;
    specialSystems->layerManager = layerManager;

    scriptEngine = new ScriptEngine(globalRegister, resourceLoader, specialSystems);

    sandboxGameObjects = new SandboxGameObjects(globalRegister, layerManager);
    specialSystems->sandboxGameObjects = sandboxGameObjects;
}

void EngineBase::destroy()
{
    delete sandboxGameObjects;
    delete scriptEngine;
    delete specialSystems;
    delete physicsEngine;
    delete debugOutput;
    delete graphicEngine;
    delete layerManager;
    delete resourceLoader;
    globalRegister->clear();
    delete globalRegister;
    delete window;
}
/*
static void printHandler(ResourceAsyncHandler *handler)
{
    printf("path: %s\n", handler->getPath());
    if (handler->getStatus() == StatusLoad::SL_Failed)
    {
        printf("    status: failed %s\n", handler->getPath());
    }
    if (handler->getStatus() == StatusLoad::SL_None)
    {
        printf("    status: none %s\n", handler->getPath());
    }
    if (handler->getStatus() == StatusLoad::SL_Succeed)
    {
        printf("    status: succeed %s\n", handler->getPath());
    }
}*/

void EngineBase::loadConfigs()
{
    configManager.addSubConfig(&graphicConfig);
    loadConfig("configs/paths.cfg");
    loadConfig("configs/graphicEngineInternal.cfg");
    loadConfig("configs/graphicEngineCompiler.cfg");
    loadConfig("configs/graphicEngine.cfg");

    specialSystems->configManager = &configManager;
    specialSystems->graphicConfig = &graphicConfig;
}

void EngineBase::loadConfig(const char *_path)
{
    TextFile *textFile = (TextFile *)resourceLoader->loadResource(_path, TypeResource::TEXT_FILE);
    configManager.readConfigFile(textFile);
    resourceLoader->unloadResource(textFile);
}

void EngineBase::loadShaders()
{
    std::vector<std::string> paths;
    configManager.getMainTable().toArrayStrings("BasicShaders", &paths);
    for(int i = 0; i < (int)paths.size(); i++)
    {
        resourceLoader->loadResource(paths[i].c_str(), TypeResource::SHADER);
    }
}

void EngineBase::loadGraphicSettings()
{
    SettingsGraphicPipeline *settingsGraphicPipeline = graphicEngine->getSettingsGraphicPipeline();
    settingsGraphicPipeline->sky.skyMaterial = (Material *)resourceLoader->loadResource("materials/sky.mat", TypeResource::MATERIAL);
}

void EngineBase::loadScripts()
{
    std::vector<std::string> paths;
    configManager.getMainTable().toArrayStrings("LuaScripts", &paths);
    for(int i = 0; i < (int)paths.size(); i++)
    {
        scriptEngine->addScript(paths[i].c_str());
    }
}

void EngineBase::startMainLoop()
{
    loadShaders();
    loadGraphicSettings();
    loadScripts();

    GameObject *initGameObject = sandboxGameObjects->createNewGameObject("Init");
    ScriptComponent *initScript = new ScriptComponent(scriptEngine);
    sandboxGameObjects->addComponent(initGameObject, initScript);
    initScript->setType("init");

    GameObject *camObj = sandboxGameObjects->createNewGameObject("Camera");
    Camera *cameraComp = new Camera(graphicEngine);
    sandboxGameObjects->addComponent(camObj, cameraComp);
    cameraComp->setPerspectiveProjection(70, settingsMainSystems.resolutionSettings.width, settingsMainSystems.resolutionSettings.height, 0.4f, 430.0f);
    ScriptComponent *testScript = new ScriptComponent(scriptEngine);
    sandboxGameObjects->addComponent(camObj, testScript);
    testScript->setType("cameraControl");

    std::chrono::_V2::system_clock::time_point start, end;
    float lock = 1.0f / timeData.lock;
    while (window->updateEvent())
    {
        std::cout << timeData.deltaTime << std::endl;

        start = std::chrono::high_resolution_clock::now();

        resourceLoader->update();
        physicsEngine->update(timeData.deltaTime);
        scriptEngine->update();
        sandboxGameObjects->updateComponents();
        sandboxGameObjects->clearMasksGameObjects();
        debugOutput->update(timeData.deltaTime);
        GlobalProfiler::startSample("Graphic");
        graphicEngine->render(timeData.deltaTime);
        GlobalProfiler::stopSample("Graphic");

        graphicEngine->endRender();

        scriptEngine->lateUpdate();


        end = std::chrono::high_resolution_clock::now();
        timeData.deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000000.0f;

        if (lock > timeData.deltaTime)
        {
            usleep((lock - timeData.deltaTime) * 1000);
            timeData.deltaTime += (lock - timeData.deltaTime);
        }
        graphicEngine->swapWindow();

    }

    GlobalProfiler::writeResultInFile("./profileResult");
}

void EngineBase::loadTechnicalShader(const char *_path)
{
    TShader *tshader = (TShader *)resourceLoader->loadResource(_path, TypeResource::PACKED_SHADER);
    settingsMainSystems.techincalShaderStorage.addTechnicalShader(tshader->getName(), tshader);
}

void EngineBase::loadModelLight(const char *_path)
{
    ModelLightShader *modelLight = (ModelLightShader *)resourceLoader->loadResource(_path, TypeResource::MODEL_LIGHT_SHADER);
    settingsMainSystems.modelLightShaderStorage.addModelLight(modelLight);
}
void EngineBase::loadModelLights()
{
    std::vector<std::string> paths;
    configManager.getMainTable().toArrayStrings("ShaderModels", &paths);
    for(int i = 0; i < (int)paths.size(); i++)
    {
        loadModelLight(paths[i].c_str());
    }
}

void EngineBase::initGraphyicEngine()
{
    settingsMainSystems.graphicConfig = graphicConfig;
    settingsMainSystems.resolutionSettings.width = window->getWidth();
    settingsMainSystems.resolutionSettings.height = window->getHeight();

    std::vector<std::string> paths;
    configManager.getMainTable().toArrayStrings("TechnicalShaders", &paths);
    for(int i = 0; i < (int)paths.size(); i++)
    {
        loadTechnicalShader(paths[i].c_str());
    }

    loadModelLights();

    std::cout << "Start init graphicEngine" << std::endl;
    graphicEngine = new GraphicEngine(&settingsMainSystems);
    std::cout << "End init graphicEngine" << std::endl;

    graphicEngine->connectSDLWindow(window);
}