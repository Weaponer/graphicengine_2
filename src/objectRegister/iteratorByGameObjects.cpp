#include "objectRegister/iteratorByGameObjects.h"
#include "objectRegister/objectRegister.h"

IteratorByGameObjects::IteratorByGameObjects(Indexedtable<ObjectReference> *_table)
{
    table = _table;
    Reset();
}

IteratorByGameObjects::~IteratorByGameObjects()
{
}

GameObject *IteratorByGameObjects::getNext()
{
    sizeM limit = table->getPosBuffer();
    pos++;
    while (true)
    {
        if (limit <= pos)
        {
            end = true;
            return NULL;
        }
        ObjectReference mem = *table->getMemory(pos);
        if (mem.object == NULL)
        {
            pos++;
            continue;
        }
        else
        {
            return (GameObject *)mem.object;
        }
    }
}

bool IteratorByGameObjects::isEnd()
{
    return end;
}

void IteratorByGameObjects::Reset()
{
    end = false;
    pos = -1;
}