#include "objectRegister/objectRegister.h"
#include "stdio.h"
ObjectRegister::ObjectRegister() : events(NULL), resources(1024), gameObjects(1024), extraObjects(1024)
{
}

ObjectRegister::~ObjectRegister()
{
}

void ObjectRegister::setPointEvents(ObjectRegisterEvents *e)
{
    events = e;
}

void ObjectRegister::discardPointEvents()
{
    events = NULL;
}

long long ObjectRegister::getCodeRegistration(Object *obj)
{
    return obj->index;
}

void ObjectRegister::registerResource(Object *obj)
{
    sizeM posMemory = 0;
    ObjectReference *o = resources.assignmentMemory(posMemory);
    o->object = obj;
    o->indexAdditionalData = -1;
    obj->index = posMemory;
    if (events != NULL)
    {
        events->onRegisterResource(obj);
        events->onRegisterAnyObject(obj);
    }
}

Object *ObjectRegister::getResourceByIndex(uint index)
{
    return resources.getMemory(index)->object;
}

void ObjectRegister::registerGameObject(GameObject *obj)
{
    sizeM posMemory = 0;
    ObjectReference *o = gameObjects.assignmentMemory(posMemory);
    o->object = obj;
    o->indexAdditionalData = -1;
    obj->index = posMemory;
    if (events != NULL)
    {
        events->onRegisterGameObject(obj);
        events->onRegisterAnyObject(obj);
    }
}

IteratorByGameObjects ObjectRegister::getIteratorByGameObjects()
{
    IteratorByGameObjects iter = IteratorByGameObjects(&gameObjects);
    return iter;
}

void ObjectRegister::registerExtraObject(Object *obj)
{
    sizeM posMemory = 0;
    ObjectReference *o = extraObjects.assignmentMemory(posMemory);
    o->object = obj;
    o->indexAdditionalData = -1;

    obj->index = posMemory;
    if (events != NULL)
    {
        events->onRegisterExtraObject(obj);
        events->onRegisterAnyObject(obj);
    }
}

void ObjectRegister::removeObject(Object *obj)
{
    Indexedtable<ObjectReference> *table = getTableWithObject(obj);
    ObjectReference *mem = table->getMemory(obj->index);

    if (events != NULL)
    {
        if (table == &resources)
        {
            events->onRemoveResource(obj);
            events->onRemoveAnyObject(obj);
        }
        if (table == &gameObjects)
        {
            events->onRemoveGameObject((GameObject *)obj);
            events->onRemoveAnyObject(obj);
        }
        if (table == &extraObjects)
        {
            events->onRegisterExtraObject(obj);
            events->onRemoveAnyObject(obj);
        }
    }
    if (mem->indexAdditionalData != -1)
    {
        IteratorOnData *iter = iteratorsOnDatas.getMemory(mem->indexAdditionalData);
        while (iter != NULL)
        {
            IteratorOnData *cur = iter;
            if (iter->next == -1)
            {
                iter = NULL;
            }
            else
            {
                iter = iteratorsOnDatas.getMemory(iter->next);
            }
            cur->next = -1;
            cur->back = -1;
            cur->data = NULL;
            cur->isNull = true;
            iteratorsOnDatas.popMemory(cur->index);
            cur->index = 0;
        }
    }
    mem->object = NULL;
    mem->indexAdditionalData = -1;
    table->popMemory(obj->index);
}

void ObjectRegister::addSetAdditionalData(Object *obj, int code, void *data)
{
    ObjectReference *reference = getObjectReference(obj);
    IteratorOnData *iter = getIretator(obj, code);
    if (iter != NULL)
    {
        iter->data = data;
        return;
    }

    sizeM index;
    iter = iteratorsOnDatas.assignmentMemory(index);
    iter->index = index;
    iter->isNull = false;
    iter->data = data;
    iter->code = code;
    iter->next = -1;
    iter->back = -1;

    if (reference->indexAdditionalData == -1)
    {
        reference->indexAdditionalData = iter->index;
        return;
    }
    else
    {
        IteratorOnData *curIter = iteratorsOnDatas.getMemory(reference->indexAdditionalData);
        while (true)
        {
            if (curIter->next == -1)
            {
                curIter->next = iter->index;
                iter->back = curIter->index;
                break;
            }

            curIter = iteratorsOnDatas.getMemory(curIter->next);
        }
    }
}

void *ObjectRegister::getAdditionalData(Object *obj, int code)
{
    ObjectReference *reference = getObjectReference(obj);
    if (reference == NULL || reference->indexAdditionalData == -1)
    {
        return NULL;
    }
    IteratorOnData *iterator = getIretator(obj, code);
    if (iterator == NULL)
    {
        return NULL;
    }
    return iterator->data;
}

void ObjectRegister::removeAdditionalData(Object *obj, int code)
{
    ObjectReference *reference = getObjectReference(obj);
    if (reference->indexAdditionalData == -1)
    {
        return;
    }

    IteratorOnData *iterator = getIretator(obj, code);
    if (iterator == NULL)
    {
        reference->indexAdditionalData = -1;
        return;
    }
    if (iterator->back != -1 && iterator->next != -1)
    {
        IteratorOnData *backItor = iteratorsOnDatas.getMemory(iterator->back);
        IteratorOnData *nextItor = iteratorsOnDatas.getMemory(iterator->next);
        nextItor->back = backItor->index;
        backItor->next = nextItor->index;
    }
    else if (iterator->back != -1)
    {
        IteratorOnData *backItor = iteratorsOnDatas.getMemory(iterator->back);
        backItor->next = 0;
    }
    else if (iterator->next != -1)
    {
        IteratorOnData *nextItor = iteratorsOnDatas.getMemory(iterator->next);
        nextItor->back = 0;
        reference->indexAdditionalData = nextItor->index;
    }
    else
    {
        reference->indexAdditionalData = -1;
    }

    iterator->next = 0;
    iterator->back = 0;
    iterator->data = NULL;
    iterator->isNull = true;
    iterator->index = 0;
    iteratorsOnDatas.popMemory(iterator->index);
}

void ObjectRegister::clear()
{
    printf("--%i\n", iteratorsOnDatas.getCountUse());
    clearGameObjects();
    clearResources();
    clearExtraObjects();
}

void ObjectRegister::clearGameObjects()
{
    int posBuffer = gameObjects.getPosBuffer();
    for (int i = 0; i < posBuffer; i++)
    {
        ObjectReference *ref = gameObjects.getMemory(i);
        if (ref->object != NULL)
        {
            removeObject(ref->object);
        }
    }
}

void ObjectRegister::clearResources()
{
    int posBuffer = resources.getPosBuffer();
    for (int i = 0; i < posBuffer; i++)
    {
        ObjectReference *ref = resources.getMemory(i);
        if (ref->object != NULL)
        {
            removeObject(ref->object);
        }
    }
}

void ObjectRegister::clearExtraObjects()
{
    int posBuffer = extraObjects.getPosBuffer();
    for (int i = 0; i < posBuffer; i++)
    {
        ObjectReference *ref = extraObjects.getMemory(i);
        if (ref->object != NULL)
        {
            removeObject(ref->object);
        }
    }
}

Indexedtable<ObjectReference> *ObjectRegister::getTableWithObject(Object *object)
{
    if (object->index < resources.getPosBuffer())
    {
        ObjectReference *mem = resources.getMemory(object->index);
        if (mem->object == object)
        {
            return &resources;
        }
    }

    if (object->index < gameObjects.getPosBuffer())
    {
        ObjectReference *mem = gameObjects.getMemory(object->index);
        if (mem->object == object)
        {
            return &gameObjects;
        }
    }

    if (object->index < extraObjects.getPosBuffer())
    {
        ObjectReference *mem = extraObjects.getMemory(object->index);
        if (mem->object == object)
        {
            return &extraObjects;
        }
    }
    return NULL;
}

ObjectReference *ObjectRegister::getObjectReference(Object *object)
{
    if (object->index < resources.getPosBuffer())
    {
        ObjectReference *mem = resources.getMemory(object->index);
        if (mem->object == object)
        {
            return mem;
        }
    }

    if (object->index < gameObjects.getPosBuffer())
    {
        ObjectReference *mem = gameObjects.getMemory(object->index);
        if (mem->object == object)
        {
            return mem;
        }
    }

    if (object->index < extraObjects.getPosBuffer())
    {
        ObjectReference *mem = extraObjects.getMemory(object->index);
        if (mem->object == object)
        {
            return mem;
        }
    }
    return NULL;
}

ObjectRegister::IteratorOnData *ObjectRegister::getIretator(Object *obj, int code)
{
    ObjectReference *reference = getObjectReference(obj);
    if (reference->indexAdditionalData == -1)
    {
        return NULL;
    }

    IteratorOnData *iterator = iteratorsOnDatas.getMemory(reference->indexAdditionalData);
    int i = 0;
    while (iterator != NULL)
    {
        if (iterator->code == code)
        {
            return iterator;
        }
        if (iterator->next == -1)
        {
            iterator = NULL;
        }
        else
        {
            iterator = iteratorsOnDatas.getMemory(iterator->next);
        }

        i++;
    }
    return NULL;
}
