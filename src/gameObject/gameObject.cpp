#include "gameObject/gameObject.h"

GameObject::GameObject() : transform(this)
{
    setActive(true);
}

GameObject::~GameObject()
{
}

void GameObject::addComponent(Component *component)
{
    components.push_back(component);
}

void GameObject::removeComponent(Component *component)
{
    components.remove(component);
}

void GameObject::setLayer(Layer _layer)
{
    if (layer != _layer)
    {
        layer = _layer;
        for (auto i : components)
        {
            i->updateOnNewLayer(_layer);
        }
    }
}

void GameObject::setActive(bool _active)
{
    transform.setActive(_active);
}

bool GameObject::getActive()
{
    return transform.getActive();
}

bool GameObject::getLocalActive()
{
    return transform.getLocalActive();
}

void GameObject::updateStatusActive()
{
    for(auto i : components)
    {
        (*i).upadteOnActive();
    }
}