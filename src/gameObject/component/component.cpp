#include "gameObject/component/component.h"
#include "gameObject/gameObject.h"

Component::Component()
{
}

Component::~Component()
{
}

void Component::init(GameObject *_gameObject)
{
    gameObject = _gameObject;
    transform = _gameObject->getTransform();
    oldState = false;
    onInit();
    setActive(true);
}

void Component::destroy()
{
    setActive(false);
    onDestroy();
    gameObject = NULL;
}

void Component::update()
{
}

Transform *Component::getTransform()
{
    return gameObject->getTransform();
}

GameObject *Component::getGameObject()
{
    return gameObject;
}

void Component::onInit()
{
}

void Component::onDestroy()
{
}

void Component::setActive(bool _active)
{
    localActive = _active;
    upadteOnActive();
}

bool Component::getActive()
{
    if (localActive == true && gameObject->getActive() == true)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Component::getLocalActive()
{
    return localActive;
}

void Component::upadteOnActive()
{
    bool active = getActive();
    if (active != oldState)
    {
        oldState = active;
    }
    if (active)
    {
        onEnable();
    }
    else
    {
        onDisable();
    }
}