#include <MTH/boundAABB.h>
#include <MTH/glGen_matrix.h>

#include "gameObject/gameObject.h"
#include "meshRenderer.h"

MeshRenderer::MeshRenderer(GraphicEngine *gEngine) : gEngine(gEngine), sceneStructure(gEngine->getSceneStructure()), changeBound(false)
{
}

MeshRenderer::~MeshRenderer()
{
}

Material *MeshRenderer::getMaterial(int _lod) const
{
    if (_lod <= meshRenderGE.countLods())
    {
        return meshRenderGE.getLod(_lod).first;
    }
    else
    {
        return NULL;
    }
}

Mesh *MeshRenderer::getMesh(int _lod) const
{
    if (_lod <= meshRenderGE.countLods())
    {
        return meshRenderGE.getLod(_lod).second;
    }
    else
    {
        return NULL;
    }
}

float MeshRenderer::getCoefHeightLod(int _lod) const
{
    if (_lod <= meshRenderGE.countLods())
    {
        return meshRenderGE.getHeightCoefLod(_lod);
    }
    else
    {
        return 0.0f;
    }
}

void MeshRenderer::setMaterial(Material *m, int _lod)
{
    if (_lod <= meshRenderGE.countLods())
    {
        meshRenderGE.setMaterialInLod(_lod, m);
    }
}

void MeshRenderer::setMesh(Mesh *m, int _lod)
{
    if (_lod > meshRenderGE.countLods())
    {
        return;
    }

    meshRenderGE.setMeshInLod(_lod, m);

    if (_lod == 0 && m != NULL)
    {
        meshRenderGE.bound = m->getBoundAABB();
        meshRenderGE.bound.origin.componentProductUpdate(meshRenderGE.scale);
        meshRenderGE.bound.size.componentProductUpdate(meshRenderGE.scale);

        meshRenderGE.bound.resizeForRotate(meshRenderGE.rotate);
        meshRenderGE.bound.origin += meshRenderGE.position;
        changeBound = true;
    }
    else if (_lod == 0 && m == NULL)
    {
        meshRenderGE.bound = mth::boundAABB();
        changeBound = true;
    }
}

void MeshRenderer::setHeightCoefLod(int _lod, float _coefHeight)
{
    if (_lod > meshRenderGE.countLods())
    {
        return;
    }
    _coefHeight = std::max(0.0f, std::min(_coefHeight, 1.0f));
    meshRenderGE.setHeightCoefInLod(_lod, _coefHeight);
}

bool MeshRenderer::getGenerateShadows() const
{
    return meshRenderGE.generateShadows;
}

void MeshRenderer::setGenerateShadows(bool _generate)
{
    meshRenderGE.generateShadows = _generate;
}

int MeshRenderer::coundLods() const
{
    return meshRenderGE.countLods();
}

void MeshRenderer::addLod()
{
    return meshRenderGE.addLod();
}

void MeshRenderer::removeLod(int _lod)
{
    return meshRenderGE.removeLod(_lod);
}

void MeshRenderer::onInit()
{
    meshRenderGE.objectGridData.active = true;
    meshRenderGE.material = NULL;
    meshRenderGE.mesh = NULL;
    meshRenderGE.position = transform->getPosition();
    meshRenderGE.rotate = transform->getQuaternion();
    meshRenderGE.scale = transform->getScale();
    meshRenderGE.matrices.globalMat = transform->getGlobalMatrix();
    meshRenderGE.matrices.invGlobalMat = mth::inverseTRS(meshRenderGE.matrices.globalMat);
    meshRenderGE.bound = mth::boundAABB();
    meshRenderGE.typeEntity = TypeGraphicEntity::MESH_RENDER_GRAPHIC_ENTITY;

    changeBound = true;
    sceneStructure->addEntityGE(TypeGraphicEntity::MESH_RENDER_GRAPHIC_ENTITY, &meshRenderGE);

    meshRenderGE.matricesUpdated();
}

void MeshRenderer::onDestroy()
{
    sceneStructure->removeEntityGE(&meshRenderGE);
}

void MeshRenderer::onEnable()
{
    meshRenderGE.objectGridData.active = true;
}

void MeshRenderer::update()
{
    if (transform->getMaskChange() != 0 || changeBound)
    {
        meshRenderGE.position = transform->getPosition();
        meshRenderGE.rotate = transform->getQuaternion();
        meshRenderGE.scale = transform->getScale();
        meshRenderGE.matrices.globalMat = transform->getGlobalMatrix();
        meshRenderGE.matrices.invGlobalMat = mth::inverseTRS(meshRenderGE.matrices.globalMat);

        if (meshRenderGE.mesh != NULL)
        {
            meshRenderGE.bound = meshRenderGE.mesh->getBoundAABB();
            meshRenderGE.bound.origin.componentProductUpdate(meshRenderGE.scale);
            meshRenderGE.bound.size.componentProductUpdate(meshRenderGE.scale);

            meshRenderGE.bound.resizeForRotate(meshRenderGE.rotate);
            meshRenderGE.bound.origin += meshRenderGE.position;
        }
        sceneStructure->changeEntityOrientation(&meshRenderGE);
        meshRenderGE.matricesUpdated();

        changeBound = false;
    }
}

void MeshRenderer::onDisable()
{
    meshRenderGE.objectGridData.active = false;
}