#include "gameObject/graphicComponents/camera.h"
#include "gameObject/gameObject.h"

Camera::Camera(GraphicEngine *gEngine) : Component(), gEngine(gEngine), cameraGE()
{
}

Camera::~Camera()
{
}

void Camera::setOrtographicProjection(float wight, float height, float near, float front)
{
    cameraGE.matrixPojection = mth::ortho(-wight, wight, -height, height, near, front);
    cameraGE.inverseMatrixPojection = mth::inverseOrtho(-wight, wight, -height, height, near, front);

    cameraGE.nearPlane = near;
    cameraGE.farPlane = front;

    cameraGE.halfWidth = wight;
    cameraGE.halfHeight = height;

    cameraGE.clippingPlanes[0] = mth::plane(-near, mth::vec3(0, 0, -1)); // rear
    cameraGE.clippingPlanes[1] = mth::plane(front, mth::vec3(0, 0, 1));  // front

    cameraGE.clippingPlanes[2] = mth::plane(wight, mth::vec3(1, 0, 0));  // left
    cameraGE.clippingPlanes[3] = mth::plane(wight, mth::vec3(-1, 0, 0)); // right

    cameraGE.clippingPlanes[4] = mth::plane(height, mth::vec3(0, 1, 0));  // top
    cameraGE.clippingPlanes[5] = mth::plane(height, mth::vec3(0, -1, 0)); // buttom

    cameraGE.pointsPiramidClipping[0] = mth::vec3(wight, height, near);
    cameraGE.pointsPiramidClipping[1] = mth::vec3(-wight, height, near);
    cameraGE.pointsPiramidClipping[2] = mth::vec3(wight, -height, near);
    cameraGE.pointsPiramidClipping[3] = mth::vec3(-wight, -height, near);

    cameraGE.pointsPiramidClipping[4] = mth::vec3(wight, height, front);
    cameraGE.pointsPiramidClipping[5] = mth::vec3(-wight, height, front);
    cameraGE.pointsPiramidClipping[6] = mth::vec3(wight, -height, front);
    cameraGE.pointsPiramidClipping[7] = mth::vec3(-wight, -height, front);
}

void Camera::setPerspectiveProjection(float angle, float sizeScreenX, float sizeScreenY, float near, float front)
{
    float aspect = sizeScreenX / sizeScreenY;

    cameraGE.matrixPojection = mth::perspective(angle, aspect, near, front);
    cameraGE.inverseMatrixPojection = mth::inversePerspective(angle, aspect, near, front);

    cameraGE.nearPlane = near;
    cameraGE.farPlane = front;

    cameraGE.clippingPlanes[0] = mth::plane(-near, mth::vec3(0, 0, -1)); // rear
    cameraGE.clippingPlanes[1] = mth::plane(front, mth::vec3(0, 0, 1));  // front

    float ab = front * tan(angle * RADIAN / 2);

    mth::vec2 rightP = mth::vec2(-ab * aspect, front);
    rightP = rightP.perpendecular();
    rightP.normalise();

    mth::vec2 leftP = mth::vec2(-rightP.x, rightP.y);

    cameraGE.clippingPlanes[2] = mth::plane(0, mth::vec3(leftP.x, 0, leftP.y));   // left
    cameraGE.clippingPlanes[3] = mth::plane(0, mth::vec3(rightP.x, 0, rightP.y)); // right

    mth::vec2 bottomP = mth::vec2(-ab, front);
    bottomP = bottomP.perpendecular();
    bottomP.normalise();

    mth::vec2 topP = mth::vec2(-bottomP.x, bottomP.y);

    cameraGE.clippingPlanes[4] = mth::plane(0, mth::vec3(0, topP.x, topP.y));       // top
    cameraGE.clippingPlanes[5] = mth::plane(0, mth::vec3(0, bottomP.x, bottomP.y)); // bottom

    float abN = near * tan(angle * RADIAN / 2);

    cameraGE.pointsPiramidClipping[0] = mth::vec3(abN * aspect, abN, near);
    cameraGE.pointsPiramidClipping[1] = mth::vec3(-abN * aspect, abN, near);
    cameraGE.pointsPiramidClipping[2] = mth::vec3(abN * aspect, -abN, near);
    cameraGE.pointsPiramidClipping[3] = mth::vec3(-abN * aspect, -abN, near);

    cameraGE.pointsPiramidClipping[4] = mth::vec3(ab * aspect, ab, front);
    cameraGE.pointsPiramidClipping[5] = mth::vec3(-ab * aspect, ab, front);
    cameraGE.pointsPiramidClipping[6] = mth::vec3(ab * aspect, -ab, front);
    cameraGE.pointsPiramidClipping[7] = mth::vec3(-ab * aspect, -ab, front);
    cameraGE.halfWidth = ab * aspect;
    cameraGE.halfHeight = ab;
}

void Camera::setProjectionMatrix(mth::mat4 &matrix)
{
    cameraGE.matrixPojection = matrix;
}

mth::mat4 Camera::getProjectionMatrix()
{
    return cameraGE.matrixPojection;
}

const std::array<mth::plane, 6> *Camera::getClippingPlanes()
{
    return &(cameraGE.clippingPlanes);
}

const std::array<mth::vec3, 8> *Camera::getPointsPiramidClipping()
{
    return &(cameraGE.pointsPiramidClipping);
}

void Camera::onInit()
{
    cameraGE.typeEntity = TypeGraphicEntity::CAMERA_GRAPHIC_ENTITY;
    cameraGE.enable = true;
    mth::vec2 sizeS = gEngine->getSizeWindow();
    setPerspectiveProjection(70, sizeS.x, sizeS.y, 0.1f, 80);
    cameraGE.position = transform->getPosition();
    cameraGE.rotate = transform->getQuaternion();
    gEngine->getSceneStructure()->addEntityGE(TypeGraphicEntity::CAMERA_GRAPHIC_ENTITY, &cameraGE);
}

void Camera::onDestroy()
{
    gEngine->getSceneStructure()->removeEntityGE(&cameraGE);
}

void Camera::onEnable()
{
    cameraGE.enable = true;
}

void Camera::update()
{
    cameraGE.position = transform->getPosition();
    cameraGE.rotate = transform->getQuaternion();
}

void Camera::onDisable()
{
    cameraGE.enable = false;
}
