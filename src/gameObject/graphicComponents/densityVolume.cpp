#include "gameObject/graphicComponents/densityVolume.h"

DensityVolume::DensityVolume(GraphicEngine *gEngine) : gEngine(gEngine), sceneStructure(gEngine->getSceneStructure())
{
}

DensityVolume::~DensityVolume()
{
}

void DensityVolume::onInit()
{
    densityVolume.typeEntity = TypeGraphicEntity::DENSITY_VOLUME_GRAPHIC_ENTITY;
    densityVolume.position = transform->getPosition();
    densityVolume.rotate = transform->getQuaternion();
    densityVolume.scale = transform->getScale();

    densityVolume.bound = mth::boundAABB(mth::vec3(0, 0, 0), mth::vec3(1, 1, 1));
    densityVolume.bound.size.componentProductUpdate(densityVolume.scale);

    densityVolume.bound.resizeForRotate(densityVolume.rotate);
    densityVolume.bound.origin += densityVolume.position;
    densityVolume.objectGridData.active = false;
    densityVolume.setWasUpdate();

    sceneStructure->addEntityGE(densityVolume.typeEntity, &densityVolume);
}

void DensityVolume::onDestroy()
{
    sceneStructure->removeEntityGE(&densityVolume);
}

void DensityVolume::onEnable()
{
    densityVolume.objectGridData.active = true;
}

void DensityVolume::update()
{
    if (transform->getMaskChange() != 0)
    {
        densityVolume.position = transform->getPosition();
        densityVolume.rotate = transform->getQuaternion();
        densityVolume.scale = transform->getScale();

        densityVolume.bound = mth::boundAABB(mth::vec3(0, 0, 0), mth::vec3(1, 1, 1));
        densityVolume.bound.size.componentProductUpdate(densityVolume.scale);

        densityVolume.bound.resizeForRotate(densityVolume.rotate);
        densityVolume.bound.origin += densityVolume.position;

        sceneStructure->changeEntityOrientation(&densityVolume);
        densityVolume.setWasUpdate();
    }
}

void DensityVolume::onDisable()
{
    densityVolume.objectGridData.active = false;
}

void DensityVolume::setDensityVolumeType(DENSITY_VOLUME_TYPE _type)
{
    densityVolume.typeVolume = (DensityVolumeGE::DENSITY_VOLUME_GE_TYPE)_type;
    densityVolume.setWasUpdate();
}

DensityVolume::DENSITY_VOLUME_TYPE DensityVolume::getDensityVolumeType() const
{
    return (DensityVolume::DENSITY_VOLUME_TYPE)densityVolume.typeVolume;
}

void DensityVolume::setDensityVolumePlacementPlane(DENSITY_VOLUME_PLACEMENT_PLANE _placement)
{
    densityVolume.placementPlaneVolume = (DensityVolumeGE::DENSITY_VOLUME_GE_PLACEMENT_PLANE)_placement;
    densityVolume.setWasUpdate();
}

DensityVolume::DENSITY_VOLUME_PLACEMENT_PLANE DensityVolume::getDensityVolumePlacementPlane() const
{
    return (DensityVolume::DENSITY_VOLUME_PLACEMENT_PLANE)densityVolume.placementPlaneVolume;
}

void DensityVolume::setDensity(float _density)
{
    densityVolume.density = _density;
    densityVolume.setWasUpdate();
}

float DensityVolume::getDensity() const
{
    return densityVolume.density;
}

void DensityVolume::setColorVolume(mth::col _color)
{
    densityVolume.colorVolume = _color;
    densityVolume.setWasUpdate();
}

mth::col DensityVolume::getColorVolume() const
{
    return densityVolume.colorVolume;
}

void DensityVolume::setPowGradient(float _powGradient)
{
    densityVolume.powGradient = _powGradient;
    densityVolume.setWasUpdate();
}

float DensityVolume::getPowGradient() const
{
    return densityVolume.powGradient;
}

void DensityVolume::setPlacementPlaneGradient(DENSITY_VOLUME_PLACEMENT_PLANE _placement)
{
    densityVolume.placementPlaneGradient = (DensityVolumeGE::DENSITY_VOLUME_GE_PLACEMENT_PLANE)_placement;
    densityVolume.setWasUpdate();
}

DensityVolume::DENSITY_VOLUME_PLACEMENT_PLANE DensityVolume::getPlacementPlaneGradient() const
{
    return (DensityVolume::DENSITY_VOLUME_PLACEMENT_PLANE)densityVolume.placementPlaneGradient;
}

void DensityVolume::setStartDecay(float _startDecay)
{
    densityVolume.startDecay = _startDecay;
    densityVolume.setWasUpdate();
}

float DensityVolume::getStartDecay() const
{
    return densityVolume.startDecay;
}

void DensityVolume::setEndDecay(float _endDecay)
{
    densityVolume.endDecay = _endDecay;
    densityVolume.setWasUpdate();
}

float DensityVolume::getEndDecay() const
{
    return densityVolume.endDecay;
}

void DensityVolume::setEdgeFade(float _edgeFade)
{
    densityVolume.edgeFade = std::clamp(_edgeFade, 0.01f, 1.0f);
    densityVolume.setWasUpdate();
}

float DensityVolume::getEdgeFade() const
{
    return densityVolume.edgeFade;
}