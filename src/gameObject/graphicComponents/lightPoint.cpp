#include "gameObject/graphicComponents/lightPoint.h"
#include "gameObject/gameObject.h"

LightPoint::LightPoint(GraphicEngine *gEngine) : gEngine(gEngine), sceneStructure(gEngine->getSceneStructure()), isChangeRadius(false)
{
}

void LightPoint::update()
{
    debugRendererGE.clear();
    debugRendererGE.setMatrixFromTransform(*getTransform());
    debugRendererGE.setColor(pointLightGE.color);
    debugRendererGE.drawLine(mth::vec3(-1, 0, 0), mth::vec3(1, 0, 0));
    debugRendererGE.drawLine(mth::vec3(0, -1, 0), mth::vec3(0, 1, 0));
    debugRendererGE.drawLine(mth::vec3(0, 0, -1), mth::vec3(0, 0, 1));

    debugRendererGE.drawLine(mth::vec3(-0.7f, -0.7f, -0.7f), mth::vec3(0.7f, 0.7f, 0.7f));
    debugRendererGE.drawLine(mth::vec3(0.7f, -0.7f, 0.7f), mth::vec3(-0.7f, 0.7f, -0.7f));

    debugRendererGE.drawLine(mth::vec3(0.7f, -0.7f, -0.7f), mth::vec3(-0.7f, 0.7f, 0.7f));
    debugRendererGE.drawLine(mth::vec3(-0.7f, -0.7f, 0.7f), mth::vec3(0.7f, 0.7f, -0.7f));

    if (transform->isChangeMove() || isChangeRadius)
    {
        pointLightGE.position = transform->getPosition();
        sceneStructure->changeEntityOrientation(&pointLightGE);
    }
}

LightPoint::~LightPoint()
{
}

void LightPoint::onInit()
{
    pointLightGE.typeEntity = TypeGraphicEntity::POINT_LIGHT_GRAPHIC_ENTITY;
    pointLightGE.objectGridData.active = false;
    pointLightGE.range = 35;
    pointLightGE.position = transform->getPosition();
    pointLightGE.intensivity = 5;
    pointLightGE.createShadows = true;
    pointLightGE.color = mth::vec3(1, 1, 1);
    pointLightGE.shadowCubeFrameBuffer = NULL;

    debugRendererGE.typeEntity = TypeGraphicEntity::DEBUG_RENDER_GRAPHIC_ENTITY;

    sceneStructure->addEntityGE(TypeGraphicEntity::DEBUG_RENDER_GRAPHIC_ENTITY, &debugRendererGE);
    sceneStructure->addEntityGE(TypeGraphicEntity::POINT_LIGHT_GRAPHIC_ENTITY, &pointLightGE);
}

void LightPoint::onDestroy()
{
    sceneStructure->removeEntityGE(&pointLightGE);
    sceneStructure->removeEntityGE(&debugRendererGE);
}

void LightPoint::onEnable()
{
    pointLightGE.objectGridData.active = true;
}

void LightPoint::onDisable()
{
    pointLightGE.objectGridData.active = false;
}