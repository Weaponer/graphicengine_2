#include <algorithm>
#include <iterator>
#include <MTH/glGen_matrix.h>

#include "transform.h"
#include "gameObject/gameObject.h"

Transform::Transform(GameObject *_gameObject) : gameObject(_gameObject), parent(NULL), branch(-1), localMatrix(1, 0, 0, 0,
                                                                                                               0, 1, 0, 0,
                                                                                                               0, 0, 1, 0,
                                                                                                               0, 0, 0, 1),
                                                globalMatrix(1, 0, 0, 0,
                                                             0, 1, 0, 0,
                                                             0, 0, 1, 0,
                                                             0, 0, 0, 1),

                                                localPosition(0, 0, 0), localRotate(0, 0, 0, 1),
                                                position(0, 0, 0), rotate(0, 0, 0, 1), scale(1, 1, 1), localScale(1, 1, 1), maskChangeTransform(0)
{
    updateDatas();
}

GameObject *Transform::getGameObject() const
{
    return gameObject;
}

void Transform::setActive(bool _active)
{
    localActive = _active;
    bool oldGlobal = globalActive;
    if ((parent == NULL || parent->globalActive == true) && localActive == true)
    {
        globalActive = true;
    }
    else
    {
        globalActive = false;
    }

    if (oldGlobal != globalActive)
    {
        for (auto i : children)
        {
            (*i).setActive((*i).getLocalActive());
        }
    }
    gameObject->updateStatusActive();
}

bool Transform::getActive()
{
    return globalActive;
}

bool Transform::getLocalActive()
{
    return localActive;
}

void Transform::setScale(mth::vec3 _scale)
{
    localScale = _scale;
    maskChangeTransform = maskChangeTransform | (1 << 2);
    updateDatas();
}

mth::vec3 Transform::getScale() const
{
    return scale;
}

mth::vec3 Transform::getLocalScale() const
{
    return localScale;
}

void Transform::setLocalPosition(mth::vec3 _pos)
{
    localPosition = _pos;
    maskChangeTransform = maskChangeTransform | 1;
    updateDatas();
}

mth::vec3 Transform::getLocalPosition() const
{
    return localPosition;
}

void Transform::setPosition(mth::vec3 _pos)
{
    if (parent == NULL)
    {
        localPosition = _pos;
        updateDatas();
        maskChangeTransform = maskChangeTransform | 1;
    }
    else
    {
        mth::mat4 beforeMat = parent->globalMatrix;
        mth::mat4 inverseTRSG = mth::inverseTRS(beforeMat);
        mth::vec3 direct = inverseTRSG.transform(_pos);
        localPosition = direct;
        maskChangeTransform = maskChangeTransform | 1;
        updateDatas();
    }
}

mth::vec3 Transform::getPosition() const
{
    return position;
}

void Transform::setLocalRotationEuler(mth::vec3 _rot)
{
    mth::quat q;
    q.setEuler(_rot.x, _rot.y, _rot.z);
    maskChangeTransform = maskChangeTransform | (1 << 1);
    setLocalQuaternion(q);
}

void Transform::setLocalQuaternion(mth::quat _quaternion)
{
    localRotate = _quaternion;
    maskChangeTransform = maskChangeTransform | (1 << 1);
    updateDatas();
}

void Transform::setRotationEuler(mth::vec3 _rot)
{
    mth::quat q;
    q.setEuler(_rot.x, _rot.y, _rot.z);
    maskChangeTransform = maskChangeTransform | (1 << 1);
    setQuaternion(q);
}

void Transform::setQuaternion(mth::quat _quaternion)
{
    mth::quat curRotate = rotate;
    curRotate.inverse();
    mth::quat differentRot = curRotate;
    differentRot *= _quaternion;
    localRotate *= differentRot;
    maskChangeTransform = maskChangeTransform | (1 << 1);
    updateDatas();
}

mth::quat Transform::getQuaternion() const
{
    return rotate;
}

mth::quat Transform::getLocalQuaternion() const
{
    return localRotate;
}

void Transform::setBranch(int _number)
{
    branch = _number;
    for (auto i : children)
    {
        (*i).setBranch(_number);
    }
}

void Transform::setParent(Transform *_transform)
{
    if (parent == _transform)
    {
        return;
    }

    if (parent != NULL)
    {
        parent->children.remove(this);
    }
    parent = _transform;
    if (parent != NULL)
    {
        parent->children.push_back(this);
    }

    if (parent == NULL)
    {
        localPosition = position;
        localRotate = rotate;
    }
    else
    {
        mth::mat4 beforeMat = parent->globalMatrix;
        mth::mat4 inverseTRSG = mth::inverseTRS(beforeMat);
        mth::vec3 direct = inverseTRSG.transform(position);
        localPosition = direct;

        mth::quat prot = parent->rotate;
        prot.inverse();
        mth::quat currot = rotate;
        prot *= currot;
        localRotate = prot;
    }

    setActive(localActive);
}

int Transform::getBranch() const
{
    return branch;
}

Transform *Transform::getParent() const
{
    return parent;
}

int Transform::getCountChildren() const
{
    return children.size();
}

Transform *Transform::getChildren(int _index) const
{
    if (_index >= (int)children.size())
    {
        return NULL;
    }
    auto iter = children.begin();
    std::advance(iter, _index);
    return *iter;
}

const std::list<Transform *> *Transform::getListChildren() const
{
    const std::list<Transform *> *list = &children;
    return list;
}

mth::mat4 Transform::getLocalMatrix() const
{
    return localMatrix;
}

mth::mat4 Transform::getGlobalMatrix() const
{
    return globalMatrix;
}

void Transform::updateDatas()
{
    mth::mat4 basicMatrix;
    if (parent != NULL)
    {
        basicMatrix = parent->globalMatrix;
    }
    else
    {
        basicMatrix.setSingleMatrix();
    }

    localMatrix = mth::TRS(localPosition, localRotate, localScale);
    globalMatrix = basicMatrix * localMatrix;
    rotate = mth::getRotateInTRS(globalMatrix);
    position = mth::getPositionInTRS(globalMatrix);
    scale = mth::getScaleInTRS(globalMatrix);

    for (auto i : children)
    {
        (*i).updateDatas();
    }
}

bool Transform::isChangeMove() const
{
    return (maskChangeTransform & 1) != 0;
}

bool Transform::isChangeRotation() const
{
    return (maskChangeTransform & (1 << 1)) != 0;
}

bool Transform::isChangeScale() const
{
    return (maskChangeTransform & (1 << 2)) != 0;
}

void Transform::resetMaskChange()
{
    maskChangeTransform = 0x00;
}