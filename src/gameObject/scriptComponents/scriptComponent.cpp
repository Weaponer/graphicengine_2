#include "gameObject/scriptComponents/scriptComponent.h"

ScriptComponent::ScriptComponent(ScriptEngine *_scriptEngine) : scriptEngine(_scriptEngine), osd(NULL)
{
}

ScriptComponent::~ScriptComponent()
{
    if (osd != NULL)
    {
        onDestroy();
    }
}

void ScriptComponent::setType(const char *type)
{
    if (osd != NULL)
    {
        scriptEngine->removeObjectScriptData(osd);
    }
    osd = scriptEngine->newObjectScriptData(type, this);
    osd->setEnable(getActive());
}

void ScriptComponent::onInit()
{
}

void ScriptComponent::onDestroy()
{
    if (osd != NULL)
    {
        scriptEngine->removeObjectScriptData(osd);
        osd = NULL;
    }
}

void ScriptComponent::onEnable()
{
    if(osd != NULL)
    {
        osd->setEnable(true);
        scriptEngine->callMethod(osd, ON_ENABLE_METHOD_NAME);
    }
}

void ScriptComponent::onDisable()
{
    if(osd != NULL)
    {
        osd->setEnable(false);
        scriptEngine->callMethod(osd, ON_DISABLE_METHOD_NAME);
    }
}