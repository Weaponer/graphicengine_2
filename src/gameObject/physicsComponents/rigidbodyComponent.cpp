#include "gameObject/physicsComponents/rigidbodyComponent.h"
#include "gameObject/gameObject.h"

Rigidbody::Rigidbody(PhysicsEngine *engine) : engine(engine), actor(NULL)
{
}

Rigidbody::~Rigidbody()
{
    if (actor != NULL)
    {
        onDestroy();
    }
}

void Rigidbody::update()
{
    Transform *tr = gameObject->getTransform();
    if (tr->isChangeMove() == false)
    {
        tr->setPosition(actor->getPosition());
    }
    else
    {
        actor->setPosition(tr->getPosition());
    }
    if (tr->isChangeRotation() == false)
    {
        tr->setQuaternion(actor->getOrientation());
    }
    else
    {
        actor->setOrientation(tr->getQuaternion());
    }
}

void Rigidbody::connectCollider(ColliderComponent *collider)
{
    actor->colliderConstruct.connectCollider(collider->getColliderData());
}

void Rigidbody::disconnectCollider(ColliderComponent *collider)
{
    actor->colliderConstruct.removeCollider(collider->getColliderData());
}

void Rigidbody::updateOnNewLayer(Layer layer)
{
    actor->layer = layer;
}

void Rigidbody::onInit()
{
    actor = engine->createActor();
    actor->setPosition(gameObject->getTransform()->getPosition());
    actor->setOrientation(gameObject->getTransform()->getQuaternion());
    actor->layer = gameObject->getLayer();
}

void Rigidbody::onDestroy()
{
    engine->removeActor(actor);
    actor = NULL;
}

void Rigidbody::onEnable()
{
    actor->enable = true;
}

void Rigidbody::onDisable()
{
    actor->enable = false;
}