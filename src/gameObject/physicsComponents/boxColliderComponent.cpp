#include <MTH/glGen_matrix.h>

#include "gameObject/physicsComponents/boxColliderComponent.h"
#include "gameObject/gameObject.h"

BoxColliderComponent::BoxColliderComponent(ThreadDebugOutput *mainDebugOutput, PhysicsEngine *phsyics)
    : ColliderComponent(phsyics), mainDebugOutput(mainDebugOutput), size(1, 1, 1)
{
}

BoxColliderComponent::~BoxColliderComponent()
{
    if (collider != NULL)
    {
        onDestroy();
    }
}

void BoxColliderComponent::setSize(const mth::vec3 &size)
{
    this->size = size;
    Transform *tr = gameObject->getTransform();
    mth::vec3 scale = tr->getScale();
    scale.componentProductUpdate(size);
    ((BoxCollider *)collider)->setSize(scale);
}

void BoxColliderComponent::setPosition(const mth::vec3 &pos)
{
    origin = pos;
    Transform *tr = gameObject->getTransform();
    mth::vec3 scale = tr->getScale();
    scale.componentProductUpdate(origin);
    ((BoxCollider *)collider)->setOrigin(scale);
}

void BoxColliderComponent::update()
{
    Transform *tr = gameObject->getTransform();
    if ((tr->getMaskChange() & (1 << 1)) != 0)
    {
        setPosition(origin);
        setSize(size);
    }

    mth::quat q = tr->getQuaternion();
    mth::vec3 pos = tr->getPosition();
    mth::vec3 scale = tr->getScale();

    mth::mat4 mxRot = mth::translate(pos.x, pos.y, pos.z) * q.getMatrix() * mth::scale(scale.x, scale.y, scale.z);

    mainDebugOutput->drawCube(origin, size, mxRot, mth::vec3(0, 0, 1), 0);
}

void BoxColliderComponent::onInit()
{
    collider = new BoxCollider();
    phsyics->addCollider(collider);
    setPosition(origin);
    setSize(size);
}

void BoxColliderComponent::onDestroy()
{
    phsyics->removeCollider(collider);
    delete collider;
    collider = NULL;
}