#include "gameObject/physicsComponents/sphereColliderComponent.h"
#include "gameObject/gameObject.h"

SphereColliderComponent::SphereColliderComponent(ThreadDebugOutput *mainDebugOutput, PhysicsEngine *phsyics)
    : ColliderComponent(phsyics), mainDebugOutput(mainDebugOutput), radius(1)
{
}

SphereColliderComponent::~SphereColliderComponent()
{
    if(collider != NULL)
    {
        onDestroy();
    }
}

void SphereColliderComponent::setRadius(float radius)
{
    this->radius = radius;
    Transform *tr = gameObject->getTransform();
    mth::vec3 scale = tr->getScale();
    ((SphereCollider *)collider)->setRadius(radius * std::max(scale.x, std::max(scale.y, scale.z)));
}

void SphereColliderComponent::setPosition(const mth::vec3 &pos)
{
    origin = pos;
    Transform *tr = gameObject->getTransform();
    mth::vec3 scale = tr->getScale();
    scale.componentProductUpdate(origin);
    ((SphereCollider *)collider)->setOrigin(scale);
}

void SphereColliderComponent::update()
{
    Transform *tr = gameObject->getTransform();
    if ((tr->getMaskChange() & (1 << 1)) != 0)
    {
        setPosition(origin);
        setRadius(radius);
    }
    /*debug->clear();
    debug->setColor(mth::vec3(0, 0, 1));
    debug->setMatrixFromTransform(*(object->getTransform()));
    debug->drawLine(mth::vec3(getRadius(), 0, 0), mth::vec3(getRadius() * -1, 0, 0));
    debug->drawLine(mth::vec3(0, getRadius(), 0), mth::vec3(0, getRadius() * -1, 0));
    debug->drawLine(mth::vec3(0, 0, getRadius()), mth::vec3(0, 0, getRadius() * -1));*/
}

void SphereColliderComponent::onInit()
{
    collider = new SphereCollider();
    phsyics->addCollider(collider);
    setPosition(origin);
    setRadius(radius);
}

void SphereColliderComponent::onDestroy()
{
    phsyics->removeCollider(collider);
    delete collider;
    collider = NULL;
}