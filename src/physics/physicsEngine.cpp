#include "physicsEngine.h"

PhysicsEngine::PhysicsEngine(LayerManager *layerManager, ThreadDebugOutput *debugOutput) : collisionsSystem(layerManager, debugOutput), contactResolver(debugOutput), layerManager(layerManager), debugOutput(debugOutput)
{
}

PhysicsEngine::~PhysicsEngine()
{
    if (actors.size() != 0)
    {
        std::for_each(actors.begin(), actors.end(), [](RigidbodyActor *actor)
                      { delete actor; });
    }
}

void PhysicsEngine::update(float duration)
{
    auto end = actors.end();
    for (auto i = actors.begin(); i != end; ++i)
    {
        if((*i)->enable != false)
        {
            (*i)->update(duration);
        }
    }
    collisionsSystem.update();
    const EasyStack<Contact> *contacts = collisionsSystem.getContacts();

    contactResolver.resolveContacts(contacts, &actors, duration);
}

RigidbodyActor *PhysicsEngine::createActor()
{
    RigidbodyActor *actor = new RigidbodyActor(this, &collisionsSystem);
    actor->enable = false;
    actors.push_back(actor);
    return actor;
}

void PhysicsEngine::removeActor(RigidbodyActor *actor)
{
    auto end = actors.end();
    auto found = std::find(actors.begin(), end, actor);
    if (found != end)
    {
        actors.erase(found);
        delete actor;
    }
}

void PhysicsEngine::setKinematic(RigidbodyActor *actor, bool _b)
{
    if (_b == true)
    {
        if (actor->kinematic != _b)
        {
            std::list<Collider *> *colls = actor->colliderConstruct.getListColliders();
            for (auto coll : *colls)
            {
                collisionsSystem.setKinematicCollider(coll);
            }
            actor->kinematic = _b;
        }
    }
    else
    {
        if (actor->kinematic != _b)
        {
            std::list<Collider *> *colls = actor->colliderConstruct.getListColliders();
            for (auto coll : *colls)
            {
                collisionsSystem.setDynamicCollider(coll);
            }
            actor->kinematic = _b;
        }
    }
}