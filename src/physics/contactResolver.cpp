#include "contactResolver.h"
#include "rigidbodyActor.h"
#include "transform.h"

ContactResolver::ContactResolver(ThreadDebugOutput *debugOutput) : debugOutput(debugOutput)
{
    velocityIterationsUsed = 0;
    positionIterationsUsed = 0;
    velocityIterations = 3;
    positionIterations = 3;

    velocityEpsilon = 0.001f;
    positionEpsilon = 0.001f;
}

ContactResolver::~ContactResolver()
{
}

void ContactResolver::resolveContacts(const EasyStack<Contact> *contacts, std::list<RigidbodyActor *> *actors, float duration)
{
    auto end = actors->end();
    for (auto i = actors->begin(); i != end; ++i)
    {
        (*i)->contactData.clearContacts();
    }

    pushContactsToActors(contacts);
    Contact *startC = contacts->getMemory(0);
    prepareContacts(startC, contacts->getPos(), duration);
    // debug

    for (int i = 0; i < (int)contacts->getPos(); i++)
    {
        Contact *contact = contacts->getMemory(i);

        debugOutput->drawLine(contact->point, contact->point + contact->basis.getVector(0) * contact->penetration, mth::vec3(1, 0, 0));
        debugOutput->drawLine(contact->point, contact->point + contact->basis.getVector(1), mth::vec3(0, 1, 0));
        debugOutput->drawLine(contact->point, contact->point + contact->basis.getVector(2), mth::vec3(0, 0, 1));
    }

    for (auto i = actors->begin(); i != end; ++i)
    {
        Contact *cts = ((*i)->contactData.getContact(0));
        adjustPositions(cts, (*i)->contactData.getCountContacts(), duration);
        adjustVelocities(cts, (*i)->contactData.getCountContacts(), duration);
    }
}

void ContactResolver::swapContact(Contact *contact)
{
    RigidbodyActor *temp = contact->rigidbodies[0];
    contact->rigidbodies[0] = contact->rigidbodies[1];
    contact->rigidbodies[1] = temp;
    contact->normal.invert();
}

void ContactResolver::pushContactsToActors(const EasyStack<Contact> *contacts)
{
    int size = contacts->getPos();
    for (int i = 0; i < size; i++)
    {

        Contact *contact = contacts->getMemory(i);
        if (contact->rigidbodies[0]->kinematic == true)
        {
            swapContact(contact);
        }

        contact->rigidbodies[0]->contactData.addContact(contact);
        if (contact->rigidbodies[1]->kinematic == false)
        {
            contact->rigidbodies[1]->contactData.addContact(contact);
        }
    }
}

void ContactResolver::prepareContacts(Contact *contacts, int numContacts, float duration)
{
    for (int i = 0; i < numContacts; i++)
    {
        Contact *contact = contacts + i;

        //bool isK = contact->rigidbodies[0]->kinematic;
        calculateInternals(contact, duration);
    }
}

void ContactResolver::adjustPositions(Contact *contacts, int numContacts, float duration)
{
    unsigned i = 0;
    unsigned index = 0;
    mth::vec3 linearChange[2];
    mth::vec3 angularChange[2];
    float max = 0;
    mth::vec3 deltaPosition;

    positionIterationsUsed = 0;
    while (positionIterationsUsed < positionIterations)
    {
        max = positionEpsilon;
        index = numContacts;

        for (i = 0; i < (unsigned int)numContacts; i++)
        {
            Contact *contactI = contacts + i;
            if (contactI->penetration > max)
            {
                max = contactI->penetration;
                index = i;
            }
        }

        if (index == (unsigned int)numContacts)
            break;

        Contact *contactI = contacts + index;
        applyPositionChange(contactI, linearChange, angularChange, max);

        for (i = 0; i < (unsigned int)numContacts; i++)
        {
            Contact *cI = contacts + i;
            for (unsigned b = 0; b < 2; b++)
                if (contactI->rigidbodies[b]->kinematic == false)
                {
                    for (unsigned d = 0; d < 2; d++)
                    {
                        if (cI->rigidbodies[b] == contactI->rigidbodies[d])
                        {
                            deltaPosition = linearChange[d] + angularChange[d].vectorProduct(cI->relativeContactPosition[b]);

                            cI->penetration += deltaPosition.scalarProduct(cI->normal) * (b ? 1 : -1);
                        }
                    }
                }
        }

        if (contactI->rigidbodies[1]->kinematic == false)
        {
            RigidbodyActor *rigTwo = contactI->rigidbodies[1];
            //Contact *contatsRigTwo = rigTwo->contactData.getContact(0);
            int numContactsRigTwo = rigTwo->contactData.getCountContacts();

            for (i = 0; i < (unsigned int)numContactsRigTwo; i++)
            {
                Contact *cI = contacts + i;
                if (cI == contactI)
                    continue;

                for (unsigned b = 0; b < 2; b++)
                {
                    if (cI->rigidbodies[b] == rigTwo)
                    {
                        deltaPosition = linearChange[1] + angularChange[1].vectorProduct(cI->relativeContactPosition[b]);

                        cI->penetration += deltaPosition.scalarProduct(cI->normal) * (b ? 1 : -1);
                    }
                }
            }
        }

        positionIterationsUsed++;
    }
}

void ContactResolver::adjustVelocities(Contact *contacts, int numContacts, float duration)
{
    mth::vec3 velocityChange[2], rotationChange[2];
    mth::vec3 deltaVel;

    velocityIterationsUsed = 0;

    while (velocityIterationsUsed < velocityIterations)
    {

        float max = velocityEpsilon;
        unsigned index = numContacts;
        for (int i = 0; i < numContacts; i++)
        {
            Contact *contact = contacts + i;
            if (contact->desiredDeltaVelocity > max)
            {
                max = contact->desiredDeltaVelocity;
                index = i;
            }
        }

        if (index == (unsigned int)numContacts)
            break;
        Contact *contactI = contacts + index;
        applyVelocityChange(contactI, velocityChange, rotationChange);

        for (int i = 0; i < numContacts; i++)
        {
            Contact *cI = contacts + i;
            for (unsigned b = 0; b < 2; b++)
            {
                if (cI->rigidbodies[b]->kinematic == false)
                {
                    for (unsigned d = 0; d < 2; d++)
                    {
                        if (cI->rigidbodies[b] == contactI->rigidbodies[d])
                        {
                            deltaVel = velocityChange[d] + rotationChange[d].vectorProduct(cI->relativeContactPosition[b]);

                            cI->contactVelocity += cI->basis.transformTranspose(deltaVel) * (b ? -1 : 1);
                            calculateDesiredDeltaVelocity(cI, duration);
                        }
                    }
                }
            }
        }

        if (contactI->rigidbodies[1]->kinematic == false)
        {
            RigidbodyActor *rigTwo = contactI->rigidbodies[1];
            //Contact *contatsRigTwo = rigTwo->contactData.getContact(0);
            int numContactsRigTwo = rigTwo->contactData.getCountContacts();

            for (int i = 0; i < numContactsRigTwo; i++)
            {
                Contact *cI = contacts + i;
                if (cI == contactI)
                    continue;

                for (unsigned b = 0; b < 2; b++)
                {
                    if (cI->rigidbodies[b] == rigTwo)
                    {
                        deltaVel = velocityChange[1] + rotationChange[1].vectorProduct(cI->relativeContactPosition[b]);

                        cI->contactVelocity += cI->basis.transformTranspose(deltaVel) * (b ? -1 : 1);
                        calculateDesiredDeltaVelocity(cI, duration);
                    }
                }
            }
        }
        velocityIterationsUsed++;
    }
}

////Contact methods

void ContactResolver::calculateInternals(Contact *contact, float duration)
{
    calculateContactBasis(contact);

    contact->relativeContactPosition[0] = contact->point - contact->rigidbodies[0]->getPosition();
    contact->relativeContactPosition[1] = contact->point - contact->rigidbodies[1]->getPosition();

    contact->contactVelocity = calculateLocalVelocity(contact, 0, duration);
    if (contact->rigidbodies[1]->kinematic == false)
    {
        contact->contactVelocity -= calculateLocalVelocity(contact, 1, duration);
    }

    calculateDesiredDeltaVelocity(contact, duration);
}

void ContactResolver::calculateContactBasis(Contact *contact)
{
    mth::vec3 contactTangent[2];

    if (abs(contact->normal.x) > abs(contact->normal.y))
    {
        const float s = (float)1.0f / abs(contact->normal.z * contact->normal.z +
                                          contact->normal.x * contact->normal.x);

        contactTangent[0].x = contact->normal.z * s;
        contactTangent[0].y = 0;
        contactTangent[0].z = -contact->normal.x * s;

        contactTangent[1].x = contact->normal.y * contactTangent[0].x;
        contactTangent[1].y = contact->normal.z * contactTangent[0].x -
                              contact->normal.x * contactTangent[0].z;
        contactTangent[1].z = -contact->normal.y * contactTangent[0].x;
    }
    else
    {
        const float s = (float)1.0f / abs(contact->normal.z * contact->normal.z +
                                          contact->normal.y * contact->normal.y);

        contactTangent[0].x = 0;
        contactTangent[0].y = -contact->normal.z * s;
        contactTangent[0].z = contact->normal.y * s;

        contactTangent[1].x = contact->normal.y * contactTangent[0].z -
                              contact->normal.z * contactTangent[0].y;
        contactTangent[1].y = -contact->normal.x * contactTangent[0].z;
        contactTangent[1].z = contact->normal.x * contactTangent[0].y;
    }

    contact->basis.setOrientation(
        contact->normal,
        contactTangent[0],
        contactTangent[1]);
}

mth::vec3 ContactResolver::calculateLocalVelocity(Contact *contact, unsigned bodyIndex, float duration)
{
    RigidbodyActor *thisBody = contact->rigidbodies[bodyIndex];
    mth::vec3 velocity = thisBody->angularVelocity % contact->relativeContactPosition[bodyIndex] + thisBody->velocity;

    mth::vec3 contactVelocity = contact->basis.transformTranspose(velocity);

    mth::vec3 accVelocity = contact->rigidbodies[bodyIndex]->getLastFrameAcceleration() * duration;

    accVelocity = contact->basis.transformTranspose(accVelocity);
    accVelocity.x = 0;
    contactVelocity += accVelocity;

    return contactVelocity;
}

void ContactResolver::calculateDesiredDeltaVelocity(Contact *contact, float duration)
{
    const static float velocityLimit = 0.25f;

    float velocityFromAcc = 0;

    velocityFromAcc += contact->rigidbodies[0]->getLastFrameAcceleration() * duration * contact->normal;

    if (contact->rigidbodies[1]->kinematic == false)
    {
        velocityFromAcc -= contact->rigidbodies[1]->getLastFrameAcceleration() * duration * contact->normal;
    }

    float thisRestitution = 0.4f;
    if (abs(contact->contactVelocity.x) < velocityLimit)
    {
        thisRestitution = 0.0f;
    }

    contact->desiredDeltaVelocity = (-contact->contactVelocity.x) - thisRestitution * (contact->contactVelocity.x - velocityFromAcc);
}

void ContactResolver::applyPositionChange(Contact *contact, mth::vec3 linearChange[2], mth::vec3 angularChange[2], float penetration)
{
    const float angularLimit = 0.002f;
    float angularMove[2];
    float linearMove[2];

    float totalInertial = 0;
    float linearInertial[2];
    float angularInertial[2];

    for (unsigned i = 0; i < 2; i++)
    {
        if (contact->rigidbodies[i]->kinematic == false)
        {
            mth::mat3 inverseInertialTensor = contact->rigidbodies[i]->inverseInertiaTensorWorld;

            mth::vec3 angularInetrialWorld = contact->relativeContactPosition[i] % contact->normal;
            angularInetrialWorld = inverseInertialTensor.transform(angularInetrialWorld);
            angularInetrialWorld = angularInetrialWorld % contact->relativeContactPosition[i];
            angularInertial[i] = angularInetrialWorld * contact->normal;

            linearInertial[i] = contact->rigidbodies[i]->inverseMass;
            totalInertial += linearInertial[i] + angularInertial[i];
        }
    }

    float inverseInertial = 1 / totalInertial;

    for (int i = 0; i < 2; i++)
    {
        if (contact->rigidbodies[i]->kinematic == false)
        {
            float sign = (i == 0) ? 1 : -1;

            linearMove[i] = sign * penetration * linearInertial[i] * inverseInertial;
            angularMove[i] = sign * penetration * angularInertial[i] * inverseInertial;

            mth::vec3 projection = contact->relativeContactPosition[i];
            projection.addScaledVector(contact->normal, -contact->relativeContactPosition[i].scalarProduct(contact->normal));

            float maxMagnitude = angularLimit * projection.magnitude();
            if (angularMove[i] < -maxMagnitude)
            {
                float totalMove = angularMove[i] + linearMove[i];
                angularMove[i] = -maxMagnitude;
                linearMove[i] = totalMove - angularMove[i];
            }
            else if (angularMove[i] > maxMagnitude)
            {
                float totalMove = angularMove[i] + linearMove[i];
                angularMove[i] = maxMagnitude;
                linearMove[i] = totalMove - angularMove[i];
            }

            if (angularMove[i] == 0)
            {
                angularChange[i].clear();
            }
            else
            {
                mth::vec3 targetAngularDirection = contact->relativeContactPosition[i].vectorProduct(contact->normal);

                mth::mat3 inverseInertialTensor = contact->rigidbodies[i]->inverseInertiaTensorWorld;
                angularChange[i] = inverseInertialTensor.transform(targetAngularDirection) * (angularMove[i] / angularInertial[i]);
            }

            linearChange[i] = contact->normal * linearMove[i];

            contact->rigidbodies[i]->setPosition(contact->rigidbodies[i]->getPosition() + contact->normal * linearMove[i]);

            mth::quat q = contact->rigidbodies[i]->getOrientation();
            q.addScaledVector(angularChange[i], -1.0f);
            q.normalise();
            contact->rigidbodies[i]->setOrientation(q);
            contact->rigidbodies[i]->calculateGlobalInverseInertialTensor();
        }
    }
}

void ContactResolver::applyVelocityChange(Contact *contact, mth::vec3 velocityChange[2], mth::vec3 rotationChange[2])
{
    mth::mat3 inverseInertialTensor[2];
    inverseInertialTensor[0] = contact->rigidbodies[0]->inverseInertiaTensorWorld;
    if (contact->rigidbodies[1]->kinematic == false)
        inverseInertialTensor[1] = contact->rigidbodies[1]->inverseInertiaTensorWorld;

    mth::vec3 impulseContact = calculateFrictionImpulse(contact, inverseInertialTensor);

    mth::vec3 impulse = contact->basis.transform(impulseContact);

    debugOutput->drawLine(contact->point, contact->point + impulse, mth::vec3(1, 1, 0));

    mth::vec3 impulsiveTorque = contact->relativeContactPosition[0] % impulse;
    rotationChange[0] = inverseInertialTensor[0].transform(impulsiveTorque);
    velocityChange[0].clear();
    velocityChange[0].addScaledVector(impulse, contact->rigidbodies[0]->inverseMass);
    contact->rigidbodies[0]->velocity += velocityChange[0];
    contact->rigidbodies[0]->angularVelocity += rotationChange[0];

    if (contact->rigidbodies[1]->kinematic == false)
    {
        mth::vec3 impulsiveTorque = impulse % contact->relativeContactPosition[1];
        rotationChange[1] = inverseInertialTensor[1].transform(impulsiveTorque);
        velocityChange[1].clear();
        velocityChange[1].addScaledVector(impulse, -contact->rigidbodies[1]->inverseMass);

        contact->rigidbodies[1]->velocity += velocityChange[1];
        contact->rigidbodies[1]->angularVelocity += rotationChange[1];
    }
}

mth::vec3 ContactResolver::calculateFrictionlessImpulse(Contact *contact, const mth::mat3 *inverseInertialTensor)
{
    mth::vec3 impulseContact;

    mth::vec3 deltaVelWorld = contact->relativeContactPosition[0] % contact->normal;

    deltaVelWorld = inverseInertialTensor[0].transform(deltaVelWorld);
    deltaVelWorld = deltaVelWorld % contact->relativeContactPosition[0];

    float deltaVelocity = deltaVelWorld * contact->normal;

    deltaVelocity += contact->rigidbodies[0]->inverseMass;

    if (contact->rigidbodies[1]->kinematic == false)
    {
        deltaVelWorld = contact->relativeContactPosition[1] % contact->normal;
        deltaVelWorld = inverseInertialTensor[1].transform(deltaVelWorld);
        deltaVelWorld = deltaVelWorld % contact->relativeContactPosition[1];

        deltaVelocity += deltaVelWorld * contact->normal;

        deltaVelocity += contact->rigidbodies[1]->inverseMass;
    }

    impulseContact.x = contact->desiredDeltaVelocity / deltaVelocity;
    impulseContact.y = 0;
    impulseContact.z = 0;
    return impulseContact;
}

mth::vec3 ContactResolver::calculateFrictionImpulse(Contact *contact, const mth::mat3 *inverseInertialTensor)
{
    mth::vec3 impulseContact;
    float inverseMass = contact->rigidbodies[0]->inverseMass;

    mth::vec3 rlCP = contact->relativeContactPosition[0];
    mth::mat3 impulseToTorque = mth::mat3(0, -rlCP.z, rlCP.y,
                                          rlCP.z, 0, -rlCP.x,
                                          -rlCP.y, rlCP.x, 0);

    mth::mat3 deltaVelWorld = impulseToTorque;
    deltaVelWorld *= inverseInertialTensor[0];
    deltaVelWorld *= impulseToTorque;
    deltaVelWorld *= -1;

    if (contact->rigidbodies[1]->kinematic == false)
    {
        rlCP = contact->relativeContactPosition[1];
        impulseToTorque = mth::mat3(0, -rlCP.z, rlCP.y,
                                    rlCP.z, 0, -rlCP.x,
                                    -rlCP.y, rlCP.x, 0);

        mth::mat3 deltaVelWorld2 = impulseToTorque;
        deltaVelWorld2 *= inverseInertialTensor[1];
        deltaVelWorld2 *= impulseToTorque;
        deltaVelWorld2 *= -1;

        deltaVelWorld += deltaVelWorld2;

        inverseMass += contact->rigidbodies[1]->inverseMass;
    }

    mth::mat3 deltaVelocity = contact->basis;
    deltaVelocity.transpose();
    deltaVelocity *= deltaVelWorld;
    deltaVelocity *= contact->basis;

    deltaVelocity.m[0][0] += inverseMass;
    deltaVelocity.m[1][1] += inverseMass;
    deltaVelocity.m[2][2] += inverseMass;

    mth::mat3 impulseMatrix = deltaVelocity;
    impulseMatrix.inverse();

    mth::vec3 velKill(contact->desiredDeltaVelocity, -contact->contactVelocity.y, -contact->contactVelocity.z);

    impulseContact = impulseMatrix.transform(velKill);

    float planarImpulse = sqrt(impulseContact.y * impulseContact.y + impulseContact.z * impulseContact.z);
    float friction = 0.7;
    if (planarImpulse > impulseContact.x * friction)
    {
        impulseContact.y /= planarImpulse;
        impulseContact.z /= planarImpulse;

        impulseContact.x = deltaVelocity.m[0][0] +
                           deltaVelocity.m[0][1] * friction * impulseContact.y +
                           deltaVelocity.m[0][2] * friction * impulseContact.z;

        impulseContact.x = contact->desiredDeltaVelocity / impulseContact.x;
        impulseContact.y *= friction * impulseContact.x;
        impulseContact.z *= friction * impulseContact.x;
    }
    return impulseContact;
}