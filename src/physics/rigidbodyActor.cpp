#include "rigidbodyActor.h"
#include "mathForces/gravitation.h"

RigidbodyActor::RigidbodyActor(PhysicsEngine *engine, CollisionsSystem *collisionsSystem) : velocity(), angularVelocity(),
                                                                                            acceleration(), angularAcceleration(),
                                                                                            engine(engine), colliderConstruct(this, collisionsSystem)
{
    kinematic = false;
    setMass(1);
    drag = 0.005f;
}

RigidbodyActor::~RigidbodyActor()
{
}

void RigidbodyActor::update(float duration)
{
    if (kinematic == true)
        return;

    if (checkChangeInverseInertialTensor())
        calculateInverseInertialTensor();

    calculateGlobalInverseInertialTensor();

    //velocity
    acceleration += DIRECTION_GRAVITATION * getFroceGravitation(mass);

    float vel = velocity.magnitude();
    mth::vec3 normalVel = velocity;
    normalVel.normalise();
    normalVel.invert();
    acceleration += normalVel * (drag * vel + drag * vel * vel);

    lastFrameAcceleration = acceleration;
    lastFrameAcceleration *= inverseMass;

    velocity += lastFrameAcceleration * duration;

    //angular velocity
    angularAcceleration = inverseInertiaTensorWorld.transform(angularAcceleration);
    angularVelocity.addScaledVector(angularAcceleration, duration);

    //move
    position.addScaledVector(velocity, duration);

    //mth::mat4 rotM = orientation.getMatrix();
    //mth::vec3 rotV = rotM.transform(angularVelocity);

    orientation.addScaledVector(angularVelocity, duration);
    orientation.normalise();

    //clear
    acceleration.clear();
    angularAcceleration.clear();
}

void RigidbodyActor::setMass(float _mass)
{
    mass = _mass;
    if (mass <= 0)
    {
        mass = 0;
        inverseMass = 0;
    }
    else
    {
        inverseMass = 1.0f / mass;
    }
}

void RigidbodyActor::addForce(const mth::vec3 &force)
{
    acceleration += force;
}

void RigidbodyActor::addForceAtPoint(const mth::vec3 &force, const mth::vec3 &point)
{
    mth::vec3 p = point;
    p -= position;

    acceleration += force;
    angularAcceleration += p % force;
}

void RigidbodyActor::addTorque(const mth::vec3 &torque)
{
    angularAcceleration += torque;
}

bool RigidbodyActor::checkChangeInverseInertialTensor()
{
    return true;
}

void RigidbodyActor::calculateInverseInertialTensor()
{
    inverseInertiaTensor = mth::mat3();
    inverseInertiaTensor.setScale(inverseMass);
    //inverseInertiaTensor.inverse();
    /*inverseInertiaTensor.m[0][0] = inverseMass * 2;
    inverseInertiaTensor.m[1][1] = inverseMass * 2.5f;
    inverseInertiaTensor.m[2][2] = inverseMass * 3;*/
}

void RigidbodyActor::calculateGlobalInverseInertialTensor()
{
    mth::mat4 rot = orientation.getMatrix();

    mth::mat3 r = mth::matrix3FromMatrix4(rot);
    inverseInertiaTensorWorld = r * inverseInertiaTensor;
    r.transpose();
    inverseInertiaTensorWorld *= r;

    /*float t4 = rot.m[0][0] * inverseInertiaTensor.m[0][0] + rot.m[0][1] * inverseInertiaTensor.m[1][0] + rot.m[0][2] * inverseInertiaTensor.m[2][0];
    float t9 = rot.m[0][0] * inverseInertiaTensor.m[0][1] + rot.m[0][1] * inverseInertiaTensor.m[1][1] + rot.m[0][2] * inverseInertiaTensor.m[2][1];
    float t14 = rot.m[0][0] * inverseInertiaTensor.m[0][2] + rot.m[0][1] * inverseInertiaTensor.m[1][2] + rot.m[0][2] * inverseInertiaTensor.m[2][2];
    float t28 = rot.m[1][0] * inverseInertiaTensor.m[0][0] + rot.m[1][1] * inverseInertiaTensor.m[1][0] + rot.m[1][2] * inverseInertiaTensor.m[2][0];
    float t33 = rot.m[1][0] * inverseInertiaTensor.m[0][1] + rot.m[1][1] * inverseInertiaTensor.m[1][1] + rot.m[1][2] * inverseInertiaTensor.m[2][1];
    float t38 = rot.m[1][0] * inverseInertiaTensor.m[0][2] + rot.m[1][1] * inverseInertiaTensor.m[1][2] + rot.m[1][2] * inverseInertiaTensor.m[2][2];
    float t52 = rot.m[2][0] * inverseInertiaTensor.m[0][0] + rot.m[2][1] * inverseInertiaTensor.m[1][0] + rot.m[2][2] * inverseInertiaTensor.m[2][0];
    float t57 = rot.m[2][0] * inverseInertiaTensor.m[0][1] + rot.m[2][1] * inverseInertiaTensor.m[1][1] + rot.m[2][2] * inverseInertiaTensor.m[2][1];
    float t62 = rot.m[2][0] * inverseInertiaTensor.m[0][2] + rot.m[2][1] * inverseInertiaTensor.m[1][2] + rot.m[2][2] * inverseInertiaTensor.m[2][2];

    inverseInertiaTensorWorld.m[0][0] = t4 * rot.m[0][0] + t9*rot.m[0][1] + t14*rot.m[0][2];
    inverseInertiaTensorWorld.m[0][1] = t4 * rot.m[1][0] + t9*rot.m[1][1] + t14*rot.m[1][2];
    inverseInertiaTensorWorld.m[0][2] = t4 * rot.m[2][0] + t9*rot.m[2][1] + t14*rot.m[2][2];
    inverseInertiaTensorWorld.m[1][0] = t28 * rot.m[0][0] + t33*rot.m[0][1] + t38*rot.m[0][2];
    inverseInertiaTensorWorld.m[1][1] = t28 * rot.m[1][0] + t33*rot.m[1][1] + t38*rot.m[1][2];
    inverseInertiaTensorWorld.m[1][2] = t28 * rot.m[2][0] + t33*rot.m[2][1] + t38*rot.m[2][2];
    inverseInertiaTensorWorld.m[2][0] = t52 * rot.m[0][0] + t57*rot.m[0][1] + t62*rot.m[0][2];
    inverseInertiaTensorWorld.m[2][1] = t52 * rot.m[1][0] + t57*rot.m[1][1] + t62*rot.m[1][2];
    inverseInertiaTensorWorld.m[2][2] = t52 * rot.m[2][0] + t57*rot.m[2][1] + t62*rot.m[2][2];*/

    /*printf("-\n");
    printf("%f %f %f\n", inverseInertiaTensorWorld.m[0][0], inverseInertiaTensorWorld.m[0][1], inverseInertiaTensorWorld.m[0][2]);
    printf("%f %f %f\n", inverseInertiaTensorWorld.m[1][0], inverseInertiaTensorWorld.m[1][1], inverseInertiaTensorWorld.m[1][2]);
    printf("%f %f %f\n", inverseInertiaTensorWorld.m[2][0], inverseInertiaTensorWorld.m[2][1], inverseInertiaTensorWorld.m[2][2]);
    printf("-\n");*/
    return;
}