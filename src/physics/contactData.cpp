#include "contactData.h"

Contact *ContactData::getContact(unsigned index) const
{
    return contacts[index];
}

void ContactData::addContact(Contact *contact)
{
    if (count < MAX_CONTACTS)
    {
        contacts[count] = contact;
        count++;
    }
}

void ContactData::clearContacts()
{
    count = 0;
}