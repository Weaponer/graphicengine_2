#include "boxCollider.h"
#include "colliderConstruct.h"
#include "rigidbodyActor.h"
#include "transform.h"

BoxCollider::BoxCollider() : Collider(TypeCollider::BOX_COLLIDER), size(1, 1, 1)
{
}

BoxCollider::~BoxCollider()
{
}

void BoxCollider::setSize(mth::vec3 size)
{
    this->size = size;
}

mth::boundOBB BoxCollider::calculateBoundOBB() const
{
    if(constuct == NULL)
    {
        return mth::boundOBB();
    }
    mth::mat3 rot = mth::matrix3FromMatrix4(constuct->getActor()->getOrientation().getMatrix());
    mth::vec3 pos = rot.transform(origin) + constuct->getActor()->getPosition();
    return mth::boundOBB(pos, rot, size);
}

mth::boundSphere BoxCollider::generateGlobalBound()
{
    mth::boundSphere sphere;
    if (constuct == NULL)
    {
        sphere.radius = 0.001f;
        return sphere;
    }
    else
    {
        mth::vec3 pos = constuct->getActor()->getPosition();
        mth::quat rot = constuct->getActor()->getOrientation();
        sphere.origin = rot.rotateVector(origin) + pos;
        sphere.radius = size.magnitude();
        return sphere;
    }
}
