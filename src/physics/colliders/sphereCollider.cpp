#include "sphereCollider.h"
#include "colliderConstruct.h"
#include "rigidbodyActor.h"
#include "transform.h"

SphereCollider::SphereCollider() : Collider(TypeCollider::SPHERE_COLLIDER), radius(1)
{
}

SphereCollider::~SphereCollider()
{
}

void SphereCollider::setRadius(float radius)
{
    this->radius = radius;
}

mth::boundSphere SphereCollider::generateGlobalBound()
{
    mth::boundSphere sphere;
    if (constuct == NULL)
    {
        sphere.radius = 0.001f;
        return sphere;
    }
    else
    {
        mth::vec3 pos = constuct->getActor()->getPosition();
        mth::quat rot = constuct->getActor()->getOrientation();
        sphere.origin = rot.rotateVector(origin) + pos;
        sphere.radius = radius;
        return sphere;
    }
}
