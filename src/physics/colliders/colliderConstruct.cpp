#include <iostream>

#include "colliderConstruct.h"
#include "rigidbodyActor.h"

ColliderConstruct::ColliderConstruct(RigidbodyActor *actor, CollisionsSystem *collisionsSystem) : actor(actor), collisionsSystem(collisionsSystem)
{
}

ColliderConstruct::~ColliderConstruct()
{
    while (listColliders.size() != 0)
    {
        removeCollider(*listColliders.begin());
    }
}

void ColliderConstruct::connectCollider(Collider *collider)
{
    if (collider->constuct != NULL)
    {
        collider->constuct->removeCollider(collider);
    }

    listColliders.push_back(collider);
    collider->constuct = this;

    collisionsSystem->enableCollider(collider);

    if (actor->kinematic != collider->kinematic)
    {
        if (actor->kinematic)
        {
            collisionsSystem->setKinematicCollider(collider);
        }
        else
        {
            collisionsSystem->setDynamicCollider(collider);
        }
    }
}

void ColliderConstruct::removeCollider(Collider *collider)
{
    auto end = listColliders.end();
    auto found = std::find(listColliders.begin(), end, collider);
    if (found != end)
    {
        listColliders.erase(found);
        collider->constuct = NULL;
        collisionsSystem->disableCollider(collider);
    }
}