#include "collisionsSystem.h"
#include "checkCounter.h"
#include "colliderConstruct.h"
#include "rigidbodyActor.h"

CollisionsSystem::CollisionsSystem(LayerManager *layerManager, ThreadDebugOutput *debugOutput) : worldGrid(debugOutput), potentialContactGenerator(&checkCounter, &worldGrid, layerManager, debugOutput)
{
}

CollisionsSystem::~CollisionsSystem()
{
}

void CollisionsSystem::update()
{
    checkCounter.reset();
    generateListsColliders();
    worldGrid.update(&dynamicColliderBuffer, &staticColliderBuffer);
    potentialContactGenerator.generatePotentialContacts(&dynamicColliderBuffer);
    contactGenerator.generateContacts(potentialContactGenerator.getBufferPotentialContacts());
}

void CollisionsSystem::generateListsColliders()
{
    // clear
    dynamicColliderBuffer.clearMemory();
    staticColliderBuffer.clearMemory();

    analytics.clear();
    //

    uint size = colliders.getPosBuffer();
    for (uint i = 0; i < size; i++)
    {
        Collider *collider = *colliders.getMemory(i);
        if (collider->index == -1 || collider->enable == false || collider->constuct == NULL ||
            collider->constuct->getActor()->enable == false)
        {
            continue;
        }
        checkCounter.updateCollider(collider);

        if (collider->kinematic)
        {
            staticColliderBuffer.assignmentMemoryAndCopy(&collider);
        }
        else
        {
            dynamicColliderBuffer.assignmentMemoryAndCopy(&collider);
        }
    }
}

void CollisionsSystem::addCollider(Collider *collider)
{
    uint index = -1;
    colliders.assignmentMemoryAndCopy(&collider, index);
    collider->index = index;
}

void CollisionsSystem::removeCollider(Collider *collider)
{
    colliders.popMemory(collider->index);
    if (collider->enable)
    {
        disableCollider(collider);
    }
    collider->index = -1;
}

void CollisionsSystem::enableCollider(Collider *collider)
{
    collider->enable = true;
}

void CollisionsSystem::disableCollider(Collider *collider)
{
    if (collider->enable == true)
    {
        if (collider->kinematic == true)
        {
            worldGrid.popColliderToKinematic(collider);
        }
        else
        {
            worldGrid.popColliderToDynamic(collider);
        }
    }
    collider->enable = false;
}

void CollisionsSystem::setKinematicCollider(Collider *collider)
{
    if (collider->kinematic == false && collider->enable == true)
    {
        worldGrid.popColliderToDynamic(collider);
    }
    collider->kinematic = true;
}

void CollisionsSystem::setDynamicCollider(Collider *collider)
{
    if (collider->kinematic == true && collider->enable == true)
    {
        worldGrid.popColliderToKinematic(collider);
    }
    collider->kinematic = false;
}