#include "checkCounter.h"

void CheckCounter::reset()
{
    actualNumUpdate = 0;
}

void CheckCounter::nextUpdate()
{
    actualNumUpdate++;
}

bool CheckCounter::checkCollider(const Collider *collider) const
{
    if (collider->numUpdate != actualNumUpdate)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void CheckCounter::updateCollider(Collider *collider)
{
    collider->numUpdate = actualNumUpdate;
}