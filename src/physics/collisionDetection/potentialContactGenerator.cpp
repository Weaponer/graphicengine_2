#include "potentialContactGenerator.h"

#include "layerManager/layer.h"
#include "layerManager/layerManager.h"
#include "physics/colliders/colliderConstruct.h"
#include "physics/rigidbodyActor.h"

#include "MTH/bounds.h"

PotentialContactGenerator::PotentialContactGenerator(CheckCounter *checkCounter, WorldGrid *worldGrid, LayerManager *layerManager, ThreadDebugOutput *debugOutput)
    : checkCounter(checkCounter), worldGrid(worldGrid), layerManager(layerManager), debugOutput(debugOutput)
{
}

PotentialContactGenerator::~PotentialContactGenerator()
{
}

void PotentialContactGenerator::generatePotentialContacts(const EasyStack<Collider *> *dynamicColliderBuffer)
{
    bufferPotentialContacts.clearMemory();

    GridCell *bufferCellsAroundD[27];
    int size = dynamicColliderBuffer->getPos();
    for (int i = 0; i < size; i++)
    {
        checkCounter->nextUpdate();

        Collider *curCol = *dynamicColliderBuffer->getMemory(i);
        const AdditionalDataCollider *additionalData = worldGrid->getAdditionalData(curCol);
        checkCounter->updateCollider(curCol);

        // check from dynamic grid
        worldGrid->getDynamicCellsAroundPoint(additionalData->currentBound.origin, bufferCellsAroundD);
        for (int i2 = 0; i2 < 27; i2++)
        {
            GridCell *cell = bufferCellsAroundD[i2];
            if (cell != NULL)
            {
                workFromCell(cell, curCol);
            }
        }

        // check from kinematic grid
        int minX = 0, minY = 0, minZ = 0;
        int maxX = 0, maxY = 0, maxZ = 0;

        worldGrid->getAreaKinematicCells(additionalData->currentBound, minX, minY, minZ, maxX, maxY, maxZ);
        int cellDist = worldGrid->getSizeKinematicCell() * 2;

        for (int iX = minX; iX <= maxX; iX += cellDist)
        {
            for (int iY = minY; iY <= maxY; iY += cellDist)
            {
                for (int iZ = minZ; iZ <= maxZ; iZ += cellDist)
                {
                    GridCell *cell = worldGrid->getKinematicCell(iX, iY, iZ);
                    if (cell != NULL)
                    {
                        debugOutput->drawCube(mth::vec3(iX, iY, iZ), mth::vec3(worldGrid->getSizeKinematicCell(), worldGrid->getSizeKinematicCell(), worldGrid->getSizeKinematicCell()), mth::vec3(1, 0, 0));
                        workFromCell(cell, curCol);
                    }
                }
            }
        }
    }

    //printf("count potential contacts: %i\n", bufferPotentialContacts.getPos());
}

void PotentialContactGenerator::workFromCell(const GridCell *cell, Collider *collider)
{
    const AdditionalDataCollider *additionalData = worldGrid->getAdditionalData(collider);
    IteratorCell *nextIter = cell->begin;
    while (nextIter != NULL)
    {
        Collider *secondCollider = nextIter->collider;
        if (secondCollider->constuct != collider->constuct && checkCounter->checkCollider(secondCollider))
        {
            checkCounter->updateCollider(secondCollider);
            Layer l1 = collider->constuct->getActor()->layer;
            Layer l2 = secondCollider->constuct->getActor()->layer;

            const AdditionalDataCollider *additionalDataNext = worldGrid->getAdditionalData(secondCollider);
            if (layerManager->isUsePhysics(l1, l2) && mth::test_Sphere_Sphere(additionalData->currentBound, additionalDataNext->currentBound))
            {
                pushPotentialContact(collider, secondCollider);
            }
        }

        nextIter = nextIter->next;
    }
}

bool PotentialContactGenerator::checkNoHavePotentialContact(Collider *one, Collider *two)
{
    int size = bufferPotentialContacts.getPos();
    for (int i = 0; i < size; i++)
    {
        PotentialContact *pt = bufferPotentialContacts.getMemory(i);
        if (pt->colliders[0] == two && pt->colliders[1] == one)
        {
            return false;
        }
        if (pt->colliders[1] == two && pt->colliders[0] == one)
        {
            return false;
        }
    }

    return true;
}

void PotentialContactGenerator::pushPotentialContact(Collider *one, Collider *two)
{
    if (checkNoHavePotentialContact(one, two))
    {
        PotentialContact *ptC = bufferPotentialContacts.assignmentMemory();
        ptC->colliders[0] = one;
        ptC->colliders[1] = two;
    }
}