#include "grid/gridCell.h"

GridCell::GridCell()
{
}

GridCell::~GridCell()
{
}

void GridCell::clearCell()
{
    iX = 0;
    iY = 0;
    iZ = 0;
    typeMemory = -1;
    parentCell = NULL;
    nextCell = NULL;
    begin = NULL;
    end = NULL;
}