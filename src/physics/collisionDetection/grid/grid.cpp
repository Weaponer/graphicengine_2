#include <stdint.h>

#include "grid/grid.h"
#include "ctime"
#include "sphereCollider.h"
Grid::Grid()
{
    gridCells.setAutoUpdate(false);
    extraGridCells.setAutoUpdate(false);
    noUseExtraGridCells.setAutoUpdate(false);

    stackIterators.setAutoUpdate(false);
    stackNoUseIterators.setAutoUpdate(false);

    countUseCells = 0;
    size = 0;
    clearForRestruct(DEFAULT_SIZE_CELL, DEFAULT_SIZE);
}

Grid::~Grid()
{
}

// extra cell
GridCell *Grid::getExtraGridCell()
{
    if (noUseExtraGridCells.getPos() != 0)
    {
        GridCell *cell = *noUseExtraGridCells.getMemoryAndPop();
        cell->clearCell();
        cell->typeMemory = 1;
        return cell;
    }
    else
    {
        GridCell *cell = extraGridCells.assignmentMemory();
        cell->clearCell();
        cell->typeMemory = 1;
        return cell;
    }
}

void Grid::clearExtraGridCell(GridCell *cell)
{
    cell->clearCell();
    noUseExtraGridCells.assignmentMemoryAndCopy(&cell);
}

// cell

GridCell *Grid::getCellAndOther(int iX, int iY, int iZ, uint &cod, GridCell **backCell)
{
    int _n = generateCode(iX, iY, iZ);
    cod = _n;
    _n = getHashIndex(_n);

    GridCell *cell = gridCells.getMemory(_n);

    if (cell->typeMemory == -1)
    {
        return NULL;
    }
    else if (cell->iX == iX && cell->iY == iY && cell->iZ == iZ)
    {
        backCell = NULL;
        return cell;
    }
    else
    {
        GridCell *_ind = cell->nextCell;
        *backCell = cell;
        while (_ind != NULL)
        {
            if (_ind->typeMemory != -1 && _ind->iX == iX && _ind->iY == iY && _ind->iZ == iZ)
            {
                backCell = NULL;
                return _ind;
            }
            *backCell = _ind;
            _ind = _ind->nextCell;
        }
    }

    return NULL;
}

GridCell *Grid::getCell(int iX, int iY, int iZ)
{
    uint cod;
    GridCell *back;
    GridCell *cell = getCellAndOther(iX, iY, iZ, cod, &back);
    return cell;
}

GridCell *Grid::getCellOrCreate(int iX, int iY, int iZ)
{
    uint cod;
    GridCell *back = NULL;
    GridCell *foundCell = getCellAndOther(iX, iY, iZ, cod, &back);

    if (foundCell != NULL)
    {
        return foundCell;
    }
    if (back == NULL)
    {
        countUseCells++;
        int hash = getHashIndex(cod);
        GridCell *cell = gridCells.getMemory(hash);
        cell->typeMemory = 0;
        cell->iX = iX;
        cell->iY = iY;
        cell->iZ = iZ;
        return cell;
    }
    else
    {
        countUseCells++;
        GridCell *cell = getExtraGridCell();
        cell->parentCell = back;
        back->nextCell = cell;
        cell->iX = iX;
        cell->iY = iY;
        cell->iZ = iZ;
        return cell;
    }
}

void Grid::eraseColliderInCell(GridCell *cell, Collider *collider)
{
    IteratorCell *next = cell->begin;
    while (next != NULL)
    {
        if (next->collider == collider)
        {
            IteratorCell *cur = next;
            if (cur->back == NULL && cur->next == NULL)
            {
                cell->begin = NULL;
                cell->end = NULL;
            }
            else if (cur->back == NULL)
            {
                cell->begin = cur->next;
                cell->begin->back = NULL;
            }
            else if (cur->next == NULL)
            {
                cell->end = cur->back;
                cur->back->next = NULL;
            }
            else
            {
                cur->back->next = cur->next;
                cur->next->back = cur->back;
            }

            removeIterator(&cur);
            return;
            break;
        }
        next = next->next;
    }
}

IteratorCell *Grid::addColliderInCell(GridCell *cell, Collider *collider)
{
    IteratorCell *getIter = assignmentIterator();

    if (cell->isEmpty())
    {
        cell->begin = getIter;
        cell->end = getIter;

        getIter->collider = collider;
        getIter->back = NULL;
        getIter->next = NULL;
    }
    else
    {
        IteratorCell *end = cell->end;
        end->next = getIter;
        getIter->back = end;
        getIter->collider = collider;
        getIter->next = NULL;

        cell->end = getIter;
    }
    return getIter;
}

void Grid::clearCell(GridCell *cell)
{
    if (cell->typeMemory != -1)
    {
        countUseCells--;
        if (cell->isEmpty() == false)
        {
            IteratorCell *next = cell->begin;
            while (next != NULL)
            {
                IteratorCell *cur = next;
                next = next->next;

                removeIterator(&cur);
            }
        }

        if (cell->typeMemory == 0 && cell->nextCell == NULL)
        {
            cell->clearCell();
        }
        else if (cell->typeMemory == 0 && cell->nextCell != NULL)
        {
            GridCell *nCell = cell->nextCell;
            cell->iX = nCell->iX;
            cell->iY = nCell->iY;
            cell->iZ = nCell->iZ;
            cell->begin = nCell->begin;
            cell->end = nCell->end;
            
            if(nCell->nextCell != NULL)
            {
                cell->nextCell = nCell->nextCell;
                nCell->nextCell->parentCell = cell;
            }
            else
            {
                cell->nextCell  = NULL;
            }
            
            clearExtraGridCell(nCell);
        }
        else if (cell->typeMemory == 1)
        {
            cell->parentCell->nextCell = cell->nextCell;
            if (cell->nextCell != NULL)
            {
                cell->nextCell->parentCell = cell->parentCell;
            }
            clearExtraGridCell(cell);
        }
    }
}

// iterator
IteratorCell *Grid::assignmentIterator()
{
    if (stackNoUseIterators.getPos() != 0)
    {
        return *stackNoUseIterators.getMemoryAndPop();
    }
    else
    {
        return stackIterators.assignmentMemory();
    }
}

void Grid::removeIterator(IteratorCell **iter)
{
    stackNoUseIterators.assignmentMemoryAndCopy(iter);
}

// clear

void Grid::clear()
{
    countUseCells = 0;
    for (int i = 0; i < size; i++)
    {
        gridCells.getMemory(i)->clearCell();
        extraGridCells.getMemory(i)->clearCell();
    }
}

void Grid::clearForRestruct(int sizeCell, int countCells)
{
    countUseCells = 0;

    gridCells.clearMemory();
    gridCells.setCapacity(countCells);
    extraGridCells.clearMemory();
    extraGridCells.setCapacity(countCells);
    noUseExtraGridCells.clearMemory();
    noUseExtraGridCells.setCapacity((sizeM)countCells);

    stackIterators.clearMemory();
    stackIterators.setCapacity((sizeM)countCells);
    stackNoUseIterators.clearMemory();
    stackNoUseIterators.setCapacity((sizeM)countCells);

    size = countCells;
    clear();
    this->sizeCell = sizeCell;
}

// hash

int Grid::generateCode(int iX, int iY, int iZ)
{
    const uint h1 = 0x8da6b343;
    const uint h2 = 0xd8163841;
    const uint h3 = 0xcb1ab31f;
    int n = h1 * iX + h2 * iY + h3 + iZ;
    return n;
}

int Grid::getHashIndex(int n)
{
    n = n % size;

    if (n < 0)
    {
        n += size;
    }
    return n;
}

static int getPos(float _v, int size)
{
    int side = 1;
    if (_v < 0)
    {
        side = -1;
    }
    _v = abs(_v);
    int v = _v / size;
    v *= size;
    int v2 = v + size;
    if (_v - v < v2 - _v)
    {
        return v * side;
    }
    else
    {
        return v2 * side;
    }
}

void Grid::getPointCell(const mth::vec3 &pos, int &iX, int &iY, int &iZ)
{
    int cellDist = sizeCell << 1;

    iX = getPos(pos.x, cellDist);
    iY = getPos(pos.y, cellDist);
    iZ = getPos(pos.z, cellDist);
}