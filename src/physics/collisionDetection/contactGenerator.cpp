#include <MTH/bounds.h>

#include "contactGenerator.h"

#include "rigidbodyActor.h"
#include "colliderConstruct.h"
#include "collider.h"
#include "sphereCollider.h"
#include "boxCollider.h"

ContactGenerator::ContactGenerator()
{
}

ContactGenerator::~ContactGenerator()
{
}

void ContactGenerator::generateContacts(const EasyStack<PotentialContact> *pContacts)
{
    contacts.clearMemory();
    int size = pContacts->getPos();
    for (int i = 0; i < size; i++)
    {
        PotentialContact *potential = pContacts->getMemory(i);
        createContact(potential);
    }
}

void ContactGenerator::createContact(const PotentialContact *pContact)
{
    if (pContact->colliders[0]->type == TypeCollider::SPHERE_COLLIDER && pContact->colliders[1]->type == TypeCollider::SPHERE_COLLIDER)
    {
        generateSphereAndSphere(pContact);
    }
    else if (pContact->colliders[0]->type == TypeCollider::BOX_COLLIDER && pContact->colliders[1]->type == TypeCollider::BOX_COLLIDER)
    {
        generateBoxAndBox(pContact);
    }
    else if (pContact->colliders[0]->type == TypeCollider::SPHERE_COLLIDER && pContact->colliders[1]->type == TypeCollider::BOX_COLLIDER)
    {
        generateSphereAndBox(pContact);
    }
    else if (pContact->colliders[0]->type == TypeCollider::BOX_COLLIDER && pContact->colliders[1]->type == TypeCollider::SPHERE_COLLIDER)
    {
        PotentialContact spawpP = swapColliders(pContact);
        generateSphereAndBox(&spawpP);
    }
}

PotentialContact ContactGenerator::swapColliders(const PotentialContact *pContact)
{
    PotentialContact p;
    p.colliders[0] = pContact->colliders[1];
    p.colliders[1] = pContact->colliders[0];
    return p;
}

void ContactGenerator::generateSphereAndSphere(const PotentialContact *pContact)
{
    SphereCollider *one = reinterpret_cast<SphereCollider *>(pContact->colliders[0]);
    SphereCollider *two = reinterpret_cast<SphereCollider *>(pContact->colliders[1]);

    mth::boundSphere oneB = one->generateGlobalBound();
    mth::boundSphere twoB = two->generateGlobalBound();

    mth::vec3 posOne = oneB.origin;
    mth::vec3 posTwo = twoB.origin;

    mth::vec3 midline = posOne - posTwo;
    float size = midline.magnitude();
    if (size <= 0.0f || size >= oneB.radius + twoB.radius)
    {
        return;
    }

    mth::vec3 normal = midline * (1.0f / size);

    Contact *contact = contacts.assignmentMemory();
    contact->rigidbodies[0] = one->constuct->getActor();
    contact->rigidbodies[1] = two->constuct->getActor();
    contact->point = posOne - midline * 0.5f;
    contact->normal = normal;
    contact->penetration = oneB.radius + twoB.radius - size;
}

void ContactGenerator::generateSphereAndBox(const PotentialContact *pContact)
{
    SphereCollider *one = reinterpret_cast<SphereCollider *>(pContact->colliders[0]);
    BoxCollider *two = reinterpret_cast<BoxCollider *>(pContact->colliders[1]);

    mth::boundSphere sphere = one->generateGlobalBound();
    mth::boundOBB box = two->calculateBoundOBB();

    if (!mth::test_OBB_Sphere(box, sphere))
    {
        return;
    }

    mth::vec3 relCentre = box.rotate.transformTranspose(sphere.origin - box.center);
    float dist = 0;
    mth::vec3 closestPt;

    dist = relCentre.x;
    if (dist > box.size.x)
        dist = box.size.x;
    if (dist < -box.size.x)
        dist = -box.size.x;
    closestPt.x = dist;

    dist = relCentre.y;
    if (dist > box.size.y)
        dist = box.size.y;
    if (dist < -box.size.y)
        dist = -box.size.y;
    closestPt.y = dist;

    dist = relCentre.z;
    if (dist > box.size.z)
        dist = box.size.z;
    if (dist < -box.size.z)
        dist = -box.size.z;
    closestPt.z = dist;

    closestPt = box.rotate.transform(closestPt) + box.center;

    Contact *contact = contacts.assignmentMemory();
    contact->rigidbodies[0] = one->constuct->getActor();
    contact->rigidbodies[1] = two->constuct->getActor();
    contact->normal = (sphere.origin - closestPt);
    contact->normal.normalise();
    contact->point = closestPt;
    contact->penetration = sphere.radius - (closestPt - sphere.origin).magnitude();
}

//box and box
static inline float transformToAxis(const mth::boundOBB &box, const mth::vec3 &axis)
{

    return box.size.x * abs(axis * box.rotate.getVector(0)) +
           box.size.y * abs(axis * box.rotate.getVector(1)) +
           box.size.z * abs(axis * box.rotate.getVector(2));
}

static inline float penetrationOnAxis(const mth::boundOBB &one, const mth::boundOBB &two, const mth::vec3 &axis, const mth::vec3 &toCentre)
{
    float oneProject = transformToAxis(one, axis);
    float twoProject = transformToAxis(two, axis);

    float distance = abs(toCentre * axis);

    return oneProject + twoProject - distance;
}

static inline bool tryAxis(const mth::boundOBB &one, const mth::boundOBB &two, mth::vec3 axis, const mth::vec3 &toCentre, unsigned index,
                           float &smallestPenetration, unsigned &smallestCase)
{
    if (axis.squareMagnitude() < 0.0001f)
        return true;
    axis.normalise();

    float penetration = penetrationOnAxis(one, two, axis, toCentre);

    if (penetration < 0)
        return false;
    if (penetration < smallestPenetration)
    {
        smallestPenetration = penetration;
        smallestCase = index;
    }
    return true;
}

void fillPointFaceBoxBox(const BoxCollider *one, const BoxCollider *two, const mth::boundOBB &oneB, const mth::boundOBB &twoB, const mth::vec3 &toCentre,
                         Contact *contact, unsigned best, float pen)
{
    mth::vec3 normal = oneB.rotate.getVector(best);
    if (oneB.rotate.getVector(best) * toCentre > 0)
    {
        normal = normal * -1.0f;
    }

    mth::vec3 vertex = two->getSize();
    if (twoB.rotate.getVector(0) * normal < 0)
        vertex.x = -vertex.x;
    if (twoB.rotate.getVector(1) * normal < 0)
        vertex.y = -vertex.y;
    if (twoB.rotate.getVector(2) * normal < 0)
        vertex.z = -vertex.z;

    contact->rigidbodies[0] = one->constuct->getActor();
    contact->rigidbodies[1] = two->constuct->getActor();
    contact->point = twoB.rotate.transform(vertex) + twoB.center;

    contact->normal = normal;

    contact->penetration = pen;
}

static inline mth::vec3 contactPoint(const mth::vec3 &pOne, const mth::vec3 &dOne, float oneSize,
                                     const mth::vec3 &pTwo, const mth::vec3 &dTwo, float twoSize, bool useOne)
{
    mth::vec3 toSt, cOne, cTwo;
    float dpStaOne, dpStaTwo, dpOneTwo, smOne, smTwo;
    float denom, mua, mub;

    smOne = dOne.squareMagnitude();
    smTwo = dTwo.squareMagnitude();
    dpOneTwo = dTwo * dOne;

    toSt = pOne - pTwo;
    dpStaOne = dOne * toSt;
    dpStaTwo = dTwo * toSt;

    denom = smOne * smTwo - dpOneTwo * dpOneTwo;

    if (abs(denom) < 0.0001f)
    {
        return useOne ? pOne : pTwo;
    }

    mua = (dpOneTwo * dpStaTwo - smTwo * dpStaOne) / denom;
    mub = (smOne * dpStaTwo - dpOneTwo * dpStaOne) / denom;

    if (mua > oneSize ||
        mua < -oneSize ||
        mub > twoSize ||
        mub < -twoSize)
    {
        return useOne ? pOne : pTwo;
    }
    else
    {
        cOne = pOne + dOne * mua;
        cTwo = pTwo + dTwo * mub;

        return cOne * 0.5 + cTwo * 0.5;
    }
}

#define CHECK_OVERLAP(axis, index)                                \
    if (!tryAxis(one, two, (axis), toCenter, (index), pen, best)) \
        return;

void ContactGenerator::generateBoxAndBox(const PotentialContact *pContact)
{

    BoxCollider *oneB = (BoxCollider *)pContact->colliders[0];
    BoxCollider *twoB = (BoxCollider *)pContact->colliders[1];

    mth::boundOBB one = oneB->calculateBoundOBB();
    mth::boundOBB two = twoB->calculateBoundOBB();

    mth::vec3 toCenter = two.center - one.center;
    float pen = 100000;

    unsigned best = 0xffffff;
    CHECK_OVERLAP(one.rotate.getVector(0), 0);
    CHECK_OVERLAP(one.rotate.getVector(1), 1);
    CHECK_OVERLAP(one.rotate.getVector(2), 2);

    CHECK_OVERLAP(two.rotate.getVector(0), 3);
    CHECK_OVERLAP(two.rotate.getVector(1), 4);
    CHECK_OVERLAP(two.rotate.getVector(2), 5);

    unsigned bestSingleAxis = best;

    CHECK_OVERLAP(one.rotate.getVector(0) % two.rotate.getVector(0), 6);
    CHECK_OVERLAP(one.rotate.getVector(0) % two.rotate.getVector(1), 7);
    CHECK_OVERLAP(one.rotate.getVector(0) % two.rotate.getVector(2), 8);
    CHECK_OVERLAP(one.rotate.getVector(1) % two.rotate.getVector(0), 9);
    CHECK_OVERLAP(one.rotate.getVector(1) % two.rotate.getVector(1), 10);
    CHECK_OVERLAP(one.rotate.getVector(1) % two.rotate.getVector(2), 11);
    CHECK_OVERLAP(one.rotate.getVector(2) % two.rotate.getVector(0), 12);
    CHECK_OVERLAP(one.rotate.getVector(2) % two.rotate.getVector(1), 13);
    CHECK_OVERLAP(one.rotate.getVector(2) % two.rotate.getVector(2), 14);

    if (best == 0xffffff)
        return;

    if (best < 3)
    {
        Contact *contact = contacts.assignmentMemory();
        fillPointFaceBoxBox(oneB, twoB, one, two, toCenter, contact, best, pen);
        return;
    }
    else if (best < 6)
    {
        Contact *contact = contacts.assignmentMemory();
        fillPointFaceBoxBox(twoB, oneB, two, one, toCenter * -1, contact, best - 3, pen);
        return;
    }
    else
    {
        best -= 6;
        unsigned oneAxisIndex = best / 3;
        unsigned twoAxisIndex = best % 3;

        mth::vec3 oneAxis = one.rotate.getVector(oneAxisIndex);
        mth::vec3 twoAxis = two.rotate.getVector(twoAxisIndex);
        mth::vec3 axis = oneAxis % twoAxis;
        axis.normalise();

        if (axis * toCenter > 0)
            axis = axis * -1.0f;

        mth::vec3 ptOnOneEdge = one.size;
        mth::vec3 ptOnTwoEdge = two.size;

        for (unsigned i = 0; i < 3; i++)
        {
            if (i == oneAxisIndex)
                ptOnOneEdge[i] = 0;
            else if (one.rotate.getVector(i) * axis > 0)
                ptOnOneEdge[i] = -ptOnOneEdge[i];

            if (i == twoAxisIndex)
                ptOnTwoEdge[i] = 0;
            else if (two.rotate.getVector(i) * axis < 0)
                ptOnTwoEdge[i] = -ptOnTwoEdge[i];
        }

        ptOnOneEdge = one.rotate.transform(ptOnOneEdge) + one.center;
        ptOnTwoEdge = two.rotate.transform(ptOnTwoEdge) + two.center;

        mth::vec3 vertex = contactPoint(ptOnOneEdge, oneAxis, one.size[oneAxisIndex],
                                        ptOnTwoEdge, twoAxis, two.size[twoAxisIndex],
                                        bestSingleAxis > 2);

        Contact *contact = contacts.assignmentMemory();
        contact->rigidbodies[0] = oneB->constuct->getActor();
        contact->rigidbodies[1] = twoB->constuct->getActor();
        contact->penetration = pen;
        contact->normal = axis;
        contact->point = vertex;
        return;
    }
}