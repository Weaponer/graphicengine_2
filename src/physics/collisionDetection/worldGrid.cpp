#include <MTH/boundAABB.h>
#include <MTH/bounds.h>

#include "worldGrid.h"

#include "transform.h"
#include "collider.h"
#include "colliderConstruct.h"
#include "rigidbodyActor.h"
#include "boxCollider.h"

WorldGrid::WorldGrid(ThreadDebugOutput *debugOutput) : dynamicGrid(), staticGrid(), analytics(), debugOutput(debugOutput)
{
    dynamicGrid.clearForRestruct(DEFAULT_DYNAMIC_CELL_SIZE, 1);
    staticGrid.clearForRestruct(DEFAULT_STATIC_CELL_SIZE, 1);

    AdditionalDataCollider addData;
    addData.isChange = false;
    addData.isKinematic = false;
    addData.currentBound = mth::boundSphere();
    addData.newBound = mth::boundSphere();
    collidersAdditionalData.setUseDefault(true);
    collidersAdditionalData.setDefault(&addData);
}

WorldGrid::~WorldGrid()
{
}

void WorldGrid::update(const EasyStack<Collider *> *dynamicColliders, const EasyStack<Collider *> *kinematicColliders)
{
    analytics.clear();

    int size = dynamicColliders->getPos();

    for (int i = 0; i < size; i++)
    {
        Collider *collider = *dynamicColliders->getMemory(i);
        AdditionalDataCollider *additionalData = getAdditionalData(collider);
        additionalData->newBound = collider->generateGlobalBound();
        if (additionalData->currentBound.radius != additionalData->newBound.radius ||
            additionalData->currentBound.origin != additionalData->newBound.origin)
        {
            additionalData->isChange = true;
            analytics.changeDynamicColliders = true;
        }
        float fR = additionalData->newBound.radius;
        int iR = (int)fR;
        if (fR > iR)
        {
            iR++;
        }
        analytics.maxRadiusDynamic = std::max(analytics.maxRadiusDynamic, (float)iR);
    }

    size = kinematicColliders->getPos();
    for (int i = 0; i < size; i++)
    {
        Collider *collider = *kinematicColliders->getMemory(i);
        AdditionalDataCollider *additionalData = getAdditionalData(collider);
        additionalData->newBound = collider->generateGlobalBound();
        if (additionalData->currentBound.radius != additionalData->newBound.radius ||
            additionalData->currentBound.origin != additionalData->newBound.origin)
        {
            additionalData->isChange = true;
            analytics.changeKinematicColliders = true;
        }

        analytics.potentialAmountStaticCells += maxCountCellsForRadiusKinematic(additionalData->newBound.radius);
    }

    updateDynamicColliders(dynamicColliders);
    updateKinematicColliders(kinematicColliders);
}

void WorldGrid::updateDynamicColliders(const EasyStack<Collider *> *dynamicColliders)
{
    int size = dynamicColliders->getPos();

    if (analytics.changeDynamicColliders == false)
    {
        return;
    }

    if (analytics.maxRadiusDynamic > dynamicGrid.getSizeCell() || dynamicGrid.getSize() < size)
    {
        reconstructDynamicGrid(dynamicColliders);
        return;
    }
    for (int i = 0; i < size; i++)
    {
        Collider *collider = *(dynamicColliders->getMemory(i));
        AdditionalDataCollider *additionalData = getAdditionalData(collider);

        if (additionalData->isChange)
        {
            popColliderToDynamic(collider);
            additionalData->currentBound = additionalData->newBound;
            additionalData->isChange = false;
            pushColliderToDynamic(collider);
        }
    }
}

void WorldGrid::updateKinematicColliders(const EasyStack<Collider *> *kinematicColliders)
{
    if (analytics.changeKinematicColliders == false)
    {
        return;
    }

    if (staticGrid.getSize() <= analytics.potentialAmountStaticCells)
    {
        reconstructKinematicGrid(kinematicColliders);
        return;
    }

    int size = kinematicColliders->getPos();
    for (int i = 0; i < size; i++)
    {
        Collider *collider = *kinematicColliders->getMemory(i);
        AdditionalDataCollider *additionalData = getAdditionalData(collider);

        if (additionalData->isChange)
        {
            popColliderToKinematic(collider);
            additionalData->currentBound = additionalData->newBound;
            additionalData->isChange = false;
            pushColliderToKinematic(collider);
        }
    }
}

void WorldGrid::reconstructDynamicGrid(const EasyStack<Collider *> *dynamicColliders)
{
    int size = dynamicColliders->getPos();
    dynamicGrid.clearForRestruct(analytics.maxRadiusDynamic, size);
    for (int i = 0; i < size; i++)
    {
        Collider *collider = *dynamicColliders->getMemory(i);
        AdditionalDataCollider *additionalData = getAdditionalData(collider);
        if (additionalData->isChange)
        {
            additionalData->currentBound = additionalData->newBound;
            additionalData->isChange = false;
        }
        pushColliderToDynamic(collider);
    }
}

void WorldGrid::reconstructKinematicGrid(const EasyStack<Collider *> *kinematicColliders)
{
    int size = kinematicColliders->getPos();
    staticGrid.clearForRestruct(dynamicGrid.getSizeCell() * 2, analytics.potentialAmountStaticCells * 2);

    for (int i = 0; i < size; i++)
    {
        Collider *collider = *(kinematicColliders->getMemory(i));
        AdditionalDataCollider *additionalData = getAdditionalData(collider);

        if (additionalData->isChange)
        {
            additionalData->currentBound = additionalData->newBound;
            additionalData->isChange = false;
        }
        pushColliderToKinematic(collider);
    }
}

void WorldGrid::addCollider(const Collider *collider)
{
    if (collider->index >= (int)collidersAdditionalData.getCapacity())
    {
        collidersAdditionalData.setCapacity(collidersAdditionalData.getCapacity() * 2);
    }

    AdditionalDataCollider *addData = getAdditionalData(collider);
    addData->isKinematic = collider->kinematic;
    addData->currentBound = mth::boundSphere();
    addData->newBound = mth::boundSphere();

    addData->isChange = true;
}

void WorldGrid::removeCollider(const Collider *collider)
{
}

AdditionalDataCollider *WorldGrid::getAdditionalData(const Collider *collider)
{
    if (collider->index >= (int)collidersAdditionalData.getCapacity())
    {
        collidersAdditionalData.setCapacity(collidersAdditionalData.getCapacity() * 2);
    }
    return collidersAdditionalData.getMemory(collider->index);
}
static int maxCountCellsForRadius(float radius, int sizeCell)
{
    float fR = radius / sizeCell;
    int iR = (int)fR;
    int count = 0;

    if (fR > iR)
    {
        count = iR + 2;
    }
    else
    {
        count = iR + 1;
    }
    return count * count * count;
}

int WorldGrid::maxCountCellsForRadiusKinematic(float radius)
{
    return maxCountCellsForRadius(radius, staticGrid.getSizeCell());
}

GridCell *WorldGrid::getKinematicCell(int iX, int iY, int iZ)
{
    return staticGrid.getCell(iX, iY, iZ);
}

void WorldGrid::getAreaKinematicCells(const mth::boundSphere &globalBound, int &minX, int &minY, int &minZ, int &maxX, int &maxY, int &maxZ)
{
    mth::vec3 size = mth::vec3(globalBound.radius, globalBound.radius, globalBound.radius);
    mth::vec3 min = globalBound.origin - size;
    mth::vec3 max = globalBound.origin + size;

    staticGrid.getPointCell(min, minX, minY, minZ);
    staticGrid.getPointCell(max, maxX, maxY, maxZ);
}

void WorldGrid::getDynamicCellsAroundPoint(const mth::vec3 &point, GridCell **bufferCells)
{
    int cellDist = dynamicGrid.getSizeCell() * 2;
    int iX = 0, iY = 0, iZ = 0;
    dynamicGrid.getPointCell(point, iX, iY, iZ);
    iX -= cellDist;
    iY -= cellDist;
    iZ -= cellDist;
    int limX = iX + cellDist * 2;
    int limY = iY + cellDist * 2;
    int limZ = iZ + cellDist * 2;
    int next = 0;

    for (int i = iX; i <= limX; i += cellDist)
    {
        for (int i2 = iY; i2 <= limY; i2 += cellDist)
        {
            for (int i3 = iZ; i3 <= limZ; i3 += cellDist)
            {
                bufferCells[next] = dynamicGrid.getCell(i, i2, i3);
                next++;
            }
        }
    }
}

void WorldGrid::popColliderToKinematic(Collider *collider)
{
    AdditionalDataCollider *additionalData = getAdditionalData(collider);
    mth::vec3 size = mth::vec3(additionalData->currentBound.radius, additionalData->currentBound.radius, additionalData->currentBound.radius);
    mth::vec3 min = additionalData->currentBound.origin - size;
    mth::vec3 max = additionalData->currentBound.origin + size;

    int minX = 0;
    int minY = 0;
    int minZ = 0;
    int maxX = 0;
    int maxY = 0;
    int maxZ = 0;

    staticGrid.getPointCell(min, minX, minY, minZ);
    staticGrid.getPointCell(max, maxX, maxY, maxZ);
    int cellDist = staticGrid.getSizeCell() * 2;
    for (int i = minX; i <= maxX; i += cellDist)
    {
        for (int i2 = minY; i2 <= maxY; i2 += cellDist)
        {
            for (int i3 = minZ; i3 <= maxZ; i3 += cellDist)
            {
                GridCell *cell = staticGrid.getCell(i, i2, i3);
                if (cell != NULL && cell->isEmpty() == false)
                {
                    staticGrid.eraseColliderInCell(cell, collider);
                    if (cell->isEmpty())
                    {
                        staticGrid.clearCell(cell);
                    }
                }
            }
        }
    }
}

void WorldGrid::popColliderToDynamic(Collider *collider)
{
    AdditionalDataCollider *additionalData = getAdditionalData(collider);
    int iX = 0;
    int iY = 0;
    int iZ = 0;
    dynamicGrid.getPointCell(additionalData->currentBound.origin, iX, iY, iZ);
    GridCell *cell = dynamicGrid.getCell(iX, iY, iZ);
    if (cell != NULL && cell->isEmpty() == false)
    {
        dynamicGrid.eraseColliderInCell(cell, collider);
        if (cell->isEmpty())
        {
            dynamicGrid.clearCell(cell);
        }
    }
}

void WorldGrid::pushColliderToKinematic(Collider *collider)
{
    AdditionalDataCollider *additionalData = getAdditionalData(collider);
    mth::vec3 size = mth::vec3(additionalData->currentBound.radius, additionalData->currentBound.radius, additionalData->currentBound.radius);
    mth::vec3 min = additionalData->currentBound.origin - size;
    mth::vec3 max = additionalData->currentBound.origin + size;

    int minX = 0;
    int minY = 0;
    int minZ = 0;
    int maxX = 0;
    int maxY = 0;
    int maxZ = 0;

    staticGrid.getPointCell(min, minX, minY, minZ);
    staticGrid.getPointCell(max, maxX, maxY, maxZ);

    int sizeCell = staticGrid.getSizeCell();
    int step = sizeCell * 2;
    mth::boundAABB aabb = mth::boundAABB();

    for (int i = minX; i <= maxX; i += step)
    {
        for (int i2 = minY; i2 <= maxY; i2 += step)
        {
            for (int i3 = minZ; i3 <= maxZ; i3 += step)
            {
                aabb = mth::boundAABB(mth::vec3(i, i2, i3), mth::vec3(sizeCell, sizeCell, sizeCell));

                if (collider->type == TypeCollider::SPHERE_COLLIDER && mth::test_AABB_Sphere(aabb, additionalData->currentBound))
                {
                    GridCell *cell = staticGrid.getCellOrCreate(i, i2, i3);
                    staticGrid.addColliderInCell(cell, collider);
                }
                else if (collider->type == TypeCollider::BOX_COLLIDER &&
                         mth::test_OBB_OBB(mth::boundOBB(aabb.origin, aabb.size), ((BoxCollider *)collider)->calculateBoundOBB()))
                {
                    GridCell *cell = staticGrid.getCellOrCreate(i, i2, i3);
                    staticGrid.addColliderInCell(cell, collider);
                }
            }
        }
    }
}

void WorldGrid::pushColliderToDynamic(Collider *collider)
{
    AdditionalDataCollider *additionalData = getAdditionalData(collider);

    int iX = 0;
    int iY = 0;
    int iZ = 0;
    dynamicGrid.getPointCell(additionalData->currentBound.origin, iX, iY, iZ);
    GridCell *cell = dynamicGrid.getCellOrCreate(iX, iY, iZ);
    dynamicGrid.addColliderInCell(cell, collider);

}
