#include "window/inputSystem.h"

InputSystem::InputSystem()
{
}

InputSystem::~InputSystem()
{
}

void InputSystem::startNewUpdates()
{
    mouse.nextUpdate();
    keyboard.nextUpdate();
}

void InputSystem::updateByEvent(SDL_Event *event)
{

    if (event->type == SDL_MOUSEBUTTONDOWN || event->type == SDL_MOUSEBUTTONUP || event->type == SDL_MOUSEMOTION || event->type == SDL_MOUSEWHEEL)
    {
        mouse.updateByEvent(event);
    }
    else if (event->type == SDL_KEYDOWN || event->type == SDL_KEYUP)
    {
        keyboard.updateByEvent(event);
    }
}