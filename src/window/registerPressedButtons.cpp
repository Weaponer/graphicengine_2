#include <algorithm>

#include "window/registerPressedButtons.h"

void RegisterPressedButtons::nextUpdate()
{
    std::for_each(buttonsData.begin(), buttonsData.end(), [](pressData& data) {
        data.down = false;
        data.up = false;
    });
}

void RegisterPressedButtons::setDownButton(Uint32 button)
{
    pressData press;
    press.current = true;
    press.down = true;
    press.up = false;
    buttonsData[button] = press;
}

void RegisterPressedButtons::setUpButton(Uint32 button)
{
    pressData press;
    press.current = false;
    press.down = false;
    press.up = true;
    buttonsData[button] = press;
}

bool RegisterPressedButtons::getButtonDown(Uint32 button) const
{
    return buttonsData[button].down;
}

bool RegisterPressedButtons::getButton(Uint32 button) const
{
    return buttonsData[button].current;
}

bool RegisterPressedButtons::getButtonUp(Uint32 button) const
{
    return buttonsData[button].up;
}

void RegisterPressedButtons::setCountButtons(Uint32 count)
{
    buttonsData.reserve(count);
    for(Uint32 i = 0; i < count; i++)
    {
        pressData press;
        press.current = false;
        press.down = false;
        press.up = false;
        buttonsData.push_back(press);
    }
}
