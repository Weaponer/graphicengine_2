#include "window/keyboard.h"

Keyboard::Keyboard()
{
    buttonsData.setCountButtons(SDL_NUM_SCANCODES);
}

Keyboard::~Keyboard()
{
}

void Keyboard::nextUpdate()
{
    buttonsData.nextUpdate();
}

void Keyboard::updateByEvent(SDL_Event *event)
{
    if (event->type == SDL_KEYUP)
    {
        buttonsData.setUpButton(event->key.keysym.scancode);
    }
    else if (event->type == SDL_KEYDOWN)
    {
        buttonsData.setDownButton(event->key.keysym.scancode);
    }
}

bool Keyboard::getKeyDown(SDL_Scancode code) const
{
    return buttonsData.getButtonDown(code);
}

bool Keyboard::getKey(SDL_Scancode code) const
{
    return buttonsData.getButton(code);
}

bool Keyboard::getKeyUp(SDL_Scancode code) const
{
    return buttonsData.getButtonUp(code);
}