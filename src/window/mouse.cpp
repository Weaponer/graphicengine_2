#include "window/mouse.h"

Mouse::Mouse() : position(0, 0), delta(0, 0)
{
    buttonsData.setCountButtons(SDL_BUTTON_X2 + 1);
}

Mouse::~Mouse()
{
}

void Mouse::nextUpdate()
{
    buttonsData.nextUpdate();
    delta.clear();
}

void Mouse::updateByEvent(SDL_Event *event)
{
    if (event->type == SDL_MOUSEBUTTONUP)
    {
        buttonsData.setUpButton(event->button.button);
    }
    else if (event->type == SDL_MOUSEBUTTONDOWN)
    {
        buttonsData.setDownButton(event->button.button);
    }
    else if (event->type == SDL_MOUSEMOTION)
    {
        position = mth::vec2(event->motion.x, event->motion.y);
        delta = mth::vec2(event->motion.xrel, event->motion.yrel);
    }
}

mth::vec2 Mouse::getMousePosition() const
{
    return position;
}

mth::vec2 Mouse::getMouseDeltaMove() const
{
    return delta;
}

bool Mouse::getMouseButtonDown(Uint8 code) const
{
    return buttonsData.getButtonDown(code);
}

bool Mouse::getMouseButton(Uint8 code) const
{
    return buttonsData.getButton(code);
}

bool Mouse::getMouseButtonUp(Uint8 code) const
{
    return buttonsData.getButtonUp(code);
}