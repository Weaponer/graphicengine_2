#include "window.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

Window::Window(int width, int height) : width(width), height(height), isMouse(false)
{
    SDL_Init(SDL_INIT_VIDEO);
    IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    wind = SDL_CreateWindow("Window", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

    if (wind == NULL)
    {
        printf("Could not create window: %s\n", SDL_GetError());
        return;
    }

    glContext = SDL_GL_CreateContext(wind);
    SDL_GL_SetSwapInterval(1);
    SDL_SetRelativeMouseMode(SDL_FALSE);
}

Window::~Window()
{
    if (wind)
    {
        SDL_GL_DeleteContext(glContext);
        SDL_DestroyWindow(wind);
    }
}

int Window::updateEvent()
{
    inputSystem.startNewUpdates();
    SDL_Event e;
    while (SDL_PollEvent(&e) != 0)
    {
        if (e.type == SDL_QUIT || (e.type == SDL_KEYDOWN && e.key.keysym.scancode == SDL_SCANCODE_ESCAPE))
        {
            return 0;
        }
        inputSystem.updateByEvent(&e);
        if (e.type == SDL_KEYDOWN && e.key.keysym.scancode == SDL_SCANCODE_C)
        {
            isMouse = !isMouse;
            if (isMouse)
                SDL_SetRelativeMouseMode(SDL_TRUE);
            else
                SDL_SetRelativeMouseMode(SDL_FALSE);
        }
    }

    return 1;
}

void Window::swapBuffers()
{
    SDL_GL_SwapWindow(wind);
}

void Window::setMouseLock(bool isLock)
{
    SDL_SetRelativeMouseMode((SDL_bool)isLock);
    isMouse = isLock;
}