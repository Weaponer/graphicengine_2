#include "resourceLoader/resourceAsyncHandler.h"

ResourceAsyncHandler::ResourceAsyncHandler(AsyncLoader *loader, const char *path, TypeResource type)
    : path(path), type(type), status(StatusLoad::SL_None), object(NULL), loader(loader), isDiscard(false)
{

}

ResourceAsyncHandler::~ResourceAsyncHandler()
{
}
