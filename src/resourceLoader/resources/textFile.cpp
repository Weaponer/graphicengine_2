#include "resourceLoader/resources/textFile.h"

TextFile::TextFile(const char *_nameFile, const char *_contentFile) : nameFile(_nameFile), contentFile(_contentFile)
{
}

TextFile::~TextFile()
{
}

const char *TextFile::getNameFile() const
{
    return nameFile.c_str();
}

const char *TextFile::getContentFile() const
{
    return contentFile.c_str();
}