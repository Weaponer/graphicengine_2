#include <set>
#include <string.h>
#include <cstring>

#include "resourceLoader/loaders/shaderLoader.h"
#include "renderEngine/graphicEngine.h"
#include "renderEngine/resources/shader/shaderPropertiesData/propertiesRegistry.h"
#include "resourceLoader/loaders/cubeMapLoad.h"
#include "resourceLoader/loaders/hdrTextureLoad.h"
#include "renderEngine/resources/shader/shadersStorage.h"
#include "resourceLoader/resourceLoader.h"

#include "renderEngine/resources/shader/setInitializedTextures.h"

int readInt(SDL_RWops *_file)
{
    int val;
    _file->read(_file, &val, sizeof(int), 1);
    return val;
}

char readChar(SDL_RWops *_file)
{
    int val;
    _file->read(_file, &val, sizeof(char), 1);
    return val;
}

float readFloat(SDL_RWops *_file)
{
    float val;
    _file->read(_file, &val, sizeof(float), 1);
    return val;
}

bool readBool(SDL_RWops *_file)
{
    bool val;
    _file->read(_file, &val, sizeof(bool), 1);
    return val;
}

std::string readString(SDL_RWops *_file)
{
    std::string val;
    int count = readInt(_file);
    char *str = new char[count + 1];
    _file->read(_file, str, sizeof(char), count);
    str[count] = 0;
    val = std::string(str);
    delete str;
    return val;
}

std::vector<PropertyTemp *> readProperties(SDL_RWops *_file)
{
    int countPrperties = readInt(_file);
    std::vector<PropertyTemp *> properties;
    properties.reserve(countPrperties);
    for (int i = 0; i < countPrperties; i++)
    {
        PropertyTemp *tempProperty = NULL;
        std::string name = readString(_file);
        TypeProperty type = (TypeProperty)readInt(_file);

        if (type == TypeProperty::Int_P)
        {
            IntPropertyT *prop = new IntPropertyT();
            prop->value = readInt(_file);
            tempProperty = prop;
        }
        else if (type == TypeProperty::Float_P)
        {
            FloatPropertyT *prop = new FloatPropertyT();
            prop->value = readFloat(_file);
            tempProperty = prop;
        }
        else if (type == TypeProperty::Toggle_P)
        {
            TogglePropertyT *prop = new TogglePropertyT();
            prop->value = readBool(_file);
            prop->mask = readInt(_file);
            tempProperty = prop;
        }
        else if (type == TypeProperty::Vector_P)
        {
            VectorPropertyT *prop = new VectorPropertyT();
            prop->value.w = readFloat(_file);
            prop->value.x = readFloat(_file);
            prop->value.y = readFloat(_file);
            prop->value.z = readFloat(_file);
            tempProperty = prop;
        }
        else if (type == TypeProperty::Color_P)
        {
            ColorPropertyT *prop = new ColorPropertyT();
            float a = readFloat(_file);
            float b = readFloat(_file);
            float g = readFloat(_file);
            float r = readFloat(_file);
            prop->value.setColor(r, g, b, a);
            tempProperty = prop;
        }
        else if (type == TypeProperty::Texture_P)
        {
            TexturePropertyT *prop = new TexturePropertyT();
            prop->defaultPath = readString(_file);
            prop->isHDRTexture = readBool(_file);
            tempProperty = prop;
        }
        else if (type == TypeProperty::CubeMap_P)
        {
            CubeMapPropertyT *prop = new CubeMapPropertyT();
            prop->defaultPath = readString(_file);
            tempProperty = prop;
        }
        tempProperty->name = name;
        tempProperty->type = type;

        properties.push_back(tempProperty);
    }

    return properties;
}

void readSettings(SDL_RWops *_file, ShaderSettings *_shaderSettings)
{
    _shaderSettings->normalMapIndex = readInt(_file);
    _shaderSettings->clippingTextureIndex = readInt(_file);
    _shaderSettings->typeClippingChanel = (TypeClippingChanel)readInt(_file);
    _shaderSettings->clippingValue = readFloat(_file);
    _shaderSettings->inverseClipping = readBool(_file);
}

void readProgram(SDL_RWops *_file, ProgramTemp *_destProgram)
{
    _destProgram->mask = readInt(_file);
    _destProgram->program = readString(_file);

    int countProperties = readInt(_file);
    for (int i = 0; i < countProperties; i++)
    {
        DefineProperty prop = DefineProperty();
        prop.second = (TypeProperty)readInt(_file);
        prop.first = readString(_file);
        _destProgram->properties.push_back(prop);
    }

    int countUniformBlocksUse = readInt(_file);
    for (int i = 0; i < countUniformBlocksUse; i++)
    {
        std::string str = readString(_file);
        _destProgram->uniformBlocksUse.push_back(str);
    }
}

void readPass(SDL_RWops *_file, PassTemp *_destPass)
{
    _destPass->name = readString(_file);
    _destPass->pass = (TypePass)readInt(_file);

    bool haveAdditionalData = readBool(_file);
    if (haveAdditionalData)
    {
        _destPass->sizeAdditionalData = readInt(_file);
        for (int i = 0; i < _destPass->sizeAdditionalData; i++)
        {
            _destPass->additionalData.push_back(readChar(_file));
        }
    }
    else
    {
        _destPass->sizeAdditionalData = 0;
        _destPass->additionalData.clear();
    }

    _destPass->useDefaultPrograms = readBool(_file);
    _destPass->useNormalMap = readBool(_file);
    _destPass->useClippingMap = readBool(_file);

    _destPass->queue = (QueueRender)readInt(_file);
    _destPass->renderType = (RenderType)readInt(_file);
    _destPass->cullType = (CullType)readInt(_file);
    _destPass->zTest = readBool(_file);
    _destPass->zWrite = readBool(_file);
    _destPass->generateInstancing = readBool(_file);

    _destPass->maskUsingDefinesVertex = readInt(_file);
    _destPass->maskUsingDefinesFragment = readInt(_file);

    int countVertexes = readInt(_file);
    for (int i = 0; i < countVertexes; i++)
    {
        _destPass->vertexPrograms.push_back(ProgramTemp());
        readProgram(_file, &(_destPass->vertexPrograms[i]));
    }

    int countFragment = readInt(_file);
    for (int i = 0; i < countFragment; i++)
    {
        _destPass->fragmentPrograms.push_back(ProgramTemp());
        readProgram(_file, &(_destPass->fragmentPrograms[i]));
    }
}

void deleteTempProperties(std::vector<PropertyTemp *> *_properties)
{
    int count = _properties->size();
    for (int i = 0; i < count; i++)
    {
        delete (*_properties)[i];
    }

    _properties->clear();
}

void generateReleaseProperties(const std::vector<PropertyTemp *> *_propertiesTemp, std::vector<Property *> *_releaseProperties, SpecialSystems *_specialSystems)
{
    ShadersStorage *shadersStorage = _specialSystems->graphicEngine->getMainSystems()->getShadersStorage();
    PropertiesRegistry *propertiesRegistry = shadersStorage->getPropertiesRegistry();

    int countProperties = _propertiesTemp->size();
    for (int i = 0; i < countProperties; i++)
    {
        Property *prop = NULL;

        if (_propertiesTemp->at(i)->type == TypeProperty::Int_P)
        {
            IntPropertyT *propTemp = reinterpret_cast<IntPropertyT *>(_propertiesTemp->at(i));
            prop = propertiesRegistry->createIntProperty(propTemp->name.c_str(), propTemp->value);
        }
        else if (_propertiesTemp->at(i)->type == TypeProperty::Float_P)
        {
            FloatPropertyT *propTemp = reinterpret_cast<FloatPropertyT *>(_propertiesTemp->at(i));
            prop = propertiesRegistry->createFloatProperty(propTemp->name.c_str(), propTemp->value);
        }
        else if (_propertiesTemp->at(i)->type == TypeProperty::Toggle_P)
        {
            TogglePropertyT *propTemp = reinterpret_cast<TogglePropertyT *>(_propertiesTemp->at(i));
            prop = propertiesRegistry->createToggleProperty(propTemp->name.c_str(), propTemp->value);
        }
        else if (_propertiesTemp->at(i)->type == TypeProperty::Vector_P)
        {
            VectorPropertyT *propTemp = reinterpret_cast<VectorPropertyT *>(_propertiesTemp->at(i));
            prop = propertiesRegistry->createVectorProperty(propTemp->name.c_str(), propTemp->value);
        }
        else if (_propertiesTemp->at(i)->type == TypeProperty::Color_P)
        {
            ColorPropertyT *propTemp = reinterpret_cast<ColorPropertyT *>(_propertiesTemp->at(i));
            prop = propertiesRegistry->createColorProperty(propTemp->name.c_str(), propTemp->value);
        }
        else if (_propertiesTemp->at(i)->type == TypeProperty::Texture_P)
        {
            TexturePropertyT *propTemp = reinterpret_cast<TexturePropertyT *>(_propertiesTemp->at(i));
            prop = propertiesRegistry->createTextureProperty(propTemp->name.c_str(), propTemp->texture);
        }
        else if (_propertiesTemp->at(i)->type == TypeProperty::CubeMap_P)
        {
            CubeMapPropertyT *propTemp = reinterpret_cast<CubeMapPropertyT *>(_propertiesTemp->at(i));
            prop = propertiesRegistry->createCubeMapProperty(propTemp->name.c_str(), propTemp->cubeMap);
        }

        _releaseProperties->push_back(prop);
    }
}

void fillProgramForPass(Program *_program, const ProgramTemp *_programTemp, SetInitializedTextures *_setInitializedTextures, UniformBlocksRegistry *_uniformBlocksRegistry, PropertiesRegistry *propertiesRegistry, GLenum _typeProgram)
{
    _program->setMask(_programTemp->mask);
    _program->setProgram(_programTemp->program.c_str(), _typeProgram);
    std::vector<const Property *> propertiesInProgram;
    int count = (int)_programTemp->properties.size();
    propertiesInProgram.reserve(count);
    for (int i = 0; i < count; i++)
    {
        const std::pair<std::string, TypeProperty> *data = &(_programTemp->properties[i]);
        const Property *prop = propertiesRegistry->getGlobalSettingProperty(data->first.c_str(), data->second);
        if (prop != NULL)
        {
            propertiesInProgram.push_back(prop);
        }
    }
    _program->setProperties(propertiesInProgram.size(), &(propertiesInProgram[0]), _setInitializedTextures);
    int countUniformBlocksUse = _programTemp->uniformBlocksUse.size();
    for (int i = 0; i < countUniformBlocksUse; i++)
    {
        const char *name = _programTemp->uniformBlocksUse[i].c_str();
        int index = _uniformBlocksRegistry->getIndexReserveUniformBlock(name);
        if (index != -1)
        {
            _program->setActiveUniformBlock(name, (GLuint)index);
        }
    }
}

Pass *getDefaultPass(PassTemp *passTemp, ShadersStorage *_shaderStorage)
{
    Pass *pass = _shaderStorage->getDefaultPassesStorage()->getDefaultPass(passTemp->pass, passTemp->useClippingMap, passTemp->useNormalMap);
    return pass;
}

std::pair<int, char *> generateInitAdditionalDatas(TypePass _typePass, int _size, const char *_data,
                                                   const ModelLightShaderStorage *_modelLightShaderStorage)
{
    if (_typePass == TypePass::Surface)
    {
        char *additionalData = new char[2];
        additionalData[0] = _data[0];
        std::string nameModelLight;
        for (int i = 1; i < _size; i++)
        {
            nameModelLight += _data[i];
        }
        additionalData[1] = _modelLightShaderStorage->getIndexModelLight(nameModelLight.c_str());
        return std::make_pair(2, additionalData);
    }
    return std::make_pair(0, (char *)NULL);
}

ResultLoad GeneratorSyncLoadShader::generate(const PathToResource *define)
{
    ResultLoad result;
    const char *path = define->path;

    SDL_RWops *file = SDL_RWFromFile(path, "rb");
    if (file == NULL)
    {
        result.isAllNormal = false;
        result.resource = NULL;
        return result;
    }

    std::string name = readString(file);
    std::vector<PropertyTemp *> propertiesTemp = readProperties(file);
    ShaderSettings settings;
    readSettings(file, &settings);

    std::vector<PassTemp> passes;
    int countPasses = readInt(file);
    for (int i = 0; i < countPasses; i++)
    {
        passes.push_back(PassTemp());
        readPass(file, &(passes[i]));
    }

    file->close(file);

    for (int i = 0; i < (int)propertiesTemp.size(); i++)
    {
        if (propertiesTemp[i]->type == TypeProperty::Texture_P)
        {
            TexturePropertyT *prop = reinterpret_cast<TexturePropertyT *>(propertiesTemp[i]);
            if (prop->isHDRTexture)
            {
                prop->texture = reinterpret_cast<Texture *>(specialSystems->resourceLoader->loadResource(prop->defaultPath.c_str(), TypeResource::HDR_TEXTURE));
            }
            else
            {
                prop->texture = reinterpret_cast<Texture *>(specialSystems->resourceLoader->loadResource(prop->defaultPath.c_str(), TypeResource::TEXTURE));
            }
        }
        else if (propertiesTemp[i]->type == TypeProperty::CubeMap_P)
        {
            CubeMapPropertyT *prop = reinterpret_cast<CubeMapPropertyT *>(propertiesTemp[i]);
            prop->cubeMap = reinterpret_cast<CubeMap *>(specialSystems->resourceLoader->loadResource(prop->defaultPath.c_str(), TypeResource::CUBE_MAP));
        }
    }

    Shader *shader = new Shader(name.c_str());

    ShadersStorage *shadersStorage = specialSystems->graphicEngine->getMainSystems()->getShadersStorage();
    PropertiesRegistry *propertiesRegistry = shadersStorage->getPropertiesRegistry();
    UniformBlocksRegistry *uniformBlocksRegistry = shadersStorage->getUniformBlocksRegistry();
    const ModelLightShaderStorage *modelLightShaderStorage = &specialSystems->graphicEngine->getSettingsMainSystems()->modelLightShaderStorage;

    std::vector<std::string *> propertiesNames = std::vector<std::string *>();
    std::vector<TypeProperty> propertiesTypes = std::vector<TypeProperty>();
    for (int i = 0; i < (int)propertiesTemp.size(); i++)
    {
        propertiesNames.push_back(&(propertiesTemp[i]->name));
        propertiesTypes.push_back(propertiesTemp[i]->type);
    }

    shadersStorage->addNewShader(shader, propertiesTemp.size(), &propertiesNames, &propertiesTypes);

    std::vector<Property *> releaseProperties;
    generateReleaseProperties(&propertiesTemp, &releaseProperties, specialSystems);

    shader->setDefaultProperties(releaseProperties.size(), &(releaseProperties[0]));
    for (int i = 0; i < (int)propertiesTemp.size(); i++)
    {
        TogglePropertyT *toggle = reinterpret_cast<TogglePropertyT *>(propertiesTemp[i]);
        shader->setMaskToggle(releaseProperties[i]->getCodeData(), toggle->mask);
    }
    deleteTempProperties(&propertiesTemp);

    shader->setSettings(settings);
    for (int i = 0; i < (int)passes.size(); i++)
    {
        PassTemp *tpass = &(passes[i]);
        if (tpass->useDefaultPrograms)
        {
            Pass *pass = getDefaultPass(tpass, shadersStorage);
            if (pass != NULL)
            {
                shader->addPass(pass);
            }
        }
        else
        {
            Pass *pass = new Pass(tpass->name.c_str(), tpass->pass, tpass->queue, tpass->renderType, tpass->cullType,
                                  tpass->zTest, tpass->zWrite, tpass->generateInstancing);

            pass->setMasks(passes[i].maskUsingDefinesVertex, passes[i].maskUsingDefinesFragment);
            SetInitializedTextures setInitializedTextures = SetInitializedTextures();
            for (int i2 = 0; i2 < (int)passes[i].vertexPrograms.size(); i2++)
            {
                Program *program = new Program();
                fillProgramForPass(program, &(passes[i].vertexPrograms[i2]), &setInitializedTextures, uniformBlocksRegistry, propertiesRegistry, GL_VERTEX_SHADER);
                pass->connectProgram(program, GL_VERTEX_SHADER);
            }

            for (int i2 = 0; i2 < (int)passes[i].fragmentPrograms.size(); i2++)
            {
                Program *program = new Program();
                fillProgramForPass(program, &(passes[i].fragmentPrograms[i2]), &setInitializedTextures, uniformBlocksRegistry, propertiesRegistry, GL_FRAGMENT_SHADER);
                pass->connectProgram(program, GL_FRAGMENT_SHADER);
            }
            if (setInitializedTextures.wasErrorTry())
            {
                specialSystems->mainDebugOutput->debugError("Shader: %s, pass: %s, the programs try to use more textures, than possible.");
            }

            std::pair<int, char *> additionalData = generateInitAdditionalDatas(tpass->pass,
                                                                                tpass->sizeAdditionalData, &(tpass->additionalData[0]), modelLightShaderStorage);
            pass->initAdditionalData(additionalData.first, additionalData.second);
            shader->addPass(pass);
        }
    }

    result.isAllNormal = true;
    result.resource = shader;
    return result;
}

AsyncLoader *GeneratorAsyncLoadShader::generate(const PathToResource *define)
{
    AsyncLoader *loader = new ShaderAsyncLoader(define, ((AsyncLoadersFactory *)factory)->getAsyncLoaderBuffer(), specialSystems);
    return loader;
}

ShaderAsyncLoader::ShaderAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer, SpecialSystems *_specialSystems)
    : AsyncLoader(path, asyncLoadersBuffer), specialSystems(_specialSystems)
{
}

ShaderAsyncLoader::~ShaderAsyncLoader()
{
    if (tempProperties.size() != 0)
    {
        deleteTempProperties(&tempProperties);
    }
}

StatusState ShaderAsyncLoader::updateAsyncProcess()
{
    const char *c_path = path.path.c_str();

    SDL_RWops *file = SDL_RWFromFile(c_path, "rb");
    if (file == NULL)
    {
        return StatusState::SS_Failed;
    }

    name = readString(file);
    tempProperties = readProperties(file);
    readSettings(file, &settings);
    int countPasses = readInt(file);
    for (int i = 0; i < countPasses; i++)
    {
        tempPasses.push_back(PassTemp());
        readPass(file, &(tempPasses[i]));
    }

    file->close(file);

    for (int i = 0; i < (int)tempProperties.size(); i++)
    {
        if (tempProperties[i]->type == TypeProperty::Texture_P)
        {
            TexturePropertyT *text = reinterpret_cast<TexturePropertyT *>(tempProperties[i]);
            if (text->defaultPath.empty() == false)
            {
                PathToResource path;
                path.path = text->defaultPath.c_str();
                if (text->isHDRTexture == false)
                {
                    path.type = TypeResource::TEXTURE;
                    textures.push_back(std::make_pair(new TextureAsyncLoader(&path, asyncLoadersBuffer), text));
                }
                else
                {
                    path.type = TypeResource::HDR_TEXTURE;
                    textures.push_back(std::make_pair(new HDRTextureAsyncLoader(&path, asyncLoadersBuffer), text));
                }
            }
        }
        else if (tempProperties[i]->type == TypeProperty::CubeMap_P)
        {
            CubeMapPropertyT *cubeMap = reinterpret_cast<CubeMapPropertyT *>(tempProperties[i]);
            if (cubeMap->defaultPath.empty() == false)
            {
                PathToResource path;
                path.type = TypeResource::CUBE_MAP;
                path.path = cubeMap->defaultPath.c_str();
                textures.push_back(std::make_pair(new CubeMapAsyncLoader(&path, asyncLoadersBuffer), cubeMap));
            }
        }
    }

    int countLoaders = textures.size();
    loaders = new AsyncLoader *[textures.size()];
    for (int i = 0; i < countLoaders; i++)
    {
        loaders[i] = textures[i].first;
        initSubAsyncLoader(loaders[i]);
    }

    return StatusState::SS_Succeed;
}

StatusState ShaderAsyncLoader::updateSyncProcess()
{
    Shader *shader = new Shader(name.c_str());

    ShadersStorage *shadersStorage = specialSystems->graphicEngine->getMainSystems()->getShadersStorage();
    PropertiesRegistry *propertiesRegistry = shadersStorage->getPropertiesRegistry();
    UniformBlocksRegistry *uniformBlocksRegistry = shadersStorage->getUniformBlocksRegistry();
    const ModelLightShaderStorage *modelLightShaderStorage = &specialSystems->graphicEngine->getSettingsMainSystems()->modelLightShaderStorage;

    std::vector<std::string *> propertiesNames = std::vector<std::string *>();
    std::vector<TypeProperty> propertiesTypes = std::vector<TypeProperty>();
    for (int i = 0; i < (int)tempProperties.size(); i++)
    {
        propertiesNames.push_back(&(tempProperties[i]->name));
        propertiesTypes.push_back(tempProperties[i]->type);
    }

    shadersStorage->addNewShader(shader, tempProperties.size(), &propertiesNames, &propertiesTypes);

    for (int i = 0; i < (int)textures.size(); i++)
    {
        if (textures[i].second->type == TypeProperty::Texture_P)
        {
            ((TexturePropertyT *)textures[i].second)->texture = (Texture *)textures[i].first->getResource();
        }
        else if (textures[i].second->type == TypeProperty::CubeMap_P)
        {
            ((CubeMapPropertyT *)textures[i].second)->cubeMap = (CubeMap *)textures[i].first->getResource();
        }
    }
    std::vector<Property *> releaseProperties;
    generateReleaseProperties(&tempProperties, &releaseProperties, specialSystems);

    shader->setDefaultProperties(releaseProperties.size(), &(releaseProperties[0]));
    for (int i = 0; i < (int)tempProperties.size(); i++)
    {
        TogglePropertyT *toggle = reinterpret_cast<TogglePropertyT *>(tempProperties[i]);
        shader->setMaskToggle(releaseProperties[i]->getCodeData(), toggle->mask);
    }
    deleteTempProperties(&tempProperties);

    shader->setSettings(settings);
    for (int i = 0; i < (int)tempPasses.size(); i++)
    {
        PassTemp *tpass = &(tempPasses[i]);
        if (tpass->useDefaultPrograms)
        {
            Pass *pass = getDefaultPass(tpass, shadersStorage);
            if (pass != NULL)
            {
                shader->addPass(pass);
            }
        }
        else
        {
            Pass *pass = new Pass(tempPasses[i].name.c_str(), tempPasses[i].pass, tempPasses[i].queue, tempPasses[i].renderType, tpass->cullType,
                                  tempPasses[i].zTest, tempPasses[i].zWrite, tempPasses[i].generateInstancing);
            pass->setMasks(tempPasses[i].maskUsingDefinesVertex, tempPasses[i].maskUsingDefinesFragment);

            SetInitializedTextures setInitializedTextures = SetInitializedTextures();
            for (int i2 = 0; i2 < (int)tempPasses[i].vertexPrograms.size(); i2++)
            {
                Program *program = new Program();
                fillProgramForPass(program, &(tempPasses[i].vertexPrograms[i2]), &setInitializedTextures, uniformBlocksRegistry, propertiesRegistry, GL_VERTEX_SHADER);
                pass->connectProgram(program, GL_VERTEX_SHADER);
            }

            for (int i2 = 0; i2 < (int)tempPasses[i].fragmentPrograms.size(); i2++)
            {
                Program *program = new Program();
                fillProgramForPass(program, &(tempPasses[i].fragmentPrograms[i2]), &setInitializedTextures, uniformBlocksRegistry, propertiesRegistry, GL_FRAGMENT_SHADER);
                pass->connectProgram(program, GL_FRAGMENT_SHADER);
            }
            if (setInitializedTextures.wasErrorTry())
            {
                specialSystems->mainDebugOutput->debugError("Shader: %s, pass: %s, the programs try to use more textures, than possible.");
            }

            std::pair<int, char *> additionalData = generateInitAdditionalDatas(tpass->pass,
                                                                                tpass->sizeAdditionalData, &(tpass->additionalData[0]), modelLightShaderStorage);
            pass->initAdditionalData(additionalData.first, additionalData.second);
            shader->addPass(pass);
        }
    }

    resource = shader;
    return StatusState::SS_Succeed;
}