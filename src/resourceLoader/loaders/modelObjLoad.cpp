#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <MTH/vectors.h>

#include "resourceLoader/loaders/modelObjLoad.h"

static mth::vec3 getTanget(mth::vec3 pos1, mth::vec3 pos2, mth::vec3 pos3, mth::vec2 uv1, mth::vec2 uv2, mth::vec2 uv3)
{
    mth::vec3 edge1 = pos2 - pos1;
    mth::vec3 edge2 = pos3 - pos1;
    mth::vec2 deltaUV1 = uv2 - uv1;
    mth::vec2 deltaUV2 = uv3 - uv1;

    float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

    mth::vec3 tangent;
    tangent.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
    tangent.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
    tangent.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);

    return tangent;
}

static int loadDataFromFile(std::ifstream &file, std::vector<std::string> &names, std::list<std::vector<GLushort>> &index_pos,
                            std::list<std::vector<GLushort>> &index_uv, std::list<std::vector<GLushort>> &index_normal, std::vector<GLushort> *indexs,
                            std::vector<mth::vec3> *positions, std::vector<mth::vec2> *uvs,
                            std::vector<mth::vec3> *normals)
{
    float scale = 0.01f;

    std::stringstream ss;
    std::string line = "";
    std::string prefix = "";
    int countMeshes = 0;

    mth::vec3 pos;
    mth::vec2 uv;
    mth::vec3 normal;

    std::vector<GLushort> *current_indexes_pos;
    std::vector<GLushort> *current_indexes_uv;
    std::vector<GLushort> *current_indexes_normal;

    while (std::getline(file, line))
    {
        ss.clear();
        ss.str(line);
        prefix.clear();
        ss >> prefix;

        if (prefix == "#")
        {
        }
        else if (prefix == "o")
        {
            std::string name;
            ss >> name;
            names.push_back(name);
            countMeshes++;

            index_pos.push_back(std::vector<GLushort>());
            current_indexes_pos = &(index_pos.back());

            index_uv.push_back(std::vector<GLushort>());
            current_indexes_uv = &(index_uv.back());

            index_normal.push_back(std::vector<GLushort>());
            current_indexes_normal = &(index_normal.back());
        }
        else if (prefix == "s")
        {
        }
        else if (prefix == "v")
        {
            ss >> pos.x >> pos.y >> pos.z;
            positions->push_back(pos * scale);
        }
        else if (prefix == "vt")
        {
            ss >> uv.x >> uv.y;
            uvs->push_back(uv);
        }
        else if (prefix == "vn")
        {
            ss >> normal.x >> normal.y >> normal.z;
            normals->push_back(normal);
        }
        else if (prefix == "f")
        {
            int temp = 0;
            int count = 0;

            std::vector<GLushort> ip;
            std::vector<GLushort> iuv;
            std::vector<GLushort> in;
            while (ss >> temp)
            {
                if (count == 0)
                    ip.push_back(temp);
                if (count == 1)
                    iuv.push_back(temp);
                if (count == 2)
                    in.push_back(temp);

                if (ss.peek() == '/')
                {
                    ++count;
                    ss.ignore(1, '/');
                }

                if (ss.peek() == ' ')
                {
                    ++count;
                    ss.ignore(1, ' ');
                }

                if (count > 2)
                {
                    count = 0;
                }
            }
            if (ip.size() > 3)
            {
                current_indexes_pos->push_back(ip[2]);
                current_indexes_pos->push_back(ip[1]);
                current_indexes_pos->push_back(ip[0]);
                current_indexes_uv->push_back(iuv[2]);
                current_indexes_uv->push_back(iuv[1]);
                current_indexes_uv->push_back(iuv[0]);
                current_indexes_normal->push_back(in[2]);
                current_indexes_normal->push_back(in[1]);
                current_indexes_normal->push_back(in[0]);

                current_indexes_pos->push_back(ip[3]);
                current_indexes_pos->push_back(ip[2]);
                current_indexes_pos->push_back(ip[0]);
                current_indexes_uv->push_back(iuv[3]);
                current_indexes_uv->push_back(iuv[2]);
                current_indexes_uv->push_back(iuv[0]);
                current_indexes_normal->push_back(in[3]);
                current_indexes_normal->push_back(in[2]);
                current_indexes_normal->push_back(in[0]);
            }
            else
            {
                current_indexes_pos->push_back(ip[2]);
                current_indexes_pos->push_back(ip[1]);
                current_indexes_pos->push_back(ip[0]);
                current_indexes_uv->push_back(iuv[2]);
                current_indexes_uv->push_back(iuv[1]);
                current_indexes_uv->push_back(iuv[0]);
                current_indexes_normal->push_back(in[2]);
                current_indexes_normal->push_back(in[1]);
                current_indexes_normal->push_back(in[0]);
            }
        }
        else
        {
        }
    }
    return countMeshes;
}

static void fillBuffersForMesh(Vertex *verts, int count, std::vector<GLushort> *indexs, std::vector<mth::vec3> *positions,
                               std::vector<mth::vec2> *uvs, std::vector<mth::vec3> *tangent, std::vector<mth::vec3> *normals)
{
    bool t = false;
    int num = 0;

    for (int i2 = 0; i2 < count; i2++)
    {
        t = false;
        uint ct = positions->size();

        for (uint i3 = 0; i3 < ct; i3++)
        {
            if (verts[i2].normal == (*normals)[i3] && verts[i2].tangent == (*tangent)[i3] && verts[i2].uv == (*uvs)[i3] &&
                verts[i2].pos == (*positions)[i3])
            {
                num = i3;
                t = true;
                break;
            }
        }

        if (t)
        {
            indexs->push_back(num);
        }
        else
        {
            indexs->push_back(positions->size());
            positions->push_back(verts[i2].pos);
            uvs->push_back(verts[i2].uv);
            normals->push_back(verts[i2].normal);
            tangent->push_back(verts[i2].tangent);
        }
    }
}

ResultLoad GeneratorSyncLoadModelObj::generate(const PathToResource *define)
{
    ResultLoad result;
    std::ifstream file(define->path);
    Model *model = NULL;

    result.isAllNormal = false;
    result.resource = NULL;
    if (file.is_open())
    {
        std::vector<GLushort> *indexs = new std::vector<GLushort>();
        std::vector<mth::vec3> *positions = new std::vector<mth::vec3>();
        std::vector<mth::vec2> *uvs = new std::vector<mth::vec2>();
        std::vector<mth::vec3> *tangent = new std::vector<mth::vec3>();
        std::vector<mth::vec3> *normals = new std::vector<mth::vec3>();

        int countMeshes = 0;
        std::vector<std::string> names;

        std::vector<Vertex *> bufferVertexes;
        std::vector<uint> countIndexesInBuffers;

        std::list<std::vector<GLushort>> index_pos;
        std::list<std::vector<GLushort>> index_uv;
        std::list<std::vector<GLushort>> index_normal;

        /*std::vector<GLushort> *current_indexes_pos;
        std::vector<GLushort> *current_indexes_uv;
        std::vector<GLushort> *current_indexes_normal;*/

        mth::vec3 pos;
        mth::vec2 uv;
        mth::vec3 normal;

        countMeshes = loadDataFromFile(file, names, index_pos, index_uv, index_normal, indexs, positions, uvs, normals);
        file.close();

        for (int i = 0; i < countMeshes; i++)
        {
            auto c_indexes_pos = index_pos.begin();
            std::advance(c_indexes_pos, i);

            auto c_indexes_uv = index_uv.begin();
            std::advance(c_indexes_uv, i);

            auto c_indexes_normal = index_normal.begin();
            std::advance(c_indexes_normal, i);

            uint count = (*c_indexes_pos).size();
            Vertex *verts = new Vertex[count];

            bufferVertexes.push_back(verts);
            countIndexesInBuffers.push_back(count);

            for (uint i2 = 0; i2 < count; i2 = i2 + 3)
            {
                for (uint i3 = 0; i3 < 3; i3++)
                {
                    verts[i2 + i3].pos = (*positions)[(*c_indexes_pos)[i2 + i3] - 1];
                    verts[i2 + i3].uv = (*uvs)[(*c_indexes_uv)[i2 + i3] - 1];
                    verts[i2 + i3].normal = (*normals)[(*c_indexes_normal)[i2 + i3] - 1];
                }

                mth::vec3 tang = getTanget(verts[i2].pos, verts[i2 + 1].pos, verts[i2 + 2].pos, verts[i2].uv, verts[i2 + 1].uv, verts[i2 + 2].uv);

                verts[i2].tangent = tang;
                verts[i2 + 1].tangent = tang;
                verts[i2 + 2].tangent = tang;
            }
        }

        // its generate graphic buffer for each mesh
        std::vector<Mesh *> meshs;
        meshs.reserve(countMeshes);
        for (int i = 0; i < countMeshes; i++)
        {
            indexs->clear();
            positions->clear();
            uvs->clear();
            normals->clear();
            tangent->clear();

            fillBuffersForMesh(bufferVertexes[i], countIndexesInBuffers[i], indexs, positions, uvs, tangent, normals);
            Mesh *mesh = new Mesh(names[i], indexs, positions, uvs, normals, tangent);
            meshs.push_back(mesh);
        }

        model = new Model(meshs);

        std::for_each(bufferVertexes.begin(), bufferVertexes.end(), [](Vertex *vert)
                      { delete[] vert; });
        delete indexs;
        delete positions;
        delete uvs;
        delete normals;
        delete tangent;
        result.isAllNormal = true;
        result.resource = model;
    }
    return result;
}

AsyncLoader *GeneratorAsyncLoadModelObj::generate(const PathToResource *define)
{
    AsyncLoader *loader = new ModelObjAsyncLoader(define, ((AsyncLoadersFactory *)factory)->getAsyncLoaderBuffer());
    return loader;
}

ModelObjAsyncLoader::ModelObjAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer) : AsyncLoader(path, asyncLoadersBuffer)
{
}

ModelObjAsyncLoader::~ModelObjAsyncLoader()
{
}

StatusState ModelObjAsyncLoader::updateAsyncProcess()
{
    std::ifstream file(path.path);
    if (file.is_open())
    {
        std::list<std::vector<GLushort>> index_pos;
        std::list<std::vector<GLushort>> index_uv;
        std::list<std::vector<GLushort>> index_normal;
        std::vector<GLushort> indexs_t;
        std::vector<mth::vec3> positions_t;
        std::vector<mth::vec2> uvs_t;
        std::vector<mth::vec3> tangent_t;
        std::vector<mth::vec3> normals_t;

        std::vector<Vertex *> bufferVertexes;
        std::vector<uint> countIndexesInBuffers;

        int countMeshes = loadDataFromFile(file, names, index_pos, index_uv, index_normal, &indexs_t, &positions_t, &uvs_t, &normals_t);
        file.close();

        for (int i = 0; i < countMeshes; i++)
        {
            auto c_indexes_pos = index_pos.begin();
            std::advance(c_indexes_pos, i);

            auto c_indexes_uv = index_uv.begin();
            std::advance(c_indexes_uv, i);

            auto c_indexes_normal = index_normal.begin();
            std::advance(c_indexes_normal, i);

            uint count = (*c_indexes_pos).size();
            Vertex *verts = new Vertex[count];

            bufferVertexes.push_back(verts);
            countIndexesInBuffers.push_back(count);

            for (uint i2 = 0; i2 < count; i2 = i2 + 3)
            {
                for (uint i3 = 0; i3 < 3; i3++)
                {
                    verts[i2 + i3].pos = (positions_t)[(*c_indexes_pos)[i2 + i3] - 1];
                    verts[i2 + i3].uv = (uvs_t)[(*c_indexes_uv)[i2 + i3] - 1];
                    verts[i2 + i3].normal = (normals_t)[(*c_indexes_normal)[i2 + i3] - 1];
                }

                mth::vec3 tang = getTanget(verts[i2].pos, verts[i2 + 1].pos, verts[i2 + 2].pos, verts[i2].uv, verts[i2 + 1].uv, verts[i2 + 2].uv);

                verts[i2].tangent = tang;
                verts[i2 + 1].tangent = tang;
                verts[i2 + 2].tangent = tang;
            }
        }

        indexs.reserve(countMeshes);
        positions.reserve(countMeshes);
        uvs.reserve(countMeshes);
        tangent.reserve(countMeshes);
        normals.reserve(countMeshes);
        for (int i = 0; i < countMeshes; i++)
        {
            indexs.push_back(std::vector<GLushort>());
            std::vector<GLushort> *ind = &(indexs.back());
            positions.push_back(std::vector<mth::vec3>());
            std::vector<mth::vec3>*pos = &(positions.back());
            uvs.push_back(std::vector<mth::vec2>());
            std::vector<mth::vec2>*uv = &(uvs.back());
            tangent.push_back(std::vector<mth::vec3>());
            std::vector<mth::vec3>*tang = &(tangent.back());
            normals.push_back(std::vector<mth::vec3>());
            std::vector<mth::vec3>*normal = &(normals.back());

            fillBuffersForMesh(bufferVertexes[i], countIndexesInBuffers[i], ind, pos, uv, tang, normal);
            delete[] bufferVertexes[i];
        }
        return StatusState::SS_Succeed;
    }
    else
    {
        return StatusState::SS_Failed;
    }
}

StatusState ModelObjAsyncLoader::updateSyncProcess()
{
    std::vector<Mesh *> meshes;
    int count = names.size();
    for (int i = 0; i < count; i++)
    {
        Mesh *mesh = new Mesh(names[i], &(indexs[i]), &(positions[i]), &(uvs[i]), &(normals[i]), &(tangent[i]));
        meshes.push_back(mesh);
    }
    Model *model = new Model(meshes);
    resource = model;
    return StatusState::SS_Succeed;
}