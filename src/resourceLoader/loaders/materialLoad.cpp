#include "resourceLoader/loaders/materialLoad.h"

#include "renderEngine/resources/material/material.h"
#include "renderEngine/resources/shader/shader.h"
#include "renderEngine/graphicEngine.h"
#include "renderEngine/resources/shader/shadersStorage.h"
#include "resourceLoader/resourceLoader.h"
#include "resourceLoader/loaders/textureLoad.h"
#include "resourceLoader/loaders/hdrTextureLoad.h"
#include "resourceLoader/loaders/cubeMapLoad.h"

void pushInt(lua_State *L, const char *_name, Material *_material)
{
    lua_getfield(L, -1, "value");
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        _material->setInt(_name, lua_tointeger(L, -1));
    }
    lua_remove(L, -1);
}

void pushFloat(lua_State *L, const char *_name, Material *_material)
{
    lua_getfield(L, -1, "value");
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        _material->setFloat(_name, lua_tonumber(L, -1));
    }
    lua_remove(L, -1);
}

void pushToggle(lua_State *L, const char *_name, Material *_material)
{
    lua_getfield(L, -1, "value");
    if (lua_type(L, -1) == LUA_TBOOLEAN)
    {
        _material->setToggle(_name, lua_toboolean(L, -1));
    }
    lua_remove(L, -1);
}

void pushColor(lua_State *L, const char *_name, Material *_material)
{
    mth::col color;
    float r = 0;
    float g = 0;
    float b = 0;
    float a = 0;
    lua_getfield(L, -1, "r");
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        r = lua_tonumber(L, -1);
    }
    lua_remove(L, -1);
    lua_getfield(L, -1, "g");
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        g = lua_tonumber(L, -1);
    }
    lua_remove(L, -1);
    lua_getfield(L, -1, "b");
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        b = lua_tonumber(L, -1);
    }
    lua_remove(L, -1);
    lua_getfield(L, -1, "a");
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        a = lua_tonumber(L, -1);
    }
    lua_remove(L, -1);
    color.setColorByte(r, g, b, a);
    _material->setColor(_name, &color);
}

void pushVector(lua_State *L, const char *_name, Material *_material)
{
    mth::vec4 vec;
    lua_getfield(L, -1, "x");
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        vec.x = lua_tonumber(L, -1);
    }
    lua_remove(L, -1);
    lua_getfield(L, -1, "y");
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        vec.y = lua_tonumber(L, -1);
    }
    lua_remove(L, -1);
    lua_getfield(L, -1, "z");
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        vec.z = lua_tonumber(L, -1);
    }
    lua_remove(L, -1);
    lua_getfield(L, -1, "w");
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        vec.w = lua_tonumber(L, -1);
    }
    lua_remove(L, -1);
    _material->setVector(_name, &vec);
}

void pushSyncTexture(lua_State *L, const char *_name, Material *_material, SpecialSystems *_specialSystems)
{
    Texture *texture = NULL;

    bool isLoadStandardTexture = false;
    lua_getfield(L, -1, FIELD_PATH_FOR_STANDARD_TEXTURE);
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        const char *_path = lua_tostring(L, -1);
        texture = reinterpret_cast<Texture *>(_specialSystems->resourceLoader->loadResource(_path, TypeResource::TEXTURE));
        isLoadStandardTexture = true;
    }
    lua_remove(L, -1);

    if (!isLoadStandardTexture)
    {
        lua_getfield(L, -1, FIELD_PATH_FOR_HDR_TEXTURE);
        if (lua_type(L, -1) == LUA_TSTRING)
        {
            const char *_path = lua_tostring(L, -1);
            texture = reinterpret_cast<Texture *>(_specialSystems->resourceLoader->loadResource(_path, TypeResource::HDR_TEXTURE));
            isLoadStandardTexture = true;
        }
        lua_remove(L, -1);
    }

    _material->setTexture(_name, texture);
}

void pushSyncCubeMap(lua_State *L, const char *_name, Material *_material, SpecialSystems *_specialSystems)
{
    CubeMap *cubeMap = NULL;

    lua_getfield(L, -1, FIELD_PATH_FOR_CUBE_MAP);
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        const char *_path = lua_tostring(L, -1);
        cubeMap = reinterpret_cast<CubeMap *>(_specialSystems->resourceLoader->loadResource(_path, TypeResource::CUBE_MAP));
    }
    lua_remove(L, -1);

    _material->setCubeMap(_name, cubeMap);
}

void pushASyncTexture(const char *_name, Material *_material, std::vector<std::pair<std::string, AsyncLoader *>> *_loadersForParameter)
{
    Texture *texture = NULL;

    int count = _loadersForParameter->size();
    for (int i = 0; i < count; i++)
    {
        if (std::strcmp(_name, _loadersForParameter->at(i).first.c_str()) == 0)
        {
            texture = reinterpret_cast<Texture *>(_loadersForParameter->at(i).second->getResource());
            _material->setTexture(_name, texture);
            return;
        }
    }
}

void pushASyncCubeMap(const char *_name, Material *_material, std::vector<std::pair<std::string, AsyncLoader *>> *_loadersForParameter)
{
    CubeMap *cubeMap = NULL;

    int count = _loadersForParameter->size();
    for (int i = 0; i < count; i++)
    {
        if (std::strcmp(_name, _loadersForParameter->at(i).first.c_str()) == 0)
        {
            cubeMap = reinterpret_cast<CubeMap *>(_loadersForParameter->at(i).second->getResource());
            _material->setCubeMap(_name, cubeMap);
            return;
        }
    }
}

ResultLoad GeneratorSyncLoadMaterial::generate(const PathToResource *define)
{
    ResultLoad result;
    result.isAllNormal = false;
    result.resource = NULL;
    lua_State *L = luaL_newstate();
    int error = luaL_dofile(L, define->path);

    if (error)
    {
        std::cout << "Load material error: " << lua_tostring(L, -1) << std::endl; 
        lua_close(L);
        return result;
    }

    std::string name = "Material";
    lua_getglobal(L, "Name");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        name = std::string(lua_tostring(L, -1));
    }
    lua_remove(L, -1);

    bool generateInstancing = true;
    lua_getglobal(L, "GenerateInstancing");
    if (lua_type(L, -1) == LUA_TBOOLEAN)
    {
        generateInstancing = lua_toboolean(L, -1);
    }
    lua_remove(L, -1);

    std::string shaderName = "";
    lua_getglobal(L, "Shader");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        shaderName = std::string(lua_tostring(L, -1));
    }
    lua_remove(L, -1);

    Shader *shader = specialSystems->graphicEngine->getMainSystems()->getShadersStorage()->findShader(shaderName.c_str());
    if (shader == NULL)
    {
        lua_close(L);
        return result;
    }
    Material *material = new Material(shader);

    material->setName(name.c_str());
    material->setGenerateInstancing(generateInstancing);

    lua_getglobal(L, "Properties");
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        lua_remove(L, -1);
    }
    else
    {
        lua_pushnil(L);
        while (lua_next(L, -2))
        {
            if (lua_type(L, -1) != LUA_TTABLE)
            {
                lua_pop(L, 1);
                continue;
            }

            std::string nameProperty = lua_tostring(L, -2);

            TypeProperty type = material->getTypeProperty(nameProperty.c_str());
            if (type == TypeProperty::Int_P)
            {
                pushInt(L, nameProperty.c_str(), material);
            }
            else if (type == TypeProperty::Float_P)
            {
                pushFloat(L, nameProperty.c_str(), material);
            }
            else if (type == TypeProperty::Toggle_P)
            {
                pushToggle(L, nameProperty.c_str(), material);
            }
            else if (type == TypeProperty::Vector_P)
            {
                pushVector(L, nameProperty.c_str(), material);
            }
            else if (type == TypeProperty::Color_P)
            {
                pushColor(L, nameProperty.c_str(), material);
            }
            else if (type == TypeProperty::Texture_P)
            {
                pushSyncTexture(L, nameProperty.c_str(), material, specialSystems);
            }
            else if (type == TypeProperty::CubeMap_P)
            {
                pushSyncCubeMap(L, nameProperty.c_str(), material, specialSystems);
            }

            lua_pop(L, 1);
        }
        lua_remove(L, -1);
    }

    lua_close(L);

    result.isAllNormal = true;
    result.resource = material;
    return result;
}

AsyncLoader *GeneratorAsyncLoadMaterial::generate(const PathToResource *define)
{
    AsyncLoader *loader = new MaterialAsyncLoader(define, ((AsyncLoadersFactory *)factory)->getAsyncLoaderBuffer(), specialSystems);
    return loader;
}

MaterialAsyncLoader::MaterialAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer, SpecialSystems *_specialSystems)
    : AsyncLoader(path, asyncLoadersBuffer), specialSystems(_specialSystems)
{
    L = luaL_newstate();
}

MaterialAsyncLoader::~MaterialAsyncLoader()
{
    lua_close(L);
}

StatusState MaterialAsyncLoader::updateAsyncProcess()
{
    int error = luaL_dofile(L, path.path.c_str());

    if (error)
    {
        return StatusState::SS_Failed;
    }

    name = "Material";
    lua_getglobal(L, "Name");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        name = std::string(lua_tostring(L, -1));
    }
    lua_remove(L, -1);

    generateInstancing = true;
    lua_getglobal(L, "GenerateInstancing");
    if (lua_type(L, -1) == LUA_TBOOLEAN)
    {
        generateInstancing = lua_toboolean(L, -1);
    }
    lua_remove(L, -1);

    shaderName = "";
    lua_getglobal(L, "Shader");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        shaderName = std::string(lua_tostring(L, -1));
    }
    lua_remove(L, -1);

    lua_getglobal(L, "Properties");
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        lua_remove(L, -1);
    }
    else
    {
        lua_pushnil(L);
        while (lua_next(L, -2))
        {
            if (lua_type(L, -1) != LUA_TTABLE)
            {
                lua_pop(L, 1);
                continue;
            }
            const char *_nameProperty = lua_tostring(L, -2);

            lua_getfield(L, -1, FIELD_PATH_FOR_STANDARD_TEXTURE);
            if (lua_type(L, -1) == LUA_TSTRING)
            {
                const char *_path = lua_tostring(L, -1);
                PathToResource pathToTexture = PathToResource();
                pathToTexture.path = _path;
                pathToTexture.type = TypeResource::TEXTURE;
                loadersForParameter.push_back(std::make_pair(_nameProperty, new TextureAsyncLoader(&pathToTexture, asyncLoadersBuffer)));
            }
            lua_pop(L, 1);

            lua_getfield(L, -1, FIELD_PATH_FOR_HDR_TEXTURE);
            if (lua_type(L, -1) == LUA_TSTRING)
            {
                const char *_path = lua_tostring(L, -1);
                PathToResource pathToTexture = PathToResource();
                pathToTexture.path = _path;
                pathToTexture.type = TypeResource::HDR_TEXTURE;
                loadersForParameter.push_back(std::make_pair(_nameProperty, new HDRTextureAsyncLoader(&pathToTexture, asyncLoadersBuffer)));
            }
            lua_pop(L, 1);
            
            lua_getfield(L, -1, FIELD_PATH_FOR_CUBE_MAP);
            if (lua_type(L, -1) == LUA_TSTRING)
            {
                const char *_path = lua_tostring(L, -1);
                PathToResource pathToTexture = PathToResource();
                pathToTexture.path = _path;
                pathToTexture.type = TypeResource::CUBE_MAP;
                loadersForParameter.push_back(std::make_pair(_nameProperty, new CubeMapAsyncLoader(&pathToTexture, asyncLoadersBuffer)));
            }
            lua_pop(L, 2);
        }
        lua_remove(L, -1);
    }

    loaders = new AsyncLoader *[loadersForParameter.size()];
    for (int i = 0; i < (int)loadersForParameter.size(); i++)
    {
        loaders[i] = loadersForParameter[i].second;
        initSubAsyncLoader(loaders[i]);
    }
    return StatusState::SS_Succeed;
}

StatusState MaterialAsyncLoader::updateSyncProcess()
{
    Shader *shader = specialSystems->graphicEngine->getMainSystems()->getShadersStorage()->findShader(shaderName.c_str());

    if (shader == NULL)
    {
        return StatusState::SS_Failed;
    }

    Material *material = new Material(shader);
    material->setName(name.c_str());
    material->setGenerateInstancing(generateInstancing);

    lua_getglobal(L, "Properties");
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        lua_remove(L, -1);
    }
    else
    {
        lua_pushnil(L);
        while (lua_next(L, -2))
        {
            if (lua_type(L, -1) != LUA_TTABLE)
            {
                lua_pop(L, 1);
                continue;
            }

            std::string nameProperty = lua_tostring(L, -2);

            TypeProperty type = material->getTypeProperty(nameProperty.c_str());
            if (type == TypeProperty::Int_P)
            {
                pushInt(L, nameProperty.c_str(), material);
            }
            else if (type == TypeProperty::Float_P)
            {
                pushFloat(L, nameProperty.c_str(), material);
            }
            else if (type == TypeProperty::Toggle_P)
            {
                pushToggle(L, nameProperty.c_str(), material);
            }
            else if (type == TypeProperty::Vector_P)
            {
                pushVector(L, nameProperty.c_str(), material);
            }
            else if (type == TypeProperty::Color_P)
            {
                pushColor(L, nameProperty.c_str(), material);
            }
            else if (type == TypeProperty::Texture_P)
            {
                pushASyncTexture(nameProperty.c_str(), material, &loadersForParameter);
            }
            else if (type == TypeProperty::CubeMap_P)
            {
                pushASyncCubeMap(nameProperty.c_str(), material, &loadersForParameter);
            }

            lua_pop(L, 1);
        }
        lua_remove(L, -1);
    }

    resource = material;
    return StatusState::SS_Succeed;
}