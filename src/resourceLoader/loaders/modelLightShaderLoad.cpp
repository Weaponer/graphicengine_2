#define GL_GLEXT_PROTOTYPES
#include <SDL2/SDL.h>
#include <GL/glu.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <string>

#include "resourceLoader/loaders/modelLightShaderLoad.h"
#include "renderEngine/resources/modelLightShader/modelLightShader.h"
#include "renderEngine/mainFrameBuffers/GBuffer.h"

#include "engineBase/configManager/configTable.h"
#include "renderEngine/settings/graphicConfig.h"

static bool readModelLightShaderFile(const char *_path, std::string &_name,
                                     int &_useAdditionalLayers, std::vector<std::string> &_libs,
                                     std::string &_vertexProgram, std::string &_program)
{
    lua_State *L = luaL_newstate();

    int error = luaL_dofile(L, _path);

    if (error)
    {
        lua_close(L);
        return false;
    }

    lua_getglobal(L, "Shader");
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        lua_close(L);
        return false;
    }

    bool hasName = false;
    bool hasLibs = false;
    bool hasVertexProgram = false;
    bool hasProgram = false;

    lua_getfield(L, -1, "Name");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        _name = lua_tostring(L, -1);
        hasName = true;
    }
    lua_remove(L, -1);

    _useAdditionalLayers = 0;
    for (int i = 0; i < G_BUFFER_COUNT_ADDITIONAL_BUFFERS; i++)
    {
        std::string nameField = std::string("AdditionalLayer_") + std::to_string(i + 1);
        lua_getfield(L, -1, nameField.c_str());
        if (lua_type(L, -1) == LUA_TBOOLEAN)
        {
            bool use = lua_toboolean(L, -1);
            if (use)
            {
                _useAdditionalLayers = _useAdditionalLayers | (1 << i);
            }
        }
        lua_remove(L, -1);
    }

    lua_getfield(L, -1, "Vertex");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        hasVertexProgram = true;
        _vertexProgram = lua_tostring(L, -1);
    }
    lua_remove(L, -1);

    lua_getfield(L, -1, "Program");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        hasProgram = true;
        _program = lua_tostring(L, -1);
    }
    lua_remove(L, -1);

    lua_getfield(L, -1, "Libs");
    if (lua_type(L, -1) == LUA_TTABLE)
    {
        hasLibs = true;
    }
    if (hasLibs)
    {
        lua_pushnil(L);
        while (lua_next(L, -2))
        {
            if (lua_type(L, -1) == LUA_TSTRING)
            {
                const char *_str = lua_tostring(L, -1);
                if (std::find(_libs.begin(), _libs.end(), std::string(_str)) == _libs.end())
                {
                    _libs.push_back(std::string(_str));
                }
            }
            lua_pop(L, 1);
        }
        lua_pop(L, 1);
    }
    lua_remove(L, -1);

    lua_close(L);

    if (!hasName || !hasVertexProgram || !hasProgram)
    {
        return false;
    }

    return true;
}

static std::string readAllFile(const std::string *_path)
{
    SDL_RWops *file = SDL_RWFromFile(_path->c_str(), "r");
    if (file == NULL)
    {
        printf("Model light shader loader: file %s don't open.\n", _path->c_str());
        return std::string("");
    }
    file->seek(file, 0, RW_SEEK_SET);

    int size = file->size(file);
    char *mem = new char[size + 1];
    mem[size] = 0;
    int pos = 0;
    while (size)
    {
        int count = file->read(file, mem + pos, sizeof(char), size);
        pos += count;
        size -= count;
    }
    std::string data = std::string(mem);
    delete mem;
    file->close(file);
    return data;
}

static std::string generateResultProgram(GraphicConfig *graphicConfig, int _useAdditionalLayers, const std::vector<std::string> &_libs, const std::string &_program)
{
    std::string resultProgram = graphicConfig->getGraphicCompilerTable().toString("VersionLine");

    for (int i = 0; i < G_BUFFER_COUNT_ADDITIONAL_BUFFERS; i++)
    {
        if ((_useAdditionalLayers & (1 << i)) != 0)
        {
            resultProgram += "#define " + std::string(graphicConfig->getGraphicCompilerTable().toString("DefineUseAdditionaLayer")) + std::to_string(i + 1) + "\n";
        }
    }

    auto end = _libs.end();
    for (auto i = _libs.begin(); i != end; ++i)
    {
        resultProgram += "\n" + readAllFile(&(*i));
    }
    resultProgram += "\n" + readAllFile(&_program);
    return resultProgram;
}

ResultLoad GeneratorSyncLoadModelLightShader::generate(const PathToResource *define)
{
    ResultLoad result;
    result.isAllNormal = false;
    result.resource = NULL;
    ModelLightShader *modelLightShader = NULL;

    printf("open model light\n");

    std::string name;
    int useAdditionalLayers = 0;
    std::vector<std::string> libs;
    std::string vertexProgram;
    std::string program;

    if (!readModelLightShaderFile(define->path, name, useAdditionalLayers, libs, vertexProgram, program))
    {
        return result;
    }
    std::string vertexCompileProg = generateResultProgram(specialSystems->graphicConfig, useAdditionalLayers, std::vector<std::string>(), vertexProgram);
    std::string compileProg = generateResultProgram(specialSystems->graphicConfig, useAdditionalLayers, libs, program);

    modelLightShader = new ModelLightShader(name.c_str(), compileProg.c_str(), vertexCompileProg.c_str(), useAdditionalLayers);
    result.resource = modelLightShader;
    result.isAllNormal = true;
    return result;
}

AsyncLoader *GeneratorAsyncLoadModelLightShader::generate(const PathToResource *define)
{
    AsyncLoader *loader = new ModelLightShaderAsyncLoader(define, ((AsyncLoadersFactory *)factory)->getAsyncLoaderBuffer(), specialSystems);
    return loader;
}

ModelLightShaderAsyncLoader::ModelLightShaderAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer, SpecialSystems *_specialSystems)
    : AsyncLoader(path, asyncLoadersBuffer), specialSystems(_specialSystems)
{
}

ModelLightShaderAsyncLoader::~ModelLightShaderAsyncLoader()
{
}

StatusState ModelLightShaderAsyncLoader::updateAsyncProcess()
{
    std::fstream file(path.path);
    if (!file.is_open())
    {
        return StatusState::SS_Failed;
    }

    printf("open model light\n");
    if (!readModelLightShaderFile(path.path.c_str(), name, useAdditionalLayers, libs, vertexProgram, program))
    {
        return StatusState::SS_Failed;
    }
    finalVertexProgram = generateResultProgram(specialSystems->graphicConfig, useAdditionalLayers, std::vector<std::string>(), vertexProgram);
    finalProgram = generateResultProgram(specialSystems->graphicConfig, useAdditionalLayers, libs, program);
    return StatusState::SS_Succeed;
}

StatusState ModelLightShaderAsyncLoader::updateSyncProcess()
{
    ModelLightShader *modelLightShader = new ModelLightShader(name.c_str(), finalProgram.c_str(), finalVertexProgram.c_str(), useAdditionalLayers);
    resource = modelLightShader;
    return StatusState::SS_Succeed;
}