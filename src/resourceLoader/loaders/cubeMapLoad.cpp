#include "resourceLoader/loaders/cubeMapLoad.h"
#include "renderEngine/resources/cubeMap.h"

bool fillSurfases(lua_State *L, std::array<SDL_Surface *, 6> &_surfaces)
{
    std::string paths[6];

    const char *sides[6] = {"forward", "back", "up", "down", "left", "right"};
    for (int i = 0; i < 6; i++)
    {
        lua_getglobal(L, sides[i]);
        if (lua_type(L, -1) != LUA_TSTRING)
        {
            paths[i] = "";
            continue;
        }
        paths[i] = lua_tostring(L, -1);

        lua_remove(L, -1);
    }

    bool nonLoad = false;
    for (int i = 0; i < 6; i++)
    {
        _surfaces[i] = IMG_Load(paths[i].c_str());
        if (_surfaces[i] == NULL)
        {
            nonLoad = true;
            break;
        }
    }

    if (nonLoad)
    {
        for (int i = 0; i < 6; i++)
        {
            if (_surfaces[i] == NULL)
            {
                break;
            }
            SDL_FreeSurface(_surfaces[i]);
            _surfaces[i] = NULL;
        }
        return false;
    }
    return true;
}

ResultLoad GeneratorSyncLoadCubeMap::generate(const PathToResource *define)
{
    ResultLoad result;
    result.isAllNormal = false;
    result.resource = NULL;

    lua_State *L = luaL_newstate();
    int error = luaL_dofile(L, define->path);

    if (error)
    {
        lua_close(L);
        return result;
    }

    std::array<SDL_Surface *, 6> surfaces;
    if (fillSurfases(L, surfaces))
    {
        result.isAllNormal = true;
        result.resource = new CubeMap(surfaces);
    }

    lua_close(L);
    return result;
}

AsyncLoader *GeneratorAsyncLoadCubeMap::generate(const PathToResource *define)
{
    AsyncLoader *loader = new CubeMapAsyncLoader(define, ((AsyncLoadersFactory *)factory)->getAsyncLoaderBuffer());
    return loader;
}

CubeMapAsyncLoader::CubeMapAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer)
    : AsyncLoader(path, asyncLoadersBuffer)
{
    for (int i = 0; i < 6; i++)
    {
        surfaces[i] = NULL;
    }

    L = luaL_newstate();
}

CubeMapAsyncLoader::~CubeMapAsyncLoader()
{
    for (int i = 0; i < 6; i++)
    {
        if (surfaces[i] != NULL)
        {
            SDL_FreeSurface(surfaces[i]);
        }
    }

    lua_close(L);
}

StatusState CubeMapAsyncLoader::updateAsyncProcess()
{
    lua_State *L = luaL_newstate();
    int error = luaL_dofile(L, path.path.c_str());

    if (error)
    {
        return StatusState::SS_Failed;
    }

    if (fillSurfases(L, surfaces))
    {
        return StatusState::SS_Succeed;
    }
    else
    {
        return StatusState::SS_Failed;
    }
}

StatusState CubeMapAsyncLoader::updateSyncProcess()
{
    resource = new CubeMap(surfaces);
    for (int i = 0; i < 6; i++)
    {
        surfaces[i] = NULL;
    }
    return StatusState::SS_Succeed;
}