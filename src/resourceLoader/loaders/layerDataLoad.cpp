#include "layerManager/layer.h"
#include "resourceLoader/loaders/layerDataLoad.h"
/*
static void stackDump(lua_State *L)
{
    printf("----stack dump----\n");
    int top = lua_gettop(L);
    for (int i = 1; i <= top; i++)
    {
        int type = lua_type(L, i);
        if (LUA_TSTRING == type)
        {
            printf("%s", lua_tostring(L, i));
        }
        else if (LUA_TBOOLEAN == type)
        {
            printf(lua_toboolean(L, i) ? "true" : "false");
        }
        else if (LUA_TNUMBER == type)
        {
            printf("%g", lua_tonumber(L, i));
        }
        else
        {
            printf("%s", lua_typename(L, type));
        }
        printf("\n");
    }
    printf("\n");
}*/

static int getLayersFromFile(lua_State *L, LayerData *data)
{
    lua_getglobal(L, LAYERS_V);
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        return 1;
    }

    lua_pushnil(L);
    int count = MAX_COUNT_LAYERS;
    while (lua_next(L, -2) != 0 && count > 0)
    {
        if (lua_type(L, -1) == LUA_TSTRING)
        {
            const char *str = lua_tostring(L, -1);
            data->layers.push_back(std::string(str));
            count--;
        }
        lua_pop(L, 1);
    }
    lua_settop(L, 0);
    return 0;
}

static int getPhysicsContactsFromFile(lua_State *L, LayerData *data)
{
    lua_getglobal(L, PHYSICS_CONTACTS_V);
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        return 1;
    }

    std::vector<std::string> physicsContacts;

    lua_pushnil(L);
    while (lua_next(L, -2) != 0)
    {
        if (lua_type(L, -1) == LUA_TSTRING)
        {
            const char *str = lua_tostring(L, -1);
            physicsContacts.push_back(str);
        }
        else if (physicsContacts.size() % 2 != 0)
        {
            physicsContacts.pop_back();
        }
        lua_pop(L, 1);
    }

    for (int i = 0; i < (int)physicsContacts.size(); i += 2)
    {
        const char *name1 = physicsContacts[i].c_str();
        const char *name2 = physicsContacts[i + 1].c_str();
        int f1 = -1;
        int f2 = -1;
        for (int i = 0; i < (int)data->layers.size(); i++)
        {
            if (strcmp(name1, data->layers[i].c_str()) == 0)
            {
                f1 = i;
            }
            if (strcmp(name2, data->layers[i].c_str()) == 0)
            {
                f2 = i;
            }
        }

        if (f1 == -1 || f2 == -1)
        {
            continue;
        }
        data->physicsEnable.push_back(std::make_pair(f1, f2));
    }
    lua_settop(L, 0);
    return 0;
}

ResultLoad GeneratorSyncLoadLayerData::generate(const PathToResource *define)
{
    ResultLoad result;
    result.isAllNormal = false;
    result.resource = NULL;
    lua_State *L = luaL_newstate();

    bool errors = luaL_dofile(L, define->path);
    if (errors)
    {

        //stackDump(L);
        lua_close(L);
        return result;
    }

    LayerData *data = new LayerData();
    int error = getLayersFromFile(L, data);
    if (error)
    {
        lua_close(L);
        delete data;
        return result;
    }
    getPhysicsContactsFromFile(L, data);

    lua_close(L);

    result.isAllNormal = true;
    result.resource = data;
    return result;
}

AsyncLoader *GeneratorAsyncLoadLayerData::generate(const PathToResource *define)
{
    AsyncLoader *loader = new LayerDataAsyncLoader(define, ((AsyncLoadersFactory *)factory)->getAsyncLoaderBuffer());
    return loader;
}

LayerDataAsyncLoader::LayerDataAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer) : AsyncLoader(path, asyncLoadersBuffer)
{
    data = NULL;
    L = luaL_newstate();
}

LayerDataAsyncLoader::~LayerDataAsyncLoader()
{
    lua_close(L);
    if (data != NULL)
    {
        delete data;
    }
}

StatusState LayerDataAsyncLoader::updateAsyncProcess()
{
    data = new LayerData();
    bool errors = luaL_dofile(L, path.path.c_str());
    if (errors)
    {
        return StatusState::SS_Failed;
    }
    int error = getLayersFromFile(L, data);
    if (error)
    {
        return StatusState::SS_Failed;
    }
    getPhysicsContactsFromFile(L, data);
    return StatusState::SS_Succeed;
}

StatusState LayerDataAsyncLoader::updateSyncProcess()
{
    resource = data;
    data = NULL;
    return StatusState::SS_Succeed;
}