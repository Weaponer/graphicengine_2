#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>

#include "resourceLoader/loaders/scriptLoad.h"
#include "scriptEngine/base/script.h"

ResultLoad GeneratorSyncLoadScript::generate(const PathToResource *define)
{
    ResultLoad result;

    SDL_RWops *ofile = SDL_RWFromFile(define->path, "rb");
    if (ofile == NULL)
    {
        result.isAllNormal = false;
        return result;
    }
    int leng = ofile->size(ofile);
    char *data = (char *)malloc(leng + 1);
    int countRead = 0;
    if (countRead != leng)
    {
        countRead += ofile->read(ofile, data + countRead, sizeof(char), leng - countRead);
    }
    ofile->close(ofile);
    data[countRead] = 0;

    Script *script = new Script(define->path, data);
    result.isAllNormal = true;
    result.resource = script;
    return result;
}

AsyncLoader *GeneratorAsyncLoadScript::generate(const PathToResource *define)
{
    AsyncLoader *loader = new ScriptAsyncLoader(define, ((AsyncLoadersFactory *)factory)->getAsyncLoaderBuffer());
    return loader;
}

ScriptAsyncLoader::ScriptAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer) : AsyncLoader(path, asyncLoadersBuffer)
{
    data = NULL;
}

ScriptAsyncLoader::~ScriptAsyncLoader()
{
    if (data != NULL)
    {
        free(data);
    }
}

StatusState ScriptAsyncLoader::updateAsyncProcess()
{
    SDL_RWops *ofile = SDL_RWFromFile(path.path.c_str(), "rb");
    if(ofile == NULL)
    {
        return StatusState::SS_Failed;
    }
    int leng = ofile->size(ofile);
    data = (char *)malloc(leng + 1);
    int countRead = 0;
    if (countRead != leng)
    {
        countRead += ofile->read(ofile, data + countRead, sizeof(char), leng - countRead);
    }
    ofile->close(ofile);
    data[countRead] = 0;
    return StatusState::SS_Succeed;
}

StatusState ScriptAsyncLoader::updateSyncProcess()
{
    resource = new Script(path.path.c_str(), data);
    data = NULL;
    return StatusState::SS_Succeed;
}