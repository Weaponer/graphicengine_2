#define GL_GLEXT_PROTOTYPES
#define _DEBUG
#include <SDL2/SDL.h>
#include <GL/glu.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <string>

#include "resourceLoader/loaders/tshaderLoad.h"
#include "tshader.h"

#include "engineBase/configManager/configTable.h"
#include "renderEngine/settings/graphicConfig.h"

static bool readTSHFile(const char *_path, std::string &_name, GLenum &_type, std::vector<std::string> &_libs, std::string &_program)
{
    lua_State *L = luaL_newstate();
    lua_pushinteger(L, GL_VERTEX_SHADER);
    lua_setglobal(L, VERTEX_P);
    lua_pushinteger(L, GL_FRAGMENT_SHADER);
    lua_setglobal(L, FRAGMENT_P);
    lua_pushinteger(L, GL_COMPUTE_SHADER);
    lua_setglobal(L, COMPUT_P);

    int error = luaL_dofile(L, _path);

    if (error)
    {
        lua_close(L);
        return false;
    }

    lua_getglobal(L, "Shader");
    if (lua_type(L, -1) != LUA_TTABLE)
    {
        lua_close(L);
        return false;
    }

    bool hasName = false;
    bool hasType = false;
    bool hasLibs = false;
    bool hasProgram = false;

    lua_getfield(L, -1, "Name");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        _name = lua_tostring(L, -1);
        hasName = true;
    }
    lua_remove(L, -1);

    lua_getfield(L, -1, "Type");
    if (lua_type(L, -1) == LUA_TNUMBER)
    {
        _type = lua_tointeger(L, -1);
        hasType = true;
    }
    lua_remove(L, -1);

    lua_getfield(L, -1, "Program");
    if (lua_type(L, -1) == LUA_TSTRING)
    {
        hasProgram = true;
        _program = lua_tostring(L, -1);
    }
    lua_remove(L, -1);

    lua_getfield(L, -1, "Libs");
    if (lua_type(L, -1) == LUA_TTABLE)
    {
        hasLibs = true;
    }
    if (hasLibs)
    {
        lua_pushnil(L);
        while (lua_next(L, -2))
        {
            if (lua_type(L, -1) == LUA_TSTRING)
            {
                const char *_str = lua_tostring(L, -1);
                if (std::find(_libs.begin(), _libs.end(), std::string(_str)) == _libs.end())
                {
                    _libs.push_back(std::string(_str));
                }
            }
            lua_pop(L, 1);
        }
        lua_pop(L, 1);
    }
    lua_remove(L, -1);

    lua_close(L);

    if (!hasName || !hasProgram || !hasType)
    {
        return false;
    }

    return true;
}

static std::string readAllFile(const std::string *_path)
{
    SDL_RWops *file = SDL_RWFromFile(_path->c_str(), "r");
    if (file == NULL)
    {
        printf("Technical shader loader: file %s don't open.\n", _path->c_str());
        return std::string("");
    }
    file->seek(file, 0, RW_SEEK_SET);

    int size = file->size(file);
    char *mem = new char[size + 1];
    mem[size] = 0;
    int pos = 0;
    while (size)
    {
        int count = file->read(file, mem + pos, sizeof(char), size);
        pos += count;
        size -= count;
    }
    std::string data = std::string(mem);
    delete mem;
    file->close(file);
    return data;
}

static std::string generateResultProgram(std::string _versionLine, const std::vector<std::string> &_libs, const std::string &_program)
{
    std::string resultProgram = _versionLine;

    auto end = _libs.end();
    for (auto i = _libs.begin(); i != end; ++i)
    {
        resultProgram += "\n" + readAllFile(&(*i));
    }
    resultProgram += "\n" + readAllFile(&_program);
    return resultProgram;
}

ResultLoad GeneratorSyncLoadPacketShader::generate(const PathToResource *define)
{
    ResultLoad result;
    result.isAllNormal = false;
    result.resource = NULL;
    TShader *shader = NULL;

    printf("open shaderpack\n");

    std::string name;
    GLenum type;
    std::vector<std::string> libs;
    std::string program;

    if (!readTSHFile(define->path, name, type, libs, program))
    {
        return result;
    }

    std::string compileProg = generateResultProgram(specialSystems->graphicConfig->getGraphicCompilerTable().toString("VersionLine"), libs, program);
    shader = new TShader(name.c_str(), type, compileProg.c_str());
    result.resource = shader;
    result.isAllNormal = true;
    return result;
}

AsyncLoader *GeneratorAsyncLoadPacketShader::generate(const PathToResource *define)
{
    AsyncLoader *loader = new PacketShaderAsyncLoader(define, ((AsyncLoadersFactory *)factory)->getAsyncLoaderBuffer(), specialSystems);
    return loader;
}

PacketShaderAsyncLoader::PacketShaderAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer, SpecialSystems *_specialSystems)
    : AsyncLoader(path, asyncLoadersBuffer), specialSystems(_specialSystems)
{
}

PacketShaderAsyncLoader::~PacketShaderAsyncLoader()
{
}

StatusState PacketShaderAsyncLoader::updateAsyncProcess()
{
    std::fstream file(path.path);
    if (!file.is_open())
    {
        return StatusState::SS_Failed;
    }

    printf("open shaderpack\n");
    if (!readTSHFile(path.path.c_str(), name, type, libs, program))
    {
        return StatusState::SS_Failed;
    }
    finalProgram = generateResultProgram(specialSystems->graphicConfig->getGraphicCompilerTable().toString("VersionLine"), libs, program);
    return StatusState::SS_Succeed;
}

StatusState PacketShaderAsyncLoader::updateSyncProcess()
{
    TShader *shader = new TShader(name.c_str(), type, finalProgram.c_str());
    resource = shader;
    return StatusState::SS_Succeed;
}