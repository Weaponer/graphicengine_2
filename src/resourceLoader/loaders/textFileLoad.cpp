#include "resourceLoader/loaders/textFileLoad.h"

static TextFile *loadFile(const char *_path)
{
    SDL_RWops *file = SDL_RWFromFile(_path, "r");

    if (file == NULL)
    {
        return NULL;
    }
    std::string content = std::string();

    int size = file->seek(file, 0, RW_SEEK_END);
    file->seek(file, 0, RW_SEEK_SET);
    if (size > 0)
    {
        std::vector<char> buffer = std::vector<char>(size + 1);
        buffer[size] = '\0';

        int countRead = 0;
        while (size > 0)
        {
            int count = file->read(file, buffer.data() + countRead, sizeof(char), size);
            countRead += count;
            size -= count;
        }
        
        content = std::string(buffer.data());
    }
    file->close(file);

    int endLine = -1;
    int count = 0;
    for (; _path[count] != '\0'; count++)
    {
        if (_path[count] == '\\' || _path[count] == '/')
        {
            endLine = count;
        }
    }

    std::string path = std::string(_path);
    std::string name = path.substr(endLine + 1, count - std::max(endLine, 0));
    return new TextFile(name.c_str(), content.c_str());
}

ResultLoad GeneratorSyncLoadTextFile::generate(const PathToResource *define)
{
    ResultLoad result;
    result.isAllNormal = false;
    result.resource = NULL;

    TextFile *textFile = loadFile(define->path);
    if(textFile == NULL)
    {
        return result;
    }

    result.isAllNormal = true;
    result.resource = textFile;
    return result;
}

AsyncLoader *GeneratorAsyncLoadTextFile::generate(const PathToResource *define)
{
    AsyncLoader *loader = new TextFileAsyncLoader(define, ((AsyncLoadersFactory *)factory)->getAsyncLoaderBuffer());
    return loader;
}

TextFileAsyncLoader::TextFileAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer) : AsyncLoader(path, asyncLoadersBuffer), textFile(NULL)
{
}

TextFileAsyncLoader::~TextFileAsyncLoader()
{
    if(textFile != NULL)
    {
        delete textFile;
    }
}

StatusState TextFileAsyncLoader::updateAsyncProcess()
{
    textFile = loadFile(path.path.c_str());
    if(textFile == NULL)
    {
        return SS_Failed;
    }
    return StatusState::SS_Succeed;
}

StatusState TextFileAsyncLoader::updateSyncProcess()
{
    resource = textFile;
    textFile = NULL;
    return StatusState::SS_Succeed;
}