#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>

#include "texture.h"
#include "textureLoad.h"

static GLenum getFormatTexture(SDL_Surface *_surface)
{
    GLint format = GL_RGB;
    if (_surface->format->BytesPerPixel == 1)
    {
        format = GL_RED;
    }
    else if (_surface->format->BytesPerPixel == 2)
    {
        format = GL_RG;
    }
    else if (_surface->format->BytesPerPixel == 3)
    {
        format = GL_BGR;
    }
    else
    {
        format = GL_BGRA;
    }
    return format;
}

static GLenum getInternalFormatTexture(SDL_Surface *_surface)
{
    GLint format = GL_COMPRESSED_RGB;
    if (_surface->format->BytesPerPixel == 1)
    {
        format = GL_COMPRESSED_RED_RGTC1;
    }
    else if (_surface->format->BytesPerPixel == 2)
    {
        format = GL_COMPRESSED_RG_RGTC2;
    }
    else if (_surface->format->BytesPerPixel == 3)
    {
        format = GL_COMPRESSED_RGB8_ETC2;
    }
    else
    {
        format = GL_COMPRESSED_RGBA8_ETC2_EAC;
    }
    return format;
}

ResultLoad GeneratorSyncLoadTexture::generate(const PathToResource *define)
{
    ResultLoad result;

    Texture *texture = NULL;
    SDL_Surface *surf = NULL;
    surf = IMG_Load(define->path);
    std::cout << define->path << std::endl;
    if (surf == NULL)
    {
        result.isAllNormal = false;
        result.resource = NULL;
    }
    else
    {
        result.isAllNormal = true;
        texture = new Texture(surf->w, surf->h, getInternalFormatTexture(surf), getFormatTexture(surf), GL_UNSIGNED_BYTE, surf->pixels, true);
        result.resource = texture;
        SDL_FreeSurface(surf);
    }
    return result;
}

AsyncLoader *GeneratorAsyncLoadTexture::generate(const PathToResource *define)
{
    AsyncLoader *loader = new TextureAsyncLoader(define, ((AsyncLoadersFactory *)factory)->getAsyncLoaderBuffer());
    return loader;
}

TextureAsyncLoader::TextureAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer) : AsyncLoader(path, asyncLoadersBuffer)
{
    surf = NULL;
}

TextureAsyncLoader::~TextureAsyncLoader()
{
    if (surf != NULL)
    {
        SDL_FreeSurface(surf);
    }
}

StatusState TextureAsyncLoader::updateAsyncProcess()
{
    surf = IMG_Load(path.path.c_str());
    std::cout << path.path.c_str() << std::endl;
    if (surf == NULL)
    {
        return StatusState::SS_Failed;
    }
    return StatusState::SS_Succeed;
}

StatusState TextureAsyncLoader::updateSyncProcess()
{
    resource = new Texture(surf->w, surf->h, getInternalFormatTexture(surf), getFormatTexture(surf), GL_UNSIGNED_BYTE, surf->pixels, true);
    return StatusState::SS_Succeed;
}