#include "resourceLoader/loaders/hdrTextureLoad.h"
#include "renderEngine/resources/texture.h"

typedef unsigned char RGBE[4];
#define MINELEN 8
#define MAXELEN 0x7fff

#define R 0
#define G 1
#define B 2
#define E 3

static bool oldDecrunch(RGBE *scanline, int len, SDL_RWops *_file)
{
    int i = 0;
    int rshift = 0;

    while (len > 0)
    {
        _file->read(_file, &scanline[0][R], 1, sizeof(unsigned char));
        _file->read(_file, &scanline[0][G], 1, sizeof(unsigned char));
        _file->read(_file, &scanline[0][B], 1, sizeof(unsigned char));
        _file->read(_file, &scanline[0][E], 1, sizeof(unsigned char));

        char buffer = 0;
        char testEOF = _file->read(_file, &buffer, 1, sizeof(buffer));

        if (testEOF == 0)
        {
            return false;
        }
        else
        {
            _file->seek(_file, -1, RW_SEEK_CUR);
        }

        if (scanline[0][R] == 1 &&
            scanline[0][G] == 1 &&
            scanline[0][B] == 1)
        {
            for (i = scanline[0][E] << rshift; i > 0; i--)
            {
                memcpy(&scanline[0][0], &scanline[-1][0], 4);
                scanline++;
                len--;
            }
            rshift += 8;
        }
        else
        {
            scanline++;
            len--;
            rshift = 0;
        }
    }
    return true;
}

static bool decrunch(RGBE *scanline, int len, SDL_RWops *_file)
{
    int i, j;

    if (len < MINELEN || len > MAXELEN)
    {
        return oldDecrunch(scanline, len, _file);
    }

    i = 0;
    _file->read(_file, &i, 1, sizeof(unsigned char));

    if (i != 2)
    {
        _file->seek(_file, -1, RW_SEEK_CUR);
        return oldDecrunch(scanline, len, _file);
    }

    _file->read(_file, &scanline[0][G], 1, sizeof(unsigned char));
    _file->read(_file, &scanline[0][B], 1, sizeof(unsigned char));

    i = 0;
    _file->read(_file, &i, 1, sizeof(unsigned char));

    if (scanline[0][G] != 2 || scanline[0][B] & 128)
    {
        scanline[0][R] = 2;
        scanline[0][E] = i;
        return oldDecrunch(scanline + 1, len - 1, _file);
    }

    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < len;)
        {
            unsigned char code = 0;
            _file->read(_file, &code, 1, sizeof(unsigned char));

            if (code > 128)
            {
                code &= 127;
                unsigned char val = 0;
                _file->read(_file, &val, 1, sizeof(unsigned char));

                while (code--)
                {
                    scanline[j++][i] = val;
                }
            }
            else
            {
                while (code--)
                {
                    _file->read(_file, &scanline[j++][i], 1, sizeof(unsigned char));
                }
            }
        }
    }

    char buffer = 0;
    char testEOF = _file->read(_file, &buffer, 1, sizeof(buffer));

    if (testEOF == 0)
    {
        return false;
    }
    else
    {
        _file->seek(_file, -1, RW_SEEK_CUR);
        return true;
    }
}

static float convertComponent(int expo, int val)
{
    float v = val / 256.0f;
    float d = (float)pow(2, expo);
    return v * d;
}

static void workOnRGBE(RGBE *scan, int len, float *cols)
{
    while (len-- > 0)
    {
        int expo = scan[0][E] - 128;
        cols[0] = convertComponent(expo, scan[0][R]);
        cols[1] = convertComponent(expo, scan[0][G]);
        cols[2] = convertComponent(expo, scan[0][B]);
        cols += 3;
        scan++;
    }
}

static HDRTextureLoadData loadHDRTexture(SDL_RWops *_file)
{
    HDRTextureLoadData result;
    result.width = 0;
    result.height = 0;
    result.sizeData = 0;
    result.data = NULL;

    int i = 0;
    char str[200];
    _file->read(_file, str, 10, sizeof(char));
    if (memcmp(str, "#?RADIANCE", 10) != 0)
    {
        return result;
    }

    _file->seek(_file, 1, RW_SEEK_CUR);
    char cmd[200];
    i = 0;
    char c = 0;
    char oldc = 0;

    while (true)
    {
        oldc = c;
        _file->read(_file, &c, 1, sizeof(char));
        if (c == 0xa && oldc == 0xa)
        {
            break;
        }
        cmd[i++] = c;
    }

    char reso[200];
    i = 0;
    while (true)
    {
        _file->read(_file, &c, 1, sizeof(char));
        reso[i++] = c;
        if (c == 0xa)
        {
            break;
        }
    }

    int w = 0;
    int h = 0;
    if (!sscanf(reso, "-Y %i +X %i", &h, &w))
    {
        return result;
    }

    int countFloatsInBuffer = w * h * 3;
    float *cols = new float[countFloatsInBuffer];
    float *resultColorBuffer = cols;

    RGBE *scanline = new RGBE[w];
    if (!scanline)
    {
        delete[] cols;
        delete[] scanline;
        return result;
    }

    for (int y = h - 1; y >= 0; y--)
    {
        if (decrunch(scanline, w, _file) == false)
        {
            break;
        }
        workOnRGBE(scanline, w, cols);
        cols += w * 3;
    }

    delete[] scanline;
    result.width = w;
    result.height = h;
    result.sizeData = countFloatsInBuffer;
    result.data = resultColorBuffer;
    return result;
}

ResultLoad GeneratorSyncLoadHDRTexture::generate(const PathToResource *define)
{
    ResultLoad result;
    result.isAllNormal = false;
    result.resource = NULL;

    SDL_RWops *file = SDL_RWFromFile(define->path, "rb");
    if (file == NULL)
    {
        return result;
    }

    HDRTextureLoadData hdrTextureData = loadHDRTexture(file);
    file->close(file);

    if (hdrTextureData.sizeData == 0)
    {
        return result;
    }
    result.resource = new Texture(hdrTextureData.width, hdrTextureData.height, GL_RGB16F, GL_RGB, GL_FLOAT, hdrTextureData.data, false);
    result.isAllNormal = true;
    delete[] hdrTextureData.data;
    return result;
}

AsyncLoader *GeneratorAsyncLoadHDRTexture::generate(const PathToResource *define)
{
    AsyncLoader *loader = new HDRTextureAsyncLoader(define, ((AsyncLoadersFactory *)factory)->getAsyncLoaderBuffer());
    return loader;
}

HDRTextureAsyncLoader::HDRTextureAsyncLoader(const PathToResource *path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer) : AsyncLoader(path, asyncLoadersBuffer)
{
    hdrTextureData.sizeData = 0;
    hdrTextureData.data = NULL;
    hdrTextureData.width = 0;
    hdrTextureData.height = 0;
}

HDRTextureAsyncLoader::~HDRTextureAsyncLoader()
{
    if (hdrTextureData.data != NULL)
    {
        delete[] hdrTextureData.data;
    }
}

StatusState HDRTextureAsyncLoader::updateAsyncProcess()
{

    if (hdrTextureData.data == NULL)
    {
        return StatusState::SS_Failed;
    }
    return StatusState::SS_Succeed;
}

StatusState HDRTextureAsyncLoader::updateSyncProcess()
{
    resource = new Texture(hdrTextureData.width, hdrTextureData.height, GL_RGB16F, GL_RGB, GL_FLOAT, hdrTextureData.data, false);
    return StatusState::SS_Succeed;
}