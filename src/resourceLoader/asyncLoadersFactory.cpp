#include "resourceLoader/asyncLoadersFactory.h"

AsyncLoader::AsyncLoader(const PathToResource *_path, Indexedtable<AsyncLoader *> *asyncLoadersBuffer)
{
    this->path.path = std::string(_path->path);
    this->path.type = _path->type;
    this->asyncLoadersBuffer = asyncLoadersBuffer;
    statusLastOperation = StatusState::SS_NotStart;
    resource = NULL;
    isEndAsyncProcess = false;
    isEndSyncProcess = false;
    isFailed = false;
    useLoadedResource = false;

    loaders = NULL;
    coundLoaders = 0;
}

AsyncLoader::~AsyncLoader()
{
}

Object *AsyncLoader::getResource()
{
    return resource;
}

bool AsyncLoader::checkLoadedAdditionalResources()
{
    for (uint i = 0; i < coundLoaders; i++)
    {
        AsyncLoader *loader = loaders[i];
        if (loader != NULL)
        {
            if(loader->isEndSyncProcess == false)
            {
                return false;
            }
        }
    }
    return true;
}

void AsyncLoader::clearAllSubloaders()
{
    for (uint i = 0; i < coundLoaders; i++)
    {
        if (loaders[i] != NULL)
        {
            loaders[i]->clearAllSubloaders();
            asyncLoadersBuffer->popMemory((loaders[i])->indexInBuffer);
            delete loaders[i];
        }
    }
    coundLoaders = 0;
}

void AsyncLoader::initSubAsyncLoader(AsyncLoader *asyncLoader)
{
    sizeM index;
    asyncLoadersBuffer->assignmentMemoryAndCopy(&asyncLoader, index);
    asyncLoader->indexInBuffer = index;
    loaders[coundLoaders] = asyncLoader;
    coundLoaders++;
}