#include "resourceLoader/resourceLoader.h"
#include "resourceLoader/loaders/textureLoad.h"
#include "resourceLoader/loaders/tshaderLoad.h"
#include "resourceLoader/loaders/scriptLoad.h"
#include "resourceLoader/loaders/modelObjLoad.h"
#include "resourceLoader/loaders/layerDataLoad.h"
#include "resourceLoader/loaders/shaderLoader.h"
#include "resourceLoader/loaders/materialLoad.h"
#include "resourceLoader/loaders/cubeMapLoad.h"
#include "resourceLoader/loaders/modelLightShaderLoad.h"
#include "resourceLoader/loaders/hdrTextureLoad.h"
#include "resourceLoader/loaders/textFileLoad.h"
#include "objectRegister/reservedCodes.h"

ResourceLoader::ResourceLoader(ObjectRegister *objectRegister, SpecialSystems *_specialSystems)
    : objectRegister(objectRegister), syncLoadersFactory(this), asyncLoadersFactory(this, &asyncLoaders)
{
    AsyncLoader *nullAsync = NULL;
    ResourceAsyncHandler *nullHandle = NULL;

    requrestsAsyncLoaders.setUseDefault(true);
    requrestsAsyncLoaders.setDefault(&nullAsync);
    asyncLoaders.setUseDefault(true);
    asyncLoaders.setDefault(&nullAsync);
    handlersInQueue.setUseDefault(true);
    handlersInQueue.setDefault(&nullHandle);

    syncLoadersFactory.addRegisterType(new GeneratorSyncLoadTexture());
    asyncLoadersFactory.addRegisterType(new GeneratorAsyncLoadTexture());
    syncLoadersFactory.addRegisterType(new GeneratorSyncLoadHDRTexture());
    asyncLoadersFactory.addRegisterType(new GeneratorAsyncLoadHDRTexture());
    syncLoadersFactory.addRegisterType(new GeneratorSyncLoadPacketShader(_specialSystems));
    asyncLoadersFactory.addRegisterType(new GeneratorAsyncLoadPacketShader(_specialSystems));
    syncLoadersFactory.addRegisterType(new GeneratorSyncLoadScript());
    asyncLoadersFactory.addRegisterType(new GeneratorAsyncLoadScript());
    syncLoadersFactory.addRegisterType(new GeneratorSyncLoadModelObj());
    asyncLoadersFactory.addRegisterType(new GeneratorAsyncLoadModelObj());
    syncLoadersFactory.addRegisterType(new GeneratorSyncLoadLayerData());
    asyncLoadersFactory.addRegisterType(new GeneratorAsyncLoadLayerData());
    syncLoadersFactory.addRegisterType(new GeneratorSyncLoadShader(_specialSystems));
    asyncLoadersFactory.addRegisterType(new GeneratorAsyncLoadShader(_specialSystems));
    syncLoadersFactory.addRegisterType(new GeneratorSyncLoadMaterial(_specialSystems));
    asyncLoadersFactory.addRegisterType(new GeneratorAsyncLoadMaterial(_specialSystems));
    syncLoadersFactory.addRegisterType(new GeneratorSyncLoadCubeMap());
    asyncLoadersFactory.addRegisterType(new GeneratorAsyncLoadCubeMap());
    syncLoadersFactory.addRegisterType(new GeneratorSyncLoadModelLightShader(_specialSystems));
    asyncLoadersFactory.addRegisterType(new GeneratorAsyncLoadModelLightShader(_specialSystems));
    syncLoadersFactory.addRegisterType(new GeneratorSyncLoadTextFile());
    asyncLoadersFactory.addRegisterType(new GeneratorAsyncLoadTextFile());
    enableThread();
}

ResourceLoader::~ResourceLoader()
{
    disableThread();
    int posBuffer = requrestsAsyncLoaders.getPos();
    for (int i = 0; i < posBuffer; i++)
    {
        AsyncLoader **asyncLoader = requrestsAsyncLoaders.getMemory(i);
        if (asyncLoader != NULL)
        {
            delete (*requrestsAsyncLoaders.getMemory(i));
        }
    }

    posBuffer = asyncLoaders.getPosBuffer();
    for (int i = 0; i < posBuffer; i++)
    {
        AsyncLoader **asyncLoader = asyncLoaders.getMemory(i);
        if (*asyncLoader != NULL)
        {
            delete (*asyncLoader);
        }
    }

    while (loadedPaths.size() > 0)
    {
        auto i = loadedPaths.begin();
        removeResource(objectRegister->getResourceByIndex((*i).second));
    }
}

void ResourceLoader::disableThread()
{
    discardThread = true;
    if (threadAsyncLoader.joinable())
    {
        threadAsyncLoader.join();
    }
}

void ResourceLoader::enableThread()
{
    discardThread = false;
    threadAsyncLoader = std::thread(&ResourceLoader::updateAsyncThread, this);
}

Object *ResourceLoader::loadResource(const char *path, TypeResource type)
{
    int posOnResource = getLoadedResourceWithPath(path);
    if (posOnResource != -1)
    {
        return objectRegister->getResourceByIndex(posOnResource);
    }

    PathToResource pathToResource;
    pathToResource.path = path;
    pathToResource.type = type;
    ResultLoad result = syncLoadersFactory.loadGenerator(&pathToResource);
    if (result.isAllNormal)
    {
        registerNewResource(result.resource, path);
        return result.resource;
    }
    return NULL;
}

void ResourceLoader::unloadResource(Object *object)
{
    removeResource(object);
}

ResourceAsyncHandler *ResourceLoader::loadAsyncResource(const char *path, TypeResource type)
{
    PathToResource pathToR;
    pathToR.path = path;
    pathToR.type = type;

    AsyncLoader *asyncLoader = asyncLoadersFactory.loadGenerator(&pathToR);
    if (asyncLoader == NULL)
    {
        return NULL;
    }
    asyncLoader->asyncLoadersBuffer = &asyncLoaders;
    sizeM index;
    requrestsAsyncLoaders.assignmentMemoryAndCopy(&asyncLoader);
    index = requrestsAsyncLoaders.getPos() - 1;
    asyncLoader->indexInBuffer = (uint)index;

    ResourceAsyncHandler *handler = new ResourceAsyncHandler(asyncLoader, path, type);
    objectRegister->registerExtraObject(handler);
    handlersInQueue.assignmentMemoryAndCopy(&handler, index);
    return handler;
}

void ResourceLoader::unloadAsyncResource(ResourceAsyncHandler *handler)
{
    if (handler->status == StatusLoad::SL_None)
    {
        handler->isDiscard = true;
        return;
    }

    if (handler->status == StatusLoad::SL_Succeed)
    {
        removeResource(handler->object);
        handler->object = NULL;
    }

    objectRegister->removeObject(handler);
    delete handler;
}

void ResourceLoader::update()
{
    mutexAsyncLoader.lock();
    moveRequrests();
    int countUpdate = 0;
    int posBuffer = asyncLoaders.getPosBuffer();
    for (int i = 0; i < posBuffer && countUpdate < NUMBER_REQURESTS_UPDATED_SYNC_TICK; i++)
    {
        AsyncLoader *asyncLoader = *asyncLoaders.getMemory(i);
        if (asyncLoader == NULL)
        {
            continue;
        }

        if (asyncLoader->isFailed == false && asyncLoader->isEndAsyncProcess && !asyncLoader->isEndSyncProcess)
        {
            countUpdate++;
            int posLoadedResource = getLoadedResourceWithPath(asyncLoader->path.path.c_str());
            if (posLoadedResource != -1)
            {
                setLoadedResourceInAsyncLoader(asyncLoader, objectRegister->getResourceByIndex(posLoadedResource));
                asyncLoader->isEndSyncProcess = true;
                continue;
            }
            if (!asyncLoader->checkLoadedAdditionalResources())
            {
                bool faild = false;
                for (uint i = 0; i < asyncLoader->coundLoaders; i++)
                {
                    if (asyncLoader->loaders[i] != NULL && asyncLoader->loaders[i]->isFailed == true)
                    {
                        faild = true;
                        break;
                    }
                }
                if (faild)
                {
                    unloadNoNormalResources(asyncLoader);
                    asyncLoader->clearAllSubloaders();
                    asyncLoader->isFailed = true;
                }
                continue;
            }

            StatusState state = asyncLoader->updateSyncProcess();
            asyncLoader->statusLastOperation = state;
            if (state != StatusState::SS_NotFinished)
            {
                asyncLoader->isEndSyncProcess = true;
                if (asyncLoader->useLoadedResource == true)
                {
                }
                else if (asyncLoader->statusLastOperation == StatusState::SS_Succeed)
                {
                    registerNewResource(asyncLoader->resource, asyncLoader->path.path.c_str());
                }
                else
                {
                    asyncLoader->isFailed = true;
                }
            }
        }
    }

    posBuffer = handlersInQueue.getPosBuffer();
    for (int i = 0; i < posBuffer; i++)
    {
        ResourceAsyncHandler *handel = *handlersInQueue.getMemory(i);
        if (handel == NULL)
        {
            continue;
        }

        if (handel->loader->isEndSyncProcess || handel->loader->isFailed)
        {
            StatusState statusOperation = handel->loader->statusLastOperation;
            if (statusOperation == StatusState::SS_Succeed && handel->loader->isEndSyncProcess && handel->isDiscard == false)
            {
                handel->status = StatusLoad::SL_Succeed;
                handel->object = handel->loader->getResource();
            }
            else
            {
                unloadNoNormalResources(handel->loader);
                handel->status = StatusLoad::SL_Failed;
                handel->object = NULL;
            }
            handel->loader->clearAllSubloaders();
            delete handel->loader;
            asyncLoaders.popMemory(handel->loader->indexInBuffer);
            handel->loader = NULL;
            handlersInQueue.popMemory(i);
            if (handel->isDiscard)
            {
                objectRegister->removeObject(handel);
                delete handel;
            }
        }
    }

    mutexAsyncLoader.unlock();
}

void ResourceLoader::moveRequrests()
{
    int pos = requrestsAsyncLoaders.getPos();
    for (int i = 0; i < pos; i++)
    {
        AsyncLoader *asyncLoader = *requrestsAsyncLoaders.getMemory(i);
        sizeM index;
        asyncLoaders.assignmentMemoryAndCopy(&asyncLoader, index);
        asyncLoader->indexInBuffer = index;
    }
    requrestsAsyncLoaders.clearMemory();
}

void ResourceLoader::unloadNoNormalResources(AsyncLoader *asyncLoader)
{
    for (uint i = 0; i < asyncLoader->coundLoaders; i++)
    {
        if (asyncLoader->loaders[i] != NULL && asyncLoader->loaders[i]->statusLastOperation == StatusState::SS_Succeed && asyncLoader->resource != NULL && asyncLoader->useLoadedResource == false)
        {
            unloadNoNormalResources(asyncLoader->loaders[i]);
        }
    }

    if (asyncLoader->statusLastOperation == StatusState::SS_Succeed && asyncLoader->resource != NULL && asyncLoader->useLoadedResource == false)
    {
        removeResource(asyncLoader->resource);
        asyncLoader->resource = NULL;
    }
}

void ResourceLoader::updateAsyncThread()
{
    while (!discardThread)
    {
        mutexAsyncLoader.lock();
        int posBuffer = asyncLoaders.getPosBuffer();
        int countUpdate = 0;
        for (int i = 0; i < posBuffer && countUpdate < NUMBER_REQURESTS_UPDATED_ASYNC_TICK; i++)
        {
            AsyncLoader *asyncLoader = *asyncLoaders.getMemory(i);
            if (asyncLoader != NULL && asyncLoader->isEndAsyncProcess == false && (asyncLoader->statusLastOperation == StatusState::SS_NotStart || asyncLoader->statusLastOperation == StatusState::SS_NotFinished))
            {
                countUpdate++;
                asyncLoader->statusLastOperation = asyncLoader->updateAsyncProcess();
                if (asyncLoader->statusLastOperation != StatusState::SS_NotFinished)
                {
                    asyncLoader->isEndAsyncProcess = true;
                }
                if (asyncLoader->statusLastOperation == StatusState::SS_Failed)
                {
                    asyncLoader->isFailed = true;
                }
            }
        }
        mutexAsyncLoader.unlock();
    }
}

int ResourceLoader::getLoadedResourceWithPath(const char *path)
{
    auto end = loadedPaths.end();
    for (auto i = loadedPaths.begin(); i != end; ++i)
    {
        if (strcmp(path, (*i).first.c_str()) == 0)
        {
            return (*i).second;
        }
    }
    return -1;
}

void ResourceLoader::setLoadedResourceInAsyncLoader(AsyncLoader *asyncLoader, Object *resource)
{
    asyncLoader->useLoadedResource = true;
    asyncLoader->statusLastOperation = StatusState::SS_Succeed;
    if (asyncLoader->resource != NULL)
    {
        delete asyncLoader->resource;
    }
    asyncLoader->resource = resource;
    unloadNoNormalResources(asyncLoader);
    asyncLoader->clearAllSubloaders();
}

void ResourceLoader::registerNewResource(Object *resource, const char *path)
{
    objectRegister->registerResource(resource);
    loadedPaths.push_back(std::make_pair(std::string(path), objectRegister->getCodeRegistration(resource)));
    objectRegister->addSetAdditionalData(resource, ReservedCodes::RESOURCE_DATA, &loadedPaths.back());
}

void ResourceLoader::removeResource(Object *resource)
{
    std::string *str = (std::string *)objectRegister->getAdditionalData(resource, ReservedCodes::RESOURCE_DATA);
    uint codeRegistration = objectRegister->getCodeRegistration(resource);
    loadedPaths.remove(std::make_pair(*str, codeRegistration));
    objectRegister->removeAdditionalData(resource, ReservedCodes::PATH_ON_RESOURCE);
    objectRegister->removeObject(resource);
    delete resource;
}