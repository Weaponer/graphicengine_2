#include <iostream>
#include <time.h>
#include <unistd.h>

#include "engineBase/engineBase.h"

int main(int argc, char *argv[])
{
	EngineBase engineBase;
	engineBase.init();

	engineBase.startMainLoop();
	engineBase.destroy();
	return 0;
}