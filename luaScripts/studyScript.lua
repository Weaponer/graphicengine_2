---@class studyScript
_G.studyScript = {}

---@type GameObject
studyScript.object = studyScript.object

studyScript.type = "studyScript"
studyScript.prototype = "script"

function studyScript:start()
    self.speed = 15
    self.move = Vector3.new()
end

function studyScript:update()
    local deltaMove = Vector3.new()

    if Input.keyboard():getButton(KeyCode.I) then
        deltaMove:setX(1)
    end

    if Input.keyboard():getButton(KeyCode.K) then
        deltaMove:setX(-1)
    end

    if Input.keyboard():getButton(KeyCode.J) then
        deltaMove:setZ(1)
    end

    if Input.keyboard():getButton(KeyCode.L) then
        deltaMove:setZ(-1)
    end

---@type Vector3
---@diagnostic disable-next-line: cast-local-type
    deltaMove = (self.speed * Time.deltaTime()) * deltaMove

    self.move = self.move + deltaMove

    self.object:setPos(self.move)
end


return studyScript