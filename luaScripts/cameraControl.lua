---@class cameraControl
cameraControl = {}

cameraControl.type = "cameraControl"
cameraControl.prototype = "script"

---@type GameObject
cameraControl.object = cameraControl.object

function cameraControl:start()
    self.speedMove = 0
    self.maxSpeedMove = 20
    self.minSpeedMove = 1
    self.acceleration = 8
    self.powBreak = 5
    self.speedRotate = 50
    self.newRotate = Vector3.new(0, 0, 0)
end

function cameraControl:update()

    if not self:updateFreezCamera()
    then
        return
    end
    local move = self:getMove()
    
    local vec2DeltaRot = Time.deltaTime() * self.speedRotate * Input.mouse():deltaPosition()
    self.newRotate = self.newRotate + Vector3.new(vec2DeltaRot:y(), -vec2DeltaRot:x(), 0)

    local newRot = Quaternion.new()
    local newRotMove = Quaternion.new()
    newRot:setEuler(self.newRotate:x(), self.newRotate:y(), self.newRotate:z())
    newRotMove:setEuler(0, self.newRotate:y(), self.newRotate:z())
    local mat4NewPos = newRotMove:getMatrix()

    local newPos = self.object:pos() + mat4NewPos:transform(move)

    self.object:setPos(newPos);
    self.object:setRotate(newRot);
end

function cameraControl:updateFreezCamera()
    return Screen.mouseLock()
end

---@return Vector3
function cameraControl:getMove()
    local move = Vector3.new(0, 0, 0)
    if Input.keyboard():getButton(KeyCode.W)
    then
        move:setZ(1)
    end
    if Input.keyboard():getButton(KeyCode.S)
    then
        move:setZ(-1)
    end
    if Input.keyboard():getButton(KeyCode.A)
    then
        move:setX(1)
    end
    if Input.keyboard():getButton(KeyCode.D)
    then
        move:setX(-1)
    end
    if Input.keyboard():getButton(KeyCode.Space)
    then
        move:setY(1)
    end
    if Input.keyboard():getButton(KeyCode.LShift)
    then
        move:setY(-1)
    end
    if move:magnitude()==0
    then
        local pB = self.speedMove - self.speedMove*self.powBreak*Time.deltaTime()
        if pB < self.minSpeedMove
        then
            self.speedMove=0
        else
            self.speedMove=pB
        end
    else
        self.speedMove=math.min(self.speedMove + self.acceleration*Time.deltaTime(), self.maxSpeedMove)
    end

---@diagnostic disable-next-line: return-type-mismatch
    return Time.deltaTime() * self.speedMove * move
end

return cameraControl