---@class init
init = {
    type = "init",
    prototype = "script"
}

---@type GameObject
init.object = init.object

function init:start()
    self:setSkyBoxIslands()

    --self:loadForestLocation()
    self:loadTestObjects()
    self:loadPhysicsSample()
end

function init:loadTestObjects()
    ---@type Material
    local transparentMaterial = Resources.loadResource(Resource.material, "materials/pbrTransparent.mat")
    ---@type Material
    local transparentMaterialRed = Resources.loadResource(Resource.material, "materials/pbrTransparentRed.mat")
    ---@type Material
    local brickMat = Resources.loadResource(Resource.material, "materials/brickMaterial.mat")
    ---@type Material
    local groundMat = Resources.loadResource(Resource.material, "materials/ground.mat")

    ---@type Material
    local redMat = Resources.loadResource(Resource.material, "materials/redMat.mat")
    ---@type Material
    local greendMat = Resources.loadResource(Resource.material, "materials/greenMat.mat")
    ---@type Material
    local yellowMat = Resources.loadResource(Resource.material, "materials/yellowMat.mat")

    ---@type Model 
    local tableInfoModel = Resources.loadResource(Resource.modelOBJ, "models/MazeGame/Table_1_Final.obj")
    ---@type Mesh
    local tableInfoMesh = tableInfoModel:meshOnIndex(0)

    ---@type Material
    local tableInfoStartM = Resources.loadResource(Resource.material, "materials/MazeGame/tableInfo_Start.mat")

    ---@type Material
    local tableInfoStopM = Resources.loadResource(Resource.material, "materials/MazeGame/tableInfo_Stop.mat")

    ---@type Material
    local tableInfoWinM = Resources.loadResource(Resource.material, "materials/MazeGame/tableInfo_Win.mat")

    Spawner.spawnMesh(tableInfoMesh, nil, Vector3.new(0, 30, 0)):getComponent(Component.meshRenderer):setMaterial(tableInfoStartM)
    Spawner.spawnMesh(tableInfoMesh, nil, Vector3.new(2, 30, 0)):getComponent(Component.meshRenderer):setMaterial(tableInfoStopM)
    Spawner.spawnMesh(tableInfoMesh, nil, Vector3.new(4, 30, 0)):getComponent(Component.meshRenderer):setMaterial(tableInfoWinM)

    local cube = Spawner.spawnCube(nil, Vector3.new(0, 0, 0), Quaternion.new(), Vector3.new(80, 1, 80)):getComponent(Component.meshRenderer):setMaterial(groundMat)
    local cylinder = Spawner.spawnCylinder(nil, Vector3.new(4, 5, 4), Quaternion.new(), Vector3.new(1, 10, 1))

    local cube = Spawner.spawnCube(nil, Vector3.new(0, 0, 0), Quaternion.new(), Vector3.new(1, 1, 1))
    local rot = Quaternion.new();
    Spawner.spawnSphere(nil, Vector3.new(22, 22, -15))
    rot:setEuler(50, 50, 50);
    Spawner.spawnCylinder(nil, Vector3.new(22, 22, -25))
    Spawner.spawnCone(nil, Vector3.new(22, 15, -22), rot)
    Spawner.spawnCapsule(nil, Vector3.new(20, 25, -20))
    Spawner.spawnOilTank(nil, Vector3.new(15, 20, -20))
    Spawner.spawnPyramid(nil, Vector3.new(25, 20, -20))

    Spawner.spawnCube(nil, Vector3.new(20, 30, -20), Quaternion.new(), Vector3.new(5, 0.3, 5)):getComponent(Component.meshRenderer):setMaterial(brickMat)
    Spawner.spawnCube(nil, Vector3.new(20, 5, -20), Quaternion.new(), Vector3.new(15, 0.3, 15))
    Spawner.spawnCube(nil, Vector3.new(30, 20, -20), Quaternion.new(), Vector3.new(0.3, 5, 5)):getComponent(Component.meshRenderer):setMaterial(brickMat)
    Spawner.spawnCube(nil, Vector3.new(10, 20, -20), Quaternion.new(), Vector3.new(0.3, 5, 5)):getComponent(Component.meshRenderer):setMaterial(brickMat)
    Spawner.spawnCube(nil, Vector3.new(20, 20, -30), Quaternion.new(), Vector3.new(5, 5, 0.3)):getComponent(Component.meshRenderer):setMaterial(brickMat)
    Spawner.spawnCube(nil, Vector3.new(20, 20, -10), Quaternion.new(), Vector3.new(5, 5, 0.3)):getComponent(Component.meshRenderer):setMaterial(brickMat)

    Spawner.spawnSphere(nil, Vector3.new(30, 15, 0)):getComponent(Component.meshRenderer):setMaterial(redMat)
    Spawner.spawnSphere(nil, Vector3.new(30, 15, 3)):getComponent(Component.meshRenderer):setMaterial(greendMat)
    Spawner.spawnSphere(nil, Vector3.new(30, 15, 6)):getComponent(Component.meshRenderer):setMaterial(yellowMat)


    local light = GameObject.create("Light")
    light:setPos(Vector3.new(-14, 12, -6))

    ---@type PointLightComponent
    local light = light:addComponent(Component.pointLight)
    light:setIsCreateShadow(false)
    light:setColor(Vector3.new(0.9, 0.7, 0.1))
    light:setRange(10)
    light:setIntensivity(10)

    local light = GameObject.create("Light")
    light:setPos(Vector3.new(-5.5, 10.5, -5.5))

    ---@type PointLightComponent
    local light = light:addComponent(Component.pointLight)
    light:setIsCreateShadow(false)
    light:setColor(Vector3.new(0.9, 0.7, 0.1))
    light:setRange(13)
    light:setIntensivity(18)

    local light = GameObject.create("Light")
    light:setPos(Vector3.new(-4.5, 9.5, -4.5))

    ---@type PointLightComponent
    local light = light:addComponent(Component.pointLight)
    light:setIsCreateShadow(false)
    light:setColor(Vector3.new(0.9, 0.7, 0.1))
    light:setRange(9)
    light:setIntensivity(20)


    local light = GameObject.create("Light 2")
    light:setPos(Vector3.new(-15, 10, 1.5))

    ---@type PointLightComponent
    local light = light:addComponent(Component.pointLight)
    light:setIsCreateShadow(false)
    light:setColor(Vector3.new(1, 0.2, 0.1))
    light:setRange(17)
    light:setIntensivity(40)
    

    Spawner.spawnSphere(nil, Vector3.new(3, 15, 3)):getComponent(Component.meshRenderer):setMaterial(brickMat)

    for i = 0, 15, 2.1 do
        for i2 = 0, 15, 2.1 do
            for i3 = 0, 15, 3 do
                Spawner.spawnCube(nil, Vector3.new(i, i2 + 4, i3)):getComponent(Component.meshRenderer)
            end
        end
    end
    Spawner.spawnCube(nil, Vector3.new(-6, 15, 0)):getComponent(Component.meshRenderer):setMaterial(transparentMaterial)
    Spawner.spawnCube(nil, Vector3.new(-9, 15, 0)):getComponent(Component.meshRenderer):setMaterial(transparentMaterialRed)
    Spawner.spawnCube(nil, Vector3.new(-12, 15, 0)):getComponent(Component.meshRenderer):setMaterial(transparentMaterial)


    Spawner.spawnMesh(Resources.loadResource(Resource.modelOBJ, "models/BigFuelTankL.obj"):meshOnIndex(0), nil, Vector3.new(15, 10, 15))

    for i = 0, 60, 2.1 do
        for i2 = 0, 60, 2.1 do
            ---@type MeshRendererComponent
            local renderComponentWithLod = Spawner.spawnSphere(nil, Vector3.new(i, 35, i2)):getComponent(Component.meshRenderer)

            renderComponentWithLod:addLod()
            renderComponentWithLod:setHeightCoefInLod(1, 0.6)
            renderComponentWithLod:setMeshInLod(1, renderComponentWithLod:mesh())
            renderComponentWithLod:setMaterialInLod(1, redMat)

            renderComponentWithLod:addLod()
            renderComponentWithLod:setHeightCoefInLod(2, 0.4)
            renderComponentWithLod:setMeshInLod(2, renderComponentWithLod:mesh())
            renderComponentWithLod:setMaterialInLod(2, greendMat)

            renderComponentWithLod:addLod()
            renderComponentWithLod:setHeightCoefInLod(3, 0.2)
            renderComponentWithLod:setMeshInLod(3, renderComponentWithLod:mesh())
            renderComponentWithLod:setMaterialInLod(3, yellowMat)

            renderComponentWithLod:addLod()
            renderComponentWithLod:setHeightCoefInLod(4, 0.1)
        end
    end

    local density = GameObject.create("Density")
    ---@type DensityVolumeComponent
    local densityVolume = density:addComponent(Component.densityVolume)
    density:setPos(Vector3.new(0, 60, 0))
    density:setLocalScale(Vector3.new(100, 100, 100))
    densityVolume:setDensity(100)
    densityVolume:setStartDecay(30)
    densityVolume:setEndDecay(80)
    densityVolume:setColorVolume(Color.new(0, 0, 1, 1))
    densityVolume:setDensityVolumeType(DensityVolumeType.cube)
    densityVolume:setDensityVolumePlacementPlane(DensityVolumePlacement.planeXZ)
    densityVolume:setPlacementPlaneGradient(DensityVolumePlacement.planeXZ)
    densityVolume:setPowGradient(0)
    densityVolume:setEdgeFade(1.0)


    local light = GameObject.create("Light 3")
    light:setPos(Vector3.new(0, 60, 0))

    ---@type PointLightComponent
    local light = light:addComponent(Component.pointLight)
    light:setIsCreateShadow(false)
    light:setColor(Vector3.new(0, 0, 1))
    light:setRange(30)
    light:setIntensivity(100)
end

function init:loadForestLocation()
    Resources.loadResource(Resource.shader, "forestLocation/cshaders/objectShader.cshader")
    Resources.loadResource(Resource.shader, "forestLocation/cshaders/rockCubeProjShader.cshader")
    Resources.loadResource(Resource.shader, "forestLocation/cshaders/grassShader.cshader")
    Resources.loadResource(Resource.shader, "forestLocation/cshaders/treeFoliageShader.cshader")
    Resources.loadResource(Resource.shader, "forestLocation/cshaders/terrainShader.cshader")
    SceneLoader.loadScene([[forestLocation/scene]])

    local density = GameObject.create("Density")
    ---@type DensityVolumeComponent
    local densityVolume = density:addComponent(Component.densityVolume)
    density:setPos(Vector3.new(-32, 4, 32))
    density:setLocalScale(Vector3.new(38, 4, 38))
    densityVolume:setColorVolume(Color.new(1.0, 1, 1.0))
    densityVolume:setDensity(600)
    densityVolume:setStartDecay(40)
    densityVolume:setEndDecay(80)
    densityVolume:setDensityVolumeType(DensityVolumeType.cube)
    densityVolume:setDensityVolumePlacementPlane(DensityVolumePlacement.planeXZ)
    densityVolume:setPlacementPlaneGradient(DensityVolumePlacement.planeXZ)
    densityVolume:setPowGradient(8)
    densityVolume:setEdgeFade(1.0)

    Graphic.LODSettings.setLodBias(4.0)
    Graphic.VolumetricFog.setIntensivityFog(1.4)
    Graphic.Shadow.setShadowBias(0, 0.01)
    Graphic.Shadow.setShadowBias(1, 0.1)
    Graphic.Shadow.setShadowBias(2, 0.2)
end

function init:setSkyBoxIslands()
---@type Shader
    local shaderCubeSky = Resources.loadResource(Resource.shader, "shaders/newShaders/compiledShaders/cubeSky.cshader")
    Debug.log(tostring(shaderCubeSky))

---@type Material
    local materialCubeSky = Resources.loadResource(Resource.material, "materials/cubeSky.mat")
    Debug.log(tostring(materialCubeSky))
    Debug.log(tostring(materialCubeSky:shader()))

    --materialCubeSky:setTexture("_ReflectionTexture", reflectionTexture)
    Graphic.Sky.setMaterial(materialCubeSky)

    local colSun = 0.8 * Color.white()
    colSun = colSun + Color.yellow()
    colSun = colSun;
    Graphic.Sky.setColorSun(Color.normalized(colSun))
    

    Graphic.Sky.setIntensityAmbientLight(1)
    Graphic.Sky.setAmbientLightColor(Color.white())
    Graphic.Sky.setEnvironmentBufferAsAmbientLight(true)

    local directionSun = Vector3.new(1.0, 0.8, 1.0)
    directionSun:normalize()
    Graphic.Sky.setDirectionSun(directionSun)
    Graphic.Sky.setIntensity(4)

    Graphic.Sky.setTypeUpdateSky(TypeUpdateSky.call)
    Graphic.Sky.callUpdate()

    Graphic.HDRCorrection.setDynamicHDRCorrection(true)
    Graphic.HDRCorrection.setStaticValue(1)

    Graphic.VolumetricFog.setIntensivityFog(0.0)
    Graphic.VolumetricFog.setGenerateFog(true)
    Graphic.VignetteEffect.setGenerateVignetteEffect(false)
    Graphic.Bloom.setGenerateBloom(true)
    Graphic.Bloom.setForce(0.2)
end

function init:loadPhysicsSample()
    self.initPhysSample = true    

    local physStaticPlatform = Spawner.spawnCube(nil, Vector3.new(115, 0, 115), Quaternion.new(), Vector3.new(15, 1, 15))
    local rigidbodyStaticPlaform = physStaticPlatform:addComponent(Component.rigidbody)
    local boxColliderStaticPlaform = physStaticPlatform:addComponent(Component.boxCollider)
    rigidbodyStaticPlaform:setKinematic(true)
    rigidbodyStaticPlaform:connectCollider(boxColliderStaticPlaform)

    local dist = 40
    local angle = 30
    local offset = 7
    while dist > 0 do
        local rotate = Quaternion.new()
        rotate:setEuler(0, 0, angle)
        local physStaticPlatform = Spawner.spawnCube(nil, Vector3.new(115 + offset, dist, 115), rotate, Vector3.new(8, 0.5, 8))
        local rigidbodyStaticPlaform = physStaticPlatform:addComponent(Component.rigidbody)
        local boxColliderStaticPlaform = physStaticPlatform:addComponent(Component.boxCollider)
        rigidbodyStaticPlaform:setKinematic(true)
        rigidbodyStaticPlaform:connectCollider(boxColliderStaticPlaform)

        dist = dist - 10
        angle = -angle
        offset = -offset
    end
end

function init:update()
    Debug.drawCube(Vector3.new(0, 0, 0), Vector3.new(3, 3, 3))

    if Input.keyboard():getButtonDown(KeyCode.J) and self.initPhysSample then
        local physSphere = Spawner.spawnSphere(nil, Vector3.new(115 + math.random(-100, 100) / 100.0, 50, 115.1 + math.random(-100, 100) / 100.0), Quaternion.new(), Vector3.new(1, 1, 1))
        local rigidbodySphere = physSphere:addComponent(Component.rigidbody)
        local sphereColliderSphere = physSphere:addComponent(Component.sphereCollider)
        rigidbodySphere:connectCollider(sphereColliderSphere)
    end
end

function init:onDiscard()
end

return init
