---@class testTransform
testTransform = {
    type = "testTransform",
    prototype = "script"
}

function testTransform:start()
    self.child = self.object:getChild(0)
    self.rotate = Vector3.new(0, 0, 0)
end

function testTransform:update()
    self.child:setLocalPos(self.child:localPos() + Vector3.new(0, 0.005, 0))
    self.rotate = self.rotate + Vector3.new(0.3, 0.6, 0)
    local rot = Quaternion.new()
    rot:setEuler(self.rotate:x(), self.rotate:y(), self.rotate:z())
    self.object:setRotate(rot)

    if Input.keyboard():getButtonDown(KeyCode.E) then
        self.child:setParent(nil)
        --self.child:setPos(Vector3.new(0, 14, 0))
        --self.child:setRotate(Quaternion.new())        
    end
end

function testTransform:onDiscard()
end

return testTransform
