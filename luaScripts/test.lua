local test = {
    type = "test",
    prototype = "script"
}

function test:update()
    print("test")
end

function test:testCall()
    print("test call")
end

return test