SceneLoader = {}

local readAll = function (file)
    local f = assert(io.open(file, "rb"))
    local content = f:read("*all")
    f:close()
    return content
end


function SceneLoader.loadScene(path)
    
    local objectsDataFile = readAll(path .. [[/data/objectsData.lua]]) 
    local objects = load(objectsDataFile)()
    local texturesDataFile = readAll(path .. [[/data/texturesData.lua]]) 
    local textures = load(texturesDataFile)()
    local meshesDataFile = readAll(path .. [[/data/meshesData.lua]]) 
    local meshes = load(meshesDataFile)()
    local materialsDataFile = readAll(path .. [[/data/materialsData.lua]]) 
    local materials = load(materialsDataFile)()

    local loadedTextures = {}
    for i, j in pairs(textures) do
        loadedTextures[i] = Resources.loadResource(Resource.texture, path .. j)
        Debug.log(tostring(loadedTextures[i]))
    end

    local loadedMeshs = {}
    for i, j in pairs(meshes) do
        ---@type Model
        local model = Resources.loadResource(Resource.modelOBJ, path .. j)
        if isValid(model) then
            loadedMeshs[i] = model:meshOnIndex(0)
            Debug.log(tostring(loadedMeshs[i]))
        else
            Debug.log("Mesh: " .. i .. " - " .. path .. j .. " not loaded.")
        end
    end

    local loadedMaterials = {}
    for i, j in pairs(materials) do
        ---@type Material
        local material = Resources.loadResource(Resource.material, path .. j["localPath"])
        Debug.log(tostring(material))
        if material ~= nil then
            for property, texture in pairs(j["textures"]) do
                material:setTexture(property, loadedTextures[texture])
            end
        end
        loadedMaterials[i] = material
    end

    for i, v in ipairs(objects) do
        SceneLoader.loadGameObject(v, nil, loadedMeshs, loadedMaterials)
    end
end

function SceneLoader.loadGameObject(gameObject, parent, meshes, materials)
    local useMeshRenderer = gameObject.useMeshRenderer

    local object = GameObject.create("Object")
    object:setParent(parent)
    object:setLocalPos(Vector3.new(-gameObject.localPos.x, gameObject.localPos.y, gameObject.localPos.z))
    object:setLocalScale(Vector3.new(gameObject.localScale.x, gameObject.localScale.y, gameObject.localScale.z))
    object:setLocalRotate(Quaternion.new(gameObject.localRotation.x, -gameObject.localRotation.y, -gameObject.localRotation.z, gameObject.localRotation.w))
    if useMeshRenderer then
        ---@type MeshRendererComponent
        local meshRenderer = object:addComponent(Component.meshRenderer)
        meshRenderer:setGenerateShadows(gameObject.generateShadows)
        local countLods = gameObject.countLods
        
        while countLods > meshRenderer:countLods() do
            meshRenderer:addLod()
        end

        for i = 0, countLods - 1, 1 do
            local nameMesh = gameObject.lods[tostring(i)].mesh
            if nameMesh ~= nil then
                Debug.log(tostring(meshes[nameMesh]))
                meshRenderer:setMeshInLod(i, meshes[nameMesh])
            end
            local nameMaterial = gameObject.lods[tostring(i)].material
            if nameMaterial ~= nil then
                meshRenderer:setMaterialInLod(i, materials[nameMaterial])
            end
            meshRenderer:setHeightCoefInLod(i, gameObject.lods[tostring(i)].lodCoef)
        end

    end

    if gameObject.countChilds then        
        for i, v in ipairs(gameObject.childs) do
            SceneLoader.loadGameObject(v, object, meshes, materials)
        end
    end

end