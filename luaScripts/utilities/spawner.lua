Spawner = {}

---@type Material
local standartMaterial = Resources.loadResource(Resource.material, "materials/testMaterial.mat")

local cubeMesh = Resources.loadResource(Resource.modelOBJ, "models/primitives/Cube.obj"):meshOnIndex(0)
local capsuleMesh = Resources.loadResource(Resource.modelOBJ, "models/primitives/Capsule.obj"):meshOnIndex(0)
local coneMesh = Resources.loadResource(Resource.modelOBJ, "models/primitives/Cone.obj"):meshOnIndex(0)
local cylinderMesh = Resources.loadResource(Resource.modelOBJ, "models/primitives/Cylinder.obj"):meshOnIndex(0)
--local figureMesh = Resources.loadResource(Resource.modelOBJ, "models/primitives/Figure.obj"):meshOnIndex(0)
local oilTankMesh = Resources.loadResource(Resource.modelOBJ, "models/primitives/Oil_Tank.obj"):meshOnIndex(0)
local platonicMesh = Resources.loadResource(Resource.modelOBJ, "models/primitives/Platonic.obj"):meshOnIndex(0)
local pyramidMesh = Resources.loadResource(Resource.modelOBJ, "models/primitives/Pyramid.obj"):meshOnIndex(0)
local sphereMesh = Resources.loadResource(Resource.modelOBJ, "models/primitives/Sphere.obj"):meshOnIndex(0)

local spawn = function (mesh, name, parent, position, rotate, scale)
    position = position or Vector3.new()
    rotate = rotate or Quaternion.new()
    scale = scale or Vector3.new(1, 1, 1)

    local gameObject = GameObject.create(name)

    if isValid(parent) then
        gameObject:setParent(parent)
    end

    gameObject:setLocalPos(position)
    gameObject:setLocalRotate(rotate)
    gameObject:setLocalScale(scale)

    ---@type MeshRendererComponent
    local meshRenderer = gameObject:addComponent(Component.meshRenderer)
    meshRenderer:setMesh(mesh)
    meshRenderer:setMaterial(standartMaterial)

    return gameObject
end

function Spawner.spawnCube(parent, position, rotate, scale)
    return spawn (cubeMesh, "Cube", parent, position, rotate, scale)
end

function Spawner.spawnCapsule(parent, position, rotate, scale)
    return spawn (capsuleMesh, "Capsule", parent, position, rotate, scale)
end

function Spawner.spawnCone(parent, position, rotate, scale)
    return spawn (coneMesh, "Cone", parent, position, rotate, scale)
end

function Spawner.spawnCylinder(parent, position, rotate, scale)
    return spawn (cylinderMesh, "Cylinder", parent, position, rotate, scale)
end

--[[
    function Spawner.spawnFigure(parent, position, rotate, scale)
    return spawn (figureMesh, "Figure", parent, position, rotate, scale)
end
]]--

function Spawner.spawnOilTank(parent, position, rotate, scale)
    return spawn (oilTankMesh, "OilTank", parent, position, rotate, scale)
end

function Spawner.spawnPlatonic(parent, position, rotate, scale)
    return spawn (platonicMesh, "Platonic", parent, position, rotate, scale)
end

function Spawner.spawnPyramid(parent, position, rotate, scale)
    return spawn (pyramidMesh, "Pyramid", parent, position, rotate, scale)
end

function Spawner.spawnSphere(parent, position, rotate, scale)
    return spawn (sphereMesh, "Sphere", parent, position, rotate, scale)
end

function  Spawner.spawnMesh(mesh, parent, position, rotate, scale)
    return spawn (mesh, "Mesh", parent, position, rotate, scale)
end