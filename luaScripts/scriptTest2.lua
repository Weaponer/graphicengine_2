---@class testClass
testClass = {}

testClass.type = "testClass"
testClass.prototype = "script"


---@type GameObject
testClass.object = testClass.object

function testClass:start()
    
    Debug.log("Test calss start " .. self.object:name())
end 

function testClass:update()
    Debug.log("Test calss update " .. self.object:name())
    
end 

function testClass:lateUpdate()
    Debug.log("Test calss lateUpdate " .. self.object:name())
    
end 

function testClass:onDiscard()
    Debug.log("Test calss onDiscard " .. self.object:name())
    
end 

function testClass:onEnable()
    Debug.log("Test calss onEnable " .. self.object:name())
end 

function testClass:onDisable()
    Debug.log("Test calss onDisable " .. self.object:name())
end 

return testClass