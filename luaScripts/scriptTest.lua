---@class basic
basic = {}

basic.type = "scriptTest"
basic.prototype = "script"

function basic:start()
    print("start")
    data.speed = 0.3
    
    --[[data.cubes = Resources.loadResource(Resource.modelOBJ, "models/pairCubes.obj")
    meshs = table.pack(data.cubes:names())
    for i, mesh in ipairs(meshs) do
        print(mesh)
    end]]

    collider = object:getComponent(Component.sphereCollider)
    print(collider)
    collider:discard()
end

function basic:update()
    print("update sphere")
    print(Input.keyboard():getButton(KeyCode.A) )
    print(Input.mouse():getButton(MouseButtons.left) )
    object:setPos(Vector3.new(data.speed, data.speed * -0.5, 0) + object:pos())
end

return basic