Map = {}
Map.sizeSide = 24
Map.scaleFactorSide = 6
Map.heightAll = 10
Map.heightWall = 9


MapInside = {}
MapInside.mapFillArray = ArrayInt.new(Map.sizeSide, Map.sizeSide, 1, 0)
MapInside.beginPoint = Vector2.new(1, math.floor(Map.sizeSide / 2))
MapInside.endPoint = Vector2.new(MapInside.beginPoint)
MapInside.directionEndPoint = Vector2.new(1, 0)

math.randomseed(os.time())

function MapInside.MoveNext(stack, newPos)
    local newCell = { pos = Vector2.new(newPos), back = stack, next = nil }
    stack.next = newCell

    return newCell
end

function MapInside.MoveBack(stack)
    if stack.back == nil then
        return nil    
    end
    local back = stack.back
    stack.back = nil
    back.next = nil
    return back
end

function MapInside.IsNoDefine(x, y)
    return MapInside.mapFillArray:get(x, y) == 1
end

function MapInside.IsWall(x, y)
    return MapInside.mapFillArray:get(x, y) == 2
end

function MapInside.IsFree(x, y)
    return MapInside.mapFillArray:get(x, y) == 0
end

function MapInside.getRandomDirection(x, y)
    local listFree = {}
    if MapInside.IsNoDefine(x + 1, y) then
        listFree[#listFree + 1] = Vector2.new(1, 0) 
    end
    if MapInside.IsNoDefine(x - 1, y)  then
        listFree[#listFree + 1] = Vector2.new(-1, 0) 
    end
    if MapInside.IsNoDefine(x, y + 1) then
        listFree[#listFree + 1] = Vector2.new(0, 1) 
    end
    if MapInside.IsNoDefine(x, y - 1) then
        listFree[#listFree + 1] = Vector2.new(0, -1) 
    end

    if #listFree == 0 then
        return nil
    end
   
    local direcitonNum = math.random(1, #listFree)
    --Debug.log(tostring("1 - " .. tostring(#listFree) .. " " .. tostring(direcitonNum) .. " " .. tostring(listFree[direcitonNum]) .. " " .. tostring(Vector2.new(x, y))))
    return listFree[direcitonNum]
end

function MapInside.getRandomDirectionForMaxDist(x, y, arrayInt, needValue)
    local listFree = {}
    if MapInside.IsFree(x + 1, y) and (arrayInt == nil or arrayInt:get(x + 1, y) ==  needValue) then
        listFree[#listFree + 1] = Vector2.new(1, 0) 
    end
    if MapInside.IsFree(x - 1, y) and (arrayInt == nil or arrayInt:get(x - 1, y) ==  needValue)  then
        listFree[#listFree + 1] = Vector2.new(-1, 0) 
    end
    if MapInside.IsFree(x, y + 1) and (arrayInt == nil or arrayInt:get(x, y + 1) ==  needValue) then
        listFree[#listFree + 1] = Vector2.new(0, 1) 
    end
    if MapInside.IsFree(x, y - 1) and (arrayInt == nil or arrayInt:get(x, y - 1) ==  needValue) then
        listFree[#listFree + 1] = Vector2.new(0, -1) 
    end

    if #listFree == 0 then
        return nil
    end
   
    local direcitonNum = math.random(1, #listFree)
    --Debug.log(tostring("1 - " .. tostring(#listFree) .. " " .. tostring(direcitonNum) .. " " .. tostring(listFree[direcitonNum]) .. " " .. tostring(Vector2.new(x, y))))
    return listFree[direcitonNum]
end

function MapInside.generate()
    for i = 0, Map.sizeSide - 1, 1 do
        MapInside.mapFillArray:set(2, i, 0)
        MapInside.mapFillArray:set(2, 0, i)
        MapInside.mapFillArray:set(2, Map.sizeSide - 1, i)
        MapInside.mapFillArray:set(2, i, Map.sizeSide - 1)
        
    end

    for i = 1, Map.sizeSide - 2, 1 do
        for i2 = 1, Map.sizeSide - 2, 1 do
            MapInside.mapFillArray:set(1, i, i2)
        end
    end
    

    local startPos = Vector2.new(MapInside.beginPoint)

    Debug.log(tostring(startPos))
    local direction = Vector2.new(1, 0)
    local stack = { pos = Vector2.new(startPos), back = nil, next = nil }

    local stop = false
    while stop == false do
        local maxDist = math.random(0, 6)
        
        for i = 0, maxDist, 1 do
            local nextPos = startPos + direction  

            if MapInside.IsWall(nextPos:x(), nextPos:y()) then
                break
            end 
            local left = startPos + (Vector2.new(direction:y(), -direction:x()))
            local right = startPos + (-1 * Vector2.new(direction:y(), -direction:x()) )
            
            if MapInside.IsNoDefine(left:x(), left:y()) then
                MapInside.mapFillArray:set(2, left:x(), left:y())   
            end
            if MapInside.IsNoDefine(right:x(), right:y()) then
                MapInside.mapFillArray:set(2, right:x(), right:y())   
            end

            MapInside.mapFillArray:set(0, startPos:x(), startPos:y())

            startPos = nextPos

            stack = MapInside.MoveNext(stack, nextPos)
        end
        
        local newDirection = MapInside.getRandomDirection(startPos:x(), startPos:y())
        
        if newDirection == nil then 
            MapInside.mapFillArray:set(0, startPos:x(), startPos:y())
            while true do
                if newDirection == nil then
                    local back = MapInside.MoveBack(stack)
                    if back == nil then
                        Debug.log("Stop")
                        stop = true
                        break
                    end

                    stack = back
                    startPos = Vector2.new(stack.pos)
                else
                    break
                end
        
                newDirection = MapInside.getRandomDirection(startPos:x(), startPos:y())
            end
        end

---@type Vector2
        newDirection = newDirection
        direction = newDirection
    end
end

function MapInside.generateEndPoint()
    local distArray = ArrayInt.new(Map.sizeSide, Map.sizeSide, 1, -1)

    local stack = { pos = Vector2.new(MapInside.beginPoint), back = nil, next = nil }

    local currentPos = Vector2.new(MapInside.beginPoint)
    local dist = 0
    distArray:set(0, currentPos:x(), currentPos:y())
    while true do
        local direction = MapInside.getRandomDirectionForMaxDist(currentPos:x(), currentPos:y(), distArray, -1)

        if direction == nil then
            local back = MapInside.MoveBack(stack)
            if back == nil then
                break
            end

            stack = back
            dist = distArray:get(stack.pos:x(), stack.pos:y())
            currentPos = Vector2.new(stack.pos)
        else
            currentPos = currentPos + direction
            dist = dist + 1
            distArray:set(dist, currentPos:x(), currentPos:y())
            stack = MapInside.MoveNext(stack, currentPos)
        end
    end


    local maxDist = 0
    local posOnDist = Vector2.new(MapInside.beginPoint)
    for i = 1, Map.sizeSide - 2, 1 do
        for i2 = 1, Map.sizeSide - 2, 1 do
            local dist = distArray:get(i, i2)
            local countWalls = 0

            if MapInside.IsWall(i - 1, i2)  then
                countWalls = countWalls + 1
            end
            if MapInside.IsWall(i + 1, i2)  then
                countWalls = countWalls + 1
            end
            if MapInside.IsWall(i, i2 + 1)  then
                countWalls = countWalls + 1
            end
            if MapInside.IsWall(i, i2 - 1)  then
                countWalls = countWalls + 1
            end

            if dist > maxDist and countWalls == 3 then
                maxDist = dist
                posOnDist = Vector2.new(i, i2)
            end
        end
    end

    if MapInside.IsFree(posOnDist:x() - 1, posOnDist:y()) then
        MapInside.directionEndPoint = Vector2.new(-1, 0)
    elseif MapInside.IsFree(posOnDist:x(), posOnDist:y() + 1) then
        MapInside.directionEndPoint = Vector2.new(0, 1)
    elseif MapInside.IsFree(posOnDist:x(), posOnDist:y() - 1) then
        MapInside.directionEndPoint = Vector2.new(0, -1)
    end

    MapInside.endPoint = posOnDist 
end


function MapInside.checkRect(x, y, w, h, spawnedWalls)
    if x + w > (Map.sizeSide) or y + h > (Map.sizeSide ) then
        return false
    end

    for i = 0, w - 1, 1 do
        for i2 = 0, h - 1, 1 do
            local good = false
            if((MapInside.IsWall(x + i, y + i2) or MapInside.IsNoDefine(x + i, y + i2) ) and spawnedWalls:get(x + i, y + i2) == false) then
                good = true                
            end
            if not good then
                return false
            end
        end
    end

    return true
end


function MapInside.spawnWall(x, y, w, h, spawnedWalls)
    for i = 0, w - 1, 1 do
        for i2 = 0, h - 1, 1 do
            spawnedWalls:set(true, x + i, y + i2)
        end
    end

    local min = Vector3.new(x, 0, y)
    local max = Vector3.new(x + w, 0, y + h)
    local pos = Map.scaleFactorSide * (2 / (max + min))
    local size = Map.scaleFactorSide * (2 / (max - min))
    pos:setY((Map.heightAll - Map.heightWall) + Map.heightWall / 2)
    size:setY(Map.heightWall / 2)
    local object = Spawner.spawnCube(nil, pos, Quaternion.new(), size)
    return object
end

function Map.spawnMaze()
    local objects = {}

    local spawnedWalls = ArrayBool.new(Map.sizeSide, Map.sizeSide, 1, false)
    for i = 0, Map.sizeSide - 1, 1 do
        for i2 = 0, Map.sizeSide - 1, 1 do
            if MapInside.IsWall(i, i2) == false or spawnedWalls:get(i, i2) == true then
                goto continue
            end
            
            local w = 1
            local h = 1
            local blockW = false
            local blockH = false
            while true do
                if not MapInside.checkRect(i, i2, w + 1, h, spawnedWalls) then
                    blockW = true
                else
                    w = w + 1
                end
                
                if not MapInside.checkRect(i, i2, w, h + 1, spawnedWalls) then
                    blockH = true
                else
                    h = h + 1
                end

                if blockW and blockH then
                    break
                end
            end
            
            local object = MapInside.spawnWall(i, i2, w, h, spawnedWalls)
            objects[#objects+1] = object
            ::continue::
        end
    end
    objects[#objects+1] = Spawner.spawnCube(nil, Vector3.new(Map.sizeSide * Map.scaleFactorSide / 2, (Map.heightAll - Map.heightWall) / 2, Map.sizeSide * Map.scaleFactorSide / 2), 
        Quaternion.new(), Vector3.new(Map.sizeSide * Map.scaleFactorSide / 2, (Map.heightAll - Map.heightWall) / 2, Map.sizeSide * Map.scaleFactorSide / 2))
    return objects
end

function Map.getBeginPoint()
---@type Vector2
    return MapInside.beginPoint
end

function Map.getEndPoint()
---@type Vector2
    return MapInside.endPoint
end

function Map.getEndPointDirection()
---@type Vector2
    return MapInside.directionEndPoint
end

function Map.checkFreeSpace(x, y)
    x = x / Map.scaleFactorSide
    y = y / Map.scaleFactorSide
    if x < 0 or x >= Map.sizeSide or y < 0 or y >= Map.sizeSide then
        return true
    end

    return MapInside.IsFree(x, y)
end

MapInside.generate()
MapInside.generateEndPoint()

MapInside.beginPoint = Map.scaleFactorSide * MapInside.beginPoint
MapInside.endPoint = Map.scaleFactorSide * MapInside.endPoint

Debug.drawCube(Vector3.new(Map.sizeSide * Map.scaleFactorSide / 2, Map.heightAll / 2, Map.sizeSide * Map.scaleFactorSide / 2), Vector3.new(Map.sizeSide * Map.scaleFactorSide / 2, Map.heightAll / 2, Map.sizeSide * Map.scaleFactorSide / 2), 100)

Debug.drawLine(Vector3.new(MapInside.beginPoint:x(), 0, MapInside.beginPoint:y()), 
    Vector3.new(MapInside.beginPoint:x(), 3, MapInside.beginPoint:y()), Vector3.new(1, 0 ,0), 100)
Debug.drawLine(Vector3.new(MapInside.endPoint:x(), 0, MapInside.endPoint:y()), 
    Vector3.new( MapInside.endPoint:x(), 3, MapInside.endPoint:y()), Vector3.new(0, 1 ,0), 100)