---@class initMazeGame
initMazeGame = {
    type = "init",
    prototype = "script"
}

---@type GameObject
initMazeGame.object = init.object

function initMazeGame:start()
    self:setSkyBoxIslands()
    --Spawner.spawnCube():addComponent("testClass")
    Map.spawnMaze()

    TableManager.spawnStartTable()
    TableManager.spawnEndTable()
end

function initMazeGame:setSkyBoxIslands()
---@type Shader
    local shaderCubeSky = Resources.loadResource(Resource.shader, "shaders/newShaders/compiledShaders/cubeSky.cshader")
    Debug.log(tostring(shaderCubeSky))

---@type Material
    local materialCubeSky = Resources.loadResource(Resource.material, "materials/cubeSky.mat")
    Debug.log(tostring(materialCubeSky))
    Debug.log(tostring(materialCubeSky:shader()))

    --materialCubeSky:setTexture("_ReflectionTexture", reflectionTexture)
    Graphic.Sky.setMaterial(materialCubeSky)

    local colSun = 0.2 * Color.white()
    local colSun = colSun + Color.yellow()
---@type Color
    local colSun = 1.2 / colSun;
    Graphic.Sky.setColorSun(Color.normalized(colSun))
    
    Graphic.Sky.setIntensityAmbientLight(5)
    Graphic.Sky.setAmbientLightColor(Color.white())
    Graphic.Sky.setEnvironmentBufferAsAmbientLight(true)

    local directionSun = Vector3.new(-0.5, 0.42, -0.53)
    directionSun:normalize()
    Graphic.Sky.setDirectionSun(directionSun)
    Graphic.Sky.setIntensity(30)

    Graphic.Sky.setTypeUpdateSky(TypeUpdateSky.call)
    Graphic.Sky.callUpdate()

    Graphic.VolumetricFog.setIntensivityFog(0.24)

end

function initMazeGame:update()
end

function initMazeGame:onDiscard()
end

return initMazeGame
