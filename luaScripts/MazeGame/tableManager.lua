TableManager = {}

---@type Model 
local tableInfoModel = Resources.loadResource(Resource.modelOBJ, "models/MazeGame/Table_1_Final.obj")
---@type Mesh
TableManager.tableInfoMesh = tableInfoModel:meshOnIndex(0)

---@type Material
TableManager.tableInfoStartM = Resources.loadResource(Resource.material, "materials/MazeGame/tableInfo_Start.mat")

---@type Material
TableManager.tableInfoStopM = Resources.loadResource(Resource.material, "materials/MazeGame/tableInfo_Stop.mat")

---@type Material
TableManager.tableInfoWinM = Resources.loadResource(Resource.material, "materials/MazeGame/tableInfo_Win.mat")

TableManager.listTables = {}

TableManager.scale = 1.5

local addTableToList = function (table, canDestroy, x, y)
    TableManager.listTables[tostring(x) .. " " .. tostring(y)] = { object = table, canDestroy = canDestroy}
end

local checkCanSpawnTable = function (x, y)
    return TableManager.listTables[tostring(x) .. " " .. tostring(y)] == nil
end

local destroyTable = function (x, y)
    local pos = tostring(x) .. " " .. tostring(y)
    if (TableManager.listTables[pos] ~= nil) and (TableManager.listTables[pos].canDestroy == true) then
        GameObject.destroy(TableManager.listTables[pos].object)
        TableManager.listTables[pos] = nil
    end
end

function TableManager.spawnStartTable()
    local pos = Vector3.new(Vector3.new(Map.getBeginPoint():x(), 0, Map.getBeginPoint():y())  + (Map.scaleFactorSide * Vector3.new(0.5, 0.0, 0.5)))
    local rotate = Quaternion.new()
    rotate:setEuler(0, 180, 0)
    local table = Spawner.spawnMesh(TableManager.tableInfoMesh, nil, pos, rotate, Vector3.new(TableManager.scale))
    table:getComponent(Component.meshRenderer):setMaterial(TableManager.tableInfoStartM)
    local x = math.floor(Map.getBeginPoint():x() / Map.scaleFactorSide)
    local y = math.floor(Map.getBeginPoint():y() / Map.scaleFactorSide)
    addTableToList(table, false, x, y)
end


function TableManager.spawnEndTable()
    local pos = Vector3.new(Vector3.new(Map.getEndPoint():x(), 0, Map.getEndPoint():y())  + (Map.scaleFactorSide * Vector3.new(0.5, 0.0, 0.5)))
    local rotate = Quaternion.new()
    if Map.getEndPointDirection() == Vector2.new(1, 0) then
        rotate:setEuler(0, 180, 0)   
    end
    if Map.getEndPointDirection() == Vector2.new(-1, 0) then
        rotate:setEuler(0, 0, 0)   
    end
    if Map.getEndPointDirection() == Vector2.new(0, 1) then
        rotate:setEuler(0, 90, 0)   
    end
    if Map.getEndPointDirection() == Vector2.new(0, -1) then
        rotate:setEuler(0, -90, 0)   
    end
    local table = Spawner.spawnMesh(TableManager.tableInfoMesh, nil, pos, rotate, Vector3.new(TableManager.scale))
    table:getComponent(Component.meshRenderer):setMaterial(TableManager.tableInfoWinM)
    local x = math.floor(Map.getEndPoint():x() / Map.scaleFactorSide)
    local y = math.floor(Map.getEndPoint():y() / Map.scaleFactorSide)
    addTableToList(table, false, x, y)
end

function TableManager.spawnStopTable(x, y, rotate)
    x = math.floor(x / Map.scaleFactorSide)
    y = math.floor(y / Map.scaleFactorSide)
    if checkCanSpawnTable(x, y) then
        local pos = Vector3.new(Map.scaleFactorSide * Vector3.new(x, 0, y)  + (Map.scaleFactorSide * Vector3.new(0.5, 0.0, 0.5)))
        local table = Spawner.spawnMesh(TableManager.tableInfoMesh, nil, pos, rotate, Vector3.new(TableManager.scale))
        table:getComponent(Component.meshRenderer):setMaterial(TableManager.tableInfoStopM)
        addTableToList(table, true, x, y)
    end
end

function TableManager.removeStopTable(x, y)
    x = math.floor(x / Map.scaleFactorSide)
    y = math.floor(y / Map.scaleFactorSide)
    destroyTable(x, y)
end