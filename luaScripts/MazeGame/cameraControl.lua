---@class cameraControlMazeGame
cameraControlMazeGame = {}

cameraControlMazeGame.type = "cameraControl"
cameraControlMazeGame.prototype = "script"

---@type GameObject
cameraControlMazeGame.object = cameraControlMazeGame.object

function cameraControlMazeGame:start()
    self.speedMove = 0
    self.maxSpeedMove = 20
    self.minSpeedMove = 1
    self.acceleration = 8
    self.powBreak = 5
    self.speedRotate = 50
    self.newRotate = Vector3.new(0, 0, 0)
    self.height = 7.5

---@type Vector2
    local point = Map.getBeginPoint()
    self.object:setPos(Vector3.new(point:x(), self.height, point:y()));
end

function cameraControlMazeGame:update()

    if not self:updateFreezCamera()
    then
        return
    end
    local move = self:getMove()
    
    local vec2DeltaRot = Time.deltaTime() * self.speedRotate * Input.mouse():deltaPosition()
    self.newRotate = self.newRotate + Vector3.new(vec2DeltaRot:y(), -vec2DeltaRot:x(), 0)

    local newRot = Quaternion.new()
    local newRotMove = Quaternion.new()
    newRot:setEuler(self.newRotate:x(), self.newRotate:y(), self.newRotate:z())
    newRotMove:setEuler(0, self.newRotate:y(), self.newRotate:z())
    local mat4NewPos = newRotMove:getMatrix()

    local newPos = self.object:pos() + mat4NewPos:transform(move)
    newPos:setY(self.height)

    if Map.checkFreeSpace(newPos:x(), newPos:z()) then
        self.object:setPos(newPos);
    end
    self.object:setRotate(newRot);

    if Input.mouse():getButtonDown(MouseButtons.left) then
        local rot = Quaternion.new()
        rot:setEuler(0, self.newRotate:y() - 90, 0)
        TableManager.spawnStopTable(newPos:x(), newPos:z(), rot)
    end

    if Input.mouse():getButtonDown(MouseButtons.right) then
        TableManager.removeStopTable(newPos:x(), newPos:z())
    end
end

function cameraControlMazeGame:updateFreezCamera()
    return Screen.mouseLock()
end

---@return Vector3
function cameraControlMazeGame:getMove()
    local move = Vector3.new(0, 0, 0)
    if Input.keyboard():getButton(KeyCode.W)
    then
        move:setZ(1)
    end
    if Input.keyboard():getButton(KeyCode.S)
    then
        move:setZ(-1)
    end
    if Input.keyboard():getButton(KeyCode.A)
    then
        move:setX(1)
    end
    if Input.keyboard():getButton(KeyCode.D)
    then
        move:setX(-1)
    end
    if move:magnitude()==0
    then
        local pB = self.speedMove - self.speedMove*self.powBreak*Time.deltaTime()
        if pB < self.minSpeedMove
        then
            self.speedMove=0
        else
            self.speedMove=pB
        end
    else
        self.speedMove=math.min(self.speedMove + self.acceleration*Time.deltaTime(), self.maxSpeedMove)
    end

---@diagnostic disable-next-line: return-type-mismatch
    return Time.deltaTime() * self.speedMove * move
end

return cameraControlMazeGame