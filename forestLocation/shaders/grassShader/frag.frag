#ifdef SHADOW_PASS
#define SINGLE_PIXEL
#endif

#ifdef DEPTH_PASS
#define SINGLE_PIXEL
#endif

#ifdef SINGLE_PIXEL
    uniform float _Cutout;
    uniform vec4 _ScaleAndOffset;
    uniform sampler2D _MainTex;

    in VS_OUT {
        vec2 uv;
    } fs_in;

    layout(location = 0) out vec4 fcol;

    void main()
    {
        float alpha = texture(_MainTex, fs_in.uv * _ScaleAndOffset.xy + _ScaleAndOffset.zw).a;
        if(alpha < _Cutout)
        {
            discard;
        }
        fcol =  vec4(1, 1, 1, 1);
    }
#endif

#ifdef SURFACE_PASS

    uniform float _Cutout;
    uniform float _Roughness;
    uniform float _Metallic;
    uniform vec4 _Color;

    uniform sampler2D _MainTex;
    uniform vec4 _ScaleAndOffset;

    void main()
    {
        vec2 uv = fs_in.uv * _ScaleAndOffset.xy + _ScaleAndOffset.zw;

        vec4 mainTex = texture(_MainTex, uv).rgba;
        float dist = pow(1.0 - min(distance(fs_in.pos, position) / 30.0f, 1.0f), 0.5f);
        if(mainTex.a - _Cutout >= 0)
        {
            SurfaceData surf;
            surf.albedo = mainTex.bgr * _Color.rgb;

            vec3 basicNormal = normalize(fs_in.normal);
            if(gl_FrontFacing)
            {
                surf.normal = basicNormal * 1.0;
            }
            else
            {
                surf.normal = basicNormal * -1.0;
            }

            surf.roughness = max(_Roughness, 0.01);
            surf.metallic = max(_Metallic, 0.01);
            surf.ao = 1;
            surf.additionalChanel_1 = 0.1;

            setSurfaceData(surf);
        }
        else
        {
            discard;
        }
    }
#endif