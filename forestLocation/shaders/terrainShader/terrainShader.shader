Shader = {
    Name = "TerrainShader",
    Properties = {
        _SplatMap = {
            type = "Texture",
        },
        _Layer_1_Color = {
            type = "Texture",
        },
        _Layer_1_Normal = {
            type = "Texture",
        },
        _Layer_1_MAOR = {
            type = "Texture",
        },
        _Layer_2_Color = {
            type = "Texture",
        },
        _Layer_2_Normal = {
            type = "Texture",
        },
        _Layer_2_MAOR = {
            type = "Texture",
        },
        _Layer_3_Color = {
            type = "Texture",
        },
        _Layer_3_Normal = {
            type = "Texture",
        },
        _Layer_3_MAOR = {
            type = "Texture",
        },
        _Scale = {
            type = "Float",
            default = 1,
        }, 
    },
    SubShader = {
        ZTest = true,
        ZWrite = true,

        Queue = "Geometry",
        RenderType = "Opaque",
        GenerateInstancing = true,

        SurfacePass = {
            NamePass = "SurfacePass",
            TypePass = "Surface",

            ModelLight = "Standard",

            Includes = {
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/matricesCamera.glsl",
                "shaders/newShaders/libs/matricesObject.glsl",
                "shaders/newShaders/libs/vertexOutBasic.vert",
                "shaders/newShaders/libs/fragmentInBasic.frag",
                "shaders/newShaders/libs/surface.glsl",
            },

            Vertex = "forestLocation/shaders/terrainShader/vert.vert",
            Fragment = "forestLocation/shaders/terrainShader/frag.frag",
        },
        --[[ShadowPass = {
            NamePass = "ShadowPass",
            TypePass = "Shadow",

            Includes = {
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/matricesCamera.glsl",
                "shaders/newShaders/libs/matricesObject.glsl",
            },

            Vertex = "forestLocation/shaders/terrainShader/vert.vert",
            Fragment = "forestLocation/shaders/terrainShader/frag.frag",
        },]]--
    }
}