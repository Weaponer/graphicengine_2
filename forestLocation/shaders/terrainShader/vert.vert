#ifdef SHADOW_PASS
#define SINGLE_PIXEL
#endif

#ifdef DEPTH_PASS
#define SINGLE_PIXEL
#endif

#ifdef SINGLE_PIXEL
out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    #ifdef USE_INSTANCING
        mat4 matrixM = objects[gl_InstanceID]._matrix_M;
    #else
        mat4 matrixM = _matrix_M;
    #endif

    vec4 posBase = matrixM * vec4(vPosition, 1);
    gl_Position = _matrix_VP * posBase;
}
#endif

#ifdef SURFACE_PASS
void main()
{
    #ifdef USE_INSTANCING
        mat4 matrixM = objects[gl_InstanceID]._matrix_M;
    #else
        mat4 matrixM = _matrix_M;
    #endif

    vec4 posBase = matrixM * vec4(vPosition, 1);
    vec4 normalBase = matrixM * vec4(vNormal, 0);

    vec3 T = normalize(vec3(matrixM * vec4(vTanget, 0)));
    vec3 B = normalize(vec3(matrixM * vec4(normalize(cross(vTanget, vNormal)), 0)));
    vec3 N = normalize(normalBase.xyz);

    mat3 TBN = transpose(mat3(T, B, N));
    vs_out.TBN = TBN;

    vs_out.pos = posBase.xyz;
    vs_out.uv = vUV;
    vs_out.uv.y = 1.0 - vs_out.uv.y;
    vs_out.normal = N;
    vs_out.view = -normalize((_matrix_V * posBase).xyz);
    vs_out.view = (_matrixInverse_V * vec4(vs_out.view, 0)).xyz;

    gl_Position = _matrix_VP * posBase;
}
#endif