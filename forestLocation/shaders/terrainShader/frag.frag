#ifdef SHADOW_PASS
#define SINGLE_PIXEL
#endif

#ifdef DEPTH_PASS
#define SINGLE_PIXEL
#endif

#ifdef SINGLE_PIXEL
    layout(location = 0) out vec4 fcol;

    void main()
    {
        fcol =  vec4(1, 1, 1, 1);
    }
#endif

#ifdef SURFACE_PASS
    uniform sampler2D _SplatMap;
    uniform sampler2D _Layer_1_Color;
    uniform sampler2D _Layer_1_Normal;
    uniform sampler2D _Layer_1_MAOR;
    uniform sampler2D _Layer_2_Color;
    uniform sampler2D _Layer_2_Normal;
    uniform sampler2D _Layer_2_MAOR;
    uniform sampler2D _Layer_3_Color;
    uniform sampler2D _Layer_3_Normal;
    uniform sampler2D _Layer_3_MAOR;
    uniform float _Scale;

    void main()
    {
        vec3 splat = texture(_SplatMap, fs_in.uv).bgr;

        vec2 uv = fs_in.uv * _Scale;
        vec3 colorLayer1 = texture(_Layer_1_Color, uv).bgr;
        vec3 colorLayer2 = texture(_Layer_2_Color, uv).bgr;
        vec3 colorLayer3 = texture(_Layer_3_Color, uv).bgr * vec3(1.0, 0.8, 1.0);

        vec3 normalLayer1 = normalize((texture(_Layer_1_Normal, uv).rgb * 2.0 - 1.0) * fs_in.TBN);
        vec3 normalLayer2 = normalize((texture(_Layer_2_Normal, uv).rgb * 2.0 - 1.0) * fs_in.TBN);
        vec3 normalLayer3 = normalize((texture(_Layer_3_Normal, uv).rgb * 2.0 - 1.0) * fs_in.TBN);

        vec3 maorLayer1 = texture(_Layer_1_MAOR, uv).bgr;
        vec3 maorLayer2 = texture(_Layer_2_MAOR, uv).bgr;
        vec3 maorLayer3 = texture(_Layer_3_MAOR, uv).bgr;

        float layer1 = min(splat.g / (splat.r + splat.g + splat.b), 1);
        float layer2 = min(splat.b / (splat.r + splat.g + splat.b), 1);
        float layer3 = min(splat.r / (splat.r + splat.g + splat.b), 1);
        
        SurfaceData surf;
        surf.albedo = pow((colorLayer1 * layer1 + colorLayer2 * layer2 + colorLayer3 * layer3)  * vec3(0.6, 0.65, 0.3), vec3(1.5));
        vec3 basicNormal = normalize(fs_in.normal);
        surf.normal = normalize(normalLayer1 * layer1 + normalLayer2 * layer2 + normalLayer3 * layer3 + basicNormal);
        surf.roughness = min(max(maorLayer1.b * layer1 + maorLayer2.b * layer2 + maorLayer3.b * layer3, 0.01) * 2, 1);
        surf.metallic = max(maorLayer1.r * layer1 + maorLayer2.r * layer2 + maorLayer3.r * layer3, 0.01);
        surf.ao = maorLayer1.g * layer1 + maorLayer2.g * layer2 + maorLayer3.g * layer3;
        
        setSurfaceData(surf);
    }
#endif