#!/bin/bash
./output/cshader -I forestLocation/shaders/objectShader/objectShader.shader -O forestLocation/cshaders/objectShader.cshader
./output/cshader -I forestLocation/shaders/rockCubeProjShader/rockCubeProjShader.shader -O forestLocation/cshaders/rockCubeProjShader.cshader
./output/cshader -I forestLocation/shaders/grassShader/grassShader.shader -O forestLocation/cshaders/grassShader.cshader
./output/cshader -I forestLocation/shaders/foliageShader/treeFoliageShader.shader -O forestLocation/cshaders/treeFoliageShader.cshader
./output/cshader -I forestLocation/shaders/terrainShader/terrainShader.shader -O forestLocation/cshaders/terrainShader.cshader