Shader = {
    Name = "TreeFoliageShader",
    Properties = {
        _MainTex = {
            type = "Texture",
        },
        _ScaleAndOffset = {
            type = "Vector",
            default = { x = 1, y = 1, z = 0, w = 0},
        },
        _VertexAnimMask = {
            type = "Texture",
        },
        _ScaleAndOffsetAnimMask = {
            type = "Vector",
            default = { x = 1, y = 1, z = 0, w = 0},
        },
        _NormalMap = {
            type = "Texture",
        },
        _IntensityNormalMap = {
            type = "Float",
            default = 1,
        }, 
        _RoughnessMap = {
            type = "Texture",
        },
        _MetalicMap = {
            type = "Texture",
        },
        _AmbientOcclusionMap = {
            type = "Texture",
        },
        _AmbientOcclusion = {
            type = "Float",
            default = 1,
        }, 
        _Color = {
            type = "Color",
            default = { r = 255, g = 255, b = 255, a = 255},
        },
        _Cutout = {
            type = "Float",
            default = 1,
        }, 
        _Roughness = {
            type = "Float",
            default = 0.95,
        },
        _Metallic = {
            type = "Float",
            default = 0.01,
        }, 
        _NoiseSpeed = {
            type = "Float",
            default = 1,
        }, 
        _VertexAnim_NoiseSize = {
            type = "Float",
            default = 0,
        }, 
        _VertexAnim_Intensity = {
            type = "Float",
            default = 0,
        }, 
    },
    SubShader = {
        ZTest = true,
        ZWrite = true,

        Queue = "Geometry",
        RenderType = "TransparentCutout",
        GenerateInstancing = true,

        --[[DepthPass = {
            NamePass = "DepthPass",
            TypePass = "Depth",

            Cull = "Off",

            Includes = {
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/matricesCamera.glsl",
                "shaders/newShaders/libs/matricesObject.glsl",
            },

            Vertex = "forestLocation/shaders/foliageShader/vert.vert",
            Fragment = "forestLocation/shaders/foliageShader/frag.frag",
        },]]--
        SurfacePass = {
            NamePass = "SurfacePass",
            TypePass = "Surface",

            ModelLight = "PBRFoliage",

            Cull = "Off",

            Includes = {
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/matricesCamera.glsl",
                "shaders/newShaders/libs/matricesObject.glsl",
                "shaders/newShaders/libs/vertexOutBasic.vert",
                "shaders/newShaders/libs/fragmentInBasic.frag",
                "shaders/newShaders/libs/surface.glsl",
            },

            Vertex = "forestLocation/shaders/foliageShader/vert.vert",
            Fragment = "forestLocation/shaders/foliageShader/frag.frag",
        },
        ShadowPass = {
            NamePass = "ShadowPass",
            TypePass = "Shadow",

            Cull = "Off",

            Includes = {
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/matricesCamera.glsl",
                "shaders/newShaders/libs/matricesObject.glsl",
            },

            Vertex = "forestLocation/shaders/foliageShader/vert.vert",
            Fragment = "forestLocation/shaders/foliageShader/frag.frag",
        },
    }
}