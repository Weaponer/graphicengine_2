uniform sampler2D _VertexAnimMask;

uniform vec4 _ScaleAndOffsetAnimMask;
uniform float _NoiseSpeed;
uniform float _VertexAnim_NoiseSize;
uniform float _VertexAnim_Intensity;

uniform vec4 _Time;

vec3 mod2D289( vec3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; } 
 
vec2 mod2D289( vec2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; } 
 
vec3 permute( vec3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

float snoise(vec2 v ) 
{ 
    const vec4 C = vec4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 ); 
    vec2 i = floor( v + dot( v, C.yy ) ); 
    vec2 x0 = v - i + dot( i, C.xx ); 
    vec2 i1; 
    i1 = ( x0.x > x0.y ) ? vec2( 1.0, 0.0 ) : vec2( 0.0, 1.0 ); 
    vec4 x12 = x0.xyxy + C.xxzz; 
    x12.xy -= i1; 
    i = mod2D289( i ); 
    vec3 p = permute( permute( i.y + vec3( 0.0, i1.y, 1.0 ) ) + i.x + vec3( 0.0, i1.x, 1.0 ) ); 
    vec3 m = max( 0.5 - vec3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 ); 
    m = m * m; 
    m = m * m; 
    vec3 x = 2.0 * fract( p * C.www ) - 1.0; 
    vec3 h = abs( x ) - 0.5; 
    vec3 ox = floor( x + 0.5 ); 
    vec3 a0 = x - ox; 
    m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h ); 
    vec3 g; 
    g.x = a0.x * x0.x + h.x * x0.y; 
    g.yz = a0.yz * x12.xz + h.yz * x12.yw; 
    return 130.0 * dot( m, g ); 
}

#ifdef SHADOW_PASS
#define SINGLE_PIXEL
#endif

#ifdef DEPTH_PASS
#define SINGLE_PIXEL
#endif

#ifdef SINGLE_PIXEL
out gl_PerVertex
{
    vec4 gl_Position;
};

out VS_OUT {
    vec2 uv;
} vs_out;

void main()
{
    #ifdef USE_INSTANCING
        mat4 matrixM = objects[gl_InstanceID]._matrix_M;
        mat4 matrixInverse_M = objects[gl_InstanceID]._matrixInverse_M; 
    #else
        mat4 matrixM = _matrix_M;
        mat4 matrixInverse_M = _matrixInverse_M;
    #endif

    vs_out.uv = vUV;
    vs_out.uv.y = 1.0 - vs_out.uv.y;

    vec2 uv_vertexAnimMask = vs_out.uv * _ScaleAndOffsetAnimMask.xy + _ScaleAndOffsetAnimMask.zw;
    vec3 ase_vertex3Pos = vPosition;
    float mutTime = _Time.y * _NoiseSpeed;
    float simplePerlin = snoise(_VertexAnim_NoiseSize * 3.6 * ase_vertex3Pos.xy + mutTime);

    vec3 directWind = normalize((matrixInverse_M * vec4(1, 0, 0, 0)).xyz);

    vec4 posBase = matrixM * vec4(vPosition + textureLod(_VertexAnimMask, uv_vertexAnimMask, 0.0).r * (simplePerlin * 0.18 * _VertexAnim_Intensity) * directWind, 1);

    
    gl_Position = _matrix_VP * posBase;
}
#endif

#ifdef SURFACE_PASS
void main()
{
    #ifdef USE_INSTANCING
        mat4 matrixM = objects[gl_InstanceID]._matrix_M;
        mat4 matrixInverse_M = objects[gl_InstanceID]._matrixInverse_M; 
    #else
        mat4 matrixM = _matrix_M;
        mat4 matrixInverse_M = _matrixInverse_M;
    #endif

    vs_out.uv = vUV;
    vs_out.uv.y = 1.0 - vs_out.uv.y;

    vec2 uv_vertexAnimMask = vs_out.uv * _ScaleAndOffsetAnimMask.xy + _ScaleAndOffsetAnimMask.zw;
    vec3 ase_vertex3Pos = vPosition;
    float mutTime = _Time.y * _NoiseSpeed;
    float simplePerlin = snoise(_VertexAnim_NoiseSize * 3.6 * ase_vertex3Pos.xy + mutTime);

    vec3 directWind = normalize((matrixInverse_M * vec4(1, 0, 0, 0)).xyz);

    vec4 posBase = matrixM * vec4(vPosition + textureLod(_VertexAnimMask, uv_vertexAnimMask, 0.0).r * (simplePerlin * 0.18 * _VertexAnim_Intensity) * directWind, 1);

    vec4 normalBase = matrixM * vec4(vNormal, 0);

    vec3 T = normalize(vec3(matrixM * vec4(vTanget, 0)));
    vec3 B = normalize(vec3(matrixM * vec4(normalize(cross(vTanget, vNormal)), 0)));
    vec3 N = normalize(normalBase.xyz);

    mat3 TBN = transpose(mat3(T, B, N));
    vs_out.TBN = TBN;

    vs_out.pos = posBase.xyz;
    vs_out.normal = N;
    vs_out.view = -normalize((_matrix_V * posBase).xyz);
    vs_out.view = (_matrixInverse_V * vec4(vs_out.view, 0)).xyz;

    gl_Position = _matrix_VP * posBase;
}
#endif