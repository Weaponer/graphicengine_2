struct PBRFoliageData
{
    vec3 albedo;
    vec3 normal;
    vec3 view;
    float roughness;
    float metallic;
    float ao;
    float thickness;
    float alpha;
};

vec4 PBRFoliageLight(PBRFoliageData pbrFoliageData, vec4 PDLF, vec3 LPD)
{
    float forceDirectionLight = PDLF.a;
    vec3 forceLightPoints = PDLF.rgb;
    vec3 directionLightPoints = normalize(LPD);

    vec3 V = pbrFoliageData.view;

    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, pbrFoliageData.albedo.rgb, pbrFoliageData.metallic);

    vec3 L0 = vec3(0);

    vec3 basicNormal = pbrFoliageData.normal;
    vec3 backNormal = -basicNormal; 

    const float density = 9.8;
    float transmittance = exp(-density * pbrFoliageData.thickness * 0.01f);
    
    if(forceDirectionLight != 0)
    {
        bool backSide = false;
        vec3 normalSurfToSun = basicNormal;
        if(dot(basicNormal, _directionSun) < 0.0)
        {
            normalSurfToSun = backNormal;
            backSide = true;
        }
        vec3 L1 = lightFromDirect(_colorSun.rgb * _intensity * forceDirectionLight, _directionSun, normalSurfToSun, normalize(V + _directionSun), V,
                                     pbrFoliageData.metallic, pbrFoliageData.albedo, pbrFoliageData.roughness, F0);

        if(backSide)
        {
            L0 += L1 * transmittance;
        }
        else
        {
            L0 += L1;
        }
    }

    L0 += lightFromMultiLightPoints(forceLightPoints, directionLightPoints, pbrFoliageData.normal, normalize(V + directionLightPoints), V,
                                   pbrFoliageData.metallic, pbrFoliageData.albedo, pbrFoliageData.roughness, F0) * (1.0 - transmittance);

    if(_environmentBufferAsAmbientLight)
    {
        vec3 dirAimbentLight = normalize(-reflect(V, basicNormal));
        vec3 irradiance = textureLod(_environmentBuffer, dirAimbentLight, pbrFoliageData.roughness * float(_environmentBufferMaxLevel)).rgb;
        
        vec3 kS = fresnelSchlickRoughness(max(dot(basicNormal, V), 0), F0, pbrFoliageData.roughness);
        vec3 kD = 1.0 - kS;

        vec3 diffuse = irradiance * pbrFoliageData.albedo;
        vec3 ambient = (kD * diffuse);
        
        L0 += ambient * pbrFoliageData.ao * (1.0 - transmittance);

        dirAimbentLight = normalize(-reflect(V, backNormal));
        irradiance = textureLod(_environmentBuffer, dirAimbentLight, pbrFoliageData.roughness * float(_environmentBufferMaxLevel)).rgb;
        
        kS = fresnelSchlickRoughness(max(dot(backNormal, V), 0), F0, pbrFoliageData.roughness);
        kD = 1.0 - kS;

        diffuse = irradiance * pbrFoliageData.albedo;
        ambient = (kD * diffuse);
        
        L0 += ambient * pbrFoliageData.ao * transmittance;
    }
    else
    {
        L0 += _intensityAmbientLight * _ambientLightColor.rgb * pbrFoliageData.ao;
    }
    return vec4(L0, pbrFoliageData.alpha);
}

vec3 getLight(SurfaceData surf, LightData light)
{
    PBRFoliageData pbrFoliageData;
    pbrFoliageData.view = light.view;
    pbrFoliageData.albedo = surf.albedo;
    pbrFoliageData.normal = surf.normal;
    pbrFoliageData.roughness = surf.roughness;
    pbrFoliageData.metallic = surf.metallic;
    pbrFoliageData.alpha = 1.0;
    pbrFoliageData.ao = surf.ao;
    pbrFoliageData.thickness = surf.additionalChanel_1;

    return PBRFoliageLight(pbrFoliageData, vec4(light.forcePointsLight, light.forceDirectionLight), vec3(light.directionPointsLight)).rgb;
}