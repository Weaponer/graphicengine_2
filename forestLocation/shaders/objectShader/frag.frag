#ifdef SHADOW_PASS
#define SINGLE_PIXEL
#endif

#ifdef DEPTH_PASS
#define SINGLE_PIXEL
#endif

#ifdef SINGLE_PIXEL
    uniform float _Cutout;
    uniform vec4 _ScaleAndOffset;
    uniform sampler2D _MainTex;

    in VS_OUT {
        vec2 uv;
    } fs_in;

    layout(location = 0) out vec4 fcol;

    void main()
    {
        float alpha = texture(_MainTex, fs_in.uv * _ScaleAndOffset.xy + _ScaleAndOffset.zw).a;
        if(alpha < _Cutout)
        {
            discard;
        }
        fcol =  vec4(1, 1, 1, 1);
    }
#endif

#ifdef SURFACE_PASS
    uniform sampler2D _MainTex;
    uniform vec4 _ScaleAndOffset;

    uniform sampler2D _NormalMap;
    uniform sampler2D _RoughnessMap;
    uniform sampler2D _MetalicMap;
    uniform sampler2D _AmbientOcclusionMap;
    
    uniform vec4 _Color;
    uniform float _Cutout;
    uniform float _IntensityNormalMap;
    uniform float _Roughness;
    uniform float _Metallic;
    uniform float _AmbientOcclusion;


    void main()
    {
        /*SurfaceData surf;
        surf.albedo = vec3(1, 1, 1);
        surf.normal = normalize(fs_in.normal);

        surf.roughness = 0.9;
        surf.metallic = 0.05;
        surf.ao = 1;
        setSurfaceData(surf);*/

        vec2 uv = fs_in.uv * _ScaleAndOffset.xy + _ScaleAndOffset.zw;

        vec4 mainTex = texture(_MainTex, uv).rgba;
        if(mainTex.a < _Cutout)
        {
            discard;
        }

        SurfaceData surf;
        surf.albedo = mainTex.bgr * _Color.rgb;

        vec3 basicNormal = normalize(fs_in.normal);
        vec3 normal = normalize((texture(_NormalMap, uv).rgb * 2.0 - 1.0) * fs_in.TBN);
        normal = mix(basicNormal, normal, _IntensityNormalMap); 
        normal = normalize(normal);
        surf.normal = normal;

        surf.roughness = max(texture(_RoughnessMap, uv ).r * _Roughness, 0.01);
        surf.metallic = max(texture(_MetalicMap, uv ).r * _Metallic, 0.01);
        surf.ao = texture(_AmbientOcclusionMap, uv ).r * _AmbientOcclusion;

        setSurfaceData(surf);
    }
#endif