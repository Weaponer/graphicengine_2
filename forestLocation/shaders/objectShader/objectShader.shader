Shader = {
    Name = "Object",
    Properties = {
        _MainTex = {
            type = "Texture",
        },
        _ScaleAndOffset = {
            type = "Vector",
            default = { x = 1, y = 1, z = 0, w = 0},
        },
        _NormalMap = {
            type = "Texture",
        },
        _RoughnessMap = {
            type = "Texture",
        },
        _MetalicMap = {
            type = "Texture",
        },
        _AmbientOcclusionMap = {
            type = "Texture",
        },
        _Color = {
            type = "Color",
            default = { r = 255, g = 255, b = 255, a = 255},
        },
        _Cutout = {
            type = "Float",
            default = 1,
        }, 
        _IntensityNormalMap = {
            type = "Float",
            default = 1,
        }, 
        _Roughness = {
            type = "Float",
            default = 0.95,
        },
        _Metallic = {
            type = "Float",
            default = 0.01,
        }, 
        _AmbientOcclusion = {
            type = "Float",
            default = 1.0,
        }, 
    },
    SubShader = {
        ZTest = true,
        ZWrite = true,

        Queue = "Geometry",
        RenderType = "TransparentCutout",
        GenerateInstancing = true,

        --[[DepthPass = {
            NamePass = "DepthPass",
            TypePass = "Depth",

            Includes = {
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/matricesCamera.glsl",
                "shaders/newShaders/libs/matricesObject.glsl",
            },

            Vertex = "forestLocation/shaders/objectShader/vert.vert",
            Fragment = "forestLocation/shaders/objectShader/frag.frag",
        },]]--
        SurfacePass = {
            NamePass = "SurfacePass",
            TypePass = "Surface",

            ModelLight = "Standard",

            Includes = {
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/matricesCamera.glsl",
                "shaders/newShaders/libs/matricesObject.glsl",
                "shaders/newShaders/libs/vertexOutBasic.vert",
                "shaders/newShaders/libs/fragmentInBasic.frag",
                "shaders/newShaders/libs/surface.glsl",
            },

            Vertex = "forestLocation/shaders/objectShader/vert.vert",
            Fragment = "forestLocation/shaders/objectShader/frag.frag",
        },
        ShadowPass = {
            NamePass = "ShadowPass",
            TypePass = "Shadow",

            Cull = "Off",

            Includes = {
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/matricesCamera.glsl",
                "shaders/newShaders/libs/matricesObject.glsl",
            },

            Vertex = "forestLocation/shaders/objectShader/vert.vert",
            Fragment = "forestLocation/shaders/objectShader/frag.frag",
        },
    }
}