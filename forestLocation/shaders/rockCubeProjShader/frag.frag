#ifdef SURFACE_PASS
    uniform sampler2D _MainTex;
    uniform vec4 _ScaleAndOffsetMainTex;
    uniform sampler2D _NormalMap;
    uniform sampler2D _RoughnessMap;
    uniform sampler2D _MetalicMap;
    uniform sampler2D _AmbientOcclusionMap;

    uniform sampler2D _MainTexGround;
    uniform vec4 _ScaleAndOffsetGround;
    uniform sampler2D _NormalMapGround;
    uniform sampler2D _RoughnessMapGround;
    uniform sampler2D _MetalicMapGround;
    uniform sampler2D _AmbientOcclusionMapGround;

    uniform sampler2D _MaskGround;
    
    uniform vec4 _DirectionGroundCoef;
    uniform vec4 _DirectionCubeGround;
    uniform vec4 _CubeData;

    uniform vec4 _Color;

    uniform float _IntensityNormalMap;
    uniform float _Roughness;
    uniform float _Metallic;
    uniform float _AmbientOcclusion;

    uniform float _IntensityNormalMapGround;
    uniform float _RoughnessGround;
    uniform float _MetallicGround;
    uniform float _AmbientOcclusionGround;

    uniform float _AddDirectionCoef;
    uniform float _IntensityGround;
    uniform float _PowGround;

    in vec3 localPos;


    void main()
    {
        vec3 worldNormal = normalize(fs_in.normal);
        vec3 worldPos = fs_in.pos;

        vec3 projNormal = abs(worldNormal);
        projNormal /= (projNormal.x + projNormal.y + projNormal.z) + 0.00001;
        vec3 nsign = sign(worldNormal);
        vec4 xNorm;
        vec4 yNorm;
        vec4 zNorm;

        vec2 uvX = worldPos.zy * vec2(nsign.x, 1.0) * _ScaleAndOffsetGround.x;
        vec2 uvY = worldPos.xz * vec2(nsign.y, 1.0) * _ScaleAndOffsetGround.x;
        vec2 uvZ = worldPos.xy * vec2(-nsign.z, 1.0) * _ScaleAndOffsetGround.x;

        xNorm = texture(_MainTexGround, uvX); yNorm = texture(_MainTexGround, uvY); zNorm = texture(_MainTexGround, uvZ); 
        vec4 groundTex = xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z; 
        xNorm = texture(_NormalMapGround, uvX); yNorm = texture(_NormalMapGround, uvY); zNorm = texture(_NormalMapGround, uvZ); 
        vec4 normalGround = xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z; 
        xNorm = texture(_RoughnessMapGround, uvX); yNorm = texture(_RoughnessMapGround, uvY); zNorm = texture(_RoughnessMapGround, uvZ); 
        vec4 roughnessGround = xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z;
        xNorm = texture(_MetalicMapGround, uvX); yNorm = texture(_MetalicMapGround, uvY); zNorm = texture(_MetalicMapGround, uvZ); 
        vec4 metallicGround = xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z;
        xNorm = texture(_AmbientOcclusionMapGround, uvX); yNorm = texture(_AmbientOcclusionMapGround, uvY); zNorm = texture(_AmbientOcclusionMapGround, uvZ); 
        vec4 occlusionGround = xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z;

        xNorm = texture(_MaskGround, uvX); yNorm = texture(_MaskGround, uvY); zNorm = texture(_MaskGround, uvZ); 
        vec4 maskGround = xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z;

        vec2 uv = fs_in.uv * _ScaleAndOffsetMainTex.xy + _ScaleAndOffsetMainTex.zw;
    
        vec4 mainTex = texture(_MainTex, uv);
        vec4 normalMain = texture(_NormalMap, uv);
        vec4 roughnessMain = texture(_RoughnessMap, uv);
        vec4 metallicMain = texture(_MetalicMap, uv);
        vec4 occlusionMap = texture(_AmbientOcclusionMap, uv);

        vec3 normalPlanes = normalize(_DirectionCubeGround.xyz);
        float dist = dot(normalPlanes, localPos);
        float range = _CubeData.x - _CubeData.y;

        float coefGround = pow(max(dist - _CubeData.y, 0) / range, _CubeData.z);

        coefGround = clamp(coefGround, 0.0, 1.0);

        if(_AddDirectionCoef != 0.0)
        {
            coefGround += clamp(dot(worldNormal, normalize(_DirectionGroundCoef.xyz)) * _DirectionGroundCoef.w, 0.0, 1.0);
        }

        coefGround = clamp(coefGround, 0.0, 1.0);
        coefGround = pow(coefGround * maskGround.r * _IntensityGround, _PowGround);
        coefGround = clamp(coefGround, 0.0, 1.0);

        SurfaceData surf;
        surf.albedo = mix(mainTex.bgr, groundTex.bgr, coefGround) * _Color.rgb;
        surf.metallic = max(mix(metallicMain.r * _Metallic, metallicGround.r * _MetallicGround, coefGround), 0.01);
        surf.roughness = max(mix(roughnessMain.r * _Roughness, roughnessGround.r * _RoughnessGround, coefGround), 0.01);
        surf.ao = mix(occlusionMap.r * _AmbientOcclusion, occlusionGround.r * _AmbientOcclusionGround, coefGround);

        vec3 nGround = normalize(normalGround.rgb * 2.0 - 1.0);
        vec3 nMain = normalize((normalMain.rgb * 2.0 - 1.0) * fs_in.TBN);

        surf.normal = mix(worldNormal, normalize(mix(nMain, nGround, coefGround)).xyz, mix(_IntensityNormalMap, _IntensityNormalMapGround, coefGround));
        setSurfaceData(surf);
    }
#endif