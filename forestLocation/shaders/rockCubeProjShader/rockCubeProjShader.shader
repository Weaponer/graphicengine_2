Shader = {
    Name = "RockCubeProjShader",
    Properties = {
        _MainTex = {
            type = "Texture",
        },
        _ScaleAndOffsetMainTex = {
            type = "Vector",
            default = { x = 1, y = 1, z = 0, w = 0},
        },
        _NormalMap = {
            type = "Texture",
        },
        _RoughnessMap = {
            type = "Texture",
        },
        _MetalicMap = {
            type = "Texture",
        },
        _AmbientOcclusionMap = {
            type = "Texture",
        },
        _MainTexGround = {
            type = "Texture",
        },
        _ScaleAndOffsetGround = {
            type = "Vector",
            default = { x = 1, y = 1, z = 0, w = 0},
        },
        _NormalMapGround = {
            type = "Texture",
        },
        _RoughnessMapGround = {
            type = "Texture",
        },
        _MetalicMapGround = {
            type = "Texture",
        },
        _AmbientOcclusionMapGround = {
            type = "Texture",
        },
        _MaskGround = {
            type = "Texture",
        },
        _DirectionGroundCoef = {
            type = "Vector",
            default = { x = 1, y = 1, z = 0, w = 0},
        },
        _DirectionCubeGround = {
            type = "Vector",
            default = { x = 1, y = 1, z = 0, w = 0},
        },
        _CubeData = {
            type = "Vector",
            default = { x = 1, y = 1, z = 0, w = 0},
        },
        _Color = {
            type = "Color",
            default = { r = 255, g = 255, b = 255, a = 255},
        },
        _IntensityNormalMap = {
            type = "Float",
            default = 1,
        }, 
        _Roughness = {
            type = "Float",
            default = 0.95,
        },
        _Metallic = {
            type = "Float",
            default = 0.01,
        }, 
        _AmbientOcclusion = {
            type = "Float",
            default = 1.0,
        }, 
        _IntensityNormalMapGround = {
            type = "Float",
            default = 1,
        }, 
        _RoughnessGround = {
            type = "Float",
            default = 0.95,
        },
        _MetallicGround = {
            type = "Float",
            default = 0.01,
        }, 
        _AmbientOcclusionGround = {
            type = "Float",
            default = 1.0,
        }, 
        _AddDirectionCoef = {
            type = "Float",
            default = 1.0,
        }, 
        _IntensityGround = {
            type = "Float",
            default = 1.0,
        }, 
        _PowGround = {
            type = "Float",
            default = 1.0,
        }, 
    },
    SubShader = {
        ZTest = true,
        ZWrite = true,

        Queue = "Geometry",
        RenderType = "Opaque",
        GenerateInstancing = true,

        --[[DepthPass = {
            NamePass = "DepthPass",
            TypePass = "Depth",
            
            UseDefaultPrograms = true,
            UseNormalMap = false,
            UseClippingMap = false,
        },]]--
        SurfacePass = {
            NamePass = "SurfacePass",
            TypePass = "Surface",

            ModelLight = "Standard",

            Includes = {
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/matricesCamera.glsl",
                "shaders/newShaders/libs/matricesObject.glsl",
                "shaders/newShaders/libs/vertexOutBasic.vert",
                "shaders/newShaders/libs/fragmentInBasic.frag",
                "shaders/newShaders/libs/surface.glsl",
            },

            Vertex = "forestLocation/shaders/rockCubeProjShader/vert.vert",
            Fragment = "forestLocation/shaders/rockCubeProjShader/frag.frag",
        },
        ShadowPass = {
            NamePass = "ShadowPass",
            TypePass = "Shadow",
            
            UseDefaultPrograms = true,
            UseNormalMap = false,
            UseClippingMap = false,
        },
    }
}