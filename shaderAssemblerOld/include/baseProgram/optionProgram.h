#ifndef OPTION_PROGRAM_H
#define OPTION_PROGRAM_H

#define DEFAULT_NEW_PREFIX "new_"

#include <string>

class OptionProgram
{
private:
    std::string pathShader;
    std::string pathToLibrary;
    std::string pathToOutput;
    std::string nameOutputFile;
    std::string startPath;

    std::string prefixForNewFiles;

public:
    OptionProgram(int argc, char *argv[]);

    virtual ~OptionProgram() {}

    const std::string &getPathShader() const { return pathShader; }

    const std::string &getPathToLibrary() const { return pathToLibrary; }

    const std::string &getPathToOutput() const { return pathToOutput; }

    const std::string &getNameOutputFile() const { return nameOutputFile; }

    const std::string &getStartPath() const { return startPath; }

    const std::string &getPrefixForNewFiles() const { return prefixForNewFiles; }

    bool isHavePathToOutput() const { return pathToOutput.size() != 0; }

    bool isHaveInput() const { return pathShader.size() != 0; }

    bool isHaveLibrary() const { return pathToLibrary.size() != 0; }

    bool isNameOutputFile() const { return nameOutputFile.size() != 0; }

private:
    void initCurrentPath();

    void processOption(const char *c_str);

    void processPathShader(const char *c_str);

    void processPathToLibrary(const char *c_str);

    void processPathToOutput(const char *c_str);

    void processNameOutputFile(const char *c_str);

    void processPrefixForNewFiles(const char *c_str);
};

#endif