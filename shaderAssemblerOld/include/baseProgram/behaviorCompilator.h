#ifndef BEHAVIOR_COMPILATOR_H
#define BEHAVIOR_COMPILATOR_H

#include <list>
#include <vector>

#include "file.h"

#include "macroCompilator/macroLocalData.h"
#include "macroCompilator/macroInclude.h"
#include "optionProgram.h"
#include "libraryLoader.h"

class BehaviorCompilator
{
private:
    const OptionProgram *option;
    LibraryLoader libraryLoader;

    MacroLocalData macroLD;
    MacroInclude macroI;

    std::list<File*> files;

public:
    BehaviorCompilator(const OptionProgram *option);

    virtual ~BehaviorCompilator() {}

private:
    std::vector<bool> processFiles();

    int applyingMacro(std::string& str, Macro* macro);

    void saveFiles(std::vector<bool>& isChangedFiles);

    void loadFiles();

    void unloadFiles();
};

#endif