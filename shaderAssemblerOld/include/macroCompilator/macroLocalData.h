#ifndef MACRO_LOCAL_DATA_H
#define MACRO_LOCAL_DATA_H

#define PREFIX_LOCAL_DATA "#localdata"

#include "macro.h"

class MacroLocalData : public Macro
{
private:

public:
    MacroLocalData() {}

    virtual ~MacroLocalData() {}

    virtual bool isMinePrefix(const char* c_str) const override;

    virtual int correctString(std::string &str, size_t point) override;
};

#endif