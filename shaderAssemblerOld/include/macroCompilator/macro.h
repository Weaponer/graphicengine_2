#ifndef MACRO_H
#define MACRO_H

#include <string>
#include <list>

class Macro
{
private:
    std::list<std::string> errors;

public:
    Macro() {}

    virtual ~Macro() {}

    virtual bool isMinePrefix(const char* c_str) const = 0;

    virtual int correctString(std::string &str, size_t point) = 0;

    void clearErrors() { errors.clear(); }

    const std::list<std::string> &getErrors() const { return errors; }

    void addError(const std::string &str) { errors.push_back(str); }
};

#endif