#ifndef MACRO_INCLUDE_H
#define MACRO_INCLUDE_H

#define PREFIX_INCLUDE "#include"

#include "macro.h"
#include "libraryLoader.h"

class MacroInclude : public Macro
{
private:
    LibraryLoader *libraryLoader;

public:
    MacroInclude(LibraryLoader *libraryLoader) : libraryLoader(libraryLoader) {}

    virtual ~MacroInclude() {}

    virtual bool isMinePrefix(const char *c_str) const override;

    virtual int correctString(std::string &str, size_t point) override;
};
#endif