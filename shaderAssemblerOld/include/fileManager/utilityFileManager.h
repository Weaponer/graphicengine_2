#ifndef UTILITY_FILE_MANAGER_H
#define UTILITY_FILE_MANAGER_H

#include <string>

bool isBracket(const char *symbol);

void editEndPath(std::string &path);

void editBracketBeginPath(std::string &path);

void setBracketsPath(std::string &path);

std::string compoundPathToDirectoryFormPathToFile(const std::string &pathToDirectory, const std::string &pathToFile);

#endif