#ifndef LIBRARY_LOADER_H
#define LIBRARY_LOADER_H

#include <dirent.h>
#include <unistd.h>

#include "file.h"
#include "optionProgram.h"

class LibraryLoader
{
private:
    const OptionProgram *option;

public:
    LibraryLoader(const OptionProgram *option) : option(option) {}

    virtual ~LibraryLoader() {}

    File *loadFile(const std::string &path);

private:
    bool isHaveFile(const std::string &str);

    bool isHaveDirectory(const std::string &str);
};

#endif