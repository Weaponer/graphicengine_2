#ifndef FILE_H
#define FILE_H

#include <string>

class File
{
private:
    std::string fileContents;
    std::string path;
    std::string name;
    std::string onlyPath;

    bool errorFile;

public:
    File(const std::string &path);

    File(const std::string &path, const std::string &name);

    virtual ~File() {}

    void setNewPath(const std::string &path);

    std::string *getFileContents() { return &fileContents; }

    const std::string *getName() const { return &name; }

    const std::string *getOnlyPath() const { return &onlyPath; }

    const std::string *getFullPath() const { return &path; }

    bool isError() { return errorFile; }

    void readFile();

    void writeFile();
};

#endif