#include <fstream>

#include "file.h"

static std::string getNameFromPath(const std::string &path)
{
    for (int i = path.size(); i > 0; --i)
    {
        if (path[i] == '\\' || path[i] == '/')
        {
            return path.substr(i + 1, path.size() - i);
        }
    }
    return "";
}

static std::string getOnlyPathFromPath(const std::string &path)
{
    for (int i = path.size(); i > 0; --i)
    {
        if (path[i] == '\\' || path[i] == '/')
        {
            return path.substr(0, i);
        }
    }
    return "";
}

File::File(const std::string &path) : fileContents(""), path(path), name(getNameFromPath(path)), onlyPath(getOnlyPathFromPath(path))
{
}

File::File(const std::string &path, const std::string &name) : fileContents(""), path(path), name(name), onlyPath(getOnlyPathFromPath(path))
{
}

void File::setNewPath(const std::string &path)
{
    this->path = path;
    onlyPath = getOnlyPathFromPath(path);
    name = getNameFromPath(path);
}

void File::readFile()
{
    std::ifstream file = std::ifstream(path);
    if (file.is_open())
    {
        file.seekg(0, std::ios::end);
        size_t size = file.tellg();
        fileContents = std::string(size, ' ');
        file.seekg(0);
        file.read(&fileContents[0], size);
        file.close();
        errorFile = false;
    }
    else
    {
        errorFile = true;
    }
}

void File::writeFile()
{
    std::ofstream file = std::ofstream(path);
    if (file.is_open())
    {
        file.clear();
        file.write(fileContents.c_str(), fileContents.size());
        file.close();
        errorFile = false;
    }
    else
    {
        errorFile = true;
    }
}