#include "libraryLoader.h"
#include "utilityFileManager.h"

File *LibraryLoader::loadFile(const std::string &path)
{
    std::string fileP = compoundPathToDirectoryFormPathToFile(option->getPathToLibrary(), path);
    File *file = new File(fileP);
    file->readFile();
    return file;
}