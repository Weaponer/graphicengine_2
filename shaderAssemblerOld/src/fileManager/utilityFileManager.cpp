#include "utilityFileManager.h"

bool isBracket(const char *symbol)
{
    return *symbol == '/' || *symbol == '\\';
}

void editEndPath(std::string &path)
{
    if (!isBracket(&(path[path.size() - 1])))
        path += '/';
}

void editBracketBeginPath(std::string &path)
{
    if (path[0] != '.')
    {
        if (!isBracket(&(path[0])))
        {
            path.insert(0, "/");
        }
    }
}

void setBracketsPath(std::string &path)
{
    editEndPath(path);
    editBracketBeginPath(path);
}

std::string compoundPathToDirectoryFormPathToFile(const std::string &pathToDirectory, const std::string &pathToFile)
{
    std::string newPath = pathToDirectory;
    setBracketsPath(newPath);
    if (isBracket(&(pathToFile[0])))
    {
        newPath.append(&(pathToFile[1]));
    }
    else
    {
        newPath.append(pathToFile.c_str());
    }
    return newPath;
}
