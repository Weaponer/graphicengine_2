#include <unistd.h>
#include <iostream>

#include "optionProgram.h"

static void writeStrFromPtrChar(const char *c_str, std::string &str);

OptionProgram::OptionProgram(int argc, char *argv[])
{
    prefixForNewFiles = DEFAULT_NEW_PREFIX;

    for (int i = 0; i < argc; i++)
    {
        processOption(argv[i]);
    }

    initCurrentPath();
    if (!isHaveLibrary())
    {
        pathToLibrary = startPath;
    }
}

void OptionProgram::initCurrentPath()
{
    char *bufferDirectory = new char[256];
    getcwd(bufferDirectory, 256);
    writeStrFromPtrChar(bufferDirectory, startPath);
    delete[] bufferDirectory;
}

void OptionProgram::processOption(const char *c_str)
{
    if (c_str[0] == '-')
    {
        if (c_str[1] == 'i')
        {
            processPathShader(&(c_str[2]));
        }
        else if (c_str[1] == 'o')
        {
            processPathToOutput(&(c_str[2]));
        }
        else if (c_str[1] == 'l')
        {
            processPathToLibrary(&(c_str[2]));
        }
        else if (c_str[1] == 'n')
        {
            processNameOutputFile(&(c_str[2]));
        }
        else if (c_str[1] == 'p')
        {
            processPrefixForNewFiles(&(c_str[2]));
        }
    }
}

static void writeStrFromPtrChar(const char *c_str, std::string &str)
{
    while (*c_str)
    {
        str.push_back(*c_str);
        c_str++;
    }
}

void OptionProgram::processPathShader(const char *c_str)
{
    writeStrFromPtrChar(c_str, pathShader);
}

void OptionProgram::processPathToLibrary(const char *c_str)
{
    writeStrFromPtrChar(c_str, pathToLibrary);
}

void OptionProgram::processPathToOutput(const char *c_str)
{
    writeStrFromPtrChar(c_str, pathToOutput);
}

void OptionProgram::processNameOutputFile(const char *c_str)
{
    writeStrFromPtrChar(c_str, nameOutputFile);
}

void OptionProgram::processPrefixForNewFiles(const char *c_str)
{
    prefixForNewFiles.clear();
    writeStrFromPtrChar(c_str, prefixForNewFiles);
}