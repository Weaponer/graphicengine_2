#include <iostream>
#include <dirent.h>
#include <unistd.h>
#include <algorithm>
#include <vector>

#include "behaviorCompilator.h"
#include "utilityFileManager.h"

BehaviorCompilator::BehaviorCompilator(const OptionProgram *option) : option(option), libraryLoader(option),
 macroLD(), macroI(&libraryLoader), files()
{
    loadFiles();

    if (files.size() > 0)
    {
        std::vector<bool> isChangeFile = processFiles();
        saveFiles(isChangeFile);
        unloadFiles();
    }
    else
    {
        std::cout << "Error:No files found." << std::endl;
    }
}

std::vector<bool> BehaviorCompilator::processFiles()
{
    std::vector<bool> isChangeFile = std::vector<bool>(files.size());

    auto end = files.end();
    int j = 0;
    for (auto i = files.begin(); i != end; ++i, j++)
    {
        int res = applyingMacro(*((*i)->getFileContents()), &macroI);
        if (res == 1)
        {
            std::cout << "Error[macro include] in file: " << *(*i)->getName() << std::endl;
            const std::list<std::string> &errors = macroI.getErrors();

            auto endErrors = errors.end();
            for (auto i2 = errors.begin(); i2 != endErrors; ++i2)
                std::cout << "\t" << *i2 << std::endl;

            macroI.clearErrors();
        }
        if (res == 0)
        {
            isChangeFile[j] = true;
        }

        res = applyingMacro(*((*i)->getFileContents()), &macroLD);
        if (res == 1)
        {
            std::cout << "Error[macro local data] in file: " << *(*i)->getName() << std::endl;
            const std::list<std::string> &errors = macroLD.getErrors();

            auto endErrors = errors.end();
            for (auto i2 = errors.begin(); i2 != endErrors; ++i2)
                std::cout << "\t" << *i2 << std::endl;

            macroLD.clearErrors();
        }
        if (res == 0)
        {
            isChangeFile[j] = true;
        }
    }

    j = 0;
    for (auto i = files.begin(); i != end; ++i, j++)
    {
        if (isChangeFile[j] == false)
        {
            std::cout << "File: " << *(*i)->getName() << " no changed." << std::endl;
        }
    }

    return isChangeFile;
}


void BehaviorCompilator::saveFiles(std::vector<bool> &isChangedFiles)
{
    if (option->isHaveInput() && isChangedFiles[0] == true)
    {
        std::string path = "";
        if (option->isHavePathToOutput())
        {
            path = option->getPathToOutput();
        }
        else
        {
            path = option->getStartPath();
        }
        editEndPath(path);

        if (option->isNameOutputFile())
        {
            File *file = *files.begin();

            path += option->getNameOutputFile();
            file->setNewPath(path);
            file->writeFile();
        }
        else
        {

            File *file = *files.begin();

            path += option->getPrefixForNewFiles();
            path += *(file->getName());

            file->setNewPath(path);
            file->writeFile();
        }
    }
    else
    {
        auto end = files.end();
        int j = 0;
        for (auto i = files.begin(); i != end; ++i, j++)
        {
            if (isChangedFiles[j] == true)
            {
                std::string path = "";
                if (option->isHavePathToOutput())
                {
                    path = option->getPathToOutput();
                }
                else
                {
                    path = option->getStartPath();
                }
                editEndPath(path);
                File *file = *i;

                path += option->getPrefixForNewFiles();
                path += *(file->getName());

                file->setNewPath(path);
                file->writeFile();
            }
        }
    }
}

int BehaviorCompilator::applyingMacro(std::string &str, Macro *macro)
{
    bool isChange = false;

    for (size_t i = 0; i < str.size(); i++)
    {
        if (str[i] == '#')
        {
            if (macro->isMinePrefix(&(str[i])))
            {
                int res = macro->correctString(str, i);
                if (res == 1)
                {
                    return 1;
                }
                else
                {
                    isChange = true;
                }
            }
        }
    }
    if (isChange)
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

void BehaviorCompilator::loadFiles()
{
    if (option->isHaveInput())
    {
        std::string path = "";
        path += option->getStartPath();
        editEndPath(path);
        path += option->getPathShader();
        File *file = new File(path);

        file->readFile();
        if (file->isError() == false)
            files.push_back(file);
        else
            delete file;
    }
    else
    {
        DIR *d;
        dirent *dir;
        d = opendir(".");
        if (d)
        {
            while ((dir = readdir(d)) != NULL)
            {
                if (dir->d_type == DT_REG)
                {
                    std::string path = "";
                    path += option->getStartPath();
                    editEndPath(path);
                    path += std::string(dir->d_name);

                    File *file = new File(path);

                    file->readFile();
                    if (file->isError() == false)
                        files.push_back(file);
                    else
                        delete file;
                }
            }
            closedir(d);
        }
    }
}

void BehaviorCompilator::unloadFiles()
{
    std::for_each(files.begin(), files.end(), [](File *file)
                  { delete file; });

    files.clear();
}