#include <string.h>
#include <iostream>
#include "macroInclude.h"
#include "utilityFileManager.h"

bool MacroInclude::isMinePrefix(const char *c_str) const
{
    const char *pr = PREFIX_INCLUDE;

    while (*pr != 0)
    {
        if(*c_str == 0)
        {
            return false;
        }
        if (*pr != *c_str)
        {
            return false;
        }
        c_str++;
        pr++;
    }
    return true;
}

int MacroInclude::correctString(std::string &str, size_t point)
{
    int starPath = -1;
    int endPath = -1;
    size_t max = point + 300;
    for (size_t i = point; i < str.length() && i < max; ++i)
    {
        if (str[i] == '<')
        {
            if (starPath != -1)
            {
                break;
            }
            starPath = i + 1;
        }

        if (str[i] == '>' && starPath != -1)
        {
            endPath = i - 1;
            break;
        }
    }

    if (starPath != -1 && endPath != -1)
    {
        std::string path = str.substr(starPath, endPath + 1 - starPath);
        str.erase(point, endPath + 2 - point);

        File *file = libraryLoader->loadFile(path);
        if (file->isError())
        {
            addError("MacroInclude: Unable to load: " + *file->getFullPath());
            delete file;
            return 1;
        }
        else
        {
            str.insert(point, *(file->getFileContents()));
            delete file;
            return 0;
        }
    }
    else
    {
        addError("MacroInclude: Structure \"#include <...>\" is not written correctly ");
        return 1;
    }
}