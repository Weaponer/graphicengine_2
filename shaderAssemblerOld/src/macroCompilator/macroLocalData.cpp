#include <string.h>
#include "macroCompilator/macroLocalData.h"

bool MacroLocalData::isMinePrefix(const char *c_str) const
{
    const char *pr = PREFIX_LOCAL_DATA;

    while (*pr != 0)
    {
        if(*c_str == 0)
        {
            return false;
        }
        if (*pr != *c_str)
        {
            return false;
        }
        c_str++;
        pr++;
    }
    return true;
}

int MacroLocalData::correctString(std::string &str, size_t point)
{
    int countBracket = -1;
    int maxSize = str.size();
    size_t endPos;

    for (int i = point; i < maxSize; ++i)
    {
        if (str[i] == '{')
        {
            if (countBracket == -1)
                countBracket = 1;
            else
                countBracket++;
        }
        else if (str[i] == '}')
        {
            if (countBracket < 1)
            {
                addError("MacrosLocalData: Unknown '}'");
                return 1;
            }

            countBracket--;
            if (countBracket == 0)
            {
                endPos = i;
                break;
            }
        }
    }
    if (countBracket != 0)
    {
        addError("MacrosLocalData: Expected '}'");
        return 1;
    }
    str.erase(point, endPos - point + 1);
    return 0;
}