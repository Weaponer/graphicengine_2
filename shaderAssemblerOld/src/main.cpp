#include <iostream>

#include "optionProgram.h"
#include "behaviorCompilator.h"

int main(int argc, char *argv[])
{
    OptionProgram option = OptionProgram(argc, argv);
    BehaviorCompilator compilator = BehaviorCompilator(&option);

    return 0;
}