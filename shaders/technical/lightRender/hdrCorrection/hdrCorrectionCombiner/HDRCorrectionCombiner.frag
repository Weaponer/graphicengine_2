layout(location = 0) in vec2 uv;

layout(rgba16f, binding = 0) uniform readonly image2D _mainTex;
layout(r16f, binding = 1) uniform readonly image2D _hdrValue;

out vec4 result;

void main()
{   
    vec3 value = imageLoad(_mainTex, ivec2(gl_FragCoord.xy)).rgb;
    float hdrCorrection = imageLoad(_hdrValue, ivec2(0, 0)).r;
    value = value / (value + vec3(hdrCorrection)); 
    result = vec4(pow(value, vec3(1.0/2.2)), 1);
}