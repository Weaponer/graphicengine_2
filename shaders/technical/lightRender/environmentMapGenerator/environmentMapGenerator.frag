layout(location = 0) in vec2 uv;
layout(location = 1) in vec3 normalSide_1;
layout(location = 2) in vec3 normalSide_2;
layout(location = 3) in vec3 normalSide_3;
layout(location = 4) in vec3 normalSide_4;
layout(location = 5) in vec3 normalSide_5;
layout(location = 6) in vec3 normalSide_6;


layout(location = 0) out vec4 resultSide_1;
layout(location = 1) out vec4 resultSide_2;
layout(location = 2) out vec4 resultSide_3;
layout(location = 3) out vec4 resultSide_4;
layout(location = 4) out vec4 resultSide_5;
layout(location = 5) out vec4 resultSide_6;

layout(rgba16f, binding = 0) uniform readonly imageCube beforeMipLevel;


ivec3 getCoordFromDirection(vec3 normal)
{
    vec3 absDir = abs(normal);
    float s = 0;
    float t = 0;
    int side = 0;
    if(absDir.x > absDir.y && absDir.x > absDir.z)
    {
        
        if(normal.x >= 0)
        {
            s = normal.z / absDir.x;
            t = -normal.y / absDir.x;   
            side = 4;
        }
        else
        {
            s = -normal.z / absDir.x;
            t = -normal.y / absDir.x;   
            side = 5;
        }
    }
    else if(absDir.y > absDir.x && absDir.y > absDir.z)
    {
        
        if(normal.y >= 0)
        {
            s = normal.z / absDir.y;
            t = normal.x / absDir.y;
            side = 2;
        }
        else
        {
            s = normal.z / absDir.y;
            t = -normal.x / absDir.y;
            side = 3;
        }
    }
    else if(absDir.z > absDir.y && absDir.z > absDir.x)
    {   
        
        if(normal.z >= 0)
        {
            s = -normal.x / absDir.z;
            t = -normal.y / absDir.z;
            side = 0;
        }
        else
        {
            s = normal.x / absDir.z;
            t = -normal.y / absDir.z;
            side = 1;
        }
    }
    vec2 uv = (vec2(s, t) + 1.0) / 2.0;
    ivec2 sizeI = imageSize(beforeMipLevel);
    return ivec3(uv.x * sizeI.x, uv.y * sizeI.y, side); 
}

vec3 getLightEnvironment(vec3 normal, mat3[2] rotates)
{
    int count = 5;
    vec3 sum = imageLoad(beforeMipLevel, getCoordFromDirection((normal * rotates[0]) * rotates[1])).rgb;
    sum += imageLoad(beforeMipLevel, getCoordFromDirection(rotates[0] * (normal * rotates[1]))).rgb;
    sum += imageLoad(beforeMipLevel, getCoordFromDirection(rotates[1] * (normal * rotates[0]))).rgb;
    sum += imageLoad(beforeMipLevel, getCoordFromDirection(rotates[1] * (rotates[0] * normal))).rgb;
    sum += imageLoad(beforeMipLevel, getCoordFromDirection(normal)).rgb;
    return sum / count;
}


void main()
{
    float offsetAngle = radians(0.2);
    mat3[2] rotates;
    rotates[0] = mat3(cos(offsetAngle), 0, sin(offsetAngle),
                        0, 1, 0,
                        -sin(offsetAngle), 0, cos(offsetAngle));

    rotates[1] = mat3(1, 0, 0,
                        0, cos(offsetAngle), -sin(offsetAngle),
                        0, sin(offsetAngle), cos(offsetAngle));

    
    resultSide_1 = vec4(getLightEnvironment(normalize(normalSide_1), rotates), 1);
    resultSide_2 = vec4(getLightEnvironment(normalize(normalSide_2), rotates), 1);
    resultSide_3 = vec4(getLightEnvironment(normalize(normalSide_3), rotates), 1);
    resultSide_4 = vec4(getLightEnvironment(normalize(normalSide_4), rotates), 1);
    resultSide_5 = vec4(getLightEnvironment(normalize(normalSide_5), rotates), 1);
    resultSide_6 = vec4(getLightEnvironment(normalize(normalSide_6), rotates), 1);
}