layout(location = 0) in vec2 vUV;
layout(location = 1) in vec3 vNormalSide_1;
layout(location = 2) in vec3 vNormalSide_2;
layout(location = 3) in vec3 vNormalSide_3;
layout(location = 4) in vec3 vNormalSide_4;
layout(location = 5) in vec3 vNormalSide_5;
layout(location = 6) in vec3 vNormalSide_6;

layout(location = 0) out vec2 uv;
layout(location = 1) out vec3 normalSide_1;
layout(location = 2) out vec3 normalSide_2;
layout(location = 3) out vec3 normalSide_3;
layout(location = 4) out vec3 normalSide_4;
layout(location = 5) out vec3 normalSide_5;
layout(location = 6) out vec3 normalSide_6;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    normalSide_1 = normalize(vNormalSide_1);
    normalSide_2 = normalize(vNormalSide_2);
    normalSide_3 = normalize(vNormalSide_3);
    normalSide_4 = normalize(vNormalSide_4);
    normalSide_5 = normalize(vNormalSide_5);
    normalSide_6 = normalize(vNormalSide_6);
    gl_Position = vec4(vUV * 2.0 - 1.0, 0, 1);
    uv = vUV;
}