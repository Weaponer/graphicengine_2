layout(binding = 0) uniform sampler2D _basicColorBuffer;

out vec4 _result;

void main()
{
    vec3 basicColor = texelFetch(_basicColorBuffer, ivec2(gl_FragCoord.xy), 0).rgb;
    _result = vec4(combineQpaqueGeometryWithTransprent(basicColor), 1);
}