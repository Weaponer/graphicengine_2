#define EPSILON 0.0001

layout(location = 0) in vec2 uv;

layout(binding = 0) uniform sampler2D _depthBuffer;
layout(binding = 1) uniform sampler2D _basicColorBuffer;
layout(binding = 5) uniform sampler3D _FogGridBuffer;

uniform uvec3 _sizeGrid;

out vec4 _result;


vec4 getDataFog(float depth)
{
    float far = min(far_plane, farPlaneFog);
    float near = near_plane + nearPlaneFog;

    vec3 ndc = vec3(uv.x * 2.0 - 1.0, uv.y * 2.0 - 1.0, depth * 2.0 - 1.0);

    vec4 p = matrixInverse_P * vec4(ndc, 1);
    p /= p.w;
    depth = (p.z - near) / (far - near);

    vec2 params = vec2(float(_sizeGrid.z) / log2(far / near), -(float(_sizeGrid.z) * log2(near) / log2(far / near)));

    float view_z = depth * far;
    depth = (max(log2(view_z) * params.x + params.y, 0.0)) / _sizeGrid.z;

    return textureLod(_FogGridBuffer, vec3(uv, depth), 0.0).rgba;
}

void main()
{
    dataTransparentGeometry transparentData = getCenterZTransparentGeometry();

    float depth = texelFetch(_depthBuffer, ivec2(gl_FragCoord.xy), 0).r;
    vec3 light = texelFetch(_basicColorBuffer, ivec2(gl_FragCoord.xy), 0).rgb;
    vec4 fog = getDataFog(depth);


    if(transparentData.haveTransparentGeometry)
    {
        vec4 fogTransparentPos = getDataFog(transparentData.averageZ);
        vec4 difFog = vec4(max(fog.rgb - fogTransparentPos.rgb, vec3(0, 0, 0)), fog.a / fogTransparentPos.a);

        light = light * difFog.a + difFog.rgb;

        vec4 Ct = texelFetch(_transparentBuffer, ivec2(gl_FragCoord.xy), 0).rgba;
        float squareAlpha = texelFetch(_alphaBuffer, ivec2(gl_FragCoord.xy), 0).a;

        light = (Ct.rgb / Ct.a) * (1.0 - squareAlpha) + light * (squareAlpha);
        _result = vec4(max(light * fogTransparentPos.a, EPSILON) + fogTransparentPos.rgb, 1);
    }
    else
    {
        _result = vec4(light * fog.a + fog.rgb, 1);
    }
}