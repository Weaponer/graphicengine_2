layout(location = 0) out vec4 pointAndDirectionLightsForce;

layout(location = 0) in vec2 uv;

layout(binding = 0) uniform sampler2D shadowDirectionLight;

void main()
{
    pointAndDirectionLightsForce = vec4(vec3(0), textureLod(shadowDirectionLight, uv, 0).r);
}