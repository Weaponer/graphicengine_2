layout(location = 0) out vec4 pointAndDirectionLightsForce;
layout(location = 1) out vec4 lightPointsDirection;

layout(location = 0) in vec2 uv;
layout(location = 1) in vec3 normal;

layout(binding = 0) uniform sampler2D depthBuffer;
layout(binding = 1) uniform sampler2D normalBuffer;
layout(binding = 2) uniform samplerCube shadow;

vec3 getPosition(float depth)
{
    depth = depth * 2.0 - 1.0;

    vec4 clipSpacePosition = vec4(vec2(uv * 2.0 - 1.0), depth, 1.0);
    vec4 viewSpacePosition = matrixInverse_P * clipSpacePosition;

    viewSpacePosition /= viewSpacePosition.w;

    vec4 worldSpacePosition = matrixInverse_V * viewSpacePosition;
    return worldSpacePosition.xyz;
}

vec3 getNormal()
{
    return (textureLod(normalBuffer, uv, 0).rgb - 0.5) * 2.0;
}

float getLightShadowForce(vec3 position, vec2 offsetDir)
{
    for(int i = 0; i < 6; i++)
    {
        vec4 posInShadow = (pointLightShadow.matrix_V[i] * vec4(position, 1));
        vec4 posInProject = (pointLightShadow.matrix_P * posInShadow);
        if(abs(posInProject.x / posInProject.w) >= 1 || abs(posInProject.y / posInProject.w) >= 1 || abs(posInProject.z / posInProject.w) >= 1) 
        {
            continue;
        }
        vec3 dir = normalize((position - pointLightShadow.position));
        if(abs(dot(dir, vec3(0, 1, 0))) >= 0.7)
        {
            vec3 side = normalize(cross(dir, vec3(0, 0, 1)));
            vec3 up = normalize(cross(dir, side));
            dir = normalize(dir + (side * offsetDir.x) + (up * offsetDir.y));
        }
        else
        {
            vec3 side = normalize(cross(dir, vec3(0, 1, 0)));
            vec3 up = normalize(cross(dir, side));
            dir = normalize(dir + (side * offsetDir.x) + (up * offsetDir.y));
        }

        float depthShadow = textureLod(shadow, dir, 0).r * 2.0 - 1.0;

        if((posInProject.z / posInProject.w) >= depthShadow)
        {
            return 1;
        }
    }
    return 0;
}

void main()
{
    float depth = textureLod(depthBuffer, uv, 0).r;
    if(depth == 1.0)
    {
        return;
    }

    vec3 normal = getNormal();
    vec3 position = getPosition(depth);

    vec3 pL = pointLightShadow.position;
    vec3 cL = pointLightShadow.color;
    float range = pointLightShadow.range;
    float intensivity = pointLightShadow.intensivity;

    vec3 dir = pL - position;
    float dist = length(dir);
    dir = normalize(dir);
    float dotND = dot(dir, normal);
    if(dist > range || dotND <= 0)
    {
        pointAndDirectionLightsForce = vec4(0);
        lightPointsDirection = vec4(0);
        return;
    }

    float shadowForce = 0;
    int countShadows = 0;
    for(int i = -2; i < 2; i++)
    {
        for(int i2 = -2; i2 < 2; i2++)
        {
            countShadows++;
            shadowForce += getLightShadowForce(position, vec2(float(i) / (range * 10), float(i2) / (range * 10)));
        }
    }

    float finalIntensivity = mix(max(((range - dist) / range) * intensivity * dotND, 0), 0, shadowForce / countShadows);
    
    pointAndDirectionLightsForce = vec4(cL * finalIntensivity, 0);
    lightPointsDirection = vec4(dir * finalIntensivity, 0);
}