layout(location = 0) out vec4 pointAndDirectionLightsForce;
layout(location = 1) out vec4 lightPointsDirection;

layout(location = 0) in vec2 uv;
layout(location = 1) in vec3 normal;

layout(binding = 0) uniform sampler2D depthBuffer;
layout(binding = 1) uniform sampler2D normalBuffer;


vec3 getPosition(float depth)
{
    depth = depth * 2.0 - 1.0;

    vec4 clipSpacePosition = vec4(vec2(uv * 2.0 - 1.0), depth, 1.0);
    vec4 viewSpacePosition = matrixInverse_P * clipSpacePosition;

    viewSpacePosition /= viewSpacePosition.w;

    vec4 worldSpacePosition = matrixInverse_V * viewSpacePosition;
    return worldSpacePosition.xyz;
}

vec3 getNormal()
{
    return (textureLod(normalBuffer, uv, 0).rgb - 0.5) * 2.0;
}


void main()
{
    float depth = textureLod(depthBuffer, uv, 0).r;
    if(depth == 1.0)
    {
        pointAndDirectionLightsForce = vec4(0);
        lightPointsDirection = vec4(0);
        return;
    }

    vec3 normal = getNormal();
    vec3 position = getPosition(depth);

    vec3 sumColorLights = vec3(0);
    vec3 sumLightPointsDirections = vec3(0);

    for(int i = 0; i < countLightsPointsWithoutShadow; i++)
    {
        vec3 pL = pointsLights[i].position;
        vec3 cL = pointsLights[i].color;
        float range = pointsLights[i].range;
        float intensivity = pointsLights[i].intensivity;

        vec3 dir = pL - position;
        float dist = length(dir);
        dir = normalize(dir);
        float dotND = dot(dir, normal);
        if(dist > range || dotND <= 0)
        {
            continue;
        }

        float finalIntensivity = max(((range - dist) / range) * intensivity * dotND, 0);

        sumColorLights += cL * finalIntensivity;
        sumLightPointsDirections += dir * finalIntensivity;
    }

    pointAndDirectionLightsForce = vec4(sumColorLights, 0);
    lightPointsDirection = vec4(sumLightPointsDirections, 0);
}