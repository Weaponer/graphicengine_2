layout(location = 0) in vec2 uv;
layout(location = 1) in vec3 normal;

uniform sampler2D _bloomSubMaps[12];

layout(std140, binding = 0) uniform CombinerData 
{
    uniform int _countSubMaps;
    uniform float _forceBloom;
};

layout(location = 0) out vec3 ResultColor;

void main()
{
    vec3 bloom = vec3(0);
    for(int i = 0; i < _countSubMaps; i++)
    {
        bloom += texture(_bloomSubMaps[i], uv).rgb;
    }
    bloom /= _countSubMaps;
    ResultColor = bloom * _forceBloom;
}