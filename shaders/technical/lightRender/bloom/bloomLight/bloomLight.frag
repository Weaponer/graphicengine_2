layout(location = 0) out vec3 outResult;

layout(location = 0) in vec2 uv;
layout(location = 1) in vec3 normal;

layout(binding = 0) uniform sampler2D _brightColor;

void main()
{
    vec3 bright = vec3(0);
    vec2 brightSize = 1.0 / textureSize(_brightColor, 0); 
    
    vec3 a = texture(_brightColor, uv + vec2(brightSize.x * -2, brightSize.y * 2)).rgb;
    vec3 b = texture(_brightColor, uv + vec2(0, brightSize.y * 2)).rgb;
    vec3 c = texture(_brightColor, uv + vec2(brightSize.x * 2, brightSize.y * 2)).rgb;
    
    vec3 d = texture(_brightColor, uv + vec2(brightSize.x * -2, brightSize.y)).rgb;
    vec3 e = texture(_brightColor, uv + vec2(0, brightSize.y)).rgb;
    vec3 f = texture(_brightColor, uv + vec2(brightSize.x * 2, brightSize.y)).rgb;

    vec3 g = texture(_brightColor, uv + vec2(brightSize.x * -2, brightSize.y * -2)).rgb;
    vec3 h = texture(_brightColor, uv + vec2(0, brightSize.y * -2)).rgb;
    vec3 i = texture(_brightColor, uv + vec2(brightSize.x * 2, brightSize.y * -2)).rgb;

    vec3 j = texture(_brightColor, uv + vec2(-brightSize.x, brightSize.y)).rgb;
    vec3 k = texture(_brightColor, uv + vec2(brightSize.x, brightSize.y)).rgb;
    vec3 l = texture(_brightColor, uv + vec2(-brightSize.x, -brightSize.y)).rgb;
    vec3 m = texture(_brightColor, uv + vec2(brightSize.x, -brightSize.y)).rgb;

    bright = e * 0.125;
    bright += (a + c + g + i) * 0.03125;
    bright += (b + d + f + h) * 0.0625;
    bright += (j + k + l + m) * 0.125;

    outResult = max(bright, 0.0001);
}