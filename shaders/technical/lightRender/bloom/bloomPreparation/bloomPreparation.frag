layout(location = 0) in vec2 uv;
layout(location = 1) in vec3 normal;

uniform sampler2D _mainTex;

layout(location = 0) out vec3 resultColor;

void main()
{
    vec3 col = texture(_mainTex, uv).rgb;
    float brightness = dot(col, vec3(0.2126, 0.7152, 0.0722));
    if(brightness > 0.004)
    {
        resultColor = col;
    }
    else
    {
        resultColor = vec3(0.0, 0.0, 0.0);
    }
}