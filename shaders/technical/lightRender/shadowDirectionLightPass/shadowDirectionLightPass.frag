layout(location = 0) out vec4 pointAndDirectionLightsForce;

layout(location = 0) in vec2 uv;
layout(location = 1) in vec3 normal;

uniform int indexPartion;

layout(binding = 0) uniform sampler2D depthBuffer;

uniform sampler2D _shadowMaps[MAX_COUNT_SHADOW_DIRECTION_LIGHT];

vec3 getPosition(float depth)
{
    depth = depth * 2.0 - 1.0;

    vec4 clipSpacePosition = vec4(vec2(uv * 2.0 - 1.0), depth, 1.0);
    vec4 viewSpacePosition = matrixInverse_P * clipSpacePosition;

    viewSpacePosition /= viewSpacePosition.w;

    vec4 worldSpacePosition = matrixInverse_V * viewSpacePosition;
    return worldSpacePosition.xyz;
}

float getForceShadow(sampler2D _shadow, vec3 _pos, vec3 _distancesBias)
{    
    vec2 texelSize = 1.0 / textureSize(_shadow, 0);
    float offset = _distancesBias.z / (_distancesBias.y - _distancesBias.x);
    float res = 0;
    int count = 0;
    for(float i = -2; i < 2; i += 0.75)
    {
        for(float i2 = -2; i2 < 2; i2 += 0.75)
        {
            vec2 p = _pos.xy + vec2(texelSize.x * i, texelSize.y * i2);
            if(float(fract( abs((i + i2) * 0.5))) > 0.25)
            {
                continue;
            }
            count++;
            float mapDepth = textureLod(_shadow, p, 0).r;
            if(_pos.z - offset > mapDepth)
            {
                res += 1;
            }
        }
    }
    if(count == 0)
    {
        return 0;
    }
    else
    {
        return res / count;
    }
    return res / count;
}

void main()
{
    float depth = textureLod(depthBuffer, uv, 0).r;
    if(depth >= 1.0)
    {
        discard;
    }

    int instance = indexPartion;

    vec3 posSurface = getPosition(depth);
    
    float dist = dot(_directionBoundAlongCamera, posSurface - camPosition);
    if(dist > _shadowDistancesBiases[instance].w)
    {
        discard;
    }

    vec3 posInShadow = (_shadowMatrix_VP[instance] * vec4(posSurface, 1)).xyz;
    if(abs(posInShadow.x) >= 1.0 || abs(posInShadow.y) >= 1.0 || abs(posInShadow.z) >= 1.0 ) 
    {
        discard;
    }
        
    posInShadow = posInShadow * 0.5 + vec3(0.5);
    float forceShadow = getForceShadow(_shadowMaps[instance], posInShadow, _shadowDistancesBiases[instance].xyz);

    float distanceToCamera = distance(posSurface, camPosition);
    float fade = 1.0 - min(1, max(distanceToCamera - startFadeDistance, 0) / (endFadeDistance - startFadeDistance)); 
    pointAndDirectionLightsForce = vec4(1.0 - forceShadow * fade, 0, 0, 0);
}