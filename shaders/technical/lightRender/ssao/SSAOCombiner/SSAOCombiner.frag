layout(location = 0) in vec2 uv;

uniform sampler2D _ssaoTexture;

out vec4 _result;

void main()
{   
    float value = texture(_ssaoTexture, uv).r;
    _result = vec4(value, value, value, 1);
}