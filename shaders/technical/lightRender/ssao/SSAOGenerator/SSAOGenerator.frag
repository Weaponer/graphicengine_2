layout(location = 0) in vec2 uv;
layout(location = 1) in vec3 normal;

layout(binding = 0) uniform sampler2D _depthBuffer;
layout(binding = 1) uniform sampler2D _normalBuffer;

#define MAX_SSAO_ITERATIONS 16

struct IterationData
{
    mat2 _rotate;
    int _countSubIterations;
    float _scale;
};

layout(std140, binding = 1) uniform SSAOGeneratorData 
{
    IterationData iterations[MAX_SSAO_ITERATIONS];
    ivec2 _size;
    uint _countIterations;
    float _radius;
};

out float _result;

float getDepth(vec2 _uv)
{
    float depth = texture(_depthBuffer, _uv).r;
    depth = depth * 2.0 - 1.0;
    return depth;
}

vec3 getPosition(vec2 _uv, float _depth)
{
    vec4 clipSpacePosition = vec4(vec2(_uv * 2.0 - 1.0), _depth, 1.0);
    vec4 viewSpacePosition = matrixInverse_P * clipSpacePosition;

    viewSpacePosition /= viewSpacePosition.w;

    vec4 worldSpacePosition = matrixInverse_V * viewSpacePosition;
    return worldSpacePosition.xyz;
}

vec3 getNormal(vec2 _uv)
{  
    vec3 normal = (texture(_normalBuffer, _uv).rgb - 0.5) * 2.0;
    return normal;
}

float getForceOcclusion(vec3 _pos, float _depth, vec3 _normal, vec2 _uv)
{
    float nearDepth = getDepth(_uv);
    vec3 nearPos = getPosition(_uv, nearDepth);
    vec3 nearNormal = getNormal(_uv);
    
    vec3 dir = nearPos - _pos;
    float dist = dot(dir, _normal);
    
    float force = max(dist / _radius, 0.0);

    if(force > 1.0)
    {
        return 0;
    }
    else
    {
        return force;
    }
}

void main()
{   
    float depth = getDepth(uv);
    if(depth == 1.0)
    {
        _result = 1;
        return;
    }

    vec3 mainPos = getPosition(uv, depth);
    vec3 mainNormal = getNormal(uv);

    float result = 0;
    vec2 texelSize = (1.0 / _size) * 1;
    int count = 0;

    for(int i = 0; i < _countIterations; i++)
    {
        for(int i2 = 0; i2 < iterations[i]._countSubIterations; i2++)
        {
            float force = texelSize.y * iterations[i]._scale * (i2 + 1);

            vec2 offsetUV = (iterations[i]._rotate * vec2(0, 1)) *  force;
            result += getForceOcclusion(mainPos, depth, mainNormal, offsetUV + uv);

            offsetUV = (iterations[i]._rotate * vec2(1, 0)) *  force;
            result += getForceOcclusion(mainPos, depth, mainNormal, offsetUV + uv);

            offsetUV = (iterations[i]._rotate * vec2(0, -1)) *  force;
            result += getForceOcclusion(mainPos, depth, mainNormal, offsetUV + uv);

            offsetUV = (iterations[i]._rotate * vec2(-1, 0)) *  force;
            result += getForceOcclusion(mainPos, depth, mainNormal, offsetUV + uv);
            count += 4;
        }
    }
    
    vec4 clipSpacePosition = vec4(vec2(uv * 2.0 - 1.0), depth, 1.0);
    vec4 viewSpacePosition = matrixInverse_P * clipSpacePosition;

    viewSpacePosition /= viewSpacePosition.w;

    float dist = length(viewSpacePosition.xyz);

    _result = pow(1 - (result / (count) * (1.0 - min(dist / 18, 1))), 8);
}