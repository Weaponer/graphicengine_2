layout(location = 0) in vec2 uv;
layout(location = 1) in vec3 normal;

uniform ivec2 _sizeSource;
uniform ivec2 _sizeDest;

layout(binding = 0) uniform sampler2D _source;

void main()
{
    int difX = int(floor(float(_sizeSource.x) / float(_sizeDest.x)));
    int difY = int(floor(float(_sizeSource.y) / float(_sizeDest.y)));

    vec2 offset = 1.0 / vec2(_sizeSource.x, _sizeSource.y);
    gl_FragDepth = 0;

    for(int i = 0; i < difX; i++)
    {
        for(int i2 = 0; i2 < difY; i2++)
        {
            gl_FragDepth += texture(_source, uv + vec2(offset.x * (i - difX / 2), offset.y * (i - difY / 2)) ).r;
        }
    }

    gl_FragDepth /= difX * difY;
}