layout(location = 0) in vec2 uv;
layout(location = 1) in vec3 normal;

layout(binding = 0) uniform sampler2D _depthBuffer;
layout(binding = 1) uniform sampler2D _lightBuffer;
layout(binding = 2) uniform sampler3D _FogGridBuffer;

uniform uvec3 _sizeGrid;

out vec4 _result;

void main()
{   
    vec3 light = texture(_lightBuffer, uv).rgb;
    float depth = texture(_depthBuffer, uv).r;
    
    float far = min(far_plane, farPlaneFog);
    float near = near_plane + nearPlaneFog;

    vec3 ndc = vec3(uv.x * 2.0 - 1.0, uv.y * 2.0 - 1.0, depth * 2.0 - 1.0);

    vec4 p = matrixInverse_P * vec4(ndc, 1);
    p /= p.w;
    depth = (p.z - near) / (far - near);

    vec2 params = vec2(float(_sizeGrid.z) / log2(far / near), -(float(_sizeGrid.z) * log2(near) / log2(far / near)));

    float view_z = depth * far;
    depth = (max(log2(view_z) * params.x + params.y, 0.0)) / _sizeGrid.z;

    vec4 fog = textureLod(_FogGridBuffer, vec3(uv, depth), 0.0).rgba;
    _result = vec4(light * fog.a + fog.rgb, 1);
}