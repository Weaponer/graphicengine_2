#define M_PI 3.14159265359
#define EPSILON 0.0001

uniform sampler2D _shadowMap;

layout(rgba16f, binding = 0) uniform readonly image3D _FogDensityGrid;
layout(rgba16f, binding = 1) uniform writeonly image3D _FogLightGrid;

layout(r8, binding = 2) uniform readonly image2D _noise;

uniform uvec3 _sizeGrid;
uniform int _index;

layout (local_size_x = 8, local_size_y = 8, local_size_z = 8) in;

float sliceDistance(int z)
{
    const float center = (float(z) + 0.5) / _sizeGrid.z;
    return (near_plane + nearPlaneFog) * pow(min(far_plane, farPlaneFog) / (near_plane + nearPlaneFog), center);
}

float phaseFunction(vec3 Wo, vec3 Wi, float g)
{
    float cos_theta = dot(Wo, Wi);
    float denom = 1.0 + g * g + 2.0 * g * cos_theta;
    return (1.0 / (4.0 * M_PI)) * (1.0 - g * g) / max(pow(denom, 1.5), EPSILON);
}

vec3 coordToWorld(ivec3 coord)
{
    const int _sizeX = imageSize(_noise).x;
    ivec2 coordNoise = (coord.xy + ivec2(0, 1) * coord.z * _sizeX) % _sizeX;
    float offset = imageLoad(_noise, coordNoise).r;

    float far = min(far_plane, farPlaneFog);
    float near = near_plane + nearPlaneFog;
    float z = near * pow(far / near, (float(coord.z) + offset * (1.0 - EPSILON)) / float(_sizeGrid.z));


    vec3 uv = vec3((float(coord.x) + 0.5f) / float(_sizeGrid.x), 
                   (float(coord.y) + 0.5f) / float(_sizeGrid.y), 
                   0);

    vec4 proj = matrix_P * vec4(0, 0, z, 1);

    vec3 ndc = uv;
    ndc.x = 2.0 * ndc.x - 1.0;
    ndc.y = 2.0 * ndc.y - 1.0;
    ndc.z = proj.z / proj.w;

    vec4 world = matrixInverse_P * vec4(ndc, 1);

    world /= world.w;
    
    world = matrixInverse_V * world;

    return world.xyz;
}

void main()
{   
    ivec3 coord = ivec3(gl_GlobalInvocationID); 

    if(sliceDistance(coord.z) > farPlaneRays)
    {
        return;
    }

    vec3 worldPos = coordToWorld(coord);

    vec3 posInShadow = (_shadowMatrix_VP[_index] * vec4(worldPos, 1)).xyz;
    if(abs(posInShadow.x) < 1.0 && abs(posInShadow.y) < 1.0 && abs(posInShadow.z) < 1.0 ) 
    {
        posInShadow = posInShadow * 0.5 + vec3(0.5);
        
        vec3 distanceBias = _shadowDistancesBiases[_index].xyz;
        float offset = distanceBias.z / (distanceBias.y - distanceBias.x);

        ivec2 sizeShadowMap = textureSize(_shadowMap, 0).xy;
        vec2 UVPixel = (1.0 / sizeShadowMap);
        vec2 pixelCenterCoords = vec2(ivec2(sizeShadowMap * posInShadow.xy) + vec2(0.5)) / vec2(sizeShadowMap);
        
        int counter = 1;
        float shadowForce = 0.0;

        for(int i = -1; i <= 1; i += 2)
        {
            for(int i2 = -1; i2 <= 1; i2 += 2)
            {
                vec2 coordShadowMap = pixelCenterCoords + UVPixel * vec2(i, i2);
                float mapDepth = textureLod(_shadowMap, coordShadowMap, 0).r;
                if((posInShadow.z - offset) > mapDepth)
                {
                    shadowForce += 1.0;
                }   
                counter += 1;
            }
        }

        float mapDepth = textureLod(_shadowMap, pixelCenterCoords, 0).r;
        if((posInShadow.z - offset) > mapDepth)
        {
            shadowForce += 1.0;
        }   

        float visibilityValue = 1.0 - (shadowForce / counter);

        vec3 dir = normalize(camPosition - worldPos);

        vec4 resDensity = imageLoad(_FogDensityGrid, coord).rgba;

        vec3 lighting = resDensity.rgb * (environmentLight.rgb * environmentLight.a);
        lighting += visibilityValue * colorSun.a * colorSun.rgb * resDensity.rgb * phaseFunction(dir, directionSun.xyz, anisotropic);

        lighting *= 1.0 - (scattering + absorption);

        vec4 colorAndDensity = vec4(lighting * resDensity.a, resDensity.a);

        imageStore(_FogLightGrid, coord, colorAndDensity);
    }    
}