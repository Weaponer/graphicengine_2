#define M_PI 3.14159265359
#define EPSILON 0.0001

layout(rgba16f, binding = 0) uniform writeonly image3D _FogDensityGrid;

uniform uvec3 _sizeGrid;

float sliceDistance(int z)
{
    const float center = (float(z) + 0.5) / _sizeGrid.z;
    return (near_plane + nearPlaneFog) * pow(min(far_plane, farPlaneFog) / (near_plane + nearPlaneFog), center);
}

float sliceThickness(int z)
{
    return abs(sliceDistance(z + 1) - sliceDistance(z));
}

layout (local_size_x = 8, local_size_y = 8, local_size_z = 8) in;
void main()
{   
    ivec3 coord = ivec3(gl_GlobalInvocationID); 

    float thickness = sliceThickness(coord.z);
    float resDensity = density * thickness + EPSILON;

    vec4 colorAndDensity = vec4(colorFog, resDensity);

    imageStore(_FogDensityGrid, coord, colorAndDensity);
}