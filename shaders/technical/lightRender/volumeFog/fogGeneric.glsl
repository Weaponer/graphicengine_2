layout(std140, binding = 5) uniform fogDataBlock
{
    vec3 colorFog;              //16  0
    float nearPlaneFog;         //4   12
    vec3 directionSun;          //16  16
    float farPlaneFog;          //4   28
    vec4 colorSun;              //16  32
    vec4 environmentLight;      //16  48

    float farPlaneRays;          //4   64

    float anisotropic;          //4   68
    float density;              //4   72
    float scattering;           //4   76
    float absorption;           //4   80
};
