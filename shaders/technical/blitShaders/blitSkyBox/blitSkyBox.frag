layout(location = 0) in vec2 uv;
layout(location = 1) in vec3 normal;

uniform samplerCube skyBox;

out vec4 outColor;

void main()
{
    vec3 N = normalize(normal);
    outColor = textureLod(skyBox, N, 0);
}