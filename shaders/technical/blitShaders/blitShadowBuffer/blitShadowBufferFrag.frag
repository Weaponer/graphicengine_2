in vec2 uv;

uniform sampler2D _mainTex;

out vec4 outColor;

void main()
{
    /*float n = 0.01f;
    float f = 150.0f;
    float depth = texture(_mainTex, uv).r;
    depth = 1 / ((depth + (n+f) / (n - f)) / ( (2*f*n) / (n - f) ));
    depth = (2 * (depth - n)) / (f - n);*/
    float depth = pow(textureLod(_mainTex, uv, 0).r, 1);//pow(1 - );
    outColor = vec4(depth, depth, depth, 1);
}