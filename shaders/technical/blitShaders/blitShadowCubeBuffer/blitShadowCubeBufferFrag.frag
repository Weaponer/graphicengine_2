layout(location = 0) in vec2 uv;
layout(location = 1) in vec3 normal;

uniform samplerCube _mainTex;

out vec4 outColor;

void main()
{
    float depth = 1 - pow(texture(_mainTex, normal).r, 1500);
    outColor = vec4(vec3(depth), 1);
}