layout(location = 0) in vec2 uv;
layout(location = 1) in vec3 normal;

uniform isampler2D _mainTex;

out vec4 outColor;

void main()
{
    outColor = vec4(vec3(texture(_mainTex, uv).r), 1);
}