layout(location = 0) in vec2 uv;
layout(location = 1) in vec3 normal;

uniform sampler2D _mainTex;

out vec4 outColor;

void main()
{
    float depth = texture(_mainTex, uv).r;
    outColor = vec4(pow(depth, 1));
}