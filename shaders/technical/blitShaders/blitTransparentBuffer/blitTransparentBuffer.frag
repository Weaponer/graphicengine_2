in vec2 uv;

layout (binding = 0, r32ui) uniform uimage2D _trnspHeadsBuffer;
layout (binding = 1, r32i) uniform iimage2D _trnspCountUseBuffer;

struct TrspNodeND
{
    vec4 normalDepth;
    uint next;
};

layout (binding = 0, std430) readonly buffer _trnspNodesBuffer
{
    TrspNodeND nodesND[];
};

struct TrspNodeLight
{
    vec4 pointAndDirectionLightsForce;
    vec4 lightPointsDirection;
};

layout (binding = 1, std430) readonly buffer _trnspNodesLightBuffer
{
    TrspNodeLight nodesLight[];
};


struct TrspNodeResult
{
    vec4 color;
};

layout (binding = 2, std430) readonly buffer _trnspNodesResultBuffer
{
    TrspNodeResult nodesResult[];
};

uniform int _trnspMaxNodes;
uniform int _trnspMaxNodesPerPixel;

out vec4 outColor;

struct Frag
{
    vec4 color;
    float depth;
};

void main()
{
    Frag frags[9];
    int count = 0;

    ivec2 coord = ivec2(gl_FragCoord.xy);
    uint index = 0;
    index = imageLoad(_trnspHeadsBuffer, coord).r;

    while ( index != 0xFFFFFFFF && count < 9 )
    {
        frags[count].color = nodesResult[index].color;
        frags[count].depth = nodesND[index].normalDepth.a;
        index = nodesND[index].next;
        count++;
    }

    for ( int i = 1; i < count; i++ )
    {
        Frag toInsert = frags[i];
        uint j = i;
        
        while (j > 0 && toInsert.depth > frags[j-1].depth )
        {
            frags[j] = frags[j-1];
            j--;
        }
        
        frags[j] = toInsert;
    }
    
    outColor = vec4(0);
    for (int i = 0; i < count; i++)
    {
        outColor = mix(outColor, frags[i].color, frags[i].color.a);
    }
}