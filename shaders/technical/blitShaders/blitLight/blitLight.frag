in vec2 uv;

layout(binding = 0) uniform sampler2D _pointAndDirectionLightsForce;
layout(binding = 1) uniform sampler2D _lightPointsDirection;

out vec4 outColor;

void main()
{
    float alpha = texture(_pointAndDirectionLightsForce, uv).a; 
    outColor = vec4(vec3(alpha), 1);
}