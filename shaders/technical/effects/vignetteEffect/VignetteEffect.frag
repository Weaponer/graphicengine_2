layout(location = 0) in vec2 uv;

layout (std140, binding = 0) uniform settingsVignette
{
    vec4 _color;
    float _radius;
    float _pow;
};

out vec4 result;

void main()
{   
    vec2 pos = (uv - 0.5) * 2;
    float mag = length(pos);
    mag = mag - _radius;
    mag = pow(mag, _pow);
    mag = max(0, mag);
    result = vec4(_color.rgb, _color.a * mag);
}