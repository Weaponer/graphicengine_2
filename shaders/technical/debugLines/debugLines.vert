layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec3 vColor;

out gl_PerVertex
{
    vec4 gl_Position;
};

out vec4 vPos;
out vec3 vCol;
out float depth;

void main()
{

    vPos = matrix_VP * vec4(vPosition, 1);
    vCol = vColor;
    gl_Position = vPos;

    depth = gl_Position.z / gl_Position.w;
}