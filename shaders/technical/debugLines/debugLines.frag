uniform sampler2D _depthBuffer;
uniform ivec2 _viewSize;

in vec4 vPos;
in vec3 vCol;
in float depth;

out vec4 vColor;

void main()
{
    vec2 screenPos = vec2(gl_FragCoord.x / _viewSize.x, gl_FragCoord.y / _viewSize.y);
    float bufDepth = texture(_depthBuffer, screenPos).r;
    if(gl_FragCoord.z > bufDepth)
    {
        vColor = vec4(vCol, 0.3);
    }
    else
    {
        vColor = vec4(vCol, 1);
    }
}