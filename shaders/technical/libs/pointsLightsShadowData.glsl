struct pointLightShadowData  
{
    mat4 matrix_V[6];               //16        0
    mat4 matrixInverse_V[6];         //16        384
    mat4 matrix_P;                  //16        768
    mat4 matrixInverse_P;           //16        832
    vec3 color;                     //16        896
    float intensivity;              //4         908
    vec3 position;                  //16        912
    float range;                    //4         924
    float near;                     //4         928
    float far;                      //4         932
};

layout(std140, row_major, binding = 3) uniform pointLightWithShadowsDataBlock
{   
    uniform pointLightShadowData pointLightShadow;   //16    0
};
