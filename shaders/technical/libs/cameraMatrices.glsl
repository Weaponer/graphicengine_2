layout(std140, row_major, binding = 0) uniform cameraMatrices 
{
    mat4 matrix_V;
    mat4 matrix_P;
    mat4 matrix_VP;
    mat4 matrixInverse_P;
    mat4 matrixInverse_V;

    vec3 camPosition;
    float near_plane;
    vec3 camDirection;
    float far_plane;
};
