layout(binding = 2) uniform sampler2D _transparentBuffer;
layout(binding = 3) uniform sampler2D _alphaBuffer;
layout(binding = 4) uniform sampler2D _sumZAndCountBuffer;

vec3 combineQpaqueGeometryWithTransprent(vec3 opaque)
{
    vec4 Ct = texelFetch(_transparentBuffer, ivec2(gl_FragCoord.xy), 0).rgba;
    if(Ct.a == 0)
    {
        return opaque;
    }

    float squareAlpha = texelFetch(_alphaBuffer, ivec2(gl_FragCoord.xy), 0).a;
    return (vec3(Ct.rgb) / Ct.a) * (1.0 - squareAlpha) + opaque * squareAlpha;
}

struct dataTransparentGeometry
{
    bool haveTransparentGeometry;
    float averageZ;
};

dataTransparentGeometry getCenterZTransparentGeometry()
{
    dataTransparentGeometry result;
    vec2 dataPos = texelFetch(_sumZAndCountBuffer, ivec2(gl_FragCoord.xy), 0).rg;
    if(dataPos.y == 0.0)
    {
        result.haveTransparentGeometry = false;
        result.averageZ = 0.0;
    }
    else
    {
        result.haveTransparentGeometry = true;
        result.averageZ = dataPos.x / dataPos.y;
    }
    return result;
}