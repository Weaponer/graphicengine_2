#define MAX_COUNT_SHADOW_DIRECTION_LIGHT 8

layout(std140, row_major, binding = 1) uniform shadowDirectionLightData
{
    uniform mat4 _shadowMatrix_VP[MAX_COUNT_SHADOW_DIRECTION_LIGHT];
    uniform vec4 _shadowDistancesBiases[MAX_COUNT_SHADOW_DIRECTION_LIGHT]; // xy - near and far planes, z - bias, w - distance
    uniform vec3 _directionBoundAlongCamera;
    uniform int _countPartition;
    float startFadeDistance;
    float endFadeDistance;
};
