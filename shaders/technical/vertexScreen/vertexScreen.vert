layout(location = 0) in vec2 vPos;
layout(location = 1) in vec2 vUV;
layout(location = 2) in vec3 vNormal;

layout(location = 0) out vec2 uv;
layout(location = 1) out vec3 normal;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    gl_Position = vec4(vPos.x, vPos.y, 1, 1);
    uv = vUV;
    normal = vNormal;
}