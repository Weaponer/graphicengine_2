layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vUV;
layout(location = 2) in vec3 vNormal;
layout(location = 3) in vec3 vTanget;

struct MatricesInctanceObject
{
    mat4 _matrix_M;
    mat4 _matrixInverse_M;
};

layout (std140, row_major, binding = 1) uniform matricesObjects
{
    MatricesInctanceObject objects[500];
};

out gl_PerVertex
{
    vec4 gl_Position;
};

out VS_OUT {
    mat3 TBN;
    vec3 normal;
    vec2 uv;
} vs_out;

void main()
{
    mat4 matrixM = objects[gl_InstanceID]._matrix_M;

    vec4 normalBase = matrixM * vec4(vNormal, 0);

    vec3 T = normalize(vec3(matrixM * vec4(vTanget, 0)));
    vec3 B = normalize(vec3(matrixM * vec4(normalize(cross(vTanget, vNormal)), 0)));
    vec3 N = normalize(normalBase.xyz);
    
    mat3 TBN = transpose(mat3(T, B, N));
    vs_out.TBN = TBN;

    vs_out.uv = vUV;
    vs_out.uv.y = 1.0 - vs_out.uv.y;
    vs_out.normal = normalBase.xyz;

    vec4 posBase = matrixM * vec4(vPosition, 1);
    gl_Position = matrix_VP * posBase;
}

