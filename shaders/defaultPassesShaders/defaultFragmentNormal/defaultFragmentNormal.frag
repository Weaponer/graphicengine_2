in VS_IN {
    mat3 TBN;
    vec3 normal;
    vec2 uv;
} fs_in;

layout(location = 1) uniform sampler2D normalMap;

out vec4 result;

void main()
{
    vec3 N = normalize((texture(normalMap, fs_in.uv).rgb * 2.0 - 1.0) * fs_in.TBN);
    N += normalize(fs_in.normal);
    N = normalize(N);
    result =  vec4(N / 2 + vec3(0.5), 1);
}