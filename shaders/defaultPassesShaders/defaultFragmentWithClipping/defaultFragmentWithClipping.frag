in VS_IN {
    mat3 TBN;
    vec3 normal;
    vec2 uv;
} fs_in;

layout(location = 0) uniform sampler2D clippingTexture;

layout(std140, binding = 2) uniform clippingData
{
    vec4 maskChanel;
    float value;
    bool inverse;
};

out vec4 result;

void main()
{
    vec4 tex = texture(clippingTexture, fs_in.uv).rgba;
    float force = maskChanel.r * tex.r + maskChanel.g * tex.g + maskChanel.b * tex.b + maskChanel.a * tex.a;
    if(inverse)
    {
        force = 1.0 - force;
    }
    if(force < value)
    {
        discard;
    }
    result =  vec4(1, 1, 1, 1);
}