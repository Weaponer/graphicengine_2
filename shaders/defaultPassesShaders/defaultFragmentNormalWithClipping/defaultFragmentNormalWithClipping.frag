in VS_IN {
    mat3 TBN;
    vec3 normal;
    vec2 uv;
} fs_in;

layout(location = 0) uniform sampler2D clippingTexture;
layout(location = 1) uniform sampler2D normalMap;


layout(std140, binding = 2) uniform clippingData
{
    vec4 maskChanel;
    float value;
    bool inverse;
};

out vec4 result;

void main()
{
    vec4 tex = texture(clippingTexture, fs_in.uv).rgba;
    float force = maskChanel.r * tex.r + maskChanel.g * tex.g + maskChanel.b * tex.b + maskChanel.a * tex.a;
    if(inverse)
    {
        force = 1.0 - force;
    }
    if(force < value)
    {
        discard;
    }

    vec3 N = normalize((texture(normalMap, fs_in.uv).rgb * 2.0 - 1.0) * fs_in.TBN);
    N += normalize(fs_in.normal);
    N = normalize(N);
    result =  vec4(N / 2 + vec3(0.5), 1);
}