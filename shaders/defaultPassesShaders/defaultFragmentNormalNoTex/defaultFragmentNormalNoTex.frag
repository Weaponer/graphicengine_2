in VS_IN {
    mat3 TBN;
    vec3 normal;
    vec2 uv;
} fs_in;

out vec4 result;

void main()
{
    vec3 N = normalize(fs_in.normal);
    result =  vec4(N / 2 + vec3(0.5), 1);
}