Shader = {
    Name = "CubeSky",
    Properties = {
        _ReflectionTexture = {
            type = "Texture",
        },
    },
    SubShader = {
        ReflectionPass = {
            NamePass = "ReflectionPass",
            TypePass = "BasicColor",
            Priority = 0,

            Cull = "Off",

            Includes = {
                "shaders/newShaders/libs/skyData.glsl",
                "shaders/newShaders/libs/sky.glsl"
            },

            Vertex = "shaders/newShaders/skyShaders/cubeSky/vert.vert",
            Fragment = "shaders/newShaders/skyShaders/cubeSky/frag.frag",
        },
    },
}