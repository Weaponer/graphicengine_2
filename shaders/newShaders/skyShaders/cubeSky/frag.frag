uniform sampler2D _ReflectionTexture;

const float PI = 3.14159265359;


vec3 getLightEnvironment(vec3 normal)
{
    vec3 N = normalize(normal);
    vec2 uv = vec2(atan(N.x, N.z), asin(-N.y) * 2.0);
    uv /= PI;
    uv = uv / 2 + 0.5;
    return texture(_ReflectionTexture, uv).rgb * _intensityAmbientLight * _ambientLightColor.rgb;
}