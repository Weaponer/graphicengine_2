uniform vec4 _SkyColor;
uniform vec4 _GroundColor;
uniform vec4 _ColorEdgeHorizont;

uniform float rangeBlackoutCameraSky = 1;
uniform float rangeBlackoutSunSky = 0.2;

float getIntesivitySunForSize(float size, float dotSun)
{
   return pow(1.3 * pow(dotSun, 10 / size), 5.5 );
}

vec3 getLightEnvironment(vec3 normal)
{
    float sizeSun = 0.006 * _intensity;

    vec3 N = normalize(normal);

    float dotUp = dot(vec3(0, 1, 0), N);
    float dotSun = clamp(dot(_directionSun, N), 0, 1);
    float dotSunToCenterCamera = 0.7f;

    float stateSun = dot(vec3(0, 1, 0), _directionSun);
    stateSun = clamp(stateSun, 0, 1);


    //back

    float blackoutSky = max(0, 1 - pow(( 1 - stateSun) * rangeBlackoutCameraSky,2) - pow((1 - dotSunToCenterCamera) * rangeBlackoutSunSky, 2 ));
    float skyCoef = dotUp;
    skyCoef = clamp(skyCoef, 0, 1);
    skyCoef = pow(skyCoef, 0.2);
    vec3 colorBack = _GroundColor.rgb * (1 - skyCoef) + _SkyColor.rgb * skyCoef * blackoutSky;
    
    //sun
    float intesity = getIntesivitySunForSize(sizeSun, dotSun);
    intesity = intesity * ( 1 - step(dotUp,0));
    vec3 sun = _colorSun.rgb * intesity;

    //sun blum effect 
    vec3 intesityBlum =  pow(dotSun * dotSunToCenterCamera, 128) * _colorSun.rgb * 0.2;
    intesityBlum += sqrt(dotSun * dotSun +  dotSunToCenterCamera * dotSunToCenterCamera) * 0.2 * _colorSun.rgb;

    //horizontal
    float horizontLight = 1 - abs(dotUp);
    horizontLight = pow(horizontLight, 10) * 0.8;

    float horizontSun = horizontLight + pow(pow(horizontLight, 4.9) * pow(intesity, 1.6), 0.1);
    horizontSun = horizontSun + intesity * 0.4;

    vec3 light =  mix(horizontSun * _ColorEdgeHorizont.rgb + intesity * _colorSun.rgb, vec3(horizontLight) + intesity * _colorSun.rgb, stateSun) * 0.8;
    colorBack = mix(colorBack, light, horizontLight * 0.4); 
    colorBack = mix(colorBack, light, horizontSun  * 0.4); 
    colorBack += light * stateSun * 0.4;
    colorBack += intesityBlum;
    return colorBack;
}   