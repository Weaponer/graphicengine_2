Shader = {
    Name = "DefaultSky",
    Properties = {
        _SkyColor = {
            type = "Color",
            default = { r = 116, g = 142, b = 168, a = 255},
        },
        _GroundColor = {
            type = "Color",
            default = { r = 68, g = 63, b = 67, a = 255 },
        },
        _ColorEdgeHorizont = {
            type = "Color",
            default = { r = 209, g = 90, b = 0, a = 255 },
        },
    },
    SubShader = {
        SkyPass = {
            NamePass = "SkyPass",
            TypePass = "BasicColor",

            Cull = "Off",

            Includes = {
                "shaders/newShaders/libs/skyData.glsl",
                "shaders/newShaders/libs/sky.glsl",
            },
            Vertex = "shaders/newShaders/skyShaders/defaultSky/vert.vert",
            Fragment = "shaders/newShaders/skyShaders/defaultSky/frag.frag",
        },
    }
}