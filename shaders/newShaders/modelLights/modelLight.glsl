#define FRAGMENT_SHADER

layout(location = 0) in vec2 uv;

layout(binding = 0) uniform samplerCube _environmentBuffer;
layout(binding = 1) uniform samplerCube _reflectionBuffer;

layout (rgba16f, binding = 0) uniform readonly image2D _pointAndDirectionLightsForceBuffer;
layout (rgba16f, binding = 1) uniform readonly image2D _lightPointsDirectionBuffer;

//G buffer
layout (binding = 2) uniform sampler2D depthBuffer;

layout (binding = 3) uniform sampler2D layer1; // normals

layout (rgba16f, binding = 4) uniform readonly image2D layer2;
layout (rgba16f, binding = 5) uniform readonly image2D layer3;

#ifdef USE_ADDITIONAL_LAYER_1
layout (rgba16f, binding = 6) uniform readonly image2D layer4;
#endif

#ifdef USE_ADDITIONAL_LAYER_2
layout (rgba16f, binding = 7) uniform readonly image2D layer5;
#endif
//

//model light
layout(location = 8) uniform uint _modelLight;

//output
layout(location = 0) out vec4 resultHDR;

struct SurfaceData
{
    vec3 normal;
    vec3 position;
    vec3 albedo;
    float metallic;
    float roughness;
    float ao;
    float additionalChanel_1;
    
    #ifdef USE_ADDITIONAL_LAYER_1
    vec4 additionalDataLayer_1;
    #endif

    #ifdef USE_ADDITIONAL_LAYER_1
    vec4 additionalDataLayer_2;
    #endif
};

struct LightData
{
    vec3 view;
    vec3 forcePointsLight;
    vec3 directionPointsLight;
    float forceDirectionLight;
};

vec3 getPosition()
{
    float depth = texelFetch(depthBuffer, ivec2(gl_FragCoord.xy), 0).r * 2.0 - 1.0;

    vec4 clipSpacePosition = vec4(vec2(uv * 2.0 - 1.0), depth, 1.0);
    vec4 viewSpacePosition = _matrixInverse_P * clipSpacePosition;

    viewSpacePosition /= viewSpacePosition.w;

    vec4 worldSpacePosition = _matrixInverse_V * viewSpacePosition;
    return worldSpacePosition.xyz;
}

vec3 getLight(SurfaceData surf, LightData light);

void main()
{
    vec4 layer2Data = imageLoad(layer2, ivec2(gl_FragCoord.xy)).rgba;
    uint currentLightModel = uint(layer2Data.a);
    
    if(currentLightModel != _modelLight)
    {
        resultHDR = vec4(0, 0, 0, 1);
        return;
    }

    LightData light;
    vec4 PDLF = imageLoad(_pointAndDirectionLightsForceBuffer, ivec2(gl_FragCoord.xy));
    vec3 LPD = imageLoad(_lightPointsDirectionBuffer, ivec2(gl_FragCoord.xy)).rgb;

    light.forceDirectionLight = PDLF.a;
    light.forcePointsLight = PDLF.rgb;
    light.directionPointsLight = LPD;
    vec4 view = _matrixInverse_P * vec4(vec2(uv * 2.0 - 1.0), 1.0, 1.0);
    view /= view.w;
    view = _matrixInverse_V * view;
    light.view = -normalize(view.xyz - position);

    SurfaceData surfData;
    surfData.normal = (texelFetch(layer1, ivec2(gl_FragCoord.xy), 0).rgb - 0.5) * 2;
    surfData.position = getPosition();
    surfData.albedo = layer2Data.rgb;
    vec4 layer3Data = imageLoad(layer3, ivec2(gl_FragCoord.xy)).rgba;
    surfData.metallic = layer3Data.r;
    surfData.roughness = layer3Data.g;
    surfData.ao = layer3Data.b;
    surfData.additionalChanel_1 = layer3Data.a;


    #ifdef USE_ADDITIONAL_LAYER_1
    surfData.additionalDataLayer_1 = imageLoad(layer4, ivec2(gl_FragCoord.xy)).rgba;
    #endif

    #ifdef USE_ADDITIONAL_LAYER_2
    surfData.additionalDataLayer_2 = imageLoad(layer5, ivec2(gl_FragCoord.xy)).rgba;
    #endif

    vec3 resultLight = getLight(surfData, light);
    resultHDR = vec4(resultLight, 0);
}