vec3 getLight(SurfaceData surf, LightData light)
{
    PBRData pbrData;
    pbrData.view = light.view;
    pbrData.albedo = surf.albedo;
    pbrData.normal = surf.normal;
    pbrData.roughness = surf.roughness;
    pbrData.metallic = surf.metallic;
    pbrData.alpha = 1.0;
    pbrData.ao = surf.ao;

    return PBRLight(pbrData, vec4(light.forcePointsLight, light.forceDirectionLight), vec3(light.directionPointsLight)).rgb;
}