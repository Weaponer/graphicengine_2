vec3 getLight(SurfaceData surf, LightData light)
{
    LambertData lambertData;
    lambertData.view = light.view;
    lambertData.albedo = surf.albedo;
    lambertData.normal = surf.normal;
    lambertData.alpha = 1.0;

    return LambertLight(lambertData, vec4(light.forcePointsLight, light.forceDirectionLight), vec3(light.directionPointsLight)).rgb;
}