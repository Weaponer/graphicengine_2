#ifdef FRAGMENT_SHADER

layout(early_fragment_tests) in;

layout (std140, binding = 2) uniform skyData
{
    vec3 _directionSun;
    float _intensity;
    vec4 _colorSun;
    vec3 _ambientLightColor;
    float _intensityAmbientLight;
    int _environmentBufferMaxLevel;
    bool _environmentBufferAsAmbientLight;
};

layout (rgba16f, binding = 0) uniform readonly image2D _pointAndDirectionLightsForceBuffer;
layout (rgba16f, binding = 1) uniform readonly image2D _lightPointsDirectionBuffer;

#define USE_IMAGE_LIGHT_SURFACE_DATA

uniform samplerCube _environmentBuffer;

#endif