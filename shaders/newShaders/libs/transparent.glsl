#ifdef FRAGMENT_SHADER

layout(location = 0) out vec4 transparentColor;
layout(location = 1) out vec4 transparentSquareAlpha;
layout(location = 2) out vec4 transparentSumZAndCount;

vec4 transparentMain();

float weigthTransparent(float alpha)
{
    float d = ((near_plane - far_plane) / gl_FragCoord.z - far_plane) / (near_plane - far_plane);
    return alpha * max(pow(10, -2), 3 * pow(10, 3) * pow(1.0 - pow(d, -3), 3));
}

void main()
{
    vec4 transparent = transparentMain();

    float weight = weigthTransparent(transparent.a);

    transparentColor = vec4(transparent.rgb * weight, weight);
    transparentSquareAlpha = vec4(1.0 - transparent.a);
    transparentSumZAndCount = vec4(gl_FragCoord.z, 1.0, 0.0, 0.0);
}

#endif