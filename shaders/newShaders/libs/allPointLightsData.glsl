#ifndef POINT_LIGHT_DATA
#define POINT_LIGHT_DATA
    struct pointLightData   //32
    {
        vec3 color;         //16 0
        float intensivity;  //4  12
        vec3 position;      //16 16
        float range;        //4  28
    };
#endif

layout(std140, binding = 4) uniform allPointsLightsDataBlock
{   
    uniform pointLightData allPointsLights[16];    //16    0
    uniform int countLightsPoints;                  //4     512
};