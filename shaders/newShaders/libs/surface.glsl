#ifdef FRAGMENT_SHADER

layout(location = 0) out vec4 layer1;
layout(location = 1) out vec4 layer2;
layout(location = 2) out vec4 layer3;

#ifdef USE_ADDITIONAL_LAYER_1
layout(location = 3) out vec4 layer4;
#endif

#ifdef USE_ADDITIONAL_LAYER_2
layout(location = 4) out vec4 layer5;
#endif

uniform int _modelLight;

struct SurfaceData
{
    vec3 normal;
    vec3 albedo;
    float metallic;
    float roughness;
    float ao;
    float additionalChanel_1;
    
    #ifdef USE_ADDITIONAL_LAYER_1
    vec4 additionalDataLayer_1;
    #endif

    #ifdef USE_ADDITIONAL_LAYER_1
    vec4 additionalDataLayer_2;
    #endif
};

void setSurfaceData(SurfaceData data)
{
    layer1.rgba = vec4(data.normal / 2.0 + vec3(0.5), 0);
    layer2.rgba = vec4(data.albedo, float(_modelLight));
    layer3.r = data.metallic;
    layer3.g = data.roughness;
    layer3.b = data.ao;
    layer3.a = data.additionalChanel_1;

    #ifdef USE_ADDITIONAL_LAYER_1
    layer4.rgba = data.additionalDataLayer_1;
    #endif

    #ifdef USE_ADDITIONAL_LAYER_2
    layer5.rgba = data.additionalDataLayer_2;
    #endif
}

#endif