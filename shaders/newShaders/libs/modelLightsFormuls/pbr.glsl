#ifdef FRAGMENT_SHADER

const float PI = 3.14159265359;

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a2 = roughness * roughness;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / max(denom, 0.001);
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float nom   = NdotV;
    float denom = NdotV * (1.0 - roughness) + roughness;

    return nom / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return max(F0, F0 + vec3( (max(vec3(1.0 - roughness), F0) - F0) ) * pow(1.0 - cosTheta, 5.0));
}


vec3 lightFromDirect(vec3 radiance, vec3 L, vec3 N, vec3 H, vec3 V, float metallic, vec3 albedo, float roughness, vec3 F0)
{
    float NDF = DistributionGGX(N, H, roughness);
    float G = GeometrySmith(N, V, L, roughness);
    vec3 F = fresnelSchlick(max(dot(H, V), 0), F0);
                
    vec3 nominator = NDF * G * F;
    float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
    vec3 specular = nominator / max(denominator, 0.001);

    vec3 kS = F;

    vec3 kD = vec3(1.0) - kS;
    
    kD *= 1.0 - metallic;
    
    float NdotL = max(dot(N, L), 0.0);
    return (kD * albedo / PI + specular) * radiance * NdotL;
}

vec3 lightFromMultiLightPoints(vec3 radiance, vec3 L, vec3 N, vec3 H, vec3 V, float metallic, vec3 albedo, float roughness, vec3 F0)
{
    float NDF = DistributionGGX(N, H, roughness);
    float G = GeometrySmith(N, V, L, roughness);
    vec3 F = fresnelSchlick(max(dot(H, V), 0), F0);
                
    vec3 nominator = NDF * G * F;
    float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
    vec3 specular = nominator / max(denominator, 0.001);

    vec3 kS = F;

    vec3 kD = vec3(1.0) - kS;

    kD *= 1.0 - metallic;
    
    return (kD * albedo / PI + specular) * radiance;
}

struct PBRData
{
    vec3 albedo;
    vec3 normal;
    vec3 view;
    float roughness;
    float metallic;
    float ao;
    float alpha;
};

vec4 PBRLight(PBRData pbrData, vec4 PDLF, vec3 LPD)
{
    float forceDirectionLight = PDLF.a;
    vec3 forceLightPoints = PDLF.rgb;
    vec3 directionLightPoints = normalize(LPD);

    vec3 V = pbrData.view;

    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, pbrData.albedo.rgb, pbrData.metallic);

    vec3 L0 = vec3(0);

    if(forceDirectionLight != 0)
    {
        L0 += lightFromDirect(_colorSun.rgb * _intensity * forceDirectionLight, _directionSun, pbrData.normal, normalize(V + _directionSun), V,
                                     pbrData.metallic, pbrData.albedo, pbrData.roughness, F0);
    }

    L0 += lightFromMultiLightPoints(forceLightPoints, directionLightPoints, pbrData.normal, normalize(V + directionLightPoints), V,
                                   pbrData.metallic, pbrData.albedo, pbrData.roughness, F0);

    if(_environmentBufferAsAmbientLight)
    {
        vec3 dirAimbentLight = normalize(-reflect(V, pbrData.normal));
        vec3 irradiance = textureLod(_environmentBuffer, dirAimbentLight, pbrData.roughness * float(_environmentBufferMaxLevel)).rgb;
        
        vec3 kS = fresnelSchlickRoughness(max(dot(pbrData.normal, V), 0), F0, pbrData.roughness);
        vec3 kD = 1.0 - kS;

        vec3 diffuse = irradiance * pbrData.albedo;
        vec3 ambient = (kD * diffuse);
        
        L0 += ambient * pbrData.ao;
    }
    else
    {
        L0 += _intensityAmbientLight * _ambientLightColor.rgb * pbrData.ao;
    }
    return vec4(L0, pbrData.alpha);
}

#ifdef USE_IMAGE_LIGHT_SURFACE_DATA
    vec4 PBRLight(PBRData pbrData)
    {
        ivec2 screenPos = ivec2(gl_FragCoord.x, gl_FragCoord.y);
        vec4 PDLF = imageLoad(_pointAndDirectionLightsForceBuffer, screenPos).rgba;
        vec3 LPD = imageLoad(_lightPointsDirectionBuffer, screenPos).rgb;

        return PBRLight(pbrData, PDLF, LPD);
    }
#endif
#endif