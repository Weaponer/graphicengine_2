#ifdef FRAGMENT_SHADER

struct LambertData
{
    vec3 albedo;
    vec3 normal;
    vec3 view;
    float alpha;
};

vec4 LambertLight(LambertData lambertData, vec4 PDLF, vec3 LPD)
{
    float forceDirectionLight = PDLF.a;
    vec3 forceLightPoints = PDLF.rgb;
    vec3 directionLightPoints = normalize(LPD);

    vec3 V = normalize(lambertData.view);

    vec3 L0 = vec3(0);

    if(forceDirectionLight != 0)
    {
        float lamb = max(0.0, dot(lambertData.normal, _directionSun));
        L0 += lamb * _colorSun.rgb * _intensity * forceDirectionLight;
    }

    L0 += forceLightPoints;

    vec3 dirAimbentLight = -reflect(V, lambertData.normal);
    L0 += _ambientLightColor * _intensityAmbientLight;

    return vec4(L0, lambertData.alpha);
}

#ifdef USE_IMAGE_LIGHT_SURFACE_DATA
    vec4 LambertLight(LambertData lambertData)
    {
        ivec2 screenPos = ivec2(gl_FragCoord.x, gl_FragCoord.y);
        vec4 PDLF = imageLoad(_pointAndDirectionLightsForceBuffer, screenPos).rgba;
        vec3 LPD = imageLoad(_lightPointsDirectionBuffer, screenPos).rgb;

        return LambertLight(lambertData, PDLF, LPD);
    }
#endif
#endif