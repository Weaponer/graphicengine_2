#ifdef FRAGMENT_SHADER

layout(early_fragment_tests) in;

uniform samplerCube _environmentBuffer;

//skyData.glsl
layout (std140, binding = 2) uniform skyData
{
    vec3 _directionSun;
    float _intensity;
    vec4 _colorSun;
    vec3 _ambientLightColor;
    float _intensityAmbientLight;
    int _environmentBufferMaxLevel;
    bool _environmentBufferAsAmbientLight;
};

//allPointLightsData.glsl
#ifndef POINT_LIGHT_DATA
#define POINT_LIGHT_DATA
    struct pointLightData   //32
    {
        vec3 color;         //16 0
        float intensivity;  //4  12
        vec3 position;      //16 16
        float range;        //4  28
    };
#endif

layout(std140, binding = 4) uniform allPointsLightsDataBlock
{   
    uniform pointLightData allPointsLights[16];    //16    0
    uniform int countLightsPoints;                  //4     512
};

struct LightContactData
{
    vec3 PLD; //point light direction
    vec3 PLF; //point light force
    float forceDirectionLight;
};


LightContactData getLightContactData(vec3 _normal, vec3 _globalPos)
{
    LightContactData data;
    data.PLD = vec3(0);
    data.PLF = vec3(0);
    data.forceDirectionLight = 1.0;

    for(int i = 0; i < countLightsPoints; i++)
    {
        float r = allPointsLights[i].range;
        float intens = allPointsLights[i].intensivity;
        vec3 color = allPointsLights[i].color;
        vec3 pos = allPointsLights[i].position;

        vec3 dir = pos - _globalPos;
        float dist = length(dir);
        dir = normalize(dir);
        float dotND = dot(dir, _normal);
        if(dist > r || dotND <= 0)
        {
            continue;
        }

        float finalIntensivity = max(((r - dist) / r) * intens * dotND, 0);

        data.PLF += color * finalIntensivity;
        data.PLD += dir * finalIntensivity;
    }
    return data;
}

#endif