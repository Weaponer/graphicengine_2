#ifdef USE_INSTANCING

struct MatricesInctanceObject
{
    mat4 _matrix_M;
    mat4 _matrixInverse_M;
};

layout (std140, row_major, binding = 1) uniform matricesObjects
{
    MatricesInctanceObject objects[MAX_COUNT_INSTANC_OBJECTS];
};

#else

layout (std140, row_major, binding = 1) uniform matricesObject
{
    mat4 _matrix_M;
    mat4 _matrixInverse_M;
};

#endif

