layout (std140, row_major, binding = 0) uniform matricesCamera
{
    mat4 _matrix_V;
    mat4 _matrix_P;
    mat4 _matrix_VP;
    mat4 _matrixInverse_P;
    mat4 _matrixInverse_V;

    vec3 position;
    float near_plane;
    vec3 direction;
    float far_plane;
};