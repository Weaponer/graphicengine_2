layout (std140, binding = 2) uniform skyData
{
    vec3 _directionSun;
    float _intensity;
    vec4 _colorSun;
    vec3 _ambientLightColor;
    float _intensityAmbientLight;
    int _environmentBufferMaxLevel;
    bool _environmentBufferAsAmbientLight;
};