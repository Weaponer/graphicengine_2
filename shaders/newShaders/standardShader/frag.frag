#ifndef SURFACE_PASS
    #ifndef RENDER_TYPE_TRANSPARENT
        out vec4 fcol;
    #endif
#endif

#ifdef DEPTH_PASS
    void main()
    {
        fcol =  vec4(1, 1, 1, 1);
    }
#endif

#ifdef SHADOW_PASS
    void main()
    {
        fcol =  vec4(1, 1, 1, 1);
    }
#endif

#ifdef NORMAL_PASS
    #ifdef RENDER_TYPE_TRANSPARENT
    #else
        #define STANDARD_NORMAL
    #endif
#endif

#ifdef DEPTH_NORMAL_PASS 
    #ifdef RENDER_TYPE_TRANSPARENT
        vec4 normalDepthTransparent()
        {
            return vec4(normalize(fs_in.normal) / 2 + vec3(0.5), gl_FragCoord.z);
        }
    #else
        #define STANDARD_NORMAL
    #endif
#endif

#ifdef STANDARD_NORMAL
    #ifdef USE_TEXTURES
        uniform sampler2D _NormalTex;
        uniform vec4 _ScaleOffset;
    #endif

    void main()
    {
        #ifdef USE_TEXTURES
            vec2 uv = fract(fs_in.uv * _ScaleOffset.xy + _ScaleOffset.zw);

            vec3 normal = normalize((texture(_NormalTex, uv).rgb * 2.0 - 1.0) * fs_in.TBN);
            normal += normalize(fs_in.normal);
            normal = normalize(normal);
        #else
            vec3 normal = normalize(fs_in.normal);
        #endif

        fcol = vec4(normal / 2 + vec3(0.5), 1);
    }
#endif

#ifdef BASE_COLOR_PASS

    #ifdef USE_TEXTURES
        uniform sampler2D _MainTex;
        uniform sampler2D _NormalTex;
        uniform sampler2D _MetallicTex;
        uniform sampler2D _RoughnessTex;
        uniform sampler2D _AOTex;
        uniform vec4 _ScaleOffset;
    #endif

    uniform vec4 _Color;
    uniform float _Metallic;
    uniform float _Roughness;
    uniform float _AO;

    #ifdef RENDER_TYPE_TRANSPARENT
        float depthTransparent()
        {
            return gl_FragCoord.z;
        }
    #endif

#ifdef RENDER_TYPE_TRANSPARENT
        vec4 transparentMain()
        {
            vec3 normal = normalize(fs_in.normal);
            vec3 globalPos = fs_in.pos;
            LightContactData lcd = getLightContactData(normal, globalPos);

            PBRData pbrData;
            pbrData.view = normalize(fs_in.view);
            pbrData.albedo = _Color.rgb;
            pbrData.metallic = _Metallic;
            pbrData.roughness = _Roughness;
            pbrData.ao = 1.0;
            pbrData.normal = normal;
            pbrData.alpha = _Color.a;
            return PBRLight(pbrData, vec4(lcd.PLF, lcd.forceDirectionLight), lcd.PLD);
        }
#else
        void main()
        {  
            #ifdef USE_TEXTURES
                vec2 uv = fract(fs_in.uv * _ScaleOffset.xy + _ScaleOffset.zw);
            #else
                vec2 uv = fs_in.uv;
            #endif

            #ifdef USE_TEXTURES
                vec4 tex = texture(_MainTex, uv) * _Color;
            #else
                vec4 tex = _Color;
            #endif

            PBRData pbrData;
            pbrData.view = normalize(fs_in.view);
            
            pbrData.albedo = tex.rgb;

            #ifdef USE_TEXTURES
                pbrData.metallic = texture(_MetallicTex, uv).r * _Metallic;
                pbrData.roughness = texture(_RoughnessTex, uv).a * _Roughness;
                pbrData.ao = texture(_AOTex, uv).a * _AO;

                vec3 normal = normalize((texture(_NormalTex, uv).rgb * 2.0 - 1.0) * fs_in.TBN );
                normal += normalize(fs_in.normal);
                normal = normalize(normal);
                pbrData.normal = normal;

            #else
                pbrData.metallic = _Metallic;
                pbrData.roughness = _Roughness;
                pbrData.normal = normalize(fs_in.normal);
                pbrData.ao = _AO;
            #endif
            pbrData.alpha = tex.a;

            fcol = PBRLight(pbrData);           
        }
#endif

#endif

#ifdef SURFACE_PASS
        uniform vec4 _Color;
        uniform float _Metallic;
        uniform float _Roughness;

        void main()
        {
            SurfaceData surf;
            surf.normal = normalize(fs_in.normal);
            surf.albedo = _Color.rgb;
            surf.metallic = _Metallic;
            surf.roughness = _Roughness;
            surf.ao = 1;
            setSurfaceData(surf);
        }
#endif