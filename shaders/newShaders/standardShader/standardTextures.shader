Shader = {
    Name = "StandardTextures",
    Properties = {
        _MainTex = {
            type = "Texture",
            default = "textures/white.bmp",
        },
        _NormalTex = {
            type = "Texture",
            default = "textures/white.bmp",
        },
        _MetallicTex = {
            type = "Texture",
            default = "textures/white.bmp",
        },
        _RoughnessTex = {
            type = "Texture",
            default = "textures/white.bmp",
        },
        _AOTex = {
            type = "Texture",
            default = "textures/black.bmp",
        },
        _ScaleOffset = {
            type = "Vector",
            default = { x = 1, y = 1, z = 0, w = 0},
        },
        _Color = {
            type = "Color",
            default = { r = 255, g = 255, b = 255, a = 255},
        },
        _Metallic = {
            type = "Float",
            default = 0.01,
        }, 
        _Roughness = {
            type = "Float",
            default = 0.9,
        }, 
        _AO = {
            type = "Float",
            default = 1.0,
        }, 
    },
    Settings = {
        NormalMap = "_NormalTex",
    },
    SubShader = {
        ZTest = true,
        ZWrite = true,

        Queue = "Geometry",
        RenderType = "Opaque",
        GenerateInstancing = true,

        PassOne = {
            NamePass = "BasicPass",
            TypePass = "BasicColor",

            StaticDefines = {
                "USE_TEXTURES"
            },

            Includes = {
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/matricesCamera.glsl",
                "shaders/newShaders/libs/matricesObject.glsl",
                "shaders/newShaders/libs/vertexOutBasic.vert",
                "shaders/newShaders/libs/fragmentInBasic.frag",
                "shaders/newShaders/libs/light.glsl",
                "shaders/newShaders/libs/modelLightsFormuls/pbr.glsl",
            },
            Vertex = "shaders/newShaders/standardShader/vert.vert",
            Fragment = "shaders/newShaders/standardShader/frag.frag",
        
        },
        DepthNormalPass = {
            NamePass = "DepthNormalPass",
            TypePass = "DepthNormal",
            
            UseDefaultPrograms = true,
            UseNormalMap = true,
            UseClippingMap = false,
        },
        ShadowPass = {
            NamePass = "ShadowPass",
            TypePass = "Shadow",

            UseDefaultPrograms = true,
            UseNormalMap = false,
            UseClippingMap = false,
        },
    }
}