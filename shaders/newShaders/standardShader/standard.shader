Shader = {
    Name = "Standard",
    Properties = {
        _Color = {
            type = "Color",
            default = { r = 255, g = 255, b = 255, a = 255},
        },
        _Metallic = {
            type = "Float",
            default = 0.01,
        }, 
        _Roughness = {
            type = "Float",
            default = 0.95,
        },
        _AO = {
            type = "Float",
            default = 1.0,
        }, 
    },
    SubShader = {
        ZTest = true,
        ZWrite = true,

        Queue = "Geometry",
        RenderType = "Opaque",
        GenerateInstancing = true,

        --[[PassOne = {
            NamePass = "BasicPass",
            TypePass = "BasicColor",

            
            Includes = {
                "shaders/newShaders/libs/additionalDefines.glsl",
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/matricesCamera.glsl",
                "shaders/newShaders/libs/matricesObject.glsl",
                "shaders/newShaders/libs/vertexOutBasic.vert",
                "shaders/newShaders/libs/fragmentInBasic.frag",
                "shaders/newShaders/libs/light.glsl",
            },
            Vertex = "shaders/newShaders/standardShader/vert.vert",
            Fragment = "shaders/newShaders/standardShader/frag.frag",
        
        },
        DepthNormalPass = {
            NamePass = "DepthNormalPass",
            TypePass = "DepthNormal",
            
            UseDefaultPrograms = true,
            UseNormalMap = false,
            UseClippingMap = false,
        },]]--

        SurfacePass = {
            NamePass = "SurfacePass",
            TypePass = "Surface",

            ModelLight = "Standard",

            Includes = {
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/matricesCamera.glsl",
                "shaders/newShaders/libs/matricesObject.glsl",
                "shaders/newShaders/libs/vertexOutBasic.vert",
                "shaders/newShaders/libs/fragmentInBasic.frag",
                "shaders/newShaders/libs/surface.glsl",
            },

            Vertex = "shaders/newShaders/standardShader/vert.vert",
            Fragment = "shaders/newShaders/standardShader/frag.frag",
        },
        ShadowPass = {
            NamePass = "ShadowPass",
            TypePass = "Shadow",

            UseDefaultPrograms = true,
            UseNormalMap = false,
            UseClippingMap = false,
        },
    }
}