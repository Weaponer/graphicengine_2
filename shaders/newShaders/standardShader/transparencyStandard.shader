Shader = {
    Name = "StandardTransparent",
    Properties = {
        _Color = {
            type = "Color",
            default = { r = 255, g = 255, b = 255, a = 255},
        },
        _Metallic = {
            type = "Float",
            default = 0.01,
        }, 
        _Roughness = {
            type = "Float",
            default = 0.85,
        }, 
    },
    SubShader = {
        ZTest = true,
        ZWrite = false,

        Queue = "Transparent",
        RenderType = "Transparent",
        
        PassOne = {
            NamePass = "BasicPass",
            TypePass = "BasicColor",

            
            Includes = {
                "shaders/newShaders/libs/additionalDefines.glsl",
                "shaders/newShaders/libs/modelLayers.vert",
                "shaders/newShaders/libs/matricesCamera.glsl",
                "shaders/newShaders/libs/matricesObject.glsl",
                "shaders/newShaders/libs/vertexOutBasic.vert",
                "shaders/newShaders/libs/fragmentInBasic.frag",
                "shaders/newShaders/libs/transparent.glsl",
                "shaders/newShaders/libs/lightTransparent.glsl",
                "shaders/newShaders/libs/modelLightsFormuls/pbr.glsl",
            },
            Vertex = "shaders/newShaders/standardShader/vert.vert",
            Fragment = "shaders/newShaders/standardShader/frag.frag",
        
        },
    }
}