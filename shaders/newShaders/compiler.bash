#!/bin/bash
./output/cshader -I shaders/newShaders/standardShader/standard.shader -O shaders/newShaders/compiledShaders/standard.cshader
./output/cshader -I shaders/newShaders/standardShader/standardTextures.shader -O shaders/newShaders/compiledShaders/standardTextures.cshader
./output/cshader -I shaders/newShaders/standardShader/transparencyStandard.shader -O shaders/newShaders/compiledShaders/transparencyStandard.cshader
./output/cshader -I shaders/newShaders/skyShaders/defaultSky/defaultSky.shader -O shaders/newShaders/compiledShaders/defaultSky.cshader
./output/cshader -I shaders/newShaders/skyShaders/cubeSky/cubeSky.shader -O shaders/newShaders/compiledShaders/cubeSky.cshader