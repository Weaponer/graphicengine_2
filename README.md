
# GraphicEngine

OpenGL graphic engine & simple physics engine & lua scripts.

Possibilities:
- 
- Render opaque geometry.
- Render transparent geometry.
- Point light.
- Direction Light.
- Shadows of direction and point light.
- Volumetic fog.
- Density volume (fog).
- HDR rendering.
- Post processing (Bloom, SSAO, Vignette).
- Different light models (PBR, Lambert).
- Custom shaders for objects.
- Custom shaders for sky box.
- Simple physycs (deprecated)
- Loader images: jpg, png, bmp, hdr etc.
- Loader models obj.
- Support lua script and custom components.



## Demo

![](https://gitlab.com/Weaponer/graphicengine_2/-/raw/master/doc/screenshot_1.jpg?ref_type=heads)

![](https://gitlab.com/Weaponer/graphicengine_2/-/raw/master/doc/screenshot_2.jpg?ref_type=heads)

![](https://gitlab.com/Weaponer/graphicengine_2/-/raw/master/doc/screenshot_3.jpg?ref_type=heads)

![](https://gitlab.com/Weaponer/graphicengine_2/-/raw/master/doc/screenshot_4.jpg?ref_type=heads)

![](https://gitlab.com/Weaponer/graphicengine_2/-/raw/master/doc/screenshot_5.jpg?ref_type=heads)

![](https://gitlab.com/Weaponer/graphicengine_2/-/raw/master/doc/screenshot_6.jpg?ref_type=heads)

![](https://gitlab.com/Weaponer/graphicengine_2/-/raw/master/doc/screenshot_7.jpg?ref_type=heads)

![](https://gitlab.com/Weaponer/graphicengine_2/-/raw/master/doc/screenshot_8.jpg?ref_type=heads)
## Dependencies

- OpenGL
- SDL2
- Lua5.2
- [ShaderAssembler](https://gitlab.com/Weaponer/shaderassembler.git)
- [MathLib](https://gitlab.com/Weaponer/mathlib-linux.git)

## Run Locally

Clone the project

```bash
  git clone git@gitlab.com:Weaponer/graphicengine_2.git
```

Go to the project directory

```bash
  cd graphicengine_2
```

Install dependencies (Ubuntu Linux)

```bash
  sudo ./.packages/install_packages
```

Make engine

```bash
  make
```

Launch

```bash
  ./output/main
```
## Usage/Examples

The engine uses configs files to launching:
- /config

```lua
Graphic.Internal.Width = 1400
Graphic.Internal.Height = 700

Graphic.Internal.GridCountLayers = 4
Graphic.Internal.GridMaxSizeCell = 162
Graphic.Internal.GridStepDived = 3
```

The main files that will be loaded on start are placed in:
- /config/paths.cfg

```lua
    LuaScripts = {
        --Scripts
    }

    TechnicalShaders = {
        --Internal shaders
    }

    ShaderModels = {
        --Light models
    }

    BasicShaders = {
        --Shaders for object and sky box
    }
```

Lua functions is described in:
- /luaLibs
If you use Visual Code with extension [sumneko.lua](https://luals.github.io/) you`ll be able to use engine function hints.
## Scripts Examples

- Example component script

```lua
---@class testTransform
testTransform = {
    type = "testTransform",
    prototype = "script"
}

function testTransform:start()
    self.child = self.object:getChild(0)
    self.rotate = Vector3.new(0, 0, 0)
end

function testTransform:update()
    self.child:setLocalPos(self.child:localPos() + Vector3.new(0, 0.005, 0))
    self.rotate = self.rotate + Vector3.new(0.3, 0.6, 0)
    local rot = Quaternion.new()
    rot:setEuler(self.rotate:x(), self.rotate:y(), self.rotate:z())
    self.object:setRotate(rot)

    if Input.keyboard():getButtonDown(KeyCode.E) then
        self.child:setParent(nil)
        --self.child:setPos(Vector3.new(0, 14, 0))
        --self.child:setRotate(Quaternion.new())        
    end
end

function testTransform:onDiscard()
end

return testTransform
```

- Example basic callback function of component

```lua
---@class testClass
testClass = {}

testClass.type = "testClass"
testClass.prototype = "script"


---@type GameObject
testClass.object = testClass.object

function testClass:start()
    
    Debug.log("Test calss start " .. self.object:name())
end 

function testClass:update()
    Debug.log("Test calss update " .. self.object:name())
    
end 

function testClass:lateUpdate()
    Debug.log("Test calss lateUpdate " .. self.object:name())
    
end 

function testClass:onDiscard()
    Debug.log("Test calss onDiscard " .. self.object:name())
    
end 

function testClass:onEnable()
    Debug.log("Test calss onEnable " .. self.object:name())
end 

function testClass:onDisable()
    Debug.log("Test calss onDisable " .. self.object:name())
end 

return testClass
```