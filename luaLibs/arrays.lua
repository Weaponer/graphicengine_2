---@meta

---@class ArrayInt
---@operator len() : integer
ArrayInt = {}

---@param sizeX integer
---@param sizeY integer
---@param sizeZ integer
---@param defaultValue integer
---@return ArrayInt
---@overload fun(sizeX: integer): ArrayInt
---@overload fun(sizeX: integer, sizeY: integer): ArrayInt
---@overload fun(sizeX: integer, sizeY: integer, sizeZ: integer): ArrayInt
function ArrayInt.new(sizeX, sizeY, sizeZ, defaultValue) end

---@param array ArrayInt
---@param indexX integer
---@param indexY integer
---@param indexZ integer
---@return integer
---@overload fun(array: ArrayInt, indexX: integer): integer
---@overload fun(array: ArrayInt, indexX: integer, indexY: integer): integer
function ArrayInt:get(array, indexX, indexY, indexZ) end

---@param array ArrayInt
---@param value integer
---@param indexX integer
---@param indexY integer
---@param indexZ integer
---@overload fun(array: ArrayInt, value: integer, indexX: integer): nil
---@overload fun(array: ArrayInt, value: integer, indexX: integer, indexY: integer): nil
function ArrayInt.set(array, value, indexX, indexY, indexZ) end


---@class ArrayFloat
---@operator len() : integer
ArrayFloat = {}

---@param sizeX integer
---@param sizeY integer
---@param sizeZ integer
---@param defaultValue number
---@return ArrayFloat
---@overload fun(sizeX: integer): ArrayFloat
---@overload fun(sizeX: integer, sizeY: integer): ArrayFloat
---@overload fun(sizeX: integer, sizeY: integer, sizeZ: integer): ArrayFloat
function ArrayFloat.new(sizeX, sizeY, sizeZ, defaultValue) end

---@param array ArrayFloat
---@param indexX integer
---@param indexY integer
---@param indexZ integer
---@return number
---@overload fun(array: ArrayFloat, indexX: integer): integer
---@overload fun(array: ArrayFloat, indexX: integer, indexY: integer): integer
function ArrayFloat:get(array, indexX, indexY, indexZ) end

---@param array ArrayFloat
---@param value number
---@param indexX integer
---@param indexY integer
---@param indexZ integer
---@overload fun(array: ArrayFloat, value: number, indexX: integer): nil
---@overload fun(array: ArrayFloat, value: number, indexX: integer, indexY: integer): nil
function ArrayFloat.set(array, value, indexX, indexY, indexZ) end

---@class ArrayBool
---@operator len() : integer
ArrayBool = {}

---@param sizeX integer
---@param sizeY integer
---@param sizeZ integer
---@param defaultValue boolean
---@return ArrayBool
---@overload fun(sizeX: integer): ArrayBool
---@overload fun(sizeX: integer, sizeY: integer): ArrayBool
---@overload fun(sizeX: integer, sizeY: integer, sizeZ: integer): ArrayBool
function ArrayBool.new(sizeX, sizeY, sizeZ, defaultValue) end

---@param array ArrayBool
---@param indexX integer
---@param indexY integer
---@param indexZ integer
---@return boolean
---@overload fun(array: ArrayBool, indexX: integer): boolean
---@overload fun(array: ArrayBool, indexX: integer, indexY: integer): boolean
function ArrayBool:get(array, indexX, indexY, indexZ) end

---@param array ArrayBool
---@param value boolean
---@param indexX integer
---@param indexY integer
---@param indexZ integer
---@overload fun(array: ArrayBool, value: boolean, indexX: integer): nil
---@overload fun(array: ArrayBool, value: boolean, indexX: integer, indexY: integer): nil
function ArrayBool.set(array, value, indexX, indexY, indexZ) end