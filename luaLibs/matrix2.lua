---@meta

---@class Matrix2
---@operator add(Matrix2) : Matrix2
---@operator sub(Matrix2) : Matrix2
---@operator mul(Matrix2) : Matrix2
---@operator mul(number) : Matrix2
---@operator div(number) : Matrix2
Matrix2 = {}

---@param num0 number
---@param num1 number
---@param num2 number
---@param num3 number
---@return Matrix2
---@overload fun(mtx2: Matrix2): Matrix2
---@overload fun(): Matrix2
function Matrix2.new(num0, num1, num2, num3) end

---@param vec2_one Vector2
---@param vec2_two Vector2
function Matrix2:setOrientation(vec2_one, vec2_two) end

---@param num number
function Matrix2:setOrientation(num) end

function Matrix2:transpose() end

function Matrix2:setSingleMatrix() end

---@param vec2 Vector2
---@return Vector2
function Matrix2:transform(vec2) end

---@param vec2 Vector2
---@return Vector2
function Matrix2:transformTranspose(vec2) end

function Matrix2:inverse() end

function Matrix2:inverseRotate() end

---@param num number
---@return Vector2
function Matrix2:getVector(num) end

---@param num number
---@return Vector2
function Matrix2:getVectorTranspose(num) end

---@return number
function Matrix2:getRadian() end

---@param index number
---@return number
function Matrix2:get(index) end

---@param index number
---@param num number
function Matrix2:set(index, num) end