---@meta

---@class Matrix4
---@operator add(Matrix4) : Matrix4
---@operator sub(Matrix4) : Matrix4
---@operator mul(Matrix4) : Matrix4
---@operator mul(number) : Matrix4
---@operator div(number) : Matrix4
Matrix4 = {}

---@param num0_0 number
---@param num0_1 number
---@param num0_2 number
---@param num0_3 number
---@param num1_0 number
---@param num1_1 number
---@param num1_2 number
---@param num1_3 number
---@param num2_0 number
---@param num2_1 number
---@param num2_2 number
---@param num2_3 number
---@param num3_0 number
---@param num3_1 number
---@param num3_2 number
---@param num3_3 number
---@return Matrix4
---@overload fun(mtx4: Matrix4): Matrix4
---@overload fun(): Matrix4
function Matrix4.new(num0_0, num0_1, num0_2, num0_3, num1_0, num1_1, num1_2, num1_3, num2_0, num2_1, num2_2, num2_3, num3_0, num3_1, num3_2, num3_3) end

---@param mtx4 Matrix4
---@return Matrix3
function Matrix4.M3fromM4(mtx4) end

function Matrix4:transpose() end

function Matrix4:setSingleMatrix() end

---@param vec3 Vector3
---@return Vector3
function Matrix4:transform(vec3) end

---@param vec3 Vector3
---@return Vector3
function Matrix4:transformTranspose(vec3) end

---@param vec3 Vector3
---@return Vector3
function Matrix4:transformNoMove(vec3) end

---@param vec3 Vector3
---@return Vector3
function Matrix4:transformTransposeNoMove(vec3) end

---@param index1 number
---@param index2 number
---@return number
function Matrix4:get(index1, index2) end

---@param index1 number
---@param index2 number
---@param num number
function Matrix4:set(index1, index2, num) end