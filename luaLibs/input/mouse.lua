---@meta

---@class Mouse
Mouse = {}

---@enum MouseButtons
MouseButtons = {
    left = 1,
    right = 2,
    middle = 3,
    button4 = 4,
    button5 = 5
}

---@return Vector2
function Mouse:position() end

---@return Vector2
function Mouse:deltaPosition() end

---@param mouseButton MouseButtons
---@return boolean
function Mouse:getButton(mouseButton) end

---@param mouseButton MouseButtons
---@return boolean
function Mouse:getButtonDown(mouseButton) end

---@param mouseButton MouseButtons
---@return boolean
function Mouse:getButtonUp(mouseButton) end