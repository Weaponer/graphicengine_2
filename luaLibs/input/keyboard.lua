---@meta

---@class Keyboard
Keyboard = {}

---@param keyCode KeyCode
---@return boolean
function Keyboard:getButton(keyCode) end

---@param keyCode KeyCode
---@return boolean
function Keyboard:getButtonDown(keyCode) end

---@param keyCode KeyCode
---@return boolean
function Keyboard:getButtonUp(keyCode) end