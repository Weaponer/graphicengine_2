---@meta

---@class Input
Input = {}

---@return Mouse
function Input.mouse() end

---@return Keyboard
function Input.keyboard() end