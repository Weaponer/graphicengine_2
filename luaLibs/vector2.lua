---@meta

---@class Vector2
---@operator add(Vector2) : Vector2
---@operator sub(Vector2) : Vector2
---@operator mul(Vector2) : number
---@operator mul(number) : Vector2
---@operator div(number) : Vector2
---@operator len(Vector2) : number
Vector2 = {}

---@param x number
---@param y number
---@return Vector2
---@overload fun(vec2: Vector2): Vector2
---@overload fun(num: number): Vector2
---@overload fun(): Vector2
function Vector2.new(x, y) end

---@param vec2_one Vector2
---@param vec2_two Vector2
---@return number
function Vector2.dot(vec2_one, vec2_two) end

---@param vec2 Vector2
---@return Vector2
function Vector2.perpendecular(vec2) end

---@param vec2 Vector2
---@return Vector2
function Vector2.normalized(vec2) end

---@return number
function Vector2:magnitude() end

function Vector2:normalize() end

---@return number
function Vector2:x() end

---@return number
function Vector2:y() end

---@param num number
function Vector2:setX(num) end

---@param num number
function Vector2:setY(num) end