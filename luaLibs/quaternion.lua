---@meta

---@class Quaternion
---@operator mul(Quaternion) : Quaternion
---@operator len(Quaternion) : number
Quaternion = {}

---@param num1 number
---@param num2 number
---@param num3 number
---@param num4 number
---@return Quaternion
---@overload fun(quat: Quaternion): Quaternion
---@overload fun(): Quaternion
function Quaternion.new(num1, num2, num3, num4) end

---@param num1 number
---@param num2 number
---@param num3 number
---@param num4 number
function Quaternion:setDirect(num1, num2, num3, num4) end

---@param num1 number
---@param num2 number
---@param num3 number
function Quaternion:setEuler(num1, num2, num3) end

---@param mtx3 Matrix3
function Quaternion:setMatrix(mtx3) end

---@return Vector3
function Quaternion:getEuler() end

---@return Matrix4
function Quaternion:getMatrix() end

---@return number
function Quaternion:magnitude() end

---@return number
function Quaternion:squareMagnitude() end

function Quaternion:normalize() end

function Quaternion:inverse() end

---@param num number
---@param vec3 Vector3
function Quaternion:addScaledVector(num, vec3) end

---@param vec3 Vector3
function Quaternion:rotateByVector(vec3) end

---@param vec3 Vector3
---@return Vector3
function Quaternion:rotateVector(vec3) end

---@return number
function Quaternion:x() end

---@return number
function Quaternion:y() end

---@return number
function Quaternion:z() end

---@return number
function Quaternion:w() end

---@param num number
function Quaternion:setX(num) end

---@param num number
function Quaternion:setY(num) end

---@param num number
function Quaternion:setZ(num) end

---@param num number
function Quaternion:setW(num) end