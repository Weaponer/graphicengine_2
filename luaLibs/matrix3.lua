---@meta

---@class Matrix3
---@operator add(Matrix3) : Matrix3
---@operator sub(Matrix3) : Matrix3
---@operator mul(Matrix3) : Matrix3
---@operator mul(number) : Matrix3
---@operator div(number) : Matrix3
Matrix3 = {}

---@param num0_0 number
---@param num0_1 number
---@param num0_2 number
---@param num1_0 number
---@param num1_1 number
---@param num1_2 number
---@param num2_0 number
---@param num2_1 number
---@param num2_2 number
---@return Matrix3
---@overload fun(mtx3: Matrix3): Matrix3
---@overload fun(): Matrix3
function Matrix3.new(num0_0, num0_1, num0_2, num1_0, num1_1, num1_2, num2_0, num2_1, num2_2) end

---@param vec3_one Vector3
---@param vec3_two Vector3
---@param vec3_three Vector3
function Matrix3:setOrientation(vec3_one, vec3_two, vec3_three) end

function Matrix3:transpose() end

function Matrix3:setSingleMatrix() end

---@param vec3 Vector3
---@return Vector3
function Matrix3:transform(vec3) end

---@param vec3 Vector3
---@return Vector3
function Matrix3:transformTranspose(vec3) end

function Matrix3:inverse() end

function Matrix3:inverseRotate() end

function Matrix3:inverseScale() end

---@param num number
---@return Vector3
function Matrix3:getVector(num) end

---@param num number
---@return Vector3
function Matrix3:getVectorTranspose(num) end

---@param index1 number
---@param index2 number
---@return number
function Matrix3:get(index1, index2) end

---@param index1 number
---@param index2 number
---@param num number
function Matrix3:set(index1, index2, num) end