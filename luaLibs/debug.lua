---@meta

---@class Debug
Debug = {}

--Write log in terminal: [Log] *
---@param log string
function Debug.log(log) end

--Write warning in terminal: [Warning] *
---@param log string
function Debug.logWarning(log) end

--Write error in terminal: [Error] *
---@param log string
function Debug.logError(log) end

--[[Draw a white line on one the frame.
You can change time render and color line.]]
---@param startPos Vector3
---@param endPos Vector3
---@vararg Vector3|number
function Debug.drawLine(startPos, endPos, ...) end

--[[Draw a white cube on one the frame.
You can change time render and line color and set a matrix for rotate.]]
---@param pos Vector3
---@param size Vector3
---@vararg Vector3|Matrix4|number
function Debug.drawCube(pos, size, ...) end

--[[Draw a white rect on one the frame.
You can change time render and line color and set a matrix for rotate.]]
---@param pos Vector3
---@param size Vector3
---@vararg Vector3|Matrix4|number
function Debug.drawRect(pos, size, ...) end
