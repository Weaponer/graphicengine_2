---@meta

---@class GameObject
GameObject = {}

---@enum Component
Component = {
    ---@class Component.pointLight
    ---@type Component.pointLight
    pointLight = nil,
    ---@class Component.meshRenderer
    ---@type Component.meshRenderer
    meshRenderer = nil,
    ---@class Component.boxCollider
    ---@type Component.boxCollider
    boxCollider = nil,
    ---@class Component.sphereCollider
    ---@type Component.sphereCollider
    sphereCollider = nil,
    ---@class Component.rigidbody
    ---@type Component.rigidbody
    rigidbody = nil,
    ---@class Component.camera
    ---@type Component.camera
    camera = nil,
    ---@class Component.densityVolume
    ---@type Component.densityVolume
    densityVolume = nil
}

---@param name string
---@param scene Scene
---@return GameObject
---@overload fun(name: string): GameObject
---@overload fun(scene: Scene): GameObject
function GameObject.create(name, scene) end

---@param object GameObject
function GameObject.destroy(object) end

---@param object GameObject
function GameObject.makeDontDestroy(object) end

---@param object GameObject
---@param scene Scene
function GameObject.moveOnAnotherScene(object, scene) end

---@return string
function GameObject:name() end

---@param name string
function GameObject:setName(name) end

---@return boolean
function GameObject:active() end

---@return boolean
function GameObject:localActive() end

---@param bool boolean
function GameObject:setActive(bool) end

---@param type string
---@overload fun(object: GameObject, type: Component.pointLight): PointLightComponent
---@overload fun(object: GameObject, type: Component.meshRenderer): MeshRendererComponent
---@overload fun(object: GameObject, type: Component.boxCollider): BoxColliderComponent
---@overload fun(object: GameObject, type: Component.sphereCollider): SphereColliderComponent
---@overload fun(object: GameObject, type: Component.rigidbody): RigidbodyComponent
---@overload fun(object: GameObject, type: Component.camera): CameraComponent
---@overload fun(object: GameObject, type: Component.densityVolume): DensityVolumeComponent
---@return any|nil
function GameObject:getComponent(type) end

---@return nil
function GameObject:getComponents() end

---@param type string
---@return any|nil
---@overload fun(object: GameObject, type: Component.pointLight): PointLightComponent
---@overload fun(object: GameObject, type: Component.meshRenderer): MeshRendererComponent
---@overload fun(object: GameObject, type: Component.boxCollider): BoxColliderComponent
---@overload fun(object: GameObject, type: Component.sphereCollider): SphereColliderComponent
---@overload fun(object: GameObject, type: Component.rigidbody): RigidbodyComponent
---@overload fun(object: GameObject, type: Component.camera): CameraComponent
---@overload fun(object: GameObject, type: Component.densityVolume): DensityVolumeComponent
function GameObject:addComponent(type) end

---@return Vector3
function GameObject:pos() end

---@param vec3 Vector3
function GameObject:setPos(vec3) end

---@return Vector3
function GameObject:localPos() end

---@param vec3 Vector3
function GameObject:setLocalPos(vec3) end

---@return Quaternion
function GameObject:rotate() end

---@return Vector3
function GameObject:rotateE() end

---@param quaternion Quaternion
function GameObject:setRotate(quaternion) end

---@param vec3 Vector3
function GameObject:setRotateE(vec3) end

---@return Quaternion
function GameObject:localRotate() end

---@return Vector3
function GameObject:localRotateE() end

---@param quaternion Quaternion
function GameObject:setLocalRotate(quaternion) end

---@param vec3 Vector3
function GameObject:setLocalRotateE(vec3) end

---@return Vector3
function GameObject:localScale() end

---@param vec3 Vector3
function GameObject:setLocalScale(vec3) end

---@return Matrix4
function GameObject:localMatrix() end

---@return Matrix4
function GameObject:globalMatrix() end

---@return GameObject|nil
function GameObject:parent() end

---@param object GameObject|nil
function GameObject:setParent(object) end

---@return number
function GameObject:countChildren() end

---@param index number
---@return GameObject|nil
function GameObject:getChild(index) end

---@return Layer
function GameObject:layer() end

---@param layer Layer
function GameObject:setLayer(layer) end