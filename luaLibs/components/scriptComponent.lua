--[[
---@meta

---@class ScriptComponent
ScriptComponent = {}

---@return GameObject
function ScriptComponent:gameObject() end

---@return boolean
function ScriptComponent:active() end

---@return boolean
function ScriptComponent:localActive() end

---@param status boolean
function ScriptComponent:setActive(status) end

function ScriptComponent:discard() end

---@param type string
function ScriptComponent:setType(type) end
]]