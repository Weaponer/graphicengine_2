---@meta

---@class MeshRendererComponent
MeshRendererComponent = {}

---@return GameObject
function MeshRendererComponent:gameObject() end

---@return boolean
function MeshRendererComponent:active() end

---@return boolean
function MeshRendererComponent:localActive() end

---@param status boolean
function MeshRendererComponent:setActive(status) end

function MeshRendererComponent:discard() end

---@return Mesh
function MeshRendererComponent:mesh() end

---@param mesh Mesh
function MeshRendererComponent:setMesh(mesh) end

---@return Material
function MeshRendererComponent:material() end

---@param material Material
function MeshRendererComponent:setMaterial(material) end

---@return integer
function MeshRendererComponent:countLods() end

function MeshRendererComponent:addLod() end

---@param index integer
function MeshRendererComponent:removeLod(index) end

---@param index integer
---@return Mesh
function MeshRendererComponent:meshInLod(index) end

---@param index integer
---@return Material
function MeshRendererComponent:materialInLod(index) end

---@param index integer
---@return number
function MeshRendererComponent:heightCoefInLod(index) end

---@param index integer
---@param mesh Mesh
function MeshRendererComponent:setMeshInLod(index, mesh) end

---@param index integer
---@param material Material
function MeshRendererComponent:setMaterialInLod(index, material) end

---@param index integer
---@param heightCoef number
function MeshRendererComponent:setHeightCoefInLod(index, heightCoef) end

---@return boolean
function MeshRendererComponent:generateShadows() end

---@param generate boolean
function MeshRendererComponent:setGenerateShadows(generate) end