---@meta

---@class PointLightComponent
PointLightComponent = {}

---@return GameObject
function PointLightComponent:gameObject() end

---@return boolean
function PointLightComponent:active() end

---@return boolean
function PointLightComponent:localActive() end

---@param status boolean
function PointLightComponent:setActive(status) end

function PointLightComponent:discard() end

---@return number
function PointLightComponent:range() end

---@param num number
function PointLightComponent:setRange(num) end

---@return number
function PointLightComponent:intensivity() end

---@param num number
function PointLightComponent:setIntensivity(num) end

---@return boolean
function PointLightComponent:isCreateShadow() end

---@param status boolean
function PointLightComponent:setIsCreateShadow(status) end

---@return Vector3
function PointLightComponent:color() end

---@param vec3 Vector3
function PointLightComponent:setColor(vec3) end