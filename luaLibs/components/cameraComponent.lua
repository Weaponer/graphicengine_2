---@meta

---@class CameraComponent
CameraComponent = {}

---@return GameObject
function CameraComponent:gameObject() end

---@return boolean
function CameraComponent:active() end

---@return boolean
function CameraComponent:localActive() end

---@param status boolean
function CameraComponent:setActive(status) end

function CameraComponent:discard() end

---@param weight number
---@param height number
---@param near number
---@param front number
function CameraComponent:setOrtographicProjection(weight, height, near, front) end

---@param angle number
---@param sizeScreenX number
---@param sizeScreenY number
---@param near number
---@param front number
function CameraComponent:setPerspectiveProjection(angle, sizeScreenX, sizeScreenY, near, front) end

---@return Matrix4
function CameraComponent:projectionMatrix() end