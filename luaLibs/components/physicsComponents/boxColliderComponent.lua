---@meta

---@class BoxColliderComponent
BoxColliderComponent = {}

---@return GameObject
function BoxColliderComponent:gameObject() end

---@return boolean
function BoxColliderComponent:active() end

---@return boolean
function BoxColliderComponent:localActive() end

---@param status boolean
function BoxColliderComponent:setActive(status) end

function BoxColliderComponent:discard() end

---@return Vector3
function BoxColliderComponent:origin() end

---@param vec3 Vector3
function BoxColliderComponent:setOrigin(vec3) end

---@return Vector3
function BoxColliderComponent:size() end

---@param vec3 Vector3
function BoxColliderComponent:setSize(vec3) end