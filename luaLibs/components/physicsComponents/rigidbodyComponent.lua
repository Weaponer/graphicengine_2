---@meta

---@class RigidbodyComponent
RigidbodyComponent = {}

---@return GameObject
function RigidbodyComponent:gameObject() end

---@return boolean
function RigidbodyComponent:active() end

---@return boolean
function RigidbodyComponent:localActive() end

---@param status boolean
function RigidbodyComponent:setActive(status) end

function RigidbodyComponent:discard() end

---@return number
function RigidbodyComponent:mass() end

---@param num number
function RigidbodyComponent:setMass(num) end

---@return boolean
function RigidbodyComponent:kinematic() end

---@param status boolean
function RigidbodyComponent:setKinematic(status) end

---@param force Vector3
function RigidbodyComponent:addForce(force) end

---@param force Vector3
---@param point Vector3
function RigidbodyComponent:addForceAtPoint(force, point) end

---@param torque Vector3
function RigidbodyComponent:addTorque(torque) end

---@return Vector3
function RigidbodyComponent:velocity() end

---@return Vector3
function RigidbodyComponent:angularVelocity() end

---@param velocity Vector3
function RigidbodyComponent:setVelocity(velocity) end

---@param velocity Vector3
function RigidbodyComponent:setAngularVelocity(velocity) end

---@param collider BoxColliderComponent|SphereColliderComponent
function RigidbodyComponent:disconnectCollider(collider) end

---@param collider BoxColliderComponent|SphereColliderComponent
function RigidbodyComponent:connectCollider(collider) end