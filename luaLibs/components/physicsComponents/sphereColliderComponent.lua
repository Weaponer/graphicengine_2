---@meta

---@class SphereColliderComponent
SphereColliderComponent = {}

---@return GameObject
function SphereColliderComponent:gameObject() end

---@return boolean
function SphereColliderComponent:active() end

---@return boolean
function SphereColliderComponent:localActive() end

---@param status boolean
function SphereColliderComponent:setActive(status) end

function SphereColliderComponent:discard() end

---@return Vector3
function SphereColliderComponent:origin() end

---@param vec3 Vector3
function SphereColliderComponent:setOrigin(vec3) end

---@return number
function SphereColliderComponent:radius() end

---@param num number
function SphereColliderComponent:setRadius(num) end