---@meta

---@class DensityVolumeComponent
DensityVolumeComponent = {}

---@return GameObject
function DensityVolumeComponent:gameObject() end

---@return boolean
function DensityVolumeComponent:active() end

---@return boolean
function DensityVolumeComponent:localActive() end

---@param status boolean
function DensityVolumeComponent:setActive(status) end

function DensityVolumeComponent:discard() end

---@enum DensityVolumeType
DensityVolumeType = {
    cube = 0,
    sphere = 1,
    cylinder = 2,
}

---@enum DensityVolumePlacement
DensityVolumePlacement = {
    planeXY = 0,
    planeXZ = 1,
    planeYZ = 2,
}

---@param type DensityVolumeType
function DensityVolumeComponent:setDensityVolumeType(type) end

---@return DensityVolumeType
function DensityVolumeComponent:densityVolumeType() end

---@param placement DensityVolumePlacement
function DensityVolumeComponent:setDensityVolumePlacementPlane(placement) end

---@return DensityVolumePlacement
function DensityVolumeComponent:densityVolumePlacementPlane() end

---@param density number
function DensityVolumeComponent:setDensity(density) end

---@return number
function DensityVolumeComponent:density() end

---@param color Color
function DensityVolumeComponent:setColorVolume(color) end

---@return Color
function DensityVolumeComponent:colorVolume() end

---@param gradient number
function DensityVolumeComponent:setPowGradient(gradient) end

---@return number
function DensityVolumeComponent:powGradient() end

---@param placement DensityVolumePlacement
function DensityVolumeComponent:setPlacementPlaneGradient(placement) end

---@return DensityVolumePlacement
function DensityVolumeComponent:placementPlaneGradient() end

---@param decay number
function DensityVolumeComponent:setStartDecay(decay) end

---@return number
function DensityVolumeComponent:startDecay() end

---@param decay number
function DensityVolumeComponent:setEndDecay(decay) end

---@return number
function DensityVolumeComponent:endDecay() end

---@param fade number
function DensityVolumeComponent:setEdgeFade(fade) end

---@return number
function DensityVolumeComponent:edgeFade() end