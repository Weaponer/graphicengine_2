---@meta

---@class Vector4
---@operator add(Vector4) : Vector4
---@operator sub(Vector4) : Vector4
---@operator mul(number) : Vector4
---@operator div(number) : Vector4
---@operator len(Vector4) : number
Vector4 = {}

---@param x number
---@param y number
---@param z number
---@param w number
---@return Vector4
---@overload fun(vec4: Vector4): Vector4
---@overload fun(num: number): Vector4
---@overload fun(): Vector4
function Vector4.new(x, y, z, w) end

---@param vec4 Vector4
---@return number
function Vector4.dot(vec4) end

---@param vec4 Vector4
---@return Vector4
function Vector4.normalized(vec4) end

---@return number
function Vector4:magnitude() end

function Vector4:normalize() end

---@return number
function Vector4:x() end

---@return number
function Vector4:y() end

---@return number
function Vector4:z() end

---@return number
function Vector4:w() end

---@param num number
function Vector4:setX(num) end

---@param num number
function Vector4:setY(num) end

---@param num number
function Vector4:setZ(num) end

---@param num number
function Vector4:setW(num) end