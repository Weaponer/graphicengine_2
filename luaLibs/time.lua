---@meta

---@class Time
Time = {}

---@return number
function Time.deltaTime() end

---@return number
function Time.lock() end

---@param num number
function Time.setLock(num) end