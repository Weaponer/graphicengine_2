---@meta

---@class VolumetricFog
Graphic.VolumetricFog = {}

---@return boolean
function Graphic.VolumetricFog.generateFog() end

---@param use boolean
function Graphic.VolumetricFog.setGenerateFog(use) end

---@return number
function Graphic.VolumetricFog.intensivityFog() end

---@param intensivity number
function Graphic.VolumetricFog.setIntensivityFog(intensivity) end

---@return number
function Graphic.VolumetricFog.anisotropic() end

---@param anisotropic number
function Graphic.VolumetricFog.setAnisotropic(anisotropic) end

---@return number
function Graphic.VolumetricFog.scattering() end

---@param scattering number
function Graphic.VolumetricFog.setScattering(scattering) end

---@return number
function Graphic.VolumetricFog.absorption() end

---@param absorption number
function Graphic.VolumetricFog.setAbsorption(absorption) end

---@return Vector3
function Graphic.VolumetricFog.sizeGridFog() end

---@param xSize integer
---@param ySize integer
---@param zSize integer
function Graphic.VolumetricFog.setSizeGridFog(xSize, ySize, zSize) end

---@return Color
function Graphic.VolumetricFog.colorFog() end

---@param color Color
function Graphic.VolumetricFog.setColorFog(color) end

---@return number
function Graphic.VolumetricFog.startDistanceFog() end

---@param distance number
function Graphic.VolumetricFog.setStartDistanceFog(distance) end

---@return number
function Graphic.VolumetricFog.endDistanceFog() end

---@param distance number
function Graphic.VolumetricFog.setEndDistanceFog(distance) end

---@return boolean
function Graphic.VolumetricFog.usePointsLightForce() end

---@param use boolean
function Graphic.VolumetricFog.setUsePointsLightForce(use) end

---@return boolean
function Graphic.VolumetricFog.generateRaysDirectionLight() end

---@param use boolean
function Graphic.VolumetricFog.setGenerateRaysDirectionLight(use) end

---@return number
function Graphic.VolumetricFog.maxDistanceRay() end

---@param distance number
function Graphic.VolumetricFog.setMaxDistanceRay(distance) end