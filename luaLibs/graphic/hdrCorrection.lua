---@meta

---@class HDRCorrection
Graphic.HDRCorrection = {}

---@return boolean
function Graphic.HDRCorrection.dynamicHDRCorrection() end

---@param use boolean
function Graphic.HDRCorrection.setDynamicHDRCorrection(use) end

---@return number
function Graphic.HDRCorrection.staticValue() end

---@param value number
function Graphic.HDRCorrection.setStaticValue(value) end

---@return number
function Graphic.HDRCorrection.minimumLogLuminance() end

---@param value number
function Graphic.HDRCorrection.setMinimumLogLuminance(value) end

---@return number
function Graphic.HDRCorrection.inverseLogLuminanceRange() end

---@param value number
function Graphic.HDRCorrection.setInverseLogLuminanceRange(value) end