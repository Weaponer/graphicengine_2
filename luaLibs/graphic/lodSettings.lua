---@meta

---@class LODSettings
Graphic.LODSettings = {}

---@return boolean
function Graphic.LODSettings.useLods() end

---@param use boolean
function Graphic.LODSettings.setUseLods(use) end

---@return boolean
function Graphic.LODSettings.useLodsForShadow() end

---@param use boolean
function Graphic.LODSettings.setUseLodsForShadow(use) end

---@return number
function Graphic.LODSettings.lodBias() end

---@param value number
function Graphic.LODSettings.setLodBias(value) end

---@return number
function Graphic.LODSettings.lodShadowBias() end

---@param value number
function Graphic.LODSettings.setLodShadowBias(value) end