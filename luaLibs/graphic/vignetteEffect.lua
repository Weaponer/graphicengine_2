---@meta

---@class VignetteEffect
Graphic.VignetteEffect = {}

---@return boolean
function Graphic.VignetteEffect.generateVignetteEffect() end

---@param generate boolean
function Graphic.VignetteEffect.setGenerateVignetteEffect(generate) end

---@return Color
function Graphic.VignetteEffect.color() end

---@param value Color
function Graphic.VignetteEffect.setColor(value) end

---@return number
function Graphic.VignetteEffect.radius() end

---@param value number
function Graphic.VignetteEffect.setRadius(value) end

---@return number
function Graphic.VignetteEffect.pow() end

---@param value number
function Graphic.VignetteEffect.setPow(value) end