---@meta

---@class Shadow
Graphic.Shadow = {}

---@enum ReductionType
ReductionType = {
    reductionLinear = 0,
    reductionSquare = 1,
}

---@param part integer
---@return number
function Graphic.Shadow.getShadowBias(part) end

---@param part integer
---@param bias number
function Graphic.Shadow.setShadowBias(part, bias) end

---@param dist number
function Graphic.Shadow.setShadowDistance(dist) end

---@return number
function Graphic.Shadow.shadowDistance() end

---@param dist number
function Graphic.Shadow.setShadowDistance(dist) end

---@return number
function Graphic.Shadow.maxUpShadowDistance() end

---@param dist number
function Graphic.Shadow.setMaxUpShadowDistance(dist) end

---@return integer
function Graphic.Shadow.degreePartition() end

---@param degree integer
function Graphic.Shadow.setDegreePartition(degree) end

---@return integer
function Graphic.Shadow.sizeSide() end

---@param size integer
function Graphic.Shadow.setSizeSide(size) end

---@return number
function Graphic.Shadow.getStartFadeDistance() end

---@param dist number
function Graphic.Shadow.setStartFadeDistance(dist) end

---@return ReductionType
function Graphic.Shadow.reductionTypePartitionVolume() end

---@param reduction ReductionType
function Graphic.Shadow.setReductionTypePartitionVolume(reduction) end

---@return ReductionType
function Graphic.Shadow.reductionTypeSizePartition() end

---@param reduction ReductionType
function Graphic.Shadow.setReductionTypeSizePartition(reduction) end