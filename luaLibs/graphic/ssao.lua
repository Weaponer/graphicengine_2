---@meta

---@class SSAO
Graphic.SSAO = {}

---@return boolean
function Graphic.SSAO.generateSSAO() end

---@param use boolean
function Graphic.SSAO.setGenerateSSAO(use) end
