---@meta

---@class Sky
Graphic.Sky = {}

---@enum TypeUpdateSky
TypeUpdateSky = {
    everyFrame = 0,
    call = 1,
}

---@return TypeUpdateSky
function Graphic.Sky.typeUpdateSky() end

---@param typeUpdate TypeUpdateSky
function Graphic.Sky.setTypeUpdateSky(typeUpdate) end

---@return Material
function Graphic.Sky.material() end

---@param material Material|nil
function Graphic.Sky.setMaterial(material) end

---@return Vector3
function Graphic.Sky.directionSun() end

---@param dir Vector3
function Graphic.Sky.setDirectionSun(dir) end

---@return Color
function Graphic.Sky.colorSun() end

---@param col Color
function Graphic.Sky.setColorSun(col) end

---@return number
function Graphic.Sky.intensity() end

---@param intensity number
function Graphic.Sky.setIntensity(intensity) end

---@return Color
function Graphic.Sky.ambientLightColor() end

---@param col Color
function Graphic.Sky.setAmbientLightColor(col) end

---@return number
function Graphic.Sky.intensityAmbientLight() end

---@param intensity number
function Graphic.Sky.setIntensityAmbientLight(intensity) end

---@return boolean
function Graphic.Sky.environmentBufferAsAmbientLight() end

---@param enable boolean
function Graphic.Sky.setEnvironmentBufferAsAmbientLight(enable) end

function Graphic.Sky.callUpdate() end