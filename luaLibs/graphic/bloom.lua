---@meta

---@class Bloom
Graphic.Bloom = {}

---@return boolean
function Graphic.Bloom.generateBloom() end

---@param use boolean
function Graphic.Bloom.setGenerateBloom(use) end

---@return integer
function Graphic.Bloom.countSamples() end

---@param count integer
function Graphic.Bloom.setCountSamples(count) end

---@return number
function Graphic.Bloom.force() end

---@param force number
function Graphic.Bloom.setForce(force) end