---@meta

---@class Model
Model = {}

---@return string
function Model:names() end

---@return number
function Model:countMeshes() end

---@param num number
---@return Mesh
function Model:meshOnIndex(num) end

---@param str string
---@return Mesh
function Model:meshOnName(str) end