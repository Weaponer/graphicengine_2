---@meta

---@class Material
Material = {}

---@return string
function Material:name() end

---@param name string
function Material:setName(name) end

---@return Shader
function Material:shader() end

---@param name string
---@param value integer
function Material:setInt(name, value) end

---@param name string
---@param value number
function Material:setFloat(name, value) end

---@param name string
---@param value boolean
function Material:setToggle(name, value) end

---@param name string
---@param value Vector4
function Material:setVector(name, value) end

---@param name string
---@param value Color
function Material:setColor(name, value) end

---@param name string
---@param value Texture
function Material:setTexture(name, value) end

---@param name string
---@param value CubeMap
function Material:setCubeMap(name, value) end

---@param name string
---@return integer
function Material:getInt(name) end

---@param name string
---@return number
function Material:getFloat(name) end

---@param name string
---@return boolean
function Material:getToggle(name) end

---@param name string
---@return Vector4
function Material:getVector(name) end

---@param name string
---@return Color
function Material:getColor(name) end

---@param name string
---@return Texture
function Material:getTexture(name) end

---@param name string
---@return CubeMap
function Material:getCubeMap(name) end

---@return boolean
function Material:generateInstancing() end

---@param generate boolean
function Material:setGenerateInstancing(generate) end