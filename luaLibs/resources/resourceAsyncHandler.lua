---@meta

---@class ResourceAsyncHandler
ResourceAsyncHandler = {}

---@enum StatusLoad
StatusLoad = {
    none = 0,
    failed = 1,
    succeed = 2
}

---@return string
function ResourceAsyncHandler:path() end

---@return StatusLoad
function ResourceAsyncHandler:status() end

---@return Material|Texture|Model|CubeMap|Shader|nil
function ResourceAsyncHandler:resource() end

function ResourceAsyncHandler:discard() end

---@return boolean
function ResourceAsyncHandler:isDiscard() end