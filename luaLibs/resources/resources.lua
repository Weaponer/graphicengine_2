---@meta

---@class Resources
Resources = {}

---@enum Resource
Resource = {
    ---@class Resource.material
    ---@type Resource.material
    material = nil,
    ---@class Resource.modelOBJ
    ---@type Resource.modelOBJ
    modelOBJ = nil,
    ---@class Resource.texture
    ---@type Resource.texture
    texture = nil,
    ---@class Resource.hdrTexture
    ---@type Resource.hdrTexture
    hdrTexture = nil,
    ---@class Resource.cubeMap
    ---@type Resource.cubeMap
    cubeMap = nil,
    ---@class Resource.shader
    ---@type Resource.shader
    shader = nil
}

---@param type Resource.material
---@param path string
---@return Material|nil
---@overload fun(type: Resource.modelOBJ, path: string): Model|nil
---@overload fun(type: Resource.texture, path: string): Texture|nil
---@overload fun(type: Resource.hdrTexture, path: string): Texture|nil
---@overload fun(type: Resource.cubeMap, path: string): CubeMap|nil
---@overload fun(type: Resource.shader, path: string): Shader|nil
function Resources.loadResource(type, path) end

---@param resource Material|Texture|Model|CubeMap|Shader
function Resources.unloadResource(resource) end

---@param type Resource.material
---@param path string
---@return ResourceAsyncHandler
---@overload fun(type: Resource.modelOBJ, path: string): ResourceAsyncHandler
---@overload fun(type: Resource.texture, path: string): ResourceAsyncHandler
---@overload fun(type: Resource.hdrTexture, path: string): ResourceAsyncHandler
---@overload fun(type: Resource.cubeMap, path: string): ResourceAsyncHandler
---@overload fun(type: Resource.shader, path: string): ResourceAsyncHandler
function Resources.loadAsyncResource(type, path) end

---@param resourceAsyncHandler ResourceAsyncHandler
function Resources.unloadAsyncResource(resourceAsyncHandler) end