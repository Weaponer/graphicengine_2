---@meta

---@class Shader
Shader = {}

---@param name string
---@return Shader
function Shader.find(name) end

---@param name string
---@return integer
function Shader.findPropertyInt(name) end

---@param name string
---@return integer
function Shader.findPropertyFloat(name) end

---@param name string
---@return integer
function Shader.findPropertyToggle(name) end

---@param name string
---@return integer
function Shader.findPropertyVector(name) end

---@param name string
---@return integer
function Shader.findPropertyColor(name) end

---@param name string
---@return integer
function Shader.findPropertyTexture(name) end

---@param name string
---@return integer
function Shader.findPropertyCubeMap(name) end

---@param index integer
---@param use boolean
function Shader.setUseGlobalProperty(index, use) end

---@param index integer
---@return boolean
function Shader.useGlobalProperty(index) end

---@param index integer
---@return integer
---@overload fun(name: string): integer
function Shader.globalPropertyInt(index) end

---@param index integer
---@return number
---@overload fun(name: string): number
function Shader.globalPropertyFloat(index) end

---@param index integer
---@return Vector4
---@overload fun(name: string): Vector4
function Shader.globalPropertyVector(index) end

---@param index integer
---@return Color
---@overload fun(name: string): Color
function Shader.globalPropertyColor(index) end

---@param index integer
---@return Texture
---@overload fun(name: string): Texture
function Shader.globalPropertyTexture(index) end

---@param index integer
---@return CubeMap
---@overload fun(name: string): CubeMap
function Shader.globalPropertyCubeMap(index) end

---@param index integer
---@param value integer
---@overload fun(name: string, value: integer)
function Shader.setGlobalPropertyInt(index, value) end

---@param index integer
---@param value number
---@overload fun(name: string, value: number)
function Shader.setGlobalPropertyFloat(index, value) end

---@param index integer
---@param value Vector4
---@overload fun(name: string, value: Vector4)
function Shader.setGlobalPropertyVector(index, value) end

---@param index integer
---@param value Color
---@overload fun(name: string, value: Color)
function Shader.setGlobalPropertyColor(index, value) end

---@param index integer
---@param value Texture
---@overload fun(name: string, value: Texture)
function Shader.setGlobalPropertyTexture(index, value) end

---@param index integer
---@param value CubeMap
---@overload fun(name: string, value: CubeMap)
function Shader.setGlobalPropertyCubeMap(index, value) end


---@return string
function Shader:name() end

---@return integer
function Shader:countProperties() end

---@param index integer
---@return boolean
function Shader:haveProperty(index) end