---@meta

---@class Mesh
---@operator len(Mesh) : number
Mesh = {}

---@return string
function Mesh:name() end

---@return number
function Mesh:indexes() end