---@meta

---@class Screen
Screen = {}

---@return number
function Screen.wigth() end

---@return number
function Screen.height() end

---@return boolean
function Screen.mouseLock() end

---@param bool boolean
function Screen.setMouseLock(bool) end