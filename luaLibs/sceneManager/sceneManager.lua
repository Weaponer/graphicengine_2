---@meta

---@class SceneManager
SceneManager = {}

---@param str string
---@return Scene
function SceneManager.createScene(str) end

---@param scene Scene
---@return boolean
function SceneManager.removeScene(scene) end

---@return Scene
function SceneManager.getCurrentScene() end

---@param str string
---@return Scene
function SceneManager.getSceneByName(str) end

---@param scene Scene
function SceneManager.setSceneDefault(scene) end