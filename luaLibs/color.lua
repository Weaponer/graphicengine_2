---@meta

---@class Color
---@operator add(Color) : Color
---@operator sub(Color) : Color
---@operator mul(Color) : Color
---@operator mul(number) : Color
---@operator div(number) : Color
Color = {}

---@param r number
---@param g number
---@param b number
---@param a number
---@return Color
---@overload fun(col: Color): Color
---@overload fun(num: number): Color
---@overload fun(): Color
function Color.new(r, g, b, a) end


---@param col Color
---@return Color
function Color.normalized(col) end

---@return Color
function Color.white() end

---@return Color
function Color.black() end

---@return Color
function Color.red() end

---@return Color
function Color.green() end

---@return Color
function Color.blue() end

---@return Color
function Color.cyan() end

---@return Color
function Color.gray() end

---@return Color
function Color.magenta() end

---@return Color
function Color.yellow() end

---@return number
function Color:r() end

---@return number
function Color:g() end

---@return number
function Color:b() end

---@return number
function Color:a() end

---@param num number
function Color:setR(num) end

---@param num number
function Color:setG(num) end

---@param num number
function Color:setB(num) end

---@param num number
function Color:setA(num) end

---@param r number
---@param g number
---@param b number
---@param a number
---@overload fun(r: number, g: number, b: number)
function Color:setColor(r, g, b, a) end

---@param r integer
---@param g integer
---@param b integer
---@param a integer
---@overload fun(r: integer, g: integer, b: integer)
function Color:setColorByte(r, g, b, a) end

function Color:normalize() end

---@return Vector4
function Color:toHSVA() end

---@param HSVA Vector4
function Color:setHSVA(HSVA) end