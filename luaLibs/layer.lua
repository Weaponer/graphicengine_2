---@meta

---@class Layer
Layer = {}

---@param str string
---@return Layer
function Layer.layerByName(str) end

---@param num number
---@return Layer
function Layer.layerByNumber(num) end

---@return Layer
function Layer.defaultLayer() end

---@return string
function Layer:name() end

---@return number
function Layer:number() end