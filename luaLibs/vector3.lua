---@meta

---@class Vector3
---@operator add(Vector3) : Vector3
---@operator sub(Vector3) : Vector3
---@operator mul(Vector3) : number
---@operator mul(number) : Vector3
---@operator div(number) : Vector3
---@operator len(Vector3) : number
Vector3 = {}

---@param x number
---@param y number
---@param z number
---@return Vector3
---@overload fun(vec3: Vector3): Vector3
---@overload fun(num: number): Vector3
---@overload fun(): Vector3
function Vector3.new(x, y, z) end

---@param vec3_one Vector3
---@param vec3_two Vector3
---@return number
function Vector3.dot(vec3_one, vec3_two) end

---@param vec3_one Vector3
---@param vec3_two Vector3
---@return Vector3
function Vector3.cross(vec3_one, vec3_two) end

---@param vec3 Vector3
---@return Vector3
function Vector3.normalized(vec3) end

---@return number
function Vector3:magnitude() end

function Vector3:normalize() end

---@return number
function Vector3:x() end

---@return number
function Vector3:y() end

---@return number
function Vector3:z() end

---@param num number
function Vector3:setX(num) end

---@param num number
function Vector3:setY(num) end

---@param num number
function Vector3:setZ(num) end
